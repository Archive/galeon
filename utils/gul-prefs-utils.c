/*
 *  Copyright (C) 2000 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "gul-prefs-utils.h"
#include "gul-gui.h"
#include "eel-gconf-extensions.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <gtk/gtkmenu.h>
#include <gtk/gtkmenushell.h>
#include <gtk/gtkspinbutton.h>
#include <gtk/gtktogglebutton.h>
#include <gtk/gtkcombobox.h>
#include <gtk/gtklist.h>
#include <gtk/gtkexpander.h>

void
gul_pu_set_config_from_editable (GtkWidget *editable, const char *config_name)
{
	GConfValue *gcvalue = eel_gconf_get_value (config_name);
	GConfValueType value_type;
	char *value;
	gint ivalue;
	gfloat fvalue;

	if (gcvalue == NULL) {
		/* ugly hack around what appears to be a gconf bug 
		 * it returns a NULL GConfValue for a valid string pref
		 * which is "" by default */
		value_type = GCONF_VALUE_STRING;
	} else {
		value_type = gcvalue->type;
		gconf_value_free (gcvalue);
	}

	/* get all the text into a new string */	
	value = gtk_editable_get_chars (GTK_EDITABLE(editable), 0, -1);

	switch (value_type) {
	case GCONF_VALUE_STRING:
		eel_gconf_set_string (config_name, 
				      value);
		break;
	/* FIXME : handle possible errors in the input for int and float */
	case GCONF_VALUE_INT:
		ivalue = atoi (value);
		eel_gconf_set_integer (config_name, ivalue);
		break;
	case GCONF_VALUE_FLOAT:
		fvalue = strtod (value, (char**)NULL);
		eel_gconf_set_float (config_name, fvalue);
		break;
	default:
		break;
	}

	/* free the allocated strings */
	g_free (value);
}

void
gul_pu_set_config_from_combobox (GtkWidget *combobox, const char *config_name)
{
	int index = gtk_combo_box_get_active(GTK_COMBO_BOX(combobox));
	
	eel_gconf_set_integer (config_name, index);
}

void
gul_pu_set_config_from_radiobuttongroup (GtkWidget *radiobutton, const char *config_name)
{
	gint index;

	/* get value from radio button group */
	index = gul_gui_gtk_radio_button_get (GTK_RADIO_BUTTON (radiobutton));

	eel_gconf_set_integer (config_name, index);
}

void
gul_pu_set_config_from_spin_button (GtkWidget *spinbutton, const char *config_name)
{
	gdouble value;
	gboolean use_int;

	/* read the value as an integer */
	value = gtk_spin_button_get_value (GTK_SPIN_BUTTON(spinbutton));

	use_int = (gtk_spin_button_get_digits (GTK_SPIN_BUTTON(spinbutton)) == 0);

	if (use_int)
	{
		eel_gconf_set_integer (config_name, value);
	}
	else
	{
		eel_gconf_set_float (config_name, value);
	}
}

void
gul_pu_set_config_from_togglebutton (GtkWidget *togglebutton, const char *config_name)
{
	gboolean value;

	/* read the value */
	value = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(togglebutton));

	eel_gconf_set_boolean (config_name, value);
}

void
gul_pu_set_config_from_expander (GtkWidget *expander, const char *config_name)
{
	gboolean value;

	/* read the value */
	value = gtk_expander_get_expanded (GTK_EXPANDER (expander));

	eel_gconf_set_boolean (config_name, value);
}

void
gul_pu_set_editable_from_config (GtkWidget *editable, const char *config_name)
{
	GConfValue *gcvalue = eel_gconf_get_value (config_name);
	GConfValueType value_type;
	gchar *value;

	if (gcvalue == NULL) {
		/* ugly hack around what appears to be a gconf bug 
		 * it returns a NULL GConfValue for a valid string pref
		 * which is "" by default */
		value_type = GCONF_VALUE_STRING;
	} else {	
		value_type = gcvalue->type;
		gconf_value_free (gcvalue);
	}
  	
	switch (value_type) {
	case GCONF_VALUE_STRING:
		value = eel_gconf_get_string (config_name);
		break;
	case GCONF_VALUE_INT:
		value = g_strdup_printf ("%d",eel_gconf_get_integer (config_name));
		break;
	case GCONF_VALUE_FLOAT:
		value = g_strdup_printf ("%.2f",eel_gconf_get_float (config_name));
		break;
	default:
		value = NULL;
	}
	
	/* set this string value in the widget */
	if (value)
	{
		gtk_entry_set_text(GTK_ENTRY(editable), value);
	}

	/* free the allocated string */
	g_free (value);
}

void
gul_pu_set_combobox_from_config (GtkWidget *combobox, const char *config_name)
{
	gint index;

	/* get the current value from the configuration space */
	index = eel_gconf_get_integer (config_name);

	/* set this option value in the widget */
	gtk_combo_box_set_active (GTK_COMBO_BOX (combobox), index);
}

void
gul_pu_set_radiobuttongroup_from_config (GtkWidget *radiobutton, const char *config_name)
{
	gint index;

        /* get the current value from the configuration space */
        index = eel_gconf_get_integer (config_name);

	/* set it (finds the group for us) */
	gul_gui_gtk_radio_button_set (GTK_RADIO_BUTTON (radiobutton), index);
}

void
gul_pu_set_spin_button_from_config (GtkWidget *spinbutton, const char *config_name)
{
	gdouble value;
	gint use_int;
	
	use_int = (gtk_spin_button_get_digits (GTK_SPIN_BUTTON(spinbutton)) == 0);
	
	if (use_int)
	{
		/* get the current value from the configuration space */
		value = eel_gconf_get_integer (config_name);
	}
	else
	{
		/* get the current value from the configuration space */
		value = eel_gconf_get_float (config_name);
	}

	/* set this option value in the widget */
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinbutton), value);
}

void
gul_pu_set_togglebutton_from_config (GtkWidget *togglebutton, const char *config_name)
{
	gboolean value;

	/* get the current value from the configuration space */
	value = eel_gconf_get_boolean (config_name);

	/* set this option value in the widget */
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (togglebutton), value);
}

void
gul_pu_set_expander_from_config (GtkWidget *expander, const char *config_name)
{
	gboolean value;

	/* get the current value from the configuration space */
	value = eel_gconf_get_boolean (config_name);

	/* set this option value value in the widget */
	gtk_expander_set_expanded (GTK_EXPANDER (expander), value);
}

static void
gul_pu_connect_config_spin_button_value_changed_cb (GtkSpinButton *spinbutton, const gchar *key)
{
	int value = gtk_spin_button_get_value_as_int (spinbutton);
	eel_gconf_set_integer (key, value);
}

static void
gul_pu_set_spin_button_from_config_notification_cb (GConfClient *client,
						    guint cnxn_id,
						    GConfEntry *entry,
						    gpointer user_data)
{
	GtkSpinButton *spinbutton = GTK_SPIN_BUTTON (user_data);
	GConfValue *value = gconf_entry_get_value (entry);
	int ivalue = gconf_value_get_int (value);
	gtk_spin_button_set_value (spinbutton, ivalue);
}

static void
gul_pu_connect_config_spin_button_destroy_cb (GtkWidget *widget, gpointer data)
{
	int id = GPOINTER_TO_INT (data);
	eel_gconf_notification_remove (id);
}

void
gul_pu_connect_config_spin_button (GtkWidget *spinbutton, 
				   const char *config_name)
{
	char *key = g_strdup (config_name);
	int id;
	g_return_if_fail (GTK_IS_SPIN_BUTTON (spinbutton));
	gul_pu_set_spin_button_from_config (spinbutton, key);

	id = eel_gconf_notification_add (key, gul_pu_set_spin_button_from_config_notification_cb,
					 spinbutton);
	g_object_set_data_full (G_OBJECT (spinbutton), "gul_pu_connect_config_spin_button_key", 
				key, g_free);
	g_signal_connect (spinbutton, "value-changed", 
			  G_CALLBACK (gul_pu_connect_config_spin_button_value_changed_cb), key);
	g_signal_connect (spinbutton, "destroy", 
			  G_CALLBACK (gul_pu_connect_config_spin_button_destroy_cb), GINT_TO_POINTER (id));
}

