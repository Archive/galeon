/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>

#include "galeon-shell.h"
#include "galeon-config.h"
#include "window-commands.h"
#include "bookmarks-add-dialog.h"
#include "bookmarks-editor.h"
#include "eel-gconf-extensions.h"
#include "prefs-strings.h"
#include "galeon-embed-utils.h"
#include "gul-general.h"
#include "page-info-dialog.h"
#include "pdm-dialog.h"
#include "galeon-window-bookmark-activated-proxy.h"
#include "galeon-prefs-dialog.h"
#include "egg-toolbar-editor.h"
#include "gul-state.h"
#include "gul-gui.h"
#include "gul-file-chooser.h"
#include "galeon-find-toolbar.h"
#include "galeon-simple-window.h"

#include <string.h>
#include <libgnomevfs/gnome-vfs-uri.h>
#include <libgnomevfs/gnome-vfs-utils.h>
#include <glib/gi18n.h>
#include <gtk/gtkversion.h>
#if GTK_CHECK_VERSION(2,5,0)
# include <gtk/gtkaboutdialog.h>
# define USE_GTKABOUTDIALOG
#else
# include <libgnomeui/gnome-about.h>
#endif
#include <gtk/gtkeditable.h>
#include <gtk/gtkmain.h>
#include <gtk/gtkaction.h>
#include <gtk/gtkstock.h>

#define VIEW_IMAGES_BLOCKSITE_ACTION	"ViewImagesBlockSiteAction"
#define VIEW_IMAGES_ALLOWSITE_ACTION	"ViewImagesAllowSiteAction"
#define WEB_COOKIES_BLOCKSITE_ACTION	"WebCookiesBlockSiteAction"
#define WEB_COOKIES_ALLOWSITE_ACTION	"WebCookiesAllowSiteAction"
#define WEB_POPUPS_ALLOWSITE_ACTION	"WebPopupsAllowSiteAction"

enum
{
	RESPONSE_ADD_TOOLBAR
};

void 
window_cmd_file_print (GtkAction *action, 
		       GaleonWindow *window) 
{
	GaleonDialog *dialog;
	GaleonEmbed *embed;
	
	embed = galeon_window_get_active_embed (window);
	g_return_if_fail (embed != NULL);

	galeon_embed_print (embed);	
}

void 
window_cmd_file_send_to	(GtkAction *action, 
		      	 GaleonWindow *window) 
{
	char *url;
	GaleonTab *tab;
	GaleonEmbed *embed;
	char *location;
	char *title;

	tab = galeon_window_get_active_tab (window);
	g_return_if_fail (tab);
	
	embed = galeon_window_get_active_embed (window);
	g_return_if_fail (tab);
	
	location = gnome_vfs_escape_string (galeon_tab_get_location (tab));
	title = galeon_embed_get_title (embed);

	if (title)
	{
		char *tmp = gnome_vfs_escape_string (title);
		g_free (title);
		title = tmp;
	}
	else
	{
		title = gnome_vfs_escape_string (_("Check this out!"));
	}

	url = g_strconcat ("mailto:",
                           "?Subject=", title,
                           "&Body=", location, NULL);

	galeon_embed_load_url (embed, url);

	g_free (title);
	g_free (location);
	g_free (url);
}

void
window_cmd_go_myportal (GtkAction *action, 
		        GaleonWindow *window) 
{
	GaleonEmbed *embed;
	
	embed = galeon_window_get_active_embed (window);
	g_return_if_fail (embed != NULL);

	galeon_embed_load_url (embed, "myportal:");
}

void
window_cmd_go_location (GtkAction *action, 
		        GaleonWindow *window) 
{
	galeon_window_edit_location (window);
}

void
window_cmd_go_stop (GtkAction *action, 
		    GaleonWindow *window) 
{
	GaleonEmbed *embed;

	embed = galeon_window_get_active_embed (window);
	g_return_if_fail (embed != NULL);

	galeon_embed_stop_load (embed);
}

void
window_cmd_go_reload (GtkAction *action, 
		      GaleonWindow *window) 
{
	GaleonEmbed *embed;
	GdkEvent *event;
	EmbedReloadFlags reload_type = EMBED_RELOAD_NORMAL;

	embed = galeon_window_get_active_embed (window);
	g_return_if_fail (embed != NULL);

	event = gtk_get_current_event ();
	if (event)
	{
		if (event->type == GDK_BUTTON_RELEASE &&
		    event->button.state & GDK_SHIFT_MASK)
		{
			reload_type = EMBED_RELOAD_FORCE;
		}
		gdk_event_free (event);
	}

	galeon_embed_reload (embed, reload_type);
}

static void
open_new_tab (GaleonWindow *window, GaleonNewTabFlags flags)
{
	GaleonTab *tab;

	flags |= GALEON_NEW_TAB_HOMEPAGE | GALEON_NEW_TAB_JUMP;

	tab = galeon_window_get_active_tab (window);
	
	galeon_shell_new_tab (galeon_shell, window, tab, NULL, flags);
}

void
window_cmd_new (GtkAction *action, 
		GaleonWindow *window) 
{
	open_new_tab (window, GALEON_NEW_TAB_APPEND);
}

void
window_cmd_new_window (GtkAction *action, 
		       GaleonWindow *window) 
{
	open_new_tab (window, GALEON_NEW_TAB_IN_NEW_WINDOW);
}

void
window_cmd_new_tab (GtkAction *action, 
		    GaleonWindow *window) 
{
	open_new_tab (window, GALEON_NEW_TAB_IN_EXISTING_WINDOW |
			      GALEON_NEW_TAB_APPEND);
}

void
window_cmd_bookmarks_edit (GtkAction *action, 
			   GaleonWindow *window) 
{
	GbBookmarkSet *set;
	GbEditor *e;
	GaleonWindowBookmarkActivatedProxy *wbap;

	set = galeon_shell_get_bookmark_set (galeon_shell);
	if (!set) return;

	e = gb_editor_new_for_set (set);
	gtk_widget_show (gb_editor_get_main_widget (e));

	wbap = galeon_window_bookmark_activated_proxy_new ();
	galeon_window_bookmark_activated_proxy_set_window (wbap, window);
	galeon_window_bookmark_activated_proxy_connect (wbap, G_OBJECT (e));
}

void window_cmd_bookmarks_add_interactive(GtkAction *action, 
			    		  GaleonWindow *window)
{
	GaleonTab *tab;
	const char *title;
	const char *location;
	GaleonDialog *dialog;
	GbBookmarkSet *set;
	GbSite *b;
	GSList *list = NULL;

	tab = galeon_window_get_active_tab (window);
	g_return_if_fail (tab);

	set = galeon_shell_get_bookmark_set (galeon_shell);
	g_return_if_fail (set);

	title = galeon_tab_get_title (tab);
	location = galeon_tab_get_location (tab);

	b = gb_site_new(set, title, location);

	list = g_slist_append(list, b);

	dialog = gb_add_dialog_new(GTK_WIDGET(window), set, list);

	galeon_dialog_show(dialog);
}

void window_cmd_bookmarks_add_tab_folder(GtkAction *action, 
					 GaleonWindow *window)
{
	GaleonDialog *dialog;
	GbBookmarkSet *set;
	GList *tabs, *i;
	GSList *list = NULL;

	tabs = galeon_window_get_tabs(window);
	g_return_if_fail(tabs);

	set = galeon_shell_get_bookmark_set(galeon_shell);
	g_return_if_fail(set);

	for (i = tabs ; i ; i = i->next)
	{
		GaleonTab *tab = GALEON_TAB(i->data);
		const gchar *title = galeon_tab_get_title(tab);
		const gchar *location = galeon_tab_get_location(tab);
		GbSite *b = gb_site_new(set, title, location);

		list = g_slist_append(list, b);
	}

	dialog = gb_add_dialog_new(GTK_WIDGET(window), set, list);

	galeon_dialog_show(dialog);
}

static void
open_response_cb (GtkDialog *dialog, int response, GaleonWindow *window)
{
	if (response == GTK_RESPONSE_ACCEPT)
	{
		char *uri, *converted;

		uri = gtk_file_chooser_get_uri (GTK_FILE_CHOOSER (dialog));
		if (uri != NULL)
		{
			converted = g_filename_to_utf8 (uri, -1, NULL, NULL, NULL);
	
			if (converted != NULL)
			{
				galeon_window_load_url (window, converted);
			}
	
			g_free (converted);
			g_free (uri);
	        }
	}

	gtk_widget_destroy (GTK_WIDGET (dialog));
}

void
window_cmd_file_open (GtkAction *action,
		      GaleonWindow *window)
{
	GulFileChooser *dialog;

	dialog = gul_file_chooser_new (_("Open"),
				       GTK_WIDGET (window),
				       GTK_FILE_CHOOSER_ACTION_OPEN,
				       CONF_STATE_OPEN_DIR);

	g_signal_connect (dialog, "response",
			  G_CALLBACK (open_response_cb), window);

	gtk_widget_show (GTK_WIDGET (dialog));
}


void
window_cmd_file_save_as (GtkAction *action, 
		      	 GaleonWindow *window) 
{
	GaleonEmbed *embed;
	GaleonEmbedPersist *persist;
	
	embed = galeon_window_get_active_embed (window);
	g_return_if_fail (embed != NULL);

	persist = galeon_embed_persist_new (embed);
	galeon_embed_persist_set_flags (persist,
					EMBED_PERSIST_MAINDOC | 
					EMBED_PERSIST_ASK_DESTINATION |
					EMBED_PERSIST_SAVE_CONTENT |
					EMBED_PERSIST_ADD_TO_RECENT);
	galeon_embed_persist_set_fc_parent (persist, GTK_WIDGET (window));
	galeon_embed_persist_set_fc_title (persist, _("Save As..."));
	galeon_embed_persist_set_user_time (persist, gtk_get_current_event_time());
	galeon_embed_persist_save (persist);

	g_object_unref (G_OBJECT(persist));
}

void window_cmd_file_quit(GtkAction *action,
			  GaleonWindow *window)
{
	Session *session =
		galeon_shell_get_session(galeon_shell);

	GList *windows = session_get_windows (session);
	GList *l;
	for (l = windows ; l ; l = l->next)
	{
		if (!galeon_window_can_close (GALEON_WINDOW (l->data)))
		{
			return;
		}
	}
	/* Don't free the windows list, it is directly from the Session */

	session_close(session);
}

void
window_cmd_file_close_tab (GtkAction *action, 
		           GaleonWindow *window) 
{
	GaleonTab *tab;
	
	tab = galeon_window_get_active_tab (window);
	g_return_if_fail (tab != NULL);
	galeon_window_remove_tab (window, tab);
}

void 
window_cmd_edit_cut (GtkAction *action, 
		     GaleonWindow *window) 
{
	GtkWidget *widget = gtk_window_get_focus (GTK_WINDOW (window));

	if (GTK_IS_EDITABLE (widget))
	{
		gtk_editable_cut_clipboard (GTK_EDITABLE (widget));
	}
	else
	{
		GaleonEmbed *embed;
		embed = galeon_window_get_active_embed (window);
		g_return_if_fail (embed != NULL);
		
		galeon_embed_selection_cut (embed);
	}
}

void
window_cmd_edit_copy (GtkAction *action, 
		      GaleonWindow *window) 
{
	GtkWidget *widget = gtk_window_get_focus (GTK_WINDOW (window));

	if (GTK_IS_EDITABLE (widget))
	{
		gtk_editable_copy_clipboard (GTK_EDITABLE (widget));
	}
	else
	{
		GaleonEmbed *embed;

		embed = galeon_window_get_active_embed (window);
		g_return_if_fail (embed != NULL);
		
		galeon_embed_selection_copy (embed);
	}
}

void
window_cmd_edit_paste (GtkAction *action, 
		       GaleonWindow *window) 
{
	GtkWidget *widget = gtk_window_get_focus (GTK_WINDOW (window));

	if (GTK_IS_EDITABLE (widget))
	{
		gtk_editable_paste_clipboard (GTK_EDITABLE (widget));
	}
	else
	{
		GaleonEmbed *embed;

		embed = galeon_window_get_active_embed (window);
		g_return_if_fail (embed != NULL);
		
		galeon_embed_paste (embed);
	}
}

void 
window_cmd_edit_select_all (GtkAction *action, 
		      	    GaleonWindow *window) 
{
	GtkWidget *widget = gtk_window_get_focus (GTK_WINDOW (window));

	if (GTK_IS_EDITABLE (widget))
	{
		gtk_editable_select_region (GTK_EDITABLE (widget), 0, -1);
	}
	else
	{
		GaleonEmbed *embed;

		embed = galeon_window_get_active_embed (window);
		g_return_if_fail (embed != NULL);
		
		galeon_embed_select_all (embed);
	}
}

void
window_cmd_edit_find (GtkAction *action, 
		      GaleonWindow *window) 
{
	GaleonFindToolbar *toolbar;

	toolbar = GALEON_FIND_TOOLBAR (galeon_window_get_find_toolbar (window));
	galeon_find_toolbar_open (toolbar, FALSE, FALSE);
}


void
window_cmd_edit_find_next (GtkAction *action, 
			   GaleonWindow *window) 
{
	GaleonFindToolbar *toolbar;

	toolbar = GALEON_FIND_TOOLBAR (galeon_window_get_find_toolbar (window));
	galeon_find_toolbar_find_next (toolbar);
}

void
window_cmd_edit_find_prev (GtkAction *action, 
			   GaleonWindow *window) 
{
	GaleonFindToolbar *toolbar;

	toolbar = GALEON_FIND_TOOLBAR (galeon_window_get_find_toolbar (window));
	galeon_find_toolbar_find_previous (toolbar);
}

void 
window_cmd_set_permission(GtkAction *action, 
		      	  GaleonWindow *window) 
{

	gchar *url;
	const char *verbname;
	PermissionType type;
	gboolean value;

	GaleonEmbed *embed;
	GaleonEmbedShell *embed_shell;

	embed_shell = galeon_shell_get_embed_shell(galeon_shell);
	g_return_if_fail(embed_shell != NULL);
	
	embed = galeon_window_get_active_embed(window);
	url = galeon_embed_get_location(embed, TRUE, FALSE);
	if (!url) return;

	verbname = gtk_action_get_name (action);

	if (!strcmp(verbname, WEB_COOKIES_ALLOWSITE_ACTION))
	{
		type = COOKIES_PERMISSION;
		value = TRUE;
	}
	else if (!strcmp(verbname, WEB_COOKIES_BLOCKSITE_ACTION))
	{
		type = COOKIES_PERMISSION;
		value = FALSE;
	}
	else if (!strcmp(verbname, VIEW_IMAGES_ALLOWSITE_ACTION))
	{
		type = IMAGES_PERMISSION;
		value = TRUE;
	}
	else if (!strcmp(verbname, VIEW_IMAGES_BLOCKSITE_ACTION))
	{
		type = IMAGES_PERMISSION;
		value = FALSE;
	}
	else /* if (!strcmp(verbname, WEB_POPUPS_ALLOWSITE_ACTION)) */
	{
		type = POPUPS_PERMISSION;
		value = TRUE;
	}
	galeon_embed_shell_set_permission(embed_shell, url, type, value);
}

void
window_cmd_show_java_console(GtkAction *action,
			     GaleonWindow *window)
{
	GaleonEmbedShell *embed_shell;

	embed_shell = galeon_shell_get_embed_shell(galeon_shell);
	g_return_if_fail(embed_shell != NULL);

	galeon_embed_shell_show_java_console(embed_shell);
}

void window_cmd_show_js_console(GtkAction *action,
				GaleonWindow *window)
{
	galeon_window_show_js_console (window);
}

void 
window_cmd_view_zoom_in	(GtkAction *action, 
		      	 GaleonWindow *window) 
{
	GaleonEmbed *embed;
	int zoom;
	
	embed = galeon_window_get_active_embed (window);
	g_return_if_fail (embed != NULL);

	zoom = galeon_embed_zoom_get (embed);
	galeon_window_set_zoom (window, zoom + 10);
}

void 
window_cmd_view_zoom_out (GtkAction *action, 
		      	  GaleonWindow *window) 
{
	GaleonEmbed *embed;
	int zoom;
	
	embed = galeon_window_get_active_embed (window);
	g_return_if_fail (embed != NULL);

	zoom = galeon_embed_zoom_get (embed);
	if (zoom >= 10)
	{
		galeon_window_set_zoom (window, zoom - 10);
	}
}

void 
window_cmd_view_zoom_normal (GtkAction *action, 
		      	     GaleonWindow *window) 
{
	galeon_window_set_zoom (window, 100);
}

void 
window_cmd_view_page_source (GtkAction *action, 
		      	     GaleonWindow *window) 
{
	GaleonTab *tab;
	
	tab = galeon_window_get_active_tab (window);
	g_return_if_fail (tab != NULL);

	galeon_shell_new_tab (galeon_shell, window, tab, NULL, 
			      GALEON_NEW_TAB_VIEW_SOURCE |
			      GALEON_NEW_TAB_COPY_HISTORY |
			      GALEON_NEW_TAB_JUMP);
}

void 
window_cmd_view_page_info(GtkAction *action, 
		      	  GaleonWindow *window)
{
	galeon_window_show_page_info (window, PAGE_INFO_GENERAL);
}

void 
window_cmd_tools_history (GtkAction *action, 
		      	  GaleonWindow *window) 
{
	galeon_window_show_history (window);
}

static void
open_pdm_dialog (GtkWidget *window, int page_num)
{
	static GaleonDialog *dialog = NULL;

	/* FIXME: we want have only one PDM dialog shown, but since it's a
	 * GaleonDialog we can't re-use set_transient_for with it like we can
	 * with sane dialogs.  So we'll just destroy and recreate the dialog
	 * instead..
	 */

	if (dialog != NULL)
	{
		galeon_dialog_destruct (dialog);
	}

	dialog = pdm_dialog_new (window);
	g_object_add_weak_pointer (G_OBJECT(dialog), (gpointer)&dialog);

	if (page_num != -1)
	{
		pdm_dialog_set_current_page (PDM_DIALOG(dialog), page_num);
	}
	galeon_dialog_show (dialog);
}

void 
window_cmd_tools_pdm (GtkAction *action, 
		      GaleonWindow *window) 
{
	open_pdm_dialog (GTK_WIDGET(window), -1);
}

static void
on_prefs_dialog_request (GaleonPrefsDialog  *dialog,
		         GaleonPrefsRequest  request,
			 GaleonWindow       *window)
{
	switch (request)
	{
	case GALEON_PREFS_CLEAR_CACHE:
		galeon_embed_shell_clear_cache (embed_shell, MEMORY_CACHE);
		galeon_embed_shell_clear_cache (embed_shell, DISK_CACHE);
		break;
	case GALEON_PREFS_MANAGE_CERTIFICATES:
	{
		GaleonSimpleWindow *new_win;

		new_win = galeon_simple_window_new ();
		gtk_window_resize (GTK_WINDOW(new_win), 550, 400);

		galeon_simple_window_load_url (new_win, "chrome://pippki/content/certManager.xul");

		gtk_window_set_title (GTK_WINDOW(new_win), _("Certificate Manager"));
		gtk_window_set_role (GTK_WINDOW(new_win), "galeon-certificate-manager");
		gtk_widget_show (GTK_WIDGET(new_win));

		break;
	}
	case GALEON_PREFS_MANAGE_DEVICES:
	{
		GaleonSimpleWindow *new_win;

		new_win = galeon_simple_window_new ();
		gtk_window_resize (GTK_WINDOW(new_win), 720, 250);

		galeon_simple_window_load_url (new_win, "chrome://pippki/content/device_manager.xul");
		
		gtk_window_set_title (GTK_WINDOW(new_win), _("Device Manager"));
		gtk_window_set_role (GTK_WINDOW(new_win), "galeon-device-manager");
		gtk_widget_show (GTK_WIDGET(new_win));

		break;
	}
	case GALEON_PREFS_MANAGE_COOKIES:
		open_pdm_dialog (GTK_WIDGET(dialog), PDM_PAGE_COOKIES);
		break;
	case GALEON_PREFS_MANAGE_IMAGES:
		open_pdm_dialog (GTK_WIDGET(dialog), PDM_PAGE_IMAGE_SITES);
		break;
	case GALEON_PREFS_MANAGE_PASSWORDS:
		open_pdm_dialog (GTK_WIDGET(dialog), PDM_PAGE_PASSWORDS);
		break;
	case GALEON_PREFS_MANAGE_POPUPS:
		open_pdm_dialog (GTK_WIDGET(dialog), PDM_PAGE_POPUP_SITES);
		break;
	default:
		g_warning ("%s: unhandled request `%d'", G_STRLOC, request);
	}
}

static char *
on_prefs_dialog_location (GaleonPrefsDialog *dialog,
		          GaleonWindow      *window)
{
	GaleonTab *tab;

	tab = galeon_window_get_active_tab (window);
	return g_strdup (galeon_tab_get_location (tab));
}

void 
window_cmd_edit_prefs (GtkAction *action, 
		       GaleonWindow *window) 
{
	static GtkWidget *dialog = NULL;

	/* We want to have only one prefs dialog shown, so reparent the one we
	 * have. */

	if (dialog == NULL)
	{
		dialog = galeon_prefs_dialog_new ();
		g_object_add_weak_pointer (G_OBJECT (dialog),
				           (gpointer)&dialog);
	}
	else
	{
		g_signal_handlers_disconnect_matched
			(G_OBJECT(dialog), G_SIGNAL_MATCH_FUNC, 0, 0, NULL, 
			 G_CALLBACK(on_prefs_dialog_request), NULL);
		g_signal_handlers_disconnect_matched
			(G_OBJECT(dialog), G_SIGNAL_MATCH_FUNC, 0, 0, NULL, 
			 G_CALLBACK(on_prefs_dialog_location), NULL);
	}

	g_signal_connect (G_OBJECT(dialog), "request",
			  G_CALLBACK(on_prefs_dialog_request),
			  window);
	g_signal_connect (G_OBJECT(dialog), "location",
			  G_CALLBACK(on_prefs_dialog_location),
			  window);

	gtk_window_set_transient_for (GTK_WINDOW (dialog),
				      GTK_WINDOW (window));
	gtk_window_present (GTK_WINDOW(dialog));
}

static void
toolbar_editor_destroy_cb (GtkWidget *tbe,
			   EggEditableToolbar *t)
{
	egg_editable_toolbar_set_edit_mode (t, FALSE);
}

static void
toolbar_editor_response_cb (GtkDialog  *dialog, gint response_id, gpointer data)
{
	EggToolbarsModel *model;
	int n;

	switch (response_id)
	{
	case GTK_RESPONSE_CLOSE:
		gtk_widget_destroy (GTK_WIDGET (dialog));
		break;
	case RESPONSE_ADD_TOOLBAR:
		model = galeon_shell_get_toolbars_model (galeon_shell);
		n = egg_toolbars_model_n_toolbars (EGG_TOOLBARS_MODEL (model));
		egg_toolbars_model_add_toolbar (model, n - 1, "UserCreated");
		break;
	case GTK_RESPONSE_HELP:
		gul_gui_help (GTK_WINDOW (dialog), "galeon.xml", "toolbar-editor");
		break;
	}
}

void 
window_cmd_settings_toolbar_editor (GtkAction *action, 
				    GaleonWindow *window) 
{
	GtkWidget *editor;
	EggToolbarsModel *model;
	GtkWidget *dialog;
	gchar *actions_file = gul_general_user_file ("galeon-egg-ui-toolbar.xml", TRUE);
	EggEditableToolbar *toolbar = galeon_window_get_toolbar (window);

	model = galeon_shell_get_toolbars_model (galeon_shell);

	dialog = gtk_dialog_new ();

	gul_state_monitor_window (dialog, "Toolbar Editor", 550, 350);

	gtk_dialog_set_has_separator (GTK_DIALOG (dialog), FALSE);
	gtk_window_set_title (GTK_WINDOW (dialog), _("Toolbar Editor"));
        gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (window));

	editor = egg_toolbar_editor_new (GTK_UI_MANAGER (window->merge), EGG_TOOLBARS_MODEL (model));
	egg_toolbar_editor_load_actions (EGG_TOOLBAR_EDITOR (editor), actions_file);
	g_free (actions_file);

	gtk_container_set_border_width (GTK_CONTAINER (editor), 5);
	gtk_box_set_spacing (GTK_BOX (editor), 5);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog)->vbox), editor);
	g_signal_connect (editor, "destroy",
			  G_CALLBACK (toolbar_editor_destroy_cb), toolbar);
	gtk_widget_show (editor);
	
	gtk_box_set_spacing (GTK_BOX (GTK_DIALOG (dialog)->vbox), 2);

	gtk_container_set_border_width (GTK_CONTAINER (GTK_DIALOG (dialog)), 5);
	
	gtk_dialog_add_button (GTK_DIALOG (dialog),
			       _("_Add a New Toolbar"), RESPONSE_ADD_TOOLBAR);
	gtk_dialog_add_button (GTK_DIALOG (dialog),
			       GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE);
	gtk_dialog_add_button (GTK_DIALOG (dialog),
			       GTK_STOCK_HELP, GTK_RESPONSE_HELP);
	
	g_signal_connect (G_OBJECT (dialog), "response",
			  G_CALLBACK (toolbar_editor_response_cb), NULL);

	gtk_widget_show (dialog);

	egg_editable_toolbar_set_edit_mode (toolbar, TRUE);
}

void
window_cmd_help_about (GtkAction *action, 
		       GaleonWindow *window) 
{
	static GtkWidget *about = NULL;
	GdkPixbuf  	 *pixbuf;
	GError     	 *error = NULL;
	gchar      	 *file;

	const gchar *authors[] = {
		"Philip Langdale <philipl@mail.utexas.edu>",
		"Tommi Komulainen <tommi.komulainen@iki.fi>",
		"Ricardo Fern\xc3\xa1ndez Pascual <ric@users.sourceforge.net>",
		/* this one is supposed to be in cyrillic */
		_("Yanko Kaneti <yaneti@declera.com>"),
		"Crispin Flowerday <gnome@flowerday.cx>",
		/* don't delete the space */
		" ",
		_("Past developers:"),
		"Marco Pesenti Gritti <mpeseng@tin.it>",
		"Remco de Jong <rdj@rdj.cg.nu>",
		"Nate Case <nd@kracked.com>",
		"Bob Phan <ex-freek@yahoo.com>",
		"Matt Aubury <matt@ookypooky.com>",
		"Joshua Horvath <jmhorvat@home.com>",
		"Matthew Mueller <donut@azstarnet.com>",
		"Daniel Erat <dan_erat@pobox.com>",
		"Jorn Baayen <jorn@nl.linux.org>",
		"Christophe Fergeau <teuf@users.sourceforge.net>",
		/* don't delete the space */
		" ",
		_("Graphic arts:"),
		"Toussaint Fr\xc3\xa9" "d\xc3\xa9ric <fred@linuxgraphic.org>",
		NULL
	};

	gchar *documenters[] = {
		NULL
	};

	/* Translator credits */
	gchar *translator_credits = _("translator-credits");

	if (about != NULL)
	{
		gdk_window_show(about->window);
		gdk_window_raise(about->window);
		return;
	}

	file = gul_general_user_file("about_logo.png", FALSE);
	pixbuf = gdk_pixbuf_new_from_file (file, &error);
	
	if (error) {
		g_warning (G_STRLOC ": cannot open %s: %s", file, error->message);
		g_error_free (error);
	}
	g_free (file);
	
#ifdef USE_GTKABOUTDIALOG
	about = g_object_new (GTK_TYPE_ABOUT_DIALOG,
			      "name", _("Galeon"),
			      "version", VERSION,
			      "copyright", "Copyright \xc2\xa9 2003-2006 The Galeon Developers\nCopyright \xc2\xa9 2000-2002 Marco Pesenti Gritti",
			      "authors", authors,
			      "documenters", *documenters ? documenters : NULL,
			      "comments", _("A GNOME browser based on Mozilla"),
			      "translator-credits", translator_credits,
			      "logo", pixbuf,
			      NULL);

	g_signal_connect (about, "response", G_CALLBACK (gtk_widget_destroy), NULL);

#else
	about = gnome_about_new(
		       _("Galeon"), VERSION,
		       "Copyright \xc2\xa9 2003-2006 The Galeon Developers\nCopyright \xc2\xa9 2000-2002 Marco Pesenti Gritti",
		       _("A GNOME browser based on Mozilla"),
		       (const char **)authors,
		       (const char **)documenters,
		       strcmp (translator_credits, "translator-credits") != 0 ? translator_credits : NULL,
		       pixbuf);
#endif
	if (pixbuf) {
		g_object_unref (G_OBJECT(pixbuf));
	}			
				
	gtk_window_set_transient_for (GTK_WINDOW (about),
				      GTK_WINDOW (window));
	gtk_window_set_role (GTK_WINDOW (about), "galeon-about");
	g_object_add_weak_pointer (G_OBJECT (about), (gpointer)&about);
	gtk_widget_show (about);
}

void 
window_cmd_tabs_next (GtkAction *action, 
		      GaleonWindow *window) 
{
	galeon_window_next_tab (window);
}

void 
window_cmd_tabs_previous (GtkAction *action, 
			  GaleonWindow *window) 
{
	galeon_window_prev_tab (window);
}

void 
window_cmd_tabs_move_left  (GtkAction *action, 
			    GaleonWindow *window) 
{
	GList *tabs;
	GaleonTab *tab;

	tab = galeon_window_get_active_tab (window);
	tabs = galeon_window_get_tabs (window);
	g_return_if_fail (tab != NULL);
	
	tabs = g_list_find (tabs, (gpointer)tab);
	tabs = tabs->prev;

	if (tabs)
	{
		GaleonTab *sibling;
		sibling = GALEON_TAB (tabs->data);
		galeon_window_move_tab (window, tab, sibling);
		g_list_free (tabs);
	}
}

void window_cmd_tabs_move_right (GtkAction *action, 
				 GaleonWindow *window) 
{
	GList *tabs;
	GaleonTab *tab;

	tab = galeon_window_get_active_tab (window);
	tabs = galeon_window_get_tabs (window);
	g_return_if_fail (tab != NULL);
	
	tabs = g_list_find (tabs, (gpointer)tab);
	tabs = tabs->next;

	if (tabs)
	{
		GaleonTab *sibling;
		sibling = GALEON_TAB (tabs->data);
		galeon_window_move_tab (window, tab, sibling);
		g_list_free (tabs);
	}
}

void
window_cmd_tabs_clone(GtkAction *action,
		      GaleonWindow *window)
{
        GaleonTab *tab;

        tab = galeon_window_get_active_tab(window);
        g_return_if_fail(tab != NULL);

	galeon_shell_new_tab(galeon_shell, window, tab, NULL,
			     GALEON_NEW_TAB_IN_EXISTING_WINDOW |
			     GALEON_NEW_TAB_JUMP |
			     GALEON_NEW_TAB_COPY_HISTORY |
			     GALEON_NEW_TAB_IS_A_COPY);
}

void 
window_cmd_tabs_detach (GtkAction *action, 
			GaleonWindow *window) 
{
	GaleonWindow *new_win;
	GaleonTab *tab;

	if (g_list_length (galeon_window_get_tabs (window)) <= 1) {
		return;
	}

	tab = galeon_window_get_active_tab (window);	
	new_win = galeon_window_new ();

	galeon_window_reparent_tab (new_win, window, tab);

	gtk_widget_show (GTK_WIDGET (new_win));
}

void window_cmd_tabs_close_other_tabs(GtkAction *action,
				      GaleonWindow *window)
{
	GaleonTab *active_tab;
	GList *i, *tabs = galeon_window_get_tabs(window);

	if (g_list_length(tabs) <= 1)
	{
		return;
	}

	active_tab = galeon_window_get_active_tab(window);
	g_return_if_fail(active_tab);

	for (i = tabs ; i ; i = i->next)
	{
		GaleonTab *tab = GALEON_TAB(i->data);

		if (tab != active_tab)
			galeon_window_remove_tab(window, tab);
	}
}

void 
window_cmd_help_manual (GtkAction *action, 
			GaleonWindow *window) 
{
	gul_gui_help (GTK_WINDOW (window), "galeon.xml", NULL);
}

void
window_cmd_help_plugins (GtkAction *action, 
			GaleonWindow *window) 
{
	galeon_shell_new_tab(galeon_shell, window, NULL, "about:plugins",
			     GALEON_NEW_TAB_JUMP);
}
