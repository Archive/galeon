/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n.h>
#include "gul-glade.h"
#include "bookmarks-single-editor-small.h"
#include "gul-string.h"
#include "eel-gconf-extensions.h"
#include "prefs-strings.h"
#include <gtk/gtkspinbutton.h>
#include <gtk/gtkbutton.h>
#include <libgnomeui/gnome-pixmap-entry.h>

/**
 * Private data
 */
#define GB_SINGLE_EDITOR_SMALL_GET_PRIVATE(object) (G_TYPE_INSTANCE_GET_PRIVATE ((object), \
				       GB_TYPE_SINGLE_EDITOR_SMALL, GbSingleEditorSmallPrivate))


struct _GbSingleEditorSmallPrivate {
	GtkWidget *pane;
};

/**
 * Private functions, only availble from this file
 */
static void		gb_single_editor_small_init_widgets_impl	(GbSingleEditor *e);
static void		gb_single_editor_small_finalize_impl		(GObject *o);
static void		gb_single_editor_small_more_properties_clicked_cb (GtkButton *b, GbSingleEditorSmall *e);



/**
 * SingleEditorSmall object
 */

G_DEFINE_TYPE (GbSingleEditorSmall, gb_single_editor_small, GB_TYPE_SINGLE_EDITOR);

static void
gb_single_editor_small_class_init (GbSingleEditorSmallClass *klass)
{
	G_OBJECT_CLASS (klass)->finalize = gb_single_editor_small_finalize_impl;
	GB_SINGLE_EDITOR_CLASS (klass)->init_widgets = gb_single_editor_small_init_widgets_impl;


	g_type_class_add_private (klass, sizeof (GbSingleEditorSmallPrivate));
}

static void 
gb_single_editor_small_init (GbSingleEditorSmall *e)
{
	GbSingleEditorSmallPrivate *p = GB_SINGLE_EDITOR_SMALL_GET_PRIVATE (e);
	e->priv = p;
	
	gb_single_editor_set_disable_policy (GB_SINGLE_EDITOR (e), GB_SINGLE_EDITOR_DISABLE_POLICY_DISABLE);
}

static void
gb_single_editor_small_finalize_impl (GObject *o)
{
	GbSingleEditorSmallPrivate *p = GB_SINGLE_EDITOR_SMALL (o)->priv;

	if (p->pane) g_object_unref (p->pane);
	/* the rest of the widgets will be unreffed by the parent class */

	G_OBJECT_CLASS (gb_single_editor_small_parent_class)->finalize (o);
}

static void
gb_single_editor_small_init_widgets_impl (GbSingleEditor *e)
{
	GbSingleEditorSmallPrivate *p = GB_SINGLE_EDITOR_SMALL (e)->priv;
	WidgetLookup lookups[] = {
		{ "bookmarks_properties_pane", &p->pane },
		{ "bookmarks_properties_more_properties", &e->more_properties_button },
		{ "bookmarks_properties_name_label", &e->name_label },
		{ "bookmarks_properties_name_entry", &e->name_entry },
		{ "bookmarks_properties_smarturl_label", &e->smarturl_label },
		{ "bookmarks_properties_smarturl_entry", &e->smarturl_entry },
		{ "bookmarks_properties_location_label", &e->location_label },
		{ "bookmarks_properties_location_entry", &e->location_entry },
		{ "bookmarks_properties_notes_label", &e->notes_label },
		{ "bookmarks_properties_notes_textview", &e->notes_textview },
		{ "bookmarks_properties_nicks_label", &e->nicks_label },
		{ "bookmarks_properties_nicks_entry", &e->nicks_entry },
		{ NULL, NULL }
	};
	GladeXML *gxml = gul_glade_widget_new ("bookmarks-editor-small.glade", "bookmarks_properties_pane",
					       NULL, e);
	gul_glade_lookup_widgets (gxml, lookups);
	g_object_unref (gxml);

	g_signal_connect (e->more_properties_button, "clicked",
			  G_CALLBACK (gb_single_editor_small_more_properties_clicked_cb), e);

	if (p->pane) g_object_ref (p->pane);
	if (e->more_properties_button) g_object_ref (e->more_properties_button);
	if (e->name_label) g_object_ref (e->name_label);
	if (e->name_entry) g_object_ref (e->name_entry);
	if (e->smarturl_label) g_object_ref (e->smarturl_label);
	if (e->smarturl_entry) g_object_ref (e->smarturl_entry);
	if (e->location_label) g_object_ref (e->location_label);
	if (e->location_entry) g_object_ref (e->location_entry);
	if (e->notes_label) g_object_ref (e->notes_label);
	if (e->notes_textview) g_object_ref (e->notes_textview);
	if (e->nicks_label) g_object_ref (e->nicks_label);
	if (e->nicks_entry) g_object_ref (e->nicks_entry);
}

GbSingleEditorSmall *
gb_single_editor_small_new (void)
{
	GbSingleEditorSmall *ret = g_object_new (GB_TYPE_SINGLE_EDITOR_SMALL, NULL);
	return ret;
}

GtkWidget *
gb_single_editor_small_get_widget (GbSingleEditorSmall *e)
{
	gb_single_editor_init_widgets_and_signals (GB_SINGLE_EDITOR (e));
	return e->priv->pane;
}

static void
gb_single_editor_small_more_properties_clicked_cb (GtkButton *b, GbSingleEditorSmall *e)
{
	GbBookmark *bm = gb_single_editor_get_bookmark (GB_SINGLE_EDITOR (e));
	if (bm)
	{
		GbSingleEditor *se = gb_single_editor_new ();
		gb_single_editor_set_bookmark (se, bm);
		gb_single_editor_show (se);
	}
}
