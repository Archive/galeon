/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GALEON_TAB_H
#define GALEON_TAB_H

#include "galeon-embed.h"
#include <gtk/gtkbin.h>

G_BEGIN_DECLS

typedef struct GaleonTabClass GaleonTabClass;

#define GALEON_TYPE_TAB             (galeon_tab_get_type ())
#define GALEON_TAB(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GALEON_TYPE_TAB, GaleonTab))
#define GALEON_TAB_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GALEON_TAB, GaleonTabClass))
#define GALEON_IS_TAB(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GALEON_TYPE_TAB))
#define GALEON_IS_TAB_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GALEON_TAB))

typedef struct GaleonTab GaleonTab;
typedef struct GaleonTabPrivate GaleonTabPrivate;

typedef enum
{
	TAB_LOAD_NONE,
	TAB_LOAD_STARTED,
	TAB_LOAD_COMPLETED
} TabLoadStatus;

struct GaleonTab 
{
        GtkBin parent;
        GaleonTabPrivate *priv;
};

struct GaleonTabClass
{
        GtkBinClass parent_class;

	void (*site_visited) (GaleonTab *tab, const char * url, 
			      const char *title);
};

/* Include the header down here to resolve circular dependency */
#include "galeon-window.h"

GType         galeon_tab_get_type     		(void);

GaleonTab    *galeon_tab_new          		(void);

GaleonEmbed  *galeon_tab_get_embed    		(GaleonTab *tab);

GaleonWindow *galeon_tab_get_window   		(GaleonTab *tab);

gboolean      galeon_tab_get_visibility         (GaleonTab *tab);

TabLoadStatus galeon_tab_get_load_status     	(GaleonTab *tab);
void	      galeon_tab_set_load_status	(GaleonTab *tab, TabLoadStatus status);

int	      galeon_tab_get_load_percent 	(GaleonTab *tab);

const char   *galeon_tab_get_status_message  	(GaleonTab *tab);

const char   *galeon_tab_get_title		(GaleonTab *tab);

const char   *galeon_tab_get_location           (GaleonTab *tab);

void	      galeon_tab_set_location           (GaleonTab *tab,
						 const char *location);

gboolean      galeon_tab_is_empty               (GaleonTab *tab);

int           galeon_tab_get_zoom               (GaleonTab *tab);

const char   *galeon_tab_get_blocked_popup_uri  (GaleonTab *tab);
void	     galeon_tab_clear_blocked_popup_uri (GaleonTab *tab);

void	      galeon_tab_get_size		(GaleonTab *tab,
						 int *width,
						 int *height);

GtkWidget    *galeon_tab_get_icon               (GaleonTab *tab);

G_END_DECLS

#endif
