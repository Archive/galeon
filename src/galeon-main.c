/*
 *  Copyright (C) 2000-2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/* We use the deprecated popt stuff from libgnome .. */
#undef GNOME_DISABLE_DEPRECATED

#include "galeon-shell.h"
#include "galeon-window.h"
#include "galeon-dbus.h"
#include "galeon-dbus-client-bindings.h"
#include "gul-state.h"
#include "galeon-debug.h"
#include "gul-x11.h"
#include "hig-alert.h"

#include <libgnome/gnome-program.h>
#include <libgnomeui/gnome-ui-init.h>
#include <libgnomeui/gnome-app-helper.h>
#include <libgnomevfs/gnome-vfs-init.h>
#include <glib/gi18n.h>
#include <gtk/gtkwindow.h>
#include <gdk/gdkx.h>
#include <string.h>
#ifdef ENABLE_NLS
# include <locale.h>
#endif

static void
galeon_main_start (DBusGProxy *proxy, guint32 user_time);

/* FIXME */
static gboolean open_in_existing      = FALSE;  /* load in existing window?     */
static gboolean open_in_new_tab       = FALSE;  /* force open in a new tab?     */
static gboolean noraise               = FALSE;  /* no raise                     */
static gboolean open_in_new_window    = FALSE;  /* force open in a new window?  */
static gboolean open_fullscreen       = FALSE;  /* open galeon in full screen ? */
static gchar   *session_filename      = NULL;   /* the session filename         */
static gchar   *geometry_string       = NULL;   /* the geometry string          */
static gchar   *bookmark_url          = NULL;   /* the temp bookmark to add     */
static gboolean close_option          = FALSE;  /* --close                      */
static gboolean quit_option           = FALSE;  /* --quit                       */
static gboolean galeon_server_mode    = FALSE;

static gchar **arguments              = NULL;

/* command line argument parsing structure */
static const GOptionEntry option_entries[] =
{
	{ "new-tab", 'n', 0, G_OPTION_ARG_NONE, &open_in_new_tab,
	  N_("Open a new tab in an existing Galeon window"),
	  NULL },
	{ "new-window", 'w', 0, G_OPTION_ARG_NONE, &open_in_new_window,
	  N_("Open a new window in an existing Galeon process"),
	  NULL },
	{ "noraise", '\0', 0, G_OPTION_ARG_NONE, &noraise,
	  N_("Do not raise the window when opening a page in an existing Galeon process"),
	  NULL },
	{ "fullscreen", 'f', 0, G_OPTION_ARG_NONE, &open_fullscreen,
	  N_("Run Galeon in full screen mode"),
	  NULL },
	{ "existing", 'x', 0, G_OPTION_ARG_NONE, &open_in_existing,
	  N_("Attempt to load URL in existing Galeon window"),
	  NULL },
	{ "load-session", 'l', 0, G_OPTION_ARG_STRING, &session_filename,
	  N_("Load the given session file"),
	  N_("FILE") },
	{ "server", 's', 0, G_OPTION_ARG_NONE, &galeon_server_mode,
	  N_("Don't open any windows; instead act as a server "
	     "for quick startup of new Galeon instances"),
	  NULL },		  
	{ "add-bookmark", 't', 0, G_OPTION_ARG_STRING, &bookmark_url,
	  N_("Add a bookmark (don't open any window)"),
	  N_("URL")},
	{ "geometry", 'g', 0, G_OPTION_ARG_STRING, &geometry_string,
          N_("Create the initial window with the given geometry, "
	     "see X(1) for the GEOMETRY format"),
	  N_("GEOMETRY")},
	{ "close", 'c', 0, G_OPTION_ARG_NONE, &close_option,
	  N_("Close all Galeon windows"),
	  NULL },
	{ "quit", 'q', 0, G_OPTION_ARG_NONE, &quit_option,
	  N_("Same as --close, but exits server mode too"),
	  NULL },
        { G_OPTION_REMAINING, '\0', 0, G_OPTION_ARG_FILENAME_ARRAY, &arguments, "",
          N_("URL ...") },

	/* terminator, must be last */
	{ NULL }
};

static void
default_window_icon_init (void)
{
	GdkPixbuf * icon;
	GList *icon_list = NULL;

	icon = gdk_pixbuf_new_from_file (DATADIR "/pixmaps/galeon.png", NULL);
	if (icon)
	{
		icon_list = g_list_prepend (icon_list, icon);

		gtk_window_set_default_icon_list (icon_list);
		
		g_object_unref (icon);
		g_list_free (icon_list);
	}
}


static gboolean
unref_when_idle (gpointer user_data)
{
	g_object_unref (user_data);
	return FALSE;
}

/* Copied from libnautilus/nautilus-program-choosing.c; Needed in case
 * we have no DESKTOP_STARTUP_ID (with its accompanying timestamp).
 */
static Time
slowly_and_stupidly_obtain_timestamp (Display *xdisplay)
{
	Window xwindow;
	XEvent event;
	
	{
		XSetWindowAttributes attrs;
		Atom atom_name;
		Atom atom_type;
		char* name;
		
		attrs.override_redirect = True;
		attrs.event_mask = PropertyChangeMask | StructureNotifyMask;
		
		xwindow =
			XCreateWindow (xdisplay,
				       RootWindow (xdisplay, 0),
				       -100, -100, 1, 1,
				       0,
				       CopyFromParent,
				       CopyFromParent,
				       CopyFromParent,
				       CWOverrideRedirect | CWEventMask,
				       &attrs);
		
		atom_name = XInternAtom (xdisplay, "WM_NAME", TRUE);
		g_assert (atom_name != None);
		atom_type = XInternAtom (xdisplay, "STRING", TRUE);
		g_assert (atom_type != None);
		
		name = "Fake Window";
		XChangeProperty (xdisplay, 
				 xwindow, atom_name,
				 atom_type,
				 8, PropModeReplace, (guchar*)name, strlen (name));
	}
	
	XWindowEvent (xdisplay,
		      xwindow,
		      PropertyChangeMask,
		      &event);
	
	XDestroyWindow(xdisplay, xwindow);
	
	return event.xproperty.time;
}

static void
shell_weak_notify (gpointer data,
                   GObject *zombie)
{
        if (gtk_main_level ())
        {
                gtk_main_quit ();
        }
}

static void
unref_proxy_reply_cb (DBusGProxy *proxy,
		      GError *error,
		      gpointer user_data)
{
	if (error != NULL)
	{
		g_warning ("An error occurred while calling remote method: %s", error->message);
		g_error_free (error);
	}

	g_object_unref (proxy);

	if (gtk_main_level ())
	{
		gtk_main_quit ();
	}
}

static gboolean
open_urls (DBusGProxy *proxy,
	   guint32 user_time,
	   GError **error)
{
	static const char *empty_arguments[] = { "", NULL };
	GString *options;
	char **uris;

	options = g_string_sized_new (64);

	if (open_in_new_window)
	{
		g_string_append (options, "new-window,");
	}
	if (open_in_new_tab)
	{
		g_string_append (options, "new-tab,");
	}
        if (noraise)
        {
		g_string_append (options, "noraise,");
        }
        if (open_fullscreen)
        {
		g_string_append (options, "fullscreen,");
        }

	if (arguments == NULL)
	{
		uris = (char **) empty_arguments;
	}
	else
	{
		uris = (char **) arguments;
	}

	org_gnome_Galeon_load_ur_ilist_async
		(proxy, (const char **) uris, options->str, user_time,
		 unref_proxy_reply_cb, NULL);

	if (arguments != NULL)
	{
		g_strfreev (arguments);
		arguments = NULL;
	}

	g_string_free (options, TRUE);

	return TRUE;
}

static void
show_error_message (GError **error)
{
        GtkWidget *dialog;

        /* FIXME better texts!!! */
        dialog = gtk_message_dialog_new (NULL,
                                         GTK_DIALOG_MODAL,
                                         GTK_MESSAGE_ERROR,
                                         GTK_BUTTONS_CLOSE,
                                         _("Could not start Galeon Browser"));
        gtk_message_dialog_format_secondary_text
                (GTK_MESSAGE_DIALOG (dialog),
                 _("Startup failed because of the following error:\n%s"),
                 (*error)->message);

        g_clear_error (error);

        gtk_dialog_run (GTK_DIALOG (dialog));
}

int
main (int argc, char *argv[])
{
        GnomeProgram *program;
        GOptionContext *option_context;
        GOptionGroup *option_group;
        GError *error = NULL;
        DBusGProxy *proxy;
	guint32 user_time;

#ifdef ENABLE_NLS
	/* Initialize the i18n stuff */
	setlocale(LC_ALL, "");
	bindtextdomain(GETTEXT_PACKAGE, DATADIR "/locale");
	textdomain(GETTEXT_PACKAGE);
#endif
	/* get this early, since gdk will unset the env var */
	user_time = gul_x11_get_startup_id ();

        g_type_init ();
        g_thread_init (NULL);

	dbus_g_thread_init ();

        option_context = g_option_context_new ("");
        option_group = g_option_group_new ("galeon",
                                           N_("Galeon Web Browser"),
                                           N_("Galeon Web Browser options"),
                                           NULL, NULL);

        g_option_group_set_translation_domain (option_group, GETTEXT_PACKAGE);

        g_option_group_add_entries (option_group, option_entries);

        g_option_context_set_main_group (option_context, option_group);

	program = gnome_program_init (PACKAGE, VERSION,
                                      LIBGNOMEUI_MODULE, argc, argv,
                                      GNOME_PARAM_GOPTION_CONTEXT, option_context,
                                      GNOME_PARAM_HUMAN_READABLE_NAME, _("Galeon"),
				      GNOME_PARAM_APP_DATADIR, DATADIR,
                                      NULL);

	/* Get a timestamp manually if need be */
	if (user_time == 0)
		user_time = slowly_and_stupidly_obtain_timestamp (gdk_display);        
#ifdef ENABLE_NLS
	bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
#endif

	/* set the application name manually until gnome handles the
	 * GNOME_PARAM_HUMAN_READABLE_NAME in a sane fashion.
	 *
	 * This MUST be after the bind_textdomain_codeset(UTF-8) - bug 301665
	 */
	g_set_application_name (_("Galeon"));

        if (! _galeon_dbus_startup (TRUE, &error))
	{
		_galeon_dbus_release ();

		show_error_message (&error);

		exit (1);
	}

        /* Create DBUS proxy */
        proxy = galeon_dbus_get_proxy (galeon_dbus_get_default (), GALEON_DBUS_SESSION);
        if (proxy == NULL)
        {
                error = g_error_new (1, // STARTUP_ERROR_QUARK,
                                     0,
                                     "Unable to get DBus proxy; aborting activation."); /* FIXME i18n */

                _galeon_dbus_release ();

                show_error_message (&error);

                exit (1);
        }

        if (! _galeon_dbus_is_name_owner ())
	{
		/* Connect to existing instance */
		galeon_main_start (proxy, user_time);
		gdk_notify_startup_complete ();
	}
	else
	{
		galeon_debug_init ();

		gul_state_init ();
		
		gnome_vfs_init ();
		
		default_window_icon_init ();
		
		galeon_shell_new ();
		
		/* Gdk will handle the user time itself */
		galeon_main_start (proxy, 0u);

		/* Flush any pending GTK events, this works around crashes when
		 * it takes a long time to startup. */
		while (gtk_events_pending())
			gtk_main_iteration();

                g_object_weak_ref (G_OBJECT (galeon_shell), shell_weak_notify, NULL);
		g_idle_add (unref_when_idle, galeon_shell);

                gtk_main ();
		
		gul_state_shutdown ();

		gnome_accelerators_sync ();

		gnome_vfs_shutdown ();
	} 

        _galeon_dbus_release ();

	return 0;
}

static void
galeon_main_start (DBusGProxy *proxy, guint32 user_time)
{
        GError   *error    = NULL;
        gboolean  success;

	/* Server mode */
	if (galeon_server_mode && ! close_option)
	{
                GaleonShell *shell = galeon_shell_get_default ();

                galeon_shell_set_server_mode (shell, TRUE);
	}

	/* load the session if requested */
	if (session_filename)
	{
                org_gnome_Galeon_load_session_async (proxy,
                                                     session_filename,
                                                     user_time,
                                                     unref_proxy_reply_cb,
                                                     NULL);
	}
	/* if found and we're given a bookmark to add... */
	else if (bookmark_url != NULL)
	{
                GbBookmarkSet *set;
                GaleonShell   *shell;
                GbSite        *bookmark;

                shell = galeon_shell_get_default ();

                set = galeon_shell_get_bookmark_set (shell);
                g_return_if_fail (set);

                bookmark = gb_site_new (set, NULL, bookmark_url);
                gb_bookmark_set_add_default (set, GB_BOOKMARK (bookmark));
	}
	else if (close_option || quit_option)
	{
                success = org_gnome_Galeon_quit (proxy, quit_option, &error);
                if (! success) {
                        _galeon_dbus_release ();

                        show_error_message (&error);

                        exit (1);
                }
	}
        else if (arguments || ! galeon_server_mode) {
         	success = open_urls (proxy, user_time, &error);
                if (! success)
                {
                        _galeon_dbus_release ();

                        show_error_message (&error);

                        exit (1);
                }
        }

	/* FIXME why? */
        dbus_g_connection_flush (galeon_dbus_get_bus (galeon_dbus_get_default (), GALEON_DBUS_SESSION));
}
