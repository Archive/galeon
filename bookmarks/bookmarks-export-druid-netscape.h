/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __bookmarks_export_druid_netscape_h
#define __bookmarks_export_druid_netscape_h

#include <glib-object.h>
#include <bookmarks-export-druid.h>

/* object forward declarations */

typedef struct _GbExportDruidNetscape GbExportDruidNetscape;
typedef struct _GbExportDruidNetscapeClass GbExportDruidNetscapeClass;
typedef struct _GbExportDruidNetscapePrivate GbExportDruidNetscapePrivate;

/**
 * Druid object
 */

#define GB_TYPE_EXPORT_DRUID_NETSCAPE		(gb_export_druid_netscape_get_type())
#define GB_EXPORT_DRUID_NETSCAPE(object)	(G_TYPE_CHECK_INSTANCE_CAST((object), \
						 GB_TYPE_EXPORT_DRUID_NETSCAPE,\
						 GbExportDruidNetscape))
#define GB_EXPORT_DRUID_NETSCAPE_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), \
						 GB_TYPE_EXPORT_DRUID_NETSCAPE,\
						 GbExportDruidNetscapeClass))
#define GB_IS_EXPORT_DRUID_NETSCAPE(object)	(G_TYPE_CHECK_INSTANCE_TYPE((object), \
						 GB_TYPE_EXPORT_DRUID_NETSCAPE))
#define GB_IS_EXPORT_DRUID_NETSCAPE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), \
						  GB_TYPE_EXPORT_DRUID_NETSCAPE))
#define GB_EXPORT_DRUID_NETSCAPE_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), \
						 GB_TYPE_EXPORT_DRUID_NETSCAPE,\
						 GbExportDruidNetscapeClass))

struct _GbExportDruidNetscapeClass 
{
	GbExportDruidClass parent_class;

};

/* Remember: fields are public read-only */
struct _GbExportDruidNetscape
{
	GbExportDruid parent_object;
};

GType			gb_export_druid_netscape_get_type		(void);
GbExportDruidNetscape *	gb_export_druid_netscape_new			(void);

#endif
