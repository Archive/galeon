/*
 *  Copyright (C) 2002 Jorn Baayen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef PPVIEW_TOOLBAR_H
#define PPVIEW_TOOLBAR_H

#include "galeon-window.h"
#include <glib-object.h>
#include <glib.h>
#include <gtk/gtkbutton.h>
#include "egg-dock-item.h"

G_BEGIN_DECLS

typedef struct PPViewToolbar PPViewToolbar;
typedef struct PPViewToolbarClass PPViewToolbarClass;

#define TYPE_PPVIEW_TOOLBAR             (ppview_toolbar_get_type ())
#define PPVIEW_TOOLBAR(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_PPVIEW_TOOLBAR, PPViewToolbar))
#define PPVIEW_TOOLBAR_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), PPVIEW_TOOLBAR, PPViewToolbarClass))
#define IS_PPVIEW_TOOLBAR(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_PPVIEW_TOOLBAR))
#define IS_PPVIEW_TOOLBAR_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), PPVIEW_TOOLBAR))

typedef struct PPViewToolbarPrivate PPViewToolbarPrivate;

struct PPViewToolbar
{
        GObject parent;
        PPViewToolbarPrivate *priv;
};

struct PPViewToolbarClass
{
        GObjectClass parent_class;

	void ( *print)       (PPViewToolbar *toolbar);	
};

GType          ppview_toolbar_get_type        		(void);

PPViewToolbar *ppview_toolbar_new             		(GaleonWindow *window);

void           ppview_toolbar_set_visibility  		(PPViewToolbar *t, 
				       		         gboolean visibility);

void           ppview_toolbar_set_old_chrome		(PPViewToolbar *t,
							 EmbedChromeMask chrome);

void           ppview_toolbar_set_dock_item             (PPViewToolbar *t,
							 EggDockItem *item);
G_END_DECLS

#endif
