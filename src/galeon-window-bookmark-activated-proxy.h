/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __galeon_window_bookmark_activated_proxy_h
#define __galeon_window_bookmark_activated_proxy_h

#include <glib-object.h>
#include "galeon-window.h"

/**
 * The purporse of this class is to receive bookmark-activated signals
 * and forward them to a GaleonWindow. It is needed because a
 * GaleonWindow can be closed at any moment and we still want to
 * receive the signals. This happens currently in the bookmarks
 * editor. When the GaleonWindow is closed, this class takes care of
 * creating a new window when a signal is received.
 */

/* object forward declarations */

typedef struct _GaleonWindowBookmarkActivatedProxy GaleonWindowBookmarkActivatedProxy;
typedef struct _GaleonWindowBookmarkActivatedProxyClass GaleonWindowBookmarkActivatedProxyClass;

/**
 * WindowBookmarkActivatedProxy object
 */
#define GALEON_TYPE_WINDOW_BOOKMARK_ACTIVATED_PROXY		\
	(galeon_window_bookmark_activated_proxy_get_type())
#define GALEON_WINDOW_BOOKMARK_ACTIVATED_PROXY(object)		\
	(G_TYPE_CHECK_INSTANCE_CAST((object), GALEON_TYPE_WINDOW_BOOKMARK_ACTIVATED_PROXY,\
	 GaleonWindowBookmarkActivatedProxy))
#define GALEON_WINDOW_BOOKMARK_ACTIVATED_PROXY_CLASS(klass)	\
	(G_TYPE_CHECK_CLASS_CAST((klass), GALEON_TYPE_WINDOW_BOOKMARK_ACTIVATED_PROXY,\
	 GaleonWindowBookmarkActivatedProxyClass))
#define GALEON_IS_WINDOW_BOOKMARK_ACTIVATED_PROXY(object)	\
	(G_TYPE_CHECK_INSTANCE_TYPE((object), GALEON_TYPE_WINDOW_BOOKMARK_ACTIVATED_PROXY))
#define GALEON_IS_WINDOW_BOOKMARK_ACTIVATED_PROXY_CLASS(klass)	\
	(G_TYPE_CHECK_CLASS_TYPE((klass), GALEON_TYPE_WINDOW_BOOKMARK_ACTIVATED_PROXY))
#define GALEON_WINDOW_BOOKMARK_ACTIVATED_PROXY_GET_CLASS(obj)	\
	(G_TYPE_INSTANCE_GET_CLASS((obj), GALEON_TYPE_WINDOW_BOOKMARK_ACTIVATED_PROXY,\
	 GaleonWindowBookmarkActivatedProxyClass))

struct _GaleonWindowBookmarkActivatedProxyClass 
{
	GObjectClass parent_class;

};

struct _GaleonWindowBookmarkActivatedProxy
{
	GObject parent_object;
	
	GaleonWindow *window;
};

GType
galeon_window_bookmark_activated_proxy_get_type		(void);

GaleonWindowBookmarkActivatedProxy *
galeon_window_bookmark_activated_proxy_new		(void);

void
galeon_window_bookmark_activated_proxy_set_window	(GaleonWindowBookmarkActivatedProxy *wbap, 
							 GaleonWindow *w);
void
galeon_window_bookmark_activated_proxy_connect		(GaleonWindowBookmarkActivatedProxy *wbap, 
							 GObject *o);


#endif
