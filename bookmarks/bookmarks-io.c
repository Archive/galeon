/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "bookmarks-io.h"

#include "xbel.h"
#include "bookmarks-netscape.h"
#include <libgnomevfs/gnome-vfs-mime.h>

/**
 * Private functions, only availble from this file
 */
static GHashTable *object_for_mime = NULL;
static GbIO *default_object = NULL;

/**
 * IO object
 */

G_DEFINE_TYPE (GbIO, gb_io, G_TYPE_OBJECT);

static void
gb_io_class_init (GbIOClass *klass)
{
	/* This class is abstract */
	/* there's no implementation for it's virtual functions */
	
	klass->gb_io_load_from_file = NULL;
	klass->gb_io_load_from_string = NULL;
	klass->gb_io_save_to_file = NULL;
	klass->gb_io_save_to_string = NULL;
	klass->gb_io_format_name = NULL;
	klass->gb_io_extensions = NULL;
	
}

static void 
gb_io_init (GbIO *io)
{
}

GbBookmarkSet *
gb_io_load_from_file (GbIO *io, const gchar *filename)
{
	GbIOClass *klass = GB_IO_GET_CLASS (io);
	return klass->gb_io_load_from_file (io, filename);
}

GbBookmarkSet *
gb_io_load_from_string (GbIO *io, const gchar *data)
{
	GbIOClass *klass = GB_IO_GET_CLASS (io);
	return klass->gb_io_load_from_string (io, data);
}

gboolean
gb_io_save_to_file (GbIO *io, GbBookmarkSet *set,
		    const gchar *filename)
{
	GbIOClass *klass = GB_IO_GET_CLASS (io);
	return klass->gb_io_save_to_file (io, set, filename);
}

gboolean
gb_io_save_to_string (GbIO *io, GbBookmarkSet *set, gchar **data)
{
	GbIOClass *klass = GB_IO_GET_CLASS (io);
	return klass->gb_io_save_to_string (io, set, data);
}

gchar *
gb_io_format_name (GbIO *io)
{
	GbIOClass *klass = GB_IO_GET_CLASS (io);
	return klass->gb_io_format_name (io);
}

GList *
gb_io_extensions (GbIO *io)
{
	GbIOClass *klass = GB_IO_GET_CLASS (io);
	return klass->gb_io_extensions (io);
}

GbIO *
gb_io_object_for_file (const gchar *file)
{
	const char *mime = gnome_vfs_get_file_mime_type (file, NULL, FALSE);
	if (!default_object)
	{
		object_for_mime = g_hash_table_new (g_str_hash, g_str_equal);
		default_object = (GbIO *) gb_xbel_new ();

		g_hash_table_insert (object_for_mime, "application/xbel",
				     default_object);
		g_hash_table_insert (object_for_mime, "application/mozilla-bookmarks",
				     gb_io_netscape_new ());
		g_hash_table_insert (object_for_mime, "application/netscape-bookmarks",
				     gb_io_netscape_new ());
	}
	if (mime)
	{
		GbIO *ret = g_hash_table_lookup (object_for_mime, mime);
		return ret ? ret : default_object;
	}
	else
	{
		return default_object;
	}
}


