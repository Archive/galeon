/*
 *  Copyright (C) 2000 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <nsCOMPtr.h>
#include <JSConsoleListener.h>
#include <nsIScriptError.h>
#include <nsMemory.h>

#include "GulString.h"
#include "gul-string.h"
#include "js-console.h"
#include "mozilla-embed-shell.h"

#include <glib/gi18n.h>

/* Implementation file */
NS_IMPL_ISUPPORTS1(JSConsoleListener, nsIConsoleListener)

JSConsoleListener::JSConsoleListener()
{
  /* member initializers and constructor code */
}

JSConsoleListener::~JSConsoleListener()
{
  /* destructor code */
}

/* void observe (in nsIConsoleMessage aMessage); */
NS_IMETHODIMP JSConsoleListener::Observe(nsIConsoleMessage *aMessage)
{
	PRUnichar *message;

#ifdef HAVE_NSICONSOLEMESSAGE_GETMESSAGEMOZ
	aMessage->GetMessageMoz (&message);
#else
	aMessage->GetMessage (&message);
#endif

	GaleonJSConsole * js_console;
	js_console = galeon_embed_shell_get_js_console (embed_shell);

	nsCOMPtr<nsIScriptError> error = do_QueryInterface (aMessage);
	if (error) /* best interface, more infos */
	{
		/* build a message */
		GaleonJSConsoleMessageType type;

		PRUint32 flags;
		error->GetFlags (&flags);
		if (flags & nsIScriptError::warningFlag)
		{
			type = GALEON_JS_CONSOLE_WARNING;
		}
		else
		{
			type = GALEON_JS_CONSOLE_ERROR;
		}

		GulString sourceName;
		error->GetSourceName (sourceName);
		GulString sourceLine;
		error->GetSourceLine (sourceLine);
		PRUint32 lineNumber;
		error->GetLineNumber (&lineNumber);
		PRUint32 columnNumber;
		error->GetColumnNumber (&columnNumber);

		galeon_js_console_error (GALEON_JS_CONSOLE(js_console),
					 GulCString(message).get(),
					 type,
					 GulCString(sourceName).get(),
					 lineNumber, 
					 GulCString(sourceLine).get(),
					 columnNumber);
	}
	else
	{
		galeon_js_console_message (GALEON_JS_CONSOLE(js_console),
					   GulCString(message).get());
	}

	nsMemory::Free (message);

	return NS_OK;
}
