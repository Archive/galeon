/* 
 *  Copyright (C) 2002 Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/* system includes */
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <glib/gi18n.h>
#include <libgnome/gnome-util.h>

#include "bookmarks-netscape.h"
#include "bookmarks-netscape-mozilla.h"
#include "gul-general.h"

#define NOT_IMPLEMENTED g_warning ("not implemented: " G_STRLOC);

/**
 * Private typedefs
 */
/**
 * NSItemType: netscape bookmark item type
 */
typedef enum
{
	NS_SITE,
	NS_NOTES,
	NS_FOLDER,
	NS_FOLDER_END,
	NS_SEPARATOR,
	NS_UNKNOWN
} NSItemType;

/**
 * Private functions, only availble from this file
 */
static GbBookmarkSet *	gb_io_netscape_load_from_file		(GbIO *io, const gchar *filename);
static GbBookmarkSet *	gb_io_netscape_load_from_string		(GbIO *io, const gchar *data);
static gboolean		gb_io_netscape_save_to_file		(GbIO *io, GbBookmarkSet *set,
							 const gchar *filename);
static gboolean		gb_io_netscape_save_to_string		(GbIO *io, GbBookmarkSet *set, gchar **data);
static gchar *		gb_io_netscape_format_name 		(GbIO *io);
static GList *		gb_io_netscape_extensions 		(GbIO *io);


/**
 * GbIONetscape object
 */

G_DEFINE_TYPE (GbIONetscape, gb_io_netscape, GB_TYPE_IO);

static void
gb_io_netscape_class_init (GbIONetscapeClass *klass)
{
	
	klass->parent_class.gb_io_load_from_file = gb_io_netscape_load_from_file;
	klass->parent_class.gb_io_load_from_string = gb_io_netscape_load_from_string;
	klass->parent_class.gb_io_save_to_file = gb_io_netscape_save_to_file;
	klass->parent_class.gb_io_save_to_string = gb_io_netscape_save_to_string;
	klass->parent_class.gb_io_format_name = gb_io_netscape_format_name;
	klass->parent_class.gb_io_extensions = gb_io_netscape_extensions;
	
}

static void 
gb_io_netscape_init (GbIONetscape *io)
{
}

GbIONetscape *
gb_io_netscape_new (void)
{
	return g_object_new (GB_TYPE_IO_NETSCAPE, NULL);
}

/**
 * Start the import of a IO_NETSCAPE document, with a given filename and
 * the default bookmarks folder
 */
GbBookmarkSet *
gb_io_netscape_load_from_file (GbIO *io, const gchar *filename)
{ 
	return netscape_import_bookmarks (filename, TRUE);
}

GbBookmarkSet *
gb_io_netscape_load_from_string (GbIO *io, const gchar *data)
{
	NOT_IMPLEMENTED;
	return NULL;
}

gboolean
gb_io_netscape_save_to_file (GbIO *io, GbBookmarkSet *set,
			     const gchar *filename)
{
	g_return_val_if_fail (filename != NULL, FALSE);

	return netscape_export_bookmarks (filename, set, TRUE);
}


gboolean
gb_io_netscape_save_to_string (GbIO *io, GbBookmarkSet *set, gchar **data)
{
	NOT_IMPLEMENTED;
	return FALSE;
}

gchar *
gb_io_netscape_format_name (GbIO *io)
{
	return g_strdup (_("Netscape bookmarks format"));
}

GList *
gb_io_netscape_extensions (GbIO *io)
{
	static gchar *extensions[] = { "html", "htm", NULL };
	GList *l = NULL;
	int i;
	for (i = 0; extensions[i] != NULL; i++)
	{
		l = g_list_append (l, g_strdup (extensions[i]));
	}
	return l;
}
	
