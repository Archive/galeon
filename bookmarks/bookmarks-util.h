/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __bookmarks_util_h
#define __bookmarks_util_h

#include "bookmarks.h"

/**
 * How can a bookmark be activated
 */
typedef enum
{
	GB_BAF_DEFAULT,
	GB_BAF_NEW_TAB_OR_WINDOW,
	GB_BAF_NEW_WINDOW,
	GB_BAF_NEW_TAB,
	GB_BAF_EDITOR
} GbBookmarkActivatedFlags;

/**
 * Activation signal
 *
 * emitter:		Object that sends the signal (tb widget, menuitem, editor...)
 * bookmark:		The activated bookmark
 * url:			The actual url. Maybe NULL (folders) or different than the 
 *			bookmark url (smart sites)
 * flags:		how to do it
 * event:		GdkEvent that triggered the activation, or NULL if not
 * 			available
 */
typedef struct
{
	GbBookmark *bookmark;
	const char *url;
	GbBookmarkActivatedFlags flags;
	GdkEvent *event;
} GbBookmarkEventActivated;

typedef void (*GbBookmarkActivatedCallback) (GObject *emitter, 
					     GbBookmarkEventActivated *event);

#define gb_activated_activate(o, b, u, f)		\
	gb_activated_event ((o), (b), (u), (f), NULL)

#define gb_activated_event(o, b, u, f, e) G_STMT_START{\
	GbBookmarkEventActivated __event = { (b), (u), (f), (e) }; \
	g_signal_emit_by_name ((o), "bookmark-activated", &__event); \
}G_STMT_END

#define gb_activated_emit(o, sig, b, u, f) G_STMT_START{\
	GbBookmarkEventActivated __event = { (b), (u), (f), NULL }; \
	g_signal_emit ((o), (sig), 0, &__event); \
}G_STMT_END


GSList *		gb_util_remove_descendants_from_list	(GSList *l);
void			gb_util_merge_trees			(GbFolder *dest, GbFolder *orig);

gchar *			gb_util_options_get			(const gchar *options, const gchar *option);
gchar *			gb_util_options_set			(const gchar *options, 
								 const gchar *option, const gchar *value);

#endif
