/*
 *  Copyright (C) 2001 Philip Langdale
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <nsIURI.h>

#include "galeon-embed-shell.h"
#include "GlobalHistory.h"
#include "GulString.h"

/**
 * class GlobalHistory: 
 *
 */

NS_IMPL_ISUPPORTS2(MozGlobalHistory, nsIGlobalHistory2, nsIGlobalHistory3)

MozGlobalHistory::MozGlobalHistory ()
{
	mGlobalHistory = galeon_embed_shell_get_global_history (embed_shell);
}

MozGlobalHistory::~MozGlobalHistory ()
{
}

/* void addURI (in nsIURI aURI, in boolean aRedirect, in boolean aToplevel); */
NS_IMETHODIMP MozGlobalHistory::AddURI (nsIURI *aURI,
					PRBool aRedirect,
					PRBool aTopLevel,
					nsIURI *aReferrer)
{
	nsresult rv;
	NS_ENSURE_ARG_POINTER(aURI);

	/* Filter out unwanted URI's from the history,
	 * 
	 * We check the common cases first, then check against the banned
	 * list */

	PRBool isHTTP, isHTTPS;

	rv  = aURI->SchemeIs("http", &isHTTP);
	rv |= aURI->SchemeIs("https", &isHTTPS);
	NS_ENSURE_SUCCESS (rv, NS_ERROR_FAILURE);

	if (!isHTTP && !isHTTPS)
	{
		const char *blacklist[] = {
			"about", "view-source", "chrome", "data", "javascript",
			"myportal"
		};

		for (guint i = 0; i < G_N_ELEMENTS(blacklist); i++)
		{
			PRBool match;
			rv = aURI->SchemeIs(blacklist[i], &match);
			NS_ENSURE_SUCCESS(rv, NS_ERROR_FAILURE);
			if (match)
			{
				return NS_OK;
			}
		}
	}

	GulCString spec;
	rv = aURI->GetSpec(spec);
	NS_ENSURE_SUCCESS(rv, rv);

	global_history_visited (mGlobalHistory, spec.get(), aRedirect, aTopLevel);
	
	return NS_OK;
}

/* boolean isVisited (in string aURL); */
NS_IMETHODIMP MozGlobalHistory::IsVisited (nsIURI *aURI, PRBool *_retval)
{
	GulCString spec;
	aURI->GetSpec(spec);

	*_retval = global_history_is_visited (mGlobalHistory, spec.get());

	return NS_OK;
}

/* void setPageTitle (in string aURL, in wstring aTitle); */
NS_IMETHODIMP MozGlobalHistory::SetPageTitle (nsIURI *aURI, 
					      const nsAString& aTitle)
{
	GulCString spec;
	aURI->GetSpec(spec);

	global_history_set_page_title(mGlobalHistory, spec.get(),
				      GulCString (aTitle).get());

	return NS_OK;
}

#ifdef HAVE_NSIGLOBALHISTORY3_SETURIGECKOFLAGS
/* unsigned long getURIGeckoFlags (in nsIURI aURI); */
NS_IMETHODIMP MozGlobalHistory::GetURIGeckoFlags(nsIURI *aURI,
						 PRUint32 *_retval)
{
	GulCString spec;
	aURI->GetSpec(spec);

	*_retval = global_history_get_page_flags(mGlobalHistory,
						 spec.get());

	return *_retval == (PRUint32)-1 ? NS_ERROR_INVALID_ARG : NS_OK;
}

/* void setURIGeckoFlags (in nsIURI aURI, in unsigned long aFlags); */
NS_IMETHODIMP MozGlobalHistory::SetURIGeckoFlags(nsIURI *aURI,
						 PRUint32 aFlags)
{
	GulCString spec;
	aURI->GetSpec(spec);

	gboolean retVal = global_history_set_page_flags(mGlobalHistory,
							spec.get(), aFlags);

	return retVal ? NS_OK: NS_ERROR_FAILURE;
}
#endif

/* void addDocumentRedirect (in nsIChannel aOldChannel, in nsIChannel aNewChannel, in PRInt32 aFlags, in boolean aTopLevel); */
NS_IMETHODIMP MozGlobalHistory::AddDocumentRedirect(nsIChannel *aOldChannel,
                                                    nsIChannel *aNewChannel,
                                                    PRInt32 aFlags,
                                                    PRBool aTopLevel)
{
	return NS_ERROR_NOT_IMPLEMENTED;
}
