/*
 *  Copyright (C) 2003  Tommi Komulainen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation. 
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */ 

#ifndef HIG_ALERT_H
#define HIG_ALERT_H 1

#include <gtk/gtkdialog.h>
#include <gtk/gtkstock.h>

G_BEGIN_DECLS

#define HIG_TYPE_ALERT	        (hig_alert_get_type ())
#define HIG_ALERT(o)	        (G_TYPE_CHECK_INSTANCE_CAST((o), HIG_TYPE_ALERT, HigAlert))
#define HIG_ALERT_CLASS(k)      (G_TYPE_CHECK_CLASS_CAST((k), HIG_TYPE_ALERT, HigAlertClass))
#define HIG_IS_ALERT(o)         (G_TYPE_CHECK_INSTANCE_TYPE((o), HIG_TYPE_ALERT))
#define HIG_IS_ALERT_CLASS(k)   (G_TYPE_CHECK_CLASS_TYPE((k), HIG_TYPE_ALERT))
#define HIG_ALERT_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS((o), HIG_TYPE_ALERT, HigAlertClass))

#define HIG_TYPE_ALERT_TYPE (hig_alert_type_get_type ())

typedef struct _HigAlert      HigAlert;
typedef struct _HigAlertClass HigAlertClass;

struct _HigAlert {
	GtkDialog parent;

	/*< public >*/
	GtkWidget *text_vbox;

	/*< protected >*/
	GtkWidget *image;
	GtkWidget *primary_text;
	GtkWidget *secondary_text;
};

struct _HigAlertClass {
	GtkDialogClass parent;
};

typedef enum {
	HIG_ALERT_INFORMATION,
	HIG_ALERT_ERROR,
	HIG_ALERT_CONFIRMATION,
	HIG_ALERT_AUTHENTICATION
} HigAlertType;

GType      hig_alert_type_get_type (void) G_GNUC_CONST;

GType      hig_alert_get_type (void) G_GNUC_CONST;

GtkWidget* hig_alert_new 		(GtkWindow      *parent,
					 GtkDialogFlags  flags,
					 HigAlertType    alert_type,
					 const char     *primary_text,
					 const char     *secondary_text,
					 const char     *first_button_text,
					 ...);

void       hig_alert_set_primary_printf	(HigAlert   *alert,
		                         const char *text_format,
					 ...) G_GNUC_PRINTF (2, 3);

void	   hig_alert_set_secondary_printf (HigAlert   *alert,
					   const char *text_format,
					   ...) G_GNUC_PRINTF (2, 3);

void       hig_alert_add_widget         (HigAlert  *alert,
		                         GtkWidget *widget);

G_END_DECLS

#endif /* HIG_ALERT_H */
