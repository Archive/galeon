/*
 *  Copyright (C) 2003 Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "galeon-action-stop.h"
#include "galeon-shell.h"
#include "gul-toolbutton.h"
#include "gul-string.h"
#include "gul-gui.h"

#include <gtk/gtkstock.h>
#include <gtk/gtkimagemenuitem.h>
#include <glib/gi18n.h>
#include <string.h>

static void		galeon_action_stop_connect_proxy		(GtkAction *action, GtkWidget *proxy);
static void		galeon_action_stop_update			(GaleonAction *action);
static void		galeon_action_stop_current_embed_changed	(GaleonAction *action, 
									 GaleonEmbed *old_embed, GaleonEmbed *new_embed);
static void       	galeon_action_stop_activate      		(GtkAction *action);


G_DEFINE_TYPE (GaleonActionStop, galeon_action_stop, GALEON_TYPE_ACTION);


static void
galeon_action_stop_class_init (GaleonActionStopClass *class)
{
	GtkActionClass *action_class = GTK_ACTION_CLASS (class);
	GaleonActionClass *galeon_action_class = GALEON_ACTION_CLASS (class);

	action_class->connect_proxy = galeon_action_stop_connect_proxy;
	action_class->menu_item_type    = GTK_TYPE_IMAGE_MENU_ITEM;
	action_class->toolbar_item_type = GUL_TYPE_TOOLBUTTON;
	action_class->activate          = galeon_action_stop_activate;

	galeon_action_class->update = galeon_action_stop_update;
	galeon_action_class->current_embed_changed = galeon_action_stop_current_embed_changed;
	
}

static void
galeon_action_stop_init (GaleonActionStop *action)
{
	g_object_set (G_OBJECT (action), "stock_id", GTK_STOCK_STOP, NULL);
	g_object_set (G_OBJECT (action), "tooltip", _("Stop current data transfer"), NULL);
}

static void
galeon_action_stop_update (GaleonAction *action)
{
	gboolean sensitive;
	GaleonWindow *window = galeon_action_get_window (action);
	GaleonTab *tab = galeon_window_get_active_tab (window);

	if (tab)
	{
		sensitive = galeon_tab_get_load_status (tab) & TAB_LOAD_STARTED;
	}
	else
	{
		sensitive = FALSE;
	}

	g_object_set (G_OBJECT (action), "sensitive", sensitive, NULL);
}

static void
galeon_action_stop_net_stop_start_cb (GaleonEmbed *embed, GaleonAction *action)
{
	galeon_action_stop_update (action);
}

static void
galeon_action_stop_current_embed_changed (GaleonAction *action, 
					  GaleonEmbed *old_embed, GaleonEmbed *new_embed)
{
	if (old_embed)
	{
		g_signal_handlers_disconnect_matched (old_embed, G_SIGNAL_MATCH_DATA,
						      0, 0, NULL, NULL, action);
	}

	if (new_embed)
	{
		g_signal_connect_object (new_embed, "ge_net_start", 
					 G_CALLBACK (galeon_action_stop_net_stop_start_cb),
					 action, 0);
		g_signal_connect_object (new_embed, "ge_net_stop", 
					 G_CALLBACK (galeon_action_stop_net_stop_start_cb),
					 action, 0);
	}

	galeon_action_stop_update (action);
}

static void
galeon_action_stop_activate (GtkAction *a)
{
	GaleonEmbed *embed = galeon_action_get_embed (GALEON_ACTION (a));

	g_return_if_fail (embed != NULL);

	galeon_embed_stop_load (embed);
}

static void
galeon_action_stop_stop_this_tab_activate_cb (GtkMenuItem *mi, GaleonActionStop *b)
{
	GaleonEmbed *embed = galeon_action_get_embed (GALEON_ACTION (b));
	galeon_embed_stop_load (embed);
}

static void
galeon_action_stop_stop_all_tabs_activate_cb (GtkMenuItem *mi, GaleonActionStop *b)
{
	GList *tabs;
	GaleonWindow *window;
	GList *li;

	window = galeon_action_get_window (GALEON_ACTION (b));
	g_return_if_fail (window != NULL);

	tabs = galeon_window_get_tabs (window);
	for (li = tabs; li; li = li->next)
	{
		GaleonEmbed *embed = galeon_tab_get_embed (li->data);
		galeon_embed_stop_load (embed);
	}
	
	g_list_free (tabs);
}

static void
galeon_action_stop_stop_all_windows_activate_cb (GtkMenuItem *mi, GaleonActionStop *b)
{
	const GList *windows;
	const GList *lj;
	Session *session;

	session = galeon_shell_get_session (galeon_shell);
	windows = session_get_windows (session);

	for (lj = windows; lj; lj = lj->next)
	{
		GaleonWindow *window = lj->data;
		GList *tabs;
		GList *li;
		
		tabs = galeon_window_get_tabs (window);
		for (li = tabs; li; li = li->next)
		{
			GaleonEmbed *embed = galeon_tab_get_embed (li->data);
			galeon_embed_stop_load (embed);
		}
		
		g_list_free (tabs);
	}
}

static void
galeon_action_stop_connect_proxy (GtkAction *a, GtkWidget *proxy)
{
	if (GUL_IS_TOOLBUTTON (proxy))
	{
		GtkMenuShell *ms;
		ms = gul_toolbutton_get_menu (GUL_TOOLBUTTON (proxy));

		gul_gui_append_new_menuitem (GTK_WIDGET (ms), _("_Stop Loading This Tab"), 
				     G_CALLBACK (galeon_action_stop_stop_this_tab_activate_cb), a);
		gul_gui_append_new_menuitem (GTK_WIDGET (ms), _("Stop Loading All Tabs in This _Window"), 
				     G_CALLBACK (galeon_action_stop_stop_all_tabs_activate_cb), a);
		gul_gui_append_new_menuitem (GTK_WIDGET (ms), _("Stop Loading _All Tabs"), 
				     G_CALLBACK (galeon_action_stop_stop_all_windows_activate_cb), a);
	}

	(* GTK_ACTION_CLASS (galeon_action_stop_parent_class)->connect_proxy) (a, proxy);
}

