/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "bookmarks-toolbar-widgets.h"
#include "bookmarks-widgets-private.h"
#include "bookmarks-site-tb-widget.h"
#include "bookmarks-smart-site-tb-widget.h"
#include "bookmarks-folder-tb-widget.h"
#include "bookmarks-separator-tb-widget.h"
#include "bookmarks-alias-placeholder-tb-widget.h"

#include "gtk/gtkhbox.h"
#include "gtk/gtklabel.h"
#include "gtk/gtkalignment.h"
#include "gtk/gtkimage.h"


GbTbWidget *
gb_create_toolbar_widget_site (GbSite *s)
{
	GbTbWidget *b = GB_TB_WIDGET (gb_site_tb_widget_new (s));
	return b;
}

GbTbWidget *
gb_create_toolbar_widget_smart_site (GbSmartSite *s)
{
	GbTbWidget *b = GB_TB_WIDGET (gb_smart_site_tb_widget_new (s));
	return b;
}

GbTbWidget *
gb_create_toolbar_widget_folder (GbFolder *f)
{
	GbTbWidget *b = GB_TB_WIDGET (gb_folder_tb_widget_new (f));
	return b;
}

GbTbWidget *
gb_create_toolbar_widget_separator (GbSeparator *s)
{
	GbTbWidget *b = GB_TB_WIDGET (gb_separator_tb_widget_new (s));
	return b;
}

GbTbWidget *
gb_create_toolbar_widget_alias_placeholder (GbAliasPlaceholder *s)
{
	GbTbWidget *b = GB_TB_WIDGET (gb_alias_placeholder_tb_widget_new (s));
	return b;
}

void
gb_widgets_fill_tb_item (GtkContainer *w, GbBookmark *b, const gchar *tip)
{
	GtkWidget *label = NULL;
	GtkWidget *icon = NULL;
	GtkWidget *image = NULL;
	GtkWidget *align;
	GdkPixbuf *pixbuf;
	GtkBox *box;

	if (b == NULL) return;

	box = GTK_BOX (gtk_hbox_new (FALSE, 2));

	pixbuf = gb_bookmark_get_image (b);
	if (pixbuf)
	{
		image = gtk_image_new_from_pixbuf (pixbuf);
		g_object_unref (G_OBJECT (pixbuf));
	}
	else
	{
		label = gtk_label_new (b->name);
		pixbuf = gb_bookmark_get_icon (b);
		if (pixbuf)
		{
			icon = gtk_image_new_from_pixbuf (pixbuf);
			g_object_unref (G_OBJECT (pixbuf));
		}
	}

	align = gtk_alignment_new (0.5, 0.5, 0.0, 0.0);
      
	if (icon) gtk_box_pack_start (box, icon, FALSE, FALSE, 0);
	if (label) gtk_box_pack_end (box, label, FALSE, FALSE, 0);
	if (image) gtk_box_pack_start (box, image, FALSE, FALSE, 0);
      
	gtk_container_add (GTK_CONTAINER (align), GTK_WIDGET (box));
	gtk_container_add (GTK_CONTAINER (w), align);
	gtk_widget_show_all (align);
	
}

