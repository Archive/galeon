/*
 *  Copyright (C) 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __MISC_GENERAL_H
#define __MISC_GENERAL_H

#include <glib.h>
#include <libxml/tree.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>

G_BEGIN_DECLS

typedef gboolean (*GulFilterFunc) (gpointer item, gpointer data);

char 		*gul_general_tmp_filename	 (const char *base,
						  const char *extension);

gboolean         gul_general_switch_temp_file (const char *filename,
					       const char *filename_temp);

gboolean         gul_general_safe_xml_save    (const char *filename,
					       xmlDocPtr doc);

gchar 		*gul_general_user_file 		 (const char *fname, gboolean critical);

gchar 		*gul_general_read_line_from_file (FILE *f);

gboolean	 gul_backup_file		 (const char *fname, 
					 	  guint num_backups, 
					 	  const gchar *newversion);

gboolean	 gul_copy_file			 (const char *orig,
						  const char *dest);

GSList *	 gul_find_file			 (const char *path, 
						  const char *fname, 
						  gint maxdepth);

gint 		xmlGetIntProp 			(xmlNodePtr node, 
					 	 const gchar *attribute);

GnomeVFSResult  gul_general_launch_application  (GnomeVFSMimeApplication *app,
                                		 const char *parameter,
						 guint32 user_time);

GSList *	gul_slist_filter		(const GSList *l, GulFilterFunc f, gpointer data);

G_END_DECLS

#endif

