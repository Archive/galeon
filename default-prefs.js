// disable annoying non-standard tags
pref("browser.blink_allowed", false);
pref("browser.display.enable_marquee", false);

// ensure the memory cache is turned on, mozilla will pick
// an appropriate memory cache size automatically
pref("browser.cache.memory.enable", true);

// disable image resizing and smooth scroll, they aren't quite there yet 
pref("browser.enable_automatic_image_resizing", false);
pref("general.smoothScroll", false);

// disable keyword search, it gives surprising results when trying to contact a
// server which is down, and we have much better smart bookmarks anyway ;)
pref("keyword.enabled", false);

// Text Zoom with Ctrl+Wheel
// http://bugzilla.gnome.org/show_bug.cgi?id=117626
pref("mousewheel.withcontrolkey.action", 3);

// History with Alt+Wheel, scroll by page with Shift+Wheel
pref("mousewheel.withaltkey.action", 2);
pref("mousewheel.withshiftkey.action", 1);

// disallow opening file:/// URLs on pages from network sources (http, etc.)
pref("security.checkloaduri", true);

// enable syntax highlighting and line wrapping in View Source
pref("view_source.syntax_highlight", true);
pref("view_source.wrap_long_lines", true);

// disable forms manager while we have no UI
pref("wallet.captureForms", false);

// disable SSLv2, it has security issues
pref("security.enable_ssl2", false);

// set mozilla's locale to match the system (firefox/thunderbird only)
pref("intl.locale.matchOS", true);

//===========================================================================
// You should not change the rest of the preferences below.

// disable sidebar What's Related, we don't use it
pref("browser.related.enabled", false);

// play nice with sloppy focus
pref("mozilla.widget.raise-on-setfocus", false);

// don't allow xpi installs from Galeon, there are crashes
pref("xpinstall.enabled", false);

// disable sucky XUL ftp view, have nice ns4-like html page instead
pref("network.dir.generate_html", true);

// deactivate mailcap support, it breaks Gnome-based helper apps
pref("helpers.global_mailcap_file", "");
pref("helpers.private_mailcap_file", "");

// use the mozilla defaults for mime.types files to let mozilla guess proper
// Content-Type for file uploads instead of always falling back to
// application/octet-stream
//
// user_pref("helpers.global_mime_types_file", null);
// user_pref("helpers.private_mime_types_file", null);

// fonts
pref("font.size.unit", "pt");
pref("font.size.fixed.ar", 10);
pref("font.size.fixed.x-baltic", 10);
pref("font.size.fixed.x-central-euro", 10);
pref("font.size.fixed.x-cyrillic", 10);
pref("font.size.fixed.x-devanagari", 10);
pref("font.size.fixed.el", 10);
pref("font.size.fixed.he", 10);
pref("font.size.fixed.ja", 10);
pref("font.size.fixed.ko", 10);
pref("font.size.fixed.x-tamil", 10);
pref("font.size.fixed.th", 10);
pref("font.size.fixed.zh-CN", 10);
pref("font.size.fixed.zh-TW", 10);
pref("font.size.fixed.zh-HK", 10);
pref("font.size.fixed.tr", 10);
pref("font.size.fixed.x-unicode", 10);
pref("font.size.fixed.x-western", 10);
pref("font.size.variable.ar", 11);
pref("font.size.variable.x-baltic", 11);
pref("font.size.variable.x-central-euro", 11);
pref("font.size.variable.x-cyrillic", 11);
pref("font.size.variable.x-devanagari", 11);
pref("font.size.variable.el", 11);
pref("font.size.variable.he", 11);
pref("font.size.variable.ja", 11);
pref("font.size.variable.ko", 11);
pref("font.size.variable.x-tamil", 11);
pref("font.size.variable.th", 11);
pref("font.size.variable.zh-CN", 11);
pref("font.size.variable.zh-TW", 11);
pref("font.size.variable.zh-HK", 11);
pref("font.size.variable.tr", 11);
pref("font.size.variable.x-unicode", 11);
pref("font.size.variable.x-western", 11);


// External protocols
pref("network.protocol-handler.external.news", true);
pref("network.protocol-handler.external.mailto", true);
pref("network.protocol-handler.external.irc", true);
pref("network.protocol-handler.external.webcal", true);

// Allow myportal file: links (moz 1.8a5 up)
user_pref("capability.policy.policynames", "myportalok");
user_pref("capability.policy.default.checkloaduri.enabled", "noAccess");
user_pref("capability.policy.myportalok.sites", "myportal:");
user_pref("capability.policy.myportalok.checkloaduri.enabled", "allAccess");

// disable usless security warnings
pref("security.warn_entering_secure", false);
pref("security.warn_entering_secure.show_once", true);
pref("security.warn_leaving_secure", false);
pref("security.warn_submit_insecure", false);

// For xulrunner
pref("signon.rememberSignons", true);                                           
pref("signon.expireMasterPassword", false);                                     
pref("signon.SignonFileName", "signons.txt");                                   
pref("browser.xul.error_pages.enabled", true);
