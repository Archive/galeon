/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef DOWNLOADER_VIEW_H
#define DOWNLOADER_VIEW_H

#include "galeon-dialog.h"

#include <glib-object.h>
#include <glib.h>

G_BEGIN_DECLS

typedef struct DownloaderView DownloaderView;
typedef struct DownloaderViewClass DownloaderViewClass;

#define TYPE_DOWNLOADER_VIEW             (downloader_view_get_type ())
#define DOWNLOADER_VIEW(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_DOWNLOADER_VIEW, DownloaderView))
#define DOWNLOADER_VIEW_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), DOWNLOADER_VIEW, DownloaderViewClass))
#define IS_DOWNLOADER_VIEW(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_DOWNLOADER_VIEW))
#define IS_DOWNLOADER_VIEW_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), DOWNLOADER_VIEW))

typedef struct DownloaderViewPrivate DownloaderViewPrivate;

#define CONF_DOWNLOADING_KEEP_OPEN "/apps/galeon/Handlers/Downloading/keep_open"

typedef struct _Download	Download;
typedef struct _DownloadClass	DownloadClass;

struct _DownloadClass
{
	GObjectClass parent;

	/* signals */
	void (* cancel) (Download *self);
	void (* pause)  (Download *self);
	void (* resume) (Download *self);
};

void download_set_progress (Download	*self,
		            guint	 elapsed,
			    guint	 remaining,
			    guint64	 speed,
			    guint64	 size_done,
			    guint64	 size_total);

void download_set_can_pause  (Download *self, gboolean can_pause);
void download_set_size_total (Download *self, guint64 size_total);

void download_paused	(Download *self);
void download_resumed	(Download *self);
void download_completed	(Download *self);
void download_cancelled	(Download *self);

struct DownloaderView
{
        GaleonDialog parent;
        DownloaderViewPrivate *priv;
};

struct DownloaderViewClass
{
        GaleonDialogClass parent_class;
};

GType           downloader_view_get_type              (void);

DownloaderView *downloader_view_new                   (void);

Download       *downloader_view_add_download          (DownloaderView *dv,
						       const char *source,
						       const char *dest);

G_END_DECLS

#endif
