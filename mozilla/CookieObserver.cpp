/*
 *  Copyright (C) 2003 Philip Langdale
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "CookieObserver.h"

#include "galeon-embed-shell.h"

NS_IMPL_ISUPPORTS1(GCookieObserver, nsIObserver);

/* void observe (in nsISupports aSubject, in string aTopic, in wstring aData); */
NS_IMETHODIMP GCookieObserver::Observe(nsISupports *aSubject,
				       const char *aTopic,
				       const PRUnichar *aData)
{

	g_signal_emit_by_name (embed_shell, "permission_changed");

	return NS_OK;
}
