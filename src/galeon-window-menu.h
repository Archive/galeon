/*
 *  Copyright (C) 2003  Ricardo Fernández Pascual
 *  Copyright (C) 2003  Philip Langdale
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _galeon_window_menu_h
#define _galeon_window_menu_h

#include <glib-object.h>
#include "galeon-window.h"

/* object forward declarations */

typedef struct _GaleonWindowMenu GaleonWindowMenu;
typedef struct _GaleonWindowMenuClass GaleonWindowMenuClass;
typedef struct _GaleonWindowMenuPrivate GaleonWindowMenuPrivate;

/**
 * GaleonCssMenu object
 */

#define GALEON_TYPE_WINDOW_MENU		(galeon_window_menu_get_type())

#define GALEON_WINDOW_MENU(object)	\
	(G_TYPE_CHECK_INSTANCE_CAST((object), \
				    GALEON_TYPE_WINDOW_MENU,\
				    GaleonWindowMenu))

#define GALEON_WINDOW_MENU_CLASS(klass)	\
	(G_TYPE_CHECK_CLASS_CAST((klass), \
				 GALEON_TYPE_WINDOW_MENU,\
				 GaleonWindowMenuClass))

#define GALEON_IS_WINDOW_MENU(object)	\
	(G_TYPE_CHECK_INSTANCE_TYPE((object), GALEON_TYPE_WINDOW_MENU))

#define GALEON_IS_WINDOW_MENU_ITEM_CLASS(klass) \
 	(G_TYPE_CHECK_CLASS_TYPE((klass), GALEON_TYPE_WINDOW_MENU))

#define GALEON_WINDOW_MENU_GET_CLASS(obj)	\
	(G_TYPE_INSTANCE_GET_CLASS((obj), \
				   GALEON_TYPE_WINDOW_MENU,\
				   GaleonWindowMenuClass))

struct _GaleonWindowMenuClass
{
	GObjectClass parent_class;
};

/* Remember: fields are public read-only */
struct _GaleonWindowMenu
{
	GObject parent_object;
	
	GaleonWindowMenuPrivate *priv;
};

GType		galeon_window_menu_get_type			(void);
GaleonWindowMenu *galeon_window_menu_new			(GaleonWindow *window);

#endif
