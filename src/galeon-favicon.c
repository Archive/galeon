/*
 *  Copyright (C) 2002 Jorn Baayen <jorn@nl.linux.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtkwidget.h>
#include <gtk/gtktoolbar.h>
#include <gtk/gtkstock.h>

#include "galeon-favicon.h"
#include "galeon-shell.h"


#define GALEON_FAVICON_GET_PRIVATE(object) (G_TYPE_INSTANCE_GET_PRIVATE ((object), \
				       GALEON_TYPE_FAVICON, GaleonFaviconPrivate))


struct GaleonFaviconPrivate
{
	GaleonFaviconCache *cache;
	GaleonFaviconCacheEntry *entry;

	char *url;
};

enum
{
	PROP_0,
	PROP_CACHE,
	PROP_URL
};

enum
{
	CHANGED,
	LAST_SIGNAL
};

static guint galeon_favicon_signals[LAST_SIGNAL] = { 0 };

G_DEFINE_TYPE (GaleonFavicon, galeon_favicon, GTK_TYPE_IMAGE);

static void
galeon_favicon_update_image (GaleonFavicon *favicon)
{
	GdkPixbuf *pixbuf = NULL;

	if (favicon->priv->entry)
	{
		pixbuf = galeon_favicon_cache_entry_get_pixbuf (favicon->priv->entry);
	}

	if (!pixbuf)
	{
		gtk_image_set_from_stock (GTK_IMAGE (favicon),
					  GTK_STOCK_JUMP_TO,
					  GTK_ICON_SIZE_MENU);
	}
	else
	{
		gtk_image_set_from_pixbuf (GTK_IMAGE (favicon), pixbuf);
	}

	g_signal_emit (G_OBJECT (favicon), galeon_favicon_signals[CHANGED], 0);
}	

static void
cache_image_changed_cb (GaleonFaviconCacheEntry *entry, GParamSpec   *pspec,
			GaleonFavicon* favicon)
{
	galeon_favicon_update_image (favicon);	
}

void
galeon_favicon_set_url (GaleonFavicon *favicon,
		        const char *url)
{
	g_return_if_fail (GALEON_IS_FAVICON (favicon));

	g_free (favicon->priv->url);
	favicon->priv->url = g_strdup (url);

	if (favicon->priv->entry)
	{
		g_signal_handlers_disconnect_by_func (favicon->priv->entry, 
						      G_CALLBACK (cache_image_changed_cb),
						      favicon);

		g_object_unref (favicon->priv->entry);
		favicon->priv->entry = NULL;
	}

	if (favicon->priv->url)
	{
		favicon->priv->entry = 
			galeon_favicon_cache_lookup (favicon->priv->cache,
						     favicon->priv->url);

		if (favicon->priv->entry)
		{
			g_signal_connect (favicon->priv->entry,
					  "notify::pixbuf",
					  G_CALLBACK (cache_image_changed_cb),
					  favicon);
		}

	}

	g_object_notify (G_OBJECT (favicon), "url");
	galeon_favicon_update_image (favicon);
}

char *
galeon_favicon_get_url (GaleonFavicon *favicon)
{
	g_return_val_if_fail (GALEON_IS_FAVICON (favicon), NULL);
	
	return g_strdup (favicon->priv->url);
}

gboolean
galeon_favicon_get_is_default (GaleonFavicon *favicon)
{
	g_return_val_if_fail (GALEON_IS_FAVICON (favicon), FALSE);

	if (favicon->priv->entry &&
	    galeon_favicon_cache_entry_get_pixbuf (favicon->priv->entry))
	{
		return FALSE;
	}
	return TRUE;
}

static void
galeon_favicon_set_cache (GaleonFavicon *favicon, GaleonFaviconCache *cache)
{
	favicon->priv->cache = cache;
}

static void
galeon_favicon_set_property (GObject *object,
			     guint prop_id,
			     const GValue *value,
			     GParamSpec *pspec)
{
	GaleonFavicon *favicon = GALEON_FAVICON (object);

	switch (prop_id)
	{
	case PROP_CACHE:
		galeon_favicon_set_cache (favicon, g_value_get_object (value));
		break;
	case PROP_URL:
		galeon_favicon_set_url (favicon, g_value_get_string (value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void 
galeon_favicon_get_property (GObject *object,
			     guint prop_id,
			     GValue *value,
			     GParamSpec *pspec)
{
	GaleonFavicon *favicon = GALEON_FAVICON (object);

	switch (prop_id)
	{
	case PROP_CACHE:
		g_value_set_object (value, favicon->priv->cache);
		break;
	case PROP_URL:
		g_value_set_string (value, favicon->priv->url);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
galeon_favicon_finalize (GObject *object)
{
	GaleonFavicon *favicon = GALEON_FAVICON (object);

	g_free (favicon->priv->url);

	if (favicon->priv->entry)
	{
		g_signal_handlers_disconnect_by_func (favicon->priv->entry, 
						      G_CALLBACK (cache_image_changed_cb),
						      favicon);

		g_object_unref (favicon->priv->entry);
	}

	G_OBJECT_CLASS (galeon_favicon_parent_class)->finalize (object);
}

static void
galeon_favicon_init (GaleonFavicon *ma)
{
	ma->priv = GALEON_FAVICON_GET_PRIVATE (ma);

	gtk_widget_set_size_request (GTK_WIDGET (ma), 16, 16);
}

static void
galeon_favicon_class_init (GaleonFaviconClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = galeon_favicon_finalize;

	object_class->set_property = galeon_favicon_set_property;
	object_class->get_property = galeon_favicon_get_property;

	g_object_class_install_property (object_class,
					 PROP_CACHE,
					 g_param_spec_object ("cache",
							      "Favicon cache",
							      "Favicon cache",
							      GALEON_TYPE_FAVICON_CACHE,
							      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
	g_object_class_install_property (object_class,
					 PROP_URL,
					 g_param_spec_string ("url",
							      "Associated URL",
							      "Associated URL",
							      NULL,
							      G_PARAM_READWRITE));

	galeon_favicon_signals[CHANGED] =
		g_signal_new ("changed",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (GaleonFaviconClass, changed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE,
			      0);

	g_type_class_add_private (klass, sizeof (GaleonFaviconPrivate));
}


GtkWidget *
galeon_favicon_new (const char *url)
{
	GObject *favicon;
	GaleonFaviconCache *cache = galeon_shell_get_favicon_cache (galeon_shell);

	g_return_val_if_fail (GALEON_IS_FAVICON_CACHE (cache), NULL);

	favicon = g_object_new (GALEON_TYPE_FAVICON,
				"cache", cache,
				"url", url,
				NULL);

	return GTK_WIDGET (favicon);
}
