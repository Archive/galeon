/*
 *  Copyright (C) 2004  Tommi Komulainen
 *  Copyright (C) 2004, 2005  Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#ifndef GALEON_FIND_TOOLBAR_H
#define GALEON_FIND_TOOLBAR_H

#include <gtk/gtktoolbar.h>

#include "galeon-window.h"

G_BEGIN_DECLS

#define GALEON_TYPE_FIND_TOOLBAR		(galeon_find_toolbar_get_type ())
#define GALEON_FIND_TOOLBAR(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), GALEON_TYPE_FIND_TOOLBAR, GaleonFindToolbar))
#define GALEON_FIND_TOOLBAR_CLASS(k)	(G_TYPE_CHECK_CLASS_CAST((k), GALEON_TYPE_FIND_TOOLBAR, GaleonFindToolbarClass))
#define GALEON_IS_FIND_TOOLBAR(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), GALEON_TYPE_FIND_TOOLBAR))
#define GALEON_IS_FIND_TOOLBAR_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), GALEON_TYPE_FIND_TOOLBAR))
#define GALEON_FIND_TOOLBAR_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), GALEON_TYPE_FIND_TOOLBAR, GaleonFindToolbarClass))

typedef struct _GaleonFindToolbar		GaleonFindToolbar;
typedef struct _GaleonFindToolbarPrivate	GaleonFindToolbarPrivate;
typedef struct _GaleonFindToolbarClass	GaleonFindToolbarClass;

struct _GaleonFindToolbar
{
	GtkToolbar parent;

	/*< private >*/
	GaleonFindToolbarPrivate *priv;
};

struct _GaleonFindToolbarClass
{
	GtkToolbarClass parent_class;

	/* Signals */
	void (* next)		(GaleonFindToolbar *toolbar);
	void (* previous)	(GaleonFindToolbar *toolbar);
	void (* close)		(GaleonFindToolbar *toolbar);
};

GType		 galeon_find_toolbar_get_type	 (void) G_GNUC_CONST;

GaleonFindToolbar *galeon_find_toolbar_new	 (GaleonWindow *window);

const char	*galeon_find_toolbar_get_text	 (GaleonFindToolbar *toolbar);

void		 galeon_find_toolbar_set_embed	 (GaleonFindToolbar *toolbar,
						  GaleonEmbed *embed);

void		 galeon_find_toolbar_find_next	 (GaleonFindToolbar *toolbar);

void		 galeon_find_toolbar_find_previous (GaleonFindToolbar *toolbar);

void		 galeon_find_toolbar_open	 (GaleonFindToolbar *toolbar,
						  gboolean links_only,
						  gboolean typing_ahead);

void		 galeon_find_toolbar_close	 (GaleonFindToolbar *toolbar);

void		 galeon_find_toolbar_request_close (GaleonFindToolbar *toolbar);

G_END_DECLS

#endif /* GALEON_FIND_TOOLBAR_H */
