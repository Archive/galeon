/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __bookmarks_tb_widget_h
#define __bookmarks_tb_widget_h

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtkstatusbar.h>
#include "bookmarks.h"
#include "bookmarks-util.h"
#include "bookmarks-location-source.h"

/* object forward declarations */

typedef struct _GbTbWidgetClass GbTbWidgetClass;
typedef struct _GbTbWidgetPrivate GbTbWidgetPrivate;

/**
 * GbTbWidget object
 */

#define GB_TYPE_TB_WIDGET		(gb_tb_widget_get_type())
#define GB_TB_WIDGET(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), GB_TYPE_TB_WIDGET,\
					 GbTbWidget))
#define GB_TB_WIDGET_CLASS(klass) 	(G_TYPE_CHECK_CLASS_CAST((klass), GB_TYPE_TB_WIDGET,\
				  	 GbTbWidgetClass))
#define GB_IS_TB_WIDGET(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), GB_TYPE_TB_WIDGET))
#define GB_IS_TB_WIDGET_CLASS(klass) 	(G_TYPE_CHECK_CLASS_TYPE((klass), GB_TYPE_TB_WIDGET))
#define GB_TB_WIDGET_GET_CLASS(obj) 	(G_TYPE_INSTANCE_GET_CLASS((obj), GB_TYPE_TB_WIDGET,\
					 GbTbWidgetClass))

struct _GbTbWidgetClass 
{
	GtkHBoxClass parent_class;
	
	/* virtual and protected */
	void		(*rebuild)		(GbTbWidget *gtw);

	/* signals */
	GbBookmarkActivatedCallback gb_tb_widget_bookmark_activated;

};

/* Remember: fields are public read-only */
struct _GbTbWidget
{
	GtkHBox parent_object;

	GbTbWidgetPrivate *priv;
};

/* this class is abstract */

/* public */
GType			gb_tb_widget_get_type		(void);
GbBookmark *		gb_tb_widget_get_bookmark 	(GbTbWidget *gtw);
void			gb_tb_widget_rebuild		(GbTbWidget *w);
void			gb_tb_widget_set_location_source (GbTbWidget *w, GbLocationSource *src);
GbLocationSource *	gb_tb_widget_get_location_source (GbTbWidget *w);
void			gb_tb_widget_set_statusbar	(GbTbWidget *w, GtkStatusbar *sb);
GtkStatusbar *		gb_tb_widget_get_statusbar	(GbTbWidget *w);

/* protected */
void			gb_tb_widget_setup_context_menu	(GbTbWidget *gtw, GtkWidget *w);

#endif

