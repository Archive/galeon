/*
 *  Copyright (C) 2003 Crispin Flowerday
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mozilla-encodings.h"

#include <glib/gi18n.h>

#define MOZILLA_ENCODINGS_GET_PRIVATE(object) (G_TYPE_INSTANCE_GET_PRIVATE ((object), \
				       MOZILLA_TYPE_ENCODINGS, MozillaEncodingsPrivate))


struct MozillaEncodingsPrivate
{
	gpointer dummy;
};

static GaleonEncodingsClass *parent_class;

static const GaleonEncodingInfo encoding_array[] = { 
	{ N_("Arabic (IBM-864)"),                  "IBM864",                LG_ARABIC },
	{ N_("Arabic (ISO-8859-6)"),               "ISO-8859-6",            LG_ARABIC },
	{ N_("Arabic (MacArabic)"),                "x-mac-arabic",          LG_ARABIC },
	{ N_("Arabic (Windows-1256)"),             "windows-1256",          LG_ARABIC },
	{ N_("Baltic (ISO-8859-13)"),              "ISO-8859-13",           LG_BALTIC },
	{ N_("Baltic (ISO-8859-4)"),               "ISO-8859-4",            LG_BALTIC },
	{ N_("Baltic (Windows-1257)"),             "windows-1257",          LG_BALTIC },
	{ N_("Armenian (ARMSCII-8)"),              "armscii-8",             LG_CAUCASIAN },
	{ N_("Georgian (GEOSTD8)"),                "geostd8",               LG_CAUCASIAN },
	{ N_("Central European (IBM-852)"),        "IBM852",                LG_C_EUROPEAN },
	{ N_("Central European (ISO-8859-2)"),     "ISO-8859-2",	    LG_C_EUROPEAN },
	{ N_("Central European (MacCE)"),          "x-mac-ce",              LG_C_EUROPEAN },
	{ N_("Central European (Windows-1250)"),   "windows-1250",          LG_C_EUROPEAN },
	{ N_("Chinese Simplified (GB18030)"),      "gb18030",               LG_CHINESE_SIMP },
	{ N_("Chinese Simplified (GB2312)"),       "GB2312",                LG_CHINESE_SIMP },
	{ N_("Chinese Simplified (GBK)"),          "x-gbk",                 LG_CHINESE_SIMP },
	{ N_("Chinese Simplified (HZ)"),           "HZ-GB-2312",	    LG_CHINESE_SIMP },
	{ N_("Chinese Simplified (ISO-2022-CN)"),  "ISO-2022-CN",           LG_CHINESE_SIMP },
	{ N_("Chinese Traditional (Big5)"),        "Big5",                  LG_CHINESE_TRAD },
	{ N_("Chinese Traditional (Big5-HKSCS)"),  "Big5-HKSCS",	    LG_CHINESE_TRAD },
	{ N_("Chinese Traditional (EUC-TW)"),      "x-euc-tw",              LG_CHINESE_TRAD },
	{ N_("Cyrillic (IBM-855)"),                "IBM855",                LG_CYRILLIC },
	{ N_("Cyrillic (ISO-8859-5)"),             "ISO-8859-5",	    LG_CYRILLIC },
	{ N_("Cyrillic (ISO-IR-111)"),             "ISO-IR-111",	    LG_CYRILLIC },
	{ N_("Cyrillic (KOI8-R)"),                 "KOI8-R",                LG_CYRILLIC },
	{ N_("Cyrillic (MacCyrillic)"),            "x-mac-cyrillic",        LG_CYRILLIC },
	{ N_("Cyrillic (Windows-1251)"),           "windows-1251",          LG_CYRILLIC },
	{ N_("Cyrillic/Russian (IBM-866)"),        "IBM866",                LG_CYRILLIC },
	{ N_("Greek (ISO-8859-7)"),                "ISO-8859-7",            LG_GREEK },
	{ N_("Greek (MacGreek)"),                  "x-mac-greek",           LG_GREEK },
	{ N_("Greek (Windows-1253)"),              "windows-1253",          LG_GREEK },
	{ N_("Gujarati (MacGujarati)"),            "x-mac-gujarati",        LG_INDIAN },
	{ N_("Gurmukhi (MacGurmukhi)"),            "x-mac-gurmukhi",        LG_INDIAN },
	{ N_("Hindi (MacDevanagari)"),             "x-mac-devanagari",      LG_INDIAN },
	{ N_("Hebrew (IBM-862)"),                  "IBM862",                LG_HEBREW },
	{ N_("Hebrew (ISO-8859-8-I)"),             "ISO-8859-8-I",          LG_HEBREW },
	{ N_("Hebrew (MacHebrew)"),                "x-mac-hebrew",          LG_HEBREW },
	{ N_("Hebrew (Windows-1255)"),             "windows-1255",          LG_HEBREW },
	{ N_("Visual Hebrew (ISO-8859-8)"),        "ISO-8859-8",            LG_HEBREW },
	{ N_("Japanese (EUC-JP)"),                 "EUC-JP",                LG_JAPANESE },
	{ N_("Japanese (ISO-2022-JP)"),            "ISO-2022-JP",           LG_JAPANESE },
	{ N_("Japanese (Shift-JIS)"),              "Shift_JIS",             LG_JAPANESE },
	{ N_("Korean (EUC-KR)"),                   "EUC-KR",                LG_KOREAN },
	{ N_("Korean (ISO-2022-KR)"),              "ISO-2022-KR",           LG_KOREAN },
	{ N_("Korean (JOHAB)"),                    "x-johab",               LG_KOREAN },
	{ N_("Korean (UHC)"),                      "x-windows-949",         LG_KOREAN },
	{ N_("Celtic (ISO-8859-14)"),              "ISO-8859-14",           LG_NORDIC },
	{ N_("Icelandic (MacIcelandic)"),          "x-mac-icelandic",       LG_NORDIC },
	{ N_("Nordic (ISO-8859-10)"),              "ISO-8859-10",           LG_NORDIC },
	{ N_("Persian (MacFarsi)"),                "x-mac-farsi",           LG_PERSIAN },
	{ N_("Croatian (MacCroatian)"),            "x-mac-croatian",        LG_SE_EUROPEAN },
	{ N_("Romanian (MacRomanian)"),            "x-mac-romanian",        LG_SE_EUROPEAN },
	{ N_("Romanian (ISO-8859-16)"),            "ISO-8859-16",           LG_SE_EUROPEAN },
	{ N_("South European (ISO-8859-3)"),	    "ISO-8859-3",           LG_SE_EUROPEAN },
	{ N_("Thai (TIS-620)"),                    "TIS-620",               LG_THAI },
	{ N_("Thai (ISO-8859-11)"),                "iso-8859-11",           LG_THAI },
	{ N_("Thai (Windows-874)"),                "windows-874",           LG_THAI },
	{ N_("Turkish (IBM-857)"),                 "IBM857",                LG_TURKISH },
	{ N_("Turkish (ISO-8859-9)"),              "ISO-8859-9",            LG_TURKISH },
	{ N_("Turkish (MacTurkish)"),              "x-mac-turkish",         LG_TURKISH },
	{ N_("Turkish (Windows-1254)"),            "windows-1254",          LG_TURKISH },
	{ N_("Unicode (UTF-8)"),                   "UTF-8",                 LG_UNICODE },
	{ N_("Cyrillic/Ukrainian (KOI8-U)"),       "KOI8-U",                LG_UKRAINIAN },
	{ N_("Cyrillic/Ukrainian (MacUkrainian)"), "x-mac-ukrainian",       LG_UKRAINIAN },
	{ N_("Vietnamese (TCVN)"),                 "x-viet-tcvn5712",       LG_VIETNAMESE },
	{ N_("Vietnamese (VISCII)"),               "VISCII",                LG_VIETNAMESE },
	{ N_("Vietnamese (VPS)"),                  "x-viet-vps",            LG_VIETNAMESE },
	{ N_("Vietnamese (Windows-1258)"),         "windows-1258",          LG_VIETNAMESE },
	{ N_("Western (IBM-850)"),                 "IBM850",                LG_WESTERN },
	{ N_("Western (ISO-8859-1)"),              "ISO-8859-1",            LG_WESTERN },
	{ N_("Western (ISO-8859-15)"),             "ISO-8859-15",           LG_WESTERN },
	{ N_("Western (MacRoman)"),                "x-mac-roman",           LG_WESTERN },
	{ N_("Western (Windows-1252)"),            "windows-1252",          LG_WESTERN },

	/* the following encodings are so rarely used that we don't want to pollute the "related"
	 * part of the encodings menu with them, so we set the language group to 0 here */
	{ N_("English (US-ASCII)"),                "us-ascii",              0 },
	{ N_("Unicode (UTF-16BE)"),                "UTF-16BE",              0 },
	{ N_("Unicode (UTF-16LE)"),                "UTF-16LE",              0 },
	{ N_("Unicode (UTF-32BE)"),                "UTF-32BE",              0 },
	{ N_("Unicode (UTF-32LE)"),                "UTF-32LE",              0 }
};

static GList *
impl_get_all_encodings (GaleonEncodings *encodings)
{
	GList *list = NULL;
	guint i;
	for (i = 0 ; i < G_N_ELEMENTS (encoding_array); i++)
	{
		list = g_list_prepend (list, (void*)(&encoding_array[i]));
	}
	return list;
}

static void
mozilla_encodings_init (MozillaEncodings *encodings)
{
	encodings->priv = MOZILLA_ENCODINGS_GET_PRIVATE (encodings);
}

static void
mozilla_encodings_finalize (GObject *object)
{
        G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
mozilla_encodings_class_init (MozillaEncodingsClass *klass)
{
	GObjectClass *object_class;
	GaleonEncodingsClass *encodings_class;

	object_class = G_OBJECT_CLASS (klass);
	encodings_class = GALEON_ENCODINGS_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = mozilla_encodings_finalize;
	encodings_class->get_all_encodings = impl_get_all_encodings;

	g_type_class_add_private (klass, sizeof (MozillaEncodingsPrivate));
}

GType
mozilla_encodings_get_type (void)
{
       static GType mozilla_encodings_type = 0;

        if (mozilla_encodings_type == 0)
        {
                static const GTypeInfo our_info =
                {
                        sizeof (MozillaEncodingsClass),
                        NULL, /* base_init */
                        NULL, /* base_finalize */
                        (GClassInitFunc) mozilla_encodings_class_init,
                        NULL, /* class_finalize */
                        NULL, /* class_data */
                        sizeof (MozillaEncodings),
                        0,    /* n_preallocs */
                        (GInstanceInitFunc) mozilla_encodings_init
                };

                mozilla_encodings_type = g_type_register_static (GALEON_TYPE_ENCODINGS,
								 "MozillaEncodings",
								 &our_info, (GTypeFlags)0);
        }
        return mozilla_encodings_type;
}

MozillaEncodings*
mozilla_encodings_new (void)
{
	return MOZILLA_ENCODINGS (g_object_new (MOZILLA_TYPE_ENCODINGS, NULL));
}

