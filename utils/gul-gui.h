/*
 *  Copyright (C) 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GUL_GUI_H
#define GUL_GUI_H

/* system includes */
#include <gtk/gtkmenu.h>
#include <gtk/gtktreeview.h>
#include <gtk/gtkradiobutton.h>
#include <gtk/gtkwindow.h>


G_BEGIN_DECLS

void             gul_gui_sanitise_popup_position (GtkMenu *menu,
						  GtkWidget *widget,
						  gint *x,
						  gint *y);
	
void 		 gul_gui_menu_position_under_widget 	(GtkMenu   *menu,
					  		 gint      *x,
					  		 gint      *y,
					  		 gboolean  *push_in,
					  		 gpointer  user_data);

void             gul_gui_menu_position_tree_selection    (GtkMenu   *menu,
                                                          gint      *x,
                                                          gint      *y,
                                                          gboolean  *push_in,
                                                          gpointer  user_data);

gint 		 gul_gui_gtk_radio_button_get 		(GtkRadioButton *radio_button);

void 		 gul_gui_gtk_radio_button_set 		(GtkRadioButton *radio_button, 
							 gint index);

GList 		*gul_gui_treeview_get_selection_refs 	(GtkTreeView *treeview);

void 		 gul_gui_treeview_free_selection_refs 	(GList *list);

GtkWidget 	*gul_gui_append_new_menuitem 		(GtkWidget  *menu,
			     				 const char *mnemonic,
			     				 GCallback   callback,
			     				 gpointer    data);

GtkWidget 	*gul_gui_append_new_menuitem_stock	(GtkWidget  *menu,
				   			 const char *stock_id,
				   			 GCallback   callback,
				   			 gpointer    data);

GtkWidget	*gul_gui_append_new_menuitem_stock_icon (GtkWidget *menu, 
							 const char *stock_id,
							 const char *mnemonic,
							 GCallback callback,
							 gpointer data);

GtkWidget      *gul_gui_append_new_check_menuitem      (GtkWidget  *menu,
                                                        const char *mnemonic,
                                                        gboolean value,
                                                        GCallback callback,
                                                        gpointer data);

GtkWidget      *gul_gui_append_separator	       (GtkWidget *menu);

gboolean	gul_gui_confirm_overwrite_file	       (GtkWidget *parent,
							const char *filename);

gboolean	gul_gui_widget_hide_now		       (GtkWidget *widget);

void            gul_gui_window_present                 (GtkWindow *widget,
							guint32 user_time);

void		gul_gui_setup_tearoff		       (GtkMenuShell *ms);
void		gul_gui_add_separator_conditional      (GtkMenuShell *ms);
void		gul_gui_remove_separator_conditional   (GtkMenuShell *ms);
void		gul_gui_remove_all_children	       (GtkContainer *c);

void            gul_gui_image_set_from_mime_type       (GtkWidget  *image,
		                                        const char *mime_type,
							GtkIconSize icon_size);


GtkWidget *     gul_gui_image_button_new               (const char *label_text,
							const char *image_stock_id);

void            gul_gui_help                           (GtkWindow* parent,
							const char *file,
							const char *link_id);
void            gul_gui_connect_checkbutton_to_gconf   (GtkWidget *widget,
                                                        const char *pref);
G_END_DECLS

#endif
