/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __bookmarks_single_editor_small_h
#define __bookmarks_single_editor_small_h

#include <glib-object.h>
#include <bookmarks-single-editor.h>

/* object forward declarations */

typedef struct _GbSingleEditorSmall GbSingleEditorSmall;
typedef struct _GbSingleEditorSmallClass GbSingleEditorSmallClass;
typedef struct _GbSingleEditorSmallPrivate GbSingleEditorSmallPrivate;

/**
 * Editor object
 */

#define GB_TYPE_SINGLE_EDITOR_SMALL		(gb_single_editor_small_get_type())
#define GB_SINGLE_EDITOR_SMALL(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), GB_TYPE_SINGLE_EDITOR_SMALL,\
						 GbSingleEditorSmall))
#define GB_SINGLE_EDITOR_SMALL_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), GB_TYPE_SINGLE_EDITOR_SMALL,\
						 GbSingleEditorSmallClass))
#define GB_IS_SINGLE_EDITOR_SMALL(object)	(G_TYPE_CHECK_INSTANCE_TYPE((object), GB_TYPE_SINGLE_EDITOR_SMALL))
#define GB_IS_SINGLE_EDITOR_SMALL_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), GB_TYPE_SINGLE_EDITOR_SMALL))
#define GB_SINGLE_EDITOR_SMALL_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), GB_TYPE_SINGLE_EDITOR_SMALL,\
						 GbSingleEditorSmallClass))

struct _GbSingleEditorSmallClass 
{
	GbSingleEditorClass parent_class;
};

/* Remember: fields are public read-only */
struct _GbSingleEditorSmall
{
	GbSingleEditor parent_object;

	GbSingleEditorSmallPrivate *priv;
};

GType			gb_single_editor_small_get_type		(void);
GbSingleEditorSmall *	gb_single_editor_small_new		(void);
GtkWidget *		gb_single_editor_small_get_widget	(GbSingleEditorSmall *editor);

#endif
