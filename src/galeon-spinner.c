/* 
 * Nautilus
 *
 * Copyright (C) 2000 Eazel, Inc.
 * Copyright (C) 2002 Marco Pesenti Gritti
 *
 * Nautilus is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Andy Hertzfeld <andy@eazel.com>
 *
 * Galeon port by Marco Pesenti Gritti <marco@it.gnome.org>
 * 
 * This is the spinner (for busy feedback) for the location bar
 *
 */

#include "config.h"
#include "galeon-spinner.h"
#include "galeon-config.h"

#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gtk/gtkicontheme.h>
#include <math.h>

#define spinner_DEFAULT_TIMEOUT 100	/* Milliseconds Per Frame */

struct GaleonSpinnerDetails {
	GList	*image_list;

	GdkPixbuf *quiescent_pixbuf;

	GtkIconTheme *icon_theme;
	
	int	max_frame;
	int	delay;
	int	current_frame;	
	guint	timer_task;
	
	gboolean small_mode;

	gboolean button_in;
	gboolean button_down;
};

static void galeon_spinner_class_init (GaleonSpinnerClass *class);
static void galeon_spinner_init (GaleonSpinner *spinner);
static void galeon_spinner_load_images            (GaleonSpinner *spinner);
static void galeon_spinner_unload_images          (GaleonSpinner *spinner);
static void galeon_spinner_remove_update_callback (GaleonSpinner *spinner);


static GObjectClass *parent_class = NULL;

GType 
galeon_spinner_get_type (void)
{
        static GType galeon_spinner_type = 0;

        if (galeon_spinner_type == 0)
        {
                static const GTypeInfo our_info =
                {
                        sizeof (GaleonSpinnerClass),
                        NULL, /* base_init */
                        NULL, /* base_finalize */
                        (GClassInitFunc) galeon_spinner_class_init,
                        NULL,
                        NULL, /* class_data */
                        sizeof (GaleonSpinner),
                        0, /* n_preallocs */
                        (GInstanceInitFunc) galeon_spinner_init
                };

                galeon_spinner_type = g_type_register_static (GTK_TYPE_EVENT_BOX,
                                                              "GaleonSpinner",
                                                              &our_info, 0);
        }

        return galeon_spinner_type;

}

/*
 * galeon_spinner_new:
 *
 * Create a new #GaleonSpinner. The spinner is a widget
 * that gives the user feedback about network status with
 * an animated image.
 *
 * Return Value: the spinner #GtkWidget
 **/
GtkWidget *
galeon_spinner_new (void)
{
        GtkWidget *s;

        s = GTK_WIDGET (g_object_new (GALEON_TYPE_SPINNER, 
				      "visible-window", FALSE, NULL));

        return s;
}

static gboolean
is_throbbing (GaleonSpinner *spinner)
{
	return spinner->details->timer_task != 0;
}

/* loop through all the images taking their union to compute the width and height of the spinner */
static void
get_spinner_dimensions (GaleonSpinner *spinner, int *spinner_width, int* spinner_height)
{
	int current_width, current_height;
	int pixbuf_width, pixbuf_height;
	GList *current_entry;
	GdkPixbuf *pixbuf;
	
	current_width = 0;
	current_height = 0;

	/* start with the quiescent image */
	if (spinner->details->quiescent_pixbuf != NULL) {
		current_width = gdk_pixbuf_get_width (spinner->details->quiescent_pixbuf);
		current_height = gdk_pixbuf_get_height (spinner->details->quiescent_pixbuf);
	}

	/* loop through all the installed images, taking the union */
	current_entry = spinner->details->image_list;
	while (current_entry != NULL) {	
		pixbuf = GDK_PIXBUF (current_entry->data);
		pixbuf_width = gdk_pixbuf_get_width (pixbuf);
		pixbuf_height = gdk_pixbuf_get_height (pixbuf);
		
		if (pixbuf_width > current_width) {
			current_width = pixbuf_width;
		}
		
		if (pixbuf_height > current_height) {
			current_height = pixbuf_height;
		}
		
		current_entry = current_entry->next;
	}
		
	/* return the result */
	*spinner_width = current_width;
	*spinner_height = current_height;
}

/* handler for handling theme changes */
static void
galeon_spinner_theme_changed (GtkIconTheme *icon_theme,
		              GaleonSpinner  *spinner)
{
	gtk_widget_hide (GTK_WIDGET (spinner));
	galeon_spinner_load_images (spinner);
	gtk_widget_show (GTK_WIDGET (spinner));	
	gtk_widget_queue_resize (GTK_WIDGET (spinner));
}

static void
galeon_spinner_init (GaleonSpinner *spinner)
{
	GtkWidget *widget = GTK_WIDGET (spinner);
	
	gtk_widget_set_events (widget, 
			       gtk_widget_get_events (widget)
			       | GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK
			       | GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK);
	
	spinner->details = g_new0 (GaleonSpinnerDetails, 1);
	
	spinner->details->delay = spinner_DEFAULT_TIMEOUT;		
	spinner->details->icon_theme = gtk_icon_theme_get_default ();
	g_signal_connect (spinner->details->icon_theme, "changed",
			  G_CALLBACK (galeon_spinner_theme_changed), spinner);
	
	galeon_spinner_load_images (spinner);
	gtk_widget_show (widget);
}

/* here's the routine that selects the image to draw, based on the spinner's state */

static GdkPixbuf *
select_spinner_image (GaleonSpinner *spinner)
{
	GList *element;

	if (spinner->details->timer_task == 0) {
		return (spinner->details->quiescent_pixbuf != NULL) ?
		        g_object_ref (spinner->details->quiescent_pixbuf) : NULL;
	}
	
	if (spinner->details->image_list == NULL) {
		return NULL;
	}

	element = g_list_nth (spinner->details->image_list, spinner->details->current_frame);
	g_return_val_if_fail (element, NULL);
	
	return g_object_ref (element->data);
}

static guchar
lighten_component (guchar cur_value)
{
        int new_value = cur_value;
        new_value += 24 + (new_value >> 3);
        if (new_value > 255) {
                new_value = 255;
        }
        return (guchar) new_value;
}

static GdkPixbuf *
create_new_pixbuf (GdkPixbuf *src)
{
        g_return_val_if_fail (gdk_pixbuf_get_colorspace (src) == GDK_COLORSPACE_RGB, NULL);
        g_return_val_if_fail ((!gdk_pixbuf_get_has_alpha (src)
                               && gdk_pixbuf_get_n_channels (src) == 3)
                              || (gdk_pixbuf_get_has_alpha (src)
                                  && gdk_pixbuf_get_n_channels (src) == 4), NULL);

        return gdk_pixbuf_new (gdk_pixbuf_get_colorspace (src),
                               gdk_pixbuf_get_has_alpha (src),
                               gdk_pixbuf_get_bits_per_sample (src),
                               gdk_pixbuf_get_width (src),
                               gdk_pixbuf_get_height (src));
}

static GdkPixbuf *
eel_create_darkened_pixbuf (GdkPixbuf *src, int saturation, int darken)
{
        gint i, j;
        gint width, height, src_row_stride, dest_row_stride;
        gboolean has_alpha;
        guchar *target_pixels, *original_pixels;
        guchar *pixsrc, *pixdest;
        guchar intensity;
        guchar alpha;
        guchar negalpha;
        guchar r, g, b;
        GdkPixbuf *dest;

        g_return_val_if_fail (gdk_pixbuf_get_colorspace (src) == GDK_COLORSPACE_RGB, NULL);
        g_return_val_if_fail ((!gdk_pixbuf_get_has_alpha (src)
                               && gdk_pixbuf_get_n_channels (src) == 3)
                              || (gdk_pixbuf_get_has_alpha (src)
                                  && gdk_pixbuf_get_n_channels (src) == 4), NULL);
        g_return_val_if_fail (gdk_pixbuf_get_bits_per_sample (src) == 8, NULL);

        dest = create_new_pixbuf (src);

        has_alpha = gdk_pixbuf_get_has_alpha (src);
        width = gdk_pixbuf_get_width (src);
        height = gdk_pixbuf_get_height (src);
        dest_row_stride = gdk_pixbuf_get_rowstride (dest);
        src_row_stride = gdk_pixbuf_get_rowstride (src);
        target_pixels = gdk_pixbuf_get_pixels (dest);
        original_pixels = gdk_pixbuf_get_pixels (src);

        for (i = 0; i < height; i++) {
                pixdest = target_pixels + i * dest_row_stride;
                pixsrc = original_pixels + i * src_row_stride;
                for (j = 0; j < width; j++) {
                        r = *pixsrc++;
                        g = *pixsrc++;
                        b = *pixsrc++;
                        intensity = (r * 77 + g * 150 + b * 28) >> 8;
                        negalpha = ((255 - saturation) * darken) >> 8;
                        alpha = (saturation * darken) >> 8;
                        *pixdest++ = (negalpha * intensity + alpha * r) >> 8;
                        *pixdest++ = (negalpha * intensity + alpha * g) >> 8;
                        *pixdest++ = (negalpha * intensity + alpha * b) >> 8;
                        if (has_alpha) {
                                *pixdest++ = *pixsrc++;
                        }
                }
        }
        return dest;
}

static GdkPixbuf *
eel_create_spotlight_pixbuf (GdkPixbuf* src)
{
        GdkPixbuf *dest;
        int i, j;
        int width, height, has_alpha, src_row_stride, dst_row_stride;
        guchar *target_pixels, *original_pixels;
        guchar *pixsrc, *pixdest;

        g_return_val_if_fail (gdk_pixbuf_get_colorspace (src) == GDK_COLORSPACE_RGB, NULL);
        g_return_val_if_fail ((!gdk_pixbuf_get_has_alpha (src)
                               && gdk_pixbuf_get_n_channels (src) == 3)
                              || (gdk_pixbuf_get_has_alpha (src)
                                  && gdk_pixbuf_get_n_channels (src) == 4), NULL);
        g_return_val_if_fail (gdk_pixbuf_get_bits_per_sample (src) == 8, NULL);

        dest = create_new_pixbuf (src);
        
        has_alpha = gdk_pixbuf_get_has_alpha (src);
        width = gdk_pixbuf_get_width (src);
        height = gdk_pixbuf_get_height (src);
        dst_row_stride = gdk_pixbuf_get_rowstride (dest);
        src_row_stride = gdk_pixbuf_get_rowstride (src);
        target_pixels = gdk_pixbuf_get_pixels (dest);
        original_pixels = gdk_pixbuf_get_pixels (src);

        for (i = 0; i < height; i++) {
                pixdest = target_pixels + i * dst_row_stride;
                pixsrc = original_pixels + i * src_row_stride;
                for (j = 0; j < width; j++) {           
                        *pixdest++ = lighten_component (*pixsrc++);
                        *pixdest++ = lighten_component (*pixsrc++);
                        *pixdest++ = lighten_component (*pixsrc++);
                        if (has_alpha) {
                                *pixdest++ = *pixsrc++;
                        }
                }
        }
        return dest;
}

/* handle expose events */

static int
galeon_spinner_expose (GtkWidget *widget, GdkEventExpose *event)
{
	GaleonSpinner *spinner;
	GdkPixbuf *pixbuf, *massaged_pixbuf;
	GdkGC *gc;
	int x_offset, y_offset, width, height;
	GdkRectangle pix_area, dest;

	g_return_val_if_fail (GALEON_IS_SPINNER (widget), FALSE);

	if (! gtk_widget_is_drawable (widget)) return FALSE;
	spinner = GALEON_SPINNER (widget);

	pixbuf = select_spinner_image (spinner);
	if (pixbuf == NULL) {
		return FALSE;
	}

	/* Get the right tint on the image */
	
	if (spinner->details->button_in) {
		if (spinner->details->button_down) {
			massaged_pixbuf = eel_create_darkened_pixbuf (pixbuf, 0.8 * 255, 0.8 * 255);
		} else {
			massaged_pixbuf = eel_create_spotlight_pixbuf (pixbuf);
		}
		g_object_unref (pixbuf);
		pixbuf = massaged_pixbuf;
	}

	width = gdk_pixbuf_get_width (pixbuf);
	height = gdk_pixbuf_get_height (pixbuf);

	/* Compute the offsets for the image centered on our allocation */
	x_offset = (widget->allocation.width - width) / 2;
	y_offset = (widget->allocation.height - height) / 2;

	pix_area.x = x_offset + widget->allocation.x;
	pix_area.y = y_offset + widget->allocation.y; 
	pix_area.width = width;
	pix_area.height = height;

	if (!gdk_rectangle_intersect (&event->area, &pix_area, &dest)) {
		g_object_unref (pixbuf);
		return FALSE;
	}
	
	gc = gdk_gc_new (widget->window);
	gdk_draw_pixbuf (widget->window, gc, pixbuf,
			 dest.x - x_offset - widget->allocation.x,
			 dest.y - y_offset - widget->allocation.y,
			 dest.x, dest.y,
			 dest.width, dest.height,
			 GDK_RGB_DITHER_MAX, 0, 0);
	g_object_unref (gc);

	g_object_unref (pixbuf);

	return FALSE;
}

/* here's the actual timeout task to bump the frame and schedule a redraw */

static gboolean 
bump_spinner_frame (gpointer callback_data)
{
	GaleonSpinner *spinner;

	spinner = GALEON_SPINNER (callback_data);
	if (! gtk_widget_is_drawable (GTK_WIDGET (spinner))) return TRUE;

	spinner->details->current_frame += 1;
	if (spinner->details->current_frame > spinner->details->max_frame - 1) {
		spinner->details->current_frame = 0;
	}

	gtk_widget_queue_draw (GTK_WIDGET (spinner));
	return TRUE;
}

/**
 * galeon_spinner_start:
 * @spinner: a #GaleonSpinner
 *
 * Start the spinner animation.
 **/
void
galeon_spinner_start (GaleonSpinner *spinner)
{
	if (is_throbbing (spinner)) {
		return;
	}

	if (spinner->details->timer_task != 0) {
		g_source_remove (spinner->details->timer_task);
	}
	
	/* reset the frame count */
	spinner->details->current_frame = 0;
	spinner->details->timer_task = g_timeout_add (spinner->details->delay,
						      bump_spinner_frame,
						      spinner);
}

static void
galeon_spinner_remove_update_callback (GaleonSpinner *spinner)
{
	if (spinner->details->timer_task != 0) {
		g_source_remove (spinner->details->timer_task);
	}
	
	spinner->details->timer_task = 0;
}

/**
 * galeon_spinner_stop:
 * @spinner: a #GaleonSpinner
 *
 * Stop the spinner animation.
 **/
void
galeon_spinner_stop (GaleonSpinner *spinner)
{
	if (!is_throbbing (spinner)) {
		return;
	}

	galeon_spinner_remove_update_callback (spinner);
	gtk_widget_queue_draw (GTK_WIDGET (spinner));

}

/* routines to load the images used to draw the spinner */

/* unload all the images, and the list itself */

static void
galeon_spinner_unload_images (GaleonSpinner *spinner)
{
	GList *current_entry;

	if (spinner->details->quiescent_pixbuf != NULL) {
		g_object_unref (spinner->details->quiescent_pixbuf);
		spinner->details->quiescent_pixbuf = NULL;
	}

	/* unref all the images in the list, and then let go of the list itself */
	current_entry = spinner->details->image_list;
	while (current_entry != NULL) {
		g_object_unref (current_entry->data);
		current_entry = current_entry->next;
	}
	
	g_list_free (spinner->details->image_list);
	spinner->details->image_list = NULL;
}

static GdkPixbuf *
scale_to_real_size (GdkPixbuf *pixbuf, int wanted_size)
{
	GdkPixbuf *result;
	int        size;

	size = gdk_pixbuf_get_width (pixbuf);

	if (wanted_size != size) {
		result = gdk_pixbuf_scale_simple (pixbuf,
						  wanted_size, wanted_size,
						  GDK_INTERP_BILINEAR);
	} else {
		result = g_object_ref (pixbuf);
	}

	return result;
}

static GdkPixbuf *
extract_frame (GdkPixbuf *grid_pixbuf, int x, int y, int size, int wanted_size)
{
	GdkPixbuf *pixbuf, *frame;

	pixbuf = gdk_pixbuf_new_subpixbuf (grid_pixbuf, x, y, size, size);
	g_return_val_if_fail (pixbuf != NULL, NULL);

	frame = scale_to_real_size (pixbuf, wanted_size);
	g_object_unref (pixbuf);

	return frame;
}

/* load all of the images of the spinner sequentially */
static void
galeon_spinner_load_images (GaleonSpinner *spinner)
{
	const char *path;
	GdkPixbuf *pixbuf;
	int        grid_width, grid_height;
	int        wanted_size, size;
	int        x, y;
	GList     *image_list;
	GtkIconInfo *icon_info;
	
	galeon_spinner_unload_images (spinner);
	
	/* FIXME: should lookup the sizes for GTK_ICON_SIZE_SMALL_TOOLBAR and
	 * GTK_ICON_SIZE_LARGE_TOOLBAR, but most of the time 24 and 32 should
	 * be close enough...
	 */
	wanted_size = spinner->details->small_mode ? 24 : 32;

#if 0
	/* Get the size of the toolbar */
	gtk_icon_size_lookup (spinner->details->small_mode ? 
			      GTK_ICON_SIZE_SMALL_TOOLBAR : 
			      GTK_ICON_SIZE_LARGE_TOOLBAR,
			      &wanted_size, &wanted_size);
#endif

	icon_info = gtk_icon_theme_lookup_icon (spinner->details->icon_theme,
						"gnome-spinner", 
						wanted_size, 0);
	if (icon_info == NULL) {
		g_warning ("Spinner animation not found");
		return;
	}

	size = gtk_icon_info_get_base_size (icon_info);
	path = gtk_icon_info_get_filename (icon_info);
	g_return_if_fail (path != NULL);

	pixbuf = gdk_pixbuf_new_from_file (path, NULL);
	gtk_icon_info_free (icon_info);

	grid_width  = gdk_pixbuf_get_width (pixbuf);
	grid_height = gdk_pixbuf_get_height (pixbuf);

	image_list = NULL;
	for (y = 0; y <= grid_height - size; y += size) {
		for (x = 0; x <= grid_width - size; x += size) {
			GdkPixbuf *frame;

			frame = extract_frame (pixbuf, x, y, size, wanted_size);
			if (frame != NULL) {
				image_list = g_list_prepend (image_list, frame);
			} else {
				g_warning ("Couldn't extract frame from grid");
			}
		}
	}
	spinner->details->max_frame  = g_list_length (image_list);
	spinner->details->image_list = g_list_reverse (image_list);

	g_object_unref (pixbuf);

	pixbuf = gtk_icon_theme_load_icon (spinner->details->icon_theme,
					   "gnome-spinner-rest", 
					   wanted_size, 0, NULL);

	if (pixbuf == NULL) {
        	pixbuf = gdk_pixbuf_new_from_file (DATADIR "/pixmaps/galeon.png", NULL);
	}

	spinner->details->quiescent_pixbuf = 
		scale_to_real_size (pixbuf, wanted_size);

	g_object_unref (pixbuf);
}

static gboolean
galeon_spinner_enter_notify_event (GtkWidget *widget, GdkEventCrossing *event)
{
	GaleonSpinner *spinner;

	spinner = GALEON_SPINNER (widget);

	if (!spinner->details->button_in) {
		spinner->details->button_in = TRUE;
		gtk_widget_queue_draw (widget);
	}

	return FALSE;
}

static gboolean
galeon_spinner_leave_notify_event (GtkWidget *widget, GdkEventCrossing *event)
{
	GaleonSpinner *spinner;

	spinner = GALEON_SPINNER (widget);

	if (spinner->details->button_in) {
		spinner->details->button_in = FALSE;
		gtk_widget_queue_draw (widget);
	}

	return FALSE;
}

/* handle button presses by posting a change on the "location" property */

static gboolean
galeon_spinner_button_press_event (GtkWidget *widget, GdkEventButton *event)
{
	GaleonSpinner *spinner;

	spinner = GALEON_SPINNER (widget);

	if (event->button == 1) {
		spinner->details->button_down = TRUE;
		spinner->details->button_in = TRUE;
		gtk_widget_queue_draw (widget);
		return TRUE;
	}

	return FALSE;
}

static void
galeon_spinner_set_location (GaleonSpinner *spinner)
{
}

static gboolean
galeon_spinner_button_release_event (GtkWidget *widget, GdkEventButton *event)
{	
	GaleonSpinner *spinner;
	
	spinner = GALEON_SPINNER (widget);

	if (event->button == 1) {
		if (spinner->details->button_in) {
			galeon_spinner_set_location (spinner);
		}
		spinner->details->button_down = FALSE;
		gtk_widget_queue_draw (widget);
		return TRUE;
	}

	return FALSE;
}

/*
 * galeon_spinner_set_small_mode:
 * @spinner: a #GaleonSpinner
 * @new_mode: pass true to enable the small mode, false to disable
 *
 * Set the size mode of the spinner. We need a small mode to deal
 * with only icons toolbars.
 **/
void
galeon_spinner_set_small_mode (GaleonSpinner *spinner, gboolean new_mode)
{
	if (new_mode != spinner->details->small_mode) {
		spinner->details->small_mode = new_mode;
		galeon_spinner_load_images (spinner);

		gtk_widget_queue_resize (GTK_WIDGET (spinner));
	}
}

/* handle setting the size */

static void
galeon_spinner_size_request (GtkWidget *widget, GtkRequisition *requisition)
{
	int spinner_width, spinner_height;
	GaleonSpinner *spinner = GALEON_SPINNER (widget);

	get_spinner_dimensions (spinner, &spinner_width, &spinner_height);
	
	/* allocate some extra margin so we don't butt up against toolbar edges */
	requisition->width = spinner_width + 8;
   	requisition->height = spinner_height;
}

static void
galeon_spinner_finalize (GObject *object)
{
	GaleonSpinner *spinner;

	spinner = GALEON_SPINNER (object);

	galeon_spinner_remove_update_callback (spinner);
	galeon_spinner_unload_images (spinner);
	
	g_signal_handlers_disconnect_matched (spinner->details->icon_theme,
					      G_SIGNAL_MATCH_DATA, 
					      0, 0, NULL, NULL, object);

	g_free (spinner->details);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
galeon_spinner_class_init (GaleonSpinnerClass *class)
{
	GtkWidgetClass *widget_class;

	parent_class = g_type_class_peek_parent (class);
	widget_class = GTK_WIDGET_CLASS (class);
	
	G_OBJECT_CLASS (class)->finalize = galeon_spinner_finalize;

	widget_class->expose_event = galeon_spinner_expose;
	widget_class->button_press_event = galeon_spinner_button_press_event;
	widget_class->button_release_event = galeon_spinner_button_release_event;
	widget_class->enter_notify_event = galeon_spinner_enter_notify_event;
	widget_class->leave_notify_event = galeon_spinner_leave_notify_event;
	widget_class->size_request = galeon_spinner_size_request;	
}

