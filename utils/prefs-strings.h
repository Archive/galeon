/* General */
#define CONF_GENERAL_HOMEPAGE "/apps/galeon/Browsing/General/startpage"
#define CONF_GENERAL_ALWAYS_SAVE_SESSION "/apps/galeon/Browsing/General/always_save_session"
#define CONF_GENERAL_NEWPAGE_TYPE "/apps/galeon/Browsing/General/newpage_type"
#define CONF_GENERAL_CONFIRM_CLOSE "/apps/galeon/Browsing/General/confirm_close"
#define CONF_GENERAL_OFFLINE "/apps/galeon/Browsing/General/offline"

/* Bookmarks */
#define CONF_BOOKMARKS_HISTORY "/apps/galeon/Browsing/Bookmarks/smart_bm_history"
#define CONF_BOOKMARKS_SMARTBOOKMARK_HIDE_ARROWS "/apps/galeon/Browsing/Bookmarks/smartbookmarks_hide_arrows"
#define CONF_BOOKMARKS_EDITOR_SPLIT_VIEW "/apps/galeon/Browsing/Bookmarks/Editor/split_view"
#define CONF_BOOKMARKS_EDITOR_EDIT_PANE "/apps/galeon/Browsing/Bookmarks/Editor/edit_pane"
#define CONF_BOOKMARKS_MANY_BOOKMARKS_WARNING_THRESHOLD "/apps/galeon/Browsing/Bookmarks/many_bookmarks_warning_threshold"

/* Autocompletion */
#define CONF_HISTORY_AUTOCOMP_ENABLE "/apps/galeon/Browsing/History/completion_enabled"
#define CONF_COMPLETION_SHOW_LIST_AUTO "/apps/galeon/Browsing/History/completion_show_list_auto"
#define CONF_COMPLETION_SHOW_TITLES "/apps/galeon/Browsing/History/completion_show_titles"

/* Tabs */
#define CONF_TABS_TABBED "/apps/galeon/UI/Tabs/tabbed"
#define CONF_TABS_TABBED_POPUPS "/apps/galeon/UI/Tabs/tabbed_popups"
#define CONF_TABS_TABBED_AUTOJUMP "/apps/galeon/UI/Tabs/tabbed_autojump"
#define CONF_TABS_TABBED_LOADING_COLOR "/apps/galeon/UI/Tabs/tabbed_loading_color"
#define CONF_TABS_TABBED_NEW_COLOR "/apps/galeon/UI/Tabs/tabbed_new_color"
#define CONF_TABS_TABBED_ALWAYS_SHOW "/apps/galeon/UI/Tabs/tabbed_always_show"
#define CONF_TABS_TABBED_EDGE "/apps/galeon/UI/Tabs/tabbed_position"
#define CONF_TABS_FAVICON "/apps/galeon/UI/Tabs/favicons_in_tabs"

/* Window appeareance */
#define CONF_WINDOWS_FS_SHOW_SIDEBAR "/apps/galeon/UI/Windows/show_sidebar_in_fullscreen"
#define CONF_WINDOWS_FS_SHOW_MENUBAR "/apps/galeon/UI/Windows/show_menubar_in_fullscreen"
#define CONF_WINDOWS_FS_SHOW_TOOLBARS "/apps/galeon/UI/Windows/show_toolbars_in_fullscreen"
#define CONF_WINDOWS_FS_SHOW_BOOKMARKS "/apps/galeon/UI/Windows/show_bookmarks_in_fullscreen"
#define CONF_WINDOWS_FS_SHOW_STATUSBAR "/apps/galeon/UI/Windows/show_statusbar_in_fullscreen"
#define CONF_WINDOWS_SHOW_SIDEBAR "/apps/galeon/UI/Windows/show_sidebar"
#define CONF_WINDOWS_SHOW_MENUBAR "/apps/galeon/UI/Windows/show_menubar"
#define CONF_WINDOWS_SHOW_TOOLBARS "/apps/galeon/UI/Windows/show_toolbars"
#define CONF_WINDOWS_SHOW_BOOKMARKS "/apps/galeon/UI/Windows/show_bookmarks"
#define CONF_WINDOWS_SHOW_STATUSBAR "/apps/galeon/UI/Windows/show_statusbar"
#define CONF_TOOLBAR_STYLE "/apps/galeon/UI/Toolbar/toolbar_override_style"

/* External apps */
#define CONF_DOWNLOADING_ASK_DIR "/apps/galeon/Handlers/Downloading/ask_for_download_dir"
#define CONF_DOWNLOADING_SHOW_DETAILS "/apps/galeon/Handlers/Downloading/show_details"
#define CONF_DOWNLOADING_EXTERNAL_DOWNLOADER "/apps/galeon/Handlers/Downloading/external_downloader"
#define CONF_DOWNLOADING_EXTERNAL_COMMAND "/apps/galeon/Handlers/Downloading/external_download_command"
#define CONF_DOWNLOADING_EXTERNAL_TERMINAL "/apps/galeon/Handlers/Downloading/external_download_terminal"
#define CONF_DOWNLOADING_DIR "/apps/galeon/Handlers/Downloading/download_dir"

/* State */ 
#define CONF_STATE_LAST_DOWNLOAD_DIR "/apps/galeon/State/last_download_dir"
#define CONF_STATE_LAST_UPLOAD_DIR "/apps/galeon/State/last_upload_dir"
#define CONF_STATE_OPEN_DIR           "/apps/galeon/State/opendir"

/* mouse */
#define CONF_MOUSE_MIDDLE_BUTTON_ACTION "/apps/galeon/UI/Mouse/middle_button_action"
#define CONF_MOUSE_RIGHT_BUTTON_ACTION "/apps/galeon/UI/Mouse/right_button_action"

/* System prefs */
#define CONF_DESKTOP_FTP_HANDLER "/desktop/gnome/url-handlers/ftp/command"	

#define CONF_DESKTOP_TOOLBAR_STYLE "/desktop/gnome/interface/toolbar_style"
