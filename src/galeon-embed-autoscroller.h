/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __galeon_embed_autoscroller_h
#define __galeon_embed_autoscroller_h

#include <gtk/gtkwidget.h>
#include <glib-object.h>
#include "galeon-embed.h"


#ifdef __cplusplus
extern "C" {
#endif


/* object forward declarations */

typedef struct _GaleonEmbedAutoscroller GaleonEmbedAutoscroller;
typedef struct _GaleonEmbedAutoscrollerClass GaleonEmbedAutoscrollerClass;
typedef struct _GaleonEmbedAutoscrollerPrivate GaleonEmbedAutoscrollerPrivate;

/**
 * EmbedAutoscroller object
 */

#define GALEON_TYPE_EMBED_AUTOSCROLLER			(galeon_embed_autoscroller_get_type())
#define GALEON_EMBED_AUTOSCROLLER(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), \
							 GALEON_TYPE_EMBED_AUTOSCROLLER,\
							 GaleonEmbedAutoscroller))
#define GALEON_EMBED_AUTOSCROLLER_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), \
							 GALEON_TYPE_EMBED_AUTOSCROLLER,\
							 GaleonEmbedAutoscrollerClass))
#define GALEON_IS_EMBED_AUTOSCROLLER(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), \
							 GALEON_TYPE_EMBED_AUTOSCROLLER))
#define GALEON_IS_EMBED_AUTOSCROLLER_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), \
							 GALEON_TYPE_EMBED_AUTOSCROLLER))
#define GALEON_EMBED_AUTOSCROLLER_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), \
							 GALEON_TYPE_EMBED_AUTOSCROLLER,\
							 GaleonEmbedAutoscrollerClass))

struct _GaleonEmbedAutoscrollerClass 
{
	GObjectClass parent_class;
};

struct _GaleonEmbedAutoscroller
{
	GObject parent_object;
	GaleonEmbedAutoscrollerPrivate *priv;
};

GType				galeon_embed_autoscroller_get_type	(void);
GaleonEmbedAutoscroller *	galeon_embed_autoscroller_new		(void);
void				galeon_embed_autoscroller_set_embed	(GaleonEmbedAutoscroller *as,
									 GaleonEmbed *embed);
void				galeon_embed_autoscroller_start_scroll	(GaleonEmbedAutoscroller *as,
									 GtkWidget *widget, 
									 gint x, gint y);

#ifdef __cplusplus
}
#endif

#endif
