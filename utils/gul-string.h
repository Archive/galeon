/*
 *  Copyright (C) 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GUL_STRING_H
#define GUL_STRING_H

#include <glib.h>

G_BEGIN_DECLS

char *          gul_string_double_underscores   (const char *string);

gchar  		*gul_string_new_num_accel 	(gint num, 
						 gchar *text, 
						 gboolean lettersok);

gchar  		*gul_string_strip_uline_accel 	(const gchar *text);

gchar 		*gul_string_shorten 		(const gchar *str, 
						 gint target_length);

const gchar 	*gul_string_ascii_strcasestr 	(const gchar *a, 
						 const gchar *b);

gchar 		*gul_string_strdup_replace 	(const gchar *str, 
						 const gchar *a,
				   		 const gchar *b);

void 		 gul_string_store_time_in_string (GTime t, 
						  gchar *str);

gchar 		*gul_string_strip_newline 	(const gchar *c);

gchar 		*gul_string_remove_outside_whitespace (const gchar *str);

gchar 		*gul_string_expand_home_dir 	(const gchar *str);

int		 gul_strcasecmp 		(const char *string_a, 
						 const char *string_b);

int		 gul_strcasecmp_compare_func    (gconstpointer string_a, 
						 gconstpointer string_b);

gchar 	       **gul_strsplit_with_quotes	(const gchar  *string,
						 const gchar  *delimiter,
						 gint max_tokens,
						 const gchar *quotes);
gchar 	       **gul_strsplit_multiple_delimiters_with_quotes (const gchar *string,
							       const gchar *delimiters,
							       gint max_tokens,
							       const gchar *quotes);

G_END_DECLS

#endif
