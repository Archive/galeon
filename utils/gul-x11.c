/*
 *  Copyright (C) 2004 Crispin Flowerday
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gul-x11.h"

#include <gtk/gtkversion.h>
#include <gdk/gdkx.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>
#include <errno.h>
#include <stdlib.h>

/**
 * gul_x11_get_current_workspace: Get the current workspace
 *
 * Get the currently visible workspace for the #GdkScreen.
 *
 * If the X11 window property isn't found, 0 (the first workspace)
 * is returned.
 */
guint
gul_x11_get_current_workspace (GdkScreen *screen)
{
       GdkWindow *root_win = gdk_screen_get_root_window (screen);
       GdkDisplay *display = gdk_screen_get_display (screen);

       Atom type;
       gint format;
       gulong nitems;
       gulong bytes_after;
       guint *current_desktop;
       guint ret = 0;

       XGetWindowProperty (GDK_DISPLAY_XDISPLAY (display), GDK_WINDOW_XID (root_win),
                           gdk_x11_get_xatom_by_name_for_display (display, "_NET_CURRENT_DESKTOP"),
                           0, G_MAXLONG,
                           False, XA_CARDINAL, &type, &format, &nitems,
                           &bytes_after, (gpointer)&current_desktop);
       if (type == XA_CARDINAL && format == 32 && nitems > 0)
       {
               ret = current_desktop[0];
               XFree (current_desktop);
       }
       return ret;
}

/**
 * gul_x11_get_window_workspace: Get the workspace the window is on
 *
 * This function gets the workspace that the #GtkWindow is visible on,
 * it returns GUL_X11_ALL_WORKSPACES if the window is sticky, or if
 * the window manager doesn support this function
 */
guint
gul_x11_get_window_workspace (GtkWindow *gtkwindow)
{
       GdkWindow *window = GTK_WIDGET (gtkwindow)->window;
       GdkDisplay *display = gdk_drawable_get_display (window);

       Atom type;
       gint format;
       gulong nitems;
       gulong bytes_after;
       guint *workspace;
       guint ret = GUL_X11_ALL_WORKSPACES;

       XGetWindowProperty (GDK_DISPLAY_XDISPLAY (display), GDK_WINDOW_XID (window),
                           gdk_x11_get_xatom_by_name_for_display (display, "_NET_WM_DESKTOP"),
                           0, G_MAXLONG,
                           False, XA_CARDINAL, &type, &format, &nitems,
                           &bytes_after, (gpointer)&workspace);
       if (type == XA_CARDINAL && format == 32 && nitems > 0)
       {
               ret = workspace[0];
               XFree (workspace);
       }
       return ret;
}


/**
 * gul_x11_window_update_user_time:
 * window: A #GtkWindow
 * user_time: The event time that caused the window to be opened
 *
 * Sets the time on the window of the event that caused it to
 * be opened. This is used for focus stealing prevention
 */
void
gul_x11_window_update_user_time (GtkWidget *window,
				 guint32 user_time)
{
	g_return_if_fail (GTK_IS_WIDGET (window));

	/* If the user has gtk 2.6, they will have gdk 2.6, so they
	 * will have this function */
#if GTK_CHECK_VERSION (2,6,0)
	if (user_time != 0)
	{
		gtk_widget_realize (window);
		gdk_x11_window_set_user_time (window->window,
					      user_time);
	}
#endif
}


/**
 * gul_x11_get_startup_id:
 * 
 * Gets the time of the event that caused the program to startup
 *
 * Returns: 0 if no time could be found, or the time.
 */
guint32
gul_x11_get_startup_id (void)
{
	/* adapted from gtk+/gdk/x11/gdkdisplay-x11.c */
	const char *startup_id, *time_str;

	startup_id = g_getenv ("DESKTOP_STARTUP_ID");
	if (startup_id == NULL) return 0;

	/* Find the launch time from the startup_id, if it's there.  Newer spec
	* states that the startup_id is of the form <unique>_TIME<timestamp>
	*/
	time_str = g_strrstr (startup_id, "_TIME");
	if (time_str != NULL)
	{
		gulong value;
		gchar *end;
		errno = 0;
	
		/* Skip past the "_TIME" part */
		time_str += 5;
	
		value = strtoul (time_str, &end, 0);
		if (end != time_str && errno == 0)
		{
			return (guint32) value;
		}
	}

	return 0;
}
