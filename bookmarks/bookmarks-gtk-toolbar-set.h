/*
 *  Copyright (C) 2003  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __bookmarks_gtk_toolbar_set_h
#define __bookmarks_gtk_toolbar_set_h

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtkwidget.h>
#include <gtk/gtkstatusbar.h>
#include "bookmarks.h"
#include "bookmarks-util.h"
#include "bookmarks-location-source.h"

/* object forward declarations */

typedef struct _GbGtkToolbarSet GbGtkToolbarSet;
typedef struct _GbGtkToolbarSetClass GbGtkToolbarSetClass;
typedef struct _GbGtkToolbarSetPrivate GbGtkToolbarSetPrivate;

/**
 * GbGtkToolbarSet object
 */

#define GB_TYPE_GTK_TOOLBAR_SET			(gb_gtk_toolbar_set_get_type())
#define GB_GTK_TOOLBAR_SET(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), GB_TYPE_GTK_TOOLBAR_SET,\
						 GbGtkToolbarSet))
#define GB_GTK_TOOLBAR_SET_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), GB_TYPE_GTK_TOOLBAR_SET,\
						 GbGtkToolbarSetClass))
#define GB_IS_GTK_TOOLBAR_SET(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), GB_TYPE_GTK_TOOLBAR_SET))
#define GB_IS_GTK_TOOLBAR_SET_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), GB_TYPE_GTK_TOOLBAR_SET))
#define GB_GTK_TOOLBAR_SET_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), GB_TYPE_GTK_TOOLBAR_SET,\
						 GbGtkToolbarSetClass))

typedef void (*GbGtkToolbarSetAddFunc) (GbGtkToolbarSet *gtbs, GbFolder *folder, GtkWidget *toolbar, gpointer user_data);
typedef void (*GbGtkToolbarSetRemoveFunc) (GbGtkToolbarSet *gtbs, GbFolder *folder, GtkWidget *toolbar, gpointer user_data);

struct _GbGtkToolbarSetClass
{
	GObjectClass parent_class;

	/* signals */
	GbBookmarkActivatedCallback gb_gtk_toolbar_set_bookmark_activated;

};

/* Remember: fields are public read-only */
struct _GbGtkToolbarSet
{
	GObject parent_object;
	
	GbGtkToolbarSetPrivate *priv;
};

GType			gb_gtk_toolbar_set_get_type			(void);
GbGtkToolbarSet *	gb_gtk_toolbar_set_new				(GbBookmarkSet *set,
									 GbGtkToolbarSetAddFunc add_toolbar,
									 GbGtkToolbarSetRemoveFunc remove_toolbar,
									 gpointer user_data);
void			gb_gtk_toolbar_set_set_visible			(GbGtkToolbarSet *gtjb, gboolean visible);
void			gb_gtk_toolbar_set_set_location_source		(GbGtkToolbarSet *gtbs, GbLocationSource *src);
void			gb_gtk_toolbar_set_set_statusbar 		(GbGtkToolbarSet *gtbs, GtkStatusbar *sb);

#endif
