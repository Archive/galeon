dnl Process this file with autoconf to produce a configure script.
AC_PREREQ(2.52)

AC_INIT(galeon, 2.0.7,
	[http://bugzilla.gnome.org/enter_bug.cgi?product=galeon])

AM_CONFIG_HEADER(config.h)
AC_CONFIG_SRCDIR(configure.in)

AC_CANONICAL_HOST
AC_DEFINE_UNQUOTED([EPHY_HOST],["$host"],[The host])
AC_DEFINE_UNQUOTED([EPHY_HOST_CPU],["$host_cpu"],[The host CPU type])
AC_DEFINE_UNQUOTED([EPHY_HOST_VENDOR],["$host_vendor"],[The host vendor])
AC_DEFINE_UNQUOTED([EPHY_HOST_OS],["$host_os"],[The host OS])
AC_DEFINE_UNQUOTED([EPHY_BUILD_ID],["$(TZ=UTC0 date +'%Y%m%d')"],[The build date])

AM_INIT_AUTOMAKE(AC_PACKAGE_NAME, AC_PACKAGE_VERSION)

dnl put the ACLOCAL flags in the Makefile
ACLOCAL="$ACLOCAL $ACLOCAL_FLAGS"

LIBGLADE_REQUIRED=2.3.1
LIBGNOMEUI_REQUIRED=2.5.2
LIBXML_REQUIRED=2.6.6
GCONF_REQUIRED=2.3.2
GLIB_REQUIRED=2.4.0
GNOME_VFS_REQUIRED=2.0.0
GTK_REQUIRED=2.12.0
SCROLLKEEPER_REQUIRED=0.1.4

dnl Only for gnome-vfs > 2.9.2
GNOME_DESKTOP_REQUIRED=2.9.91

AC_SUBST(LIBGLADE_REQUIRED)
AC_SUBST(LIBGNOMEUI_REQUIRED)
AC_SUBST(LIBXML_REQUIRED)
AC_SUBST(GCONF_REQUIRED)
AC_SUBST(GLIB_REQUIRED)
AC_SUBST(GNOME_VFS_REQUIRED)
AC_SUBST(GTK_REQUIRED)
AC_SUBST(SCROLLKEEPER_REQUIRED)

AC_ENABLE_SHARED(yes)
AC_ENABLE_STATIC(no)

AM_MAINTAINER_MODE
if test "x$enable_maintainer_mode" = "xyes"; then
  AC_DEFINE(MAINTAINER_MODE, 1, [Define to 1 if you wish to enable 'maintainer-only' behavior.])
  disable_werror=no
  enable_debug=yes
  AM_CPPFLAGS="-DG_DISABLE_DEPRECATED -DGDK_DISABLE_DEPRECATED -DGDK_PIXBUF_DISABLE_DEPRECATED -DGCONF_DISABLE_DEPRECATED -DGNOME_VFS_DISABLE_DEPRECATED -DLIBGLADE_DISABLE_DEPRECATED -DPANGO_DISABLE_DEPRECATED -DGTK_DISABLE_DEPRECATED -DGNOME_DISABLE_DEPRECATED"
else
  disable_werror=yes
  AM_CPPFLAGS=
fi
AC_SUBST(AM_CPPFLAGS)

AC_LIBTOOL_DLOPEN
AM_PROG_LIBTOOL

AC_ISC_POSIX
AC_PROG_CC
AC_PROG_CXX
AM_PROG_CC_STDC
AC_HEADER_STDC
AC_PROG_INTLTOOL
AC_PATH_PROG(GLIB_GENMARSHAL, glib-genmarshal)
AC_PATH_PROG(GLIB_MKENUMS, glib-mkenums)
AC_PATH_PROG([DBUS_BINDING_TOOL],[dbus-binding-tool],[no])

GNOME_DEBUG_CHECK

dnl ******************************
dnl Core dependancy checking
dnl ******************************

dnl Check for the > 2.9 gnome_vfs api
AC_MSG_CHECKING([for new gnome-vfs-mime API])
if pkg-config --atleast-version 2.9.2 gnome-vfs-2.0; then
   AC_DEFINE([HAVE_NEW_GNOME_VFS_MIME_API],[1],
	     [Define if you have the new style gnome-vfs mime api (>2.9)])
   result=yes
   gnomedesktop_pkgs="gnome-desktop-2.0 >= $GNOME_DESKTOP_REQUIRED"
else
   result=no
fi
AC_MSG_RESULT([$result])

PKG_CHECK_MODULES(GALEON_DEPENDENCY, \
		  gtk+-2.0 >= $GTK_REQUIRED \
		  gtk+-unix-print-2.0 >= $GTK_REQUIRED \
		  libxml-2.0 >= $LIBXML_REQUIRED \
		  libgnomeui-2.0 >= $LIBGNOMEUI_REQUIRED \
		  libglade-2.0 >= $LIBGLADE_REQUIRED \
		  glib-2.0 >= $GLIB_REQUIRED \
		  gobject-2.0 >= $GLIB_REQUIRED \
		  gdk-pixbuf-2.0 >= $GTK_REQUIRED \
		  gdk-2.0 >= $GTK_REQUIRED \
		  gnome-vfs-2.0 >= $GNOME_VFS_REQUIRED\
		  gnome-vfs-module-2.0 >= $GNOME_VFS_REQUIRED\
		  gconf-2.0 >= $GCONF_REQUIRED \
		  $gnomedesktop_pkgs
		  )
AC_SUBST(GALEON_DEPENDENCY_CFLAGS)
AC_SUBST(GALEON_DEPENDENCY_LIBS)

dnl ################
dnl GtkHTML checking
dnl ################
AC_ARG_ENABLE(gtkhtml, [  --enable-gtkhtml        Enable GtkHTML renderer])
if test "x$enable_gtkhtml" = "xyes"; then
  AC_DEFINE(ENABLE_GTKHTML_EMBED, 1, [Define to 1 if you wish to enable GtkHTML renderer.])
  PKG_CHECK_MODULES(GTKHTML, libgtkhtml-2.0)
fi
AM_CONDITIONAL(ENABLE_GTKHTML_EMBED, test "x$enable_gtkhtml" = "xyes")
AC_SUBST(GTKHTML_CFLAGS)
AC_SUBST(GTKHTML_LIBS)

AC_LANG_PUSH(C++)

dnl hacky check for the g++ headers location
AC_TRY_COMPILE([#include <g++/string>]
	[],
	[],
	[STRING_HEADER="\"g++/string\""])
AC_TRY_COMPILE([#include <g++-2/string>]
	[],
	[],
	[STRING_HEADER="\"g++-2/string\""])
AC_TRY_COMPILE([#include <g++-3/string>]
	[],
	[],
	[STRING_HEADER="\"g++-3/string\""])
if test "x$STRING_HEADER" != "x"; then
  AC_DEFINE_UNQUOTED(STRING_HEADER, $STRING_HEADER, [Define to the location of the g++ string header.])
fi

dnl #########################
dnl User-Agent header
dnl #########################
AC_MSG_CHECKING([User-Agent header to use])

AC_ARG_WITH([useragent-vendor],
        AC_HELP_STRING([--with-useragent-vendor@<:@=User-Agent vendor@:>@],
                [User-Agent header vendor part (default: "Galeon")]),
        useragent_vendor="$with_useragent_vendor",
        useragent_vendor="Galeon")
AC_DEFINE_UNQUOTED(USERAGENT_VENDOR,
        ["$useragent_vendor"],
        [User-Agent header vendor part])
AC_ARG_WITH([useragent-vendorsub],
        AC_HELP_STRING([--with-useragent-vendorsub@<:@=User-Agent vendorSub@:>@],
                [User-Agent header vendorSub part (default: release version)]),
        useragent_vendorsub="$with_useragent_vendorsub",
        useragent_vendorsub="$VERSION")
AC_DEFINE_UNQUOTED(USERAGENT_VENDORSUB,
        ["$useragent_vendorsub"],
        [User-Agent header vendorSub part])

AC_MSG_RESULT(["$useragent_vendor $useragent_vendorsub"])

dnl #########################
dnl Mozilla version checking
dnl #########################
AC_MSG_CHECKING([which mozilla engine to use])

AC_ARG_WITH([mozilla],
        AC_HELP_STRING([--with-mozilla@<:@=libxul-embedding-unstable|seamonkey|firefox|xulrunner|thunderbird@:>@],
                       [Which gecko engine to use (default: autodetect)]))

GECKOS="libxul-embedding-unstable firefox mozilla-firefox seamonkey xulrunner thunderbird mozilla-thunderbird"
gecko=$with_mozilla
autodetect=

if test "x$gecko" = "x"; then
        dnl Autodetect gecko
	autodetect=" (autodetected)"
        for g in $GECKOS; do
                if $PKG_CONFIG --exists $g-xpcom; then
                        gecko=$g
                        break;
                fi
        done
fi

if test "x$gecko" = "x"; then
        AC_MSG_ERROR([No gecko found])
elif ! ( echo "$GECKOS" | egrep "(^| )$gecko(\$| )" > /dev/null); then
        AC_MSG_ERROR([Unknown gecko "$gecko" specified])
fi

AC_MSG_RESULT([$gecko$autodetect])

case "$gecko" in
libxul-embedding-unstable) min_version=1.9 flavour=toolkit ;;
seamonkey) min_version=2.0 flavour=toolkit ;;
*firefox) min_version=2.0 flavour=toolkit ;;
*thunderbird) min_version=2.0 flavour=toolkit ;;
*xulrunner) min_version=1.8 flavour=toolkit ;;
esac

MOZILLA=$gecko
AC_SUBST([MOZILLA])

if test $MOZILLA = libxul-embedding-unstable; then
    MOZILLA_XPCOM=$MOZILLA
    MOZILLA_GTKMOZEMBED=$MOZILLA
else
    MOZILLA_XPCOM=$MOZILLA-xpcom
    MOZILLA_GTKMOZEMBED=$MOZILLA-gtkmozembed
fi
PKG_CHECK_MODULES(XPCOM_COMPONENT, $MOZILLA_XPCOM >= $min_version)

MOZILLA_INCLUDE_ROOT="`$PKG_CONFIG --variable=includedir $MOZILLA_XPCOM`"
if test $MOZILLA = libxul-embedding-unstable; then
    MOZILLA_INCLUDE_ROOT="$MOZILLA_INCLUDE_ROOT/`$PKG_CONFIG --variable=includetype $MOZILLA_XPCOM`"
fi
AC_SUBST(MOZILLA_INCLUDE_ROOT)

PKG_CHECK_MODULES(MOZILLA_COMPONENT, $MOZILLA_GTKMOZEMBED >= $min_version,
		  [has_gtkmozembed=yes], [has_gtkmozembed=no])
if test "x$has_gtkmozembed" = "xno"; then
	MOZILLA_COMPONENT_CFLAGS="$XPCOM_COMPONENT_CFLAGS -I$MOZILLA_INCLUDE_ROOT/gtkembedmoz"
	MOZILLA_COMPONENT_LIBS="$XPCOM_COMPONENT_LIBS -lxul"
fi

#if test $MOZILLA = xulrunner; then
#  MOZILLA_COMPONENT_LIBS="$MOZILLA_COMPONENT_LIBS -lxul"
#fi

AC_SUBST(MOZILLA_COMPONENT_CFLAGS)
AC_SUBST(MOZILLA_COMPONENT_LIBS)

AC_DEFINE(ENABLE_MOZILLA_EMBED, 1, [Define to 1 if you wish to enable the Mozilla renderer.])

_SAVE_CPPFLAGS="$CPPFLAGS"
CPPFLAGS="$CPPFLAGS $XPCOM_COMPONENT_CFLAGS"
AC_MSG_CHECKING([[whether we have a xpcom glue]])
AC_COMPILE_IFELSE(
        [AC_LANG_SOURCE(
                [[
                  #ifndef XPCOM_GLUE
                  #error "no xpcom glue found"
                  #endif]]
        )],
        [gecko_cv_have_xpcom_glue=yes],
        [gecko_cv_have_xpcom_glue=no])
AC_MSG_RESULT([$gecko_cv_have_xpcom_glue])
CPPFLAGS="$_SAVE_CPPFLAGS"
AM_CONDITIONAL([HAVE_XPCOM_GLUE], [test "$gecko_cv_have_xpcom_glue" = "yes"])

MOZILLA_VERSION=`$PKG_CONFIG --modversion $MOZILLA_XPCOM`
if test "$gecko_cv_have_xpcom_glue" = "yes"; then
    MOZILLA_HOME=""
    MOZILLA_COMPONENT_CFLAGS="$MOZILLA_COMPONENT_CFLAGS -DXPCOM_GLUE_USE_NSPR"
    MOZILLA_COMPONENT_CFLAGS="$MOZILLA_COMPONENT_CFLAGS `pkg-config --cflags nspr`"
    MOZILLA_COMPONENT_LIBS="$MOZILLA_COMPONENT_LIBS `pkg-config --libs nspr`"
    AC_MSG_RESULT([Using $MOZILLA version $MOZILLA_VERSION])
else
    MOZILLA_HOME="`$PKG_CONFIG --variable=libdir $MOZILLA_XPCOM`"
    AC_MSG_RESULT([Using $MOZILLA version $MOZILLA_VERSION])
fi
AC_SUBST(MOZILLA_HOME)

AC_LANG_POP(C++)

AC_ARG_ENABLE(cpp-rtti,
        [  --enable-cpp-rtti       Enable C++ RTTI (for cvs gcc)],,
        enable_cpp_rtti=no)

if test "x$enable_cpp_rtti" = "xno"; then
  CXXFLAGS="-fno-rtti $CXXFLAGS"
fi

dnl *************************************************************************
dnl This is from Mozilla's configure.in. They set almost all the config stuff
dnl they need in mozilla-config.h Except for this compiler flag, which can't
dnl go in mozilla-config.h So we check the flag too and now we can include
dnl mozilla-config.h without breaking galeon.
dnl This is really gcc-only
dnl Do this test using CXX only since some versions of gcc
dnl 2.95-2.97 have a signed wchar_t in c++ only and some versions
dnl only have short-wchar support for c++.
AC_LANG_PUSH(C++)
_SAVE_CXXFLAGS=$CXXFLAGS

CXXFLAGS="$CXXFLAGS -fshort-wchar"

AC_CACHE_CHECK(for compiler -fshort-wchar option,
    ac_cv_have_usable_wchar_option,
    [AC_TRY_RUN([#include <stddef.h>
                 int main () {
                   return (sizeof(wchar_t) != 2) ||
                          (wchar_t)-1 < (wchar_t) 0 ; } ],
                ac_cv_have_usable_wchar_option="yes",
                ac_cv_have_usable_wchar_option="no",
                ac_cv_have_usable_wchar_option="maybe")])

if test "$ac_cv_have_usable_wchar_option" != "yes"; then
    CXXFLAGS=$_SAVE_CXXFLAGS
fi


dnl **********************************
dnl now tests for mozilla API variance
dnl **********************************

dnl FIXME find a m4/autoconf guru who can distill this into a nice macro
_SAVE_CXXFLAGS=$CXXFLAGS
_SAVE_CPPFLAGS=$CPPFLAGS
_SAVE_LDFLAGS=$LDFLAGS

#CPPFLAGS="-I$MOZILLA_INCLUDE_ROOT $MOZILLA_COMPONENT_CFLAGS $GALEON_DEPENDENCY_CFLAGS"
#CXXFLAGS="$_SAVE_CXXFLAGS $AM_CXXFLAGS -I$MOZILLA_INCLUDE_ROOT $MOZILLA_COMPONENT_CFLAGS"
#LDFLAGS="$_SAVE_LDFLAGS $AM_LDFLAGS $MOZILLA_COMPONENT_LIBS"
CPPFLAGS="-I$MOZILLA_INCLUDE_ROOT `$PKG_CONFIG --cflags-only-I $MOZILLA-xpcom` `pkg-config --cflags-only-I nspr` $GALEON_DEPENDENCY_CFLAGS"
CXXFLAGS="$_SAVE_CXXFLAGS $AM_CXXFLAGS -I$MOZILLA_INCLUDE_ROOT `pkg-config --cflags $MOZILLA-xpcom` `pkg-config --cflags nspr`"
LDFLAGS="$_SAVE_LDFLAGS $AM_LDFLAGS `pkg-config --libs $MOZILLA-xpcom` `pkg-config --libs nspr`"


TEST_MOZILLA_INCLUDE_DIRS="widget mimetype docshell history dom necko string layout gfx content js exthandler pipnss uriloader caps xpconnect nkcache fastfind gtkembedmoz chrome"
for i in $TEST_MOZILLA_INCLUDE_DIRS ; do
   CXXFLAGS="$CXXFLAGS -I$MOZILLA_INCLUDE_ROOT/$i"
done
CXXFLAGS="$CXXFLAGS -DMOZILLA_STRICT_API=1"

dnl Check whether we have a mozilla debug build

AC_MSG_CHECKING([[whether we have a mozilla debug build]])

MOZ_DEBUG_FLAGS=
AC_PREPROC_IFELSE(
	[AC_LANG_SOURCE(
		[[#include <mozilla-config.h>
		  #if !defined(MOZ_REFLOW_PERF) || !defined(MOZ_REFLOW_PERF_DSP)
		  #error No
		  #endif]]
	)],
	[MOZ_DEBUG_FLAGS="-DDEBUG -D_DEBUG" CXXFLAGS="$CXXFLAGS $MOZ_DEBUG_FLAGS" have_mozilla_debug=yes],
	[have_mozilla_debug=no])

AC_MSG_RESULT([$have_mozilla_debug])

dnl In Gecko 1.9, it's not longer usable.
AC_MSG_CHECKING([whether internal string API is usable])

AC_LINK_IFELSE(
	[AC_LANG_PROGRAM(
		[[#define MOZILLA_INTERNAL_API
		  #include <nsString.h>]],
		[[nsCString c;]]
	)],
	[AC_DEFINE([HAVE_NSSTRING_INTERNAL],[1],
	           [Define if internal string API is usable]) result=yes],
	[result=no])

AC_MSG_RESULT([$result])
AM_CONDITIONAL([HAVE_NSSTRING_INTERNAL], [test "$result" = "yes"])

dnl Changed from nsIFileURL to nsIURI
AC_MSG_CHECKING([whether nsIFilePicker::GetFileURL expect nsIURI])

AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM(
		[[#include <nsIURI.h>
		  #include <nsIFilePicker.h>]],
		[[nsIFilePicker *p;
		  nsIURI *u;
		  p->GetFileURL (&u);]]
	)],
	[AC_DEFINE([HAVE_NSIFILEPICKER_NSIURI],[1],
	           [Define if nsIFilePicker::GetFileURL uses nsIURI]) result=yes],
	[result=no])

AC_MSG_RESULT([$result])

dnl nsMIMEInfoHandleAction renamed to nsHandlerInfoAction
dnl https://bugzilla.mozilla.org/show_bug.cgi?id=384374
AC_MSG_CHECKING([for nsHandlerInfoAction])

AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM(
		[[#include <nsIMIMEInfo.h>]],
		[[nsHandlerInfoAction a;]]
	)],
	[AC_DEFINE([HAVE_NSHANDLERINFOACTION],[1],
	           [Define if nsHandlerInfoAction exists]) result=yes],
	[result=no])

AC_MSG_RESULT([$result])

dnl changed in 1.9a1
dnl https://bugzilla.mozilla.org/show_bug.cgi?id=293714
AC_MSG_CHECKING([whether nsIGlobalHistory3::SetURIGeckoFlags exists])
 
AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM(
		[[#include <nsIGlobalHistory3.h>]],
		[[nsIGlobalHistory3 *p;
		  p->SetURIGeckoFlags(nsnull,0);]]
	)],
	[AC_DEFINE([HAVE_NSIGLOBALHISTORY3_SETURIGECKOFLAGS],[1],
		   [Define if nsIGlobalHistory3::SetURIGeckoFlags exists]) result=yes],
	[result=no])

AC_MSG_RESULT([$result])

dnl changed in 1.9a1
AC_MSG_CHECKING([whether nsIScriptContext is MOZILLA_INTERNAL_API])
AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM(
		[[#undef MOZILLA_STRICT_API
		  #define MOZILLA_INTERNAL_API
		  #include <nscore.h>
		  #include <nsStringAPI.h>
		  #include <nsIScriptContext.h>]],
		[[nsIScriptContext *p;]]
	)],
	[AC_DEFINE([HAVE_NSISCRIPTCONTEXT_INTERNAL_API],[1],
	           [Define if nsIScriptContext is MOZILLA_INTERNAL_API]) result=yes],
	[result=no])
AC_MSG_RESULT([$result])

dnl nsExternalProtocolService::LoadURI was added in 1.8a5
dnl https://bugzilla.mozilla.org/show_bug.cgi?id=263546
AC_MSG_CHECKING([whether nsExternalProtocolService::LoadURI exists])
AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM(
		[[#include <nsEmbedString.h>
		  #include <nsIPrompt.h>
		  #include <nsIExternalProtocolService.h>]],
		[[nsIExternalProtocolService *p;
		  nsIPrompt *prompt; nsIURI *uri;
		 p->LoadURI(uri, prompt);]]
	)],
	[AC_DEFINE([HAVE_NSIEXTERNALPROTOCOLSERVICE_LOADURI],[1],
	           [Define if nsExternalProtocolService::LoadURI exists]) result=yes],
	[result=no])
AC_MSG_RESULT([$result])

dnl nsExternalProtocolService::LoadURI was updated in 1.9
dnl https://bugzilla.mozilla.org/show_bug.cgi?id=385065
AC_MSG_CHECKING([whether nsExternalProtocolService::LoadURI takes InterfaceRequestor])
AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM(
		[[#include <nsEmbedString.h>
		  #include <nsIInterfaceRequestor.h>
		  #include <nsIExternalProtocolService.h>]],
		[[nsIExternalProtocolService *p;
		  nsIInterfaceRequestor *req; nsIURI *uri;
		 p->LoadURI(uri, req);]]
	)],
	[AC_DEFINE([HAVE_NSIEXTERNALPROTOCOLSERVICE_LOADURI_WITH_IR],[1],
	           [Define if nsExternalProtocolService::LoadURI takes InterfaceRequestor]) result=yes],
	[result=no])
AC_MSG_RESULT([$result])


dnl nsExternalProtocolService::GetProtocolHandlerInfo was added in 1.9
dnl https://bugzilla.mozilla.org/show_bug.cgi?id=391150
AC_MSG_CHECKING([whether nsExternalProtocolService::GetProtocolHandlerInfo exists])
AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM(
		[[#include <nsEmbedString.h>
		  #include <nsIExternalProtocolService.h>]],
		[[nsIExternalProtocolService *p;
		  nsIHandlerInfo *info; nsEmbedCString s;
		 p->GetProtocolHandlerInfo(s, &info);]]
	)],
	[AC_DEFINE([HAVE_NSIEXTERNALPROTOCOLSERVICE_GETPROTOCOLHANDLERINFO],[1],
	           [Define if nsExternalProtocolService::GetProtocolHandlerInfo exists]) result=yes],
	[result=no])
AC_MSG_RESULT([$result])


dnl nsIJSConsoleService was removed as useless.
dnl https://bugzilla.mozilla.org/show_bug.cgi?id=243170
AC_CHECK_FILE([$MOZILLA_INCLUDE_ROOT/jsconsole/nsIJSConsoleService.h],
	[AC_DEFINE([HAVE_NSIJSCONSOLESERVICE_H], [1], 
	[Define if nsIJSConsoleService.h exists]) result=yes],
	[result=no])
AM_CONDITIONAL([HAVE_NSIJSCONSOLESERVICE_H], [test "$result" = "yes"])

dnl Changed event to return window instead of just uri.
dnl https://bugzilla.mozilla.org/show_bug.cgi?id=354973
AC_MSG_CHECKING([for nsIDOMPopupBlockedEvent::GetRequestingWindow])

AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM(
		[[#include <nsIDOMPopupBlockedEvent.h>]],
		[[nsIDOMPopupBlockedEvent *e;
		  e->GetRequestingWindow (nsnull);]]
	)],
	[AC_DEFINE([HAVE_NSIDOMPOPUPBLOCKEDEVENT_GETREQUESTINGWINDOW],[1],
	            [Define if nsIDOMPopupBlockedEvent::GetRequestingWindow exists]) result=yes],
	[result=no])

AC_MSG_RESULT([$result])

dnl nsIHelperAppLauncherDialog::PromptForSaveToFile takes aForcePrompt
AC_MSG_CHECKING([whether nsIHelperAppLauncherDialog::PromptForSaveToFile takes aForcePrompt])
AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM(
		[[#include <nsIHelperAppLauncherDialog.h>]],
		[[ nsIHelperAppLauncherDialog *d;
		   d->PromptForSaveToFile(NULL, NULL, NULL, NULL, PR_TRUE, NULL); ]]
	)],
	[AC_DEFINE([HAVE_NSIHELPERAPPLAUNCHERDIALOG_FORCEPROMPT], [1],
		   [Define if nsIHelperAppLauncherDialog::PromptForSaveToFile takes aForcePrompt]) result=yes],
	[result=no])
AC_MSG_RESULT([$result])


dnl nsIWebProgressListener2 added onRefreshAttempted
dnl https://bugzilla.mozilla.org/show_bug.cgi?id=83265
AC_MSG_CHECKING([for nsIWebProgressListener2::OnRefreshAttempted])
AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM(
		[[#include <nsIWebProgressListener2.h>]],
		[[nsIWebProgressListener2 *w;
		  w->OnRefreshAttempted(nsnull, nsnull, 0, 0, nsnull);]]
	)],
	[AC_DEFINE([HAVE_NSIWEBPROGRESSLISTENER2_ONREFRESHATTEMPTED], [1],
		   [Define if nsIWebProgressListener2 has OnRefreshAttempted]) result=yes],
	[result=no])
AC_MSG_RESULT([$result])

dnl Check that the mozilla security stuff is installed
dnl Not a simple file check due to the header being in the SDK now
dnl https://bugzilla.mozilla.org/show_bug.cgi?id=271068
AC_MSG_CHECKING([for mozilla security compoment])
AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM(
		[[#include <nsIX509Cert.h>]],
		[[nsIX509Cert *c;
		  c->GetIssuer (nsnull);]]
	)],
	[AC_DEFINE([HAVE_MOZILLA_PSM], [1],
		   [Define if you have the mozilla psm headers installed]) result=yes],
	[result=no])
AC_MSG_RESULT([$result])

dnl nsIBadCertListener2 was added in gecko 1.9 to replace nsIBadCertListener
dnl https://bugzilla.mozilla.org/show_bug.cgi?id=3271
AC_CHECK_FILE([$MOZILLA_INCLUDE_ROOT/nsIBadCertListener.h],
	[AC_DEFINE([HAVE_NSIBADCERTLISTENER_H],[1],
	[Define if nsIBadCertListener.h exists])])
AC_CHECK_FILE([$MOZILLA_INCLUDE_ROOT/pipnss/nsIBadCertListener.h],
	[AC_DEFINE([HAVE_NSIBADCERTLISTENER_H],[1],
	[Define if nsIBadCertListener.h exists])])

dnl During 1.9a1, GetCompatibilityMode was moved to nsIDocument
dnl https://bugzilla.mozilla.org/show_bug.cgi?id=362797
AC_MSG_CHECKING([for nsIDocument::GetCompatibilityMode])
AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM(
		[[#include <nsIDocument.h>]],
		[[nsIDocument *d;
		  d->GetCompatibilityMode();]]
	)],
	[AC_DEFINE([HAVE_NSIDOCUMENT_GETCOMPATIBILITYMODE], [1],
		   [Define if nsIDocument has GetCompatibilityMode]) result=yes],
	[result=no])
AC_MSG_RESULT([$result])

dnl Allow typeaheadfind to know about focus
dnl https://bugzilla.mozilla.org/show_bug.cgi?id=189039
AC_MSG_CHECKING([for nsITypeAheadFind::SetSelectionModeAndRepaint])
AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM(
		[[#include <nsITypeAheadFind.h>]],
		[[nsITypeAheadFind *p;
		  p->SetSelectionModeAndRepaint(0);]]
	)],
	[AC_DEFINE([HAVE_NSITYPEAHEADFIND_SETSELECTIONMODEANDREPAINT],[1],[Define nsITypeAheadFind has a SetSelectionModeAndRepaint function]) result=yes],
	[result=no])
AC_MSG_RESULT([$result])

dnl typeaheadfind changed to use a single FindAgain method
dnl https://bugzilla.mozilla.org/show_bug.cgi?id=384298
AC_MSG_CHECKING([for nsITypeAheadFind::FindAgain without aLinksOnly])
AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM(
		[[#include <nsITypeAheadFind.h>]],
		[[nsITypeAheadFind *p;
		  p->FindAgain(0, 0, 0);]]
	)],
	[AC_DEFINE([HAVE_NSITYPEAHEADFIND_FINDAGAIN],[1],[Define nsITypeAheadFind has a FindAgain function]) result=yes],
	[result=no])
AC_MSG_RESULT([$result])

dnl During 1.9a1, nsICSSLoader methods were changed
dnl https://bugzilla.mozilla.org/show_bug.cgi?id=293825
AC_MSG_CHECKING([for nsICSSLoader::LoadSheetSync])
AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM(
		[[#define MOZILLA_INTERNAL_API
		  #include <nsICSSLoader.h>]],
		[[nsICSSLoader *l;
		  l->LoadSheetSync(nsnull, nsnull);]]
	)],
	[AC_DEFINE([HAVE_NSICSSLOADER_LOADSHEETSYNC], [1],
		   [Define if nsICSSLoader includes LoadSheetSync]) result=yes],
	[result=no])
AC_MSG_RESULT([$result])

dnl Durning 1.9, gtk_moz_embed_set_comp_path was
dnl pseudo-replaced by gtk_moz_embed_set_path
AC_MSG_CHECKING([for gtk_mozembed_set_path])
AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM(
		[[#include <gtkmozembed.h>]],
		[[gtk_moz_embed_set_path(NULL);]]
	)],
	[AC_DEFINE([HAVE_GTK_MOZ_EMBED_SET_PATH], [1],
		   [Define if gtk_moz_embed_set_path exists]) result=yes],
	[result=no])
AC_MSG_RESULT([$result])

dnl For xulrunner, we need to register a directory provider. This code
dnl works on any new enough toolkit based backend.
AC_CHECK_FILE([$MOZILLA_INCLUDE_ROOT/chrome/nsIToolkitChromeRegistry.h],
	[AC_DEFINE([HAVE_NSITOOLKITCHROMEREGISTRY_H], [1], 
	[Define if nsIToolkitChromeRegistery.h exists]) result=yes],
	[result=no])
AM_CONDITIONAL([HAVE_NSITOOLKITCHROMEREGISTRY_H], [test "$result" = "yes"])

dnl Another method added to nsISidebar
dnl https://bugzilla.mozilla.org/show_bug.cgi?id=334471
AC_MSG_CHECKING([for nsISidebar::AddMicrosummaryGenerator])
AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM(
		[[#include <nsISidebar.h>]],
		[[nsISidebar *p;
		  p->AddMicrosummaryGenerator(NULL);]]
	)],
	[AC_DEFINE([HAVE_NSISIDEBAR_ADDMICROSUMMARYGENERATOR], [1],
		   [Define if nsISidebar includes AddMicrosummaryGenerator]) result=yes],
	[result=no])
AC_MSG_RESULT([$result])

dnl A method added to nsIAboutModule
dnl https://bugzilla.mozilla.org/show_bug.cgi?id=341313
AC_MSG_CHECKING([for nsIAboutModule::GetURIFlags])
AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM(
		[[#include <nsIAboutModule.h>]],
		[[nsIAboutModule *p;
		  p->GetURIFlags(NULL, NULL);]]
	)],
	[AC_DEFINE([HAVE_NSIABOUTMODULE_GETURIFLAGS], [1],
		   [Define if nsIAboutModule includes GetURIFlags]) result=yes],
	[result=no])
AC_MSG_RESULT([$result])

AC_MSG_CHECKING([for nsIPrintSettings::SetOutputFormat])
AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM(
		[[#include <nsIPrintSettings.h>]],
		[[nsIPrintSettings *ps;
		  PRInt16 out;
		  ps->SetOutputFormat(out);]]
	)],
	[AC_DEFINE([HAVE_NSIPRINTSETTINGS_SETOUTPUTFORMAT], [1],
		   [Define if nsIPrintSettings::SetOutputFormat exists]) result=yes],
	[result=no])
AC_MSG_RESULT([$result])

AC_MSG_CHECKING([for nsIPrintSettings::SetPaperSize])
AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM(
		[[#include <nsIPrintSettings.h>]],
		[[nsIPrintSettings *ps;
		  PRInt16 size;
		  ps->SetPaperSize(size);]]
	)],
	[AC_DEFINE([HAVE_NSIPRINTSETTINGS_SETPAPERSIZE], [1],
		   [Define if nsIPrintSettings::SetPaperSize exists]) result=yes],
	[result=no])
AC_MSG_RESULT([$result])

AC_MSG_CHECKING([for nsIPrintSettings::SetPaperName])
AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM(
		[[#include <nsIPrintSettings.h>]],
		[[nsIPrintSettings *ps;
		  PRUnichar *s;
		  ps->SetPaperName(s);]]
	)],
	[AC_DEFINE([HAVE_NSIPRINTSETTINGS_SETPAPERNAME], [1],
		   [Define if nsIPrintSettings::SetPaperName exists]) result=yes],
	[result=no])
AC_MSG_RESULT([$result])
dnl GetMessage -> GetMessageMoz
AC_MSG_CHECKING([for nsIConsoleMessage::GetMessageMoz])
AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM(
		[[#include <nsIConsoleMessage.h>]],
		[[nsIConsoleMessage *m;
		  PRUnichar *s;
		  m->GetMessageMoz(&s);]]
	)],
	[AC_DEFINE([HAVE_NSICONSOLEMESSAGE_GETMESSAGEMOZ], [1],
		   [Define if nsIConsoleMessage::GetMessageMoz exists]) result=yes],
	[result=no])
AC_MSG_RESULT([$result])

dnl nsIClassInfo's impl macros were moved to their own file in 1.9a1
dnl https://bugzilla.mozilla.org/show_bug.cgi?id=330420
AC_CHECK_FILE([$MOZILLA_INCLUDE_ROOT/nsIClassInfoImpl.h],
	[AC_DEFINE([HAVE_NSICLASSINFOIMPL_H],[1],
	[Define if nsIClassInfoImpl.h exists])])
AC_CHECK_FILE([$MOZILLA_INCLUDE_ROOT/xpcom/nsIClassInfoImpl.h],
	[AC_DEFINE([HAVE_NSICLASSINFOIMPL_H],[1],
	[Define if nsIClassInfoImpl.h exists])])

dnl nsIMutableArray was split out when nsIArray was frozen in 1.9a1
AC_CHECK_FILE([$MOZILLA_INCLUDE_ROOT/nsIMutableArray.h],
	[AC_DEFINE([HAVE_NSIMUTABLEARRAY_H],[1],
	[Define if nsIMutableArray.h exists])])
AC_CHECK_FILE([$MOZILLA_INCLUDE_ROOT/xpcom/nsIMutableArray.h],
	[AC_DEFINE([HAVE_NSIMUTABLEARRAY_H],[1],
	[Define if nsIClassInfoImpl.h exists])])

dnl The password manager was removed in 1.9
AC_CHECK_FILE([$MOZILLA_INCLUDE_ROOT/nsIPassword.h],
	[AC_DEFINE([HAVE_NSIPASSWORD_H],[1],
	[Define if nsIPassword.h exists])])
AC_CHECK_FILE([$MOZILLA_INCLUDE_ROOT/necko/nsIPassword.h],
	[AC_DEFINE([HAVE_NSIPASSWORD_H],[1],
	[Define if nsIPassword.h exists])])
AC_CHECK_FILE([$MOZILLA_INCLUDE_ROOT/passwordmgr/nsIPassword.h],
	[AC_DEFINE([HAVE_NSIPASSWORD_H],[1],
	[Define if nsIPassword.h exists])])

dnl The Login Manager was addeded to replace it in 1.9
AC_CHECK_FILE([$MOZILLA_INCLUDE_ROOT/nsILoginManager.h],
	[AC_DEFINE([HAVE_NSILOGINMANAGER_H],[1],
	[Define if nsILoginManager.h exists])])
AC_CHECK_FILE([$MOZILLA_INCLUDE_ROOT/loginmgr/nsILoginManager.h],
	[AC_DEFINE([HAVE_NSILOGINMANAGER_H],[1],
	[Define if nsILoginManager.h exists])])

dnl nsIPromptService2 is how http auth is done in 1.9
AC_CHECK_FILE([$MOZILLA_INCLUDE_ROOT/nsIPromptService2.h],
	[AC_DEFINE([HAVE_NSIPROMPTSERVICE2_H],[1],
	[Define if nsIPromptService2.h exists])])
AC_CHECK_FILE([$MOZILLA_INCLUDE_ROOT/windowwatcher/nsIPromptService2.h],
	[AC_DEFINE([HAVE_NSIPROMPTSERVICE2_H],[1],
	[Define if nsIPromptService2.h exists])])

dnl nsIStyleSheetService to set override stylesheets in 1.9
AC_CHECK_FILE([$MOZILLA_INCLUDE_ROOT/nsIStyleSheetService.h],
	[AC_DEFINE([HAVE_NSISTYLESHEETSERVICE_H],[1],
	[Define if nsIPromptService2.h exists])])
AC_CHECK_FILE([$MOZILLA_INCLUDE_ROOT/layout/nsIStyleSheetService.h],
	[AC_DEFINE([HAVE_NSISTYLESHEETSERVICE_H],[1],
	[Define if nsIStyleSheetService.h exists])])

dnl nsIXULAppInfo is needed for 1.9
AC_CHECK_FILE([$MOZILLA_INCLUDE_ROOT/nsIXULAppInfo.h],
	[AC_DEFINE([HAVE_NSIXULAPPINFO_H],[1],
	[Define if nsIXULAppInfo.h exists])])
AC_CHECK_FILE([$MOZILLA_INCLUDE_ROOT/xpcom/nsIXULAppInfo.h],
	[AC_DEFINE([HAVE_NSIXULAPPINFO_H],[1],
	[Define if nsIXULAppInfo.h exists])])
AM_CONDITIONAL([HAVE_NSIXULAPPINFO_H], [test "$result" = "yes"])

dnl restore flags
CXXFLAGS="$_SAVE_CXXFLAGS $MOZ_DEBUG_FLAGS"
CPPFLAGS=$_SAVE_CPPFLAGS
LDFLAGS=$_SAVE_LDFLAGS
AC_LANG_POP(C++)

dnl unfortunately _NL_PAPER_WIDTH and _NL_PAPER_HEIGHT are not defines but
dnl enums so we need to check their availability the hard way
AC_MSG_CHECKING([for _NL_PAPER_WIDTH and _NL_PAPER_HEIGHT])
AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM(
	        [[#include <langinfo.h>]],
		[[int n;
		  n = (int) (long int) nl_langinfo(_NL_PAPER_WIDTH);
		  n = (int) (long int) nl_langinfo(_NL_PAPER_HEIGHT);]]
	)],
	[AC_DEFINE([HAVE__NL_PAPER_WIDTH_HEIGHT], [1],
		   [Define if _NL_PAPER_WIDTH and _NL_PAPER_HEIGHT are available]) result=yes],
	[result=no])
AC_MSG_RESULT([$result])

# Check to see whether we have gnome_vfs_get_mime_type_for_name
AC_MSG_CHECKING([for gnome_vfs_get_mime_type_for_name])
if pkg-config --atleast-version 2.13.4 gnome-vfs-2.0; then
   AC_DEFINE([HAVE_GNOME_VFS_GET_MIME_TYPE_FOR_NAME],[1],
	     [Define if you have the gnome_vfs_get_mime_type_for_name function])
   result=yes
else
   result=no
fi
AC_MSG_RESULT([$result])

dnl *******************************
dnl Gonf configuration
dnl ******************************* 

dnl Specify the gconf configuration source, 
dnl default to xml::$(sysconfdir)/gconf/gconf.xml.defaults

AC_PATH_PROG(GCONFTOOL, gconftool-2, no)

if test x"$GCONFTOOL" = xno; then
  AC_MSG_ERROR([gconftool-2 executable not found in your path - should be installed with GConf])
fi

AM_GCONF_SOURCE_2

dnl ***************
dnl Multimedia keys
dnl ***************

AC_CHECK_HEADERS([X11/XF86keysym.h])

dnl *******************************
dnl Internationalization
dnl ******************************* 
AC_MSG_CHECKING([for iso-codes package])
have_iso_codes=no
if $PKG_CONFIG --exists iso-codes --atleast-version=0.35; then
        have_iso_codes=yes
fi
AC_MSG_RESULT([$have_iso_codes])

if test "x$have_iso_codes" = "xyes"; then
        AC_MSG_CHECKING([whether iso-codes has iso-639 and iso-3166 domains])
        if $PKG_CONFIG --variable=domains iso-codes | grep -q 639 && \
           $PKG_CONFIG --variable=domains iso-codes | grep -q 3166 ; then
                result=yes
        else
                result=no
                have_iso_codes=no
        fi
        AC_MSG_RESULT([$result])
fi

if test "x$have_iso_codes" = "xyes"; then
        AC_DEFINE_UNQUOTED([ISO_CODES_PREFIX],["`$PKG_CONFIG --variable=prefix iso-codes`"],[ISO codes prefix])
        AC_DEFINE([HAVE_ISO_CODES],[1],[Define if you have the iso-codes package])
fi

dnl Add the languages which your application supports here.
ALL_LINGUAS="am ar az bg bs ca cs da de dz el en_CA en_GB eo es et eu fi fr ga gl gu hr hu it ja ko lt lv mk ml mn ms nb ne nl nn oc pa pl pt pt_BR ro ru rw sk sl sq sr sr@Latn sv ta th tr uk vi wa zh_CN zh_HK zh_TW"
GETTEXT_PACKAGE=galeon-2.0
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE,"$GETTEXT_PACKAGE", [Define to the Gettext package name.])
AM_GLIB_GNU_GETTEXT


dnl *********************************************************************
dnl ** GCC FLAGS ********************************************************
dnl *********************************************************************

AC_ARG_ENABLE(werror,
	[  --enable-werror          Pass -Werror flag to C compiler],
	if test "x$enable_werror" != "xno"; then
		disable_werror=no
	fi)

dnl *********************************************************************
dnl ** Quiet compilation ************************************************
dnl *********************************************************************
AC_ARG_ENABLE(quiet,
	      AS_HELP_STRING([--enable-quiet],
                             [Enable quiet compilation]),
	[
		Q='@'
		QUIET_CC='@echo "    CC $@";'
		QUIET_CPP='@echo "    CPP $@";'
		QUIET_LD='@echo "    LINK $@";'
		LIBTOOL_ARG="--silent"

		AC_SUBST(Q)
		AC_SUBST(QUIET_CC)
		AC_SUBST(QUIET_CPP)
		AC_SUBST(QUIET_LD)
		AC_SUBST(LIBTOOL_ARG)
	])


dnl Only use warning flags if we have gcc
CC_WARNING_FLAGS="-Wall -Wmissing-declarations -Wmissing-prototypes -Wsign-compare"
if test "x$GCC" = "xyes"; then
	for flag in $CC_WARNING_FLAGS ; do
		if test -z "`echo "$CFLAGS" | grep "\$flag" 2> /dev/null`" ; then
			CFLAGS="$CFLAGS $flag"
		fi
	done
	if test "x$disable_werror" = "xno"; then
        	if test -z "`echo "$CFLAGS" | grep "\-Werror" 2> /dev/null`" ; then
	     		CFLAGS="$CFLAGS -Werror"
		fi
        fi
fi

dnl Only use warning flags if we have g++
CXX_WARNING_FLAGS="-Wall -Wsign-compare -Wno-ctor-dtor-privacy -Wno-non-virtual-dtor"
if test "x$GXX" = "xyes"; then
	for flag in $CXX_WARNING_FLAGS ; do
		if test -z "`echo "$CXXFLAGS" | grep "\$flag" 2> /dev/null`" ; then
			CXXFLAGS="$CXXFLAGS $flag"
		fi
	done
        if test "x$disable_werror" = "xno"; then
		if test -z "`echo "$CXXFLAGS" | grep "\-Werror" 2> /dev/null`" ; then
			CXXFLAGS="$CXXFLAGS -Werror"
		fi
        fi
        gcc_check=`$CXX -v 2>&1 | grep '^gcc version 3\.'`
        if test "$gcc_check" != ""; then
                CXXFLAGS="$CXXFLAGS -Wno-deprecated"
        fi
fi

AC_OUTPUT([
galeon.spec
Makefile
libegg/Makefile
libegg/dock/Makefile
src/Makefile
utils/Makefile
mozilla/Makefile
mozilla/chrome/Makefile
mozilla/chrome/brand.dtd
mozilla/chrome/brand.properties
gtkhtml/Makefile
embed/Makefile
bookmarks/Makefile
po/Makefile.in
ui/Makefile
ui/galeon-bookmarks-editor-ui.xml
doc/Makefile
doc/C/Makefile
doc/es/Makefile
sounds/Makefile
galeon-config-tool
INSTALL
],
[chmod +x galeon-config-tool])

dnl ===========================================================================

if test "$gecko" == "xulrunner" || test "$gecko" == "libxul-embedding-unstable"; then
	echo ""
	echo "Note that Galeon has a runtime dependency on the 'cookie' and 'permission'"
	echo "mozilla extensions. These are not built as part of the default xulrunner"
	echo "build, and must be added explicitly at xulrunner configure-time."
	echo ""
fi

if test "$flavour" == "toolkit"; then
	echo ""
	echo "The 'typeaheadfind' and 'wallet' extensions should not be"
	echo "present when using $gecko. They are never part of the default build,"
	echo "but might be added manually by mistake."
	echo ""
fi
