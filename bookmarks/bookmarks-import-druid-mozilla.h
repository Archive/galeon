/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __bookmarks_import_druid_mozilla_h
#define __bookmarks_import_druid_mozilla_h

#include <glib-object.h>
#include <bookmarks-import-druid.h>

/* object forward declarations */

typedef struct _GbImportDruidMozilla GbImportDruidMozilla;
typedef struct _GbImportDruidMozillaClass GbImportDruidMozillaClass;
typedef struct _GbImportDruidMozillaPrivate GbImportDruidMozillaPrivate;

/**
 * Druid object
 */

#define GB_TYPE_IMPORT_DRUID_MOZILLA		(gb_import_druid_mozilla_get_type())
#define GB_IMPORT_DRUID_MOZILLA(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), \
						 GB_TYPE_IMPORT_DRUID_MOZILLA,\
						 GbImportDruidMozilla))
#define GB_IMPORT_DRUID_MOZILLA_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), \
						 GB_TYPE_IMPORT_DRUID_MOZILLA,\
						 GbImportDruidMozillaClass))
#define GB_IS_IMPORT_DRUID_MOZILLA(object)	(G_TYPE_CHECK_INSTANCE_TYPE((object), \
						 GB_TYPE_IMPORT_DRUID_MOZILLA))
#define GB_IS_IMPORT_DRUID_MOZILLA_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), \
						 GB_TYPE_IMPORT_DRUID_MOZILLA))
#define GB_IMPORT_DRUID_MOZILLA_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), \
						 GB_TYPE_IMPORT_DRUID_MOZILLA,\
						 GbImportDruidMozillaClass))

struct _GbImportDruidMozillaClass 
{
	GbImportDruidClass parent_class;

};

/* Remember: fields are public read-only */
struct _GbImportDruidMozilla
{
	GbImportDruid parent_object;
};

GType			gb_import_druid_mozilla_get_type		(void);
GbImportDruidMozilla *	gb_import_druid_mozilla_new			(void);

#endif
