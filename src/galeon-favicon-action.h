/*
 *  Copyright (C) 2004   Crispin Flowerday
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GALEON_FAVICON_ACTION_H
#define GALEON_FAVICON_ACTION_H

#include <glib-object.h>
#include <gtk/gtkaction.h>

/* object forward declarations */

typedef struct _GaleonFaviconAction GaleonFaviconAction;
typedef struct _GaleonFaviconActionClass GaleonFaviconActionClass;
typedef struct _GaleonFaviconActionPrivate GaleonFaviconActionPrivate;

/**
 * GaleonFaviconAction object
 */

#define GALEON_TYPE_FAVICON_ACTION		(galeon_favicon_action_get_type())
#define GALEON_FAVICON_ACTION(object)		(G_TYPE_CHECK_INSTANCE_CAST((object),\
					 GALEON_TYPE_FAVICON_ACTION,\
					 GaleonFaviconAction))
#define GALEON_FAVICON_ACTION_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass),\
					 GALEON_TYPE_FAVICON_ACTION,\
					 GaleonFaviconActionClass))
#define GALEON_IS_FAVICON_ACTION(object)	(G_TYPE_CHECK_INSTANCE_TYPE((object),\
					 GALEON_TYPE_FAVICON_ACTION))
#define GALEON_IS_FAVICON_ACTION_ITEM_CLASS(klass) \
	 				(G_TYPE_CHECK_CLASS_TYPE((klass),\
	 				 GALEON_TYPE_FAVICON_ACTION))
#define GALEON_FAVICON_ACTION_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj),\
					 GALEON_TYPE_FAVICON_ACTION,\
					 GaleonFaviconActionClass))

struct _GaleonFaviconActionClass
{
	GtkActionClass parent_class;
};

/* Remember: fields are public read-only */
struct _GaleonFaviconAction
{
	GtkAction parent_object;
	
	GaleonFaviconActionPrivate *priv;
};

GType		 galeon_favicon_action_get_type		(void);

#endif
