/*
 *  Copyright (C) 2003 Tommi Komulainen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *  
 */

#ifndef GUL_STATE_H
#define GUL_STATE_H

#include <gtk/gtkwidget.h>

G_BEGIN_DECLS

void gul_state_init                     (void);
void gul_state_shutdown                 (void);

void gul_state_monitor_window           (GtkWidget  *window,
		                         const char *name,
					 int         default_width,
					 int         default_heigth);

void gul_state_monitor_paned            (GtkWidget  *paned,
		                         const char *key,
					 int         default_size);

void gul_state_monitor_expander         (GtkWidget *expander,
					 const char *key,
					 gboolean    default_visiblity);

void   gul_state_set_string		(const char *key, const char *value);
char * gul_state_get_string		(const char *key, const char *default_val);

void   gul_state_set_boolean		(const char *key, gboolean value);
gboolean gul_state_get_boolean		(const char *key, gboolean default_val);

G_END_DECLS

#endif /* __gul_state_H */
