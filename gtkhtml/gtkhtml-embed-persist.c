/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "gtkhtml-embed-persist.h"

static void
gtkhtml_embed_persist_class_init (GtkhtmlEmbedPersistClass *klass);
static void
gtkhtml_embed_persist_init (GtkhtmlEmbedPersist *ges);
static void
gtkhtml_embed_persist_finalize (GObject *object);

static gresult 
impl_save (GaleonEmbedPersist *persist);

struct GtkhtmlEmbedPersistPrivate
{
};

static GObjectClass *parent_class = NULL;

GType
gtkhtml_embed_persist_get_type (void)
{
       static GType gtkhtml_embed_persist_type = 0;

        if (gtkhtml_embed_persist_type == 0)
        {
                static const GTypeInfo our_info =
                {
                        sizeof (GtkhtmlEmbedPersistClass),
                        NULL, /* base_init */
                        NULL, /* base_finalize */
                        (GClassInitFunc) gtkhtml_embed_persist_class_init,
                        NULL, /* class_finalize */
                        NULL, /* class_data */
                        sizeof (GtkhtmlEmbedPersist),
                        0,    /* n_preallocs */
                        (GInstanceInitFunc) gtkhtml_embed_persist_init
                };

                gtkhtml_embed_persist_type = 
				g_type_register_static (GALEON_TYPE_EMBED_PERSIST,
                                                        "GtkhtmlEmbedPersist",
                                                        &our_info, (GTypeFlags)0);
        }

        return gtkhtml_embed_persist_type;
}

static void
gtkhtml_embed_persist_class_init (GtkhtmlEmbedPersistClass *klass)
{
        GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GaleonEmbedPersistClass *persist_class;
	
        parent_class = (GObjectClass *)gtk_type_class (GTKHTML_TYPE_EMBED_PERSIST);
	persist_class = (GaleonEmbedPersistClass *) parent_class;
	
        object_class->finalize = gtkhtml_embed_persist_finalize;

	persist_class->save = impl_save;
}

static void
gtkhtml_embed_persist_init (GtkhtmlEmbedPersist *persist)
{
        persist->priv = g_new0 (GtkhtmlEmbedPersistPrivate, 1);
}

static void
gtkhtml_embed_persist_finalize (GObject *object)
{
        GtkhtmlEmbedPersist *persist;

        g_return_if_fail (object != NULL);
        g_return_if_fail (GTKHTML_IS_EMBED_PERSIST (object));

        persist = GTKHTML_EMBED_PERSIST (object);

        g_return_if_fail (persist->priv != NULL);

	g_free (persist->priv);
	
        G_OBJECT_CLASS (parent_class)->finalize (object);
}

static gresult 
impl_save (GaleonEmbedPersist *persist)
{
	return G_NOT_IMPLEMENTED;
}

