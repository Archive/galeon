/*
 *  Copyright (C) 2001 Philip Langdale
 *  Copyright (C) 2003 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef GALEON_FILEPICKER_H
#define GALEON_FILEPICKER_H

#include <nsIFilePicker.h>
#include <nsISupports.h>

#include "gul-file-chooser.h"
#include "GulString.h"

#define G_FILEPICKER_CID			     \
{ /* 3636dc79-0b42-4bad-8a3f-ae15d3671d17 */         \
    0x3636dc79,                                      \
    0x0b42,                                          \
    0x4bad,                                          \
    {0x8a, 0x3f, 0xae, 0x15, 0xd3, 0x67, 0x1d, 0x17} \
}

#define G_FILEPICKER_CONTRACTID	"@mozilla.org/filepicker;1"
#define G_FILEPICKER_CLASSNAME	"Galeon File Picker Implementation"

class GFilePicker : public nsIFilePicker
{
public:
	NS_DECL_ISUPPORTS
	NS_DECL_NSIFILEPICKER
	
	GFilePicker();
	virtual ~GFilePicker();

private:
	nsresult AppendFilter (const char * title, const char * pattern);

	GulFileChooser *mDialog;
	PRInt16 mMode;
	GulString mDefaultString;
};

#endif
