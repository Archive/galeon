/* 
 *  Copyright (C) 2002  Ricardo Fernándezs Pascual <ric@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __BOOKMARKS_TREE_MODEL_H__
#define __BOOKMARKS_TREE_MODEL_H__

#include <gtk/gtktreemodel.h>
#include "bookmarks.h"

/* object forward declarations */

typedef struct _GbTreeModel GbTreeModel;
typedef struct _GbTreeModelClass GbTreeModelClass;

typedef enum {
	GB_TREE_MODEL_COL_ICON,
	GB_TREE_MODEL_COL_TITLE,
	GB_TREE_MODEL_COL_URL,
	GB_TREE_MODEL_COL_CHECK,
	GB_TREE_MODEL_NUM_COLUMS
} GbTreeModelColumn;

typedef gboolean (*GbTreeModelCheckFunc) (GbBookmark *b, gpointer data);

/**
 * Tree model object
 */

#define GB_TYPE_TREE_MODEL		(gb_tree_model_get_type())
#define GB_TREE_MODEL(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), GB_TYPE_TREE_MODEL,\
					 GbTreeModel))
#define GB_TREE_MODEL_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), GB_TYPE_TREE_MODEL,\
					 GbTreeModelClass))
#define GB_IS_TREE_MODEL(object)	(G_TYPE_CHECK_INSTANCE_TYPE((object), GB_TYPE_TREE_MODEL))
#define GB_IS_TREE_MODEL_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), GB_TYPE_TREE_MODEL))
#define GB_TREE_MODEL_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), GB_TYPE_TREE_MODEL,\
					 GbTreeModelClass))

struct _GbTreeModel
{
	GObject parent;

	/* <private> */

	gint stamp;
	GSList *roots;
	GbBookmarkSet *set;
	gboolean only_folders;
	gboolean hide_roots;
	GbTreeModelCheckFunc check_func;
	gpointer check_func_data;
};

struct _GbTreeModelClass
{
	GObjectClass parent_class;
};


GType			gb_tree_model_get_type		(void);
GbTreeModel *		gb_tree_model_new		(GSList *roots);
GbTreeModel *		gb_tree_model_new_for_set	(GbBookmarkSet *set);
void			gb_tree_model_set_only_folders	(GbTreeModel *tm, gboolean val);
gboolean		gb_tree_model_get_only_folders	(GbTreeModel *tm);
void			gb_tree_model_set_hide_roots	(GbTreeModel *tm, gboolean val);
gboolean		gb_tree_model_get_hide_roots	(GbTreeModel *tm);

gboolean		gb_tree_model_is_ancestor       (GbTreeModel *btree_model, GtkTreeIter  *iter,
							 GtkTreeIter  *descendant);
gint			gb_tree_model_iter_depth	(GbTreeModel *btree_model, GtkTreeIter  *iter);
GbBookmark *		gb_tree_model_bookmark_from_iter (GbTreeModel *model, GtkTreeIter *iter);
gboolean		gb_tree_model_iter_from_bookmark (GbTreeModel *model, GbBookmark *b,
							  GtkTreeIter *iter);
const GSList *		gb_tree_model_get_roots		(GbTreeModel *model);
GbBookmark *		gb_tree_model_get_first_root	(GbTreeModel *model);
GbBookmarkSet *		gb_tree_model_get_set		(GbTreeModel *model);
void			gb_tree_model_set_check		(GbTreeModel *model, GbTreeModelCheckFunc cf, gpointer data);

#endif 
