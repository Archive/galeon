/*
 *  Copyright (C) 2002 Ricardo Fernández Pascual
 *  Copyright (C) 2005 Crispin Flowerday
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "galeon-bookmarks-icon-provider.h"
#include "bookmarks-iterator.h"
#include "galeon-shell.h"
#include "galeon-debug.h"

/**
 * Private data
 */
#define GB_GALEON_ICON_PROVIDER_GET_PRIVATE(object) (G_TYPE_INSTANCE_GET_PRIVATE ((object), \
				       GB_TYPE_GALEON_ICON_PROVIDER, GbGaleonIconProviderPrivate))


struct _GbGaleonIconProviderPrivate 
{
	GaleonFaviconCache *favicon_cache;

	GHashTable *cache_entries;
	GHashTable *bookmarks;

	GSList *sets;
};

typedef struct
{
	GList *bookmarks;
} CacheEntryInfo;

enum
{
	PROP_0,
	PROP_CACHE
};


G_DEFINE_TYPE (GbGaleonIconProvider, gb_galeon_icon_provider, 
	       GB_TYPE_ICON_PROVIDER);


static void
cache_entry_changed_cb (GaleonFaviconCacheEntry *entry, GParamSpec *pspec,
			GbGaleonIconProvider *dip)
{
	CacheEntryInfo *entry_info;
	GList *l;

	entry_info = g_hash_table_lookup (dip->priv->cache_entries, entry);

	g_return_if_fail (entry_info);

	for (l = entry_info->bookmarks ; l ; l = l->next)
	{
		gb_bookmark_emit_changed (GB_BOOKMARK (l->data));
	}
}

static void
remove_bookmark_from_cache_entry (GbGaleonIconProvider *dip,
				  GaleonFaviconCacheEntry *entry,
				  GbBookmark *bm)
{
	CacheEntryInfo *entry_info;
	
	entry_info = g_hash_table_lookup (dip->priv->cache_entries, entry);

	entry_info->bookmarks = g_list_remove (entry_info->bookmarks, bm);

	if (entry_info->bookmarks == NULL)
	{
		/* No more bookmarks for this cache entry */
		galeon_favicon_cache_entry_set_persistent (entry, FALSE);

		g_signal_handlers_disconnect_by_func (entry, 
						      G_CALLBACK (cache_entry_changed_cb),
						      dip);

		g_hash_table_remove (dip->priv->cache_entries, entry);
		g_free (entry_info);
		g_object_unref (entry);
	}
}
				 

static void
add_bookmark_to_cache_entry (GbGaleonIconProvider *dip,
			     GaleonFaviconCacheEntry *entry,
			     GbBookmark *bm)
{
	CacheEntryInfo *entry_info;

	entry_info = g_hash_table_lookup (dip->priv->cache_entries, entry);
	if (!entry_info)
	{
		entry_info = g_new0 (CacheEntryInfo, 1);

		g_hash_table_insert (dip->priv->cache_entries, 
				     g_object_ref (entry),
				     entry_info);

		g_signal_connect (entry, "notify::pixbuf", 
				  G_CALLBACK (cache_entry_changed_cb),
				  dip);
	}

	entry_info->bookmarks = g_list_prepend (entry_info->bookmarks, bm);
}


static void
gb_galeon_icon_provider_weak_notify (GbGaleonIconProvider *dip,
				     GbBookmark *bm)
{
	GaleonFaviconCacheEntry *entry;	

	entry = g_hash_table_lookup (dip->priv->bookmarks, bm);

	if (!entry)
	{
		g_warning ("Missing bookmark -> cache entry");
		return;
	}

	g_hash_table_remove (dip->priv->bookmarks, bm);

	remove_bookmark_from_cache_entry (dip, entry, bm);
}

static void
bookmark_url_modified_cb (GbBookmark *bm, const char *url, GbGaleonIconProvider *dip)
{
	GaleonFaviconCacheEntry *entry, *new_entry;

	entry     = g_hash_table_lookup (dip->priv->bookmarks, bm);

	if ((!url || url[0] == '\0') && GB_IS_SMART_SITE (bm))
	{
		url = GB_SMART_SITE (bm)->smarturl;
	}

	new_entry = galeon_favicon_cache_lookup (dip->priv->favicon_cache, url);

	if (entry == new_entry)
	{
		if (new_entry)
		{
			/* Don't keep the reference */
			g_object_unref (new_entry);
		}
		return;
	}

	if (entry)
	{
		remove_bookmark_from_cache_entry (dip, entry, bm);
	}

	if (new_entry)
	{
		galeon_favicon_cache_entry_set_persistent (new_entry, TRUE);
		add_bookmark_to_cache_entry (dip, new_entry, bm);
		
		g_hash_table_insert (dip->priv->bookmarks, bm, new_entry);
		g_object_unref (new_entry);
	}
	else
	{
		g_hash_table_remove (dip->priv->bookmarks, bm);
	}
}


static GaleonFaviconCacheEntry *
get_cache_entry_for_bookmark (GbGaleonIconProvider *dip, GbBookmark *b)
{
	GaleonFaviconCacheEntry *ret;
	const gchar *url;

	ret = g_hash_table_lookup (dip->priv->bookmarks, b);
	if (ret)
	{
		return ret;
	}

	/* We don't have an entry for this bookmark, so it must be new, lets
	 * get one */
	url = GB_SITE (b)->url;
	if ((!url || url[0] == '\0') && GB_IS_SMART_SITE (b))
	{
		url = GB_SMART_SITE (b)->smarturl;
	}

	ret = galeon_favicon_cache_lookup (dip->priv->favicon_cache, url);
	if (!ret)
	{
		return NULL;
	}

	galeon_favicon_cache_entry_set_persistent (ret, TRUE);

	/* Remember this entry, and add a weak notify on the bookmark so we can
	 * delete it when the bookmark is deleted */
	g_hash_table_insert (dip->priv->bookmarks, b, ret);
	g_object_weak_ref (G_OBJECT (b), 
			   (GWeakNotify)gb_galeon_icon_provider_weak_notify, dip);
	g_signal_connect (G_OBJECT (b), "url-modified",
			  G_CALLBACK (bookmark_url_modified_cb), dip);

	
	add_bookmark_to_cache_entry (dip, ret, b);

	/* Don't keep the ref to the cache entry */
	g_object_unref (ret);

	return ret;
}

static GdkPixbuf *
gb_galeon_icon_provider_get_icon (GbIconProvider *ip, GbBookmark *b)
{
	GbGaleonIconProvider *dip = GB_GALEON_ICON_PROVIDER (ip);
	GdkPixbuf *ret = NULL;
	GaleonFaviconCacheEntry *entry;
	GbBookmark *rb = gb_bookmark_real_bookmark (b);

	/* We only provide icons for sites */
	if (!GB_IS_SITE (b))
	{
		return GB_ICON_PROVIDER_CLASS (gb_galeon_icon_provider_parent_class)->get_icon (ip, b);
	}

	entry = get_cache_entry_for_bookmark (dip, rb);
	if (entry)
	{
		ret = galeon_favicon_cache_entry_get_pixbuf (entry);
	}

	if (!ret)
	{
		return GB_ICON_PROVIDER_CLASS (gb_galeon_icon_provider_parent_class)->get_icon (ip, b);
	}
	else if (gb_bookmark_is_alias (b))
	{
		return gb_icon_provider_make_alias_icon (ip, ret);
	}
	return g_object_ref (ret);
}


static void
gb_galeon_icon_provider_bset_weak_notify (gpointer datap,
					  GObject *where_the_object_was)
{
	GbGaleonIconProvider *dip = GB_GALEON_ICON_PROVIDER (datap);

	dip->priv->sets = g_slist_remove (dip->priv->sets,
					  where_the_object_was);
}

static void
gb_galeon_icon_provider_add_bmks_from_set (GbGaleonIconProvider *gip, GbBookmarkSet *set)
{
	GbIterator *i;
	GbBookmark *bm;

	/* We need pointers to all bookmarks, so that we can set the persistent
	 * flag correctly */
	i = gb_iterator_new (set);
	while ((bm = gb_iterator_next (i)))
	{
		if (GB_IS_SITE (bm))
		{
			get_cache_entry_for_bookmark (gip, bm);
		}
	}
	g_object_unref (i);
}

static void
bookmark_added_cb (GbFolder *f, GbFolder *p, GbBookmark *b, int pos, GbGaleonIconProvider *gip)
{
	if (GB_IS_SITE (b))
	{
		/* Ensure we have an entry for this bookmark */
		get_cache_entry_for_bookmark (gip, b);
	}
}

void
gb_galeon_icon_provider_add_bookmark_set (GbGaleonIconProvider *gip,
					  GbBookmarkSet *set)
{
	GbGaleonIconProviderPrivate *p = gip->priv;
	if (!g_slist_find (p->sets, set))
	{
		p->sets = g_slist_prepend (p->sets, set);
		g_object_weak_ref (G_OBJECT (set), 
				   gb_galeon_icon_provider_bset_weak_notify, gip);

		START_PROFILER ("Adding bookmarks to icon provider");
		gb_galeon_icon_provider_add_bmks_from_set (gip, set);
		STOP_PROFILER ("Adding bookmarks to icon provider");

		g_signal_connect (set->root, "descendant-added",
				  G_CALLBACK (bookmark_added_cb), gip);
	}
}

static void
gb_galeon_icon_provider_set_property (GObject *object,
				      guint prop_id,
				      const GValue *value,
				      GParamSpec *pspec)
{
	GbGaleonIconProvider *dip = GB_GALEON_ICON_PROVIDER (object);

	switch (prop_id)
	{
	case PROP_CACHE:
		dip->priv->favicon_cache = g_value_get_object (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void 
gb_galeon_icon_provider_get_property (GObject *object,
				      guint prop_id,
				      GValue *value,
				      GParamSpec *pspec)
{
	GbGaleonIconProvider *dip = GB_GALEON_ICON_PROVIDER (object);

	switch (prop_id)
	{
	case PROP_CACHE:
		g_value_set_object (value, dip->priv->favicon_cache);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
remove_bm_weak_ref_foreach (gpointer key, gpointer value, gpointer user_data)
{
	g_object_weak_unref (key, (GWeakNotify)gb_galeon_icon_provider_weak_notify, 
			     user_data);

	g_signal_handlers_disconnect_by_func (key, 
					      G_CALLBACK (bookmark_url_modified_cb),
					      user_data);
}

static void
clear_cache_entry_foreach (gpointer key, gpointer value, gpointer user_data)
{
	g_signal_handlers_disconnect_by_func (key, 
					      G_CALLBACK (cache_entry_changed_cb),
					      user_data);

	g_object_unref (key);

	g_list_free (((CacheEntryInfo*)value)->bookmarks);
	g_free (value);
}

static void
gb_galeon_icon_provider_finalize (GObject *o)
{
	GbGaleonIconProvider *dip = GB_GALEON_ICON_PROVIDER (o);
	GbGaleonIconProviderPrivate *p = dip->priv;
	GSList *li;
	
	for (li = p->sets; li; li = li->next)
	{
		g_object_weak_unref (li->data, gb_galeon_icon_provider_bset_weak_notify, dip);
	}
	g_slist_free (p->sets);

	g_hash_table_foreach (p->bookmarks, remove_bm_weak_ref_foreach, o);
	g_hash_table_destroy (p->bookmarks);

	g_hash_table_foreach (p->cache_entries, clear_cache_entry_foreach, o);
	g_hash_table_destroy (p->cache_entries);

	G_OBJECT_CLASS (gb_galeon_icon_provider_parent_class)->finalize (o);
}

static void 
gb_galeon_icon_provider_init (GbGaleonIconProvider *dip)
{
	GbGaleonIconProviderPrivate *p = GB_GALEON_ICON_PROVIDER_GET_PRIVATE (dip);
	dip->priv = p;

	p->cache_entries = g_hash_table_new (g_direct_hash, g_direct_equal);
	p->bookmarks     = g_hash_table_new (g_direct_hash, g_direct_equal);
}

static void
gb_galeon_icon_provider_class_init (GbGaleonIconProviderClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GbIconProviderClass *provider_class = GB_ICON_PROVIDER_CLASS (klass);

	object_class->finalize = gb_galeon_icon_provider_finalize;
	object_class->set_property = gb_galeon_icon_provider_set_property;
	object_class->get_property = gb_galeon_icon_provider_get_property;

	provider_class->get_icon = gb_galeon_icon_provider_get_icon;

	g_object_class_install_property (object_class,
					 PROP_CACHE,
					 g_param_spec_object ("cache",
							      "Favicon cache",
							      "Favicon cache",
							      GALEON_TYPE_FAVICON_CACHE,
							      G_PARAM_READWRITE | 
							      G_PARAM_CONSTRUCT_ONLY));

	g_type_class_add_private (klass, sizeof (GbGaleonIconProviderPrivate));
}


GbGaleonIconProvider *
gb_galeon_icon_provider_new (void)
{
	GbGaleonIconProvider *ret;
	GaleonFaviconCache *cache;
	
	cache = galeon_shell_get_favicon_cache (galeon_shell);

	g_return_val_if_fail (GALEON_IS_FAVICON_CACHE (cache), NULL);

	ret = g_object_new (GB_TYPE_GALEON_ICON_PROVIDER,
			    "cache", cache,
			    NULL);

	return ret;
}
