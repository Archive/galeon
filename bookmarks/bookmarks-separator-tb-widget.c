/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "bookmarks-separator-tb-widget.h"
#include "galeon-marshal.h"
#include "bookmarks-widgets-private.h"

#include <gtk/gtkvseparator.h>

/**
 * Private data
 */
#define GB_SEPARATOR_TB_WIDGET_GET_PRIVATE(object) (G_TYPE_INSTANCE_GET_PRIVATE ((object), \
				       GB_TYPE_SEPARATOR_TB_WIDGET, GbSeparatorTbWidgetPrivate))


struct _GbSeparatorTbWidgetPrivate 
{
	GtkWidget *mainwid;
};

/**
 * Private functions, only availble from this file
 */
static void		gb_separator_tb_widget_finalize_impl	(GObject *o);
static void		gb_separator_tb_widget_rebuild_impl	(GbTbWidget *w);


/**
 * GbSeparatorTbWidget object
 */

G_DEFINE_TYPE (GbSeparatorTbWidget, gb_separator_tb_widget, GB_TYPE_TB_WIDGET);

static void
gb_separator_tb_widget_class_init (GbSeparatorTbWidgetClass *klass)
{
	G_OBJECT_CLASS (klass)->finalize = gb_separator_tb_widget_finalize_impl;
	GB_TB_WIDGET_CLASS (klass)->rebuild = gb_separator_tb_widget_rebuild_impl;

	g_type_class_add_private (klass, sizeof (GbSeparatorTbWidgetPrivate));
}

static void 
gb_separator_tb_widget_init (GbSeparatorTbWidget *w)
{
	GbSeparatorTbWidgetPrivate *p = GB_SEPARATOR_TB_WIDGET_GET_PRIVATE (w);
	w->priv = p;
}

static void
gb_separator_tb_widget_finalize_impl (GObject *o)
{
	G_OBJECT_CLASS (gb_separator_tb_widget_parent_class)->finalize (o);
}

GbSeparatorTbWidget *
gb_separator_tb_widget_new (GbSeparator *separator)
{
	GbSeparatorTbWidget *ret = g_object_new (GB_TYPE_SEPARATOR_TB_WIDGET, "bookmark", separator, NULL);
	return ret;
}

static void
gb_separator_tb_widget_rebuild_impl (GbTbWidget *gtw)
{
	GbSeparatorTbWidgetPrivate *p = GB_SEPARATOR_TB_WIDGET (gtw)->priv;

	if (!p->mainwid)
	{
		p->mainwid = gtk_vseparator_new ();
		gtk_widget_show (p->mainwid);
		gtk_box_pack_start (GTK_BOX (gtw), p->mainwid, FALSE, FALSE, 0);
	}

	gb_tb_widget_setup_context_menu (gtw, p->mainwid);
}

