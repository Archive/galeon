/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __galeon_altenate_css_menu_
#define __galeon_altenate_css_menu_

#include <glib-object.h>
#include "galeon-window.h"

/* object forward declarations */

typedef struct _GaleonCssMenu GaleonCssMenu;
typedef struct _GaleonCssMenuClass GaleonCssMenuClass;
typedef struct _GaleonCssMenuPrivate GaleonCssMenuPrivate;

/**
 * GaleonCssMenu object
 */

#define GALEON_TYPE_CSS_MENU		(galeon_css_menu_get_type())
#define GALEON_CSS_MENU(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), GALEON_TYPE_CSS_MENU,\
					 GaleonCssMenu))
#define GALEON_CSS_MENU_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), GALEON_TYPE_CSS_MENU,\
					 GaleonCssMenuClass))
#define GALEON_IS_CSS_MENU(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), GALEON_TYPE_CSS_MENU))
#define GALEON_IS_CSS_MENU_ITEM_CLASS(klass) \
 					(G_TYPE_CHECK_CLASS_TYPE((klass), GALEON_TYPE_CSS_MENU))
#define GALEON_CSS_MENU_GET_CLASS(obj) 	(G_TYPE_INSTANCE_GET_CLASS((obj), GALEON_TYPE_CSS_MENU,\
					 GaleonCssMenuClass))

struct _GaleonCssMenuClass
{
	GObjectClass parent_class;
};

/* Remember: fields are public read-only */
struct _GaleonCssMenu
{
	GObject parent_object;
	
	GaleonCssMenuPrivate *priv;
};

GType		galeon_css_menu_get_type			(void);
GaleonCssMenu *	galeon_css_menu_new				(GaleonWindow *window);

#endif
