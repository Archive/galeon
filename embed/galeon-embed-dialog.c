/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "galeon-embed-dialog.h"
#include "galeon-embed-shell.h"

static void
galeon_embed_dialog_class_init (GaleonEmbedDialogClass *klass);
static void
galeon_embed_dialog_init (GaleonEmbedDialog *window);
static void
galeon_embed_dialog_finalize (GObject *object);
static void
galeon_embed_dialog_get_property (GObject *object,
             	            	  guint prop_id,
                            	  GValue *value,
                            	  GParamSpec *pspec);
static void
galeon_embed_dialog_set_property (GObject *object,
             	            	  guint prop_id,
                            	  const GValue *value,
                            	  GParamSpec *pspec);

enum
{
	PROP_0,
	PROP_GALEON_EMBED
};

#define GALEON_EMBED_DIALOG_GET_PRIVATE(object) (G_TYPE_INSTANCE_GET_PRIVATE ((object), \
				       GALEON_TYPE_EMBED_DIALOG, GaleonEmbedDialogPrivate))


struct GaleonEmbedDialogPrivate
{
	GaleonEmbed *embed;
};

static GObjectClass *parent_class = NULL;

GType 
galeon_embed_dialog_get_type (void)
{
        static GType galeon_embed_dialog_type = 0;

        if (galeon_embed_dialog_type == 0)
        {
                static const GTypeInfo our_info =
                {
                        sizeof (GaleonEmbedDialogClass),
                        NULL, /* base_init */
                        NULL, /* base_finalize */
                        (GClassInitFunc) galeon_embed_dialog_class_init,
                        NULL,
                        NULL, /* class_data */
                        sizeof (GaleonEmbedDialog),
                        0, /* n_preallocs */
                        (GInstanceInitFunc) galeon_embed_dialog_init
                };

                galeon_embed_dialog_type = g_type_register_static (GALEON_TYPE_DIALOG,
                                                             	   "GaleonEmbedDialog",
                                                             	   &our_info, 0);
        }

        return galeon_embed_dialog_type;
}

static void
galeon_embed_dialog_class_init (GaleonEmbedDialogClass *klass)
{
        GObjectClass *object_class = G_OBJECT_CLASS (klass);

        parent_class = g_type_class_peek_parent (klass);

        object_class->finalize = galeon_embed_dialog_finalize;
	object_class->set_property = galeon_embed_dialog_set_property;
	object_class->get_property = galeon_embed_dialog_get_property;
	
	g_object_class_install_property (object_class,
					 PROP_GALEON_EMBED,
                                         g_param_spec_object ("GaleonEmbed",
                                                              "GaleonEmbed",
                                                              "Galeon Embed",
                                                              G_TYPE_OBJECT,
                                                              G_PARAM_READWRITE));

	g_type_class_add_private (klass, sizeof (GaleonEmbedDialogPrivate));
}

static void
galeon_embed_dialog_init (GaleonEmbedDialog *dialog)
{
        dialog->priv = GALEON_EMBED_DIALOG_GET_PRIVATE (dialog);

	dialog->priv->embed = NULL;
}

static void
unset_embed (GaleonEmbedDialog *dialog)
{
	if (dialog->priv->embed != NULL)
	{
		g_object_remove_weak_pointer (G_OBJECT (dialog->priv->embed),
					      (gpointer *)&dialog->priv->embed);
	}
}

static void
galeon_embed_dialog_finalize (GObject *object)
{
        GaleonEmbedDialog *dialog;

        g_return_if_fail (object != NULL);
        g_return_if_fail (GALEON_IS_EMBED_DIALOG (object));

        dialog = GALEON_EMBED_DIALOG (object);

        g_return_if_fail (dialog->priv != NULL);

	unset_embed (dialog);

        G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
galeon_embed_dialog_set_property (GObject *object,
                              	  guint prop_id,
                            	  const GValue *value,
                            	  GParamSpec *pspec)
{
        GaleonEmbedDialog *d = GALEON_EMBED_DIALOG (object);

        switch (prop_id)
        {
                case PROP_GALEON_EMBED:
                        galeon_embed_dialog_set_embed (d, g_value_get_object (value));
                        break;
        }
}

static void
galeon_embed_dialog_get_property (GObject *object,
             	            	  guint prop_id,
                            	  GValue *value,
                            	  GParamSpec *pspec)
{
        GaleonEmbedDialog *d = GALEON_EMBED_DIALOG (object);

        switch (prop_id)
        {
                case PROP_GALEON_EMBED:
                        g_value_set_object (value, d->priv->embed);
                        break;
        }
}

GaleonEmbedDialog *
galeon_embed_dialog_new (GaleonEmbed *embed)
{
	return GALEON_EMBED_DIALOG (g_object_new (GALEON_TYPE_EMBED_DIALOG, 
						  "GaleonEmbed", embed,
						  NULL));
}

GaleonEmbedDialog *
galeon_embed_dialog_new_with_parent (GtkWidget *parent_window,
				     GaleonEmbed *embed)
{
	return GALEON_EMBED_DIALOG (g_object_new 
				    (GALEON_TYPE_EMBED_DIALOG,
				     "ParentWindow", parent_window,
				     "GaleonEmbed", embed,
				     NULL));
}

void
galeon_embed_dialog_set_embed (GaleonEmbedDialog *dialog,
			       GaleonEmbed *embed)
{
	unset_embed (dialog);
	dialog->priv->embed = embed;
	g_object_add_weak_pointer (G_OBJECT (dialog->priv->embed),
				   (gpointer *)&dialog->priv->embed);
	g_object_notify (G_OBJECT (dialog), "GaleonEmbed");
}

GaleonEmbed *
galeon_embed_dialog_get_embed (GaleonEmbedDialog *dialog)
{
	return dialog->priv->embed;
}
