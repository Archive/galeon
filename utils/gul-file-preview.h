/*
 *  Copyright (C) 2004  Tommi Komulainen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#ifndef GUL_FILE_PREVIEW_H
#define GUL_FILE_PREVIEW_H 1

#include <gtk/gtkcontainer.h>
#include <libgnomevfs/gnome-vfs-file-info.h>

G_BEGIN_DECLS

#define GUL_TYPE_FILE_PREVIEW		 (gul_file_preview_get_type ())
#define GUL_FILE_PREVIEW(obj)		 (G_TYPE_CHECK_INSTANCE_CAST ((obj), GUL_TYPE_FILE_PREVIEW, GulFilePreview))
#define GUL_FILE_PREVIEW_CLASS(klass)	 (G_TYPE_CHECK_CLASS_CAST ((klass), GUL_TYPE_FILE_PREVIEW, GulFilePreviewClass))
#define GUL_IS_FILE_PREVIEW(obj)	 (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GUL_TYPE_FILE_PREVIEW))
#define GUL_IS_FILE_PREVIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GUL_TYPE_FILE_PREVIEW))
#define GUL_FILE_PREVIEW_GET_CLASS(obj)	 (G_TYPE_INSTANCE_GET_CLASS ((obj), GUL_TYPE_FILE_PREVIEW, GulFilePreviewClass))

typedef struct _GulFilePreview	GulFilePreview;
typedef struct _GulFilePreviewClass GulFilePreviewClass;

struct _GulFilePreview
{
	GtkContainer	parent;
};

struct _GulFilePreviewClass
{
	GtkContainerClass	parent_class;
};

GType      	gul_file_preview_get_type      (void) G_GNUC_CONST;

GtkWidget*	gul_file_preview_new           (void);

void		gul_file_preview_set_file_info (GulFilePreview   *self,
		                                const char       *uri,
						GnomeVFSFileInfo *file_info);

G_END_DECLS

#endif
