/*
 *  Copyright (C) 2002 Jorn Baayen
 *  Copyright (C) 2005 Crispin Flowerday
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <glib-object.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#ifndef __GALEON_FAVICON_CACHE_H
#define __GALEON_FAVICON_CACHE_H

G_BEGIN_DECLS

#define GALEON_TYPE_FAVICON_CACHE         (galeon_favicon_cache_get_type ())
#define GALEON_FAVICON_CACHE(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), GALEON_TYPE_FAVICON_CACHE, GaleonFaviconCache))
#define GALEON_FAVICON_CACHE_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST ((k), GALEON_TYPE_FAVICON_CACHE, GaleonFaviconCacheClass))
#define GALEON_IS_FAVICON_CACHE(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), GALEON_TYPE_FAVICON_CACHE))
#define GALEON_IS_FAVICON_CACHE_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), GALEON_TYPE_FAVICON_CACHE))
#define GALEON_FAVICON_CACHE_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), GALEON_TYPE_FAVICON_CACHE, GaleonFaviconCacheClass))

typedef struct _GaleonFaviconCachePrivate GaleonFaviconCachePrivate;
typedef struct _GaleonFaviconCacheClass GaleonFaviconCacheClass;
typedef struct _GaleonFaviconCache GaleonFaviconCache;

#define GALEON_TYPE_FAVICON_CACHE_ENTRY         (galeon_favicon_cache_entry_get_type ())
#define GALEON_FAVICON_CACHE_ENTRY(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), GALEON_TYPE_FAVICON_CACHE_ENTRY, GaleonFaviconCacheEntry))
#define GALEON_FAVICON_CACHE_ENTRY_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST ((k), GALEON_TYPE_FAVICON_CACHE_ENTRY, GaleonFaviconCacheEntryClass))
#define GALEON_IS_FAVICON_CACHE_ENTRY(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), GALEON_TYPE_FAVICON_CACHE_ENTRY))
#define GALEON_IS_FAVICON_CACHE_ENTRY_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), GALEON_TYPE_FAVICON_CACHE_ENTRY))
#define GALEON_FAVICON_CACHE_ENTRY_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), GALEON_TYPE_FAVICON_CACHE_ENTRY, GaleonFaviconCacheEntryClass))

typedef struct _GaleonFaviconCacheEntryPrivate GaleonFaviconCacheEntryPrivate;
typedef struct _GaleonFaviconCacheEntryClass GaleonFaviconCacheEntryClass;
typedef struct _GaleonFaviconCacheEntry GaleonFaviconCacheEntry;

struct _GaleonFaviconCache
{
	GObject parent;

	GaleonFaviconCachePrivate *priv;
};

struct _GaleonFaviconCacheClass
{
	GObjectClass parent_class;
};

struct _GaleonFaviconCacheEntry
{
	GObject parent;

	GaleonFaviconCacheEntryPrivate *priv;
};

struct _GaleonFaviconCacheEntryClass
{
	GObjectClass parent_class;
};


GType               galeon_favicon_cache_get_type        (void);

GaleonFaviconCache *galeon_favicon_cache_new             (void);

void                galeon_favicon_cache_insert_from_url (GaleonFaviconCache *cache,
							  const char *url,
							  const char *favicon_url);

GaleonFaviconCacheEntry * galeon_favicon_cache_lookup  (GaleonFaviconCache *cache,
							const char *url);


GType        galeon_favicon_cache_entry_get_type       (void);

GdkPixbuf *  galeon_favicon_cache_entry_get_pixbuf     (GaleonFaviconCacheEntry *entry);

void         galeon_favicon_cache_entry_set_persistent (GaleonFaviconCacheEntry *entry,
							gboolean persistent);

gboolean     galeon_favicon_cache_entry_get_persistent (GaleonFaviconCacheEntry *entry);

G_END_DECLS

#endif /* __GALEON_FAVICON_CACHE_H */
