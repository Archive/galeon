/*
 *  Copyright (C) 2002 Jorn Baayen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <glib-object.h>
#include <gtk/gtkimage.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include "galeon-favicon.h"
#include "galeon-embed.h"

#ifndef __GALEON_EMBED_FAVICON_H
#define __GALEON_EMBED_FAVICON_H

G_BEGIN_DECLS

#define GALEON_TYPE_EMBED_FAVICON         (galeon_embed_favicon_get_type ())
#define GALEON_EMBED_FAVICON(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), GALEON_TYPE_EMBED_FAVICON, GaleonEmbedFavicon))
#define GALEON_EMBED_FAVICON_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST ((k), GALEON_TYPE_EMBED_FAVICON, GaleonEmbedFaviconClass))
#define GALEON_IS_EMBED_FAVICON(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), GALEON_TYPE_EMBED_FAVICON))
#define GALEON_IS_EMBED_FAVICON_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), GALEON_TYPE_EMBED_FAVICON))
#define GALEON_EMBED_FAVICON_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), GALEON_TYPE_EMBED_FAVICON, GaleonEmbedFaviconClass))

typedef struct GaleonEmbedFaviconPrivate GaleonEmbedFaviconPrivate;

typedef struct
{
	GaleonFavicon parent;

	GaleonEmbedFaviconPrivate *priv;
} GaleonEmbedFavicon;

typedef struct
{
	GaleonFaviconClass parent_class;
} GaleonEmbedFaviconClass;

GType        galeon_embed_favicon_get_type  (void);

GtkWidget   *galeon_embed_favicon_new       (GaleonEmbed *embed);

void         galeon_embed_favicon_set_embed (GaleonEmbedFavicon *favicon,
					     GaleonEmbed *embed);

GaleonEmbed *galeon_embed_favicon_get_embed (GaleonEmbedFavicon *favicon);

G_END_DECLS

#endif /* __GALEON_EMBED_FAVICON_H */
