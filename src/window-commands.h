/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "galeon-window.h"
#include "galeon-embed-utils.h"

#include <gtk/gtkaction.h>

void window_cmd_edit_find	(GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_file_print 	(GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_go_stop 	(GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_go_myportal 	(GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_go_location 	(GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_go_reload 	(GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_new	 	(GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_new_window 	(GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_new_tab 	(GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_bookmarks_add_interactive(GtkAction *action, 
			    		  GaleonWindow *window);

void window_cmd_bookmarks_add_tab_folder (GtkAction *action, 
			    		  GaleonWindow *window);

void window_cmd_bookmarks_edit 	(GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_file_open 	(GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_file_save_as 	(GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_file_send_to 	(GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_file_quit	(GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_file_close_tab	(GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_edit_cut 	(GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_edit_copy 	(GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_edit_paste 	(GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_edit_select_all (GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_edit_find_next	(GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_edit_find_prev	(GtkAction *action, 
			    	 GaleonWindow *window);
void 
window_cmd_set_permission	(GtkAction *action, 
			    	 GaleonWindow *window);
void 
window_cmd_show_java_console	(GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_show_js_console	(GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_view_zoom_in	(GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_view_zoom_out	(GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_view_zoom_normal(GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_view_page_source(GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_view_page_info  (GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_tools_history   (GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_tools_pdm	(GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_edit_prefs      (GtkAction *action, 
			    	 GaleonWindow *window);

void 
window_cmd_settings_toolbar_editor	(GtkAction *action, 
					 GaleonWindow *window);

void window_cmd_help_about      (GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_tabs_next       (GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_tabs_previous   (GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_tabs_move_left  (GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_tabs_move_right (GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_tabs_clone      (GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_tabs_detach     (GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_tabs_close_other_tabs (GtkAction *action,
				       GaleonWindow *window);

void window_cmd_help_manual     (GtkAction *action, 
			    	 GaleonWindow *window);

void window_cmd_help_plugins     (GtkAction *action, 
			    	 GaleonWindow *window);

