/*
 *  Copyright (C) 2004 Galeon Developers
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* IMPORTANT: This file contains functions that require the old style
 * string api, i.e those that CAN'T use nsEmbedString
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_NSSTRING_INTERNAL
/***** BEGIN UNSAFE HEADERS ******/
#define MOZILLA_INTERNAL_API
#include <nsICSSLoader.h>
#include <nsICSSStyleSheet.h>
#include <nsIHTMLDocument.h>
#endif

#include <nsIDocument.h>
#include <nsIPresShell.h>
#include <nsIStyleSheet.h>
#include <nsIScriptContext.h>
#include <nsStringAPI.h>

#include "MozillaPrivate.h"
#ifdef HAVE_NSSTRING_INTERNAL
#undef MOZILLA_INTERNAL_API
/***** END UNSAFE HEADERS ******/
#endif

#include <nsServiceManagerUtils.h>
#include <nsISupportsPrimitives.h>
#include <nsIInterfaceRequestorUtils.h>
#include <nsIPrintSettingsService.h>
#include <nsIScriptGlobalObject.h>
#include <nsISimpleEnumerator.h>
#include <nsIStringEnumerator.h>
#include <nsIDocShell.h>
#include <nsIDOMWindow.h>
#include <nsIDOMNSDocument.h>
#include <nsCOMPtr.h>
#ifdef HAVE_NSISTYLESHEETSERVICE_H
#include <nsIStyleSheetService.h>
#endif

#include "GaleonUtils.h"

nsresult
MozillaPrivate::LoadOverrideStyleSheet (nsIDocShell *aDocShell, nsIURI *aUri,
					nsIStyleSheet**aStyleSheet)
{
	nsresult rv;

#ifdef HAVE_NSISTYLESHEETSERVICE_H
	nsCOMPtr<nsIStyleSheetService> service
			(do_GetService ("@mozilla.org/content/style-sheet-service;1", &rv));
	NS_ENSURE_SUCCESS (rv, rv);

	PRBool isRegistered = PR_FALSE;
	rv = service->SheetRegistered (aUri, nsIStyleSheetService::USER_SHEET,
				       &isRegistered);
	if (NS_SUCCEEDED (rv) && isRegistered)
	{
		rv = service->UnregisterSheet (aUri, nsIStyleSheetService::USER_SHEET);
	}

	rv = service->LoadAndRegisterSheet (aUri, nsIStyleSheetService::AGENT_SHEET);
	if (NS_FAILED (rv))
	{
		g_warning ("Registering the user stylesheet failed (rv=%x)!\n", rv);
	}

	return rv;
#else
	nsCOMPtr<nsIPresShell> presShell;
	aDocShell->GetPresShell (getter_AddRefs(presShell));
	NS_ENSURE_TRUE (presShell, NS_ERROR_FAILURE);

	nsIDocument *doc = presShell->GetDocument();
	NS_ENSURE_TRUE (doc, NS_ERROR_FAILURE);

	nsICSSLoader *loader = doc->CSSLoader();
	NS_ENSURE_TRUE (loader, NS_ERROR_FAILURE);

	/* load sheet */
	nsCOMPtr<nsICSSStyleSheet> sheet;
#ifdef HAVE_NSICSSLOADER_LOADSHEETSYNC
	rv = loader->LoadSheetSync(aUri, getter_AddRefs(sheet));
#else
	rv = loader->LoadAgentSheet(aUri, getter_AddRefs(sheet));
#endif
	NS_ENSURE_SUCCESS (rv, rv);

	/* catch stylesheet stuff and apply by appending it as a override
	 * sheet and enabling stylesheets */
	nsCOMPtr<nsIStyleSheet> styleSheet = do_QueryInterface(sheet);
	NS_ENSURE_TRUE (styleSheet, NS_ERROR_FAILURE);
	
	/* Override - Agent */
	presShell->AddOverrideStyleSheet(styleSheet);
	styleSheet->SetOwningDocument(doc);

	/* notify mozilla that stylesheets have changed */
	styleSheet->SetEnabled(PR_FALSE);
	styleSheet->SetEnabled(PR_TRUE);
	/* This is Right[tm], but not working for some people (?!)
	doc->SetStyleSheetDisabledState(styleSheet, PR_FALSE);
	*/

	NS_ADDREF(*aStyleSheet = styleSheet);
	
	return NS_OK;
#endif
}

nsresult
MozillaPrivate::RemoveOverrideStyleSheet (nsIDocShell *aDocShell,
#if HAVE_NSISTYLESHEETSERVICE_H
					  nsIURI *aUri)
#else
					  nsIStyleSheet* aStyleSheet)
#endif
{
#if HAVE_NSISTYLESHEETSERVICE_H
	nsresult rv;

	nsCOMPtr<nsIStyleSheetService> service
			(do_GetService ("@mozilla.org/content/style-sheet-service;1", &rv));
	NS_ENSURE_SUCCESS (rv, rv);

	PRBool isRegistered = PR_FALSE;
	rv = service->SheetRegistered (aUri, nsIStyleSheetService::AGENT_SHEET,
				       &isRegistered);
	if (NS_SUCCEEDED (rv) && isRegistered)
	{
		rv = service->UnregisterSheet (aUri, nsIStyleSheetService::AGENT_SHEET);
	}
	if (NS_FAILED (rv))
	{
		g_warning ("Unregistering the user stylesheet failed (rv=%x)!\n", rv);
	}

	return rv;
#else
	nsCOMPtr<nsIPresShell> ps;
	aDocShell->GetPresShell (getter_AddRefs(ps));
	NS_ENSURE_TRUE (ps, NS_ERROR_FAILURE);

	/* remove it */
	ps->RemoveOverrideStyleSheet (aStyleSheet);
	aStyleSheet->SetEnabled(PR_FALSE);

	return NS_OK;
#endif
}


nsresult
MozillaPrivate::EvaluateJS (nsIDOMWindow *aDOMWindow, const nsAString &aScript,
			    nsAString& result, PRBool *isUndefined)
{
	nsresult rv;

	nsCOMPtr<nsIScriptGlobalObject> globalObject = 
		do_QueryInterface (aDOMWindow);
	NS_ENSURE_TRUE (globalObject, NS_ERROR_FAILURE);

	nsIScriptContext *context = globalObject->GetContext();
	NS_ENSURE_TRUE (context, NS_ERROR_FAILURE);

	context->SetProcessingScriptTag(PR_TRUE);

	*isUndefined = PR_TRUE;

	nsAutoString ret;
	rv = context->EvaluateString(aScript, nsnull, nsnull, nsnull,
				     0, nsnull, &ret, isUndefined);  

	context->SetProcessingScriptTag(PR_FALSE);
	NS_ENSURE_SUCCESS (rv, rv);
	
	result = ret;
	return NS_OK;
}


nsresult
MozillaPrivate::GetCompatibilityMode (nsIDOMNSDocument *aDoc,
				      EmbedPageRenderMode *aMode)
{
#ifdef HAVE_NSIDOCUMENT_GETCOMPATIBILITYMODE
	nsCOMPtr<nsIDocument> htmlDoc = do_QueryInterface (aDoc);
#else
	nsCOMPtr<nsIHTMLDocument> htmlDoc = do_QueryInterface (aDoc);
#endif
	NS_ENSURE_TRUE(htmlDoc, NS_ERROR_FAILURE);

	nsCompatibility compat = htmlDoc->GetCompatibilityMode();
	switch (compat)
	{
	case eCompatibility_FullStandards:
		*aMode = EMBED_RENDER_FULL_STANDARDS;
		break;
	case eCompatibility_AlmostStandards:
		*aMode = EMBED_RENDER_ALMOST_STANDARDS;
		break;
	case eCompatibility_NavQuirks:
		*aMode = EMBED_RENDER_QUIRKS;
		break;
	}

	return NS_OK;
}

