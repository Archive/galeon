/*
 *  Copyright (C) 2001 Philip Langdale
 *  Copyright (C) 2003 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "FilePicker.h"
#include "GaleonUtils.h"

#include <nsCOMPtr.h>
#include <nsCOMPtr.h>
#include <nsServiceManagerUtils.h>
#include <nsComponentManagerUtils.h>
#include <nsIURI.h>
#include <nsIFileURL.h>
#include <nsILocalFile.h>
#include <nsIDOMWindow.h>
#include <nsNetCID.h>
#include <nsXPCOMCID.h>

#include "gul-string.h"
#include "prefs-strings.h"
#include "gul-gui.h"
#include "galeon-debug.h"

#include <glib/gconvert.h>
#include <gtk/gtkfilefilter.h>
#include <gtk/gtkstock.h>
#include <gtk/gtkmessagedialog.h>
#include <glib/gi18n.h>

NS_IMPL_ISUPPORTS1(GFilePicker, nsIFilePicker)

GFilePicker::GFilePicker() : mDialog (nsnull), mMode(nsIFilePicker::modeOpen)
{
	LOG ("GFilePicker ctor (%p)", this);
}

GFilePicker::~GFilePicker()
{
	LOG ("GFilePicker dtor (%p)", this);

	if (mDialog)
	{
		g_object_remove_weak_pointer (G_OBJECT (mDialog), (gpointer *) &mDialog);
		gtk_widget_destroy (GTK_WIDGET (mDialog));
	}
}

/* void init (in nsIDOMWindowInternal parent, in wstring title, in short mode); */
NS_IMETHODIMP GFilePicker::Init(nsIDOMWindow *parent, const nsAString &title, PRInt16 mode)
{
	LOG ("GFilePicker::Init");

	GtkWidget *gparent = GaleonUtils::FindGtkParent (parent);

	mMode = mode;

	GtkFileChooserAction action;
	switch (mode)
	{
		case nsIFilePicker::modeGetFolder:
			action = GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER;
			break;

		case nsIFilePicker::modeOpenMultiple:
		case nsIFilePicker::modeOpen:
			action = GTK_FILE_CHOOSER_ACTION_OPEN;
			break;

		case nsIFilePicker::modeSave:
			action = GTK_FILE_CHOOSER_ACTION_SAVE;
			break;
		default:
			/* Avoid a warning */
			action = GTK_FILE_CHOOSER_ACTION_SAVE;
			g_assert_not_reached ();
			break;
	}



	mDialog = gul_file_chooser_new (GulCString (title).get(), 
					gparent, action,
					CONF_STATE_LAST_UPLOAD_DIR);

	g_object_add_weak_pointer (G_OBJECT (mDialog), (gpointer *) &mDialog);

	if (mode == nsIFilePicker::modeOpenMultiple)
	{
		gtk_file_chooser_set_select_multiple (GTK_FILE_CHOOSER (mDialog), TRUE);
	}

	return NS_OK;
}

/* void appendFilters (in long filterMask); */
NS_IMETHODIMP GFilePicker::AppendFilters(PRInt32 filterMask)
{
	// http://lxr.mozilla.org/seamonkey/source/xpfe/components/filepicker/res/locale/en-US/filepicker.properties
	// http://lxr.mozilla.org/seamonkey/source/xpfe/components/filepicker/src/nsFilePicker.js line 131 ff

	// FIXME: use filters with mimetypes instead of extensions

        NS_ENSURE_TRUE (mDialog, NS_ERROR_FAILURE);

	LOG ("GFilePicker::AppendFilters mask=%d", filterMask);

	if (filterMask & nsIFilePicker::filterAll)
	{
		AppendFilter (_("All files"), "*");
	}
	if (filterMask & nsIFilePicker::filterHTML)
	{
		AppendFilter (_("HTML files"), "*.html; *.htm; *.shtml; *.xhtml");
	}
	if (filterMask & nsIFilePicker::filterText)
	{
		AppendFilter (_("Text files"), "*.txt; *.text");
	}
	if (filterMask & nsIFilePicker::filterImages)
	{
		AppendFilter (_("Image files"), "*.png; *.gif; *.jpeg; *.jpg");
	}
	if (filterMask & nsIFilePicker::filterXML)
	{
		AppendFilter (_("XML files"), "*.xml");
	}
	if (filterMask & nsIFilePicker::filterXUL)
	{
		AppendFilter (_("XUL files"), "*.xul");
	}

	return NS_OK;
}

/* void appendFilter (in wstring title, in wstring filter); */
NS_IMETHODIMP GFilePicker::AppendFilter(const nsAString &title, const nsAString &filter)
{
        NS_ENSURE_TRUE (mDialog, NS_ERROR_FAILURE);

	LOG ("GFilePicker::AppendFilter ()");

	return AppendFilter (GulCString (title).get(), GulCString (filter).get());
}


nsresult GFilePicker::AppendFilter (const char * title, const char * pattern)
{
        NS_ENSURE_TRUE (title, NS_ERROR_FAILURE);
        NS_ENSURE_TRUE (pattern, NS_ERROR_FAILURE);

	char **patterns = g_strsplit (pattern, ";", -1);

	GtkFileFilter *filth = gtk_file_filter_new ();

	for (int i = 0; patterns[i] != NULL; i++)
	{
		gtk_file_filter_add_pattern (filth, patterns[i]);
	}

	gtk_file_filter_set_name (filth, title);
	gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (mDialog), filth);

	g_strfreev (patterns);

	return NS_OK;
}

/* attribute wstring defaultString; */
NS_IMETHODIMP GFilePicker::GetDefaultString(nsAString &aDefaultString)
{
        NS_ENSURE_TRUE (mDialog, NS_ERROR_FAILURE);

	LOG ("GFilePicker::GetDefaultString");

	aDefaultString = mDefaultString;

	return NS_OK;
}

NS_IMETHODIMP GFilePicker::SetDefaultString(const nsAString &aDefaultString)
{
        NS_ENSURE_TRUE (mDialog, NS_ERROR_FAILURE);

	LOG ("GFilePicker::SetDefaultString ()");

	/* Save the default string (i.e. the filename) for the open mode
	 * for when SetDefaultDir is called later */
	mDefaultString.Assign (aDefaultString);

	if (mMode == nsIFilePicker::modeSave)
	{
                if (!mDefaultString.Length()) return NS_ERROR_FAILURE;

		/* set_current_name takes UTF-8, not a filename */
		gtk_file_chooser_set_current_name
			(GTK_FILE_CHOOSER (mDialog),
			 GulCString (mDefaultString).get());
	}

	return NS_OK;
}

/* attribute wstring defaultExtension; */
NS_IMETHODIMP GFilePicker::GetDefaultExtension(nsAString &aDefaultExtension)
{
	LOG ("GFilePicker::GetDefaultExtension");

	return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP GFilePicker::SetDefaultExtension(const nsAString &aDefaultExtension)
{
	LOG ("GFilePicker::SetDefaultExtension()");

	return NS_ERROR_NOT_IMPLEMENTED;
}

/* attribute long filterIndex; */
NS_IMETHODIMP GFilePicker::GetFilterIndex(PRInt32 *aFilterIndex)
{
	LOG ("GFilePicker::GetFilterIndex");

	return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP GFilePicker::SetFilterIndex(PRInt32 aFilterIndex)
{
	LOG ("GFilePicker::SetFilterIndex index=%d", aFilterIndex);

	return NS_ERROR_NOT_IMPLEMENTED;
}

/* attribute nsILocalFile displayDirectory; */
NS_IMETHODIMP GFilePicker::GetDisplayDirectory(nsILocalFile **aDisplayDirectory)
{
        NS_ENSURE_TRUE (mDialog, NS_ERROR_FAILURE);

	LOG ("GFilePicker::GetDisplayDirectory");

	char *dir = gtk_file_chooser_get_current_folder (GTK_FILE_CHOOSER (mDialog));

	if (dir != NULL)
	{
		nsCOMPtr<nsILocalFile> file = do_CreateInstance (NS_LOCAL_FILE_CONTRACTID);
		file->InitWithNativePath (GulDependentCString (dir));
		NS_IF_ADDREF (*aDisplayDirectory = file);
	
		g_free (dir);
	}

	return NS_OK;
}

NS_IMETHODIMP GFilePicker::SetDisplayDirectory(nsILocalFile *aDisplayDirectory)
{
        NS_ENSURE_TRUE (mDialog, NS_ERROR_FAILURE);

	GulCString dir;
	aDisplayDirectory->GetNativePath (dir);

	LOG ("GFilePicker::SetDisplayDirectory to %s", dir.get());
	
	if (mDefaultString.Length() && mMode != nsIFilePicker::modeSave)
	{
		nsEmbedCString defaultString;
		/* NOTE, not UTF8, so can't use GulStrings */
		NS_UTF16ToCString (mDefaultString, NS_CSTRING_ENCODING_NATIVE_FILESYSTEM,
				   defaultString);

		char *filename = g_build_filename (dir.get(), defaultString.get(), NULL);
		LOG ("Setting filename to %s", filename);
                gtk_file_chooser_set_filename (GTK_FILE_CHOOSER (mDialog), filename);
                g_free (filename);
	}
	else
	{
		gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (mDialog),
						     dir.get());
	}

	return NS_OK;
}

/* readonly attribute nsILocalFile file; */
NS_IMETHODIMP GFilePicker::GetFile(nsILocalFile **aFile)
{
        NS_ENSURE_TRUE (mDialog, NS_ERROR_FAILURE);

	LOG ("GFilePicker::GetFile");

	char *filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (mDialog));

	if (filename != NULL)
	{
		nsCOMPtr<nsILocalFile> file = do_CreateInstance (NS_LOCAL_FILE_CONTRACTID);
		file->InitWithNativePath (GulDependentCString (filename));
		NS_IF_ADDREF (*aFile = file);
	
		g_free (filename);
	}

	return NS_OK;
}

/* readonly attribute nsIURI/nsIFileURL fileURL; */
#ifdef HAVE_NSIFILEPICKER_NSIURI
NS_IMETHODIMP GFilePicker::GetFileURL(nsIURI **aFileURL)
#else
NS_IMETHODIMP GFilePicker::GetFileURL(nsIFileURL **aFileURL)
#endif
{
	LOG ("GFilePicker::GetFileURL");

	nsCOMPtr<nsILocalFile> file;
	GetFile (getter_AddRefs(file));
	NS_ENSURE_TRUE (file, NS_ERROR_FAILURE);

	nsCOMPtr<nsIFileURL> fileURL = do_CreateInstance (NS_STANDARDURL_CONTRACTID);
	fileURL->SetFile(file);
	NS_IF_ADDREF(*aFileURL = fileURL);

	return NS_OK;
}

/* readonly attribute nsISimpleEnumerator files; */
NS_IMETHODIMP GFilePicker::GetFiles(nsISimpleEnumerator * *aFiles)
{
	// Not sure if we need to implement it at all, it's used nowhere
	// in mozilla, but I guess a javascript might call it?

	LOG ("GFilePicker::GetFiles");

	return NS_ERROR_NOT_IMPLEMENTED;
}

/* short show (); */
NS_IMETHODIMP GFilePicker::Show(PRInt16 *_retval)
{
	LOG ("GFilePicker::Show");

	gtk_window_set_modal (GTK_WINDOW (mDialog), TRUE);

	gtk_widget_show (GTK_WIDGET (mDialog));

	int response;
	char *filename = NULL;

	do
	{
		response = gtk_dialog_run (GTK_DIALOG (mDialog));

		g_free (filename);
		filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (mDialog));

		LOG ("GFilePicker::Show response=%d, filename=%s", response, filename);
	}
	while (response == GTK_RESPONSE_ACCEPT &&
	       mMode == nsIFilePicker::modeSave &&
	       !gul_gui_confirm_overwrite_file (GTK_WIDGET (mDialog), filename));

	gtk_widget_hide (GTK_WIDGET (mDialog));

	if (response == GTK_RESPONSE_ACCEPT && filename != NULL)
	{
		*_retval = nsIFilePicker::returnOK;
	}
	else
	{
		*_retval = nsIFilePicker::returnCancel;
	}

	g_free (filename);

	return NS_OK;
}
