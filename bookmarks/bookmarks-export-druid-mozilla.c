/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n.h>
#include "bookmarks-export-druid-mozilla.h"
#include "gul-string.h"
#include "gul-general.h"

#include "bookmarks-mozilla.h"


static GHashTable *strings = NULL;

/**
 * Private functions, only availble from this file
 */
static void		gb_export_druid_mozilla_finalize_impl		(GObject *o);
static void		gb_export_druid_mozilla_init_strings 		(void);
static const char *	gb_export_druid_mozilla_get_string_impl		(GbExportDruid *d, 
									 const char *string_id);
static GSList *		gb_export_druid_mozilla_get_locations_impl	(GbExportDruid *d);
static GbIO *		gb_export_druid_mozilla_get_io_impl		(GbExportDruid *d);


/**
 * ExportDruidMozilla object
 */

G_DEFINE_TYPE (GbExportDruidMozilla, gb_export_druid_mozilla, GB_TYPE_EXPORT_DRUID);

static void
gb_export_druid_mozilla_class_init (GbExportDruidMozillaClass *klass)
{
	GB_EXPORT_DRUID_CLASS (klass)->get_locations = gb_export_druid_mozilla_get_locations_impl;
	GB_EXPORT_DRUID_CLASS (klass)->get_io = gb_export_druid_mozilla_get_io_impl;
	GB_EXPORT_DRUID_CLASS (klass)->get_string = gb_export_druid_mozilla_get_string_impl;
	G_OBJECT_CLASS (klass)->finalize = gb_export_druid_mozilla_finalize_impl;

}

static void 
gb_export_druid_mozilla_init (GbExportDruidMozilla *e)
{

}

static void
gb_export_druid_mozilla_finalize_impl (GObject *o)
{
	G_OBJECT_CLASS (gb_export_druid_mozilla_parent_class)->finalize (o);
}

GbExportDruidMozilla *
gb_export_druid_mozilla_new (void)
{
	GbExportDruidMozilla *ret = g_object_new (GB_TYPE_EXPORT_DRUID_MOZILLA, NULL);
	return ret;
}

static const char *
gb_export_druid_mozilla_get_string_impl (GbExportDruid *d, 
					 const char *string_id)
{
	gb_export_druid_mozilla_init_strings ();
	return _(g_hash_table_lookup (strings, string_id));
}

static void
gb_export_druid_mozilla_init_strings (void)
{
	if (!strings)
	{
		static struct 
		{
			const char *id;
			const char *text;
		} strs[] = {
			{ "window title", N_("Mozilla Bookmarks Export Druid") },
			{ "page 1 title", N_("Mozilla Bookmarks Export") },
			{NULL, NULL}
		};			
		int i;
		strings = g_hash_table_new (g_str_hash, g_str_equal);
		for (i = 0; strs[i].id != NULL; ++i)
		{
			g_hash_table_insert (strings, (gpointer) strs[i].id, (gpointer) strs[i].text);
		}
	}
}

static GSList *
gb_export_druid_mozilla_get_locations_impl (GbExportDruid *druid)
{
	GSList *l;
	gchar *dir = g_build_filename (g_get_home_dir (), ".mozilla", NULL);
	l = gul_find_file  (dir, "bookmarks.html", 4);
	if (l == NULL)
	{
		/* give a (wrong) default */
		l = g_slist_prepend (l, g_build_filename (dir, "bookmarks.html", NULL));
	}
	g_free (dir);
	return l;
}

static GbIO *
gb_export_druid_mozilla_get_io_impl (GbExportDruid *druid)
{
	GbIOMozilla *io = gb_io_mozilla_new ();
	return GB_IO (io);
}

