/* -*- mode: c; c-style: k&r; c-basic-offset: 8 -*- */
/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "bookmarks-context-menu-several.h"
#include "bookmarks-clipboard.h"
#include <glib/gi18n.h>
#include "galeon-marshal.h"
#include "gul-gui.h"
#include <gtk/gtkclipboard.h>
#include <gtk/gtkstock.h>
#include <gtk/gtkmain.h>
#include <string.h>

/**
 * Private data
 */
#define GB_CONTEXT_MENU_SEVERAL_GET_PRIVATE(object) (G_TYPE_INSTANCE_GET_PRIVATE ((object), \
				       GB_TYPE_CONTEXT_MENU_SEVERAL, GbContextMenuSeveralPrivate))


struct _GbContextMenuSeveralPrivate 
{
	GSList *bms;
	GtkWidget *menu;
	GbLocationSource *location_source;
};

/**
 * Private functions, only availble from this file
 */
static void		gb_context_menu_several_build_menu		(GbContextMenuSeveral *cm);
static void		gb_context_menu_several_finalize_impl		(GObject *o);

static void		gb_context_menu_several_deactivate_cb		(GtkMenuShell *ms, 
									 GbContextMenuSeveral *cm);

static void		gb_context_menu_several_bookmark_activated_cb	(GbContextMenuSeveral *gm, 
									 GbBookmarkEventActivated *ev,
									 GObject *o);

static void		gb_context_menu_several_remove_cb		(GtkWidget *menu_item, 
									 GbContextMenuSeveral *cm);
static void		gb_context_menu_several_copy_cb			(GtkWidget *menu_item, 
									 GbContextMenuSeveral *cm);
static void		gb_context_menu_several_cut_cb			(GtkWidget *menu_item, 
									 GbContextMenuSeveral *cm);


enum GbContextMenuSeveralSignalsEnum {
	GB_CONTEXT_MENU_SEVERAL_BOOKMARK_ACTIVATED,
	GB_CONTEXT_MENU_SEVERAL_DEACTIVATED,
	GB_CONTEXT_MENU_SEVERAL_LAST_SIGNAL
};
static gint GbContextMenuSeveralSignals[GB_CONTEXT_MENU_SEVERAL_LAST_SIGNAL];

/**
 * ContextMenuSeveral object
 */

G_DEFINE_TYPE (GbContextMenuSeveral, gb_context_menu_several, G_TYPE_OBJECT);

static void
gb_context_menu_several_class_init (GbContextMenuSeveralClass *klass)
{
	G_OBJECT_CLASS (klass)->finalize = gb_context_menu_several_finalize_impl;
	
	GbContextMenuSeveralSignals[GB_CONTEXT_MENU_SEVERAL_BOOKMARK_ACTIVATED] = g_signal_new (
		"bookmark-activated", G_OBJECT_CLASS_TYPE (klass),  
		G_SIGNAL_RUN_FIRST | G_SIGNAL_RUN_LAST | G_SIGNAL_RUN_CLEANUP,
                G_STRUCT_OFFSET (GbContextMenuSeveralClass, gb_context_menu_several_bookmark_activated), 
		NULL, NULL, 
		g_cclosure_marshal_VOID__POINTER,
		G_TYPE_NONE, 1, G_TYPE_POINTER);
	GbContextMenuSeveralSignals[GB_CONTEXT_MENU_SEVERAL_DEACTIVATED] = g_signal_new (
		"deactivated", G_OBJECT_CLASS_TYPE (klass),  
		G_SIGNAL_RUN_FIRST | G_SIGNAL_RUN_LAST | G_SIGNAL_RUN_CLEANUP,
                G_STRUCT_OFFSET (GbContextMenuSeveralClass, gb_context_menu_several_deactivated), 
		NULL, NULL, 
		galeon_marshal_VOID__VOID,
		G_TYPE_NONE, 0);

	g_type_class_add_private (klass, sizeof (GbContextMenuSeveralPrivate));
}

static void 
gb_context_menu_several_init (GbContextMenuSeveral *cm)
{
	GbContextMenuSeveralPrivate *p = GB_CONTEXT_MENU_SEVERAL_GET_PRIVATE (cm);
	cm->priv = p;
}

static void
gb_context_menu_several_finalize_impl (GObject *o)
{
	GbContextMenuSeveral *cm = GB_CONTEXT_MENU_SEVERAL (o);
	GbContextMenuSeveralPrivate *p = cm->priv;

	gb_context_menu_several_set_location_source (cm, NULL);
	
	g_slist_foreach (p->bms, (GFunc) g_object_unref, NULL);
	g_slist_free (p->bms);
	
	G_OBJECT_CLASS (gb_context_menu_several_parent_class)->finalize (o);
	
}

GbContextMenuSeveral *
gb_context_menu_several_new (void)
{
	GbContextMenuSeveral *ret = g_object_new (GB_TYPE_CONTEXT_MENU_SEVERAL, NULL);
	return ret;
}

static void
gb_context_menu_several_deactivate_cb (GtkMenuShell *ms, GbContextMenuSeveral *cm)
{
	g_signal_emit (cm, GbContextMenuSeveralSignals[GB_CONTEXT_MENU_SEVERAL_DEACTIVATED], 0);
	
}

void
gb_context_menu_several_popup (GbContextMenuSeveral *cm, GdkEventButton *event)
{
	GbContextMenuSeveralPrivate *p = cm->priv;
	g_return_if_fail (p->bms && GB_IS_BOOKMARK (p->bms->data));
	
	gb_context_menu_several_build_menu (cm);
	g_object_ref (cm);
	
	g_signal_connect (p->menu, "deactivate", G_CALLBACK (gb_context_menu_several_deactivate_cb), cm);
	
	gtk_menu_popup (GTK_MENU (p->menu), NULL, NULL, NULL, cm, 
			event ? event->button : 0, 
			event ? event->time : gtk_get_current_event_time ());
}

void
gb_context_menu_several_set_bookmarks (GbContextMenuSeveral *cm, GSList *l)
{
	GbContextMenuSeveralPrivate *p = cm->priv;
	
	g_slist_foreach (p->bms, (GFunc) g_object_unref, NULL);
	g_slist_free (p->bms);

	p->bms = g_slist_copy (l);
	g_slist_foreach (p->bms, (GFunc) g_object_ref, NULL);
}

static void
gb_context_menu_several_build_menu (GbContextMenuSeveral *cm)
{
	GbContextMenuSeveralPrivate *p = cm->priv;
	gboolean autogenerated_parent = FALSE;
	const GSList *li;
	GtkWidget *item;

	g_return_if_fail (p->bms && GB_IS_BOOKMARK (p->bms->data));

	for (li = p->bms; li; li = li->next)
	{
	     GbBookmark *bi = li->data;
	     if (bi->parent && gb_folder_is_autogenerated (bi->parent))
	     {
		  autogenerated_parent = TRUE;
		  break;
	     }
	}
	
	if (p->menu)
	{
		g_object_unref (p->menu);
	}
	
	p->menu = gtk_menu_new ();
	
	item = gul_gui_append_new_menuitem_stock
		(p->menu, GTK_STOCK_CUT,
		 G_CALLBACK (gb_context_menu_several_cut_cb), cm);
	if (autogenerated_parent)
	{
		gtk_widget_set_sensitive (item, FALSE);
	}
	gul_gui_append_new_menuitem_stock
		(p->menu, GTK_STOCK_COPY,
		 G_CALLBACK (gb_context_menu_several_copy_cb), cm);
	
	gul_gui_append_separator (p->menu);
	
	item = gul_gui_append_new_menuitem_stock
		(p->menu, GTK_STOCK_DELETE, 
		 G_CALLBACK (gb_context_menu_several_remove_cb), cm);
	if (autogenerated_parent)
	{
		gtk_widget_set_sensitive (item, FALSE);
	}
}

/**
 * Creates and pops up a context menu.
 *
 * l: 			The list of bookmarks
 * event:		Optional button event
 * location_source:	Optional location source (for adding bookmarks)
 * creator:		The object that requests the context menu. It must have a 
 *			bookmark-activated signal, wich will be emitted if neccesary
 */
void
gb_context_menu_several_quick (GSList *l, GdkEventButton *event, GbLocationSource *location_source,
			       GObject *creator)
{
	GbContextMenuSeveral *cm = gb_context_menu_several_new_quick (l, location_source, creator);
	gb_context_menu_several_popup (cm, event);
	g_object_unref (cm);
}

/**
 * Creates a context menu that will forward activation signals.
 *
 * l: 			The list of bookmarks
 * location_source:	Optional location source (for adding bookmarks)
 * creator:		The object that requests the context menu. It must have a 
 *			bookmark-activated signal, wich will be emitted if neccesary
 */
GbContextMenuSeveral *
gb_context_menu_several_new_quick  (GSList *l, GbLocationSource *location_source,
				    GObject *creator)
{
	GbContextMenuSeveral *cm = gb_context_menu_several_new ();
	gb_context_menu_several_set_bookmarks (cm, l);
	gb_context_menu_several_set_location_source (cm, location_source);
	if (creator)
	{
		g_signal_connect (cm, "bookmark-activated", 
				  G_CALLBACK (gb_context_menu_several_bookmark_activated_cb), 
				  creator);
	}
	return cm;
}

static void
gb_context_menu_several_bookmark_activated_cb (GbContextMenuSeveral *gm, 
					       GbBookmarkEventActivated *ev,
					       GObject *o)
{
	g_signal_emit_by_name (o, "bookmark-activated", ev);
}

static void
gb_context_menu_several_remove_cb (GtkWidget *menu_item, GbContextMenuSeveral *cm)
{
	GbContextMenuSeveralPrivate *p = cm->priv;
	
	g_slist_foreach (p->bms, (GFunc) gb_bookmark_unparent_safe, NULL);
	
	g_object_unref (cm);
}

static void
gb_context_menu_several_copy_cb	(GtkWidget *menu_item, 
				 GbContextMenuSeveral *cm)
{
	GbContextMenuSeveralPrivate *p = cm->priv;
	GbClipboard *c = gb_clipboard_get_shared ();
	GSList *l = gb_util_remove_descendants_from_list (p->bms);
	GSList *li;

	gb_clipboard_clear (c);
	for (li = l; li; li = li->next)
	{
		gb_clipboard_add (c, li->data);
	}	
	
	g_slist_free (l);
	g_object_unref (c);
	g_object_unref (cm);
}

static void
gb_context_menu_several_cut_cb	(GtkWidget *menu_item, 
				 GbContextMenuSeveral *cm)
{
	GbContextMenuSeveralPrivate *p = cm->priv;
	GbClipboard *c = gb_clipboard_get_shared ();
	GSList *l = gb_util_remove_descendants_from_list (p->bms);
	GSList *li;

	gb_clipboard_clear (c);
	for (li = l; li; li = li->next)
	{
		gb_clipboard_add (c, li->data);
		gb_bookmark_unparent_safe (li->data);
	}	
	
	g_slist_free (l);
	g_object_unref (c);
	g_object_unref (cm);
}

void
gb_context_menu_several_set_location_source (GbContextMenuSeveral *cm, GbLocationSource *src)
{
	GbContextMenuSeveralPrivate *p = cm->priv;
	
	if (p->location_source)
	{
		g_object_remove_weak_pointer (G_OBJECT (p->location_source),
					      (gpointer *) &p->location_source);
	}

	p->location_source = src;

	if (p->location_source)
	{
		g_object_add_weak_pointer (G_OBJECT (p->location_source), 
					   (gpointer *) &p->location_source);
	}
}
