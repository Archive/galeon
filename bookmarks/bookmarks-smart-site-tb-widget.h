/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __bookmarks_smart_site_tb_widget_h
#define __bookmarks_smart_site_tb_widget_h

#include <glib.h>
#include <glib-object.h>
#include "bookmarks-tb-widget.h"

/* object forward declarations */

typedef struct _GbSmartSiteTbWidget GbSmartSiteTbWidget;
typedef struct _GbSmartSiteTbWidgetClass GbSmartSiteTbWidgetClass;
typedef struct _GbSmartSiteTbWidgetPrivate GbSmartSiteTbWidgetPrivate;

/**
 * GbSmartSiteTbWidget object
 */

#define GB_TYPE_SMART_SITE_TB_WIDGET		(gb_smart_site_tb_widget_get_type())
#define GB_SMART_SITE_TB_WIDGET(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), \
						 GB_TYPE_SMART_SITE_TB_WIDGET,\
						 GbSmartSiteTbWidget))
#define GB_SMART_SITE_TB_WIDGET_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), \
						 GB_TYPE_SMART_SITE_TB_WIDGET,\
						 GbSmartSiteTbWidgetClass))
#define GB_IS_SMART_SITE_TB_WIDGET(object)	(G_TYPE_CHECK_INSTANCE_TYPE((object), \
						 GB_TYPE_SMART_SITE_TB_WIDGET))
#define GB_IS_SMART_SITE_TB_WIDGET_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), \
						 GB_TYPE_SMART_SITE_TB_WIDGET))
#define GB_SMART_SITE_TB_WIDGET_GET_CLASS(obj) 	(G_TYPE_INSTANCE_GET_CLASS((obj), \
						 GB_TYPE_SMART_SITE_TB_WIDGET,\
						 GbSmartSiteTbWidgetClass))

struct _GbSmartSiteTbWidgetClass 
{
	GbTbWidgetClass parent_class;
};

/* Remember: fields are public read-only */
struct _GbSmartSiteTbWidget
{
	GbTbWidget parent_object;

	GbSmartSiteTbWidgetPrivate *priv;
};

GType			gb_smart_site_tb_widget_get_type	(void);
GbSmartSiteTbWidget *	gb_smart_site_tb_widget_new		(GbSmartSite *site);

#endif
