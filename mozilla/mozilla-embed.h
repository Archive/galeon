/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef MOZILLA_EMBED_H
#define MOZILLA_EMBED_H

#include "galeon-embed-types.h"
#include "galeon-embed.h"

#define MOZILLA_CLIENT
#include <gtkmozembed.h>
#undef MOZILLA_CLIENT

#include <glib-object.h>
#include <glib.h>

G_BEGIN_DECLS

typedef struct MozillaEmbedClass MozillaEmbedClass;

#define MOZILLA_TYPE_EMBED             (mozilla_embed_get_type ())
#define MOZILLA_EMBED(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), MOZILLA_TYPE_EMBED, MozillaEmbed))
#define MOZILLA_EMBED_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), MOZILLA_TYPE_EMBED, MozillaEmbedClass))
#define MOZILLA_IS_EMBED(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MOZILLA_TYPE_EMBED))
#define MOZILLA_IS_EMBED_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), MOZILLA_EMBED))
#define MOZILLA_EMBED_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), MOZILLA_TYPE_EMBED, MozillaEmbedClass))

typedef struct MozillaEmbed MozillaEmbed;
typedef struct MozillaEmbedPrivate MozillaEmbedPrivate;

struct MozillaEmbed
{
        GtkMozEmbed parent;
        MozillaEmbedPrivate *priv;
};

struct MozillaEmbedClass
{
        GtkMozEmbedClass parent_class;
};

GType	 mozilla_embed_get_type           (void);

gpointer mozilla_embed_get_galeon_wrapper (MozillaEmbed *embed);

G_END_DECLS

#endif
