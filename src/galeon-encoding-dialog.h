/*
 *  Copyright (C) 2000, 2001, 2002, 2003 Marco Pesenti Gritti
 *  Copyright (C) 2003 Christian Persch
 *  Copyright (C) 2003 Crispin Flowerday
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GALEON_ENCODING_DIALOG_H
#define GALEON_ENCODING_DIALOG_H

#include "galeon-embed-dialog.h"
#include "galeon-window.h"

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define GALEON_TYPE_ENCODING_DIALOG		(galeon_encoding_dialog_get_type ())
#define GALEON_ENCODING_DIALOG(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), GALEON_TYPE_ENCODING_DIALOG, GaleonEncodingDialog))
#define GALEON_ENCODING_DIALOG_CLASS(k)		(G_TYPE_CHECK_CLASS_CAST((k), GALEON_TYPE_ENCODING_DIALOG, GaleonEncodingDialogClass))
#define GALEON_IS_ENCODING_DIALOG(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), GALEON_TYPE_ENCODING_DIALOG))
#define GALEON_IS_ENCODING_DIALOG_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), GALEON_TYPE_ENCODING_DIALOG))
#define GALEON_ENCODING_DIALOG_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), GALEON_TYPE_ENCODING_DIALOG, GaleonEncodingDialogClass))

typedef struct GaleonEncodingDialog		GaleonEncodingDialog;
typedef struct GaleonEncodingDialogClass	GaleonEncodingDialogClass;
typedef struct GaleonEncodingDialogPrivate	GaleonEncodingDialogPrivate;

struct GaleonEncodingDialog
{
	GaleonEmbedDialog parent;

	/*< private >*/
	GaleonEncodingDialogPrivate *priv;
};

struct GaleonEncodingDialogClass
{
	GaleonEmbedDialogClass parent_class;
};

GType			 galeon_encoding_dialog_get_type	(void);

GaleonEncodingDialog	*galeon_encoding_dialog_new	(GaleonWindow *window);

G_END_DECLS

#endif
