/*
 *  Copyright (C) 2005 Crispin Flowerday
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __galeon_simple_window_h_
#define __galeon_simple_window_h_

#include <glib-object.h>
#include <gtk/gtkwindow.h>

#include "galeon-embed.h"

G_BEGIN_DECLS

/**
 * GaleonSimpleWindowobject
 */

#define GALEON_TYPE_SIMPLE_WINDOW	(galeon_simple_window_get_type())
#define GALEON_SIMPLE_WINDOW(object)	(G_TYPE_CHECK_INSTANCE_CAST((object), \
					 GALEON_TYPE_SIMPLE_WINDOW, GaleonSimpleWindow))
#define GALEON_SIMPLE_WINDOW_CLASS(klass)  (G_TYPE_CHECK_CLASS_CAST((klass), \
					    GALEON_TYPE_SIMPLE_WINDOW, GaleonSimpleWindowClass))
#define GALEON_IS_SIMPLE_WINDOW(object)	(G_TYPE_CHECK_INSTANCE_TYPE((object), \
					 GALEON_TYPE_SIMPLE_WINDOW))
#define GALEON_IS_SIMPLE_WINDOW_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass),\
						GALEON_TYPE_SIMPLE_WINDOW))
#define GALEON_SIMPLE_WINDOW_GET_CLASS(obj) 	(G_TYPE_INSTANCE_GET_CLASS((obj),\
						 GALEON_TYPE_SIMPLE_WINDOW, GaleonSimpleWindowClass))


/* object forward declarations */

typedef struct _GaleonSimpleWindow GaleonSimpleWindow;
typedef struct _GaleonSimpleWindowClass GaleonSimpleWindowClass;
typedef struct _GaleonSimpleWindowPrivate GaleonSimpleWindowPrivate;

struct _GaleonSimpleWindowClass
{
	GtkWindowClass parent_class;
};

/* Remember: fields are public read-only */
struct _GaleonSimpleWindow
{
	GtkWindow parent_object;
	
	GaleonSimpleWindowPrivate *priv;
};

GType		        galeon_simple_window_get_type	(void);
GaleonSimpleWindow *	galeon_simple_window_new	(void);
void                    galeon_simple_window_load_url   (GaleonSimpleWindow *win, const char *url);
GaleonEmbed *           galeon_simple_window_get_embed  (GaleonSimpleWindow *win);

G_END_DECLS

#endif
