/*
 * CookiePromptService.h
 *
 * Copyright (C) 2003 Tommi Komulainen <tommi.komulainen@iki.fi>
 * Available under the terms of the GNU General Public License version 2.
 */ 

#ifndef COOKIEPROMPTSERVICE_H
#define COOKIEPROMPTSERVICE_H 1

#include <nsError.h>
#include <nsICookiePromptService.h>

// 63bcf76c-4d64-4bcc-8ba9-b57e6650a508
#define G_COOKIEPROMPTSERVICE_CID	\
 {0x63bcf76c, 0x4d64, 0x4bcc, {0x8b, 0xa9, 0xb5, 0x7e, 0x66, 0x50, 0xa5, 0x08}}

#define G_COOKIEPROMPTSERVICE_CLASSNAME  "Galeon's Cookie Prompt Service"
#define G_COOKIEPROMPTSERVICE_CONTRACTID "@mozilla.org/embedcomp/cookieprompt-service;1"

class GCookiePromptService : public nsICookiePromptService
{
public:
	NS_DECL_ISUPPORTS
	NS_DECL_NSICOOKIEPROMPTSERVICE

	GCookiePromptService();
	virtual ~GCookiePromptService();

private:
};

#endif /* COOKIEPROMPTSERVICE_H */
