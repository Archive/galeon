/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __galeon_embed_manual_scroller_h
#define __galeon_embed_manual_scroller_h

#include <gtk/gtkwidget.h>
#include <glib-object.h>
#include "galeon-embed.h"


#ifdef __cplusplus
extern "C" {
#endif


/* object forward declarations */

typedef struct _GaleonEmbedManualScroller GaleonEmbedManualScroller;
typedef struct _GaleonEmbedManualScrollerClass GaleonEmbedManualScrollerClass;
typedef struct _GaleonEmbedManualScrollerPrivate GaleonEmbedManualScrollerPrivate;

/**
 * EmbedManualScroller object
 */

#define GALEON_TYPE_EMBED_MANUAL_SCROLLER		(galeon_embed_manual_scroller_get_type())
#define GALEON_EMBED_MANUAL_SCROLLER(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), \
							 GALEON_TYPE_EMBED_MANUAL_SCROLLER,\
							 GaleonEmbedManualScroller))
#define GALEON_EMBED_MANUAL_SCROLLER_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), \
							 GALEON_TYPE_EMBED_MANUAL_SCROLLER,\
							 GaleonEmbedManualScrollerClass))
#define GALEON_IS_EMBED_MANUAL_SCROLLER(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), \
							 GALEON_TYPE_EMBED_MANUAL_SCROLLER))
#define GALEON_IS_EMBED_MANUAL_SCROLLER_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), \
							 GALEON_TYPE_EMBED_MANUAL_SCROLLER))
#define GALEON_EMBED_MANUAL_SCROLLER_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), \
							 GALEON_TYPE_EMBED_MANUAL_SCROLLER,\
							 GaleonEmbedManualScrollerClass))

struct _GaleonEmbedManualScrollerClass 
{
	GObjectClass parent_class;
};

struct _GaleonEmbedManualScroller
{
	GObject parent_object;
	GaleonEmbedManualScrollerPrivate *priv;
};

GType				galeon_embed_manual_scroller_get_type	(void);
GaleonEmbedManualScroller *	galeon_embed_manual_scroller_new	(void);
void				galeon_embed_manual_scroller_set_embed	(GaleonEmbedManualScroller *as,
									 GaleonEmbed *embed);
void				galeon_embed_manual_scroller_start_scroll (GaleonEmbedManualScroller *as,
									   GtkWidget *widget, 
									   gint x, gint y);

#ifdef __cplusplus
}
#endif

#endif
