/*
 *  Copyright (C) 2004 Crispin Flowerday
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef GALEON_SIDEBAR_EMBED_H
#define GALEON_SIDEBAR_EMBED_H

#include <glib-object.h>
#include <gtk/gtkbin.h>

#include "galeon-embed.h"
#include "galeon-window.h"

G_BEGIN_DECLS

#define GALEON_TYPE_SIDEBAR_EMBED	(galeon_sidebar_embed_get_type ())
#define GALEON_SIDEBAR_EMBED(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), GALEON_TYPE_SIDEBAR_EMBED, GaleonSidebarEmbed))
#define GALEON_SIDEBAR_EMBED_CLASS(k)	(G_TYPE_CHECK_CLASS_CAST((k), GALEON_TYPE_SIDEBAR_EMBED, GaleonSidebarEmbedClass))
#define GALEON_IS_SIDEBAR_EMBED(o)	(G_TYPE_CHECK_INSTANCE_TYPE ((o), GALEON_TYPE_SIDEBAR_EMBED))
#define GALEON_IS_SIDEBAR_EMBED_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), GALEON_TYPE_SIDEBAR_EMBED))
#define GALEON_SIDEBAR_EMBED_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), GALEON_TYPE_SIDEBAR_EMBED, GaleonSidebarEmbedClass))

typedef struct _GaleonSidebarEmbed	GaleonSidebarEmbed;
typedef struct _GaleonSidebarEmbedPrivate	GaleonSidebarEmbedPrivate;
typedef struct _GaleonSidebarEmbedClass	GaleonSidebarEmbedClass;

struct _GaleonSidebarEmbed 
{
	GtkBin parent;

	/*< private >*/
	GaleonSidebarEmbedPrivate *priv;
};

struct _GaleonSidebarEmbedClass
{
	GtkBinClass parent_class;
};

GType	    galeon_sidebar_embed_get_type	(void);

GtkWidget  *galeon_sidebar_embed_new		(void);

void	    galeon_sidebar_embed_set_url	(GaleonSidebarEmbed *embed,
						 const char *url);

G_END_DECLS

#endif
