/*
 *  Copyright © 2004, 2005 Jean-François Rameau
 *  Copyright © 2006 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#include "config.h"

#include "galeon-dbus.h"
#include "galeon-type-builtins.h"
#include "galeon-marshal.h"
#include "galeon-activation.h"
#include "galeon-dbus-server-bindings.h"

#include <string.h>
#include <dbus/dbus-glib-bindings.h>

/* dbus 0.6 API change */
#ifndef DBUS_NAME_FLAG_PROHIBIT_REPLACEMENT
#define DBUS_NAME_FLAG_PROHIBIT_REPLACEMENT 0
#endif

/* dbus < 0.6 compat */
#ifndef DBUS_NAME_FLAG_DO_NOT_QUEUE
#define DBUS_NAME_FLAG_DO_NOT_QUEUE 0
#endif

/* Galeon's DBUS ids */
#define DBUS_GALEON_SERVICE	"org.gnome.Galeon"
#define DBUS_GALEON_PATH		"/org/gnome/Galeon"
#define DBUS_GALEON_INTERFACE	"org.gnome.Galeon"

#define RECONNECT_DELAY	3 /* seconds */

#define GALEON_DBUS_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), GALEON_TYPE_DBUS, GaleonDbusPrivate))

struct _GaleonDbusPrivate
{
	DBusGConnection *session_bus;
	DBusGConnection *system_bus;
	guint session_reconnect_timeout_id;
	guint system_reconnect_timeout_id;
	guint is_session_service_owner : 1;
	guint register_name : 1;
};

enum
{
	CONNECTED,
	DISCONNECTED,
	LAST_SIGNAL
};

enum
{
	PROP_0,
	PROP_CLAIM_NAME
};

static GaleonDbus *galeon_dbus_instance;

static guint signals[LAST_SIGNAL];
GQuark galeon_dbus_error_quark;

/* Filter signals form session bus */
static DBusHandlerResult session_filter_func (DBusConnection *connection,
				              DBusMessage *message,
				              void *user_data);
/* Filter signals from system bus */
static DBusHandlerResult system_filter_func (DBusConnection *connection,
				             DBusMessage *message,
				             void *user_data);

/* Both  connect to their respective bus */
static gboolean galeon_dbus_connect_to_session_bus (GaleonDbus*, GError**);
static gboolean galeon_dbus_connect_to_system_bus  (GaleonDbus*, GError**);

/* implementation of the DBUS helpers */

static gboolean
galeon_dbus_connect_to_session_bus_cb (gpointer user_data)
{
	GaleonDbus *dbus = GALEON_DBUS (user_data);

	if (!galeon_dbus_connect_to_session_bus (dbus, NULL))
	{
		/* try again */
		return TRUE;
	}

	dbus->priv->session_reconnect_timeout_id = 0;

	/* we're done */
	return FALSE;
}

static gboolean
galeon_dbus_connect_to_system_bus_cb (gpointer user_data)
{
	GaleonDbus *dbus = GALEON_DBUS (user_data);

	if (!galeon_dbus_connect_to_system_bus (dbus, NULL))
	{
		/* try again */
		return TRUE;
	}

	dbus->priv->system_reconnect_timeout_id = 0;

	/* we're done */
	return FALSE;
}

static DBusHandlerResult
session_filter_func (DBusConnection *connection,
	     	     DBusMessage *message,
	     	     void *user_data)
{
	GaleonDbus *galeon_dbus = GALEON_DBUS (user_data);
	GaleonDbusPrivate *priv = galeon_dbus->priv;

	if (dbus_message_is_signal (message,
				    DBUS_INTERFACE_LOCAL,
				    "Disconnected"))
	{
		// LOG ("GaleonDbus disconnected from session bus");

		dbus_g_connection_unref (priv->session_bus);
		priv->session_bus = NULL;

		g_signal_emit (galeon_dbus, signals[DISCONNECTED], 0, GALEON_DBUS_SESSION);

		/* try to reconnect later ... */
		priv->session_reconnect_timeout_id =
			g_timeout_add_seconds (RECONNECT_DELAY,
				       (GSourceFunc) galeon_dbus_connect_to_session_bus_cb,
				       galeon_dbus);

		return DBUS_HANDLER_RESULT_HANDLED;
	}

	return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
}

static DBusHandlerResult
system_filter_func (DBusConnection *connection,
	     	    DBusMessage *message,
	     	    void *user_data)
{
	GaleonDbus *galeon_dbus = GALEON_DBUS (user_data);
	GaleonDbusPrivate *priv = galeon_dbus->priv;

	// LOG ("GaleonDbus filtering message from system bus");

	if (dbus_message_is_signal (message,
				    DBUS_INTERFACE_LOCAL,
				    "Disconnected"))
	{
		// LOG ("GaleonDbus disconnected from system bus");

		dbus_g_connection_unref (priv->system_bus);
		priv->system_bus = NULL;

		g_signal_emit (galeon_dbus, signals[DISCONNECTED], 0, GALEON_DBUS_SYSTEM);

		/* try to reconnect later ... */
		priv->system_reconnect_timeout_id =
			g_timeout_add_seconds (RECONNECT_DELAY,
				       (GSourceFunc) galeon_dbus_connect_to_system_bus_cb,
				       galeon_dbus);

		return DBUS_HANDLER_RESULT_HANDLED;
	}

	return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
}

static gboolean
galeon_dbus_connect_to_system_bus (GaleonDbus *galeon_dbus,
				 GError **error)
{
	GaleonDbusPrivate *priv = galeon_dbus->priv;

	// LOG ("GaleonDbus connecting to system DBUS");

	priv->system_bus = dbus_g_bus_get (DBUS_BUS_SYSTEM, error);
	if (priv->system_bus == NULL)
	{
		g_warning ("Unable to connect to system bus: %s", error ? (*error)->message : "");
		return FALSE;
	}

	if (dbus_g_connection_get_connection (priv->system_bus) == NULL)
	{
		g_warning ("DBus connection is null");
		g_set_error (error,
			     GALEON_DBUS_ERROR_QUARK,
			     0,
			     "DBus connection is NULL");
		return FALSE;
	}

	dbus_connection_set_exit_on_disconnect 
		(dbus_g_connection_get_connection (priv->system_bus),
		 FALSE);

	dbus_connection_add_filter
		(dbus_g_connection_get_connection (priv->system_bus),
		 system_filter_func, galeon_dbus, NULL);

	g_signal_emit (galeon_dbus, signals[CONNECTED], 0, GALEON_DBUS_SYSTEM);

	return TRUE;
}

static gboolean
galeon_dbus_connect_to_session_bus (GaleonDbus *galeon_dbus,
				  GError **error)
{
	GaleonDbusPrivate *priv = galeon_dbus->priv;
	DBusGProxy *proxy;
	guint request_ret;
	
	// LOG ("GaleonDbus connecting to session DBUS");

	/* Init the DBus connection */
	priv->session_bus = dbus_g_bus_get (DBUS_BUS_SESSION, error);
	if (priv->session_bus == NULL)
	{
		g_warning("Unable to connect to session bus: %s", error && *error ? (*error)->message : "");
		return FALSE;
	}

	dbus_connection_set_exit_on_disconnect 
		(dbus_g_connection_get_connection (priv->session_bus),
		 FALSE);

	dbus_connection_add_filter
		(dbus_g_connection_get_connection (priv->session_bus),
		 session_filter_func, galeon_dbus, NULL);

	if (priv->register_name == FALSE) return TRUE;

	dbus_g_object_type_install_info (GALEON_TYPE_DBUS,
					 &dbus_glib_galeon_activation_object_info);

	/* Register DBUS path */
	dbus_g_connection_register_g_object (priv->session_bus,
					     DBUS_GALEON_PATH,
					     G_OBJECT (galeon_dbus));

	/* Register the service name, the constant here are defined in dbus-glib-bindings.h */
	proxy = dbus_g_proxy_new_for_name (priv->session_bus,
					   DBUS_SERVICE_DBUS,
					   DBUS_PATH_DBUS,
					   DBUS_INTERFACE_DBUS);

	if (!org_freedesktop_DBus_request_name (proxy,
						DBUS_GALEON_SERVICE,
						DBUS_NAME_FLAG_PROHIBIT_REPLACEMENT |
						DBUS_NAME_FLAG_DO_NOT_QUEUE,
						&request_ret, error))
	{
		/* We have a BIG problem! */
		g_warning ("RequestName failed: %s\n", error ? (*error)->message : "");
		return FALSE;
	}

	if (request_ret == DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER ||
	    request_ret == DBUS_REQUEST_NAME_REPLY_ALREADY_OWNER)
	{
		priv->is_session_service_owner = TRUE;
	}
	else if (request_ret == DBUS_REQUEST_NAME_REPLY_EXISTS ||
		 request_ret == DBUS_REQUEST_NAME_REPLY_IN_QUEUE)
	{
		priv->is_session_service_owner = FALSE;
	}

	// LOG ("Instance is %ssession bus owner.", priv->is_session_service_owner ? "" : "NOT ");

	g_object_unref (proxy);

	return TRUE;
}

/* Public methods */

static void
galeon_dbus_shutdown (GaleonDbus *dbus)
{
	GaleonDbusPrivate *priv = dbus->priv;

	// LOG ("GaleonDbus shutdown");

	if (priv->session_reconnect_timeout_id != 0)
	{
		g_source_remove (priv->session_reconnect_timeout_id);
		priv->session_reconnect_timeout_id = 0;
	}

	if (priv->system_reconnect_timeout_id != 0)
	{
		g_source_remove (priv->system_reconnect_timeout_id);
		priv->system_reconnect_timeout_id = 0;
	}

	if (priv->session_bus)
	{
		dbus_connection_remove_filter
			(dbus_g_connection_get_connection (priv->session_bus),
			 session_filter_func, dbus);
		dbus_g_connection_unref (priv->session_bus);
		priv->session_bus = NULL;
	}

        if (priv->system_bus)
	{
		dbus_connection_remove_filter
			(dbus_g_connection_get_connection (priv->system_bus),
			 system_filter_func, dbus);
		dbus_g_connection_unref (priv->system_bus);
		priv->system_bus = NULL;
	}
}

/* Class implementation */

G_DEFINE_TYPE (GaleonDbus, galeon_dbus, G_TYPE_OBJECT)

static void
galeon_dbus_get_property (GObject *object,
			guint prop_id,
		 	GValue *value,
			GParamSpec *pspec)
{
	/* no readable properties */
	g_return_if_reached ();
}

static void
galeon_dbus_set_property (GObject *object,
			guint prop_id,
			const GValue *value,
			GParamSpec *pspec)
{
	GaleonDbus *dbus = GALEON_DBUS (object);
	GaleonDbusPrivate *priv = dbus->priv;

	switch (prop_id)
	{
		case PROP_CLAIM_NAME:
			priv->register_name = g_value_get_boolean (value);
			break;
	}
}

static void
galeon_dbus_finalize (GObject *object)
{
	GaleonDbus *dbus = GALEON_DBUS (object);

	/* Have to do this after the object's weak ref notifiers have
	 * been called, see https://bugs.freedesktop.org/show_bug.cgi?id=5688
	 */
	galeon_dbus_shutdown (dbus);

	// LOG ("GaleonDbus finalised");

	G_OBJECT_CLASS (galeon_dbus_parent_class)->finalize (object);
}

static void
galeon_dbus_init (GaleonDbus *dbus)
{
	dbus->priv = GALEON_DBUS_GET_PRIVATE (dbus);

	// LOG ("GaleonDbus initialising");
}

static void
galeon_dbus_class_init (GaleonDbusClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->get_property = galeon_dbus_get_property;
	object_class->set_property = galeon_dbus_set_property;
	object_class->finalize = galeon_dbus_finalize;

	signals[CONNECTED] =
		g_signal_new ("connected",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (GaleonDbusClass, connected),
			      NULL, NULL,
			      galeon_marshal_VOID__ENUM,
			      G_TYPE_NONE,
			      1,
			      GALEON_TYPE_DBUS_BUS);

	signals[DISCONNECTED] =
		g_signal_new ("disconnected",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (GaleonDbusClass, disconnected),
			      NULL, NULL,
			      galeon_marshal_VOID__ENUM,
			      G_TYPE_NONE,
			      1,
			      GALEON_TYPE_DBUS_BUS);

	g_object_class_install_property
		(object_class,
		 PROP_CLAIM_NAME,
		 g_param_spec_boolean ("register-name",
				       "register-name",
				       "register-name",
				       TRUE,
				       G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));

	g_type_class_add_private (object_class, sizeof(GaleonDbusPrivate));
}

GaleonDbus *
galeon_dbus_get_default (void)
{
	g_assert (galeon_dbus_instance != NULL);

	return galeon_dbus_instance;
}

/**
 * galeon_dbus_get_bus:
 * @dbus:
 * @kind:
 * 
 * Returns: the #DBusGConnection for the @kind DBUS, or %NULL
 * if a connection could not be established.
 */
DBusGConnection *
galeon_dbus_get_bus (GaleonDbus *dbus,
		   GaleonDbusBus kind)
{
	GaleonDbusPrivate *priv = dbus->priv;
	DBusGConnection *bus = NULL;

	g_return_val_if_fail (GALEON_IS_DBUS (dbus), NULL);

	if (kind == GALEON_DBUS_SYSTEM)
	{
		/* We connect lazily to the system bus */
		if (priv->system_bus == NULL)
		{
			galeon_dbus_connect_to_system_bus (dbus, NULL);
		}

		bus = priv->system_bus;
	}
	else if (kind == GALEON_DBUS_SESSION)
	{
		if (priv->session_bus == NULL)
		{
			galeon_dbus_connect_to_session_bus (dbus, NULL);
		}

		bus = priv->session_bus;
	}
	else
	{
		g_assert_not_reached ();
	}

	return bus;
}

DBusGProxy *
galeon_dbus_get_proxy (GaleonDbus *dbus,
		     GaleonDbusBus kind)
{
	DBusGConnection *bus = NULL;

	g_return_val_if_fail (GALEON_IS_DBUS (dbus), NULL);
	
	bus = galeon_dbus_get_bus (dbus, kind);

	if (bus == NULL)
	{
		g_warning ("Unable to get proxy for the %s bus.\n",
			   kind == GALEON_DBUS_SESSION ? "session" : "system");
		return NULL;
	}

	return dbus_g_proxy_new_for_name (bus,
					  DBUS_GALEON_SERVICE,
					  DBUS_GALEON_PATH,
					  DBUS_GALEON_INTERFACE);
}

/* private API */

gboolean
_galeon_dbus_startup (gboolean connect_and_register_name,
		      GError **error)
{
	g_assert (galeon_dbus_instance == NULL);

	galeon_dbus_error_quark = g_quark_from_static_string ("galeon-dbus-error");
		
	galeon_dbus_instance = g_object_new (GALEON_TYPE_DBUS,
					     "register-name", connect_and_register_name,
					     NULL);

	if (!connect_and_register_name) return TRUE;

	/* We only connect to the session bus on startup*/
	return galeon_dbus_connect_to_session_bus (galeon_dbus_instance, error);
}

void
_galeon_dbus_release (void)
{
	g_assert (galeon_dbus_instance != NULL);

	g_object_unref (galeon_dbus_instance);
	galeon_dbus_instance = NULL;
}

gboolean
_galeon_dbus_is_name_owner (void)
{
	g_assert (galeon_dbus_instance != NULL);

	return galeon_dbus_instance->priv->is_session_service_owner;
}
