/*
 *  Copyright (C) 2003 Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "galeon-action-home.h"
#include "galeon-shell.h"
#include "eel-gconf-extensions.h"
#include "prefs-strings.h"
#include "gul-toolbutton.h"
#include "gul-string.h"
#include "gul-gui.h"
#include "galeon-dnd.h"
#include "hig-alert.h"

#include <gtk/gtkstock.h>
#include <gtk/gtkimagemenuitem.h>
#include <glib/gi18n.h>
#include <string.h>

static void		galeon_action_home_finalize			(GObject *object);
static void		galeon_action_home_connect_proxy		(GtkAction *action, GtkWidget *proxy);
static void             galeon_action_home_activate                     (GtkAction *action);



G_DEFINE_TYPE (GaleonActionHome, galeon_action_home, GALEON_TYPE_ACTION);


static void
galeon_action_home_class_init (GaleonActionHomeClass *class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);
	GtkActionClass *action_class = GTK_ACTION_CLASS (class);

	object_class->finalize = galeon_action_home_finalize;

	action_class->menu_item_type    = GTK_TYPE_IMAGE_MENU_ITEM;
	action_class->toolbar_item_type = GUL_TYPE_TOOLBUTTON;
	action_class->connect_proxy = galeon_action_home_connect_proxy;
	action_class->activate      = galeon_action_home_activate;

}

static void
galeon_action_home_init (GaleonActionHome *action)
{
	g_object_set (G_OBJECT (action), "stock_id", GTK_STOCK_HOME, NULL);
	g_object_set (G_OBJECT (action), "tooltip", _("Go to your home page"), NULL);
}

static void
galeon_action_home_finalize (GObject *object)
{
	G_OBJECT_CLASS (galeon_action_home_parent_class)->finalize (object);
}

static void
galeon_action_home_activate (GtkAction *action)
{
	GaleonActionHome *a = GALEON_ACTION_HOME (action);
	GaleonEmbed *embed = galeon_action_get_embed (GALEON_ACTION (a));
	char *location;

	g_return_if_fail (embed != NULL);

	location = eel_gconf_get_string (CONF_GENERAL_HOMEPAGE);
	g_return_if_fail (location != NULL);

	gtk_widget_grab_focus (GTK_WIDGET (embed));
	
	galeon_embed_load_url (embed, location);

	g_free (location);
}

static void
galeon_action_home_home_activate_cb (GtkMenuItem *mi, GaleonActionHome *b)
{
	gtk_action_activate (GTK_ACTION (b));
}

static void 
galeon_action_home_home_new_tab_activate_cb (GtkMenuItem *mi, GaleonActionHome *b)
{
	GaleonWindow *window = galeon_action_get_window (GALEON_ACTION (b));
	char *location;

	location = eel_gconf_get_string (CONF_GENERAL_HOMEPAGE);
	g_return_if_fail (location != NULL);
	
	if (location)
	{
		GaleonTab *tab = galeon_window_get_active_tab (window);
		galeon_shell_new_tab (galeon_shell, window, tab, location, 
				      GALEON_NEW_TAB_IN_EXISTING_WINDOW);
		g_free (location);
	}
}

static void 
galeon_action_home_home_new_window_activate_cb (GtkMenuItem *mi, GaleonActionHome *b)
{
	GaleonWindow *window = galeon_action_get_window (GALEON_ACTION (b));
	char *location;

	location = eel_gconf_get_string (CONF_GENERAL_HOMEPAGE);
	g_return_if_fail (location != NULL);
	
	if (location)
	{
		GaleonTab *tab = galeon_window_get_active_tab (window);
		galeon_shell_new_tab (galeon_shell, window, tab, location, 
				      GALEON_NEW_TAB_IN_NEW_WINDOW);
		g_free (location);
	}
}

static void 
galeon_action_home_set_home_activate_cb (GtkMenuItem *mi, GaleonActionHome *b)
{
	GaleonWindow *window = galeon_action_get_window (GALEON_ACTION (b));
	GaleonTab *tab = galeon_window_get_active_tab (window);
	const char *location = galeon_tab_get_location (tab);

	if (location)
	{
		eel_gconf_set_string (CONF_GENERAL_HOMEPAGE, location);
	}
}

static gboolean
galeon_action_home_button_press_event_cb (GtkWidget *widget, GdkEventButton *event, 
					  GaleonActionHome *b)
{
	GaleonWindow *window = galeon_action_get_window (GALEON_ACTION (b));

	if (event->button == 2 && GALEON_IS_WINDOW (window))
	{
		char *location;

		location = eel_gconf_get_string (CONF_GENERAL_HOMEPAGE);
		g_return_val_if_fail (location != NULL, FALSE);
	
		if (location)
		{
			GaleonTab *tab = galeon_window_get_active_tab (window);
			galeon_shell_new_tab (galeon_shell, window, tab, location, 0);
			g_free (location);
		}
		return TRUE;
	}
	
	return FALSE;
}

static void
galeon_action_home_set_home_dialog_response_cb (GtkWidget *dialog,
						gint response,
						char *url)
{
	if (response == GTK_RESPONSE_OK)
	{
		eel_gconf_set_string (CONF_GENERAL_HOMEPAGE, url);
	}
	gtk_widget_destroy (dialog);
}


static void
each_url_receive_data_binder (const char * url, const char * title, 
			      gpointer data )
{
	char ** urlp = data;
	if (*urlp == NULL)
	{
		*urlp = g_strdup (url);
	}
}	

static void
galeon_action_home_drag_data_received_cb (GtkWidget* widget, 
					  GdkDragContext *dc,
					  gint x, gint y, 
					  GtkSelectionData *selection_data,
					  guint info, guint time, 
					  GaleonActionHome *b)
{
	GaleonWindow *window = galeon_action_get_window (GALEON_ACTION (b));
	char *url = NULL;
	char *tt_url, *url_enc;
	GtkWidget *dialog;

	g_return_if_fail (GALEON_IS_WINDOW (window));

	galeon_dnd_drag_data_receive (widget, dc, x, y, selection_data,
				      info, time, &url, each_url_receive_data_binder);
	
	if (!url) return;

	dialog = hig_alert_new (GTK_WINDOW (window),
				GTK_DIALOG_DESTROY_WITH_PARENT,
				HIG_ALERT_CONFIRMATION,
				_("Change your home page?"),
				NULL,
				GTK_STOCK_CANCEL,
				GTK_RESPONSE_CANCEL,
				_("Change _Home Page" ),
				GTK_RESPONSE_OK,
				NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);
	
	url_enc = g_markup_escape_text (url, strlen (url));
	tt_url = g_strdup_printf ("\"<tt>%s</tt>\"", url_enc);
	g_free (url_enc);

	hig_alert_set_secondary_printf (HIG_ALERT (dialog),
					"Your new home page will be %s.", tt_url);
	g_free (tt_url);

	g_signal_connect_data (G_OBJECT(dialog),
			       "response",
			       G_CALLBACK(galeon_action_home_set_home_dialog_response_cb),
			       url,
			       (GClosureNotify)g_free,
			       (GConnectFlags)0);
	gtk_widget_show (dialog);
}

static void
galeon_action_home_connect_proxy (GtkAction *a, GtkWidget *proxy)
{
	GtkWidget *widget = proxy;

	if (GUL_IS_TOOLBUTTON (proxy))
	{
		GtkMenuShell *ms;
		widget = gul_toolbutton_get_button (GUL_TOOLBUTTON (proxy));

		galeon_dnd_url_drag_dest_set (proxy);

		g_signal_connect (proxy, "drag_data_received",
				  G_CALLBACK(galeon_action_home_drag_data_received_cb), a);

		ms = gul_toolbutton_get_menu (GUL_TOOLBUTTON (proxy));
	
		gul_gui_append_new_menuitem (GTK_WIDGET (ms), _("Go _Home"), 
					     G_CALLBACK (galeon_action_home_home_activate_cb), a);
		gul_gui_append_new_menuitem (GTK_WIDGET (ms), _("Go Home in New _Tab"), 
					     G_CALLBACK (galeon_action_home_home_new_tab_activate_cb), a);
		gul_gui_append_new_menuitem (GTK_WIDGET (ms), _("Go Home in New _Window"), 
					     G_CALLBACK (galeon_action_home_home_new_window_activate_cb), a);
		gul_gui_append_separator (GTK_WIDGET (ms));
		gul_gui_append_new_menuitem (GTK_WIDGET (ms), _("_Set Current Page as Home Page"), 
					     G_CALLBACK (galeon_action_home_set_home_activate_cb), a);
	}

	g_signal_connect (widget, "button-press-event",
			  G_CALLBACK (galeon_action_home_button_press_event_cb), a);

	(* GTK_ACTION_CLASS (galeon_action_home_parent_class)->connect_proxy) (a, proxy);
}

