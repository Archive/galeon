/*
 *  Copyright (C) 2000 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __ContentHandler_h
#define __ContentHandler_h

#include "mozilla-embed-shell.h"

#include <libgnomevfs/gnome-vfs-mime-handlers.h>

#include <nsIHelperAppLauncherDialog.h>
#include <nsCOMPtr.h>

#include "GulString.h"

/* forward declarations */
class nsIURI;
class nsIFile;

#define G_CONTENTHANDLER_CID			     \
{ /* 16072c4a-23a6-4996-9beb-9335c06bbeae */         \
    0x16072c4a,                                      \
    0x23a6,                                          \
    0x4996,                                          \
    {0x9b, 0xeb, 0x93, 0x35, 0xc0, 0x6b, 0xbe, 0xae} \
}

class nsIFactory;

class GContentHandler : public nsIHelperAppLauncherDialog
{
  public:
	NS_DECL_ISUPPORTS
	NS_DECL_NSIHELPERAPPLAUNCHERDIALOG

	GContentHandler();
	virtual ~GContentHandler();

	NS_METHOD LaunchExternalDownloader (void);
	NS_METHOD FindHelperApp (void);
	NS_METHOD LaunchHelperApp (void);

	NS_METHOD GetLauncher (nsIHelperAppLauncher * *_retval);
	NS_METHOD GetContext (nsISupports * *_retval);
	NS_METHOD SetHelperApp(GnomeVFSMimeApplication *mHelperApp,
			       PRBool alwaysUse);
	NS_METHOD SynchroniseMIMEInfo (void);

  private:
	/* additional members */
	NS_METHOD Init (void);
	NS_METHOD ProcessMimeInfo (void);
	NS_METHOD MIMEAskAction (void);
	NS_METHOD CheckAppSupportScheme (void);

	nsCOMPtr<nsIHelperAppLauncher> mLauncher;
	nsCOMPtr<nsISupports> mContext;

	nsCOMPtr<nsIURI> mUri;
	PRInt64 mTimeDownloadStarted;
	nsCOMPtr<nsIFile> mTempFile;

	GulCString mMimeType;
	PRBool mUrlHelper;
	GnomeVFSMimeApplication *mHelperApp;
	
	GulCString mUrl;
	GulCString mScheme;
};

#endif
