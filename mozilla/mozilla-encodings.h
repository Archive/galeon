/*
 *  Copyright (C) 2003 Crispin Flowerday
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef MOZILLA_ENCODINGS_H
#define MOZILLA_ENCODINGS_H

#include <glib-object.h>
#include <glib.h>

#include "galeon-encodings.h"

G_BEGIN_DECLS

#define MOZILLA_TYPE_ENCODINGS		(mozilla_encodings_get_type ())
#define MOZILLA_ENCODINGS(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), MOZILLA_TYPE_ENCODINGS, MozillaEncodings))
#define MOZILLA_ENCODINGS_CLASS(k)		(G_TYPE_CHECK_CLASS_CAST ((k), MOZILLA_TYPE_ENCODINGS, MozillaEncodingsClass))
#define MOZILLA_IS_ENCODINGS(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), MOZILLA_TYPE_ENCODINGS))
#define MOZILLA_IS_ENCODINGS_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), MOZILLA_TYPE_ENCODINGS))
#define MOZILLA_ENCODINGS_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), MOZILLA_TYPE_ENCODINGS, MozillaEncodingsClass))

typedef struct MozillaEncodingsPrivate MozillaEncodingsPrivate;
typedef struct MozillaEncodingsClass MozillaEncodingsClass;
typedef struct MozillaEncodings MozillaEncodings;

struct MozillaEncodings
{
	GaleonEncodings parent;

        MozillaEncodingsPrivate *priv;
};

struct MozillaEncodingsClass
{
        GaleonEncodingsClass parent_class;
};


GType		 mozilla_encodings_get_type        (void);

MozillaEncodings *mozilla_encodings_new            (void);

G_END_DECLS

#endif
