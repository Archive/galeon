/*
 *  Copyright (C) 2003 Philip Langdale
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef BOOKMARKS_ADD_DIALOG_H
#define BOOKMARKS_ADD_DIALOG_H

#include "bookmarks.h"
#include "galeon-dialog.h"
#include <glib.h>

G_BEGIN_DECLS

typedef struct _GbAddDialog GbAddDialog;
typedef struct _GbAddDialogClass GbAddDialogClass;

#define GB_TYPE_ADD_DIALOG             (gb_add_dialog_get_type ())
#define GB_ADD_DIALOG(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_ADD_DIALOG, GbAddDialog))
#define GB_ADD_DIALOG_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GB_ADD_DIALOG, GbAddDialogClass))
#define GB_IS_ADD_DIALOG(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GB_TYPE_ADD_DIALOG))
#define GB_IS_ADD_DIALOG_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GB_ADD_DIALOG))

typedef struct _GbAddDialogPrivate GbAddDialogPrivate;

struct _GbAddDialog
{
        GaleonDialog parent;
        GbAddDialogPrivate *priv;
};

struct _GbAddDialogClass
{
        GaleonDialogClass parent_class;
};

GType           gb_add_dialog_get_type    (void);

GaleonDialog   *gb_add_dialog_new         (GtkWidget *window,
					   GbBookmarkSet *set,
					   GSList *sites);
G_END_DECLS

#endif

