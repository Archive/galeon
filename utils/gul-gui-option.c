/*
 *  Copyright (C) 2003  Tommi Komulainen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation. 
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */ 

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gul-gui-option.h"

#include <glib/gi18n.h>
#include <gtk/gtkcelllayout.h>
#include <gtk/gtkcellrenderertext.h>
#include <gtk/gtkliststore.h>
#include <gtk/gtktreemodelsort.h>

#include <string.h>

/**************************************************************************
 * GulGuiOption
 *
 * GulGuiOption is an option item that has a explicitly defined value and a
 * (translated) title for showing to the user.  By linking the title and the
 * value together, instead of relying on the order of the items, we can sort
 * the user visible titles according to user's locale.  The end result is
 * likely to look more sane than just simply translating the titles in place,
 * which can make the order of items look random.
 */
/**
 * gul_gui_option_new:
 * @title: localized title for the option
 * @value: the value
 *
 * Returns: a newly allocated #GulGuiOption item.
 */
GulGuiOption *
gul_gui_option_new (const char *title, const char *value)
{
	GulGuiOption *item;
	char          *tmp;

	item = g_new(GulGuiOption, 1);
	item->title = g_strdup (title);
	item->value = g_strdup (value);

	tmp = g_utf8_casefold (item->title, -1);
	item->key = g_utf8_collate_key (tmp, -1);
	g_free (tmp);

	return item;
}

void
gul_gui_option_combobox_populate(GtkComboBox *combobox,
				 const GulGuiOption *options,
				 guint n_options,
				 gboolean sortable,
				 gboolean translate)
{
        guint i;
	GtkTreeModel *model = NULL;
	GtkListStore *store = gtk_list_store_new(2, G_TYPE_STRING, G_TYPE_STRING);

        for (i = 0; i < n_options; i++)
	{
		const char *title;
		GtkTreeIter iter;

		title = translate ? _(options[i].title) : options[i].title;

		gtk_list_store_append(store, &iter);
		gtk_list_store_set(store, &iter,
				   OPTION_COL_TITLE, title,
				   OPTION_COL_VALUE, options[i].value,
				   -1);
	}

	if (sortable)
	{
		GtkTreeModel *sorter =
			gtk_tree_model_sort_new_with_model(GTK_TREE_MODEL(store));
		gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(sorter),
						     OPTION_COL_TITLE,
						     GTK_SORT_ASCENDING);
		g_object_unref (store);
		model = sorter;
	}
	else
	{
		model = GTK_TREE_MODEL(store);
	}

        gtk_combo_box_set_model(GTK_COMBO_BOX(combobox), model);

	g_object_unref (model);

	gtk_combo_box_set_active(GTK_COMBO_BOX(combobox), 0);
}


gchar *gul_gui_option_combobox_get_value(GtkComboBox *combobox)
{
	gchar *value = NULL;
	GtkTreeIter iter;
	GtkTreeModel *model = gtk_combo_box_get_model(combobox);

	if (gtk_combo_box_get_active_iter(combobox, &iter))
	{
		gtk_tree_model_get(model, &iter,
				   OPTION_COL_VALUE, &value,
				   -1);
	}

	return value;
}

void
gul_gui_option_combobox_set_value(GtkComboBox *combobox,
				  const gchar *value)
{
	gboolean valid;
	GtkTreeIter iter;
	GtkTreeModel *model;

	g_return_if_fail (value != NULL);
	g_return_if_fail (GTK_IS_COMBO_BOX (combobox));

	model = gtk_combo_box_get_model(combobox); 

	valid = gtk_tree_model_get_iter_first(model, &iter);

	while (valid)
	{
		gchar *iter_value;
		gtk_tree_model_get(model, &iter,
				   OPTION_COL_VALUE, &iter_value,
				   -1);
		if (!iter_value)
		{
			valid = gtk_tree_model_iter_next(model, &iter);
			continue;
		}

		if (strcmp(value, iter_value) == 0)
		{
			gtk_combo_box_set_active_iter(combobox, &iter);
			g_free(iter_value);
			break;
		}
		g_free(iter_value);

		valid = gtk_tree_model_iter_next(model, &iter);
	}
}
