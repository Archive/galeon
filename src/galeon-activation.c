/*
 *  Copyright © 2005 Gustavo Gama
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#include "config.h"

#include "galeon-activation.h"

#include "galeon-shell.h"
#include "galeon-session.h"
#include "eel-gconf-extensions.h"
#include "galeon-debug.h"

static gboolean
session_queue_command (SessionResumeType  command,
		       GValue            *arg,
		       char             **args,
		       guint              startup_id,
		       GError           **error)
{
	GaleonShell *shell;
	Session *session;

	shell = galeon_shell_get_default ();
	if (shell == NULL)
	{
		g_set_error (error,
			     g_quark_from_static_string ("galeon-activation-error"),
			     0, 
			     "Shutting down." /* FIXME i18n & better string */);
		return FALSE;
	}

	session = galeon_shell_get_session (galeon_shell_get_default());
	g_assert (session != NULL);

	galeon_session_queue_command (session, command, arg, args,
				     (guint32) startup_id, FALSE);

	return TRUE;
}

gboolean
galeon_activation_load_uri_list (GaleonDbus  *galeon_dbus,
		                 char       **uris,
		                 char        *options,
	                         guint        startup_id,
	                         GError     **error)
{
	GValue new_options = { 0 };

        g_value_init (&new_options, G_TYPE_STRING);
        g_value_set_string (&new_options,
                            g_strconcat (options, "external,", NULL));        

	return session_queue_command (GALEON_SESSION_CMD_OPEN_URIS,
				      &new_options, uris, startup_id, error);
}

gboolean
galeon_activation_load_session (GaleonDbus *galeon_dbus,
	                        char       *session_name,
	                        guint       startup_id,
	                        GError    **error)
{
        GValue value = { 0 };

        g_value_init (&value, G_TYPE_STRING);
        g_value_set_string (&value, session_name);

	return session_queue_command (GALEON_SESSION_CMD_LOAD_SESSION,
				      &value, NULL, startup_id, error);
}

gboolean
galeon_activation_quit (GaleonDbus *galeon_dbus,
                        gboolean    exit_server,
                        GError    **error)
{
        GValue value = { 0 };

        g_value_init (&value, G_TYPE_BOOLEAN);
        g_value_set_boolean (&value, exit_server);

        return session_queue_command (GALEON_SESSION_QUIT,
                                      &value,
                                      NULL,
                                      0,
                                      error);
}
