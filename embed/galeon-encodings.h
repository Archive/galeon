/*
 *  Copyright (C) 2003 Christian Persch
 *  Copyright (C) 2003 Crispin Flowerday
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GALEON_ENCODINGS_H
#define GALEON_ENCODINGS_H

#include <glib-object.h>
#include <glib.h>

G_BEGIN_DECLS

#define GALEON_TYPE_ENCODINGS		(galeon_encodings_get_type ())
#define GALEON_ENCODINGS(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), GALEON_TYPE_ENCODINGS, GaleonEncodings))
#define GALEON_ENCODINGS_CLASS(k)	(G_TYPE_CHECK_CLASS_CAST ((k), GALEON_TYPE_ENCODINGS, GaleonEncodingsClass))
#define GALEON_IS_ENCODINGS(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), GALEON_TYPE_ENCODINGS))
#define GALEON_IS_ENCODINGS_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), GALEON_TYPE_ENCODINGS))
#define GALEON_ENCODINGS_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), GALEON_TYPE_ENCODINGS, GaleonEncodingsClass))

typedef struct GaleonEncodingsPrivate GaleonEncodingsPrivate;

typedef enum
{
	LG_NONE			= 0,
	LG_ARABIC		= 1 << 0,
	LG_BALTIC		= 1 << 1,
	LG_CAUCASIAN		= 1 << 2,
	LG_C_EUROPEAN		= 1 << 3,
	LG_CHINESE_TRAD		= 1 << 4,
	LG_CHINESE_SIMP		= 1 << 5,
	LG_CYRILLIC		= 1 << 6,
	LG_GREEK		= 1 << 7,
	LG_HEBREW		= 1 << 8,
	LG_INDIAN		= 1 << 9,
	LG_JAPANESE		= 1 << 10,
	LG_KOREAN		= 1 << 12,
	LG_NORDIC		= 1 << 13,
	LG_PERSIAN		= 1 << 14,
	LG_SE_EUROPEAN		= 1 << 15,
	LG_THAI			= 1 << 16,
	LG_TURKISH		= 1 << 17,
	LG_UKRAINIAN		= 1 << 18,
	LG_UNICODE		= 1 << 19,
	LG_VIETNAMESE		= 1 << 20,
	LG_WESTERN		= 1 << 21,
	LG_ALL			= 0x3fffff,
}
GaleonLanguageGroup;

typedef struct
{
	char *encoding;
	gboolean forced;
}
GaleonEncodingPageInfo;

typedef struct
{
	const char *title;
	const char *encoding;
	GaleonLanguageGroup group;	
} GaleonEncodingInfo;

typedef struct
{
	GObject parent;

	/*< private >*/
	GaleonEncodingsPrivate *priv;
} GaleonEncodings;

typedef struct
{
	GObjectClass parent_class;

	/* Methods */
	GList  *(* get_all_encodings) (GaleonEncodings *encodings);
} GaleonEncodingsClass;

GType		 galeon_encodings_get_type        (void);

void		 galeon_encodings_add_recent	(GaleonEncodings *encodings,
						 const char *code);

GList		*galeon_encodings_get_recent	(GaleonEncodings *encodings);

const GaleonEncodingInfo
                *galeon_encodings_get_encoding  (GaleonEncodings *encodings,
						 const char *code);

GList           *galeon_encodings_get_encodings  (GaleonEncodings *encodings,
						  GaleonLanguageGroup group_mask);

void		 galeon_encoding_page_info_free	 (GaleonEncodingPageInfo *info);

G_END_DECLS

#endif
