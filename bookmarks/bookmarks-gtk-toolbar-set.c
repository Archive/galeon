/* -*- mode: c c-style: k&r c-basic-offset: 8 -*- */
/*
 *  Copyright (C) 2003  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n.h>
#include "bookmarks-gtk-toolbar-set.h"
#include "galeon-marshal.h"
#include "gul-string.h"
#include "bookmarks-gtk-toolbar.h"

/**
 * Private data
 */
#define GB_GTK_TOOLBAR_SET_GET_PRIVATE(object) (G_TYPE_INSTANCE_GET_PRIVATE ((object), \
				       GB_TYPE_GTK_TOOLBAR_SET, GbGtkToolbarSetPrivate))


struct _GbGtkToolbarSetPrivate 
{
	GbBookmarkSet *set;
	GbGtkToolbarSetAddFunc add_func;
	GbGtkToolbarSetRemoveFunc remove_func;
	gpointer user_data;
	gboolean visible;
	GHashTable *folder_to_toolbar;

	GtkStatusbar *statusbar;
   	GbLocationSource *location_source;
};

static void	gb_gtk_toolbar_set_finalize_impl		(GObject *o);
static void	gb_gtk_toolbar_set_build			(GbGtkToolbarSet *gts);
static void	gb_gtk_toolbar_set_bookmark_activated_cb	(GtkWidget *w, 
								 GbBookmarkEventActivated *ev,
								 GbGtkToolbarSet *buit);
static void	gb_gtk_toolbar_set_toolbar_cb			 (GbBookmarkSet *set, GbFolder *f,
								  GbGtkToolbarSet *buit);



enum GbGtkToolbarSetSignalsEnum {
	GB_GTK_TOOLBAR_SET_BOOKMARK_ACTIVATED,
	GB_GTK_TOOLBAR_SET_LAST_SIGNAL
};
static gint GbGtkToolbarSetSignals[GB_GTK_TOOLBAR_SET_LAST_SIGNAL];

/**
 * BonoboUIToolbar object
 */

G_DEFINE_TYPE (GbGtkToolbarSet, gb_gtk_toolbar_set, G_TYPE_OBJECT);

static void
gb_gtk_toolbar_set_class_init (GbGtkToolbarSetClass *klass)
{
	G_OBJECT_CLASS (klass)->finalize = gb_gtk_toolbar_set_finalize_impl;

	GbGtkToolbarSetSignals[GB_GTK_TOOLBAR_SET_BOOKMARK_ACTIVATED] = g_signal_new (
		"bookmark-activated", G_OBJECT_CLASS_TYPE (klass),  
		G_SIGNAL_RUN_FIRST | G_SIGNAL_RUN_LAST | G_SIGNAL_RUN_CLEANUP,
                G_STRUCT_OFFSET (GbGtkToolbarSetClass, gb_gtk_toolbar_set_bookmark_activated), 
		NULL, NULL, 
		g_cclosure_marshal_VOID__POINTER,
		G_TYPE_NONE, 1, G_TYPE_POINTER);

	g_type_class_add_private (klass, sizeof (GbGtkToolbarSetPrivate));
}

static void 
gb_gtk_toolbar_set_init (GbGtkToolbarSet *m)
{
	GbGtkToolbarSetPrivate *p = GB_GTK_TOOLBAR_SET_GET_PRIVATE (m);
	m->priv = p;

	p->visible = TRUE;
	p->folder_to_toolbar = g_hash_table_new_full (NULL, NULL, g_object_unref, g_object_unref);
}

static void
gb_gtk_toolbar_set_finalize_impl (GObject *o)
{
	GbGtkToolbarSet *buit = GB_GTK_TOOLBAR_SET (o);
	GbGtkToolbarSetPrivate *p = buit->priv;

	gb_gtk_toolbar_set_set_location_source (buit, NULL);

	g_hash_table_destroy (p->folder_to_toolbar);

	g_signal_handlers_disconnect_matched (p->set, G_SIGNAL_MATCH_DATA, 0, 0, NULL, NULL, buit);
	g_object_unref (p->set);

	if (p->statusbar)
	{
		g_object_unref (p->statusbar);
	}

	G_OBJECT_CLASS (gb_gtk_toolbar_set_parent_class)->finalize (o);
}

GbGtkToolbarSet *
gb_gtk_toolbar_set_new (GbBookmarkSet *set,
			GbGtkToolbarSetAddFunc add_toolbar,
			GbGtkToolbarSetRemoveFunc remove_toolbar,
			gpointer user_data)
{
	GbGtkToolbarSet *ret = g_object_new (GB_TYPE_GTK_TOOLBAR_SET, NULL);
	GbGtkToolbarSetPrivate *p = ret->priv;

	p->set = g_object_ref (set);
	p->add_func = add_toolbar;
	p->remove_func = remove_toolbar;
	p->user_data = user_data;

	g_signal_connect (set, "toolbar", G_CALLBACK (gb_gtk_toolbar_set_toolbar_cb), ret);
	
	gb_gtk_toolbar_set_build (ret);

	return ret;
}

static void
gb_gtk_toolbar_set_foreach_set_visible (gpointer key, gpointer data, gpointer user_data)
{
	GbGtkToolbarSet *tbs = user_data;
	GbGtkToolbarSetPrivate *p = tbs->priv;
	GbGtkToolbar *tb = data;
	if (p->visible)
	{
		gtk_widget_show (GTK_WIDGET (tb));
	}
	else
	{
		gtk_widget_hide (GTK_WIDGET (tb));
	}
}

void
gb_gtk_toolbar_set_set_visible (GbGtkToolbarSet *buit, gboolean visible)
{
	GbGtkToolbarSetPrivate *p = buit->priv;
	p->visible = !!visible;

	g_hash_table_foreach (p->folder_to_toolbar, gb_gtk_toolbar_set_foreach_set_visible, buit);
}

static void
gb_gtk_toolbar_set_foreach_set_location_source (gpointer key, gpointer data, gpointer user_data)
{
	GbLocationSource *location_source = user_data;
	GbGtkToolbar *tb = data;
	gb_gtk_toolbar_set_location_source (tb, location_source);
}

void
gb_gtk_toolbar_set_set_location_source (GbGtkToolbarSet *buit, GbLocationSource *src)
{
	GbGtkToolbarSetPrivate *p = buit->priv;
	
	if (p->location_source)
	{
		g_object_remove_weak_pointer (G_OBJECT (p->location_source),
					      (gpointer *) &p->location_source);
	}

	p->location_source = src;

	if (p->location_source)
	{
		g_object_add_weak_pointer (G_OBJECT (p->location_source), 
					   (gpointer *) &p->location_source);
	}

	g_hash_table_foreach (p->folder_to_toolbar, gb_gtk_toolbar_set_foreach_set_location_source, p->location_source);
}

static void
gb_gtk_toolbar_set_bookmark_activated_cb (GtkWidget *w, 
					  GbBookmarkEventActivated *ev,
					  GbGtkToolbarSet *buit)
{
	g_signal_emit (buit, GbGtkToolbarSetSignals[GB_GTK_TOOLBAR_SET_BOOKMARK_ACTIVATED], 0, ev);
}

static void
gb_gtk_toolbar_set_remove_folder (GbGtkToolbarSet *gts, GbFolder *f)
{
	GbGtkToolbarSetPrivate *p = gts->priv;
	GtkWidget *tb = g_hash_table_lookup (p->folder_to_toolbar, f);
	
	if (!tb)
	{
		/* could this happen? */
		return;
	}

	if (p->remove_func)
	{
		p->remove_func (gts, f, tb, p->user_data);
	}

	g_hash_table_remove (p->folder_to_toolbar, f);
}

static void
gb_gtk_toolbar_set_build_folder (GbGtkToolbarSet *gts, GbFolder *f)
{
	GbGtkToolbarSetPrivate *p = gts->priv;
	GbGtkToolbar *tb = gb_gtk_toolbar_new (f);
	
	gtk_widget_show (GTK_WIDGET (tb));
	g_hash_table_insert (p->folder_to_toolbar, g_object_ref (f), g_object_ref (tb));
	g_object_ref_sink (tb);
	
	if (p->add_func)
	{
		p->add_func (gts, f, GTK_WIDGET (tb), p->user_data);
	}

	gb_gtk_toolbar_set_location_source (tb, p->location_source);
	gb_gtk_toolbar_set_statusbar (tb, p->statusbar);
	g_signal_connect (tb, "bookmark-activated", 
			  G_CALLBACK (gb_gtk_toolbar_set_bookmark_activated_cb), gts);
}

static void
gb_gtk_toolbar_set_build (GbGtkToolbarSet *gts)
{
	GbGtkToolbarSetPrivate *p = gts->priv;
	GSList *l;
	GSList *li;

	l = gb_bookmark_set_get_toolbars (p->set);
	for (li = l; li != NULL; li = li->next)
	{
		GbFolder *f = GB_FOLDER (li->data);
		gb_gtk_toolbar_set_build_folder (gts, f);
	}
	g_slist_free (l);
}

static void
gb_gtk_toolbar_set_toolbar_cb (GbBookmarkSet *set, GbFolder *f, GbGtkToolbarSet *buit)
{
	if (f->create_toolbar)
	{
		gb_gtk_toolbar_set_build_folder (buit, f);
	}
	else
	{
		gb_gtk_toolbar_set_remove_folder (buit, f);
	}
}

static void
gb_gtk_toolbar_set_foreach_set_statusbar (gpointer key, gpointer data, gpointer user_data)
{
	GtkStatusbar *statusbar = user_data;
	GbGtkToolbar *tb = data;
	gb_gtk_toolbar_set_statusbar (tb, statusbar);
}

void
gb_gtk_toolbar_set_set_statusbar (GbGtkToolbarSet *gtbs, GtkStatusbar *sb)
{
	GbGtkToolbarSetPrivate *p = gtbs->priv;
	g_return_if_fail (!sb || GTK_IS_STATUSBAR (sb));
	if (p->statusbar)
	{
		g_object_unref (p->statusbar);
		p->statusbar = NULL;
	}
	if (sb)
	{
		p->statusbar = g_object_ref (sb);
	}	

	g_hash_table_foreach (p->folder_to_toolbar, gb_gtk_toolbar_set_foreach_set_statusbar, p->statusbar);
}
