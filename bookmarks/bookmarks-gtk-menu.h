/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __bookmarks_gtk_menu_h
#define __bookmarks_gtk_menu_h

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtkmenushell.h>
#include <gtk/gtkstatusbar.h>
#include "bookmarks.h"
#include "bookmarks-util.h"
#include "bookmarks-location-source.h"

/* object forward declarations */

typedef struct _GbGtkMenu GbGtkMenu;
typedef struct _GbGtkMenuClass GbGtkMenuClass;
typedef struct _GbGtkMenuPrivate GbGtkMenuPrivate;

/**
 * GbGtkMenu object
 */

#define GB_TYPE_GTK_MENU		(gb_gtk_menu_get_type())
#define GB_GTK_MENU(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), GB_TYPE_GTK_MENU,\
					 GbGtkMenu))
#define GB_GTK_MENU_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), GB_TYPE_GTK_MENU,\
					 GbGtkMenuClass))
#define GB_IS_GTK_MENU(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), GB_TYPE_GTK_MENU))
#define GB_IS_GTK_MENU_ITEM_CLASS(klass) \
 					(G_TYPE_CHECK_CLASS_TYPE((klass), GB_TYPE_GTK_MENU))
#define GB_GTK_MENU_GET_CLASS(obj) 	(G_TYPE_INSTANCE_GET_CLASS((obj), GB_TYPE_GTK_MENU,\
					 GbGtkMenuClass))

struct _GbGtkMenuClass
{
	GObjectClass parent_class;

	/* signals */
	GbBookmarkActivatedCallback gb_gtk_menu_bookmark_activated;

};

/* Remember: fields are public read-only */
struct _GbGtkMenu
{
	GObject parent_object;
	
	GbGtkMenuPrivate *priv;
};

GType		gb_gtk_menu_get_type			(void);
GbGtkMenu *	gb_gtk_menu_new				(GbFolder *root, GtkMenuShell *ms);
GbGtkMenu *	gb_gtk_menu_new_context_only		(GbBookmarkSet *set, GtkMenuShell *ms);
void		gb_gtk_menu_set_location_source		(GbGtkMenu *gm, GbLocationSource *src);
void		gb_gtk_menu_fill_children_submenus	(GbGtkMenu *gm);
void		gb_gtk_menu_set_statusbar 		(GbGtkMenu *gm, GtkStatusbar *sb);

#endif
