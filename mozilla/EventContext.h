/*
 *  Copyright (C) 2000 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef EVENT_CONTEXT_H
#define EVENT_CONTEXT_H

#include "galeon-embed.h"
#include "galeon-embed-event.h"
#include <nsCOMPtr.h>

/* Forward declarations */
class nsIDOMMouseEvent;
class nsIDOMKeyEvent;
class nsIDOMEventTarget;
class nsIDOMNode;
class nsIDOMHTMLAnchorElement;
class nsIDOMHTMLAreaElement;
class nsIDOMHTMLBodyElement;
class nsIDOMElementCSSInlineStyle;
class nsIDOMCSSStyleDeclaration;
class nsIDOMDocument;
class GaleonWrapper;
class nsAString;
class nsACString;

class EventContext
{
public:
	EventContext();
	~EventContext();

	nsresult Init (GaleonWrapper *wrapper);

	nsresult GetMouseEventInfo (nsIDOMMouseEvent *event, GaleonEmbedEvent *info);
	nsresult GetKeyEventInfo (nsIDOMKeyEvent *event, GaleonEmbedEvent *info);
	nsresult GetTargetDocument (nsIDOMDocument **domDoc);

	static PRBool CheckKeyPress (nsIDOMKeyEvent *aEvent);

private:
	GaleonWrapper *mWrapper;
	nsCOMPtr<nsIDOMDocument> mDOMDocument;

	nsresult GetEventContext (nsIDOMEventTarget *EventTarget,
				  GaleonEmbedEvent *info);
	nsresult GetCSSBackground (nsIDOMNode *node, nsAString& url);
	nsresult IsPageFramed (nsIDOMNode *node, PRBool *Framed);
	nsresult SetIntProperty (const char *name, int value);
	nsresult SetStringProperty (const char *name, const char *value);
	nsresult SetStringProperty (const char *name, const nsAString &value);
	nsresult CheckLinkScheme (const nsAString &link);
	nsresult GatherTextUnder (nsIDOMNode* aNode, nsAString& aResult);
	nsresult Unescape (const nsACString &aEscaped, nsAString &aUnescaped);
	nsresult GetTargetCoords (nsIDOMEventTarget *aTarget, PRInt32 *aX, PRInt32 *aY);
	nsresult ResolveBaseURL  (const nsAString &relurl, nsACString &url);
	nsresult CheckInput (nsIDOMNode *aNode);

	GaleonEmbedEvent *mEmbedEvent;
};

#endif
