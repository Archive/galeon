/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __window_recent_history_h
#define __window_recent_history_h

#include "galeon-window.h"

/* object forward declarations */

typedef struct _GaleonWindowRecentHistory GaleonWindowRecentHistory;
typedef struct _GaleonWindowRecentHistoryClass GaleonWindowRecentHistoryClass;
typedef struct _GaleonWindowRecentHistoryPrivate GaleonWindowRecentHistoryPrivate;

/**
 * Editor object
 */

#define GALEON_TYPE_WINDOW_RECENT_HISTORY	      	(galeon_window_recent_history_get_type())
#define GALEON_WINDOW_RECENT_HISTORY(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), \
							 GALEON_TYPE_WINDOW_RECENT_HISTORY,\
							 GaleonWindowRecentHistory))
#define GALEON_WINDOW_RECENT_HISTORY_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), \
							 GALEON_TYPE_WINDOW_RECENT_HISTORY,\
							 GaleonWindowRecentHistoryClass))
#define GALEON_IS_WINDOW_RECENT_HISTORY(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), \
							 GALEON_TYPE_WINDOW_RECENT_HISTORY))
#define GALEON_IS_WINDOW_RECENT_HISTORY_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), \
							 GALEON_TYPE_WINDOW_RECENT_HISTORY))
#define GALEON_WINDOW_RECENT_HISTORY_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), \
							 GALEON_TYPE_WINDOW_RECENT_HISTORY,\
							 GaleonWindowRecentHistoryClass))

struct _GaleonWindowRecentHistoryClass 
{
	GObjectClass parent_class;
	
	/* signals */
	void	(*changed)	(GaleonWindowRecentHistory *wrh);
};

/* Remember: fields are public read-only */
struct _GaleonWindowRecentHistory
{
	GObject parent_object;

	GaleonWindowRecentHistoryPrivate *priv;
};

GType
galeon_window_recent_history_get_type		(void);

GaleonWindowRecentHistory *
galeon_window_recent_history_new		(void);

void
galeon_window_recent_history_visited		(GaleonWindowRecentHistory *wrh,
						 const gchar *url,
						 const gchar *title);

void
galeon_window_recent_history_get_num_items	(GaleonWindowRecentHistory *wrh, int *n);

void
galeon_window_recent_history_get_item		(GaleonWindowRecentHistory *wrh, int n,
						 gchar **url, gchar **title);

void
galeon_window_recent_history_clear		(GaleonWindowRecentHistory *wrh);

void
galeon_window_recent_history_set_max_num_items	(GaleonWindowRecentHistory *wrh, int m);


#endif

