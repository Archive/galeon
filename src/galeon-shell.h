/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GALEON_SHELL_H
#define GALEON_SHELL_H

#include "global-history.h"
#include "galeon-autocompletion.h"
#include "downloader-view.h"
#include "galeon-embed-shell.h"
#include "bookmarks.h"
#include "galeon-session.h"
#include "galeon-favicon-cache.h"
#include "egg-toolbars-model.h"
#include "galeon-sidebars.h"

#include <glib-object.h>
#include <glib.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef GALEON_SHELL_TYPE_DEF
typedef struct GaleonShell GaleonShell;
#define GALEON_SHELL_TYPE_DEF
#endif

typedef struct GaleonShellClass GaleonShellClass;

#define GALEON_TYPE_SHELL             (galeon_shell_get_type ())
#define GALEON_SHELL(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GALEON_TYPE_SHELL, GaleonShell))
#define GALEON_SHELL_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GALEON_SHELL, GaleonShellClass))
#define GALEON_IS_SHELL(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GALEON_TYPE_SHELL))
#define GALEON_IS_SHELL_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GALEON_SHELL))

typedef struct GaleonShellPrivate GaleonShellPrivate;

extern GaleonShell *galeon_shell;

typedef enum
{
	GALEON_NEW_TAB_HOMEPAGE      = 1 << 0,
	GALEON_NEW_TAB_FULLSCREEN    = 1 << 1,
	GALEON_NEW_TAB_APPEND        = 1 << 2,
	GALEON_NEW_TAB_JUMP          = 1 << 3,
	GALEON_NEW_TAB_DONT_JUMP_TO  = 1 << 4,
	GALEON_NEW_TAB_RAISE_WINDOW  = 1 << 5,
	GALEON_NEW_TAB_IN_NEW_WINDOW = 1 << 6,
	GALEON_NEW_TAB_IN_EXISTING_WINDOW = 1 << 7,
	GALEON_NEW_TAB_IS_A_COPY     = 1 << 8,
	GALEON_NEW_TAB_VIEW_SOURCE   = 1 << 9,
	GALEON_NEW_TAB_COPY_HISTORY  = 1 << 10
} GaleonNewTabFlags;

struct GaleonShell 
{
        GObject parent;
        GaleonShellPrivate *priv;
};

struct GaleonShellClass
{
        GObjectClass parent_class;
};

GType               galeon_shell_get_type            (void);

GaleonShell        *galeon_shell_new                 (void);

GaleonShell        *galeon_shell_get_default         (void);

GaleonEmbedShell   *galeon_shell_get_embed_shell     (GaleonShell *gs);

void		    galeon_shell_set_active_window   (GaleonShell *gs,
						      GaleonWindow *window);
GaleonWindow 	   *galeon_shell_get_active_window   (GaleonShell *gs);

GaleonTab          *galeon_shell_new_tab	     (GaleonShell *shell,
						      GaleonWindow *parent_window,
						      GaleonTab *previous_tab,
						      const char *url,
						      GaleonNewTabFlags flags);

GaleonTab          *galeon_shell_new_tab_full	     (GaleonShell *shell,
						      GaleonWindow *parent_window,
						      GaleonTab *previous_tab,
						      const char *url,
						      GaleonNewTabFlags flags,
						      guint32 user_time);


GaleonTab          *galeon_shell_new_tab_from_embed  (GaleonShell *shell,
						      GaleonWindow *parent_window,
						      GaleonEmbed *previous_embed,
						      const char *url,
						      GaleonNewTabFlags flags);

GaleonTab          *galeon_shell_new_tab_from_embed_full  (GaleonShell *shell,
							   GaleonWindow *parent_window,
							   GaleonEmbed *previous_embed,
							   const char *url,
							   GaleonNewTabFlags flags,
							   guint32 user_time);

GbBookmarkSet      *galeon_shell_get_bookmark_set    (GaleonShell *gs);

Session		   *galeon_shell_get_session	     (GaleonShell *gs);

GaleonFaviconCache *galeon_shell_get_favicon_cache   (GaleonShell *gs);

GaleonAutocompletion *galeon_shell_get_autocompletion (GaleonShell *gs);

GaleonNewTabFlags   galeon_shell_modifier_flags	     (GdkModifierType modifier);

gboolean            galeon_shell_get_server_mode     (GaleonShell *gs);
void                galeon_shell_set_server_mode     (GaleonShell *gs, gboolean mode);

EggToolbarsModel *  galeon_shell_get_toolbars_model  (GaleonShell *gs);

GaleonSidebars *    galeon_shell_get_sidebars        (GaleonShell *gs);

#ifdef __cplusplus
}
#endif

#endif
