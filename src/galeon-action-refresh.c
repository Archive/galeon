/*
 *  Copyright (C) 2003 Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "galeon-action-refresh.h"
#include "galeon-shell.h"
#include "gul-toolbutton.h"
#include "gul-gui.h"

#include <gtk/gtkstock.h>
#include <gtk/gtkimagemenuitem.h>
#include <gtk/gtkmain.h>
#include <glib/gi18n.h>
#include <string.h>

static void		galeon_action_refresh_connect_proxy		(GtkAction *action, GtkWidget *proxy);
static void		galeon_action_refresh_activate			(GtkAction *action);



G_DEFINE_TYPE (GaleonActionRefresh, galeon_action_refresh, GALEON_TYPE_ACTION);


static void
galeon_action_refresh_class_init (GaleonActionRefreshClass *class)
{
	GtkActionClass *action_class = GTK_ACTION_CLASS (class);

	action_class->connect_proxy = galeon_action_refresh_connect_proxy;
	action_class->menu_item_type    = GTK_TYPE_IMAGE_MENU_ITEM;
	action_class->toolbar_item_type = GUL_TYPE_TOOLBUTTON;
	action_class->activate          = galeon_action_refresh_activate;

}

static void
galeon_action_refresh_init (GaleonActionRefresh *action)
{
	g_object_set (G_OBJECT (action), "stock_id", GTK_STOCK_REFRESH, NULL);
	g_object_set (G_OBJECT (action), "tooltip", _("Display the latest content of the current page"), NULL);
}

static void
galeon_action_refresh_activate (GtkAction *a)
{
	GaleonEmbed *embed;
	GdkEvent *event;
	EmbedReloadFlags reload_type = EMBED_RELOAD_NORMAL;

	embed = galeon_action_get_embed (GALEON_ACTION (a));

	event = gtk_get_current_event ();
	if (event)
	{
		guint modifier = event->button.state &
			gtk_accelerator_get_default_mod_mask ();

		if (event->type == GDK_BUTTON_RELEASE &&
		    modifier == GDK_SHIFT_MASK)
		{
			reload_type = EMBED_RELOAD_FORCE;
		}
		gdk_event_free (event);
	}

	galeon_embed_reload (embed, reload_type);
}

static void
galeon_action_refresh_reload_all_tabs_activate_cb (GtkMenuItem *mi, GaleonActionRefresh *b)
{
	GList *tabs;
	GaleonWindow *window;
	GList *li;

	window = galeon_action_get_window (GALEON_ACTION (b));
	g_return_if_fail (window != NULL);

	tabs = galeon_window_get_tabs (window);
	for (li = tabs; li; li = li->next)
	{
		GaleonEmbed *embed = galeon_tab_get_embed (li->data);
		galeon_embed_reload (embed, EMBED_RELOAD_NORMAL);
	}
	
	g_list_free (tabs);
}

static void
galeon_action_refresh_reload_all_windows_activate_cb (GtkMenuItem *mi, GaleonActionRefresh *b)
{
	const GList *windows;
	const GList *lj;
	Session *session;

	session = galeon_shell_get_session (galeon_shell);
	windows = session_get_windows (session);

	for (lj = windows; lj; lj = lj->next)
	{
		GaleonWindow *window = lj->data;
		GList *tabs;
		GList *li;
		
		tabs = galeon_window_get_tabs (window);
		for (li = tabs; li; li = li->next)
		{
			GaleonEmbed *embed = galeon_tab_get_embed (li->data);
			galeon_embed_reload (embed, EMBED_RELOAD_NORMAL);
		}
		
		g_list_free (tabs);
	}
}

static void
galeon_action_refresh_normal_reload_activate_cb (GtkMenuItem *mi, GaleonActionRefresh *a)
{
	GaleonEmbed *embed = galeon_action_get_embed (GALEON_ACTION (a));
	galeon_embed_reload (embed, EMBED_RELOAD_NORMAL);
}

static void
galeon_action_refresh_bypass_cache_activate_cb (GtkMenuItem *mi, GaleonActionRefresh *a)
{
	GaleonEmbed *embed = galeon_action_get_embed (GALEON_ACTION (a));
	galeon_embed_reload (embed, EMBED_RELOAD_FORCE);
}

static void
galeon_action_refresh_connect_proxy (GtkAction *a, GtkWidget *proxy)
{
	GtkWidget *widget = proxy;

	if (GUL_IS_TOOLBUTTON (proxy))
	{
		GtkMenuShell *ms;
		widget = gul_toolbutton_get_button (GUL_TOOLBUTTON (proxy));

		ms = gul_toolbutton_get_menu (GUL_TOOLBUTTON (proxy));

		gul_gui_append_new_menuitem (GTK_WIDGET (ms), _("Normal _Reload"), 
			       G_CALLBACK (galeon_action_refresh_normal_reload_activate_cb), a);
		gul_gui_append_new_menuitem (GTK_WIDGET (ms), _("Bypass _Cache"), 
			       G_CALLBACK (galeon_action_refresh_bypass_cache_activate_cb), a);
		
		gul_gui_append_separator (GTK_WIDGET (ms));
		gul_gui_append_new_menuitem (GTK_WIDGET (ms), _("Reload All Tabs in This _Window"), 
			       G_CALLBACK (galeon_action_refresh_reload_all_tabs_activate_cb), a);
		gul_gui_append_new_menuitem (GTK_WIDGET (ms), _("Reload All Tabs in A_ll Windows"), 
			       G_CALLBACK (galeon_action_refresh_reload_all_windows_activate_cb), a);
	}

	(* GTK_ACTION_CLASS (galeon_action_refresh_parent_class)->connect_proxy) (a, proxy);
}

