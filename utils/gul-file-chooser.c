/*
 *  Copyright (C) 2003 Marco Pesenti Gritti
 *  Copyright (C) 2003 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gul-file-chooser.h"
#include "gul-file-preview.h"
#include "eel-gconf-extensions.h"
#include "gul-state.h"
#include "galeon-debug.h"
#include "prefs-strings.h"

#include <gtk/gtkstock.h>
#include <libgnomevfs/gnome-vfs-ops.h>
#include <libgnomevfs/gnome-vfs-utils.h>
#include <string.h>

#define GUL_FILE_CHOOSER_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), GUL_TYPE_FILE_CHOOSER, GulFileChooserPrivate))

struct GulFileChooserPrivate
{
	char *persist_key;
};

static void gul_file_chooser_class_init	(GulFileChooserClass *klass);
static void gul_file_chooser_init		(GulFileChooser *dialog);

enum
{
	PROP_0,
	PROP_PERSIST_KEY
};

static GObjectClass *parent_class = NULL;

GType
gul_file_chooser_get_type (void)
{
	static GType type = 0;

	if (type == 0)
	{
		static const GTypeInfo our_info =
		{
			sizeof (GulFileChooserClass),
			NULL,
			NULL,
			(GClassInitFunc) gul_file_chooser_class_init,
			NULL,
			NULL,
			sizeof (GulFileChooser),
			0,
			(GInstanceInitFunc) gul_file_chooser_init
		};

		type = g_type_register_static (GTK_TYPE_FILE_CHOOSER_DIALOG,
					       "GulFileChooser",
					       &our_info, 0);
	}

	return type;
}

static void
current_folder_changed_cb (GtkFileChooser *chooser, GulFileChooser *dialog)
{
	if (dialog->priv->persist_key != NULL)
	{
		char *dir;
		char *converted;

		dir = gtk_file_chooser_get_current_folder (chooser);

		converted = g_filename_to_utf8 (dir, -1, NULL, NULL, NULL);
		eel_gconf_set_string (dialog->priv->persist_key, converted);

		g_free (converted);
		g_free (dir);
	}
}

static void
update_preview_cb (GtkFileChooser *chooser)
{
	GulFilePreview *preview;
	GtkWidget      *widget;
	char           *uri;
	gboolean        have_preview = FALSE;

	widget = gtk_file_chooser_get_preview_widget (chooser);
	if (!GUL_IS_FILE_PREVIEW (widget))
	{
		return;
	}

	preview = (GulFilePreview *) widget;

	uri = gtk_file_chooser_get_preview_uri (chooser);
	if (uri != NULL)
	{
		GnomeVFSFileInfo *file_info;

		file_info = gnome_vfs_file_info_new ();
		gnome_vfs_get_file_info (uri, file_info,
					 GNOME_VFS_FILE_INFO_GET_MIME_TYPE |
					 GNOME_VFS_FILE_INFO_FOLLOW_LINKS);

		if (file_info->type == GNOME_VFS_FILE_TYPE_REGULAR)
		{
			gul_file_preview_set_file_info (preview, uri, file_info);
			have_preview = TRUE;
		}

		gnome_vfs_file_info_unref (file_info);
		g_free (uri);
	}

	gtk_file_chooser_set_preview_widget_active (chooser, have_preview);
}

static void
gul_file_chooser_init (GulFileChooser *dialog)
{
	dialog->priv = GUL_FILE_CHOOSER_GET_PRIVATE (dialog);

	dialog->priv->persist_key = NULL;
}

static void
gul_file_chooser_finalize (GObject *object)
{
	GulFileChooser *dialog = GUL_FILE_CHOOSER (object);

	g_free (dialog->priv->persist_key);

	LOG ("GulFileChooser finalised");

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gul_file_chooser_set_use_preview (GulFileChooser *self, gboolean use_preview)
{
	if (use_preview)
	{
		GtkWidget *widget;
	       
		widget = gul_file_preview_new ();

		g_object_set (self, "preview-widget",        widget,
				    "preview-widget-active", FALSE,
				    "use-preview-label",     FALSE,
				    NULL);

		g_signal_connect (self, "update-preview", G_CALLBACK (update_preview_cb), 0);
	}
	else
	{
		g_signal_handlers_disconnect_by_func (self, G_CALLBACK (update_preview_cb), 0);
		g_object_set (self, "preview-widget", NULL, NULL);
	}
}

static char *
get_filename (const char *gconf_key)
{
	char *dir, *converted = NULL, *expanded = NULL;

	dir = eel_gconf_get_string (gconf_key);
	if (dir != NULL)
	{
		converted = g_filename_from_utf8 (dir, -1, NULL, NULL, NULL);
		g_free (dir);
	}

	if (converted != NULL)
	{
		expanded = gnome_vfs_expand_initial_tilde (converted);
		g_free (converted);
	}

	return expanded;
}

void
gul_file_chooser_set_persist_key (GulFileChooser *dialog, const char *key)
{
	char *dir;

	g_return_if_fail (key != NULL && key[0] != '\0');

	dialog->priv->persist_key = g_strdup (key);

	dir = get_filename (key);
	if (dir != NULL)
	{
		gtk_file_chooser_set_current_folder
			(GTK_FILE_CHOOSER (dialog), dir);

		g_free (dir);
	}

	g_signal_connect (dialog, "current-folder-changed",
			  G_CALLBACK (current_folder_changed_cb), dialog);
}

const char *
gul_file_chooser_get_persist_key (GulFileChooser *dialog)
{
	g_return_val_if_fail (GUL_IS_FILE_CHOOSER (dialog), NULL);

	return dialog->priv->persist_key;
}

static void
gul_file_chooser_set_property (GObject *object,
				guint prop_id,
				const GValue *value,
				GParamSpec *pspec)
{
	GulFileChooser *dialog = GUL_FILE_CHOOSER (object);
	
	switch (prop_id)
	{
		case PROP_PERSIST_KEY:
			gul_file_chooser_set_persist_key (dialog, g_value_get_string (value));
			break;
	}
}

static void
gul_file_chooser_get_property (GObject *object,
				guint prop_id,
				GValue *value,
				GParamSpec *pspec)
{
	GulFileChooser *dialog = GUL_FILE_CHOOSER (object);

	switch (prop_id)
	{
		case PROP_PERSIST_KEY:
			g_value_set_string (value, gul_file_chooser_get_persist_key (dialog));
			break;
	}
}

static void
gul_file_chooser_class_init (GulFileChooserClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gul_file_chooser_finalize;
	object_class->get_property = gul_file_chooser_get_property;
	object_class->set_property = gul_file_chooser_set_property;

	g_object_class_install_property (object_class,
					 PROP_PERSIST_KEY,
					 g_param_spec_string ("persist-key",
							      "Persist Key",
							      "The gconf key to which to persist the selected directory",
							      NULL,
							      G_PARAM_READWRITE));

	g_type_class_add_private (object_class, sizeof (GulFileChooserPrivate));
}

static gboolean
str_is_equal (const char *s1, const char *s2)
{
	g_return_val_if_fail (s1 != NULL, FALSE);
	return (s2 != NULL && strcmp (s1, s2) == 0);
}

static void
gul_file_chooser_add_shortcuts (GulFileChooser *dialog)
{
	GtkFileChooser *chooser;
	char           *download_dir;
	char           *upload_dir;
	char           *open_dir;

	chooser = (GtkFileChooser *) dialog;

	download_dir = get_filename (CONF_DOWNLOADING_DIR);
	upload_dir   = get_filename (CONF_STATE_LAST_UPLOAD_DIR);
	open_dir     = get_filename (CONF_STATE_OPEN_DIR);

	/* Try to avoid adding duplicates a little, at least until 
	 * http://bugzilla.gnome.org/show_bug.cgi?id=147521 gets fixed.
	 */

	if (download_dir != NULL &&
	    !str_is_equal (download_dir, g_get_home_dir ()))
	{
		gtk_file_chooser_add_shortcut_folder (chooser, download_dir, NULL);
	}
	if (upload_dir != NULL &&
	    !str_is_equal (upload_dir, g_get_home_dir ()) &&
	    !str_is_equal (upload_dir, download_dir))
	{
		gtk_file_chooser_add_shortcut_folder (chooser, upload_dir, NULL);
	}
	if (open_dir != NULL &&
	    !str_is_equal (open_dir, g_get_home_dir ()) &&
	    !str_is_equal (open_dir, download_dir) &&
	    !str_is_equal (open_dir, upload_dir))
	{
		gtk_file_chooser_add_shortcut_folder (chooser, open_dir, NULL);
	}

	g_free (download_dir);
	g_free (upload_dir);
	g_free (open_dir);
}

GulFileChooser	*
gul_file_chooser_new (const char *title,
		       GtkWidget *parent,
		       GtkFileChooserAction action,
		       const char *persist_key)
{
	GulFileChooser *dialog;

	dialog = GUL_FILE_CHOOSER (g_object_new (GUL_TYPE_FILE_CHOOSER,
						  "title", title,
						  "action", action,
						  NULL));

	gul_file_chooser_add_shortcuts (dialog);

	if (action == GTK_FILE_CHOOSER_ACTION_OPEN)
	{
		gul_file_chooser_set_use_preview (dialog, TRUE);
	}

	/* NOTE: We cannot set this property on object construction time.
	 * This is because GtkFileChooserDialog overrides the gobject
	 * constructor; the GtkFileChooser delegate will only be set
	 * _after_ our instance_init and construct-param setters will have
	 * run.
	 */
	if (persist_key != NULL)
	{
		gul_file_chooser_set_persist_key (dialog, persist_key);
	}

	if (action == GTK_FILE_CHOOSER_ACTION_OPEN	    ||
	    action == GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER ||
	    action == GTK_FILE_CHOOSER_ACTION_CREATE_FOLDER)
	{
		gtk_dialog_add_buttons (GTK_DIALOG (dialog),
					GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
					NULL);
		gtk_dialog_set_default_response (GTK_DIALOG (dialog),
						 GTK_RESPONSE_ACCEPT);
	}
	else if (action == GTK_FILE_CHOOSER_ACTION_SAVE)
	{
		gtk_dialog_add_buttons (GTK_DIALOG (dialog),
					GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
					NULL);
		gtk_dialog_set_default_response (GTK_DIALOG (dialog),
						 GTK_RESPONSE_ACCEPT);
	}

	if (parent != NULL)
	{
		gtk_window_set_transient_for (GTK_WINDOW (dialog),
					      GTK_WINDOW (parent));

		gtk_window_set_destroy_with_parent (GTK_WINDOW (dialog), TRUE);
	}

	return dialog;
}
