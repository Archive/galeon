/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GALEON_EMBED_UTILS_H
#define GALEON_EMBED_UTILS_H

#include "galeon-embed-persist.h"
#include "galeon-embed-event.h"

#include <gtk/gtkwidget.h>

G_BEGIN_DECLS

void galeon_embed_utils_save_event_property     (GaleonEmbed *embed,
						 GaleonEmbedEvent *event,
						 gboolean always_ask_dest,
						 gboolean show_progress,
						 const char *fc_title,
						 const char *property);

void galeon_embed_utils_download_event_property (GaleonEmbed *embed,
						 GaleonEmbedEvent *event,
						 gboolean always_ask_dest,
						 const char *fc_title,
						 const char *property);

void galeon_embed_utils_nohandler_dialog_run    (GtkWidget *parent);

void galeon_embed_utils_free_page_properties	(EmbedPageProperties *props);

GList *galeon_embed_utils_get_user_stylesheets	(void);

gchar *galeon_embed_utils_get_user_sheet_path   (const gchar *sheetfile);

G_END_DECLS

#endif
