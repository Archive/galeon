/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GALEON_EMBED_PERSIST_H
#define GALEON_EMBED_PERSIST_H

#include "galeon-embed.h"

#include <gtk/gtkwidget.h>	
#include <glib-object.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>

G_BEGIN_DECLS

#define GALEON_TYPE_EMBED_PERSIST             (galeon_embed_persist_get_type ())
#define GALEON_EMBED_PERSIST(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GALEON_TYPE_EMBED_PERSIST, GaleonEmbedPersist))
#define GALEON_EMBED_PERSIST_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GALEON_TYPE_EMBED_PERSIST, GaleonEmbedPersistClass))
#define GALEON_IS_EMBED_PERSIST(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GALEON_TYPE_EMBED_PERSIST))
#define GALEON_IS_EMBED_PERSIST_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GALEON_EMBED_PERSIST))
#define GALEON_EMBED_PERSIST_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GALEON_TYPE_EMBED_PERSIST, GaleonEmbedPersistClass))

typedef struct GaleonEmbedPersist GaleonEmbedPersist;
typedef struct GaleonEmbedPersistPrivate GaleonEmbedPersistPrivate;
typedef struct GaleonEmbedPersistClass GaleonEmbedPersistClass;

typedef gboolean (*GaleonEmbedPersistStreamFunc) (GaleonEmbedPersist *persist, 
						  const char *buffer, guint len,
						  gpointer data);




typedef enum
{
	/* What to save */
	EMBED_PERSIST_MAINDOC       = 1 << 0, /* Save the main page */
	/* Not implemented yet: */
	/* EMBED_PERSIST_FRAME         = 1 << 1,  Save the current frame */

	/* Save flags */
	EMBED_PERSIST_SAVE_CONTENT  = 1 << 2, /* Save the content */
	EMBED_PERSIST_BYPASSCACHE   = 1 << 3, /* whether to bypass the cache */

	/* Other flags */
	EMBED_PERSIST_SHOW_PROGRESS  = 1 << 4, /* Show progress */ 
	EMBED_PERSIST_ASK_DESTINATION = 1 << 5, /* Always ask where to save */
	EMBED_PERSIST_ADD_TO_RECENT   = 1 << 6  /* Add to recent-files */
} EmbedPersistFlags;

struct GaleonEmbedPersist 
{
        GObject parent;
        GaleonEmbedPersistPrivate *priv;
};

struct GaleonEmbedPersistClass
{
        GObjectClass parent_class;

	/* Signals */

	void (* completed) (GaleonEmbedPersist *persist);

	void (* cancelled) (GaleonEmbedPersist *persist);

	/* Methods */
	gboolean (* save)   (GaleonEmbedPersist *persist);

	gboolean (* stream) (GaleonEmbedPersist *persist,
			     GaleonEmbedPersistStreamFunc func,
			     gpointer data);
};

GType               galeon_embed_persist_get_type    (void);

GaleonEmbedPersist *galeon_embed_persist_new         (GaleonEmbed *embed);

void                galeon_embed_persist_set_source  (GaleonEmbedPersist *persist,
						      const char *url);

const char *        galeon_embed_persist_get_source  (GaleonEmbedPersist *persist);

void		    galeon_embed_persist_set_dest    (GaleonEmbedPersist *persist,
						      const char *dir);

const char *	    galeon_embed_persist_get_dest    (GaleonEmbedPersist *persist);

void		    galeon_embed_persist_set_handler (GaleonEmbedPersist      *persist,
						      GnomeVFSMimeApplication *handler);

void                galeon_embed_persist_set_user_time (GaleonEmbedPersist *persist,
							guint32 user_time);

void		    galeon_embed_persist_set_max_size (GaleonEmbedPersist *persist,
						      int kb_size);

void                galeon_embed_persist_set_embed   (GaleonEmbedPersist *persist,
		                                      GaleonEmbed *embed);

GaleonEmbed *       galeon_embed_persist_get_embed   (GaleonEmbedPersist *persist);

void		    galeon_embed_persist_set_flags   (GaleonEmbedPersist *persist,
						      EmbedPersistFlags flags);

EmbedPersistFlags   galeon_embed_persist_get_flags   (GaleonEmbedPersist *persist);

void		    galeon_embed_persist_set_fc_title (GaleonEmbedPersist *persist,
						      const char * title);

const char *        galeon_embed_persist_get_fc_title (GaleonEmbedPersist *persist);

void		    galeon_embed_persist_set_fc_parent (GaleonEmbedPersist *persist,
							GtkWidget *parent);

GtkWidget *	    galeon_embed_persist_get_fc_parent (GaleonEmbedPersist *persist);


gboolean	    galeon_embed_persist_save        (GaleonEmbedPersist *persist);

gboolean            galeon_embed_persist_stream      (GaleonEmbedPersist *persist,
						      GaleonEmbedPersistStreamFunc func,
						      gpointer data);

G_END_DECLS

#endif
