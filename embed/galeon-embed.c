/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "galeon-embed.h"
#include "galeon-embed-shell.h"
#include "galeon-marshal.h"
#include <string.h>

enum
{
	NEW_WINDOW,
	LINK_MESSAGE,
	JS_STATUS,
	LOCATION,
	TITLE,
	PROGRESS,
	NET_STATE,
	VISIBILITY,
	DESTROY_BRSR,
	OPEN_URI,
	SIZE_TO,
	DOM_MOUSE_CLICK,
	DOM_MOUSE_DOWN,
	SECURITY_CHANGE,
	ZOOM_CHANGE,
	FAVICON,
	POPUP_BLOCKED,
	CONTEXTMENU,
	NET_STOP,
	NET_START,
	LAST_SIGNAL
};

static void
galeon_embed_base_init (gpointer base_class);

static guint galeon_embed_signals[LAST_SIGNAL] = { 0 };

GType 
galeon_embed_get_type (void)
{
        static GType galeon_embed_type = 0;

        if (galeon_embed_type == 0)
        {
                static const GTypeInfo our_info =
                {
                        sizeof (GaleonEmbedClass),
                        galeon_embed_base_init,
                        NULL,
                };

                galeon_embed_type = g_type_register_static (G_TYPE_INTERFACE,
							    "GaleonEmbed",
							    &our_info, 
							    (GTypeFlags)0);
        }

        return galeon_embed_type;
}


static void
galeon_embed_base_init (gpointer g_class)
{
	static gboolean initialized = FALSE;
	
  	if (! initialized)
    	{	
	galeon_embed_signals[NEW_WINDOW] =
                g_signal_new ("ge_new_window",
                              GALEON_TYPE_EMBED,
                              G_SIGNAL_RUN_LAST,
                              G_STRUCT_OFFSET (GaleonEmbedClass, new_window),
                              _galeon_embed_new_window_signal_accumulator, NULL,
                              galeon_marshal_OBJECT__INT,
			      G_TYPE_OBJECT,
                              1,
			      G_TYPE_INT);
	galeon_embed_signals[LINK_MESSAGE] =
                g_signal_new ("ge_link_message",
                              GALEON_TYPE_EMBED,
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (GaleonEmbedClass, link_message),
                              NULL, NULL,
                              galeon_marshal_VOID__STRING,
                              G_TYPE_NONE,
                              1,
			      G_TYPE_STRING);
	galeon_embed_signals[JS_STATUS] =
                g_signal_new ("ge_js_status",
                              GALEON_TYPE_EMBED,
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (GaleonEmbedClass, js_status),
                              NULL, NULL,
                              galeon_marshal_VOID__STRING,
			      G_TYPE_NONE,
                              1,
			      G_TYPE_STRING);
	galeon_embed_signals[LOCATION] =
                g_signal_new ("ge_location",
                              GALEON_TYPE_EMBED,
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (GaleonEmbedClass, location),
                              NULL, NULL,
                              g_cclosure_marshal_VOID__VOID,
                              G_TYPE_NONE,
                              0);
	galeon_embed_signals[TITLE] =
                g_signal_new ("ge_title",
                              GALEON_TYPE_EMBED,
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (GaleonEmbedClass, title),
                              NULL, NULL,
                              g_cclosure_marshal_VOID__VOID,
                              G_TYPE_NONE,
                              0);
	galeon_embed_signals[PROGRESS] =
                g_signal_new ("ge_progress",
                              GALEON_TYPE_EMBED,
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (GaleonEmbedClass, progress),
                              NULL, NULL,
                              galeon_marshal_VOID__STRING_INT_INT,
                              G_TYPE_NONE,
                              3,
			      G_TYPE_STRING,
			      G_TYPE_INT,
			      G_TYPE_INT);	
	galeon_embed_signals[NET_STATE] =
                g_signal_new ("ge_net_state",
                              GALEON_TYPE_EMBED,
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (GaleonEmbedClass, net_state),
                              NULL, NULL,
                              galeon_marshal_VOID__STRING_INT,
                              G_TYPE_NONE,
                              2,
			      G_TYPE_STRING,
			      G_TYPE_INT);
	galeon_embed_signals[VISIBILITY] =
                g_signal_new ("ge_visibility",
                              GALEON_TYPE_EMBED,
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (GaleonEmbedClass, visibility),
                              NULL, NULL,
                              g_cclosure_marshal_VOID__BOOLEAN,
                              G_TYPE_NONE,
                              1,
			      G_TYPE_BOOLEAN);
	galeon_embed_signals[DESTROY_BRSR] =
                g_signal_new ("ge_destroy_brsr",
                              GALEON_TYPE_EMBED,
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (GaleonEmbedClass, destroy_brsr),
                              NULL, NULL,
                              g_cclosure_marshal_VOID__VOID,
                              G_TYPE_NONE,
                              0);
	galeon_embed_signals[OPEN_URI] =
                g_signal_new ("ge_open_uri",
                              GALEON_TYPE_EMBED,
                              G_SIGNAL_RUN_LAST,
                              G_STRUCT_OFFSET (GaleonEmbedClass, open_uri),
                              NULL, NULL,
                              galeon_marshal_INT__STRING,
                              G_TYPE_INT,
                              1,
			      G_TYPE_STRING);
	galeon_embed_signals[SIZE_TO] =
                g_signal_new ("ge_size_to",
                              GALEON_TYPE_EMBED,
                              G_SIGNAL_RUN_LAST,
                              G_STRUCT_OFFSET (GaleonEmbedClass, size_to),
                              NULL, NULL,
                              galeon_marshal_VOID__INT_INT,
                              G_TYPE_NONE,
                              2,
			      G_TYPE_INT,
			      G_TYPE_INT);
	galeon_embed_signals[DOM_MOUSE_DOWN] =
                g_signal_new ("ge_dom_mouse_down",
                              GALEON_TYPE_EMBED,
                              G_SIGNAL_RUN_LAST,
                              G_STRUCT_OFFSET (GaleonEmbedClass, dom_mouse_down),
			      g_signal_accumulator_true_handled, NULL,
                              galeon_marshal_BOOLEAN__OBJECT,
                              G_TYPE_BOOLEAN,
                              1,
			      GALEON_TYPE_EMBED_EVENT);
	galeon_embed_signals[DOM_MOUSE_CLICK] =
                g_signal_new ("ge_dom_mouse_click",
                              GALEON_TYPE_EMBED,
                              G_SIGNAL_RUN_LAST,
                              G_STRUCT_OFFSET (GaleonEmbedClass, dom_mouse_click),
			      g_signal_accumulator_true_handled, NULL,
                              galeon_marshal_BOOLEAN__OBJECT,
                              G_TYPE_BOOLEAN,
                              1,
			      GALEON_TYPE_EMBED_EVENT);
	galeon_embed_signals[SECURITY_CHANGE] =
                g_signal_new ("ge_security_change",
                              GALEON_TYPE_EMBED,
                              G_SIGNAL_RUN_LAST,
                              G_STRUCT_OFFSET (GaleonEmbedClass, security_change),
                              NULL, NULL,
                              g_cclosure_marshal_VOID__INT,
                              G_TYPE_NONE,
                              1,
			      G_TYPE_INT);
	galeon_embed_signals[ZOOM_CHANGE] =
                g_signal_new ("ge_zoom_change",
                              GALEON_TYPE_EMBED,
                              G_SIGNAL_RUN_LAST,
                              G_STRUCT_OFFSET (GaleonEmbedClass, zoom_change),
                              NULL, NULL,
                              g_cclosure_marshal_VOID__INT,
                              G_TYPE_NONE,
                              1,
			      G_TYPE_INT);
	galeon_embed_signals[FAVICON] =
                g_signal_new ("ge_favicon",
                              GALEON_TYPE_EMBED,
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (GaleonEmbedClass, favicon),
                              NULL, NULL,
                              g_cclosure_marshal_VOID__STRING,
                              G_TYPE_NONE,
                              1,
			      G_TYPE_STRING);
	galeon_embed_signals[POPUP_BLOCKED] =
                g_signal_new ("ge_popupblocked",
                              GALEON_TYPE_EMBED,
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (GaleonEmbedClass, popupblocked),
                              NULL, NULL,
                              g_cclosure_marshal_VOID__STRING,
                              G_TYPE_NONE,
                              1,
			      G_TYPE_STRING);
	galeon_embed_signals[CONTEXTMENU] =
                g_signal_new ("ge_contextmenu",
                              GALEON_TYPE_EMBED,
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (GaleonEmbedClass, contextmenu),
                              NULL, NULL,
                              g_cclosure_marshal_VOID__OBJECT,
                              G_TYPE_NONE,
                              1,
			      GALEON_TYPE_EMBED_EVENT);
	galeon_embed_signals[NET_STOP] =
                g_signal_new ("ge_net_stop",
                              GALEON_TYPE_EMBED,
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (GaleonEmbedClass, net_stop),
                              NULL, NULL,
                              g_cclosure_marshal_VOID__VOID,
                              G_TYPE_NONE,
                              0);
	galeon_embed_signals[NET_START] =
                g_signal_new ("ge_net_start",
                              GALEON_TYPE_EMBED,
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (GaleonEmbedClass, net_start),
                              NULL, NULL,
                              g_cclosure_marshal_VOID__VOID,
                              G_TYPE_NONE,
                              0);
/**
 * GaleonEmbed::ge-content-change:
 * @embed:
 * @uri: URI of the new content
 *
 * The ::ge_content_change signal is emitted when a new page content
 * is being loaded into the browser. It's a good place to do view
 * related changes, for example to restore the zoom level of a page
 * or to set an user style sheet.
 **/
		g_signal_new ("ge_content_change",
			      GALEON_TYPE_EMBED,
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (GaleonEmbedClass, content_change),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__STRING,
			      G_TYPE_NONE,
			      1,
			      G_TYPE_STRING);

/**
 * GaleonEmbed::ge-modal-alert:
 * @embed:
 *
 * The ::ge-modal-alert signal is emitted when a DOM event will open a
 * modal alert.
 *
 * Return %TRUE to prevent the dialog from being opened.
 **/
		g_signal_new ("ge_modal_alert",
			      GALEON_TYPE_EMBED,
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (GaleonEmbedClass, modal_alert),
			      g_signal_accumulator_true_handled, NULL,
			      galeon_marshal_BOOLEAN__VOID,
			      G_TYPE_BOOLEAN,
			      0);
/**
 * GaleonEmbed::ge-modal-alert-closed:
 * @embed:
 *
 * The ::ge-modal-alert-closed signal is emitted when a modal alert put up by a
 * DOM event was closed.
 **/
		g_signal_new ("ge_modal_alert_closed",
			      GALEON_TYPE_EMBED,
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (GaleonEmbedClass, modal_alert_closed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE,
			      0);

/**
 * GaleonEmbed::ge-show-js-console
 * @embed:
 *
 * The ::ge-show-js-console signal is emitted when the rendering engine wants
 * the javascript console to be shown.
 *
 */
		g_signal_new ("ge_show_js_console",
			      GALEON_TYPE_EMBED,
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (GaleonEmbedClass, show_js_console),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE,
			      0);

/**
 * GaleonEmbed::ge-search-key-press:
 * @embed:
 * @event: the #GdkEventKey which triggered this signal
 *
 * The ::ge-search-key-press signal is emitted for keypresses which
 * should be used for find implementations.
 **/
                g_signal_new ("ge-search-key-press",
                              GALEON_TYPE_EMBED,
                              G_SIGNAL_RUN_LAST,
                              G_STRUCT_OFFSET (GaleonEmbedClass, search_key_press),
                              g_signal_accumulator_true_handled, NULL,
                              galeon_marshal_BOOLEAN__BOXED,
                              G_TYPE_BOOLEAN,
                              1,
                              GDK_TYPE_EVENT | G_SIGNAL_TYPE_STATIC_SCOPE);
	initialized = TRUE;
	}
}

void
galeon_embed_load_url (GaleonEmbed *embed, 
                       const char *url)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        klass->load_url (embed, url);
}

void
galeon_embed_stop_load (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        klass->stop_load (embed);
}

gboolean
galeon_embed_can_go_back (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        return klass->can_go_back (embed);
}

gboolean 
galeon_embed_can_go_forward (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        return klass->can_go_forward (embed);
}

gboolean 
galeon_embed_can_go_up (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        return klass->can_go_up (embed);
}

GSList *
galeon_embed_get_go_up_list (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        return klass->get_go_up_list (embed);
}

void
galeon_embed_go_back (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        klass->go_back (embed);
}

void
galeon_embed_go_forward (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        klass->go_forward (embed);
}

void
galeon_embed_go_up (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        klass->go_up (embed);
}

gboolean
galeon_embed_render_data (GaleonEmbed *embed, 
                          const char *data,
                          guint32 len,
                          const char *base_uri, 
                          const char *mime_type)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        return klass->render_data (embed, data, len, base_uri, mime_type);
}

gboolean
galeon_embed_open_stream (GaleonEmbed *embed,
                          const char *base_uri,
                          const char *mime_type)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        return klass->open_stream (embed, base_uri, mime_type);
}

void
galeon_embed_append_data (GaleonEmbed *embed,
                          const char *data, 
                          guint32 len)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        klass->append_data (embed, data, len);
}

void
galeon_embed_close_stream (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        klass->close_stream (embed);
}

char *
galeon_embed_get_title (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        return klass->get_title (embed);
}

char *
galeon_embed_get_location (GaleonEmbed *embed, 
                           gboolean toplevel,
                           gboolean requested)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        return klass->get_location (embed, toplevel, requested);
}

void
galeon_embed_reload (GaleonEmbed *embed, 
                     EmbedReloadFlags flags)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        klass->reload (embed, flags);
}

void
galeon_embed_copy_page (GaleonEmbed *dest,
			GaleonEmbed *source,
			EmbedDisplayType display_type)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (dest);
	klass->copy_page (dest, source, display_type);
}	

GList *
galeon_embed_get_link_tags (GaleonEmbed *embed,
                            const char *link_type)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        return klass->get_link_tags (embed, link_type);
}

void
galeon_embed_zoom_set (GaleonEmbed *embed, 
                       int zoom)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        klass->zoom_set (embed, zoom);
}

int
galeon_embed_zoom_get (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        return klass->zoom_get (embed);
}

gboolean 
galeon_embed_selection_can_cut (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        return klass->selection_can_cut (embed);
}

gboolean 
galeon_embed_selection_can_copy (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        return klass->selection_can_copy (embed);
}

gboolean 
galeon_embed_can_paste (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        return klass->can_paste (embed);
}

void
galeon_embed_select_all (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        klass->select_all (embed);
}

void
galeon_embed_selection_cut (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        klass->selection_cut (embed);
}

void
galeon_embed_selection_copy (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        klass->selection_copy (embed);
}

void
galeon_embed_paste (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        klass->paste (embed);
}

int
galeon_embed_shistory_count  (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        return klass->shistory_count (embed);
}

gboolean
galeon_embed_shistory_get_nth (GaleonEmbed *embed, 
                               int nth,
                               gboolean is_relative,
                               char **url,
                               char **title)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        return klass->shistory_get_nth (embed, nth, is_relative, url, title);
}

int
galeon_embed_shistory_get_pos (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        return klass->shistory_get_pos (embed);
}

gboolean
galeon_embed_shistory_go_nth (GaleonEmbed *embed, 
                              int nth)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        return klass->shistory_go_nth (embed, nth);
}

void
galeon_embed_shistory_copy (GaleonEmbed *source,
                            GaleonEmbed *dest,
			    gboolean back_history,
			    gboolean forward_history,
			    gboolean set_current)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (source);
        klass->shistory_copy (source, dest, back_history, forward_history, set_current);
}

void
galeon_embed_shistory_clear (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
	klass->shistory_clear (embed);
}

void
galeon_embed_scroll (GaleonEmbed *embed, 
                     EmbedScrollDirection direction)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        klass->scroll (embed, direction);
}

void
galeon_embed_scroll_page (GaleonEmbed *embed, 
			  EmbedScrollDirection direction)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        klass->scroll_page (embed, direction);
}

void
galeon_embed_fine_scroll (GaleonEmbed *embed, 
			  int horiz, int vert)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        klass->fine_scroll (embed, horiz, vert);
}

gboolean
galeon_embed_get_security_level (GaleonEmbed *embed, 
                                 EmbedSecurityLevel *level,
                                 char **description)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        return klass->get_security_level (embed, level, description);
}

gboolean
galeon_embed_set_encoding (GaleonEmbed *embed,
			   const char *encoding)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        return klass->set_encoding (embed, encoding);
}

GaleonEncodingPageInfo *
galeon_embed_get_encoding_info (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        return klass->get_encoding_info (embed);
}

gboolean
galeon_embed_has_modified_forms (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
	return klass->has_modified_forms (embed);
}

gboolean
galeon_embed_can_view_source (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
	return klass->can_view_source (embed);
}

void
galeon_embed_evaluate_javascript (GaleonEmbed *embed, const char *script)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
	klass->evaluate_javascript (embed, script);
}

gboolean
galeon_embed_print (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        return klass->print (embed);
}

void
galeon_embed_print_preview_close (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
        klass->print_preview_close (embed);
}

int
galeon_embed_print_preview_num_pages (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
	return klass->print_preview_num_pages (embed);
}	

gboolean
galeon_embed_print_preview_navigate (GaleonEmbed *embed,
				     EmbedPrintPreviewNavType navType,
				     gint pageNum)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS (embed);
	return klass->print_preview_navigate (embed, navType, pageNum);
}

EmbedPageProperties *
galeon_embed_get_page_properties(GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS(embed);
	return klass->get_page_properties (embed);
}

void
galeon_embed_show_page_certificate (GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS(embed);
	klass->show_page_certificate (embed);
}

GList *
galeon_embed_get_stylesheets(GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS(embed);
	return klass->get_stylesheets (embed);
}

EmbedStyleSheet *
galeon_embed_get_selected_stylesheet(GaleonEmbed *embed)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS(embed);
	return klass->get_selected_stylesheet (embed);
}

void
galeon_embed_set_stylesheet(GaleonEmbed *embed, EmbedStyleSheet *sheet)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS(embed);
	klass->set_stylesheet(embed, sheet);
}

gboolean
galeon_embed_apply_user_stylesheet(GaleonEmbed *embed, const gchar *sheetfile,
				   EmbedStyleSheet **retSheet)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS(embed);
	return klass->apply_user_stylesheet(embed, sheetfile, retSheet);
}

void
galeon_embed_remove_user_stylesheet(GaleonEmbed *embed, EmbedStyleSheet *sheet)
{
	GaleonEmbedClass *klass = GALEON_EMBED_GET_CLASS(embed);
	klass->remove_user_stylesheet(embed, sheet);
}

/* galeon_embed_stylesheet_new defined in mozilla-embed.cpp */
/* galeon_embed_stylesheet_free defined in mozilla-embed.cpp */

void
galeon_embed_stylesheet_list_free (GList *l)
{
	GList *li;
	for (li = l; li; li = li->next)
	{
		galeon_embed_stylesheet_free (li->data);
	}
	g_list_free (l);
}

gboolean
galeon_embed_stylesheet_compare	(EmbedStyleSheet *ess1, EmbedStyleSheet *ess2)
{
	if (ess2 == ess1)
	{
		return TRUE;
	}
	else if (ess1 && ess2)
	{
		if (ess1->name && ess2->name && !strcmp (ess1->name, ess2->name))
		{
			return TRUE;
		}
		if (ess2->type != ess1->type
		    || ess2->sheet != ess1->sheet)
		{
			return FALSE;
		}
		else if ((ess1->name == ess2->name) || !ess1->name || !ess2->name)
			 
		{
			return TRUE;
		}
		else
		{
			return TRUE;
		}
	}
	else
	{
		return FALSE;
	}
}

