/*
 *  Copyright (C) 2003 Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GALEON_LOCATION_ACTION_H
#define GALEON_LOCATION_ACTION_H


#include "galeon-action.h"

#define GALEON_TYPE_ACTION_LOCATION		(galeon_action_location_get_type ())
#define GALEON_ACTION_LOCATION(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), GALEON_TYPE_ACTION_LOCATION,\
						 GaleonActionLocation))
#define GALEON_ACTION_LOCATION_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), GALEON_TYPE_ACTION_LOCATION,\
						 GaleonActionLocationClass))
#define GALEON_IS_ACTION_LOCATION(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GALEON_TYPE_ACTION_LOCATION))
#define GALEON_IS_ACTION_LOCATION_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((obj), GALEON_TYPE_ACTION_LOCATION))
#define GALEON_ACTION_LOCATION_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), GALEON_TYPE_ACTION_LOCATION,\
						GaleonActionLocationClass))

typedef struct _GaleonActionLocation		GaleonActionLocation;
typedef struct _GaleonActionLocationClass	GaleonActionLocationClass;
typedef struct GaleonActionLocationPrivate	GaleonActionLocationPrivate;

struct _GaleonActionLocation
{
	GaleonAction parent;
};

struct _GaleonActionLocationClass
{
	GaleonActionClass parent_class;

};

GType		galeon_action_location_get_type			(void);

void		galeon_action_location_edit			(GaleonActionLocation *action);
gchar *		galeon_action_location_get_location		(GaleonActionLocation *action);
void		galeon_action_location_set_location		(GaleonActionLocation *action, const gchar *location);
void		galeon_action_location_set_is_secure		(GaleonActionLocation *action, gboolean is_secure);

#endif
