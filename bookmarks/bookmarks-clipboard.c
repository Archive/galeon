/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "bookmarks-clipboard.h"
#include "xbel.h"
#include "gul-string.h"
#include "galeon-debug.h"
#include <gtk/gtkclipboard.h>
#include <string.h>

/**
 * Private data
 */
#define GB_CLIPBOARD_GET_PRIVATE(object) (G_TYPE_INSTANCE_GET_PRIVATE ((object), \
				       GB_TYPE_CLIPBOARD, GbClipboardPrivate))


struct _GbClipboardPrivate 
{
	GbBookmarkSet *contents;
	GtkClipboard *clipboard;
};

enum {
	TARGET_XBEL
} targets_id;

#define XBEL_TARGET_ID "application/x-xbel"

static const GtkTargetEntry targets[] = 
{
	{ XBEL_TARGET_ID, 0, TARGET_XBEL }
};

static int ntargets = G_N_ELEMENTS (targets);

/**
 * Private functions, only availble from this file
 */
static void		gb_clipboard_finalize_impl		(GObject *o);
static void		gb_clipboard_get_cb			(GtkClipboard *clipboard,
								 GtkSelectionData *selection_data,
								 guint info,
								 gpointer user_data_or_owner);
static void 		gb_clipboard_clear_cb			(GtkClipboard *clipboard,
								 gpointer user_data_or_owner);


/**
 * Clipboard object
 */

G_DEFINE_TYPE (GbClipboard, gb_clipboard, G_TYPE_OBJECT);

static void
gb_clipboard_class_init (GbClipboardClass *klass)
{
	G_OBJECT_CLASS (klass)->finalize = gb_clipboard_finalize_impl;

	g_type_class_add_private (klass, sizeof (GbClipboardPrivate));
}

static void 
gb_clipboard_init (GbClipboard *cb)
{
	GbClipboardPrivate *p = GB_CLIPBOARD_GET_PRIVATE (cb);
	cb->priv = p;
	p->clipboard = gtk_clipboard_get (GDK_SELECTION_CLIPBOARD);
	p->contents = NULL;
}

static void
gb_clipboard_finalize_impl (GObject *o)
{
	GbClipboard *cb = GB_CLIPBOARD (o);

	gb_clipboard_clear (cb);

	G_OBJECT_CLASS (gb_clipboard_parent_class)->finalize (o);

}

GbClipboard *
gb_clipboard_new (void)
{
	GbClipboard *ret = g_object_new (GB_TYPE_CLIPBOARD, NULL);
	return ret;
}

/**
 * Returns a shared GbClipboard. 
 */
GbClipboard *
gb_clipboard_get_shared (void)
{
	static GbClipboard *c = NULL;
	
	if (!c)
	{
		c = gb_clipboard_new ();
		g_object_add_weak_pointer (G_OBJECT (c), (gpointer) &c);
	}
	
	g_object_ref (c);
	return c;
}

void
gb_clipboard_clear (GbClipboard *cb)
{
	GbClipboardPrivate *p = cb->priv;
	gtk_clipboard_clear (p->clipboard);
}

void 
gb_clipboard_add (GbClipboard *cb, GbBookmark *b)
{
	GbClipboardPrivate *p = cb->priv;
	GbBookmark *c;
       
	g_return_if_fail (GB_IS_BOOKMARK (b));

	if (!p->contents)
	{
		p->contents  = gb_bookmark_set_new ();
		gb_bookmark_set_set_root (p->contents, gb_folder_new (p->contents, 
								      "Clipboard root folder"));
		gtk_clipboard_set_with_owner (p->clipboard, targets, ntargets, 
					      gb_clipboard_get_cb, gb_clipboard_clear_cb, 
					      G_OBJECT (cb));
	}

	c = gb_bookmark_copy (b);
	gb_folder_add_child (p->contents->root, c, -1);
	g_object_unref (c);

}

static void
gb_clipboard_get_cb (GtkClipboard *clipboard,
		     GtkSelectionData *selection_data,
		     guint info,
		     gpointer user_data_or_owner)
{
	GbClipboard *cb = user_data_or_owner;
	GbClipboardPrivate *p = cb->priv;
	LOG ("Selection requested");
	if (p->contents)
	{
		GbIO *io = GB_IO (gb_xbel_new ());
		gchar *data = NULL;
		if (gb_io_save_to_string (io, p->contents, &data))
		{
			g_return_if_fail (data);
			gtk_selection_data_set (selection_data, gdk_atom_intern (XBEL_TARGET_ID , FALSE),
						8, (guchar*)data, strlen (data));
			g_free (data);
		}
		g_object_unref (io);
	}
}

static void
gb_clipboard_clear_cb (GtkClipboard *clipboard,
		       gpointer user_data_or_owner)
{
	GbClipboard *cb = user_data_or_owner;
	GbClipboardPrivate *p = cb->priv;
	LOG ("Clipboard cleared");
	if (p->contents)
	{
		g_object_unref (p->contents);
		p->contents = NULL;
	}
}

gboolean
gb_clipboard_can_paste (GbClipboard *cb)
{
	/* FIXME: not sure how to implement this */
	/* for now, just return always TRUE */
	return TRUE;
}

GbBookmarkSet *
gb_clipboard_paste (GbClipboard *cb)
{
	GbClipboardPrivate *p = cb->priv;
	GtkSelectionData *data = gtk_clipboard_wait_for_contents (p->clipboard, 
								  gdk_atom_intern (XBEL_TARGET_ID, FALSE));
	if (data)
	{
		GbIO *io = GB_IO (gb_xbel_new ());
		GbBookmarkSet *ret = gb_io_load_from_string (io, (char*)data->data);
		gtk_selection_data_free (data);
		g_object_unref (io);
		return ret;
	}
	return NULL;
}


