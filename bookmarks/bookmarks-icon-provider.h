/* 
 *  Copyright (C) 2002  Ricardo Fernándezs Pascual <ric@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GB_ICON_PROVIDER_H__
#define __GB_ICON_PROVIDER_H__

#include <glib-object.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include "bookmarks.h"

G_BEGIN_DECLS

/* object forward declarations */

typedef struct _GbIconProviderClass GbIconProviderClass;
typedef struct _GbIconProviderPrivate GbIconProviderPrivate;

/**
 * Editor object
 */

#define GB_TYPE_ICON_PROVIDER		      	(gb_icon_provider_get_type())
#define GB_ICON_PROVIDER(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), \
							 GB_TYPE_ICON_PROVIDER,\
							 GbIconProvider))
#define GB_ICON_PROVIDER_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), \
							 GB_TYPE_ICON_PROVIDER,\
							 GbIconProviderClass))
#define GB_IS_ICON_PROVIDER(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), \
							 GB_TYPE_ICON_PROVIDER))
#define GB_IS_ICON_PROVIDER_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), \
							 GB_TYPE_ICON_PROVIDER))
#define GB_ICON_PROVIDER_GET_CLASS(obj)		(G_TYPE_INSTANCE_GET_CLASS((obj), \
							 GB_TYPE_ICON_PROVIDER,\
							 GbIconProviderClass))

struct _GbIconProviderClass 
{
	GObjectClass parent_class;

	/* Virtual Table */
	GdkPixbuf *	(* get_icon)	(GbIconProvider *ip, GbBookmark *b);
};

/* Remember: fields are public read-only */
struct _GbIconProvider
{
	GObject parent_object;
	
	GbIconProviderPrivate *priv;
};

GType		gb_icon_provider_get_type	 (void);
GdkPixbuf *	gb_icon_provider_make_alias_icon (GbIconProvider *provider, GdkPixbuf *icon);
GdkPixbuf *     gb_icon_provider_get_icon      	 (GbIconProvider *ip, GbBookmark *b);
GbIconProvider*	gb_icon_provider_new		 (void);

G_END_DECLS

#endif

