/* 
 *  Copyright (C) 2002  Ricardo Fernándezs Pascual <ric@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GB_LOCATION_SOURCE_H__
#define __GB_LOCATION_SOURCE_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define GB_TYPE_LOCATION_SOURCE			(gb_location_source_get_type ())
#define GB_LOCATION_SOURCE(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), \
						 GB_TYPE_LOCATION_SOURCE, \
						 GbLocationSource))
#define GB_IS_LOCATION_SOURCE(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
						 GB_TYPE_LOCATION_SOURCE))
#define GB_LOCATION_SOURCE_GET_IFACE(obj) 	(G_TYPE_INSTANCE_GET_INTERFACE ((obj), \
						 GB_TYPE_LOCATION_SOURCE, \
						 GbLocationSourceIface))

/**
 * This interface marks objects that can give a location. This is used
 * for adding bookmarks (Add bookmark here in context menus, for example)
 */

/* TODO: (idea) Allow returning a list of locations, instead of a
 * single location. That would allow to implement "Add bookmarks for
 * this window" (creates a folder with a bookmark for each tab), "Add
 * bookmark for referenced pages" (creates folder with a bookmark for
 * each link in a page), etc
 */

typedef struct _GbLocationSource	GbLocationSource;
typedef struct _GbLocationSourceIface	GbLocationSourceIface;

struct _GbLocationSourceIface
{
	GTypeInterface g_iface;
	
	/* Virtual Table */
	gchar *		(* get_location)	(GbLocationSource *source);
	gchar *		(* get_title)		(GbLocationSource *source);
};

GType	gb_location_source_get_type		(void);
gchar *	gb_location_source_get_location		(GbLocationSource *source);
gchar *	gb_location_source_get_title		(GbLocationSource *source);

G_END_DECLS

#endif

