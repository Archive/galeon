/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __bookmarks_import_druid_h
#define __bookmarks_import_druid_h

#include <glib-object.h>
#include <bookmarks.h>

/* object forward declarations */

typedef struct _GbImportDruid GbImportDruid;
typedef struct _GbImportDruidClass GbImportDruidClass;
typedef struct _GbImportDruidPrivate GbImportDruidPrivate;

/**
 * Druid object
 */

#define GB_TYPE_IMPORT_DRUID			(gb_import_druid_get_type())
#define GB_IMPORT_DRUID(object)			(G_TYPE_CHECK_INSTANCE_CAST((object), GB_TYPE_IMPORT_DRUID,\
						 GbImportDruid))
#define GB_IMPORT_DRUID_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), GB_TYPE_IMPORT_DRUID,\
						 GbImportDruidClass))
#define GB_IS_IMPORT_DRUID(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), GB_TYPE_IMPORT_DRUID))
#define GB_IS_IMPORT_DRUID_CLASS(klass)		(G_TYPE_CHECK_CLASS_TYPE((klass), GB_TYPE_IMPORT_DRUID))
#define GB_IMPORT_DRUID_GET_CLASS(obj)		(G_TYPE_INSTANCE_GET_CLASS((obj), GB_TYPE_IMPORT_DRUID,\
						 GbImportDruidClass))

struct _GbImportDruidClass 
{
	GObjectClass parent_class;
	
	/* virtual and protected */
	const gchar *	(*get_string)		(GbImportDruid *d, const char *string_id);
	GSList *	(*get_locations)	(GbImportDruid *d);
	GbIO *		(*get_io)		(GbImportDruid *d);
};

/* Remember: fields are public read-only */
struct _GbImportDruid
{
	GObject parent_object;

	GbImportDruidPrivate *priv;
};

/* this is an abstract class */
GType			gb_import_druid_get_type		(void);
void			gb_import_druid_set_dest_bookmark_set	(GbImportDruid *druid, GbBookmarkSet *set);
void			gb_import_druid_show			(GbImportDruid *druid);

#endif
