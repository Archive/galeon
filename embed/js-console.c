/*
 *  Copyright (C) 2004  Tommi Komulainen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "js-console.h"
#include "gul-state.h"
#include "gul-gui.h"

#include <glib/gi18n.h>
#include <gtk/gtkbutton.h>
#include <gtk/gtkcellrendererpixbuf.h>
#include <gtk/gtkcellrenderertext.h>
#include <gtk/gtkentry.h>
#include <gtk/gtkimage.h>
#include <gtk/gtkalignment.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtkvbox.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkliststore.h>
#include <gtk/gtkscrolledwindow.h>
#include <gtk/gtkstock.h>
#include <gtk/gtktreeview.h>

G_DEFINE_TYPE(GaleonJSConsole, galeon_js_console, GTK_TYPE_DIALOG)

enum {
	EVALUATE,
	LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

#define GALEON_JS_CONSOLE_GET_PRIVATE(object) 			\
	(G_TYPE_INSTANCE_GET_PRIVATE ((object), 		\
         GALEON_TYPE_JS_CONSOLE, GaleonJSConsolePrivate))

typedef struct GaleonJSConsolePrivate GaleonJSConsolePrivate;
struct GaleonJSConsolePrivate {
	GtkWidget    *entry;
	GtkWidget    *view;
	GtkListStore *store;
};

typedef enum {
	COL_MESSAGE_TYPE,
	COL_MESSAGE_TEXT
} ModelColumn;

#define CONSOLE_RESPONSE_CLEAR 1

static void
icon_cell_data_func (GtkTreeViewColumn *column,
		     GtkCellRenderer   *renderer,
		     GtkTreeModel      *model,
		     GtkTreeIter       *iter,
		     gpointer           user_data)
{
	GaleonJSConsoleMessageType type;
	const char *stock_id;

	gtk_tree_model_get (model, iter, COL_MESSAGE_TYPE, &type, -1);
	switch (type)
	{
	case GALEON_JS_CONSOLE_WARNING:
		stock_id = GTK_STOCK_DIALOG_WARNING;
		break;
	case GALEON_JS_CONSOLE_ERROR:
		stock_id = GTK_STOCK_DIALOG_ERROR;
		break;
	case GALEON_JS_CONSOLE_MESSAGE:
	default:
		stock_id = GTK_STOCK_DIALOG_INFO;
		break;
	}

	g_object_set (renderer, "stock-id", stock_id, NULL);
}

GtkWidget *
galeon_js_console_new (void)
{
	return GTK_WIDGET(g_object_new (GALEON_TYPE_JS_CONSOLE, NULL));
}

static void
galeon_js_console_append (GaleonJSConsole           *self, 
			  GaleonJSConsoleMessageType type,
			  const gchar               *text)
{
	GaleonJSConsolePrivate *priv;
	GtkTreeIter             iter;

	g_return_if_fail (GALEON_IS_JS_CONSOLE(self));

	priv = GALEON_JS_CONSOLE_GET_PRIVATE(self);

	gtk_list_store_append (priv->store, &iter);
	gtk_list_store_set (priv->store, &iter,
			    COL_MESSAGE_TYPE, type,
			    COL_MESSAGE_TEXT, text,
			    -1);

	/* scroll only if the user isn't doing anything at the moment */
	if (!gtk_widget_has_focus (priv->view))
	{
		GtkTreePath *path;

		path = gtk_tree_model_get_path (GTK_TREE_MODEL(priv->store), &iter);
		gtk_tree_view_scroll_to_cell (GTK_TREE_VIEW(priv->view),
					      path, NULL, FALSE, 0.0, 0.0);
		gtk_tree_view_set_cursor (GTK_TREE_VIEW(priv->view), path, NULL, FALSE);
		gtk_tree_path_free (path);
	}
}

void
galeon_js_console_error (GaleonJSConsole           *self, 
			 const char                *message, 
			 GaleonJSConsoleMessageType type, 
			 const char                *sourcename,
			 guint                      linenumber,
			 const char                *sourceline,
			 guint                      column)
{
	GString *str;
	char *markup;

	g_return_if_fail (GALEON_IS_JS_CONSOLE(self));
	
	str = g_string_new (NULL);

	markup = g_markup_printf_escaped ("<b>%s</b>", message);
	g_string_append (str, markup);
	g_free (markup);

	if (sourceline[0] != '\0')
	{
		char *stripped;
		char *quoted;

		stripped = g_strstrip (g_strdup (sourceline));
		quoted = g_strdup_printf (_("\"%s\""), stripped);

		markup = g_markup_printf_escaped ("<tt>%s</tt>", quoted);
		g_string_append_c (str, '\n');
		g_string_append (str, markup);
		g_free (markup);

		g_free (stripped);
		g_free (quoted);
	}

	if (sourcename[0] != '\0')
	{
		char *dirname;
		char *basename;

		dirname = g_path_get_dirname (sourcename);
		basename = g_path_get_basename (sourcename);

		markup = g_markup_printf_escaped (_("Line %d, Column %d, File \"%s\" in \"%s\""), linenumber, column, basename, dirname);
		g_string_append_c (str, '\n');
		g_string_append (str, markup);
		g_free (markup);

		g_free (dirname);
		g_free (basename);
	}

	galeon_js_console_append (self, type, str->str);

	g_string_free (str, TRUE);
}

void
galeon_js_console_message (GaleonJSConsole           *self, 
			   const gchar               *text)
{
	gchar * str;

	g_return_if_fail (GALEON_IS_JS_CONSOLE(self));

	str = g_markup_escape_text (text, -1);

	galeon_js_console_append (self, GALEON_JS_CONSOLE_MESSAGE, str);

	g_free (str);
}

static gboolean
galeon_js_console_delete_event (GtkWidget *widget, GdkEventAny *event)
{
	return TRUE;
}

static void
galeon_js_console_response (GtkDialog *dialog, gint response_id)
{
	GaleonJSConsolePrivate *priv = GALEON_JS_CONSOLE_GET_PRIVATE(dialog);

	switch (response_id)
	{
	case CONSOLE_RESPONSE_CLEAR:
		gtk_list_store_clear (priv->store);
		break;
	case GTK_RESPONSE_CLOSE:
	case GTK_RESPONSE_DELETE_EVENT:
		gtk_widget_hide (GTK_WIDGET(dialog));
		break;
	}
}

static void
galeon_js_console_class_init (GaleonJSConsoleClass *klass)
{
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS(klass);
	GtkDialogClass *dialog_class = GTK_DIALOG_CLASS(klass);

	widget_class->delete_event = galeon_js_console_delete_event;

	dialog_class->response = galeon_js_console_response;

	signals[EVALUATE] =
		g_signal_new ("evaluate",
			      G_OBJECT_CLASS_TYPE (klass),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (GaleonJSConsoleClass, evaluate),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__STRING,
			      G_TYPE_NONE, 1,
			      G_TYPE_STRING);

	g_type_class_add_private (klass, sizeof (GaleonJSConsolePrivate));
}

static void
evaluate_clicked_cb (GtkWidget *button, GaleonJSConsole *self)
{
	GaleonJSConsolePrivate *priv = GALEON_JS_CONSOLE_GET_PRIVATE(self);
	const char *command;

	command = gtk_entry_get_text (GTK_ENTRY(priv->entry));
	if (command[0] != '\0')
	{
		g_signal_emit (self, signals[EVALUATE], 0, command);
	}
}

static void
galeon_js_console_init (GaleonJSConsole *self)
{
	GaleonJSConsolePrivate *priv = GALEON_JS_CONSOLE_GET_PRIVATE(self);
	GtkDialog         *dialog = GTK_DIALOG(self);
	GtkWidget         *label;
	GtkWidget         *hbox, *vbox;
	GtkWidget         *button;
	GtkTreeViewColumn *column;
	GtkCellRenderer   *renderer;
	GtkWidget         *scrwin;

	gtk_dialog_set_has_separator (dialog, FALSE);
	gtk_window_set_title (GTK_WINDOW(self), _("JavaScript Console"));
	gtk_container_set_border_width (GTK_CONTAINER(dialog), 6);

	/* JavaScript command to evaluate */
	label = gtk_label_new_with_mnemonic (_("Co_mmand:"));

	priv->entry = gtk_entry_new ();
	gtk_entry_set_activates_default (GTK_ENTRY(priv->entry), TRUE);

	button = gul_gui_image_button_new (_("E_valuate"), GTK_STOCK_EXECUTE);
	GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
	gtk_window_set_default (GTK_WINDOW(self), button);
	g_signal_connect (button, "clicked", G_CALLBACK(evaluate_clicked_cb), self);

	gtk_label_set_mnemonic_widget (GTK_LABEL(label), priv->entry);

	hbox = gtk_hbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX(hbox), priv->entry, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX(hbox), button, FALSE, TRUE, 0);

	/* treeview */
	priv->store = gtk_list_store_new (2, G_TYPE_INT, G_TYPE_STRING);
	priv->view = gtk_tree_view_new_with_model (GTK_TREE_MODEL(priv->store));
	gtk_tree_view_set_rules_hint (GTK_TREE_VIEW(priv->view), TRUE);
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW(priv->view), FALSE);

	column = gtk_tree_view_column_new ();

	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer,
		      "yalign",     0.0,
		      "xpad",       3, 
		      "ypad",       3, 
		      "stock-size", GTK_ICON_SIZE_LARGE_TOOLBAR,
		      NULL);
	gtk_tree_view_column_pack_start (column, renderer, FALSE);
	gtk_tree_view_column_set_cell_data_func (column, renderer, icon_cell_data_func, NULL, NULL);

	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "yalign", 0.0, "editable", TRUE, NULL);
	gtk_tree_view_column_pack_start (column, renderer, TRUE);
	gtk_tree_view_column_set_attributes (column, renderer, "markup", COL_MESSAGE_TEXT, NULL);

	gtk_tree_view_append_column (GTK_TREE_VIEW(priv->view), column);

	scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW(scrwin), 
					GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW(scrwin),
					     GTK_SHADOW_IN);

	gtk_container_add (GTK_CONTAINER(scrwin), priv->view);

	vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER(vbox), 6);
	gtk_box_pack_start (GTK_BOX(vbox), hbox, FALSE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX(vbox), scrwin, TRUE, TRUE, 0);

	gtk_box_set_spacing (GTK_BOX(dialog->vbox), 6);
	gtk_box_pack_start (GTK_BOX(dialog->vbox), vbox, TRUE, TRUE, 0);
	gtk_widget_show_all (dialog->vbox);

	g_object_unref (priv->store);

	gtk_box_set_spacing (GTK_BOX(dialog->action_area), 6);

	/* Avoid mnemonic conflict with _Close */
	button = gul_gui_image_button_new (_("Cl_ear"), GTK_STOCK_CLEAR);
	gtk_widget_show (button);
	GTK_WIDGET_UNSET_FLAGS (button, GTK_CAN_DEFAULT);
	gtk_dialog_add_action_widget (dialog, button, CONSOLE_RESPONSE_CLEAR);

	button = gtk_dialog_add_button (dialog, GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE);
	GTK_WIDGET_UNSET_FLAGS (button, GTK_CAN_DEFAULT);

	gul_state_monitor_window (GTK_WIDGET(self), "Javascript Console", 550, 350);
}
