/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "galeon-window-bookmark-activated-proxy.h"
#include "bookmarks-util.h"

/**
 * Private functions, only availble from this file
 */
static void
galeon_window_bookmark_activated_proxy_finalize_impl	(GObject *o);

static void
galeon_window_bookmark_activated_proxy_bookmark_activated_cb (GObject *sender,
							      GbBookmarkEventActivated *ev,
							      GaleonWindowBookmarkActivatedProxy *wbap);

/* this function is defined in galeon-window.c */
void
galeon_window_bookmark_activate (GaleonWindow *w, GbBookmarkEventActivated *ev);



/**
 * WindowBookmarkActivatedProxy object
 */

G_DEFINE_TYPE (GaleonWindowBookmarkActivatedProxy, galeon_window_bookmark_activated_proxy, G_TYPE_OBJECT);

static void
galeon_window_bookmark_activated_proxy_class_init (GaleonWindowBookmarkActivatedProxyClass *klass)
{
	G_OBJECT_CLASS (klass)->finalize = galeon_window_bookmark_activated_proxy_finalize_impl;
}

static void 
galeon_window_bookmark_activated_proxy_init (GaleonWindowBookmarkActivatedProxy *wbap)
{
	wbap->window = NULL;
}

static void
galeon_window_bookmark_activated_proxy_finalize_impl (GObject *o)
{
	GaleonWindowBookmarkActivatedProxy *wbap = GALEON_WINDOW_BOOKMARK_ACTIVATED_PROXY (o);

	galeon_window_bookmark_activated_proxy_set_window (wbap, NULL);

	G_OBJECT_CLASS (galeon_window_bookmark_activated_proxy_parent_class)->finalize (o);

}

GaleonWindowBookmarkActivatedProxy *
galeon_window_bookmark_activated_proxy_new (void)
{
	GaleonWindowBookmarkActivatedProxy *ret = g_object_new
		(GALEON_TYPE_WINDOW_BOOKMARK_ACTIVATED_PROXY, NULL);
	return ret;
}

void
galeon_window_bookmark_activated_proxy_set_window (GaleonWindowBookmarkActivatedProxy *wbap, 
						   GaleonWindow *w)
{
	if (wbap->window)
	{
		g_object_remove_weak_pointer (G_OBJECT (wbap->window),
					      (gpointer *) &wbap->window);
	}

	wbap->window = w;

	if (wbap->window)
	{
		g_object_add_weak_pointer (G_OBJECT (wbap->window), 
					   (gpointer *) &wbap->window);
	}
}

void
galeon_window_bookmark_activated_proxy_connect (GaleonWindowBookmarkActivatedProxy *wbap, 
						GObject *o)
{
	g_object_weak_ref (o, (GWeakNotify) g_object_unref, wbap);

	g_signal_connect (o, "bookmark-activated", 
			  G_CALLBACK (galeon_window_bookmark_activated_proxy_bookmark_activated_cb),
			  wbap);
}

static void
galeon_window_bookmark_activated_proxy_bookmark_activated_cb (GObject *sender,
							      GbBookmarkEventActivated *ev,
							      GaleonWindowBookmarkActivatedProxy *wbap)
{
	if (wbap->window)
	{
		galeon_window_bookmark_activate (wbap->window, ev);
	}
	else
	{
		/* create a new window */
		GaleonWindow *w = galeon_window_new ();
		galeon_window_bookmark_activated_proxy_set_window (wbap, w);

		galeon_window_bookmark_activate (wbap->window, ev);
	}
}

