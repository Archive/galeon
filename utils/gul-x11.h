/*
 *  Copyright (C) 2004 Crispin Flowerday
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GUL_X11_H
#define GUL_X11_H

#include <gtk/gtkwindow.h>
#include <gtk/gtkwidget.h>
#include <gdk/gdkscreen.h>

G_BEGIN_DECLS

enum { GUL_X11_ALL_WORKSPACES = 0xffffffff };

guint gul_x11_get_current_workspace (GdkScreen* screen);

guint gul_x11_get_window_workspace (GtkWindow* window);


void gul_x11_window_update_user_time (GtkWidget *window,
				      guint32 user_time);

guint32 gul_x11_get_startup_id (void);

G_END_DECLS

#endif
