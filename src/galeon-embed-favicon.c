/*
 *  Copyright (C) 2002 Jorn Baayen <jorn@nl.linux.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include "galeon-embed-favicon.h"
#include "galeon-shell.h"

static void galeon_embed_favicon_class_init (GaleonEmbedFaviconClass *klass);
static void galeon_embed_favicon_init (GaleonEmbedFavicon *ma);
static void galeon_embed_favicon_finalize (GObject *object);
static void galeon_embed_favicon_set_property (GObject *object,
					 guint prop_id,
					 const GValue *value,
					 GParamSpec *pspec);
static void galeon_embed_favicon_get_property (GObject *object,
					 guint prop_id,
					 GValue *value,
					 GParamSpec *pspec);
static void update_url (GaleonEmbedFavicon *favicon);
static void location_cb (GaleonEmbed *embed,
		         GaleonEmbedFavicon *favicon);

#define GALEON_EMBED_FAVICON_GET_PRIVATE(object) (G_TYPE_INSTANCE_GET_PRIVATE ((object), \
				       GALEON_TYPE_EMBED_FAVICON, GaleonEmbedFaviconPrivate))


struct GaleonEmbedFaviconPrivate
{
	GaleonEmbed *embed;
};

enum
{
	PROP_0,
	PROP_EMBED
};

static GObjectClass *parent_class = NULL;

GType
galeon_embed_favicon_get_type (void)
{
	static GType galeon_embed_favicon_type = 0;

	if (galeon_embed_favicon_type == 0)
	{
		static const GTypeInfo our_info =
		{
			sizeof (GaleonEmbedFaviconClass),
			NULL,
			NULL,
			(GClassInitFunc) galeon_embed_favicon_class_init,
			NULL,
			NULL,
			sizeof (GaleonEmbedFavicon),
			0,
			(GInstanceInitFunc) galeon_embed_favicon_init
		};

		galeon_embed_favicon_type = g_type_register_static (GALEON_TYPE_FAVICON,
							            "GaleonEmbedFavicon",
							            &our_info, 0);
	}

	return galeon_embed_favicon_type;
}

static void
galeon_embed_favicon_class_init (GaleonEmbedFaviconClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = galeon_embed_favicon_finalize;

	object_class->set_property = galeon_embed_favicon_set_property;
	object_class->get_property = galeon_embed_favicon_get_property;

	g_object_class_install_property (object_class,
					 PROP_EMBED,
					 g_param_spec_object ("embed",
							      "Associated embed",
							      "Associated embed",
							      G_TYPE_OBJECT,
							      G_PARAM_READWRITE));

	g_type_class_add_private (klass, sizeof (GaleonEmbedFaviconPrivate));
}

static void
galeon_embed_favicon_init (GaleonEmbedFavicon *ma)
{
	ma->priv = GALEON_EMBED_FAVICON_GET_PRIVATE (ma);
}

static void
galeon_embed_favicon_finalize (GObject *object)
{
	GaleonEmbedFavicon *ma;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GALEON_IS_EMBED_FAVICON (object));

	ma = GALEON_EMBED_FAVICON (object);

	g_return_if_fail (ma->priv != NULL);


	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
galeon_embed_favicon_set_property (GObject *object,
			           guint prop_id,
			           const GValue *value,
			           GParamSpec *pspec)
{
	GaleonEmbedFavicon *favicon = GALEON_EMBED_FAVICON (object);

	switch (prop_id)
	{
	case PROP_EMBED:
		favicon->priv->embed = g_value_get_object (value);

		if (favicon->priv->embed != NULL)
		{
			g_signal_connect_object (G_OBJECT (favicon->priv->embed),
					         "ge_location",
					         G_CALLBACK (location_cb),
					         favicon,
				        	 0);
			update_url (favicon);
		}

		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void 
galeon_embed_favicon_get_property (GObject *object,
			           guint prop_id,
			           GValue *value,
			           GParamSpec *pspec)
{
	GaleonEmbedFavicon *favicon = GALEON_EMBED_FAVICON (object);

	switch (prop_id)
	{
	case PROP_EMBED:
		g_value_set_object (value, favicon->priv->embed);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

GtkWidget *
galeon_embed_favicon_new (GaleonEmbed *embed)
{
	GaleonEmbedFavicon *favicon;
	GaleonFaviconCache *cache = galeon_shell_get_favicon_cache (galeon_shell);

	g_return_val_if_fail (GALEON_IS_FAVICON_CACHE (cache), NULL);

	favicon = GALEON_EMBED_FAVICON (g_object_new (GALEON_TYPE_EMBED_FAVICON,
						      "cache", cache,
						      "embed", embed,
					 	      NULL));

	g_return_val_if_fail (favicon->priv != NULL, NULL);

	return GTK_WIDGET (favicon);
}

void
galeon_embed_favicon_set_embed (GaleonEmbedFavicon *favicon,
			        GaleonEmbed *embed)
{
	g_return_if_fail (GALEON_IS_EMBED_FAVICON (favicon));

	g_object_set (G_OBJECT (favicon),
		      "embed", embed,
		      NULL);
}

GaleonEmbed *
galeon_embed_favicon_get_embed (GaleonEmbedFavicon *favicon)
{
	GaleonEmbed *embed;

	g_return_val_if_fail (GALEON_IS_EMBED_FAVICON (favicon), NULL);

	g_object_get (G_OBJECT (favicon),
		      "embed", &embed,
		      NULL);

	return embed;
}

static void
location_cb (GaleonEmbed *embed,
	     GaleonEmbedFavicon *favicon)
{
	update_url (favicon);
}

static void
update_url (GaleonEmbedFavicon *favicon)
{
	char *url = NULL;

	url = galeon_embed_get_location (favicon->priv->embed, TRUE, FALSE);
	if( !url ) return;

	galeon_favicon_set_url (GALEON_FAVICON (favicon), url);
	g_free (url);
}
