/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/* eel-ellipsizing-label.h: Subclass of GtkLabel that ellipsizes the text.

   Copyright (C) 2001 Eazel, Inc.

   The Gnome Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The Gnome Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Gnome Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: John Sullivan <sullivan@eazel.com>,
 */

#ifndef GUL_ELLIPSIZING_LABEL_H
#define GUL_ELLIPSIZING_LABEL_H

#include <gtk/gtklabel.h>

#define GUL_TYPE_ELLIPSIZING_LABEL            (gul_ellipsizing_label_get_type ())
#define GUL_ELLIPSIZING_LABEL(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GUL_TYPE_ELLIPSIZING_LABEL, GulEllipsizingLabel))
#define GUL_ELLIPSIZING_LABEL_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GUL_TYPE_ELLIPSIZING_LABEL, GulEllipsizingLabelClass))
#define GUL_IS_ELLIPSIZING_LABEL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GUL_TYPE_ELLIPSIZING_LABEL))
#define GUL_IS_ELLIPSIZING_LABEL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GUL_TYPE_ELLIPSIZING_LABEL))

typedef struct GulEllipsizingLabel	      GulEllipsizingLabel;
typedef struct GulEllipsizingLabelClass	      GulEllipsizingLabelClass;
typedef struct GulEllipsizingLabelDetails     GulEllipsizingLabelDetails;

struct GulEllipsizingLabel {
	GtkLabel parent;
	GulEllipsizingLabelDetails *details;
};

struct GulEllipsizingLabelClass {
	GtkLabelClass parent_class;
};

GType      gul_ellipsizing_label_get_type (void);
GtkWidget *gul_ellipsizing_label_new      (const char          *string);
void       gul_ellipsizing_label_set_text (GulEllipsizingLabel *label,
					   const char          *string);

#endif /* GUL_ELLIPSIZING_LABEL_H */
