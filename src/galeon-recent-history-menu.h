/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __galeon_recent_history_menu_
#define __galeon_recent_history_menu_

#include <glib-object.h>
#include "galeon-window.h"
#include "window-recent-history.h"

/* object forward declarations */

typedef struct _GaleonRecentHistoryMenu GaleonRecentHistoryMenu;
typedef struct _GaleonRecentHistoryMenuClass GaleonRecentHistoryMenuClass;
typedef struct _GaleonRecentHistoryMenuPrivate GaleonRecentHistoryMenuPrivate;

/**
 * GaleonRecentHistoryMenu object
 */

#define GALEON_TYPE_RECENT_HISTORY_MENU		(galeon_recent_history_menu_get_type())
#define GALEON_RECENT_HISTORY_MENU(object)	(G_TYPE_CHECK_INSTANCE_CAST((object), GALEON_TYPE_RECENT_HISTORY_MENU,\
						 GaleonRecentHistoryMenu))
#define GALEON_RECENT_HISTORY_MENU_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), GALEON_TYPE_RECENT_HISTORY_MENU,\
						 GaleonRecentHistoryMenuClass))
#define GALEON_IS_RECENT_HISTORY_MENU(object)	(G_TYPE_CHECK_INSTANCE_TYPE((object), GALEON_TYPE_RECENT_HISTORY_MENU))
#define GALEON_IS_RECENT_HISTORY_MENU_ITEM_CLASS(klass) \
	 					(G_TYPE_CHECK_CLASS_TYPE((klass), GALEON_TYPE_RECENT_HISTORY_MENU))
#define GALEON_RECENT_HISTORY_MENU_GET_CLASS(obj) \
						(G_TYPE_INSTANCE_GET_CLASS((obj), GALEON_TYPE_RECENT_HISTORY_MENU,\
						 GaleonRecentHistoryMenuClass))

struct _GaleonRecentHistoryMenuClass
{
	GObjectClass parent_class;
};

/* Remember: fields are public read-only */
struct _GaleonRecentHistoryMenu
{
	GObject parent_object;
	
	GaleonRecentHistoryMenuPrivate *priv;
};

GType				galeon_recent_history_menu_get_type	(void);
GaleonRecentHistoryMenu *	galeon_recent_history_menu_new		(GaleonWindow *window,
									 GaleonWindowRecentHistory *history);


#endif
