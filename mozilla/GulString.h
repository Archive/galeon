/*
 *  Copyright (C) 2004 Crispin Flowerday
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* This provides string classes that have a more feature rich API
 * than the base mozilla nsEmbedString ones
 *
 * The GulString object contains UTF16 strings, and the GulCString
 * object contains UTF8 strings, you can assign one to the other to
 * get conversion.
 */

#ifndef MOZILLA_GUL_STRING_H
#define MOZILLA_GUL_STRING_H

#define MOZILLA_STRICT_API
#include <nsEmbedString.h>
#undef MOZILLA_STRICT_API
#include <glib/gstrfuncs.h>
#include <string.h>

class GulString;
class GulCString;
class GulDependentString;
class GulDependentCString;

class GulDependentCString : public nsEmbedCString
{
 public:
	GulDependentCString (const char * aString)
	{
		// Copy, although it is possible in trunk to do it
		// without the copy
		// https://bugzilla.mozilla.org/show_bug.cgi?id=264274
		Assign (aString);
	}
};

#ifndef NS_LITERAL_CSTRING
#define NS_LITERAL_CSTRING(a) GulDependentCString(a)
#endif


class GulDependentString : public nsEmbedString
{
 public:
	GulDependentString (const PRUnichar * aString)
	{
		// Copy, although it is possible in trunk to do it
		// without the copy
		// https://bugzilla.mozilla.org/show_bug.cgi?id=264274
		Assign (aString);
	}
};

class GulString : public nsEmbedString
{
 public:
	GulString() {}

	GulString (const PRUnichar * aString) 
	{
		Assign (aString);
	}

	GulString (const char * aString) // conversion
	{
		NS_CStringToUTF16 (GulDependentCString (aString),
				   NS_CSTRING_ENCODING_UTF8, *this);
	}
		
	GulString (const nsACString &aString) // conversion
	{
		NS_CStringToUTF16 (aString, NS_CSTRING_ENCODING_UTF8, *this);
	}

	PRBool IsEmpty() const
	{
		return (Length() == 0 ) ? PR_TRUE : PR_FALSE;
	}

	void ReplaceChar (PRUnichar what, PRUnichar with)
	{
		const PRUnichar *text = BeginReading();
		for (PRUint32 i = 0; i < Length(); i++)
		{
			if (text[i] == what)
			{
				Replace (i, 1, with);
			}
		}
	}

	PRBool Equals (const nsAString& aString) const
	{
		const PRUnichar *p1, *p2;
		const PRUint32 l1 = NS_StringGetData (*this, &p1);
		const PRUint32 l2 = NS_StringGetData (aString, &p2);

		if (l1 != l2 )
		{
			return PR_FALSE;
		}

		return memcmp (p1, p2, l1 * sizeof (PRUnichar)) == 0 ? PR_TRUE : PR_FALSE;
	}
	

	GulString &operator=(const PRUnichar * aString)
	{
		Assign (aString);
		return *this;
	}

	GulString &operator=(const nsAString &aString)
	{
		Assign (aString);
		return *this;
	}

	GulString &operator=(const nsACString& aString) // conversion
	{
		NS_CStringToUTF16 (aString, NS_CSTRING_ENCODING_UTF8, *this);
		return *this;
	}

};

#ifndef NS_LITERAL_STRING
#define NS_LITERAL_STRING(s)         GulString(s)
#endif

#ifndef NS_NAMED_LITERAL_STRING
#define NS_NAMED_LITERAL_STRING(v,s) GulString v(s);
#endif

class GulCString : public nsEmbedCString
{
 public:
	GulCString () {}

	GulCString (const char * aString)
	{
		Assign (aString);
	}

	GulCString (const nsACString & aString)
	{
		Assign (aString);
	}

	GulCString (const PRUnichar * aString) // conversion
	{
		NS_UTF16ToCString (GulDependentString (aString),
				   NS_CSTRING_ENCODING_UTF8, *this);
	}

	GulCString (const nsAString &aString) // conversion
	{
		NS_UTF16ToCString (aString, NS_CSTRING_ENCODING_UTF8, *this);
	}

	PRBool IsEmpty() const
	{
		return (Length() == 0 ) ? PR_TRUE : PR_FALSE;
	}

	PRBool Equals (const char * aString) const
	{
		PRUint32 l = Length();
		if (strlen (aString) != l)
		{
			return PR_FALSE;
		}

		return memcmp (get(), aString, l) == 0 ? PR_TRUE : PR_FALSE;
	}
	
	PRBool Equals (const nsACString& aString) const
	{
		const char *p1, *p2;
		const PRUint32 l1 = NS_CStringGetData (*this, &p1);
		const PRUint32 l2 = NS_CStringGetData (aString, &p2);

		if (l1 != l2 )
		{
			return PR_FALSE;
		}

		return memcmp (p1, p2, l1) == 0 ? PR_TRUE : PR_FALSE;
	}
	
	PRBool EqualsAsciiIgnoreCase (const char * aString) const
	{
		return g_ascii_strcasecmp (get(), aString) == 0 ? PR_TRUE : PR_FALSE;
	}
	
	PRBool StartsWith (const char * aString) const
	{
		const unsigned int len = strlen (aString);
		if (len > Length())
		{
			return PR_FALSE;
		}

		return memcmp (get(), aString, len) == 0 ? PR_TRUE : PR_FALSE;
	}

	GulCString &operator=(const char * aString)
	{
		Assign (aString);
		return *this;
	}

	GulCString &operator=(const nsAString& aString) // conversion
	{
		NS_UTF16ToCString (aString, NS_CSTRING_ENCODING_UTF8, *this);
		return *this;
	}

	GulCString &operator=(const PRUnichar * aString) // conversion
	{
		NS_UTF16ToCString (GulDependentString (aString), 
				   NS_CSTRING_ENCODING_UTF8, *this);
		return *this;
	}
};

#endif
