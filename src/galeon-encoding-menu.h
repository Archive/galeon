/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __galeon_altenate_encoding_menu_
#define __galeon_altenate_encoding_menu_

#include <glib-object.h>
#include "galeon-window.h"

/* object forward declarations */

typedef struct _GaleonEncodingMenu GaleonEncodingMenu;
typedef struct _GaleonEncodingMenuClass GaleonEncodingMenuClass;
typedef struct _GaleonEncodingMenuPrivate GaleonEncodingMenuPrivate;

/**
 * GaleonEncodingMenu object
 */

#define GALEON_TYPE_ENCODING_MENU		(galeon_encoding_menu_get_type())
#define GALEON_ENCODING_MENU(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), GALEON_TYPE_ENCODING_MENU,\
						 GaleonEncodingMenu))
#define GALEON_ENCODING_MENU_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), GALEON_TYPE_ENCODING_MENU,\
						 GaleonEncodingMenuClass))
#define GALEON_IS_ENCODING_MENU(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), GALEON_TYPE_ENCODING_MENU))
#define GALEON_IS_ENCODING_MENU_ITEM_CLASS(klass) \
 						(G_TYPE_CHECK_CLASS_TYPE((klass), GALEON_TYPE_ENCODING_MENU))
#define GALEON_ENCODING_MENU_GET_CLASS(obj) 	(G_TYPE_INSTANCE_GET_CLASS((obj), GALEON_TYPE_ENCODING_MENU,\
						 GaleonEncodingMenuClass))

struct _GaleonEncodingMenuClass
{
	GObjectClass parent_class;
};

/* Remember: fields are public read-only */
struct _GaleonEncodingMenu
{
	GObject parent_object;
	
	GaleonEncodingMenuPrivate *priv;
};

GType		        galeon_encoding_menu_get_type			(void);
GaleonEncodingMenu *	galeon_encoding_menu_new			(GaleonWindow *window);

#endif
