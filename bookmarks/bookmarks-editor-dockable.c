/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n.h>
#include "gul-glade.h"
#include "galeon-marshal.h"
#include "bookmarks-editor-dockable.h"
#include "bookmarks-context-menu.h"
#include "bookmarks-tree-view.h"
#include "gul-string.h"
#include "gul-state.h"
#include "eel-gconf-extensions.h"
#include "prefs-strings.h"
#include "galeon-debug.h"
#include <gtk/gtkcellrenderertext.h>
#include <gtk/gtkentry.h>
#include <gtk/gtkscrolledwindow.h>
#include <gtk/gtktreeselection.h>
#include <gtk/gtkbox.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkvbox.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtktogglebutton.h>


/**
 * Private functions, only availble from this file
 */
static void		gb_editor_dockable_init_widgets_impl	(GbEditor *e);
static void		gb_editor_dockable_finalize_impl	(GObject *o);


/**
 * Editor object
 */

G_DEFINE_TYPE (GbEditorDockable, gb_editor_dockable, GB_TYPE_EDITOR);

static void
gb_editor_dockable_class_init (GbEditorDockableClass *klass)
{
	GB_EDITOR_CLASS (klass)->gb_editor_init_widgets = gb_editor_dockable_init_widgets_impl;
	G_OBJECT_CLASS (klass)->finalize = gb_editor_dockable_finalize_impl;
}

static void 
gb_editor_dockable_init (GbEditorDockable *e)
{
}

static void
gb_editor_dockable_finalize_impl (GObject *o)
{
	LOG ("GbEditorDockable finalized");
	
	G_OBJECT_CLASS (gb_editor_dockable_parent_class)->finalize (o);
}

static void
gb_editor_dockable_init_widgets_impl (GbEditor *e)
{
	GtkWidget *contents;
	GtkWidget *mw;
	GbTreeView *tv;
	GtkWidget *search_hbox;
	GtkWidget *search_text_entry;
	GtkWidget *search_label;

	mw = gtk_vbox_new (FALSE, 0);
	gb_editor_set_main_widget (e, mw);

	search_text_entry = gtk_entry_new ();
	gb_editor_set_search_widgets (e, search_text_entry);
	search_hbox = gtk_hbox_new (FALSE, 6);

	search_label = gtk_label_new_with_mnemonic (_("Fin_d:"));
	gtk_box_pack_start (GTK_BOX (search_hbox), search_label, FALSE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (search_hbox), search_text_entry, TRUE, TRUE, 0);
	gtk_widget_show_all (search_hbox);
	gtk_box_pack_start (GTK_BOX (mw), search_hbox, FALSE, TRUE, 0);
	gtk_label_set_mnemonic_widget (GTK_LABEL (search_label), search_text_entry);

	tv = gb_editor_get_tree_view (e);
	gb_tree_view_set_double_click_action (tv, GB_TV_ACTIVATE);

	contents = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (contents),
					     GTK_SHADOW_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (contents), 
					GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (contents), GTK_WIDGET (tv));
	gtk_widget_show_all (contents);
	
	gtk_box_pack_start (GTK_BOX (mw), contents, TRUE, TRUE, 0);
}

GbEditorDockable *
gb_editor_dockable_new (GbTreeModel *model)
{
	GbEditorDockable *ret = g_object_new (GB_TYPE_EDITOR_DOCKABLE, NULL);
	gb_editor_set_model (GB_EDITOR (ret), model);
	return ret;
}

GbEditorDockable *
gb_editor_dockable_new_for_set (GbBookmarkSet *set)
{
	GbTreeModel *tm;
	GbEditorDockable *e;
	
	g_return_val_if_fail (GB_IS_BOOKMARK_SET (set), NULL);
	g_return_val_if_fail (GB_IS_FOLDER (set->root), NULL);
	
	tm = gb_tree_model_new_for_set (set);
	e = gb_editor_dockable_new (tm);
	g_object_unref (tm);
	
	return e;
}

