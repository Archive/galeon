/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "galeon-config.h"
#include "galeon-embed-utils.h"
#include "bookmarks-add-dialog.h"
#include "eel-gconf-extensions.h"
#include "popup-commands.h"
#include "galeon-shell.h"
#include "galeon-embed-persist.h"

#include <string.h>
#include <glib/gi18n.h>
#include <gtk/gtkmain.h>
#include <gtk/gtkclipboard.h>

static void
popup_copy_to_clipboard (GaleonPopup *popup, const char *text)
{
	gtk_clipboard_set_text (gtk_clipboard_get (GDK_NONE),
				text, -1);
	gtk_clipboard_set_text (gtk_clipboard_get (GDK_SELECTION_PRIMARY),
				text, -1);
}

void popup_cmd_open_link (GtkAction *action, 
			  GaleonPopup *popup)
{
	GaleonEmbedEvent *info;
	GaleonEmbed *embed;
	const GValue *value;

	/* Use the active embed, so that this works for
	 * the sidebar as well */
	embed = galeon_window_get_active_embed (galeon_popup_get_window (popup));

	info = galeon_popup_get_event (popup);

	value = galeon_embed_event_get_property (info, "link");

	galeon_embed_load_url (embed, g_value_get_string (value));
}

void popup_cmd_new_window (GtkAction *action, 
			   GaleonPopup *popup)
{
	GaleonEmbedEvent *info;
	GaleonTab *tab;
	const GValue *value;

	tab = galeon_window_get_active_tab (galeon_popup_get_window (popup));
	
	info = galeon_popup_get_event (popup);
	
	value = galeon_embed_event_get_property (info, "link");
	
	galeon_shell_new_tab (galeon_shell, NULL, tab, 
			      g_value_get_string (value), 
			      GALEON_NEW_TAB_COPY_HISTORY |
			      GALEON_NEW_TAB_IN_NEW_WINDOW);
}

void popup_cmd_new_tab (GtkAction *action, 
			GaleonPopup *popup)
{
	GaleonEmbedEvent *info;
	GaleonTab *tab;
	GaleonWindow *window;
	const GValue *value;

	window = galeon_popup_get_window (popup);
	g_return_if_fail (window != NULL);
	
	tab = galeon_window_get_active_tab (window);
	
	info = galeon_popup_get_event (popup);
	
	value = galeon_embed_event_get_property (info, "link");
	
	galeon_shell_new_tab (galeon_shell, window, tab, 
			      g_value_get_string (value), 
			      GALEON_NEW_TAB_COPY_HISTORY |
			      GALEON_NEW_TAB_IN_EXISTING_WINDOW);
}

void popup_cmd_image_in_new_tab (GtkAction *action, 
				 GaleonPopup *popup)
{
	GaleonEmbedEvent *info;
	GaleonTab *tab;
	GaleonWindow *window;
	const GValue *value;

	window = galeon_popup_get_window (popup);
	g_return_if_fail (window != NULL);
	
	tab = galeon_window_get_active_tab (window);
	
	info = galeon_popup_get_event (popup);
	
	value = galeon_embed_event_get_property (info, "image");
	
	galeon_shell_new_tab (galeon_shell, window, tab, 
			      g_value_get_string (value), 
			      GALEON_NEW_TAB_COPY_HISTORY |
			      GALEON_NEW_TAB_IN_EXISTING_WINDOW);
}

void popup_cmd_image_in_new_window (GtkAction *action, 
				    GaleonPopup *popup)
{
	GaleonEmbedEvent *info;
	GaleonTab *tab;
	const GValue *value;

	tab = galeon_window_get_active_tab (galeon_popup_get_window (popup));
	
	info = galeon_popup_get_event (popup);
	
	value = galeon_embed_event_get_property (info, "image");
	
	galeon_shell_new_tab (galeon_shell, NULL, tab, 
			      g_value_get_string (value), 
			      GALEON_NEW_TAB_COPY_HISTORY |
			      GALEON_NEW_TAB_IN_NEW_WINDOW);
}

void popup_cmd_image_block_site(GtkAction *action, 
				GaleonPopup *popup)
{
	GaleonEmbedEvent *info;
	GaleonEmbedShell *embed_shell;
	const GValue *value;
	PermissionActionType perm_action;

	embed_shell = galeon_shell_get_embed_shell(galeon_shell);
	
	info = galeon_popup_get_event(popup);
	
	value = galeon_embed_event_get_property(info, "image");

	perm_action = galeon_embed_shell_test_permission(embed_shell,
							 g_value_get_string(value),
							 IMAGES_PERMISSION);
	if (perm_action == DENY_ACTION)
	{
		galeon_embed_shell_set_permission(embed_shell,
						  g_value_get_string(value),
						  IMAGES_PERMISSION,
						  TRUE);
	}
	else
	{
		galeon_embed_shell_set_permission(embed_shell,
						  g_value_get_string(value),
						  IMAGES_PERMISSION,
						  FALSE);
	}
}

void popup_cmd_add_bookmark (GtkAction *action, 
			     GaleonPopup *popup)
{
	GaleonEmbedEvent *info = galeon_popup_get_event (popup);
	const GValue *link_title;
	const GValue *link;
	const GValue *link_is_smart;
	const GValue *link_rel;
	const GValue *link_text;
	const char *title;
	const char *location;
	const char *smart_loc;
	gboolean smart_site = FALSE;
	GbBookmarkSet *set;
	GbSite *b;
	GaleonDialog *dialog;
	GSList *list = NULL;

	GaleonWindow *window = galeon_popup_get_window(popup);
	g_return_if_fail (window != NULL);

	link = galeon_embed_event_get_property (info, "link");
	link_title = galeon_embed_event_get_property (info, "link_title");
	link_is_smart = galeon_embed_event_get_property (info, "link_is_smart");
	link_rel = galeon_embed_event_get_property (info, "link_rel");
	link_text = galeon_embed_event_get_property (info, "link_text");
	
	title      = g_value_get_string (link_title);
	location   = g_value_get_string (link);
	smart_loc  = g_value_get_string (link_rel);
	smart_site = g_value_get_int (link_is_smart);

	g_return_if_fail (location);
	
	if (title == NULL || title[0] == '\0')
	{
		title = g_value_get_string (link_text);
	}

	if (title == NULL || title[0] == '\0')
	{
		title = location;
	}

	set = galeon_shell_get_bookmark_set (galeon_shell);
	g_return_if_fail (set);

	if (!smart_site)
	{
		b = gb_site_new (set, title, location);
	}
	else
	{
		b = GB_SITE (gb_smart_site_new (set, title, 
						location, smart_loc));
		/* TODO add image saving support */
	}

	list = g_slist_append(list, b);

	dialog = gb_add_dialog_new(GTK_WIDGET(window), set, list);

	galeon_dialog_show(dialog);
}

void popup_cmd_frame_in_new_tab (GtkAction *action, 
				 GaleonPopup *popup)
{
	GaleonTab *tab;
	GaleonWindow *window;
	GaleonEmbed *embed;
	char *location;
	
	window = galeon_popup_get_window (popup);
	g_return_if_fail (window != NULL);

	tab = galeon_window_get_active_tab (window);

	embed = galeon_window_get_active_embed (window);
	
	location = galeon_embed_get_location (embed, FALSE, FALSE);
	
	galeon_shell_new_tab (galeon_shell, window, tab, 
			      location, 
			      GALEON_NEW_TAB_COPY_HISTORY |
			      GALEON_NEW_TAB_IN_EXISTING_WINDOW);

	g_free (location);
}

void popup_cmd_frame_in_new_window (GtkAction *action, 
				    GaleonPopup *popup)
{
	GaleonTab *tab;
	GaleonEmbed *embed;
	GaleonWindow *window;
	char *location;
	
	window = galeon_popup_get_window (popup);
	g_return_if_fail (window != NULL);
	
	tab = galeon_window_get_active_tab (window);

	embed = galeon_window_get_active_embed (window);
	
	location = galeon_embed_get_location (embed, FALSE, FALSE);
	
	galeon_shell_new_tab (galeon_shell, NULL, tab, 
			      location, 
			      GALEON_NEW_TAB_COPY_HISTORY |
			      GALEON_NEW_TAB_IN_NEW_WINDOW);

	g_free (location);	
}

void popup_cmd_reload_frame (GtkAction *action,
			     GaleonPopup *popup)
{
	GaleonEmbed *embed = galeon_popup_get_embed (popup);

	galeon_embed_reload (embed, EMBED_RELOAD_FRAME);
}


void popup_cmd_view_source (GtkAction *action, 
			    GaleonPopup *popup)
{
	GaleonWindow *window;
	GaleonEmbed *embed;

	window = galeon_popup_get_window (popup);
	embed = galeon_popup_get_embed (popup);

	galeon_shell_new_tab_from_embed
		(galeon_shell, window, embed, NULL,
		 GALEON_NEW_TAB_COPY_HISTORY |
		 GALEON_NEW_TAB_VIEW_SOURCE);
}

void
popup_copy_text_cmd (GtkAction *action, 
		     GaleonPopup *popup)
{
	GaleonEmbed *embed;

	embed = galeon_popup_get_embed (popup);
	g_return_if_fail (GALEON_IS_EMBED (embed));
		
	galeon_embed_selection_copy (embed);
}

void 
popup_copy_location_cmd(GtkAction *action, 
			GaleonPopup *popup)
{
	GaleonEmbed *embed;
	char *location;

	embed = galeon_popup_get_embed (popup);
	g_return_if_fail (GALEON_IS_EMBED (embed));
		
	location = galeon_embed_get_location (embed, FALSE, FALSE);
	popup_copy_to_clipboard (popup, location);
	g_free (location);
}

void 
popup_copy_email_cmd(GtkAction *action, 
			GaleonPopup *popup)
{
	GaleonEmbedEvent *info;
	const char *location;
	const GValue *value;
	
	info = galeon_popup_get_event (popup);
	value = galeon_embed_event_get_property (info, "email");
	location = g_value_get_string (value);
	popup_copy_to_clipboard (popup, location);
}

void 
popup_copy_link_location_cmd(GtkAction *action, 
			GaleonPopup *popup)
{
	GaleonEmbedEvent *info;
	const char *location;
	const GValue *value;
	
	info = galeon_popup_get_event (popup);
	value = galeon_embed_event_get_property (info, "link");
	location = g_value_get_string (value);
	popup_copy_to_clipboard (popup, location);
}

void 
popup_download_link_cmd(GtkAction *action, 
			GaleonPopup *popup)
{
	GaleonEmbedEvent *event = galeon_popup_get_event(popup);
	GaleonEmbed *embed = galeon_popup_get_embed(popup);

	galeon_embed_utils_download_event_property(embed, event,
						   FALSE, NULL,
						   "link");
}

void 
popup_save_image_as_cmd(GtkAction *action, 
			GaleonPopup *popup)
{
	GaleonEmbedEvent *event = galeon_popup_get_event(popup);
	GaleonEmbed *embed = galeon_popup_get_embed(popup);
	
	galeon_embed_utils_save_event_property (embed,event,
						TRUE, /* always_ask_dir */
						FALSE, /* show_progress */
						_("Save Image As..."),
						"image");
}

#define CONF_DESKTOP_BG_PICTURE "/desktop/gnome/background/picture_filename"
#define CONF_DESKTOP_BG_TYPE "/desktop/gnome/background/picture_options"

static void
background_download_completed (GaleonEmbedPersist *persist,
			       gpointer data)
{
	const char *bg;
	char *type;
	
	bg = galeon_embed_persist_get_dest (persist);
	eel_gconf_set_string (CONF_DESKTOP_BG_PICTURE, bg);

	type = eel_gconf_get_string (CONF_DESKTOP_BG_TYPE);
	if (type && strcmp (type, "none") == 0)
	{
		eel_gconf_set_string (CONF_DESKTOP_BG_TYPE,
				      "wallpaper");
	}

	g_free (type);
}

void 
popup_set_image_as_background_cmd(GtkAction *action, 
				  GaleonPopup *popup)
{
	GaleonEmbedEvent *info;
	const char *location;
	char *dest, *base;
	const GValue *value;
	GaleonEmbedPersist *persist;
	GaleonEmbed *embed;
	
	info = galeon_popup_get_event (popup);
	value = galeon_embed_event_get_property (info, "image");
	location = g_value_get_string (value);

	embed = galeon_popup_get_embed (popup);
	persist = galeon_embed_persist_new (embed);
	
	base = g_path_get_basename (location);
	dest = g_build_filename (g_get_home_dir (), 
				 GALEON_DIR, 
				 base, NULL);
	
	galeon_embed_persist_set_source (persist, location);
	galeon_embed_persist_set_dest (persist, dest);

	g_signal_connect (persist, "completed",
			  G_CALLBACK (background_download_completed),
			  NULL);

	galeon_embed_persist_save (persist);
	g_object_unref (persist);
	
	g_free (dest);
	g_free (base);
}

void 
popup_copy_image_location_cmd(GtkAction *action, 
			      GaleonPopup *popup)
{
	GaleonEmbedEvent *info;
	const char *location;
	const GValue *value;
	
	info = galeon_popup_get_event (popup);
	value = galeon_embed_event_get_property (info, "image");
	location = g_value_get_string (value);
	popup_copy_to_clipboard (popup, location);
}

void 
popup_save_page_as_cmd(GtkAction *action, 
		       GaleonPopup *popup)
{
	GaleonWindow *window;
	GaleonEmbedPersist *persist;
	GaleonEmbed *embed;

	window = galeon_popup_get_window (popup);
	embed = galeon_popup_get_embed (popup);
	persist = galeon_embed_persist_new (embed);
	galeon_embed_persist_set_flags (persist, EMBED_PERSIST_MAINDOC
					| EMBED_PERSIST_SAVE_CONTENT
					| EMBED_PERSIST_ASK_DESTINATION
					| EMBED_PERSIST_ADD_TO_RECENT);

	galeon_embed_persist_set_fc_parent (persist, GTK_WIDGET (window));
	galeon_embed_persist_set_fc_title (persist, _("Save Page As..."));
	galeon_embed_persist_set_user_time (persist, gtk_get_current_event_time());
	galeon_embed_persist_save (persist);

	g_object_unref (persist);
}

void 
popup_save_background_as_cmd(GtkAction *action, 
			     GaleonPopup *popup)
{
	GaleonEmbedEvent *event = galeon_popup_get_event(popup);
	GaleonEmbed *embed = galeon_popup_get_embed(popup);
	
	galeon_embed_utils_save_event_property(embed,event,
					       TRUE, /* always_ask_dest */
					       FALSE, /* show_progress */
					       _("Save Background As..."),
					       "background_image");
}

void 
popup_open_frame_cmd (GtkAction *action, 
		      GaleonPopup *popup)
{
	char *location;
	GaleonEmbed *embed;
	embed = galeon_popup_get_embed (popup);

	location = galeon_embed_get_location (embed, FALSE, FALSE);
	galeon_embed_load_url (embed, location);

	g_free (location);
}

void 
popup_open_image_cmd(GtkAction *action, 
			GaleonPopup *popup)
{
	GaleonEmbedEvent *info;
	const char *location;
	const GValue *value;
	GaleonEmbed *embed;

	embed = galeon_popup_get_embed (popup);
	info = galeon_popup_get_event (popup);
	value = galeon_embed_event_get_property (info, "image");
	location = g_value_get_string (value);

	galeon_embed_load_url (embed, location);	
}

