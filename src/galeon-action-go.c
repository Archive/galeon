/*
 *  Copyright (C) 2003 Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "galeon-action-go.h"
#include "galeon-shell.h"
#include "eel-gconf-extensions.h"
#include "prefs-strings.h"
#include "gul-string.h"
#include "gul-gui.h"

#include <gtk/gtkstock.h>
#include <gtk/gtkimagemenuitem.h>
#include <gtk/gtktoolitem.h>
#include <gtk/gtkimage.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtkmain.h>
#include <glib/gi18n.h>
#include <string.h>

static void		galeon_action_go_connect_proxy		(GtkAction *action, GtkWidget *proxy);
static void		galeon_action_go_activate		(GtkAction *action);
static GtkWidget *	galeon_action_go_create_toolitem	(GtkAction *action);



G_DEFINE_TYPE (GaleonActionGo, galeon_action_go, GALEON_TYPE_ACTION);


static void
galeon_action_go_class_init (GaleonActionGoClass *class)
{
	GtkActionClass *action_class = GTK_ACTION_CLASS (class);

	action_class->connect_proxy     = galeon_action_go_connect_proxy;
	action_class->menu_item_type    = GTK_TYPE_IMAGE_MENU_ITEM;
	action_class->create_tool_item  = galeon_action_go_create_toolitem;
	action_class->activate          = galeon_action_go_activate;
}

static void
galeon_action_go_init (GaleonActionGo *action)
{
	g_object_set (G_OBJECT (action), "stock_id", GTK_STOCK_JUMP_TO, NULL);
	g_object_set (G_OBJECT (action), "tooltip", _("Go to the specified location"), NULL);
	g_object_set (G_OBJECT (action), "label", _("_Go"), NULL);
}

static void
toolbar_reconfigured_cb (GtkToolItem *button)
{
	GtkWidget *label, *image;
	GtkToolbarStyle toolbar_style;
	GtkIconSize icon_size;

	g_return_if_fail (GTK_IS_TOOL_ITEM (button));

	label = g_object_get_data (G_OBJECT (button), "go-label");
	image = g_object_get_data (G_OBJECT (button), "go-image");

	toolbar_style = gtk_tool_item_get_toolbar_style (button);
	icon_size = gtk_tool_item_get_icon_size (button);

	g_object_set (G_OBJECT (image), "icon-size", icon_size, NULL);
	
	if (toolbar_style == GTK_TOOLBAR_ICONS)
	{
		gtk_widget_hide (label);
		gtk_widget_show (image);
	}
	else
	{
		gtk_widget_hide (image);
		gtk_widget_show (label);
	}
}

static GtkWidget *
galeon_action_go_create_toolitem (GtkAction *action)
{
	GtkWidget *widget, *image, *label, *button, *hbox;
	GtkIconSize icon_size;

	widget = GTK_WIDGET (gtk_tool_item_new ());

	icon_size = gtk_tool_item_get_icon_size (GTK_TOOL_ITEM (widget));

	image = gtk_image_new_from_stock (GTK_STOCK_JUMP_TO, icon_size);
	label = gtk_label_new (_("Go"));
	button = gtk_button_new ();
	hbox = gtk_hbox_new (FALSE, 0);
	gtk_button_set_relief(GTK_BUTTON (button), GTK_RELIEF_NONE);

	gtk_widget_show (label);
	gtk_widget_show (button);
	gtk_widget_show (hbox);

	gtk_container_add (GTK_CONTAINER (widget), button);
	gtk_container_add (GTK_CONTAINER (button), hbox);
	gtk_container_add (GTK_CONTAINER (hbox), image);
	gtk_container_add (GTK_CONTAINER (hbox), label);

	g_object_set_data (G_OBJECT (widget), "go-image", image);
	g_object_set_data (G_OBJECT (widget), "go-label", label);

	g_signal_connect (widget, "toolbar_reconfigured",
			  G_CALLBACK (toolbar_reconfigured_cb), NULL);

	return widget;
}

static gchar *
galeon_action_go_get_location (GaleonActionGo *b)
{
	GaleonWindow *window;
	char *ret;
	
	window = galeon_action_get_window (GALEON_ACTION (b));
	g_return_val_if_fail (GALEON_IS_WINDOW (window), NULL);
	
	ret = galeon_window_get_location_entry_location (window);

	return ret;
}

static void
galeon_action_go_activate (GtkAction *a)
{
	GaleonEmbed *embed = galeon_action_get_embed (GALEON_ACTION (a));
	char *location;
	
	g_return_if_fail (GALEON_IS_EMBED (embed));

	location = galeon_action_go_get_location (GALEON_ACTION_GO (a));
	g_return_if_fail (location != NULL);
	
	galeon_embed_load_url (embed, location);

	g_free (location);
}


static gboolean
galeon_action_go_button_press_event_cb (GtkWidget *widget, GdkEventButton *event, 
					GaleonActionGo *b)
{
	GaleonWindow *window = galeon_action_get_window (GALEON_ACTION (b));

	if (event->button == 2 && GALEON_IS_WINDOW (window))
	{
		char *location;

		location = galeon_action_go_get_location (b);
		g_return_val_if_fail (location != NULL, FALSE);
	
		if (location)
		{
			GaleonTab *tab = galeon_window_get_active_tab (window);
			galeon_shell_new_tab (galeon_shell, window, tab, location, 
					      GALEON_NEW_TAB_JUMP);
			g_free (location);
		}
		return TRUE;
	}
	
	return FALSE;
}

static void
galeon_action_go_activate_cb (GtkMenuItem *mi, GaleonActionGo *b)
{
	GaleonEmbed *embed = galeon_action_get_embed (GALEON_ACTION (b));
	char *location;
	
	g_return_if_fail (embed != NULL);

	location = galeon_action_go_get_location (b);
	g_return_if_fail (location != NULL);
	
	galeon_embed_load_url (embed, location);

	g_free (location);
}

static void 
galeon_action_go_new_tab_activate_cb (GtkMenuItem *mi, GaleonActionGo *b)
{
	GaleonWindow *window = galeon_action_get_window (GALEON_ACTION (b));
	char *location;

	location = galeon_action_go_get_location (b);
	g_return_if_fail (location != NULL);
	
	if (location)
	{
		GaleonTab *tab = galeon_window_get_active_tab (window);
		galeon_shell_new_tab (galeon_shell, window, tab, location, 
				      GALEON_NEW_TAB_JUMP | GALEON_NEW_TAB_IN_EXISTING_WINDOW);
		g_free (location);
	}
}

static void 
galeon_action_go_new_window_activate_cb (GtkMenuItem *mi, GaleonActionGo *b)
{
	GaleonWindow *window = galeon_action_get_window (GALEON_ACTION (b));
	char *location;

	location = galeon_action_go_get_location (b);
	g_return_if_fail (location != NULL);
	
	if (location)
	{
		GaleonTab *tab = galeon_window_get_active_tab (window);
		galeon_shell_new_tab (galeon_shell, window, tab, location, 
				      GALEON_NEW_TAB_JUMP | GALEON_NEW_TAB_IN_NEW_WINDOW);
		g_free (location);
	}
}

static void 
popup_menu_cb (GtkWidget *button, GtkMenu *menu)
{
	g_return_if_fail (GTK_IS_MENU (menu));

	gtk_menu_popup (menu, NULL, NULL, 
			gul_gui_menu_position_under_widget, button, 0,
			gtk_get_current_event_time ());
}

static gboolean
for_menu_button_press_event_cb (GtkWidget *button, GdkEventButton *event, 
				GtkMenu *menu)
{
	if (event->button == 3)
	{
		gtk_menu_popup (menu, NULL, NULL, NULL, NULL, 
				event ? event->button : 0, 
				event ? event->time : gtk_get_current_event_time ());
		return TRUE;
	}
	
	return FALSE;
}

static void
widget_add_context_menu (GtkWidget *widget, GtkMenu *menu)
{
	g_return_if_fail (GTK_IS_WIDGET (widget));
	g_return_if_fail (GTK_IS_MENU (menu));

	/* Destroy the menu when the proxy goes away */
	g_object_ref (menu);
	g_object_ref_sink (G_OBJECT (menu));
	g_object_weak_ref (G_OBJECT (widget), (GWeakNotify)g_object_unref, menu);
	
	/* Popup on the popup-menu signal */
	g_signal_connect (widget, "popup-menu",
			  G_CALLBACK (popup_menu_cb), menu);

	/* popup on right mouse button */
	g_signal_connect (widget, "button-press-event", 
			  G_CALLBACK (for_menu_button_press_event_cb), menu);
}

static void
galeon_action_go_connect_proxy (GtkAction *a, GtkWidget *proxy)
{
	GtkWidget *widget = proxy;

	if (GTK_IS_TOOL_ITEM (proxy))
	{
		GtkMenuShell *ms;

		widget = GTK_BIN (proxy)->child;

		/* Activate on click */
		g_signal_connect_object (widget, "clicked",
					 G_CALLBACK (gtk_action_activate), a,
					 G_CONNECT_SWAPPED);

		/* Create the menu */
		ms = GTK_MENU_SHELL (gtk_menu_new());

		gul_gui_append_new_menuitem (GTK_WIDGET (ms), _("Go To The Specified _Location"), 
				     G_CALLBACK (galeon_action_go_activate_cb), a);
		gul_gui_append_new_menuitem (GTK_WIDGET (ms), _("Go To The Specified Location in New _Tab"), 
				     G_CALLBACK (galeon_action_go_new_tab_activate_cb), a);
		gul_gui_append_new_menuitem (GTK_WIDGET (ms), _("Go To The Specified Location in New _Window"), 
				     G_CALLBACK (galeon_action_go_new_window_activate_cb), a);

		widget_add_context_menu (widget, GTK_MENU (ms));
	}

	g_signal_connect (widget, "button-press-event",
			  G_CALLBACK (galeon_action_go_button_press_event_cb), a);

	(* GTK_ACTION_CLASS (galeon_action_go_parent_class)->connect_proxy) (a, proxy);
}

