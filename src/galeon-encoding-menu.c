/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *  Copyright (C) 2003  Marco Pesenti Gritti
 *  Copyright (C) 2003  Christian Persch
 *  Copyright (C) 2003  Crispin Flowerday
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "galeon-encoding-menu.h"
#include "galeon-encoding-dialog.h"
#include "galeon-encodings.h"
#include "galeon-embed.h"
#include "galeon-embed-shell.h"
#include "galeon-shell.h"

#include <gtk/gtkuimanager.h>
#include <gtk/gtkaction.h>
#include <gtk/gtkradioaction.h>
#include <gtk/gtktoggleaction.h>
#include <gtk/gtkactiongroup.h>

#include "galeon-debug.h"
#include <glib/gi18n.h>
#include <string.h>

#define GALEON_ENCODING_MENU_GET_PRIVATE(object) (G_TYPE_INSTANCE_GET_PRIVATE ((object), \
				       GALEON_TYPE_ENCODING_MENU, GaleonEncodingMenuPrivate))


struct _GaleonEncodingMenuPrivate
{
	GaleonEncodings *encodings;
	GaleonWindow *window;
	GtkUIManager *merge;
	GtkActionGroup *action_group;
	gboolean update_tag;
	guint merge_id;
	GSList *encodings_radio_group;
	GaleonEncodingDialog *dialog;
};

enum
{
	PROP_0,
	PROP_WINDOW
};

static GObjectClass *parent_class = NULL;

#define ENCODING_PLACEHOLDER_PATH	"/menubar/View/Encoding/ViewEncodingPlaceholder"

static int
compare_encodings (GaleonEncodingInfo *a, GaleonEncodingInfo *b)
{
	return g_utf8_collate (a->title, b->title);
}

static void
add_menu_item (GaleonEncodingInfo *info, GaleonEncodingMenu *menu)
{
	char action[128], name[128];

	g_snprintf (action, sizeof(action), "Encoding%s", info->encoding);
	g_snprintf (name, sizeof(name), "%sItem", action);
	
	gtk_ui_manager_add_ui (menu->priv->merge, menu->priv->merge_id,
			       ENCODING_PLACEHOLDER_PATH,
			       name, action,
			       GTK_UI_MANAGER_MENUITEM, FALSE);
}

static void
build_menu (GaleonEncodingMenu *menu, GList *recent, GList *related)
{
	GaleonEncodingMenuPrivate *p = menu->priv;

	/* clear the menu */
	if (p->merge_id > 0)
	{
		gtk_ui_manager_remove_ui (p->merge, p->merge_id);
		gtk_ui_manager_ensure_update (p->merge);
	}

	/* build the new menu */
	p->merge_id = gtk_ui_manager_new_merge_id (p->merge);

	gtk_ui_manager_add_ui (p->merge, p->merge_id,
			       ENCODING_PLACEHOLDER_PATH,
			       "ViewEncodingAutomaticItem",
			       "ViewEncodingAutomatic",
			       GTK_UI_MANAGER_MENUITEM, FALSE);

	gtk_ui_manager_add_ui (p->merge, p->merge_id,
			       ENCODING_PLACEHOLDER_PATH,
			       "Sep1Item", "Sep1",
			       GTK_UI_MANAGER_SEPARATOR, FALSE);

	g_list_foreach (recent, (GFunc) add_menu_item, menu);

	gtk_ui_manager_add_ui (p->merge, p->merge_id,
			       ENCODING_PLACEHOLDER_PATH,
			       "Sep2Item", "Sep2",
			       GTK_UI_MANAGER_SEPARATOR, FALSE);

	g_list_foreach (related, (GFunc) add_menu_item, menu);

	gtk_ui_manager_add_ui (p->merge, p->merge_id,
			       ENCODING_PLACEHOLDER_PATH,
			       "Sep3Item", "Sep3",
			       GTK_UI_MANAGER_SEPARATOR, FALSE);

	gtk_ui_manager_add_ui (p->merge, p->merge_id,
			       ENCODING_PLACEHOLDER_PATH,
			       "ViewEncodingOtherItem",
			       "ViewEncodingOther",
			       GTK_UI_MANAGER_MENUITEM, FALSE);
}

static void
encoding_activate_cb (GtkAction *action, GaleonEncodingMenu *menu)
{
	GaleonEmbed *embed;
	GaleonEncodingPageInfo *info;
	const char *name, *encoding;

	if (gtk_toggle_action_get_active (GTK_TOGGLE_ACTION (action)) == FALSE
	    || menu->priv->update_tag)
	{
		return;
	}

	name = gtk_action_get_name (action);
	encoding = name + strlen("Encoding");

	embed = galeon_window_get_active_embed (menu->priv->window);

	info = galeon_embed_get_encoding_info (embed);

	/* Force only when different from current encoding */
	if (info->encoding && strcmp (info->encoding, encoding) != 0)
	{
		galeon_embed_set_encoding (embed, encoding);
	}

	galeon_encoding_page_info_free (info);

	galeon_encodings_add_recent (menu->priv->encodings, encoding);
}

static GtkAction*
add_action (const GaleonEncodingInfo *info, GaleonEncodingMenu *menu)
{
	GtkAction *action;
	char name[128], *tooltip;

	g_snprintf (name, sizeof (name), "Encoding%s", info->encoding);
	tooltip = g_strdup_printf (_("Display the page using the \"%s\" encoding"),
				   info->encoding);

	action = g_object_new (GTK_TYPE_RADIO_ACTION,
			       "name", name,
			       "label", _(info->title),
			       "tooltip", tooltip,
			       NULL);

	gtk_radio_action_set_group (GTK_RADIO_ACTION (action),
				    menu->priv->encodings_radio_group);

	menu->priv->encodings_radio_group = gtk_radio_action_get_group
						(GTK_RADIO_ACTION (action));

	g_signal_connect (action, "activate",
			  G_CALLBACK (encoding_activate_cb),
			  menu);

	gtk_action_group_add_action (menu->priv->action_group, action);
	g_object_unref (action);
	g_free (tooltip);
	return action;
}

static void
update_encoding_menu_cb (GtkAction *dummy, GaleonEncodingMenu *menu)
{
	GaleonEncodingMenuPrivate *p = menu->priv;
	GaleonEmbed *embed;
	GtkAction *action;
	GaleonEncodingPageInfo *page_info;
	const GaleonEncodingInfo *info;
	char name[128];
	const char *encoding;
	GList *recent = NULL, *related = NULL, *l;

	/* get most recently used encodings */
	recent = galeon_encodings_get_recent (p->encodings);

	embed = galeon_window_get_active_embed (p->window);
	page_info = galeon_embed_get_encoding_info (embed);
	if (page_info == NULL) 
	{
		build_menu (menu, NULL, NULL);
		return;
	}

	LOG ("encoding information enc='%s', forced=%s", page_info->encoding,
	     (page_info->forced ? "Yes" : "No") );

	START_PROFILER ("Building encoding menu");

	encoding = page_info->encoding;

	info = galeon_encodings_get_encoding (p->encodings, encoding);
	g_return_if_fail (info != NULL);

	/* set the encodings group's active member */
	g_snprintf (name, sizeof (name), "Encoding%s", encoding);
	action = gtk_action_group_get_action (p->action_group, name);

	if (action == NULL)
	{
		action = add_action (info, menu);
	}

	/* FIXME: block the "activate" signal on the actions instead; needs to 
	 * wait until g_signal_handlers_block_matched supports blocking
	 * by signal id alone.
	 */
	menu->priv->update_tag = TRUE;

	/* set the encodings group's active member */
	gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (action), TRUE);

	/* check if encoding was overridden */
	action = gtk_action_group_get_action (p->action_group,
					      "ViewEncodingAutomatic");
	gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (action), !page_info->forced);
	g_object_set (G_OBJECT (action), "sensitive", page_info->forced, NULL);

	/* get encodings related to the current encoding */
	related = galeon_encodings_get_encodings (p->encodings, info->group);
	related = g_list_sort (related, (GCompareFunc) compare_encodings);

	menu->priv->update_tag = FALSE;

	/* add the current encoding to the list of things to display */
	if (g_list_find (related, info) == NULL)
	{
		related = g_list_prepend (related, (GaleonEncodingInfo*)info);
	}

	/* make sure related and recent are disjoint so we don't display twice */
	for (l = related; l != NULL; l = l->next)
	{
		recent = g_list_remove (recent, l->data);
	}

	recent = g_list_sort (recent, (GCompareFunc) compare_encodings);

	build_menu (menu, recent, related);

	STOP_PROFILER ("Building encoding menu");

	/* cleanup */
	g_list_free (related);
	g_list_free (recent);

	galeon_encoding_page_info_free (page_info);
}

static void
galeon_encoding_menu_view_dialog_cb (GtkAction *action, GaleonEncodingMenu *menu)
{
	if (menu->priv->dialog == NULL)
	{
		menu->priv->dialog = galeon_encoding_dialog_new
					(menu->priv->window);

		g_object_add_weak_pointer(G_OBJECT (menu->priv->dialog),
					  (gpointer *) &menu->priv->dialog);
	}

	galeon_dialog_show (GALEON_DIALOG (menu->priv->dialog));
}

static void
galeon_encoding_menu_automatic_cb (GtkAction *action, GaleonEncodingMenu *menu)
{
	GaleonEmbed *embed;

	if (gtk_toggle_action_get_active (GTK_TOGGLE_ACTION (action)) == FALSE
	    || menu->priv->update_tag)
	{
		return;
	}

	embed = galeon_window_get_active_embed (menu->priv->window);

	/* setting "" will clear the forced encoding */
	galeon_embed_set_encoding (embed, "");
}

static GtkActionEntry menu_entries [] =
{
	{ "ViewEncodingOther", NULL, N_("_Other..."), NULL,
	  N_("Select an alternative encoding"),
	  G_CALLBACK (galeon_encoding_menu_view_dialog_cb) },
};
static guint n_menu_entries = G_N_ELEMENTS (menu_entries);

static GtkToggleActionEntry toggle_menu_entries [] =
{
	{ "ViewEncodingAutomatic", NULL, N_("_Automatic"), NULL,
	  N_("Use the encoding specified by the document"),
	  G_CALLBACK (galeon_encoding_menu_automatic_cb), FALSE }
};
static const guint n_toggle_menu_entries = G_N_ELEMENTS (toggle_menu_entries);

static void
galeon_encoding_menu_set_window (GaleonEncodingMenu *menu, GaleonWindow *window)
{
	GtkActionGroup *action_group;
	GtkWidget *widget;
	GList *encodings;

	g_return_if_fail (GALEON_IS_WINDOW (window));

	menu->priv->window = window;
	menu->priv->merge  = GTK_UI_MANAGER (window->merge);

	action_group = gtk_action_group_new ("EncodingActions");
	gtk_action_group_set_translation_domain (action_group, NULL);
	menu->priv->action_group = action_group;

	gtk_action_group_add_actions (action_group, menu_entries,
				      n_menu_entries, menu);
	gtk_action_group_add_toggle_actions (action_group, toggle_menu_entries,
                                    	     n_toggle_menu_entries, menu);

	encodings = galeon_encodings_get_encodings (menu->priv->encodings, LG_ALL);
	g_list_foreach (encodings, (GFunc) add_action, menu);
	g_list_free (encodings);

	gtk_ui_manager_insert_action_group (menu->priv->merge,
					    action_group, 0);
	g_object_unref (action_group);

	widget = gtk_ui_manager_get_widget (menu->priv->merge,
					    "/menubar/View");
	g_signal_connect_object (widget, "activate",
				 G_CALLBACK (update_encoding_menu_cb),
				 menu, 0);
}

static void
galeon_encoding_menu_set_property (GObject *object,
				 guint prop_id,
				 const GValue *value,
				 GParamSpec *pspec)
{
	GaleonEncodingMenu *menu = GALEON_ENCODING_MENU (object);

	switch (prop_id)
	{
		case PROP_WINDOW:
			galeon_encoding_menu_set_window (menu, g_value_get_object (value));
			break;
	}
}

static void
galeon_encoding_menu_get_property (GObject *object,
				 guint prop_id,
				 GValue *value,
				 GParamSpec *pspec)
{
	GaleonEncodingMenu *menu = GALEON_ENCODING_MENU (object);

	switch (prop_id)
	{
		case PROP_WINDOW:
			g_value_set_object (value, menu->priv->window);
			break;
	}
}

static void
galeon_encoding_menu_finalize (GObject *object)
{
	GaleonEncodingMenu *menu = GALEON_ENCODING_MENU (object); 
	if (menu->priv->dialog)
	{
		g_object_unref (menu->priv->dialog);
	}

	g_object_unref (menu->priv->encodings);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
galeon_encoding_menu_init (GaleonEncodingMenu *menu)
{
	GaleonEmbedShell *embed_shell;

	menu->priv = GALEON_ENCODING_MENU_GET_PRIVATE (menu);

	embed_shell = galeon_shell_get_embed_shell (galeon_shell);

	menu->priv->encodings = galeon_embed_shell_get_encodings (embed_shell);
	g_object_ref (menu->priv->encodings);
}

static void
galeon_encoding_menu_class_init (GaleonEncodingMenuClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = galeon_encoding_menu_finalize;
	object_class->set_property = galeon_encoding_menu_set_property;
	object_class->get_property = galeon_encoding_menu_get_property;

	g_object_class_install_property (object_class,
					 PROP_WINDOW,
					 g_param_spec_object ("window",
							      "Window",
							      "Parent window",
							      GALEON_TYPE_WINDOW,
							      G_PARAM_READWRITE |
							      G_PARAM_CONSTRUCT_ONLY));

	g_type_class_add_private (klass, sizeof (GaleonEncodingMenuPrivate));
}

GaleonEncodingMenu *
galeon_encoding_menu_new (GaleonWindow *window)
{
	return g_object_new (GALEON_TYPE_ENCODING_MENU,
			     "window", window,
			     NULL);
}

GType
galeon_encoding_menu_get_type (void)
{
	static GType galeon_encoding_menu_type = 0;

	if (galeon_encoding_menu_type == 0)
	{
		static const GTypeInfo our_info =
		{
			sizeof (GaleonEncodingMenuClass),
			NULL,
			NULL,
			(GClassInitFunc) galeon_encoding_menu_class_init,
			NULL,
			NULL,
			sizeof (GaleonEncodingMenu),
			0,
			(GInstanceInitFunc) galeon_encoding_menu_init
		};

		galeon_encoding_menu_type = g_type_register_static (G_TYPE_OBJECT,
								  "GaleonEncodingMenu",
								  &our_info, 0);
	}

	return galeon_encoding_menu_type;
}
