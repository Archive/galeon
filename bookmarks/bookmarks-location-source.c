/* 
 *  Copyright (C) 2002  Ricardo Fernándezs Pascual <ric@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "bookmarks-location-source.h"
#include "galeon-marshal.h"

static void		gb_location_source_base_init		(gpointer g_class);

GType
gb_location_source_get_type (void)
{
	static GType location_source_type = 0;
	
	if (! location_source_type)
	{
		static const GTypeInfo location_source_info =
			{
				sizeof (GbLocationSourceIface),	/* class_size */
				gb_location_source_base_init,		/* base_init */
				NULL,						/* base_finalize */
				NULL,
				NULL,						/* class_finalize */
				NULL,						/* class_data */
				0,
				0,						/* n_preallocs */
				NULL
			};
		
		location_source_type = g_type_register_static 
			(G_TYPE_INTERFACE, "GbLocationSource", &location_source_info, 0);
		g_type_interface_add_prerequisite (location_source_type, G_TYPE_OBJECT);
	}
	
	return location_source_type;
}

static void
gb_location_source_base_init (gpointer g_class)
{
	static gboolean initialized = FALSE;
	
	if (! initialized)
	{
		/* no signals for now, nothing to do */
		
		initialized = TRUE;
	}
}


gchar *
gb_location_source_get_location (GbLocationSource *source)
{
	return (* GB_LOCATION_SOURCE_GET_IFACE (source)->get_location) (source);
}

gchar *
gb_location_source_get_title (GbLocationSource *source)
{
	return (* GB_LOCATION_SOURCE_GET_IFACE (source)->get_title) (source);
}

