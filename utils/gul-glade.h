/*
 *  Copyright (C) 2000 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GUL_GLADE_H
#define GUL_GLADE_H

#include <glib.h>
#include <gtk/gtksizegroup.h>
#include <glade/glade-xml.h>

typedef struct 
{ 
        const gchar *name; 
        GtkWidget **ptr;
} WidgetLookup;

typedef enum {
	GUL_SENSITIVE_FOLLOW,
	GUL_SENSITIVE_INVERT
} GulSensitiveType;

G_BEGIN_DECLS

GtkWidget  *gul_glade_lookup_widget	(GtkWidget *widget, 
				 	 const char *widget_name);

GladeXML   *gul_glade_widget_new 	(const char *file, 
					 const char *widget_name, 
		  			 GtkWidget **root, 
					 gpointer data);

void        gul_glade_lookup_widgets 	(GladeXML *gxml, 
					 WidgetLookup *lookup_table);

void        gul_glade_get_widgets       (GladeXML *gxml,
		                         const char *widget_name, ...);

void        gul_glade_size_group_widgets (GladeXML *gxml,
		                          GtkSizeGroupMode mode,
					  const char *widget_name, ...);

void        gul_glade_group_sensitive    (GladeXML *gxml,
		                          const char *master_name,
					  GulSensitiveType type,
					  const char *slave_name, ...);


G_END_DECLS

#endif
