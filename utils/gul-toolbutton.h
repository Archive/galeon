/*
 *  Copyright (C) 2003  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __gul_toolbutton_h_
#define __gul_toolutton_h_

#include <gtk/gtkmenushell.h>
#include "gtk/gtktoolbutton.h"

/* object forward declarations */

typedef struct _GulToolbuttonClass GulToolbuttonClass;
typedef struct _GulToolbutton GulToolbutton;
typedef struct _GulToolbuttonPrivate GulToolbuttonPrivate;

#define GUL_TYPE_TOOLBUTTON		(gul_toolbutton_get_type())
#define GUL_TOOLBUTTON(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), GUL_TYPE_TOOLBUTTON,\
					 GulToolbutton))
#define GUL_TOOLBUTTON_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), GUL_TYPE_TOOLBUTTON,\
					 GulToolbuttonClass))
#define GUL_IS_TOOLBUTTON(object)	(G_TYPE_CHECK_INSTANCE_TYPE((object), GUL_TYPE_TOOLBUTTON))
#define GUL_IS_TOOLBUTTON_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), GUL_TYPE_TOOLBUTTON))
#define GUL_TOOLBUTTON_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), GUL_TYPE_TOOLBUTTON,\
					 GulToolbuttonClass))


struct _GulToolbuttonClass
{
        GtkToolButtonClass parent_class;

	void		(*menu_activated)		(GulToolbutton *b);
};

struct _GulToolbutton
{
	GtkToolButton parent;
        GulToolbuttonPrivate *priv;
};

GType			gul_toolbutton_get_type			(void);
GulToolbutton *		gul_toolbutton_new			(void);
GtkMenuShell *		gul_toolbutton_get_menu			(GulToolbutton *b);
void                    gul_toolbutton_set_show_arrow           (GulToolbutton *b, gboolean val);
GtkWidget *             gul_toolbutton_get_button               (GulToolbutton *b);

G_END_DECLS;

#endif
