/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "bookmarks-folder-tb-widget.h"
#include "galeon-marshal.h"

#include "gul-gui.h"
#include "bookmarks-widgets-private.h"
#include "bookmarks-gtk-menu.h"
#include "bookmarks-dnd.h"
#include "galeon-debug.h"

#include <gtk/gtktogglebutton.h>

/**
 * Private data
 */
#define GB_FOLDER_TB_WIDGET_GET_PRIVATE(object) (G_TYPE_INSTANCE_GET_PRIVATE ((object), \
				       GB_TYPE_FOLDER_TB_WIDGET, GbFolderTbWidgetPrivate))


struct _GbFolderTbWidgetPrivate {
	GtkWidget *mainwid;
	GbGtkMenu *ggm;
	GtkWidget *menu;
	gboolean showing_menu;
};

/**
 * Private functions, only availble from this file
 */
static void		gb_folder_tb_widget_finalize_impl 	(GObject *o);
static void		gb_folder_tb_widget_rebuild_impl	(GbTbWidget *w);
static gboolean		gb_folder_tb_widget_button_press_event_cb (GtkToggleButton *toggle, 
								   GdkEventButton *event, 
								   GbFolderTbWidget *w);
static void		gb_folder_tb_widget_toggled_cb		(GtkToggleButton *toggle, 
								 GbFolderTbWidget *w);
static void		gb_folder_tb_widget_menu_hide_cb	(GtkWidget *widget, GbFolderTbWidget *w);
static void		gb_folder_tb_widget_bookmark_activated_cb (GbGtkMenu *gm,
								   GbBookmarkEventActivated *ev,
								   GbFolderTbWidget *w);


/**
 * GbFolderTbWidget object
 */

G_DEFINE_TYPE (GbFolderTbWidget, gb_folder_tb_widget, GB_TYPE_TB_WIDGET);

static void
gb_folder_tb_widget_class_init (GbFolderTbWidgetClass *klass)
{
	G_OBJECT_CLASS (klass)->finalize = gb_folder_tb_widget_finalize_impl;
	GB_TB_WIDGET_CLASS (klass)->rebuild = gb_folder_tb_widget_rebuild_impl;

	g_type_class_add_private (klass, sizeof (GbFolderTbWidgetPrivate));
}

static void 
gb_folder_tb_widget_init (GbFolderTbWidget *w)
{
	GbFolderTbWidgetPrivate *p = GB_FOLDER_TB_WIDGET_GET_PRIVATE (w);
	w->priv = p;
	w->priv->ggm = NULL;
	w->priv->menu = NULL;
	w->priv->mainwid = NULL;
}

static void
gb_folder_tb_widget_finalize_impl (GObject *o)
{
	GbFolderTbWidget *w = GB_FOLDER_TB_WIDGET (o);
	GbFolderTbWidgetPrivate *p = w->priv;
	
	LOG ("Finalizing GbFolderTbWidget");

	if (p->ggm)
	{
		g_object_unref (G_OBJECT (p->ggm));
		p->ggm = NULL;
	}

	if (p->menu)
	{
		gtk_widget_destroy (GTK_WIDGET (p->menu));
		p->menu = NULL;
	}


	G_OBJECT_CLASS (gb_folder_tb_widget_parent_class)->finalize (o);
}

GbFolderTbWidget *
gb_folder_tb_widget_new (GbFolder *folder)
{
	GbFolderTbWidget *ret = g_object_new (GB_TYPE_FOLDER_TB_WIDGET, "bookmark", folder, NULL);
	return ret;
}

static void
gb_folder_tb_widget_rebuild_impl (GbTbWidget *w)
{
	GbFolderTbWidgetPrivate *p = GB_FOLDER_TB_WIDGET (w)->priv;
	GbFolder *folder = GB_FOLDER (gb_tb_widget_get_bookmark (w));

	g_return_if_fail (GB_IS_FOLDER (folder));

	if (!p->mainwid)
	{
		p->mainwid = gtk_toggle_button_new ();
		gtk_widget_show (p->mainwid);
		gtk_box_pack_start (GTK_BOX (w), p->mainwid, FALSE, FALSE, 0);

		g_signal_connect (p->mainwid, "button-press-event", 
				  G_CALLBACK (gb_folder_tb_widget_button_press_event_cb), w);
		g_signal_connect (p->mainwid, "toggled", 
				  G_CALLBACK (gb_folder_tb_widget_toggled_cb), w);
	}

	if (gtk_bin_get_child (GTK_BIN (p->mainwid)))
	{
		gtk_container_remove (GTK_CONTAINER (p->mainwid),
				      gtk_bin_get_child (GTK_BIN (p->mainwid)));
	}
	
	gb_widgets_fill_tb_item (GTK_CONTAINER (p->mainwid), GB_BOOKMARK (folder), NULL);

	gtk_button_set_relief (GTK_BUTTON (p->mainwid), GTK_RELIEF_NONE);
	GTK_WIDGET_SET_FLAGS (GTK_BUTTON (p->mainwid), GTK_CAN_FOCUS);

	gb_tb_widget_setup_context_menu (w, p->mainwid);
	gb_bookmark_dnd_drag_dest_set (GTK_BIN (p->mainwid)->child, GB_BOOKMARK (folder));
}

static gboolean
gb_folder_tb_widget_button_press_event_cb (GtkToggleButton *toggle, GdkEventButton *event, 
					   GbFolderTbWidget *w)
{
	GbFolderTbWidgetPrivate *p = w->priv;
	if (event->button == 1)
	{
		GbFolder *folder = GB_FOLDER (gb_tb_widget_get_bookmark (GB_TB_WIDGET (w)));

		if (!p->menu)
		{
			GbLocationSource *src = gb_tb_widget_get_location_source (GB_TB_WIDGET (w));
			p->menu = gtk_menu_new ();
			p->ggm = gb_gtk_menu_new (folder, GTK_MENU_SHELL (p->menu));
			gb_gtk_menu_set_statusbar (p->ggm, gb_tb_widget_get_statusbar (GB_TB_WIDGET (w)));
			gb_gtk_menu_set_location_source (p->ggm, src);
			g_signal_connect (p->menu, "hide", 
					  G_CALLBACK (gb_folder_tb_widget_menu_hide_cb), w);
			g_signal_connect (p->ggm, "bookmark-activated", 
					  G_CALLBACK (gb_folder_tb_widget_bookmark_activated_cb), w);
			gb_gtk_menu_fill_children_submenus (p->ggm);
		}
		w->priv->showing_menu = TRUE;
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (p->mainwid), TRUE);
		gtk_menu_popup (GTK_MENU (p->menu), NULL, NULL,
				gul_gui_menu_position_under_widget,
				w, event->button, event->time);
		return TRUE;
	}
	else 
	{
		return FALSE;
	}
}

static void
gb_folder_tb_widget_toggled_cb (GtkToggleButton *toggle, 
				GbFolderTbWidget *w)
{
	GbFolderTbWidgetPrivate *p = w->priv;
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (p->mainwid))
		&& !w->priv->showing_menu)
	{
		GdkEventButton event;
		event.button = 1;
		event.time = GDK_CURRENT_TIME;
		gb_folder_tb_widget_button_press_event_cb (toggle, &event, w);
	}
}

static void
gb_folder_tb_widget_menu_hide_cb (GtkWidget *widget, GbFolderTbWidget *w)
{	
	GbFolderTbWidgetPrivate *p = w->priv;
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (p->mainwid), FALSE);
	w->priv->showing_menu = FALSE;
}

static void
gb_folder_tb_widget_bookmark_activated_cb (GbGtkMenu *gm, 
					   GbBookmarkEventActivated *ev,
					   GbFolderTbWidget *w)
{
	g_signal_emit_by_name (w, "bookmark-activated", ev);
}


