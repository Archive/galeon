/*
 *  Copyright (C) 2003  Ricardo Fernández Pascual
 *  Copyright (C) 2003  Philip Langdale
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GALEON_TAB_MENU_H
#define GALEON_TAB_MENU_H

#include <glib-object.h>
#include "galeon-window.h"

/* object forward declarations */

typedef struct _GaleonTabMenu GaleonTabMenu;
typedef struct _GaleonTabMenuClass GaleonTabMenuClass;
typedef struct _GaleonTabMenuPrivate GaleonTabMenuPrivate;

/**
 * GaleonTabMenu object
 */

#define GALEON_TYPE_TAB_MENU		(galeon_tab_menu_get_type())
#define GALEON_TAB_MENU(object)		(G_TYPE_CHECK_INSTANCE_CAST((object),\
					 GALEON_TYPE_TAB_MENU,\
					 GaleonTabMenu))
#define GALEON_TAB_MENU_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass),\
					 GALEON_TYPE_TAB_MENU,\
					 GaleonTabMenuClass))
#define GALEON_IS_TAB_MENU(object)	(G_TYPE_CHECK_INSTANCE_TYPE((object),\
					 GALEON_TYPE_TAB_MENU))
#define GALEON_IS_TAB_MENU_ITEM_CLASS(klass) \
	 				(G_TYPE_CHECK_CLASS_TYPE((klass),\
	 				 GALEON_TYPE_TAB_MENU))
#define GALEON_TAB_MENU_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj),\
					 GALEON_TYPE_TAB_MENU,\
					 GaleonTabMenuClass))

struct _GaleonTabMenuClass
{
	GObjectClass parent_class;
};

/* Remember: fields are public read-only */
struct _GaleonTabMenu
{
	GObject parent_object;
	
	GaleonTabMenuPrivate *priv;
};

GType		 galeon_tab_menu_get_type		(void);
GaleonTabMenu	*galeon_tab_menu_new			(GaleonWindow *window);

#endif
