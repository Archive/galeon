/*
 *  Copyright (C) 2001 Matt Aubury, Philip Langdale
 *  Copyright (C) 2004 Crispin Flowerday
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <nscore.h>
#include <nsComponentManagerUtils.h>
#include <nsCOMPtr.h>
#include <nsIIOService.h>
#include <nsIURI.h>
#include <nsIChannel.h>
#include <nsIOutputStream.h>
#include <nsIInputStream.h>
#include <nsILoadGroup.h>
#include <nsIInterfaceRequestor.h>

#include <nsIStorageStream.h>
#include <nsIInputStreamChannel.h>
#include <nsNetCID.h>

#include <libgnomevfs/gnome-vfs-utils.h>
#include <glib/gi18n.h>

#include "MyportalProtocolHandler.h"
#include "GaleonUtils.h"
#include "GulString.h"

#include "bookmarks.h"
#include "bookmarks-iterator.h"
#include "gul-general.h"
#include "src/galeon-shell.h"

#define FILE_PREFIX ""

static NS_DEFINE_CID(kSimpleURICID, NS_SIMPLEURI_CID);
static NS_DEFINE_CID(kInputStreamChannelCID, NS_INPUTSTREAMCHANNEL_CID);

/* Implementation file */
NS_IMPL_ISUPPORTS2 (GMyportalProtocolHandler, nsIProtocolHandler, nsIAboutModule)

GMyportalProtocolHandler::GMyportalProtocolHandler (void)
{
}

GMyportalProtocolHandler::~GMyportalProtocolHandler()
{
	/* destructor code */
}


/* readonly attribute string scheme; */
NS_IMETHODIMP
GMyportalProtocolHandler::GetScheme(nsACString &aScheme)
{
	aScheme = NS_LITERAL_CSTRING("myportal");
	return NS_OK;
}

/* readonly attribute long defaultPort; */
NS_IMETHODIMP
GMyportalProtocolHandler::GetDefaultPort(PRInt32 *aDefaultPort)
{
	NS_ENSURE_ARG_POINTER (aDefaultPort);

	*aDefaultPort = -1;
	return NS_OK;
}

/* readonly attribute short protocolFlags; */
NS_IMETHODIMP
GMyportalProtocolHandler::GetProtocolFlags(PRUint32 *aProtocolFlags)
{
	NS_ENSURE_ARG_POINTER (aProtocolFlags);

	/* Myportal urls don't have an authority component (user:pass) */
	*aProtocolFlags = nsIProtocolHandler::URI_NOAUTH;
	return NS_OK;
} 

/* nsIURI newURI (in string aSpec, in nsIURI aBaseURI); */
NS_IMETHODIMP
GMyportalProtocolHandler::NewURI(const nsACString &aSpec,
				 const char *aOriginCharset,
				 nsIURI *aBaseURI,
				 nsIURI **_retval)
{
	nsresult rv;

	nsCOMPtr<nsIURI> uri = do_CreateInstance(kSimpleURICID);
	NS_ENSURE_TRUE (uri, NS_ERROR_FAILURE);

	rv = uri->SetSpec (aSpec);
	NS_ENSURE_SUCCESS (rv, rv);

	NS_ADDREF(*_retval = uri);
	return NS_OK;
}

/* nsIChannel newChannel (in nsIURI aURI); */
NS_IMETHODIMP
GMyportalProtocolHandler::NewChannel(nsIURI *aURI,
				     nsIChannel **_retval)
{
	nsresult rv;

	PRBool isabout;
	rv =  aURI->SchemeIs ("about", &isabout);
	NS_ENSURE_SUCCESS (rv, rv);

	if (isabout)
	{
		rv = RedirectURL ("myportal:", _retval);
	}
	else
	{
		GulCString path;
		rv = aURI->GetPath (path);
		NS_ENSURE_SUCCESS (rv, rv);

		if (path.StartsWith ("form:"))
		{
			rv = HandleSmartBMSubmit (path.get() + 5, _retval);
			if (NS_SUCCEEDED (rv)) return NS_OK;
		}

		rv = CreateMyportalPage (path.get(), aURI, _retval);
	}
	return rv;
}

/* boolean allowPort (in long port, in string scheme); */
NS_IMETHODIMP 
GMyportalProtocolHandler::AllowPort(PRInt32 port,
				    const char *scheme,
				    PRBool *_retval)
{
	*_retval = PR_FALSE;
	return NS_OK;
}

#ifdef HAVE_NSIABOUTMODULE_GETURIFLAGS
/* unsigned long getURIFlags (in nsIURI aURI); */
NS_IMETHODIMP
GMyportalProtocolHandler::GetURIFlags(nsIURI *aURI,
                                      PRUint32 *_retval)
{
	*_retval = 0;
	return NS_OK;
}
#endif

// ------------------------------------------------------------
/**
 * An nsIChannel implementation that just redirects to a
 * different url */
class GRedirectChannel : public nsIChannel
{
public:
	GRedirectChannel (const nsACString &aURL)
	: mURL (aURL), mLoadFlags (LOAD_NORMAL) {}

	NS_DECL_ISUPPORTS
	NS_DECL_NSICHANNEL
	NS_DECL_NSIREQUEST

private:
	const GulCString mURL;
	nsCOMPtr<nsILoadGroup>  mLoadGroup;
	nsCOMPtr<nsIInterfaceRequestor> mCallbacks;
	PRUint32 mLoadFlags;
};

NS_IMPL_ISUPPORTS2 (GRedirectChannel, nsIChannel, nsIRequest)

NS_IMETHODIMP
GRedirectChannel::GetNotificationCallbacks(nsIInterfaceRequestor * *aCallbacks)
{
	NS_IF_ADDREF(*aCallbacks = mCallbacks);
	return NS_OK;
}

NS_IMETHODIMP 
GRedirectChannel::SetNotificationCallbacks(nsIInterfaceRequestor * aCallbacks)
{
	mCallbacks = aCallbacks;
	return NS_OK;
}

NS_IMETHODIMP
GRedirectChannel::GetLoadGroup(nsILoadGroup * *aLoadGroup)
{
	NS_IF_ADDREF(*aLoadGroup = mLoadGroup);
	return NS_OK;
}

NS_IMETHODIMP
GRedirectChannel::SetLoadGroup(nsILoadGroup * aLoadGroup)
{
	mLoadGroup =  aLoadGroup;
	return NS_OK;
}

NS_IMETHODIMP
GRedirectChannel::GetLoadFlags(nsLoadFlags *aLoadFlags)
{
	*aLoadFlags = mLoadFlags;
	return NS_OK;
}

NS_IMETHODIMP
GRedirectChannel::SetLoadFlags(nsLoadFlags aLoadFlags)
{
	mLoadFlags = aLoadFlags;
	return NS_OK;
}

NS_IMETHODIMP
GRedirectChannel::GetOriginalURI(nsIURI * *aOriginalURI)
{
	return GaleonUtils::NewURI (aOriginalURI, mURL);
}


/* void asyncOpen (in nsIStreamListener aListener, in nsISupports aContext); */
NS_IMETHODIMP 
GRedirectChannel::AsyncOpen (nsIStreamListener *aListener, nsISupports *aContext)
{
	nsresult rv;

	nsCOMPtr<nsIIOService> ioService;

	/* Create a new Channel based off the url we want to redirect to
	 * then everthing appears to magically work, and the smb forms
	 * end up with the correct url */
	rv = GaleonUtils::GetIOService (getter_AddRefs (ioService));
	NS_ENSURE_SUCCESS (rv, rv);

	nsCOMPtr<nsIChannel> newChannel;

	rv = ioService->NewChannel (mURL, nsnull, nsnull, getter_AddRefs (newChannel));
	NS_ENSURE_SUCCESS (rv, rv);

	newChannel->SetLoadGroup (mLoadGroup); 
	newChannel->SetNotificationCallbacks (mCallbacks);
	newChannel->SetLoadFlags (mLoadFlags | LOAD_REPLACE);

	rv = newChannel->AsyncOpen (aListener, aContext);
	NS_ENSURE_SUCCESS (rv, rv);

	return NS_OK;
}

// ------------------------------------------------------------

nsresult
GMyportalProtocolHandler::RedirectURL (const char *path,
				       nsIChannel **aChannel)
{
	nsCOMPtr<nsIChannel> channel;

	/* Use our own channel implementation so that url in the
	 * location bar is correct after a form submit */

	channel = new GRedirectChannel (GulDependentCString (path));

	NS_ADDREF(*aChannel = channel);

	return NS_OK;
}

/**
 * Smart bookmarks are rendered using a form that points back
 * to the magic myportal:form: url. The rest of the path is the
 * bookmark id, with the query string being the smart bookmark
 * arguments.
 *
 * The arguments are always submitted in UTF-8 and so we can just
 * extract them from the query string, and use the normal
 * bookmark code to create the final url, which we then
 * redirect to.
 */
nsresult
GMyportalProtocolHandler::HandleSmartBMSubmit (const char *path,
					       nsIChannel **aChannel)
{
	GbBookmarkSet *bmset;
	GbBookmark *b;
	int i, nargs, count;
	gchar **raw_args, **args;
	gchar *url;
	
	/* Path includes the query string, so lets strip that off */
	const char *qs = strchr (path, '?');
	if (!qs) return NS_ERROR_FAILURE;

	gchar *id = g_strndup (path, qs - path);
	bmset = galeon_shell_get_bookmark_set (galeon_shell);
	b = gb_bookmark_set_get_bookmark_for_id (bmset, id);
	g_free (id);

	if (!b || ! GB_IS_SMART_SITE (b))
	{
		return NS_ERROR_FAILURE;
	}

	args = g_strsplit (qs+1, "&", -1);
	for (nargs = 0; args[nargs]; nargs++) ;

	raw_args = g_new0 (gchar *, nargs + 1);
	count = 0;
	for (i = 0; i < nargs; i++)
	{
		/* Ignore things like x= and y= (from image submits) */
		if (!g_str_has_prefix (args[i], "arg="))
		{
			continue;
		}

		/* Replace '+''s with ' ' (gnome_vfs doesn't do this) */
		gchar * arg = args[i] +4;
		gchar *tmp = arg;
		for (; *tmp; tmp++)
		{
			if (*tmp == '+') *tmp = ' ';
		}

		raw_args[count++] = gnome_vfs_unescape_string (arg, "");

	}
	raw_args[count] = NULL;

	url = gb_smart_site_subst_args (GB_SMART_SITE (b), raw_args);

	g_strfreev (args);
	g_strfreev (raw_args);

	nsresult rv;
	rv = RedirectURL (url, aChannel);
	g_free (url);

	return rv;
}

nsresult
GMyportalProtocolHandler::Write (const char * aStr)
{
	NS_ENSURE_TRUE (mStream, NS_ERROR_FAILURE);

	PRUint32 bytesWritten;
	if (aStr)
	{
		nsresult rv;
		rv = mStream->Write (aStr, strlen (aStr), &bytesWritten);
		NS_ENSURE_SUCCESS (rv, rv);
	}
	return NS_OK;
}


nsresult
GMyportalProtocolHandler::WriteHTMLEscape (const char * aStr)
{
	nsresult rv = NS_OK;
	if (aStr)
	{
		char *str;
		
		str = g_markup_escape_text (aStr, strlen (aStr));
		rv = Write (str);
		g_free (str);
	}
	return rv;
}

nsresult
GMyportalProtocolHandler::CreateMyportalPage (const char *path,
					      nsIURI *aURI,
					      nsIChannel **result)
{
	nsresult rv;

	if (!path || path[0] == '\0')
	{
		path = "/";
	}

	/* open the rendering stream */
	nsCOMPtr<nsIStorageStream> sStream (do_CreateInstance("@mozilla.org/storagestream;1", &rv));
	NS_ENSURE_SUCCESS(rv, rv);
	rv = sStream->Init(16384, PR_UINT32_MAX, nsnull);
	NS_ENSURE_SUCCESS(rv, rv);

	rv = sStream->GetOutputStream(0, getter_AddRefs(mStream));
	NS_ENSURE_SUCCESS (rv, rv);

	/* render the complete portal */
	Write ("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" "
	       "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n");
	Write ("<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\">\n");
	Write ("<head>\n");
	gchar *cssPath = gul_general_user_file ("myportal.css", FALSE);	
	if (cssPath)
	{
		Write ("<link rel=\"stylesheet\" href=\"" FILE_PREFIX "file://");
		Write (cssPath);
		Write ("\" type=\"text/css\" />\n");
		g_free (cssPath);
	}
	Write ("<title>");

	gchar *tmp = gnome_vfs_unescape_string (path, "");
	/* Skip multiple leading '/' (it's guaranteed to be at least 1 char long due
	 * to the path being set to '/' at the top of the function */
	const gchar *q = tmp;
	while (q[1] == '/') q++;

	/* Translators: the %s is the bookmark path, e.g. "/Programs/Galeon" */
	gchar *str = g_strdup_printf (_("My Portal: %s"), q);
	WriteHTMLEscape (str);
	g_free (str);
	g_free (tmp);
	Write ("</title>");
	Write ("</head>\n");
	Write ("<body>\n");
	Write ("<div class=\"top_bar\">\n");
	Write ("<img src=\"" FILE_PREFIX "file://" SHARE_DIR "/galeon.png\" alt=\"\" />myportal:\n");
	Write ("</div>\n");
	Write ("<div class=\"top2\">\n");
	Write ("<a href=\"http://galeon.sourceforge.net\">");
	Write ("<img class=\"logo\" src=\"" FILE_PREFIX "file://" SHARE_DIR "/logo.png\" alt=\"\" />");
	Write ("</a>\n</div>\n");
	Write ("<!-- BOOKMARKS -->\n\n");
	RenderFromPath (path);
	Write ("\n\n<!-- END -->");
	Write ("<div class=\"bottom_bar\">\n");
	Write ("<b>Galeon</b> : <i>the web, only the web</i>\n");
	Write ("</div>");
	Write ("</body></html>\n");
 
	mStream = nsnull;

	/* finish the rendering */
	nsCOMPtr<nsIInputStream> iStream;
	rv = sStream->NewInputStream(0, getter_AddRefs(iStream));
	NS_ENSURE_SUCCESS (rv, rv);

	nsCOMPtr<nsIInputStreamChannel> isc =
		do_CreateInstance(kInputStreamChannelCID, &rv);
	NS_ENSURE_SUCCESS (rv, rv);

        rv |= isc->SetURI (aURI);
	rv |= isc->SetContentStream (iStream);

	nsCOMPtr<nsIChannel> channel = do_QueryInterface (isc, &rv);
	NS_ENSURE_SUCCESS (rv, rv);

        rv |= channel->SetContentType (NS_LITERAL_CSTRING ("application/xhtml+xml"));
        rv |= channel->SetContentCharset (NS_LITERAL_CSTRING ("utf-8"));
	NS_ENSURE_SUCCESS (rv, rv);

	NS_ADDREF(*result = channel);
	return rv;
}

////////////////////////////////////////////////////////////////////////////////
/* This is just used for finding, not sorting, so we only worry about
 * zero vs non-zero */ 
static gint 
folder_name_compare (const GbBookmark *a, const gchar *name)
{
	if (a->name == NULL)
		return -1;

	if (!GB_IS_FOLDER (a))
		return -1;

	return strcmp (name, a->name);
}

nsresult
GMyportalProtocolHandler::RenderFromPath (const gchar *path)
{
	GbBookmark *root;

	root = GB_BOOKMARK(galeon_shell_get_bookmark_set(galeon_shell)->root);
	if (!root) return NS_OK;
	g_return_val_if_fail (GB_IS_FOLDER (root), NS_ERROR_FAILURE);

	GString *clippedpath = g_string_new (path);
	const gchar *anch = strchr (path, '#');
	if (anch)
	{
		g_string_truncate (clippedpath, anch-path);
	}

	gchar **paths = g_strsplit (clippedpath->str, "/", 0);

	/* Draw the first folder heading (might have extra 
	 * path components) */
	Write ("<div class=\"folder\">\n");
	Write ("<h1 class=\"folderheading\">\n");

	GString *prefix = g_string_new ("myportal://");
	int count = 0;
	while (root)
	{
		if (count != 0 )
		{
			Write ("/");
		}

		Write ("<a href=\"");

		WriteHTMLEscape (prefix->str);

		Write ("\">");
		WriteHTMLEscape (root->name);
		Write ("</a>");

		/* Find the next child */
		while (paths[count] && *paths[count] == 0) count++;

		/* reached end of the paths, so break out */
		if (!paths[count])
		{
			break;
		}

		gchar *name = gnome_vfs_unescape_string (paths[count], "");

		GSList *children = gb_folder_list_children (GB_FOLDER (root));

		GSList *match = g_slist_find_custom (children, name,
						     (GCompareFunc)folder_name_compare);
		if (!match)
		{
			root = NULL;
			break;
		}

		char *escaped = gnome_vfs_escape_string (name);
		g_string_append (prefix, escaped);
		g_string_append_c (prefix, '/');
		g_free (name);
		g_free (escaped);
		
		count++;
		root = GB_BOOKMARK (match->data);
	}
	g_strfreev (paths);

	Write ("</h1>\n");

	if (root)
	{
		RenderFolderContents (GB_FOLDER (root), prefix->str, 1);
	}
	else
	{
		Write ("<div class=\"foldercontents\">\n");

		gchar *unescaped = gnome_vfs_unescape_string (clippedpath->str, "");
		gchar *buf = g_markup_printf_escaped (_("%s not found"), 
						      unescaped);
		Write (buf);
		Write ("</div>\n");

		g_free (buf);
		g_free (unescaped);
	}		

	g_string_free (clippedpath, TRUE);
	g_string_free (prefix, TRUE);
	Write ("</div>\n");

	return NS_OK;
}

/**
 * render_from_item: render a single bookmark item
 */
nsresult
GMyportalProtocolHandler::RenderItem (GbBookmark *b, const gchar *prefix, 
				      gint depth)
{
	/* could be the case if there are no bookmarks */
	if (b == NULL)
	{
		return NS_OK;
	}

	/* otherwise do the appropriate thing */
	if (GB_IS_FOLDER (b))
	{
		RenderFolder (GB_FOLDER (b), prefix, depth);
	} 
	else if (GB_IS_SMART_SITE (b))
	{
		RenderSmartURL (GB_SMART_SITE (b));
	}
	else if (GB_IS_SITE (b))
	{
		RenderURL (GB_SITE (b));
	}
	else if (GB_IS_SEPARATOR (b))
	{		
		Write ("<div class=\"separator\"></div>\n");
	}

	return NS_OK;
}

nsresult
GMyportalProtocolHandler::RenderFolderContents (GbFolder *b, const gchar *prefix, 
						gint depth)
{
	GbBookmark *bi;
	GbIterator *i;

	Write ("<div class=\"foldercontents\">\n");

	i = gb_iterator_new_folder (b, FALSE);
	bi = gb_iterator_next(i);	

	if (bi)
	{
		do
		{
			RenderItem (bi, prefix, depth);
		} while ((bi = gb_iterator_next(i)));

	}
	else
	{
		Write ("<i>");
		Write (_("Empty Folder"));
		Write ("</i>");
	}
	g_object_unref (i);

	Write ("</div>\n");

	return NS_OK;
}

/**
 * render_from_list: render a list of bookmark items
 */
nsresult
GMyportalProtocolHandler::RenderFolder (GbFolder *b, const gchar *prefix, gint depth)
{
	const gchar *name;
	gchar *newprefix;

	name = GB_BOOKMARK (b)->name;

	if (depth == 0)
	{
		newprefix = g_strdup (prefix);
	}
	else
	{
		char *escaped = gnome_vfs_escape_string (name);
		newprefix = g_strconcat (prefix, escaped, "/", NULL);
		g_free (escaped);
	}

	Write ("<div class=\"folder\">\n");
	Write ("<h1 class=\"folderheading\">\n");

	Write ("<a href=\"");
	WriteHTMLEscape (newprefix);
	Write ("\">");

	WriteHTMLEscape (name);
	Write ("</a>\n");
	Write ("</h1>\n");

	RenderFolderContents (b, newprefix, depth+1);

	Write ("</div>\n");

	g_free (newprefix);

	return NS_OK;
}


/**
 * render_url: render a bookmark url
 */
nsresult
GMyportalProtocolHandler::RenderURL (GbSite *b)
{
	Write ("<a href=\"");

	/* If url has no preceding "http://" or "ftp://" presume "http://" */
	if (!strchr (b->url, ':'))
	{
		Write ("http://");
	}

	if (g_str_has_prefix (b->url, "file:"))
	{
		Write (FILE_PREFIX);
	}

	WriteHTMLEscape (b->url);
	Write ("\">");

	if (gb_bookmark_get_image(GB_BOOKMARK(b)) == NULL)
	{
		/* use name if we have one, and otherwise, url */
		if (GB_BOOKMARK(b)->name && GB_BOOKMARK(b)->name[0] != '\0')
		{
			WriteHTMLEscape (GB_BOOKMARK(b)->name);
		}
		else
		{
			WriteHTMLEscape (b->url);
		}
	}
	else
	{
		Write ("<img src=\"" FILE_PREFIX "file://");
		Write (GB_BOOKMARK(b)->pixmap_file);
		Write ("\" />");
	}
	Write ("</a>\n");

	return NS_OK;
}

/**
 * render_search_form: render a search form based on a smart bookmark,
 * this posts back to myportal to avoid problems with %s's not
 * inside a query string
 */
nsresult
GMyportalProtocolHandler::RenderSmartURL (GbSmartSite *b)
{
	Write ("<form action=\"myportal:form:");
	WriteHTMLEscape (gb_bookmark_get_id (GB_BOOKMARK (b)));
	Write ("\" method=\"get\"><p>");

	gint num_inputs = gb_smart_site_get_num_fields (b);

	for (int i = 0 ; i < num_inputs ; i++)
	{
		Write ("<input type=\"text\" name=\"arg\" size=\"");
		Write (num_inputs>1?"10\" />":"30\" />");
	}

	if (gb_bookmark_get_image(GB_BOOKMARK(b)) == NULL)
	{
		Write ("<input type=\"submit\" value=\"");
		WriteHTMLEscape (GB_BOOKMARK (b)->name);
	}
	else
	{
		Write ("<input type=\"image\" src=\"" FILE_PREFIX "file://");
		WriteHTMLEscape (GB_BOOKMARK(b)->pixmap_file);
	}
	Write ("\" /></p></form>\n");

	return NS_OK;
}


/** ------------------------------------------------------------ *
 *  Other nsIChannel functions */
#define MOZ_NOT_IMPLEMENTED return NS_ERROR_NOT_IMPLEMENTED;

NS_IMETHODIMP GRedirectChannel::GetName(nsACString & aName)
{
    MOZ_NOT_IMPLEMENTED;
}

NS_IMETHODIMP GRedirectChannel::IsPending(PRBool *_retval)
{
    MOZ_NOT_IMPLEMENTED;
}

NS_IMETHODIMP GRedirectChannel::GetStatus(nsresult *aStatus)
{
    MOZ_NOT_IMPLEMENTED;
}

NS_IMETHODIMP GRedirectChannel::Cancel(nsresult aStatus)
{
    MOZ_NOT_IMPLEMENTED;
}

NS_IMETHODIMP GRedirectChannel::Suspend()
{
    MOZ_NOT_IMPLEMENTED;
}

NS_IMETHODIMP GRedirectChannel::Resume()
{
    MOZ_NOT_IMPLEMENTED;
}

NS_IMETHODIMP GRedirectChannel::SetOriginalURI(nsIURI * aOriginalURI)
{
    MOZ_NOT_IMPLEMENTED;
}

NS_IMETHODIMP GRedirectChannel::GetURI(nsIURI * *aURI)
{
    MOZ_NOT_IMPLEMENTED;
}

NS_IMETHODIMP GRedirectChannel::GetOwner(nsISupports * *aOwner)
{
    MOZ_NOT_IMPLEMENTED;
}

NS_IMETHODIMP GRedirectChannel::SetOwner(nsISupports * aOwner)
{
    MOZ_NOT_IMPLEMENTED;
}

NS_IMETHODIMP GRedirectChannel::GetSecurityInfo(nsISupports * *aSecurityInfo)
{
    MOZ_NOT_IMPLEMENTED;
}

NS_IMETHODIMP GRedirectChannel::GetContentType(nsACString & aContentType)
{
    MOZ_NOT_IMPLEMENTED;
}

NS_IMETHODIMP GRedirectChannel::SetContentType(const nsACString & aContentType)
{
    MOZ_NOT_IMPLEMENTED;
}

NS_IMETHODIMP GRedirectChannel::GetContentCharset(nsACString & aContentCharset)
{
    MOZ_NOT_IMPLEMENTED;
}

NS_IMETHODIMP GRedirectChannel::SetContentCharset(const nsACString & aContentCharset)
{
    MOZ_NOT_IMPLEMENTED;
}

NS_IMETHODIMP GRedirectChannel::GetContentLength(PRInt32 *aContentLength)
{
    MOZ_NOT_IMPLEMENTED;
}

NS_IMETHODIMP GRedirectChannel::SetContentLength(PRInt32 aContentLength)
{
    MOZ_NOT_IMPLEMENTED;
}

NS_IMETHODIMP GRedirectChannel::Open(nsIInputStream **_retval)
{
    MOZ_NOT_IMPLEMENTED;
}
