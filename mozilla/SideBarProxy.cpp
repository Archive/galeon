/*
 *  Copyright (C) 2002 Philip Langdale
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <nsCOMPtr.h>
#include <nsCRT.h>
#ifdef HAVE_NSICLASSINFOIMPL_H
#include <nsIClassInfoImpl.h>
#endif
#include <nsIProgrammingLanguage.h>

#include "GulString.h"
#include "SideBarProxy.h"
#include "galeon-embed-shell.h"

/* Implementation file */
NS_IMPL_ISUPPORTS1_CI(GSidebarProxy, nsISidebar)

GSidebarProxy::GSidebarProxy()
{
}

GSidebarProxy::~GSidebarProxy()
{
}


/* void addPanel (in wstring aTitle, in string aContentURL, in string aCustomizeURL); */
NS_IMETHODIMP
GSidebarProxy::AddPanel (const PRUnichar *aTitle,
			 const char *aContentURL,
			 const char *aCustomizeURL)
{
	GulCString msg (aTitle);

        if (embed_shell)
        {
                g_signal_emit_by_name (embed_shell, "add-sidebar",
                                       aContentURL, msg.get());
        }

        return NS_OK;
}

/* void addPersistentPanel (in wstring aTitle, in string aContentURL, in string aCustomizeURL); */
NS_IMETHODIMP
GSidebarProxy::AddPersistentPanel (const PRUnichar *aTitle,
                                  const char *aContentURL,
                                  const char *aCustomizeURL)
{
        return NS_ERROR_NOT_IMPLEMENTED;
}

/* void addSearchEngine (in string engineURL, in string iconURL, in wstring suggestedTitle, in wst
ring suggestedCategory); */
NS_IMETHODIMP
GSidebarProxy::AddSearchEngine (const char *engineURL,
                               const char *iconURL,
                               const PRUnichar *suggestedTitle,
                               const PRUnichar *suggestedCategory)
{
        return NS_ERROR_NOT_IMPLEMENTED;
}


#ifdef HAVE_NSISIDEBAR_ADDMICROSUMMARYGENERATOR
/* void addMicrosummaryGenerator (in string generatorURL); */
NS_IMETHODIMP
GSidebarProxy::AddMicrosummaryGenerator(const char *generatorURL)
{
        return NS_ERROR_NOT_IMPLEMENTED;
}
#endif
