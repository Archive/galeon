/* 
 *  Copyright (C) 2002 Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* This file is based on the original netscape.c file of galeon 1.x
 *
 * Netscape bookmark importing by Nate Case <nd@kracked.com>
 * Netscape bookmark exporting by Ricardo Fernández Pascual <ric@users.sourceforge.net>
 */

#include "bookmarks.h"

GbBookmarkSet *		netscape_import_bookmarks (const gchar *filename,
						   gboolean use_locale);
gboolean		netscape_export_bookmarks (const gchar *filename, GbBookmarkSet *set, 
						   gboolean use_locale);


