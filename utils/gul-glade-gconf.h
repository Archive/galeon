/*
 *  Copyright (C) 2003  Tommi Komulainen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation. 
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef GUL_GLADE_GCONF_H
#define GUL_GLADE_GCONF_H 1

#include <glade/glade-xml.h>

G_BEGIN_DECLS

void        gul_glade_gconf_connect_simple (GladeXML *gxml,
		                            const char *widget_name, ...);

void        gul_glade_gconf_connect_radio  (GladeXML *gxml,
		                            const char *gconf_key, 
					    const char *widget_name, ...);

G_END_DECLS

#endif /* GUL_GLADE_GCONF_H */
