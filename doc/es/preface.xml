<!--
<!DOCTYPE preface PUBLIC "-//GNOME//DTD DocBook PNG Variant V1.1//EN">
-->

  <preface id="introduction" lang ="es">
<!-- =============Introduction ============================= -->
    <title>Introducción</title> 

    <sect1 id="what"> 
      <title>¿Qué es Galeon?</title>
      <para>
	<application>Galeon</application> es un navegador web construido alrededor de
	Gecko (<ulink url="http://www.mozilla.org/">El motor de renderizado de <application>Mozilla</application></ulink>)
	y Necko (el motor de red de <application>Mozilla</application>).
	Es un navegador web para <ulink url="http://www.gnome.org/">GNOME</ulink>
	diseñado para aprovechar la ventaja de tantas tecnologías de GNOME como tenga sentido.
        Galeon fue escrito sólo para hacer una cosa - <emphasis>navegar por la web</emphasis>.
      </para>

      <para>
         Aquí hay una lista de algunas de las características de <application>Galeon</application>:
      </para>

      <para>
         Integración en GNOME
      </para>
	<para>
         <itemizedlist>
  	    <listitem>
               <para>Usa la apariencia (themes) y configuración global de GNOME</para>
            </listitem>
	    <listitem>
	       <para>Traducciones disponibles para muchos idiomas</para>
	    </listitem>
	    <listitem>
	       <para>Se usa CORBA para la automatización</para>
	    </listitem>
	    <listitem>
	       <para>Soporte Drag and Drop extensivo</para>
	    </listitem>
	 </itemizedlist>
	</para>    
      <para>
         Mozilla «powered»
      </para>
	<para>
	 <itemizedlist>
	    <listitem>
	       <para>Un motor de renderizado rápido y con todas las características</para>
	    </listitem>
	    <listitem>
	       <para>Suporta SSL, Java, Javascript y complementos (plugins)</para>
	    </listitem>
	 </itemizedlist>
	 </para>   
      <para>
         Características avanzadas para marcadores
      </para>
	<para>
	 <itemizedlist>
	    <listitem>
	       <para>Importa y exporta marcadores de Netscape / Mozilla / Epiphany y Konqueror</para>
	    </listitem>
	    <listitem>
	       <para>Puede mostrar varias barras de marcadores</para>
	    </listitem>
	    <listitem>
	       <para>Editor integrado</para>
	    </listitem>
	    <listitem>
	       <para>Alias para marcadores</para>
	    </listitem>
	    <listitem>
	       <para>Marcadores representados por imágenes</para>
	    </listitem>
	    <listitem>
	       <para>"Marcadores inteligentes" (marcadores con parámetros)</para>
	    </listitem>
	    <listitem>
	       <para>Página generada dinámicamente con todos sus marcadores
	       (My Portal)</para>
	    </listitem>
	 </itemizedlist>
	</para>   
      <para>
         Configurable
      </para>
	<para>
	 <itemizedlist>
	    <listitem>
	       <para>Visor con ventanas múltiples, una interfaz con solapas o una combinación de ambos</para>
	    </listitem>
	    <listitem>
	       <para>Modo de navegación a pantalla completa</para>
	    </listitem>
	    <listitem>
	       <para>Acciones de los botones del ratón configurables</para>
	    </listitem>
	    <listitem>
	       <para>Distribución, logotipo, colores y tipos personalizables</para>
	    </listitem>
	 </itemizedlist>
	</para>    
      <para>
         ¡Y más!
      </para>
	<para>
	 <itemizedlist>
	    <listitem>
	       <para>Autocompletado de URLs basado en el histórico</para>
	    </listitem>
	    <listitem>
	       <para>Recuperación de sesión automática</para>
	    </listitem>
	    <listitem>
	       <para>Filtro de cookies, publicidad, ventanas emergentes</para>
	    </listitem>
	    <listitem>
	       <para>Soporte "Favicon", permitiendo a <application>Galeon</application>
               descargar automáticamente y usar los iconos proporcionados por los sitios web</para>
	    </listitem>
	 </itemizedlist>
	</para>
      <para>
        Debido a que es parte del proyecto GNOME,
        <application>Galeon</application> es Software Libre.  El
        programa y su código fuente son liberados bajo los términos de la
        <ulink type="help" url="gnome-help:gpl"><citetitle>General
        Public License (GPL)</citetitle></ulink> de GNU, y la documentación
        bajo la <ulink type="help" url="gnome-help:fdl"><citetitle>Free Documentation
        License (FDL)</citetitle></ulink>. Para más información acerca de la GPL y
        la  FDL, visite el sitio web de la Free Software Foundation en <ulink url="http://www.fsf.org">http://www.fsf.org</ulink>.
      </para>
      <para>
        <note>
           <title>Asistencia técnica</title>
           <para>
             Se brinda asistencia técnica a través de la lista de correo <email>galeon-user@lists.sourceforge.net</email>.
             Puede informar de errores o pedir características nuevas a través del <ulink url="http://bugzilla.gnome.org">Bugzilla</ulink> de GNOME
	     o con <application>La herramienta de informe de errores</application> (conocida como 
             <command>bug-buddy</command> en la línea de órdenes).
          </para>
        </note>
      </para>

  </sect1>
  <sect1 id="aboutbook">
      <title>Acerca de este libro</title>
        <para>
	Esta versión de la Guía de Usuario de <application>Galeon</application>
	es un <emphasis>borrador</emphasis>. Describe la versión 1.3 del software.
        El contenido está sujeto a cambios, especialmente si usted ayuda.
	Envíe comentarios o sugerencias sobre esta guía a
	<email>galeon-user@list.sourceforge.net</email>. Y sobre su traducción al
        español a <email>traductores@es.gnome.org</email> Si quiere trabajar en la 
	guía original mande un correo a <email>galeon-devel@lists.sourceforge.net</email>.
      </para>
  
      <formalpara>
        <title>Convenciones tipográficas</title>
	<para>
          Algunas clases de palabras están resaltadas con tipografía especial:
          <simplelist>
	    <member><application>Aplicaciones</application></member>
	    <member><command>Comandos</command> tecleados en la línea de órdenes</member>
	    <member><guilabel>Etiquetas</guilabel> para los elementos de interfaz</member>

            <member> Las selecciones de menú tienen la apariencia siguiente:
                       <menuchoice>
                          <guimenu>Menú</guimenu>
                          <guisubmenu>Submenú</guisubmenu> 
                          <guimenuitem>Elemento de menú</guimenuitem>
                     </menuchoice>
            </member>
	    <member><guibutton>Botones</guibutton> en los que puede pulsar
            </member> <member><userinput>cualquiercosa que escriba </userinput></member>
           <member><computeroutput>Texto de salida de la computadora</computeroutput></member>
	  </simplelist>
        </para>
      </formalpara>
  </sect1>

<!-- maybe we should include something like this:
  <sect1 id="commontasks">
    <title>Quick Reference for Common Tasks</title>
    <para>
        You might want to get a copy of this section and tape it to
        the wall next to your computer: it's a very short summary of
        most of the things you'll want to do with
        <application>Evolution</application>, and pointers to the
        sections of the book where you'll find more in-depth
        description of those tasks.
    </para>
    <sect2 id="quickref-newthings">
      <title>Opening or Creating Anything</title>
      <para>
         Here are the keyboard shortcuts and menu items you're most
         likely to use: (Please note that the shortcuts listed are
         probably wrong at this point).
         
          <variablelist>

	  <varlistentry>
	    <term>Create a new folder</term>
	    <listitem>
	      <para>
                <menuchoice><guimenu>File</guimenu><guisubmenu>New</guisubmenu>
                <guimenuitem>Folder</guimenuitem></menuchoice> or
                <keycombo action="simul">
		  <keycap>Ctrl</keycap>
		  <keycap>Shift </keycap>
		  <keycap>F</keycap>
                </keycombo>
                </para>
	    </listitem>
	  </varlistentry>

	  <varlistentry>
	    <term>Create a new Shortcut in the Evolution Bar</term>
	    <listitem>
	      <para>       
                  <menuchoice><guimenu>File</guimenu>
		  <guisubmenu>New</guisubmenu>
		  <guimenuitem>Evolution Bar Shortcut</guimenuitem> </menuchoice> or 
                <keycombo action="simul">
		  <keycap>Ctrl</keycap>
		  <keycap>Shift</keycap>
		  <keycap>S</keycap>
                </keycombo>
                </para>
	    </listitem>
	  </varlistentry>

	  <varlistentry>
	    <term>Create a new email message:</term>
	    <listitem>
	      <para>
                Use
                <menuchoice><guimenu>File</guimenu><guisubmenu>New</guisubmenu><guimenuitem>Mail
                Message</guimenuitem>  </menuchoice> or 
                <keycombo action="simul">
		  <keycap>Ctrl</keycap>
		  <keycap>Shift </keycap>
		  <keycap>M</keycap>
                </keycombo>
              </para>
	    </listitem>
	  </varlistentry>

	  <varlistentry>
	    <term>Create a new Appointment</term>
	    <listitem>
	      <para>       
                  <menuchoice><guimenu>File</guimenu>
		  <guisubmenu>New</guisubmenu>
		  <guimenuitem>Appointment</guimenuitem> </menuchoice>  or 
                <keycombo action="simul">
		  <keycap>Ctrl</keycap>
		  <keycap>Shift</keycap>
		  <keycap>A</keycap>
                </keycombo>
                </para>
	    </listitem>
	  </varlistentry>

	  <varlistentry>
	    <term>Create a new Task</term>
	    <listitem>
	      <para>       
                  <menuchoice><guimenu>File</guimenu>
		  <guisubmenu>New</guisubmenu>
		  <guimenuitem>Task</guimenuitem>  </menuchoice> or 
                <keycombo action="simul">
		  <keycap>Ctrl</keycap>
		  <keycap>Shift</keycap>
		  <keycap>T</keycap>
                </keycombo>
                </para>
	    </listitem>
	  </varlistentry>

	  <varlistentry>
	    <term>Enter a new Contact</term>
	    <listitem>
	      <para>       
                  <menuchoice><guimenu>File</guimenu>
		  <guisubmenu>New</guisubmenu>
		  <guimenuitem>Contact</guimenuitem> </menuchoice>  or 
                <keycombo action="simul">
		  <keycap>Ctrl</keycap>
		  <keycap>Shift</keycap>
		  <keycap>C</keycap>
                </keycombo>
                </para>
	    </listitem>
	  </varlistentry>
	</variablelist>
      </para>
    </sect2> 
    <sect2 id="quickref-mail">
      <title>Mail Tasks</title>
      <para>
        Here are the most frequent email tasks, and shortcuts for navigating your mailbox with the keyboard instead of the mouse:
         <variablelist>
	  <varlistentry>
	    <term>Check Mail</term>
	    <listitem>
	      <para>
                Click <guibutton>Get Mail</guibutton> in the toolbar, or press 
                <keycombo action="simul">
		  <keycap>FIXME</keycap>
		  <keycap>FIXME</keycap>
		  <keycap>FIXME</keycap>
                </keycombo>
                </para>
	    </listitem>
	  </varlistentry>

          <varlistentry>
	    <term>Reply to a Message</term>
	    <listitem>
	      <para>
                To reply to the sender of the message only:
                click <guibutton>Reply</guibutton> in the
                toolbar, or press
                <keycombo action="simul">
		  <keycap>FIXME</keycap>
		  <keycap>FIXME</keycap>
		  <keycap>FIXME</keycap>
                </keycombo>
                </para>
	      <para>
                To reply to the sender and all the other visible
                recipients of the message, click:
              </para>
	    </listitem>
	  </varlistentry>
	  
          <varlistentry>
	    <term>Forward a Message</term>
	    <listitem>
	      <para>
                Select the message or messages you want to forward,
                and click <guibutton>Forward</guibutton> in the
                toolbar, or press
                <keycombo action="simul">
		  <keycap>FIXME</keycap>
		  <keycap>FIXME</keycap>
		  <keycap>FIXME</keycap>
                </keycombo>
                </para>
	    </listitem>
	  </varlistentry>

          <varlistentry>
	    <term>Open a Message in a New Window</term>
	    <listitem>
	      <para>
                Double-click the message you want to view, or select
                it and press
                <keycombo action="simul">
		  <keycap>Ctrl</keycap>
		  <keycap>O</keycap>
                </keycombo>
                </para>
	    </listitem>
	  </varlistentry>

          <varlistentry>
	    <term>Create Filters and vFolders</term>
	    <listitem>
	      <para>
                You can create filters and vFolders based on specific
                message attributes by right-clicking on a message or
                by selecting <menuchoice> <guimenu>Tools</guimenu>
                <guimenuitem>Mail Filters</guimenuitem> </menuchoice>
                or <menuchoice> <guimenu>Tools</guimenu>
                <guimenuitem>vFolder Editor</guimenuitem>
                </menuchoice>.  Filters are discussed in <xref
                linkend="usage-mail-organize-filters">, and vFolders
                in <xref linkend="usage-mail-organize-vfolders">.
            </para>
	    </listitem>
	</varlistentry>
	</variablelist>

        </para>
      </sect2>
      <sect2 id="quickref-cal">
        <title>Calendar</title>
        <para>
        <variablelist>
	  <varlistentry>
	    <term></term>
	    <listitem>
	      <para>
               </para>
	    </listitem>
	    </varlistentry>
	  </variablelist>

        </para>
      </sect2>
      <sect2 id="quickref-contact">
        <title>Contact Manager</title>
        <para>


        <variablelist>
	  <varlistentry>
	    <term></term>
	    <listitem>
	      <para>
               </para>
	    </listitem>
	    </varlistentry>
	  </variablelist>


        </para>
      </sect2>
    </sect1>
-->
  </preface>
