/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *  Copyright (C) 2003 Philip Langdale
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GALEON_POPUP_H
#define GALEON_POPUP_H

typedef struct GaleonPopup GaleonPopup;
typedef struct GaleonPopupPrivate GaleonPopupPrivate;

#include "galeon-window.h"
#include <gtk/gtkuimanager.h>
#include <glib-object.h>

G_BEGIN_DECLS

typedef struct GaleonPopupClass GaleonPopupClass;

#define GALEON_TYPE_POPUP             (galeon_popup_get_type ())
#define GALEON_POPUP(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GALEON_TYPE_POPUP, GaleonPopup))
#define GALEON_POPUP_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GALEON_TYPE_POPUP, GaleonPopupClass))
#define GALEON_IS_POPUP(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GALEON_TYPE_POPUP))
#define GALEON_IS_POPUP_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GALEON_POPUP))
#define GALEON_POPUP_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GALEON_TYPE_EMBED_POPUP, GaleonPopupClass))

struct GaleonPopup 
{
	GObject parent;
        GaleonPopupPrivate *priv;
};

struct GaleonPopupClass
{
        GObjectClass parent_class;
};


GType             	galeon_popup_get_type 		(void);
GaleonPopup *		galeon_popup_new		(GaleonWindow *window);

GaleonEmbed *		galeon_popup_get_embed		(GaleonPopup *p);

void		  	galeon_popup_set_event 		(GaleonPopup *p,
							 GaleonEmbedEvent *event);

GaleonEmbedEvent *	galeon_popup_get_event		(GaleonPopup *p);

GaleonWindow *          galeon_popup_get_window         (GaleonPopup *p);

void              	galeon_popup_show		(GaleonPopup *p,
							 GaleonEmbed *embed);

G_END_DECLS

#endif
