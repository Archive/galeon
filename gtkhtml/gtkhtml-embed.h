/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GTKHTML_EMBED_H
#define GTKHTML_EMBED_H

#ifdef __cplusplus
extern "C" {
#endif

#include "galeon-embed-types.h"
#include "galeon-embed.h"
	
#include <gtk/gtkscrolledwindow.h>
	
typedef struct GtkhtmlEmbedClass GtkhtmlEmbedClass;

#define GTKHTML_TYPE_EMBED             (gtkhtml_embed_get_type ())
#define GTKHTML_EMBED(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTKHTML_TYPE_EMBED, GtkhtmlEmbed))
#define GTKHTML_EMBED_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GTKHTML_EMBED, GtkhtmlEmbedClass))
#define GTKHTML_IS_EMBED(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTKHTML_TYPE_EMBED))
#define GTKHTML_IS_EMBED_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GTKHTML_EMBED))
#define GTKHTML_EMBED_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GTKHTML_TYPE_EMBED, GtkhtmlEmbedClass))

typedef struct GtkhtmlEmbed GtkhtmlEmbed;
typedef struct GtkhtmlEmbedPrivate GtkhtmlEmbedPrivate;

struct GtkhtmlEmbed
{
        GtkScrolledWindow parent;
        GtkhtmlEmbedPrivate *priv;
};

struct GtkhtmlEmbedClass
{
        GtkScrolledWindowClass parent_class;
};

GType gtkhtml_embed_get_type (void);

#ifdef __cplusplus
}
#endif
#endif
