/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __bookmarks_gtk_menu_item_h
#define __bookmarks_gtk_menu_item_h

#include <gtk/gtkimagemenuitem.h>
#include <gtk/gtkstatusbar.h>
#include "bookmarks.h"
#include "bookmarks-util.h"
#include "bookmarks-location-source.h"

#define GB_GTK_MENU_ITEM_NAME_MAX_LENGTH 40 /** Max length of a bookmark menu item */
#define GB_GTK_MENU_ITEM_TIP_MAX_LENGTH 30 /** Max length of a bookmark url tooltip */


/* object forward declarations */

typedef struct _GbGtkMenuItem GbGtkMenuItem;
typedef struct _GbGtkMenuItemClass GbGtkMenuItemClass;
typedef struct _GbGtkMenuItemPrivate GbGtkMenuItemPrivate;

/**
 * GbGtkMenuItem object
 */

#define GB_TYPE_GTK_MENU_ITEM		(gb_gtk_menu_item_get_type())
#define GB_GTK_MENU_ITEM(object)	(G_TYPE_CHECK_INSTANCE_CAST((object), GB_TYPE_GTK_MENU_ITEM,\
					 GbGtkMenuItem))
#define GB_GTK_MENU_ITEM_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), GB_TYPE_GTK_MENU_ITEM,\
					 GbGtkMenuItemClass))
#define GB_IS_GTK_MENU_ITEM(object)	(G_TYPE_CHECK_INSTANCE_TYPE((object), GB_TYPE_GTK_MENU_ITEM))
#define GB_IS_GTK_MENU_CLASS(klass) 	(G_TYPE_CHECK_CLASS_TYPE((klass), GB_TYPE_GTK_MENU_ITEM))
#define GB_GTK_MENU_ITEM_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), GB_TYPE_GTK_MENU_ITEM,\
					 GbGtkMenuItemClass))

struct _GbGtkMenuItemClass
{
	GtkImageMenuItemClass parent_class;

	/* signals */
	GbBookmarkActivatedCallback gb_gtk_menu_item_bookmark_activated;

};

/* Remember: fields are public read-only */
struct _GbGtkMenuItem
{
	GtkImageMenuItem parent_object;
	
	GbGtkMenuItemPrivate *priv;
};

GType		gb_gtk_menu_item_get_type			(void);
GbGtkMenuItem *	gb_gtk_menu_item_new				(GbBookmark *bookmark);
void		gb_gtk_menu_item_set_location_source		(GbGtkMenuItem *gm, GbLocationSource *src);
void 		gb_gtk_menu_item_fill_submenu			(GbGtkMenuItem *bmi, gboolean children);
void		gb_gtk_menu_item_set_statusbar			(GbGtkMenuItem *gm, GtkStatusbar *sb);

#endif
