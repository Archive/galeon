/*
 *  Copyright (C) 2002 Jorn Baayen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef SESSION_H
#define SESSION_H

#define SESSION_CRASHED "type:session_crashed"
#define SESSION_SAVED "type:session_saved"
#define SESSION_GNOME "type:session_gnome"

#include "galeon-window.h"

G_BEGIN_DECLS

#include <glib-object.h>
#include <glib.h>

typedef struct Session Session;
typedef struct SessionClass SessionClass;

#define TYPE_SESSION             (session_get_type ())
#define SESSION(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_SESSION, Session))
#define SESSION_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), SESSION, SessionClass))
#define IS_SESSION(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_SESSION))
#define IS_SESSION_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), SESSION))

typedef struct SessionPrivate SessionPrivate;

typedef enum {
   SESSION_OK,
   SESSION_RESUMED,
   SESSION_ABORT,
} SessionResumeType;

typedef enum {
   GALEON_SESSION_CMD_LOAD_SESSION,
   GALEON_SESSION_CMD_OPEN_URIS,
   GALEON_SESSION_QUIT,
} GaleonSessionCommand;

struct Session
{
        GObject parent;
        SessionPrivate *priv;
};

struct SessionClass
{
        GObjectClass parent_class;

	void ( *add_recent_session) (Session *session, 
				     char *filename);
	void ( *new_window)         (Session *session, 
				     GaleonWindow *window);
	void ( *close_window)       (Session *session);
};

GType         session_get_type 		(void);

Session      *session_new      		(void);

void	      session_close		(Session *session);

void	      session_load     		(Session *session,
					 const char *filename,
					 guint32 user_time);

void	      session_save     		(Session *session,
					 const char *filename);

SessionResumeType session_autoresume   	(Session *session);

GList	     *session_get_windows     	(Session *session);

void          session_add_window      	(Session *session,
					 GaleonWindow *window);

void          session_remove_window     (Session *session,
					 GaleonWindow *window);
void 	      galeon_session_queue_command	(Session *session,
					 GaleonSessionCommand op,
					 GValue *arg,
					 char **args,
					 guint32 user_time,
					 gboolean priority);

G_END_DECLS

#endif
