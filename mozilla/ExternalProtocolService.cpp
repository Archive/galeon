/*
 *  Copyright (C) 2001 Philip Langdale
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <nsCOMPtr.h>
#include <nsIURI.h>
#include <nsIDOMWindow.h>
#include <nsIWindowWatcher.h>
#include <nsComponentManagerUtils.h>
#include <nsServiceManagerUtils.h>
#include <nsMemory.h>
#include "AutoJSContextStack.h"

#include <libgnome/gnome-exec.h>
#include <libgnome/gnome-url.h>
#include <glib/gi18n.h>
#include <gtk/gtkdialog.h>
#include <gtk/gtkmessagedialog.h>

#include "ExternalProtocolService.h"
#include "GulString.h"

#include "prefs-strings.h"
#include "eel-gconf-extensions.h"

#define WINDOWWATCHER_CONTRACTID "@mozilla.org/embedcomp/window-watcher;1"

/* Implementation file */
NS_IMPL_ISUPPORTS1(GExternalProtocolService, nsIExternalProtocolService)

GExternalProtocolService::GExternalProtocolService()
{
  /* member initializers and constructor code */
}

GExternalProtocolService::~GExternalProtocolService()
{
  /* destructor code */
}

/* boolean externalProtocolHandlerExists (in string aProtocolScheme); */
NS_IMETHODIMP GExternalProtocolService::
		ExternalProtocolHandlerExists(const char *aProtocolScheme,
					      PRBool *_retval)
{
	NS_ENSURE_ARG_POINTER(_retval);
	*_retval = PR_FALSE;

	if (!aProtocolScheme) return NS_ERROR_NULL_POINTER;

	if (!*aProtocolScheme) return NS_ERROR_INVALID_ARG;

	/* build the config key */
	char *cmd_key = g_strconcat ("/desktop/gnome/url-handlers/",
				     aProtocolScheme,
				     "/command", NULL);

	char *enabled_key = g_strconcat ("/desktop/gnome/url-handlers/",
					 aProtocolScheme,
					 "/enabled", NULL);

	char *cmd = eel_gconf_get_string (cmd_key);
	if (cmd && cmd[0] && eel_gconf_get_boolean (enabled_key))
	{
		*_retval = PR_TRUE;
	}

	g_free (cmd_key);
	g_free (enabled_key);
	g_free (cmd);

	return NS_OK;
}

#if defined(HAVE_NSIEXTERNALPROTOCOLSERVICE_LOADURI)
NS_IMETHODIMP GExternalProtocolService::LoadURI(nsIURI *aURI, nsIPrompt *)
{
	/* Under some circumstances, the mozilla implementation of
	 * this asks the user whether they want to open the URI, e.g
	 * webcal urls. I'm not convinced we need to bother with that
	 * on linux:
	 *  http://bugzilla.mozilla.org/show_bug.cgi?id=263546 */
	return LoadUrl (aURI);
}
#elif defined(HAVE_NSIEXTERNALPROTOCOLSERVICE_LOADURI_WITH_IR)
NS_IMETHODIMP GExternalProtocolService::LoadURI(nsIURI *aURI, nsIInterfaceRequestor *aWindowContext)
{
	return LoadUrl (aURI);
}
#endif

/* void loadUrl (in nsIURI aURL); */
NS_IMETHODIMP GExternalProtocolService::LoadUrl(nsIURI *aURL)
{
	GulCString cSpec;
	aURL->GetSpec (cSpec);
	GulCString cScheme;
	aURL->GetScheme (cScheme);

	/* XXX: I don't think that this code is ever actually
	 * used ... - crispin */
	if (cScheme.Equals("http"))
	{
		nsresult rv;
		nsCOMPtr<nsIWindowWatcher> ww;
		ww = do_GetService(WINDOWWATCHER_CONTRACTID, &rv);
		if (NS_SUCCEEDED(rv))
		{
			nsCOMPtr<nsIDOMWindow> newWin;
			rv = ww->OpenWindow(nsnull, cSpec.get(),
					    nsnull, nsnull, nsnull,
					    getter_AddRefs(newWin));
			if (NS_SUCCEEDED(rv)) return NS_OK;
		}
	}

	/* build the config key */
	char *key = g_strconcat ("/desktop/gnome/url-handlers/",
				 cScheme.get(),
				 "/command", NULL);
	/* find it */
	char *result = eel_gconf_get_string (key);
	g_free (key);

	if (result)
	{
		gnome_url_show(cSpec.get(), NULL);
		g_free (result);
		return NS_OK;
	}

	nsresult rv;
	AutoJSContextStack stack;
	rv = stack.Init ();
	if (NS_FAILED (rv)) return rv;

	GtkWidget *dialog;

	/* throw the error */
	dialog = gtk_message_dialog_new (NULL, (GtkDialogFlags)0, 
					 GTK_MESSAGE_ERROR, GTK_BUTTONS_OK,
					 _("Galeon cannot handle this protocol,\n"
					   "and no GNOME default handler is set"));
	gtk_dialog_run (GTK_DIALOG(dialog));
	gtk_widget_destroy (dialog);

	/* don't let mozilla try blindly */
	return NS_ERROR_FAILURE;
}

NS_IMETHODIMP GExternalProtocolService::IsExposedProtocol(const char *aProtocolScheme, 
							  PRBool *_retval)
{
	*_retval = PR_TRUE;

	return NS_OK;
}

NS_IMETHODIMP GExternalProtocolService::GetApplicationDescription(const nsACString & aScheme, 
								  nsAString & _retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

#ifdef HAVE_NSIEXTERNALPROTOCOLSERVICE_GETPROTOCOLHANDLERINFO
NS_IMETHODIMP GExternalProtocolService::GetProtocolHandlerInfo(const nsACString & aProtocolScheme, nsIHandlerInfo **_retval)
{
	return NS_ERROR_NOT_IMPLEMENTED;
}
#endif

NS_IMETHODIMP GExternalProtocolService::GetProtocolHandlerInfoFromOS(const nsACString & aProtocolScheme, PRBool*, nsIHandlerInfo**)
{
	return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP GExternalProtocolService::SetProtocolHandlerDefaults(nsIHandlerInfo*, int)
{
	return NS_ERROR_NOT_IMPLEMENTED;
}
