Galeon - gecko-based GNOME web browser README

MANIFESTO
=========

A web browser is more than an application, it is a way of thinking, it is
a way of seeing the world. Galeon's principles are simplicity and standards
compliance.

Simplicity:

While Mozilla has an excellent rendering engine, its default
XUL-based interface is considered to be overcrowded and bloated.  Furthermore,
on slower processors even trivial tasks such as pulling down a menu is less
than responsive.

Galeon aims to utilize the simplest interface possible for a browser.  Keep
in mind that simple does not necessarily mean less powerful.  We believe
the commonly used browsers of today are too big, buggy, and bloated.  Galeon
addresses simplicity with a small browser designed for the web -- not mail,
newsgroups, file management, instant messenging or coffee making.  The UNIX
philosophy is to design small tools that do one thing, and do it well.

Galeon also address simplicity with modularity to make a light and powerful
application.  If something can be implemented using external applications
or components, we use it rather than wasting resources in the web browser.
Integration will be achived with CORBA, Bonobo, and the ever popular
command line.  

Mail will be handled with your favorite e-mail application (Evolution, pine,
mutt, balsa, pronto, whatever).

Standards compliance:

The introduction of non-standard features in browsers could make it difficult
or impossible to use alternative products like Galeon if developers embrace
them.  Alternative (standards complying) browsers could not be able to
fully access web sites making use of these features.  The success of
non-standard features can ultimately lead to forcing one browser, on
one platform to dominate the market.

Standards compliance ensures the freedom of choice.  Galeon aims to achieve
this.


Web Resources
=============

Galeon's homepage is at:

    http://galeon.sourceforge.net/
    
Be sure to visit this site for information, documentation, news, etc.

The latest version of Galeon can be found at:

    http://sourceforge.net/projects/galeon/
    
For information on the Mozilla project on which Galeon is based, see:

    http://www.mozilla.org/
    
IRC
===

If there's something you just can't find out elsewhere, you want to 
give feedback directly to the authors or you're just bored, visit 
#galeon on Byxnet (irc.gimp.org:6667).

Get Support
===========

Galeon-user mailing list:

	http://lists.sourceforge.net/mailman/listinfo/galeon-user


Help development
================

1. SUBMITTING PATCHES

The Galeon source is constantly changing.  If you want to submit a patch,
please do so from the most recent CVS source tree, subscribe to the
galeon-devel mailing list by visiting:

	http://lists.sourceforge.net/mailman/listinfo/galeon-devel

and post your patch with a description of functionality.
You can also attach patches to bugs on 

	http://bugzilla.gnome.org

2. TRANSLATIONS
Subscribe to galeon-devel (see above url) and send here translations.

3. BUGS
Naturally Galeon is far from perfect, so if you find any bugs, please 
report them to:

    http://bugzilla.gnome.org

Please make sure that what you're reporting is actually a BUG and not
a problem on your end.
Specify galeon version, distribution, mozilla and gnome packages version.

4. SUGGESTIONS
Subscribe to galeon-devel and give us your suggestions.
