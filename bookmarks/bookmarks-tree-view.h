/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __bookmarks_tree_view_h
#define __bookmarks_tree_view_h

#include <glib-object.h>
#include <gtk/gtktreeview.h>
#include "bookmarks-tree-model.h"
#include "bookmarks-location-source.h"
#include "bookmarks-util.h"

/* object forward declarations */

typedef struct _GbTreeView GbTreeView;
typedef struct _GbTreeViewClass GbTreeViewClass;
typedef struct _GbTreeViewPrivate GbTreeViewPrivate;

/**
 * TreeView object
 */

#define GB_TYPE_TREE_VIEW		(gb_tree_view_get_type())
#define GB_TREE_VIEW(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), GB_TYPE_TREE_VIEW,\
					 GbTreeView))
#define GB_TREE_VIEW_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), GB_TYPE_TREE_VIEW,\
					 GbTreeViewClass))
#define GB_IS_TREE_VIEW(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), GB_TYPE_TREE_VIEW))
#define GB_IS_TREE_VIEW_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), GB_TYPE_TREE_VIEW))
#define GB_TREE_VIEW_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), GB_TYPE_TREE_VIEW,\
					 GbTreeViewClass))

struct _GbTreeViewClass 
{
	GtkTreeViewClass parent_class;

	/* signals */

	GbBookmarkActivatedCallback bookmark_activated;
	void (*bookmark_check_toggled)	(GbTreeView *tv, GbBookmark *b); 
	/* this signal should return TRUE to stop processing the double click */
	gboolean (*bookmark_double_clicked)	(GbTreeView *tv, GbBookmark *b); 
};

/* Remember: fields are public read-only */
struct _GbTreeView
{
	GtkTreeView parent_object;

	GbTreeViewPrivate *priv;
};

typedef enum
{
	GB_TV_NONE,
	GB_TV_EDIT,
	GB_TV_ACTIVATE,
	GB_TV_ACTIVATE_NEW_WINDOW
} GbTreeViewAction;

GType		gb_tree_view_get_type			(void);
GbTreeView *	gb_tree_view_new			(void);
void		gb_tree_view_set_model			(GbTreeView *treeView, GbTreeModel *model);
void		gb_tree_view_select			(GbTreeView *e, GbBookmark *b);
void		gb_tree_view_select_add			(GbTreeView *e, GbBookmark *b);
void		gb_tree_view_select_list		(GbTreeView *tv, const GSList *l);
void		gb_tree_view_select_list_add		(GbTreeView *tv, const GSList *l);
void		gb_tree_view_ensure_selected		(GbTreeView *e, GbBookmark *b);
void		gb_tree_view_ensure_selected_add	(GbTreeView *e, GbBookmark *b);
void		gb_tree_view_ensure_expanded		(GbTreeView *e, GbBookmark *b);
const GSList *	gb_tree_view_get_selected_list		(GbTreeView *tv);
GSList *	gb_tree_view_get_selected_list_prepared	(GbTreeView *tv);
void		gb_tree_view_set_double_click_action	(GbTreeView *tv, GbTreeViewAction a);
void		gb_tree_view_set_location_visible	(GbTreeView *tv, gboolean val);
void		gb_tree_view_set_check_visible		(GbTreeView *tv, gboolean val);
void		gb_tree_view_set_autoexpand_roots	(GbTreeView *tv, gboolean val);
void		gb_tree_view_set_location_source	(GbTreeView *tv, GbLocationSource *s);

#endif

