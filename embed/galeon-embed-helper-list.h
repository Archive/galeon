/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GALEON_EMBED_HELPER_LIST_H
#define GALEON_EMBED_HELPER_LIST_H

#include "galeon-embed.h"

#include <glib-object.h>
#include <glib.h>
#include <gtk/gtkmenushell.h>
	
G_BEGIN_DECLS

typedef struct GaleonEmbedHelperListClass GaleonEmbedHelperListClass;

#define GALEON_TYPE_EMBED_HELPER_LIST             (galeon_embed_helper_list_get_type ())
#define GALEON_EMBED_HELPER_LIST(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GALEON_TYPE_EMBED_HELPER_LIST, GaleonEmbedHelperList))
#define GALEON_EMBED_HELPER_LIST_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GALEON_TYPE_EMBED_HELPER_LIST, GaleonEmbedHelperListClass))
#define GALEON_IS_EMBED_HELPER_LIST(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GALEON_TYPE_EMBED_HELPER_LIST))
#define GALEON_IS_EMBED_HELPER_LIST_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GALEON_EMBED_HELPER_LIST))
#define GALEON_EMBED_HELPER_LIST_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GALEON_TYPE_EMBED_HELPER_LIST, GaleonEmbedHelperListClass))

typedef struct GaleonEmbedHelperList GaleonEmbedHelperList;
typedef struct GaleonEmbedHelperListPrivate GaleonEmbedHelperListPrivate;

struct GaleonEmbedHelperList 
{
        GObject parent;
        GaleonEmbedHelperListPrivate *priv;
};

struct GaleonEmbedHelperListClass
{
        GObjectClass parent_class;
};

GType         	   	galeon_embed_helper_list_get_type  	(void);

GaleonEmbedHelperList  *galeon_embed_helper_list_new	     	(void);

void		   	galeon_embed_helper_list_set_embed	(GaleonEmbedHelperList *hl,
							 	 GaleonEmbed *embed);

gboolean		galeon_embed_helper_list_set_uri	(GaleonEmbedHelperList *hl,
								 const char *uri);

void			galeon_embed_helper_list_add_mime_type	(GaleonEmbedHelperList *hl,
								 const char *mime_type);

void			galeon_embed_helper_list_add_to_gtk_menu (GaleonEmbedHelperList *hl,
								  GtkMenuShell *ms);

G_END_DECLS

#endif

