/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __io_mozilla_h
#define __io_mozilla_h

#include <glib.h>
#include <glib-object.h>
#include "bookmarks-io.h"
#include "bookmarks.h"

/* object forward declarations */

typedef struct _GbIOMozilla GbIOMozilla;
typedef struct _GbIOMozillaClass GbIOMozillaClass;

/**
 * IO_MOZILLA object
 */

#define GB_TYPE_IO_MOZILLA		(gb_io_mozilla_get_type())
#define GB_IO_MOZILLA(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), GB_TYPE_IO_MOZILLA, \
					 GbIOMozilla))
#define GB_IO_MOZILLA_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), GB_TYPE_IO_MOZILLA, \
					 GbIOMozillaClass))
#define GB_IS_IO_MOZILLA(object)	(G_TYPE_CHECK_INSTANCE_TYPE((object), GB_TYPE_IO_MOZILLA))
#define GB_IS_IO_MOZILLA_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), GB_TYPE_IO_MOZILLA))
#define GB_IO_MOZILLA_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), GB_TYPE_IO_MOZILLA, \
					 GbIOMozillaClass))

struct _GbIOMozillaClass 
{
	GbIOClass parent_class;
};

struct _GbIOMozilla
{
	GbIO parent_object;
};

GType			gb_io_mozilla_get_type			(void);
GbIOMozilla *		gb_io_mozilla_new			(void);

#endif
