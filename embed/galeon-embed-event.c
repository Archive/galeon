/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "galeon-embed-event.h"

#include <glib/gi18n.h>
#include <glib/ghash.h>
#include <gtk/gtktypeutils.h>

#define GALEON_EMBED_EVENT_GET_PRIVATE(object) (G_TYPE_INSTANCE_GET_PRIVATE ((object), \
				       GALEON_TYPE_EMBED_EVENT, GaleonEmbedEventPrivate))


struct GaleonEmbedEventPrivate
{
	GHashTable *props;
};


static void galeon_embed_event_finalize (GObject *object);

G_DEFINE_TYPE (GaleonEmbedEvent, galeon_embed_event, G_TYPE_OBJECT);


static void
galeon_embed_event_class_init (GaleonEmbedEventClass *klass)
{
        GObjectClass *object_class = G_OBJECT_CLASS (klass);

        object_class->finalize = galeon_embed_event_finalize;

	g_type_class_add_private (klass, sizeof (GaleonEmbedEventPrivate));
}

static void
free_g_value (gpointer value)
{
	g_value_unset (value);
	g_free (value);
}

static void
galeon_embed_event_init (GaleonEmbedEvent *event)
{
        event->priv = GALEON_EMBED_EVENT_GET_PRIVATE (event);

	event->priv->props = 
		g_hash_table_new_full (g_str_hash, g_str_equal, 
				       g_free, free_g_value);
}

static void
galeon_embed_event_finalize (GObject *object)
{
        GaleonEmbedEvent *event;

        event = GALEON_EMBED_EVENT (object);

	g_hash_table_destroy (event->priv->props);
	
        G_OBJECT_CLASS (galeon_embed_event_parent_class)->finalize (object);
}


GaleonEmbedEvent *
galeon_embed_event_new (void)
{
	GaleonEmbedEvent *event;

        event = GALEON_EMBED_EVENT (g_object_new (GALEON_TYPE_EMBED_EVENT, NULL));

        g_return_val_if_fail (event->priv != NULL, NULL);

        return event;
}

guint
galeon_embed_event_get_modifier (GaleonEmbedEvent *event)
{
	g_return_val_if_fail (GALEON_IS_EMBED_EVENT (event), 0);

	return event->modifier;
}

guint
galeon_embed_event_get_mouse_button (GaleonEmbedEvent *event)
{
	g_return_val_if_fail (GALEON_IS_EMBED_EVENT (event), 0);

	return event->mouse_button;
}

void
galeon_embed_event_get_coords	(GaleonEmbedEvent *event,
				 guint *x, guint *y)
{
	g_return_if_fail (GALEON_IS_EMBED_EVENT (event));

	*x = event->x;
	*y = event->y;
}

EmbedEventContext
galeon_embed_event_get_context (GaleonEmbedEvent *event)
{
	g_return_val_if_fail (GALEON_IS_EMBED_EVENT (event), 0);

	return event->context;
}

void 
galeon_embed_event_set_property (GaleonEmbedEvent *event,
				  const char *name,
				  GValue *value)
{
	g_return_if_fail (GALEON_IS_EMBED_EVENT (event));

	g_hash_table_insert (event->priv->props, 
                             g_strdup (name),
                             value);
}

const GValue *
galeon_embed_event_get_property	(GaleonEmbedEvent *event,
				 const char *name)
{
	g_return_val_if_fail (GALEON_IS_EMBED_EVENT (event), NULL);
	
	return g_hash_table_lookup (event->priv->props, name);
}

gboolean
galeon_embed_event_has_property	(GaleonEmbedEvent *event,
				 const char *name)
{
	gpointer tmp;

	g_return_val_if_fail (GALEON_IS_EMBED_EVENT (event), FALSE);
	
	tmp = g_hash_table_lookup (event->priv->props,
	   			   name);

	return tmp != NULL;
}
