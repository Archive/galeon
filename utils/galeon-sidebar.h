/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GALEON_SIDEBAR_H
#define GALEON_SIDEBAR_H

#include <glib-object.h>
#include <glib.h>
#include <gtk/gtknotebook.h>
#include <gtk/gtkvbox.h>
	
G_BEGIN_DECLS

typedef struct GaleonSidebarClass GaleonSidebarClass;

#define GALEON_TYPE_SIDEBAR             (galeon_sidebar_get_type ())
#define GALEON_SIDEBAR(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GALEON_TYPE_SIDEBAR, GaleonSidebar))
#define GALEON_SIDEBAR_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GALEON_TYPE_SIDEBAR, GaleonSidebarClass))
#define GALEON_IS_SIDEBAR(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GALEON_TYPE_SIDEBAR))
#define GALEON_IS_SIDEBAR_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GALEON_SIDEBAR))
#define GALEON_SIDEBAR_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GALEON_TYPE_SIDEBAR, GaleonSidebarClass))

typedef struct GaleonSidebar GaleonSidebar;
typedef struct GaleonSidebarPrivate GaleonSidebarPrivate;

struct GaleonSidebar 
{
        GtkVBox parent;
        GaleonSidebarPrivate *priv;
};

struct GaleonSidebarClass
{
        GtkVBoxClass parent_class;

	void (* close_requested) (GaleonSidebar *sidebar);
	
	void (* page_changed) (GaleonSidebar *sidebar,
			       const char *page_id);
	
	void (* remove_requested) (GaleonSidebar *sidebar,
			           const char *page_id);
};

GType          galeon_sidebar_get_type          (void);

GtkWidget     *galeon_sidebar_new	        (void);

void           galeon_sidebar_add_page	        (GaleonSidebar *sidebar,
					         const char *title,
					         const char *page_id,
						 gboolean can_remove);

gboolean       galeon_sidebar_remove_page	(GaleonSidebar *sidebar,
						 const char *page_id);

gboolean       galeon_sidebar_select_page 	(GaleonSidebar *sidebar,
			    			 const char *page_id);

void           galeon_sidebar_set_content       (GaleonSidebar *sidebar,
						 GObject *content);


G_END_DECLS

#endif

