/*
 *  Copyright (C) 2002 Philip Langdale
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __sidebarproxy_h
#define __sidebarproxy_h

#include <nsISidebar.h>
#include <nsError.h>

#define G_SIDEBAR_CLASSNAME \
 "Galeon's Sidebar Implementation"
// {7e38cd39-b90d-4756-979c-18ea531e3bea}
#define G_SIDEBAR_CID \
{ 0x7e38cd39, 0xb90d, 0x4756, { 0x97, 0x9c, 0x18, 0xea, 0x53, 0x1e, 0x3b, 0xea } }

class GSidebarProxy : public nsISidebar
{
  public:
	NS_DECL_ISUPPORTS
	NS_DECL_NSISIDEBAR

	GSidebarProxy();
	virtual ~GSidebarProxy();
  private:
};

#endif //__sidebarproxy_h

