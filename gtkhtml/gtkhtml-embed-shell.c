/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "gtkhtml-embed-shell.h"

static void
gtkhtml_embed_shell_class_init (GtkhtmlEmbedShellClass *klass);
static void
gtkhtml_embed_shell_init (GtkhtmlEmbedShell *ges);
static void
gtkhtml_embed_shell_finalize (GObject *object);

static void              
impl_get_capabilities (GaleonEmbedShell *shell,
		       EmbedShellCapabilities *caps);
static gresult      
impl_clear_cache (GaleonEmbedShell *shell,
		  CacheType type);
static gresult          
impl_set_offline_mode (GaleonEmbedShell *shell,
		       gboolean offline);
static gresult           
impl_load_proxy_autoconf (GaleonEmbedShell *shell,
			  const char* url);
static gresult           
impl_show_java_console (GaleonEmbedShell *shell);
static gresult           
impl_shell_show_js_console (GaleonEmbedShell *shell);
static gresult           
impl_get_charset_titles (GaleonEmbedShell *shell,
			 const char *group,
		         GList **charsets);
static gresult           
impl_get_charset_groups (GaleonEmbedShell *shell,
		   	 GList **groups);
static gresult
impl_get_font_list (GaleonEmbedShell *shell,
		    const char *langGroup,
		    const char *fontType,
		    GList **fontList,
		    char **default_font);
static gresult           
impl_set_permission (GaleonEmbedShell *shell,
		     const char *url, 
		     PermissionType type,
		     gboolean allow);
static gresult           
impl_list_permissions (GaleonEmbedShell *shell,
		       PermissionType type, 
		       GList **permissions);
static gresult           
impl_remove_permissions (GaleonEmbedShell *shell,
		         PermissionType type,
		         GList *permissions);
static gresult           
impl_list_cookies (GaleonEmbedShell *shell,
		   GList **cookies);
static gresult           
impl_remove_cookies (GaleonEmbedShell *shell,
		     GList *cookies,
		     gboolean block);
static gresult           
impl_list_passwords (GaleonEmbedShell *shell,
		     PasswordType type, 
		     GList **passwords);
static gresult           
impl_remove_passwords (GaleonEmbedShell *shell,
		       GList *passwords,
		       PasswordType type);
static gresult 
impl_show_file_picker (GaleonEmbedShell *shell,
		       GtkWidget *parentWidget, 
		       const char *title,
		       const char *directory,
		       const char *file, 
		       FilePickerMode mode,
                       char **ret_fullpath, 
		       gboolean *ret_save_content, 
                       FileFormat *file_formats, 
		       int *ret_file_format);

struct GtkhtmlEmbedShellPrivate
{
};

static GObjectClass *parent_class = NULL;

GType
gtkhtml_embed_shell_get_type (void)
{
       static GType gtkhtml_embed_shell_type = 0;

        if (gtkhtml_embed_shell_type == 0)
        {
                static const GTypeInfo our_info =
                {
                        sizeof (GtkhtmlEmbedShellClass),
                        NULL, /* base_init */
                        NULL, /* base_finalize */
                        (GClassInitFunc) gtkhtml_embed_shell_class_init,
                        NULL, /* class_finalize */
                        NULL, /* class_data */
                        sizeof (GtkhtmlEmbedShell),
                        0,    /* n_preallocs */
                        (GInstanceInitFunc) gtkhtml_embed_shell_init
                };

                gtkhtml_embed_shell_type = g_type_register_static (GALEON_TYPE_EMBED_SHELL,
								   "GtkhtmlEmbedShell",
								   &our_info, (GTypeFlags)0);
        }

        return gtkhtml_embed_shell_type;
}

static void
gtkhtml_embed_shell_class_init (GtkhtmlEmbedShellClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GaleonEmbedShellClass *shell_class;
	
	parent_class = (GObjectClass *)
		gtk_type_class (GTKHTML_TYPE_EMBED_SHELL);
	shell_class = (GaleonEmbedShellClass *) parent_class;
	
        object_class->finalize = gtkhtml_embed_shell_finalize;

	shell_class->get_capabilities = impl_get_capabilities;
	shell_class->clear_cache = impl_clear_cache;
	shell_class->set_offline_mode = impl_set_offline_mode;
	shell_class->load_proxy_autoconf = impl_load_proxy_autoconf;
	shell_class->show_java_console = impl_show_java_console;
	shell_class->show_js_console = impl_shell_show_js_console;
	shell_class->get_charset_titles = impl_get_charset_titles;
	shell_class->get_charset_groups = impl_get_charset_groups;
	shell_class->get_font_list = impl_get_font_list;
	shell_class->set_permission = impl_set_permission;
	shell_class->list_permissions = impl_list_permissions;
	shell_class->remove_permissions = impl_remove_permissions;
	shell_class->list_cookies = impl_list_cookies;
	shell_class->remove_cookies = impl_remove_cookies;
	shell_class->list_passwords = impl_list_passwords;
	shell_class->remove_passwords = impl_remove_passwords;
	shell_class->show_file_picker = impl_show_file_picker;
}

static void
gtkhtml_embed_shell_init (GtkhtmlEmbedShell *mes)
{
        mes->priv = g_new0 (GtkhtmlEmbedShellPrivate, 1);
}

static void
gtkhtml_embed_shell_finalize (GObject *object)
{
	GtkhtmlEmbedShell *mes;

        g_return_if_fail (object != NULL);
        g_return_if_fail (GTKHTML_IS_EMBED_SHELL (object));

        mes = GTKHTML_EMBED_SHELL (object);

        g_return_if_fail (mes->priv != NULL);

        g_free (mes->priv);

        G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void              
impl_get_capabilities (GaleonEmbedShell *shell,
		       EmbedShellCapabilities *caps)
{
	*caps = 0;
}

static gresult      
impl_clear_cache (GaleonEmbedShell *shell,
		  CacheType type)
{
	return G_NOT_IMPLEMENTED;
}

static gresult          
impl_set_offline_mode (GaleonEmbedShell *shell,
		       gboolean offline)
{
	return G_NOT_IMPLEMENTED;
}

static gresult           
impl_load_proxy_autoconf (GaleonEmbedShell *shell,
			  const char* url)
{
	return G_NOT_IMPLEMENTED;
}

static gresult           
impl_show_java_console (GaleonEmbedShell *shell)
{
	return G_NOT_IMPLEMENTED;
}

static gresult           
impl_shell_show_js_console (GaleonEmbedShell *shell)
{
	return G_NOT_IMPLEMENTED;
}

static gresult           
impl_get_charset_titles (GaleonEmbedShell *shell,
			 const char *group,
		         GList **charsets)
{
	return G_NOT_IMPLEMENTED;
}

static gresult           
impl_get_charset_groups (GaleonEmbedShell *shell,
		   	 GList **groups)
{
	return G_NOT_IMPLEMENTED;
}

static gresult
impl_get_font_list (GaleonEmbedShell *shell,
		    const char *langGroup,
		    const char *fontType,
		    GList **fontList,
		    char **default_font)
{
	return G_NOT_IMPLEMENTED;
}

static gresult           
impl_set_permission (GaleonEmbedShell *shell,
		     const char *url, 
		     PermissionType type,
		     gboolean allow)
{
	return G_NOT_IMPLEMENTED;
}

static gresult           
impl_list_permissions (GaleonEmbedShell *shell,
		       PermissionType type, 
		       GList **permissions)
{
	return G_NOT_IMPLEMENTED;
}

static gresult           
impl_remove_permissions (GaleonEmbedShell *shell,
		         PermissionType type,
		         GList *permissions)
{
	return G_NOT_IMPLEMENTED;
}

static gresult           
impl_list_cookies (GaleonEmbedShell *shell,
		   GList **cookies)
{
	return G_NOT_IMPLEMENTED;
}

static gresult           
impl_remove_cookies (GaleonEmbedShell *shell,
		     GList *cookies,
		     gboolean block)
{
	return G_NOT_IMPLEMENTED;
}
	
static gresult           
impl_list_passwords (GaleonEmbedShell *shell,
		     PasswordType type, 
		     GList **passwords)
{
	return G_NOT_IMPLEMENTED;
}

static gresult           
impl_remove_passwords (GaleonEmbedShell *shell,
		       GList *passwords, 
		       PasswordType type)
{
	return G_NOT_IMPLEMENTED;
}

static gresult 
impl_show_file_picker (GaleonEmbedShell *shell,
		       GtkWidget *parentWidget, 
		       const char *title,
		       const char *directory,
		       const char *file, 
		       FilePickerMode mode,
                       char **ret_fullpath, 
		       gboolean *ret_save_content, 
                       FileFormat *file_formats, 
		       int *ret_file_format)
{
	return G_NOT_IMPLEMENTED;
}
