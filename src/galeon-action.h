/*
 *  Copyright (C) 2003 Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GALEON_ACTION_H
#define GALEON_ACTION_H


#include <gtk/gtkaction.h>
#include "galeon-window.h"

#define GALEON_TYPE_ACTION		(galeon_action_get_type ())
#define GALEON_ACTION(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), GALEON_TYPE_ACTION, GaleonAction))
#define GALEON_ACTION_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), GALEON_TYPE_ACTION, GaleonActionClass))
#define GALEON_IS_ACTION(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GALEON_TYPE_ACTION))
#define GALEON_IS_ACTION_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((obj), GALEON_TYPE_ACTION))
#define GALEON_ACTION_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), GALEON_TYPE_ACTION, GaleonActionClass))

typedef struct _GaleonAction		GaleonAction;
typedef struct _GaleonActionClass	GaleonActionClass;
typedef struct GaleonActionPrivate	GaleonActionPrivate;

struct _GaleonAction
{
	GtkAction parent;
	GaleonWindow *window;
	GaleonEmbed *embed;
};

struct _GaleonActionClass
{
	GtkActionClass parent_class;

	/* virtual methods */
	void (*update)			(GaleonAction *action);
	void (*current_embed_changed)	(GaleonAction *action, GaleonEmbed *old_embed, GaleonEmbed *new_embed);
};

GType		galeon_action_get_type			(void);

void		galeon_action_set_window		(GaleonAction *action, GaleonWindow *window);
GaleonWindow *	galeon_action_get_window		(GaleonAction *action);
GaleonEmbed *	galeon_action_get_embed			(GaleonAction *action);

void		galeon_action_update			(GaleonAction *action);

/* protected */
void 		galeon_action_current_embed_changed	(GaleonAction *action, 
							 GaleonEmbed *old_embed, GaleonEmbed *new_embed);

#endif
