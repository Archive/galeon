/*
 *  Copyright (C) 2004 Christian Persch
 *  Copyright (C) 2004 Crispin Flowerday
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#include "config.h"

#include "gul-iso-codes.h"
#include "galeon-debug.h"

#include <glib/gi18n.h>

#include <string.h>

#include <libxml/xmlreader.h>

static GHashTable *iso_639_table = NULL;
static GHashTable *iso_3166_table = NULL;
static GHashTable *galeon_langs_table = NULL;

#define ISO_639_DOMAIN	"iso_639"
#define ISO_3166_DOMAIN	"iso_3166"

#ifdef HAVE_ISO_CODES

#define ISOCODESLOCALEDIR ISO_CODES_PREFIX "/share/locale"

static void
read_iso_639_entry (xmlTextReaderPtr reader,
		    GHashTable *table)
{
	xmlChar *code, *name;

	code = xmlTextReaderGetAttribute (reader, (const xmlChar *) "iso_639_1_code");
	name = xmlTextReaderGetAttribute (reader, (const xmlChar *) "name");

	/* Get iso-639-2 code */
	if (code == NULL || code[0] == '\0')
	{
		xmlFree (code);
		/* FIXME: use the 2T or 2B code? */
		code = xmlTextReaderGetAttribute (reader, (const xmlChar *) "iso_639_2T_code");
	}

	if (code != NULL && code[0] != '\0' && name != NULL && name[0] != '\0')
	{
		g_hash_table_insert (table, code, name);
	}
	else
	{
		xmlFree (code);
		xmlFree (name);
	}
}

static void
read_iso_3166_entry (xmlTextReaderPtr reader,
		     GHashTable *table)
{
	xmlChar *code, *name;

	code = xmlTextReaderGetAttribute (reader, (const xmlChar *) "alpha_2_code");
	name = xmlTextReaderGetAttribute (reader, (const xmlChar *) "name");

	if (code != NULL && code[0] != '\0' && name != NULL && name[0] != '\0')
	{
		char *lcode;

		lcode = g_ascii_strdown ((char *) code, -1);
		xmlFree (code);

		g_hash_table_insert (table, lcode, name);
	}
	else
	{
		xmlFree (code);
		xmlFree (name);
	}

}

typedef enum
{
	STATE_START,
	STATE_STOP,
	STATE_ENTRIES,
} ParserState;

static gboolean
load_iso_entries (int iso,
		  GFunc read_entry_func,
		  gpointer user_data)
{
	xmlTextReaderPtr reader;
	ParserState state = STATE_START;
	xmlChar iso_entries[32], iso_entry[32];
	char *filename;
	int ret = -1;

	LOG ("Loading ISO-%d codes", iso);

	START_PROFILER ("Loading ISO codes");

	filename = g_strdup_printf (ISO_CODES_PREFIX "/share/xml/iso-codes/iso_%d.xml", iso);
	reader = xmlNewTextReaderFilename (filename);
	if (reader == NULL) goto out;

	xmlStrPrintf (iso_entries, sizeof (iso_entries), 
                      (const xmlChar*)"iso_%d_entries", iso);
	xmlStrPrintf (iso_entry, sizeof (iso_entry),
                      (const xmlChar*)"iso_%d_entry", iso);

	ret = xmlTextReaderRead (reader);

	while (ret == 1)
	{
		const xmlChar *tag;
		xmlReaderTypes type;

		tag = xmlTextReaderConstName (reader);
		type = xmlTextReaderNodeType (reader);

		if (state == STATE_ENTRIES &&
		    type == XML_READER_TYPE_ELEMENT &&
		    xmlStrEqual (tag, iso_entry))
		{
			read_entry_func (reader, user_data);
		}
		else if (state == STATE_START &&
			 type == XML_READER_TYPE_ELEMENT &&
			 xmlStrEqual (tag, iso_entries))
		{
			state = STATE_ENTRIES;
		}
		else if (state == STATE_ENTRIES &&
			 type == XML_READER_TYPE_END_ELEMENT &&
			 xmlStrEqual (tag, iso_entries))
		{
			state = STATE_STOP;
		}
		else if (type == XML_READER_TYPE_SIGNIFICANT_WHITESPACE ||
			 type == XML_READER_TYPE_WHITESPACE ||
			 type == XML_READER_TYPE_TEXT ||
			 type == XML_READER_TYPE_COMMENT)
		{
			/* eat it */
		}
		else
		{
			/* ignore it */
		}

		ret = xmlTextReaderRead (reader);
	}

	xmlFreeTextReader (reader);

out:
	if (ret < 0 || state != STATE_STOP)
	{
		/* This is not critical, we will fallback to our own code */
		LOG ("Failed to load ISO-%d codes from %s!\n",
		     iso, filename);
		g_free (filename);
		return FALSE;
	}

	g_free (filename);

	STOP_PROFILER ("Loading ISO codes");

	return TRUE;
}

#endif /* HAVE_ISO_CODES */


static const struct
{
	const char *name;
	const char *code;
} known_languages[] = {
	{ N_("Afrikaans"), "af" },
	{ N_("Albanian"), "sq" },
	{ N_("Arabic"), "ar" },
	{ N_("Azerbaijani"), "az" },
	{ N_("Basque"), "eu" },
	{ N_("Breton"), "br" },
	{ N_("Bulgarian"), "bg" },
	{ N_("Byelorussian"), "be" },
	{ N_("Catalan"), "ca" },
	{ N_("Chinese"), "zh" },
	{ N_("Croatian"), "hr" },
	{ N_("Czech"), "cs" },
	{ N_("Danish"), "da" },
	{ N_("Dutch"), "nl" },
	{ N_("English"), "en" },
	{ N_("English (British)"), "en-gb" },
	{ N_("Esperanto"), "eo" },
	{ N_("Estonian"), "et" },
	{ N_("Faeroese"), "fo" },
	{ N_("Finnish"), "fi" },
	{ N_("French"), "fr" },
	{ N_("Galician"), "gl" },
	{ N_("German"), "de" },
	{ N_("Greek"), "el" },
	{ N_("Hebrew"), "he" },
	{ N_("Hungarian"), "hu" },
	{ N_("Icelandic"), "is" },
	{ N_("Indonesian"), "id" },
	{ N_("Irish"), "ga" },
	{ N_("Italian"), "it" },
	{ N_("Japanese"), "ja" },
	{ N_("Korean"), "ko" },
	{ N_("Latvian"), "lv" },
	{ N_("Lithuanian"), "lt" },
	{ N_("Macedonian"), "mk" },
	{ N_("Malay"), "ms" },
	{ N_("Norwegian/Nynorsk"), "nn" },
	{ N_("Norwegian/Bokmaal"), "nb" },
	{ N_("Norwegian"), "no" },
	{ N_("Occitan"), "oc" },
	{ N_("Polish"), "pl" },
	{ N_("Portuguese"), "pt" },
	{ N_("Portuguese of Brazil"), "pt-br" },
	{ N_("Romanian"), "ro" },
	{ N_("Russian"), "ru" },
	{ N_("Scots Gaelic"), "gd" },
	{ N_("Serbian"), "sr" },
	{ N_("Slovak"), "sk" },
	{ N_("Slovenian"), "sl" },
	{ N_("Spanish"), "es" },
	{ N_("Swedish"), "sv" },
	{ N_("Tamil"), "ta" },
	{ N_("Thai"), "th" },
	{ N_("Turkish"), "tr" },
	{ N_("Ukrainian"), "uk" },
	{ N_("Vietnamese"), "vi" },
	{ N_("Walloon"), "wa" },
};

static void
populate_galeon_langs_table (GHashTable *table)
{
	guint i;

	for (i = 0; i < G_N_ELEMENTS (known_languages); i++)
	{
		g_hash_table_insert (table,
				     (char*)known_languages[i].code,
				     (char*)known_languages[i].name);
	}
}

static void
ensure_iso_codes_initialised (void)
{
	static gboolean initialised = FALSE;

	if (initialised == TRUE)
	{
		return;
	}
	initialised = TRUE;

#if defined (ENABLE_NLS) && defined (HAVE_ISO_CODES)
	bindtextdomain (ISO_639_DOMAIN, ISOCODESLOCALEDIR);
	bind_textdomain_codeset (ISO_639_DOMAIN, "UTF-8");

	bindtextdomain(ISO_3166_DOMAIN, ISOCODESLOCALEDIR);
	bind_textdomain_codeset (ISO_3166_DOMAIN, "UTF-8");
#endif

	iso_639_table = g_hash_table_new_full (g_str_hash, g_str_equal,
					       (GDestroyNotify) xmlFree,
					       (GDestroyNotify) xmlFree);

	iso_3166_table = g_hash_table_new_full (g_str_hash, g_str_equal,
						(GDestroyNotify) g_free,
						(GDestroyNotify) xmlFree);
	
#ifdef HAVE_ISO_CODES
	load_iso_entries (639, (GFunc) read_iso_639_entry, iso_639_table);
	load_iso_entries (3166, (GFunc) read_iso_3166_entry, iso_3166_table);
#endif

	galeon_langs_table = g_hash_table_new (g_str_hash, g_str_equal);
	populate_galeon_langs_table (galeon_langs_table);
}


static char *
get_iso_name_for_lang_code (const char *code)
{
	char **str;
	char *name = NULL;
	const char *langname, *localename;
	int len;

	str = g_strsplit (code, "-", -1);

	/* count the entries */
	for (len = 0; str[len]; len++ ) /* empty */;

	g_return_val_if_fail (len != 0, NULL);

	langname = (const char *) g_hash_table_lookup (iso_639_table, str[0]);

	if (len == 1 && langname != NULL)
	{
		name = g_strdup (dgettext (ISO_639_DOMAIN, langname));
	}
	else if (len == 2 && langname != NULL)
	{
		localename = (const char *) g_hash_table_lookup (iso_3166_table, str[1]);

		if (localename != NULL)
		{
			/* translators: the first %s is the language name, and the
			 * second %s is the locale name. Example:
			 * "French (France)
			 *
			 * Also: The text before the "|" is context to help you decide on
                         * the correct translation. You MUST OMIT it in the translated string.
			 */
			name = g_strdup_printf (Q_("language|%s (%s)"),
						dgettext (ISO_639_DOMAIN, langname),
						dgettext (ISO_3166_DOMAIN, localename));
		}
		else
		{
			name = g_strdup_printf (Q_("language|%s (%s)"),
						dgettext (ISO_639_DOMAIN, langname), str[1]);
		}
	}

	g_strfreev (str);

	return name;
}

/**
 * gul_iso_codes_lookup_name_for_code:
 * @code: A language code, e.g. en-gb
 *
 * Looks up a name to display to the user for a language code,
 * this might use the iso-codes package if support was compiled
 * in, and it is available
 *
 * Returns: the UTF-8 string to display to the user, or NULL if 
 * a name for the code could not be found
 */
char *
gul_iso_codes_lookup_name_for_code (const char *code)
{
	char * lcode;
	char * ret;

	g_return_val_if_fail (code != NULL, NULL);

	ensure_iso_codes_initialised ();

	lcode = g_ascii_strdown (code, -1);

	ret = get_iso_name_for_lang_code (lcode);
	if (!ret)
	{
		const char *langname = 
			(const char*)g_hash_table_lookup (galeon_langs_table, lcode);

		if (langname)
		{
			ret = g_strdup (_(langname));
		}
	}

	g_free (lcode);

	return ret;
}
