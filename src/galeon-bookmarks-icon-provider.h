/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __bookmarks_galeon_icon_provider_h
#define __bookmarks_galeon_icon_provider_h

#include "bookmarks-icon-provider.h"

/* object forward declarations */

typedef struct _GbGaleonIconProvider GbGaleonIconProvider;
typedef struct _GbGaleonIconProviderClass GbGaleonIconProviderClass;
typedef struct _GbGaleonIconProviderPrivate GbGaleonIconProviderPrivate;

/**
 * Editor object
 */

#define GB_TYPE_GALEON_ICON_PROVIDER		      	(gb_galeon_icon_provider_get_type())
#define GB_GALEON_ICON_PROVIDER(object)			(G_TYPE_CHECK_INSTANCE_CAST((object), \
							 GB_TYPE_GALEON_ICON_PROVIDER,\
							 GbGaleonIconProvider))
#define GB_GALEON_ICON_PROVIDER_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), \
							 GB_TYPE_GALEON_ICON_PROVIDER,\
							 GbGaleonIconProviderClass))
#define GB_IS_GALEON_ICON_PROVIDER(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), \
							 GB_TYPE_GALEON_ICON_PROVIDER))
#define GB_IS_GALEON_ICON_PROVIDER_CLASS(klass)		(G_TYPE_CHECK_CLASS_TYPE((klass), \
							 GB_TYPE_GALEON_ICON_PROVIDER))
#define GB_GALEON_ICON_PROVIDER_GET_CLASS(obj)		(G_TYPE_INSTANCE_GET_CLASS((obj), \
							 GB_TYPE_GALEON_ICON_PROVIDER,\
							 GbGaleonIconProviderClass))

struct _GbGaleonIconProviderClass 
{
	GbIconProviderClass parent_class;
};

/* Remember: fields are public read-only */
struct _GbGaleonIconProvider
{
	GbIconProvider parent_object;

	GbGaleonIconProviderPrivate *priv;
};

GType			gb_galeon_icon_provider_get_type		(void);
GbGaleonIconProvider *	gb_galeon_icon_provider_new			(void);
void			gb_galeon_icon_provider_add_bookmark_set	(GbGaleonIconProvider *gip,
									 GbBookmarkSet *set);

#endif

