/*
 * egg-dock-box.h
 *
 * Copyright (C) 2002 Gustavo Gir�ldez <gustavo.giraldez@gmx.net>
 * Copyright (C) 2003 Biswapesh Chattopadhyay <biswapesh_chatterjee@tcscal.co.in>
 * Copyright (C) 2003 Philip Langdale <philipl@mail.utexas.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#ifndef __EGG_DOCK_BOX_H__
#define __EGG_DOCK_BOX_H__

#include <dock/egg-dock-item.h>

G_BEGIN_DECLS

/* standard macros */
#define EGG_TYPE_DOCK_BOX                  (egg_dock_box_get_type ())
#define EGG_DOCK_BOX(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), EGG_TYPE_DOCK_BOX, EggDockBox))
#define EGG_DOCK_BOX_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), EGG_TYPE_DOCK_BOX, EggDockBoxClass))
#define EGG_IS_DOCK_BOX(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), EGG_TYPE_DOCK_BOX))
#define EGG_IS_DOCK_BOX_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), EGG_TYPE_DOCK_BOX))
#define EGG_DOCK_BOX_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), EGG_TYE_DOCK_BOX, EggDockBoxClass))

/* data types & structures */
typedef struct _EggDockBox      EggDockBox;
typedef struct _EggDockBoxClass EggDockBoxClass;
typedef struct _EggDockBoxPrivate EggDockBoxPrivate;

struct _EggDockBox {
    EggDockItem  dock_item;

    gboolean     position_changed;
	EggDockBoxPrivate *_priv;
};

struct _EggDockBoxClass {
    EggDockItemClass parent_class;
	gpointer unused[2];
};


/* public interface */
 
GType      egg_dock_box_get_type        (void);

GtkWidget *egg_dock_box_new             (GtkOrientation orientation);


G_END_DECLS

#endif /* __EGG_DOCK_BOX_H__ */
