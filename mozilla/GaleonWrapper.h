/*
 *  Copyright (C) 2000 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GALEON_WRAPPER_H
#define GALEON_WRAPPER_H

#include <nsIDOMEventListener.h>
#include <nsIDOMContextMenuListener.h>
#include <nsCOMPtr.h>
#define MOZILLA_CLIENT
#include <gtkmozembed.h>
#undef MOZILLA_CLIENT

#include "galeon-embed.h"

/* Forward declarations */
class nsIDocShell;
class nsIWebNavigation;
class nsIWebPageDescriptor;
class nsISHistory;
class nsIWebBrowser;
class nsIChannel;
class nsICacheEntryDescriptor;
class nsIStyleSheet;
class nsIContentViewer;
class nsIPrintSettings;
class nsISecureBrowserUI;
class nsACString;

#ifdef HAVE_MOZILLA_PSM
class nsISSLStatus;
#endif

class GDOMEventListener : public nsIDOMEventListener
{
  public:
	NS_DECL_ISUPPORTS
	NS_DECL_NSIDOMEVENTLISTENER

	GDOMEventListener();
	virtual ~GDOMEventListener();

	nsresult Init(GaleonEmbed* aEmbed);

  protected:
	GaleonEmbed	*mEmbed;
};

class GDOMPopupEventListener : public GDOMEventListener
{
  public:
  	NS_DECL_ISUPPORTS
  	NS_DECL_NSIDOMEVENTLISTENER
  	GDOMPopupEventListener() : GDOMEventListener() {}
  	virtual ~GDOMPopupEventListener() {}
};

class GDOMModalAlertEventListener : public GDOMEventListener
{
public:
	NS_IMETHOD HandleEvent(nsIDOMEvent* aEvent);
};


class GDOMContextMenuListener : public nsIDOMContextMenuListener
{
 public:
  	NS_DECL_ISUPPORTS
	GDOMContextMenuListener() {}	
	virtual ~GDOMContextMenuListener() {};

	nsresult Init(GaleonEmbed* aEmbed, PRBool aIsCapturePhase);

	NS_IMETHOD ContextMenu(nsIDOMEvent* aContextMenuEvent);
	NS_IMETHOD HandleEvent(nsIDOMEvent*);
  protected:
	PRBool          mIsCapturePhase;
	GaleonEmbed	*mEmbed;
};

class GaleonWrapper
{
  public:
	GaleonWrapper();
	~GaleonWrapper();

	nsresult Init (GtkMozEmbed *mozembed);
	nsresult Destroy (void);

	nsresult SetZoom (float aTextZoom);
	nsresult GetZoom (float *aTextZoom);

	nsresult Print ();
	nsresult GetPrintSettings (nsIPrintSettings * *options);
	nsresult PrintPreviewClose (void);
	nsresult PrintPreviewNumPages (int *numPages);
	nsresult PrintPreviewNavigate(PRInt16 navType, PRInt32 pageNum);

	nsresult FindSetProperties (const PRUnichar *search_string,
				    PRBool case_sensitive,
				    PRBool wrap_around);

	nsresult Find (PRBool backwards, PRBool *didFind);

	nsresult GetMainDocumentUrl (nsACString &url);
	nsresult GetDocumentUrl (nsACString &url);
	nsresult FocusActivate ();
	nsresult FocusDeactivate ();

	nsresult ReloadViewSourcePage ();
	nsresult ReloadFrame ();

	nsresult LoadDocument(nsISupports *aPageDescriptor, PRUint32 aDisplayType);
	nsresult GetPageDescriptor(nsISupports **aPageDescriptor);

	nsresult GetSHInfo (PRInt32 *count, PRInt32 *index);
	nsresult GetSHTitleAtIndex (PRInt32 index, PRUnichar **title);
	nsresult GetSHUrlAtIndex (PRInt32 index, nsACString &url);

	nsresult CopyHistoryTo (GaleonWrapper *embed, PRBool back_history,
				PRBool forward_history, PRBool set_current);

	nsresult GoToHistoryIndex (PRInt16 index);

	nsresult ClearHistory();

	nsresult SetForcedEncoding (const char *encoding);

	nsresult GetForcedEncoding (nsACString &encoding);

	nsresult GetEncoding (nsACString &encoding);
	
	nsresult GetHasModifiedForms (PRBool *modified);

	nsresult CanCutSelection(PRBool *result);

	nsresult CanCopySelection(PRBool *result);

	nsresult CanPaste(PRBool *result);

	nsresult CutSelection(void);

	nsresult CopySelection(void);

	nsresult Paste(void);

	nsresult GetStyleSheets (nsIDOMStyleSheetList **list);

	nsresult GetMainDOMDocument (nsIDOMDocument **aDOMDocument);

	nsresult LoadOverrideStyleSheet (char *css,
					 nsIStyleSheet **return_sheet);
#ifdef HAVE_NSISTYLESHEETSERVICE_H
	nsresult RemoveOverrideStyleSheet (char *css);
#else
	nsresult RemoveOverrideStyleSheet (nsIStyleSheet *remove);
#endif

	nsresult GetLinkInterfaceItems (GList **list);

	nsresult GetRealURL (nsACString &ret);

	nsresult SelectAll (void);

	nsresult ScrollUp (void);
	nsresult ScrollDown (void);
	nsresult ScrollLeft (void);
	nsresult ScrollRight (void);

	nsresult ScrollPageUp (void);
	nsresult ScrollPageDown (void);

	nsresult FineScroll (int horiz, int vert);

	nsresult GetPageProperties (EmbedPageProperties *props);
	nsresult ShowPageCertificate ();

	nsresult GetSecurityInfo (PRUint32 *aState, nsACString &aDescription);

	nsresult EvaluateJS (const char *script);

	nsresult PushTargetDocument (nsIDOMDocument *domDoc, GaleonEmbedEvent *event);
	nsresult PopTargetDocument ();

	nsresult GetDOMDocument (nsIDOMDocument **aDOMDocument);
	nsresult GetDOMWindow (nsIDOMWindow **aDOMWindow);

	nsCOMPtr<nsIWebBrowser>           mWebBrowser;
  private:

	nsCOMPtr<nsIDOMDocument> mTargetDocument;
	nsCOMPtr<nsISecureBrowserUI> mSecurityInfo;

	nsCOMPtr<nsIDOMEventTarget> mEventTarget;
	nsCOMPtr<GDOMEventListener> mFaviconEventListener;
	nsCOMPtr<GDOMEventListener> mPopupEventListener;
	nsCOMPtr<GDOMContextMenuListener> mContextMenuListenerBubble;
	nsCOMPtr<GDOMContextMenuListener> mContextMenuListenerCapture;
	nsCOMPtr<GDOMModalAlertEventListener> mModalAlertListener;

	GaleonEmbedEvent *mCurrentEvent;

	nsresult GetContentViewer (nsIContentViewer **aViewer);
	nsresult GetFocusedDOMWindow (nsIDOMWindow **aDOMWindow);
	nsresult GetSHistory (nsISHistory **aSHistory);

#ifdef HAVE_MOZILLA_PSM
	nsresult GetSSLStatus (nsISSLStatus **aSSLStatus);
#endif

	nsresult GetCacheEntryDescriptor(const nsAString &aKey,
					 nsICacheEntryDescriptor **aCacheEntryDescriptor);
	nsresult GetDocumentHasModifiedForms (nsIDOMDocument *aDomDoc, 
					      PRUint32 *aNumTextFields, PRBool *aIsModified);

	nsresult GetPageSecurityInfo (EmbedPageProperties *props);
	nsresult GetMetaTags(GList **ret);
	nsresult GetImages (GList **ret);
	nsresult GetForms (GList **ret);
	nsresult GetLinks (GList **ret);
};

#endif
