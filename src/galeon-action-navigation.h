/*
 *  Copyright (C) 2003 Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GALEON_NAVIGATION_ACTION_H
#define GALEON_NAVIGATION_ACTION_H


#include "galeon-action.h"

#define GALEON_TYPE_ACTION_NAVIGATION			(galeon_action_navigation_get_type ())
#define GALEON_ACTION_NAVIGATION(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), GALEON_TYPE_ACTION_NAVIGATION,\
							 GaleonActionNavigation))
#define GALEON_ACTION_NAVIGATION_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), GALEON_TYPE_ACTION_NAVIGATION,\
							 GaleonActionNavigationClass))
#define GALEON_IS_ACTION_NAVIGATION(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GALEON_TYPE_ACTION_NAVIGATION))
#define GALEON_IS_ACTION_NAVIGATION_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((obj), GALEON_TYPE_ACTION_NAVIGATION))
#define GALEON_ACTION_NAVIGATION_GET_CLASS(obj)		(G_TYPE_INSTANCE_GET_CLASS((obj), GALEON_TYPE_ACTION_NAVIGATION,\
							 GaleonActionNavigationClass))

typedef struct _GaleonActionNavigation		GaleonActionNavigation;
typedef struct _GaleonActionNavigationClass	GaleonActionNavigationClass;
typedef struct GaleonActionNavigationPrivate	GaleonActionNavigationPrivate;

typedef enum
{
	GALEON_NAVIGATION_DIRECTION_UP,
	GALEON_NAVIGATION_DIRECTION_BACK,
	GALEON_NAVIGATION_DIRECTION_FORWARD
} GaleonActionNavigationDirection;

#define GALEON_TYPE_ACTION_NAVIGATION_DIRECTION		(galeon_action_navigation_direcion_get_type ())
GType		galeon_action_navigation_direcion_get_type	(void);

struct _GaleonActionNavigation
{
	GaleonAction parent;

	/*< private >*/
	GaleonActionNavigationDirection direction;
	gboolean show_arrow;

	gboolean ignore_next_menu_activate;
};

struct _GaleonActionNavigationClass
{
	GaleonActionClass parent_class;

};

GType		galeon_action_navigation_get_type		(void);
gboolean	galeon_action_navigation_get_show_arrow		(GaleonActionNavigation *a);
void		galeon_action_navigation_set_show_arrow		(GaleonActionNavigation *a, gboolean value);
GaleonActionNavigationDirection galeon_action_navigation_get_direction (GaleonActionNavigation *a);
void		galeon_action_navigation_set_direction		(GaleonActionNavigation *a,
								 GaleonActionNavigationDirection value);

#endif
