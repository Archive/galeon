/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n.h>
#include "bookmarks-export-druid-konqueror.h"
#include "gul-string.h"
#include "gul-general.h"

#include "xbel.h"

static GHashTable *strings = NULL;

/**
 * Private functions, only availble from this file
 */
static void		gb_export_druid_konqueror_finalize_impl		(GObject *o);
static void		gb_export_druid_konqueror_init_strings 		(void);
static const char *	gb_export_druid_konqueror_get_string_impl	(GbExportDruid *d, 
									 const char *string_id);
static GSList *		gb_export_druid_konqueror_get_locations_impl	(GbExportDruid *d);
static GbIO *		gb_export_druid_konqueror_get_io_impl		(GbExportDruid *d);


/**
 * ExportDruidKonqueror object
 */

G_DEFINE_TYPE (GbExportDruidKonqueror, gb_export_druid_konqueror, GB_TYPE_EXPORT_DRUID);

static void
gb_export_druid_konqueror_class_init (GbExportDruidKonquerorClass *klass)
{
	GB_EXPORT_DRUID_CLASS (klass)->get_locations = gb_export_druid_konqueror_get_locations_impl;
	GB_EXPORT_DRUID_CLASS (klass)->get_io = gb_export_druid_konqueror_get_io_impl;
	GB_EXPORT_DRUID_CLASS (klass)->get_string = gb_export_druid_konqueror_get_string_impl;
	G_OBJECT_CLASS (klass)->finalize = gb_export_druid_konqueror_finalize_impl;
}

static void 
gb_export_druid_konqueror_init (GbExportDruidKonqueror *e)
{
}

static void
gb_export_druid_konqueror_finalize_impl (GObject *o)
{
	G_OBJECT_CLASS (gb_export_druid_konqueror_parent_class)->finalize (o);
}

GbExportDruidKonqueror *
gb_export_druid_konqueror_new (void)
{
	GbExportDruidKonqueror *ret = g_object_new (GB_TYPE_EXPORT_DRUID_KONQUEROR, NULL);
	return ret;
}

static const char *
gb_export_druid_konqueror_get_string_impl (GbExportDruid *d, 
					 const char *string_id)
{
	gb_export_druid_konqueror_init_strings ();
	return _(g_hash_table_lookup (strings, string_id));
}

static void
gb_export_druid_konqueror_init_strings (void)
{
	if (!strings)
	{
		static struct 
		{
			const char *id;
			const char *text;
		} strs[] = {
			{ "window title", N_("Konqueror Bookmarks Export Druid") },
			{ "page 1 title", N_("Konqueror Bookmarks Export") },
			{NULL, NULL}
		};			
		int i;
		strings = g_hash_table_new (g_str_hash, g_str_equal);
		for (i = 0; strs[i].id != NULL; ++i)
		{
			g_hash_table_insert (strings, (gpointer) strs[i].id, (gpointer) strs[i].text);
		}
	}
}

static GSList *
gb_export_druid_konqueror_get_locations_impl (GbExportDruid *druid)
{
	GSList *l;
	GSList *ret = NULL;
	gchar *dir;
	
	dir = g_build_filename (g_get_home_dir (), ".kde", NULL);
	l = gul_find_file  (dir, "bookmarks.xbel", 5);
	ret = g_slist_concat (ret, l);
	l = gul_find_file (dir, "bookmarks.xml", 5);
	ret = g_slist_concat (ret, l);
	g_free (dir);

	dir = g_build_filename (g_get_home_dir (), ".kde2", NULL);
	l = gul_find_file (dir, "bookmarks.xbel", 5);
	ret = g_slist_concat (ret, l);
	l = gul_find_file (dir, "bookmarks.xml", 5);
	ret = g_slist_concat (ret, l);
	g_free (dir);

	dir = g_build_filename (g_get_home_dir (), ".kde3", NULL);
	l = gul_find_file (dir, "bookmarks.xbel", 5);
	ret = g_slist_concat (ret, l);
	l = gul_find_file (dir, "bookmarks.xml", 5);
	ret = g_slist_concat (ret, l);
	g_free (dir);

	dir = g_build_filename (g_get_home_dir (), ".konqueror", NULL);
	l = gul_find_file (dir, "bookmarks.xbel", 5);
	ret = g_slist_concat (ret, l);
	l = gul_find_file (dir, "bookmarks.xml", 5);
	ret = g_slist_concat (ret, l);
	
	if (ret == NULL)
	{
		/* give a default */
		ret = g_slist_prepend
			(ret, g_build_filename (g_get_home_dir (), 
						".kde/share/apps/konqueror/bookmarks.xml", NULL));
	}

	g_free (dir);

	return ret;
}

static GbIO *
gb_export_druid_konqueror_get_io_impl (GbExportDruid *druid)
{
	GbXBEL *io = gb_xbel_new ();
	return GB_IO (io);
}

