/*
 *  Copyright (C) 2004 Crispin Flowerday
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef GALEON_SIDEBARS_H
#define GALEON_SIDEBARS_H

#include <glib-object.h>
#include <gtk/gtkbin.h>

#include "galeon-embed-shell.h"
#include "galeon-sidebar.h"

G_BEGIN_DECLS

#define GALEON_TYPE_SIDEBARS		(galeon_sidebars_get_type ())
#define GALEON_SIDEBARS(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), GALEON_TYPE_SIDEBARS, GaleonSidebars))
#define GALEON_SIDEBARS_CLASS(k)	(G_TYPE_CHECK_CLASS_CAST((k), GALEON_TYPE_SIDEBARS, GaleonSidebarsClass))
#define GALEON_IS_SIDEBARS(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), GALEON_TYPE_SIDEBARS))
#define GALEON_IS_SIDEBARS_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), GALEON_TYPE_SIDEBARS))
#define GALEON_SIDEBAR__SGET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), GALEON_TYPE_SIDEBARS, GaleonSidebarsClass))

typedef struct _GaleonSidebars	GaleonSidebars;
typedef struct _GaleonSidebarsPrivate	GaleonSidebarsPrivate;
typedef struct _GaleonSidebarsClass	GaleonSidebarsClass;

struct _GaleonSidebars 
{
	GObject object;

	/*< private >*/
	GaleonSidebarsPrivate *priv;
};

struct _GaleonSidebarsClass
{
	GtkBinClass parent_class;
};

GType	    galeon_sidebars_get_type	(void);

GaleonSidebars *galeon_sidebars_new		(GaleonEmbedShell *shell);

void	    galeon_sidebars_init_window_sidebar (GaleonSidebars *sidebars, GaleonSidebar *sidebar);

G_END_DECLS

#endif
