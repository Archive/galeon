/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <nscore.h>

/* Some versions of nscore.h appears to #define nscore_h___, while
 * gtkmozembed.h looks for nscore_h__ to see if stuff needs
 * redefining (what were they thinking?!).  Define explicitly, then
 * curse. */
#ifndef nscore_h__
#define nscore_h__
#endif

#include <nsComponentManagerUtils.h>
#include <nsServiceManagerUtils.h>

#define HAVE_VISIBILITY_HIDDEN_ATTRIBUTE 1
#define HAVE_VISIBILITY_ATTRIBUTE 1

#ifdef XPCOM_GLUE
#include <nsXPCOMGlue.h>
#include <gtkmozembed_glue.cpp>
#endif

#define MOZILLA_CLIENT
#include "gtkmozembed.h"
#include "gtkmozembed_internal.h"
#undef MOZILLA_CLIENT
#include "glib.h"
#include "js-console.h"
#include "gul-string.h"
#include "galeon-config.h"
#include "mozilla-embed-shell.h"
#include "mozilla-prefs.h"
#include "prefs-strings.h"
#include "mozilla-notifiers.h"
#include "mozilla-i18n.h"
#include "eel-gconf-extensions.h"
#include "galeon-embed-prefs.h"
#include "MozRegisterComponents.h"
#include "JSConsoleListener.h"
#include "CookieObserver.h"
#include "mozilla-cookie.h"
#include "mozilla-encodings.h"
#include "GaleonUtils.h"
#include "MozillaPrivate.h"
#include "GulString.h"

#include <time.h>
#include <glib/gi18n.h>
#include <string.h>

#include <nsIConsoleService.h>
#include <nsICacheService.h>
#include <nsCOMPtr.h>
#include <nsNetCID.h>
#include <nsIIOService.h>
#include <nsIFontEnumerator.h>
#include <nsISupportsPrimitives.h>
#include <nsIPermissionManager.h>
#include <nsICookieManager.h>
#include <nsIPermission.h>
#ifdef HAVE_NSIPASSWORD_H
#include <nsIPasswordManager.h>
#include <nsIPassword.h>
#elif defined(HAVE_NSILOGINMANAGER_H)
#include <nsILoginInfo.h>
#include <nsILoginManager.h>
#include <nsIIDNService.h>
#endif
#include <nsICookie.h>
#include <nsCPasswordManager.h>
#include <nsIObserverService.h>
#include <nsIPrefService.h>
#include <nsILocalFile.h>
#include <nsIURI.h>
#include <nsMemory.h>

//For setting the locale. hopefully gtkmozembed will do itself soon
#include <nsIChromeRegistry.h>
#include <nsILocaleService.h>

#ifdef HAVE_NSITOOLKITCHROMEREGISTRY_H
#include "EphyDirectoryProvider.h"
#endif

#define MOZILLA_PROFILE_DIR  GALEON_DIR"/mozilla"
#define MOZILLA_PROFILE_NAME "galeon"
#define MOZILLA_PROFILE_FILE "prefs.js"

//Absolute path to the file holding default preferences
#ifndef DEFAULT_PROFILE_FILE
#define DEFAULT_PROFILE_FILE SHARE_DIR"/default-prefs.js"
#endif

//Table of permission types to map to new string based api
const char* gPermType[3] = { "cookie", "image", "popup" };

static void mozilla_embed_shell_new_window_orphan_cb (GtkMozEmbedSingle *embed,
            	           		              GtkMozEmbed **retval, 
					              guint chrome_mask,
                           		              GaleonEmbedShell *shell);

#define MOZILLA_EMBED_SHELL_GET_PRIVATE(object) (G_TYPE_INSTANCE_GET_PRIVATE ((object), \
				       MOZILLA_TYPE_EMBED_SHELL, MozillaEmbedShellPrivate))


struct MozillaEmbedShellPrivate
{
	MozillaEncodings *encodings;
};

G_DEFINE_TYPE (MozillaEmbedShell, mozilla_embed_shell, GALEON_TYPE_EMBED_SHELL);

static gboolean
mozilla_set_default_prefs (void)
{
	nsCOMPtr<nsIPrefService> prefs
		= do_GetService (NS_PREFSERVICE_CONTRACTID);
	if (!prefs) return FALSE;

	// read our predefined default prefs
	nsresult rv;
	nsCOMPtr<nsILocalFile> file;
	rv = NS_NewNativeLocalFile(
		NS_LITERAL_CSTRING(DEFAULT_PROFILE_FILE),
		PR_TRUE, getter_AddRefs(file));
	if (NS_FAILED(rv)) return FALSE;

	rv = prefs->ReadUserPrefs (file);

	if (NS_FAILED(rv))
	{
		g_warning ("failed to read default preferences, error: %x", rv);
		return FALSE;
	}

	// Load the default user preferences as well.  This also makes the
	// prefs to be saved in the user's prefs.js file, instead of messing up
	// our global defaults file.
	rv = prefs->ReadUserPrefs (nsnull);
	if (NS_FAILED(rv)) 
	{
		g_warning ("failed to read user preferences, error: %x", rv);
	}

	nsCOMPtr<nsIPrefBranch> pref;
	prefs->GetDefaultBranch ("", getter_AddRefs(pref));
	if (!pref) return FALSE;

	// Search engine for keyword search, although disabled by default, set
	// here to facilitate l10n.
	pref->SetCharPref ("keyword.URL", _("http://www.google.com/search?ie=UTF-8&q="));

	// Also set vendor and vendorSub prefs so that mozilla can construct
	// the user agent string for us.
	pref->SetCharPref ("general.useragent.vendor", USERAGENT_VENDOR);
	pref->SetCharPref ("general.useragent.vendorSub", USERAGENT_VENDORSUB);

	return TRUE;
}

static void
mozilla_init_single (MozillaEmbedShell *mes)
{	
	GtkMozEmbedSingle *single;
	
	/* get single */
        single = gtk_moz_embed_single_get ();
        if (single == NULL)
        {
                g_warning ("Failed to get singleton embed object!\n");
        }

        /* allow creation of orphan windows */
        g_signal_connect (G_OBJECT (single), "new_window_orphan",
                          G_CALLBACK (mozilla_embed_shell_new_window_orphan_cb),
			  mes);
}

#ifdef GALEON_MOZILLA_HOME
static void
mozilla_init_home (void)
{
	char *mozilla_home;

	mozilla_home = g_strdup (g_getenv ("GALEON_MOZILLA_HOME"));
	if (mozilla_home == NULL)
	{
		mozilla_home = g_strdup (GALEON_MOZILLA_HOME);
	}

        gtk_moz_embed_set_comp_path (mozilla_home);
#ifdef HAVE_GTK_MOZ_EMBED_SET_PATH
	gtk_moz_embed_set_path (mozilla_home);
#endif
	g_free (mozilla_home);
}
#endif /* GALEON_MOZILLA_HOME */

static void
mozilla_init_profile (void)
{
	char *profile_path;
	profile_path = g_build_filename (g_get_home_dir (), 
					 MOZILLA_PROFILE_DIR, 
					 NULL);
        gtk_moz_embed_set_profile_path (profile_path, MOZILLA_PROFILE_NAME);
        g_free (profile_path);
}

static void
mozilla_init_login_manager(void)
{
#ifdef HAVE_NSILOGINMANAGER_H
        nsCOMPtr<nsILoginManager> loginManager =
                        do_GetService (NS_LOGINMANAGER_CONTRACTID);
	if (!loginManager)
		g_warning ("Failed to instantiate LoginManager");
#endif
}

static void
mozilla_init_plugin_path ()
{
	const char * user_path = g_getenv( "MOZ_PLUGIN_PATH" );

	GString *new_path = g_string_new (user_path);
	
	/* Possibly append a ':' */
	if (new_path->len && new_path->str[new_path->len-1] != ':')
	{
		g_string_append_c (new_path, ':');
	}

	/* Add the user's plugin path (~/.mozilla/plugins) */
	g_string_append (new_path, g_get_home_dir());
	g_string_append (new_path, "/.mozilla/plugins");

	/* Add /usr/lib/mozilla/plugins (bug 149812) */
	g_string_append (new_path, ":/usr/lib/mozilla/plugins");

#ifdef GALEON_MOZILLA_HOME
	/* Add the current mozilla build's plugins dir */
	g_string_append (new_path, ":" GALEON_MOZILLA_HOME "/plugins");
#endif

	g_setenv ("MOZ_PLUGIN_PATH", new_path->str, TRUE);

	g_string_free (new_path, TRUE);
}

static void
mozilla_init_js_console ()
{
	nsCOMPtr<nsIConsoleService> consoleService = 
		do_GetService (NS_CONSOLESERVICE_CONTRACTID);
        g_return_if_fail (consoleService != nsnull);

        JSConsoleListener *listener = new JSConsoleListener();
        consoleService->RegisterListener (listener);
}

#if 0
static gboolean
have_gnome_url_handler (const gchar *protocol)
{
	gchar *key, *cmd;
	gboolean rv;

	key = g_strdup_printf ("/desktop/gnome/url-handlers/%s/command", 
			       protocol);
	cmd = eel_gconf_get_string (key);
	g_free (key);

	rv = (cmd != NULL);
	g_free (cmd);

	if (!rv) return rv;

	key = g_strdup_printf ("/desktop/gnome/url-handlers/%s/enabled", 
			       protocol);
	rv = eel_gconf_get_boolean (key);
	g_free (key);

	return rv;
}
#endif

static gboolean
mozilla_init_cookie_observer(void)
{
	//We new this observer but never delete it
	//This is ok as mozilla_embed_shell's lifetime is
	//the whole time galeon is running. Galeon exits 
	//when it is destroyed, so there's no real need to
	//clean up. 

	nsresult rv;

	nsCOMPtr<nsIObserverService> os(do_GetService("@mozilla.org/observer-service;1", &rv));
	if(NS_FAILED(rv) || ! os) return FALSE;

	GCookieObserver* cookieObserver = new GCookieObserver();

	rv = os->AddObserver(cookieObserver,
			     PERM_CHANGE_NOTIFICATION,
			     PR_FALSE);
	if (NS_FAILED(rv)) return FALSE;

	return TRUE;
}

static void
mozilla_embed_shell_init (MozillaEmbedShell *mes)
{
        mes->priv = MOZILLA_EMBED_SHELL_GET_PRIVATE (mes);

	mes->priv->encodings = NULL;

	nsresult rv;
#ifdef XPCOM_GLUE
	static const GREVersionRange greVersion = {
	  "1.9a", PR_TRUE,
	  "1.9.*", PR_TRUE
	};
	char xpcomLocation[4096];
	rv = GRE_GetGREPathWithProperties(&greVersion, 1, nsnull, 0, xpcomLocation, 4096);
	if (NS_FAILED (rv))
	{
	  g_warning ("Could not determine locale!\n");
	  return;
	}

	// Startup the XPCOM Glue that links us up with XPCOM.
	rv = XPCOMGlueStartup(xpcomLocation);
	if (NS_FAILED (rv))
	{
	  g_warning ("Could not determine locale!\n");
	  return;
	}

	rv = GTKEmbedGlueStartup();
	if (NS_FAILED (rv))
	{
	  g_warning ("Could not startup glue!\n");
	  return;
	}

	rv = GTKEmbedGlueStartupInternal();
	if (NS_FAILED (rv))
	{
	  g_warning ("Could not startup internal glue!\n");
	  return;
	}

	char *lastSlash = strrchr(xpcomLocation, '/');
	if (lastSlash)
	  *lastSlash = '\0';

	gtk_moz_embed_set_path(xpcomLocation);
	gtk_moz_embed_set_comp_path(SHARE_DIR);
#endif // XPCOM_GLUE
#ifdef GALEON_MOZILLA_HOME
	mozilla_init_home ();
#endif
	/* Pre initialization */
	mozilla_init_plugin_path();
	mozilla_init_profile ();

#if HAVE_NSITOOLKITCHROMEREGISTRY_H
	nsCOMPtr<nsIDirectoryServiceProvider> dp = new EphyDirectoryProvider ();
	if (dp) {
		gtk_moz_embed_set_directory_service_provider (dp);
	}
#endif

	/* Fire up the beast */
	gtk_moz_embed_push_startup ();

	mozilla_init_login_manager();
}


static gboolean
impl_initialize (GaleonEmbedShell *ges)
{
	gboolean ret;
	MozillaEmbedShell *mes = MOZILLA_EMBED_SHELL(ges);

	/* Post initialization */
	ret = mozilla_set_default_prefs ();
	if (ret == FALSE) return FALSE;

	mozilla_notifiers_init (mes);

	mozilla_init_single (mes);
	
	ret = mozilla_register_components ();
	if (ret == FALSE) return FALSE;

	mozilla_init_js_console ();

	mozilla_init_cookie_observer();
		
	return TRUE;
}

static void 
mozilla_embed_shell_new_window_orphan_cb (GtkMozEmbedSingle *embed,
                      		          GtkMozEmbed **retval, 
					  guint chrome_mask,
                           		  GaleonEmbedShell *shell)
{
	/* FIXME conversion duped in mozilla_embed */
	GaleonEmbed *new_embed;
	int i;
        EmbedChromeMask mask = EMBED_CHROME_OPENASPOPUP;
        
        struct
        {
                guint chromemask;
                EmbedChromeMask embed_mask;
        }
        conversion_map [] =
        {
                { GTK_MOZ_EMBED_FLAG_DEFAULTCHROME, EMBED_CHROME_DEFAULT },
                { GTK_MOZ_EMBED_FLAG_MENUBARON, EMBED_CHROME_MENUBARON },
                { GTK_MOZ_EMBED_FLAG_TOOLBARON, EMBED_CHROME_TOOLBARON },
                { GTK_MOZ_EMBED_FLAG_STATUSBARON, EMBED_CHROME_STATUSBARON },
                { GTK_MOZ_EMBED_FLAG_WINDOWRAISED, EMBED_CHROME_WINDOWRAISED },
                { GTK_MOZ_EMBED_FLAG_WINDOWLOWERED, EMBED_CHROME_WINDOWLOWERED },
                { GTK_MOZ_EMBED_FLAG_CENTERSCREEN, EMBED_CHROME_CENTERSCREEN },
                { GTK_MOZ_EMBED_FLAG_OPENASDIALOG, EMBED_CHROME_OPENASDIALOG },
                { GTK_MOZ_EMBED_FLAG_OPENASCHROME, EMBED_CHROME_OPENASCHROME },
                { 0, EMBED_CHROME_NONE }
        };

        for (i = 0; conversion_map[i].chromemask != 0; i++)
        {
                if (chrome_mask & conversion_map[i].chromemask)
                {
                        mask = (EmbedChromeMask) (mask | conversion_map[i].embed_mask); 
                }
        }
       
	g_signal_emit_by_name (shell, "new_window_orphan", mask, &new_embed);

	g_assert (new_embed != NULL);
	
	gtk_moz_embed_set_chrome_mask (GTK_MOZ_EMBED(new_embed), chrome_mask);

	*retval = GTK_MOZ_EMBED(GALEON_EMBED(new_embed));
}

static void
mozilla_embed_shell_finalize (GObject *object)
{
	MozillaEmbedShell *mes;

        g_return_if_fail (object != NULL);
        g_return_if_fail (MOZILLA_IS_EMBED_SHELL (object));

        mes = MOZILLA_EMBED_SHELL (object);

        g_return_if_fail (mes->priv != NULL);

	mozilla_notifiers_free ();

	mozilla_prefs_save ();
	
	gtk_moz_embed_pop_startup ();
	
	if (mes->priv->encodings)
	{
		g_object_unref (mes->priv->encodings);
	}

        G_OBJECT_CLASS (mozilla_embed_shell_parent_class)->finalize (object);
}

static void
impl_clear_cache (GaleonEmbedShell *shell,
		  CacheType type)
{
	nsCOMPtr<nsICacheService> CacheService =
                        do_GetService (NS_CACHESERVICE_CONTRACTID);

	if (CacheService)
	{
		CacheService->EvictEntries((guint)type);
	}
}

static void
impl_set_offline_mode (GaleonEmbedShell *shell,
		       gboolean offline)
{
	nsCOMPtr<nsIIOService> io = do_GetService(NS_IOSERVICE_CONTRACTID);
	if (io)
	{
		io->SetOffline(offline);
	}
}

static void
impl_show_java_console (GaleonEmbedShell *shell)
{
}

static GaleonEncodings *
impl_get_encodings (GaleonEmbedShell *shell)
{
	MozillaEmbedShell *mshell = MOZILLA_EMBED_SHELL(shell);

	if (! mshell->priv->encodings)
	{
		mshell->priv->encodings = mozilla_encodings_new ();
	}
	return GALEON_ENCODINGS (mshell->priv->encodings);
}


static GList *
impl_get_font_langs (GaleonEmbedShell *shell)
{
	GList *l = NULL;
	int i;
	for (i = 0; i < num_font_languages; i++)
	{
		FontLangInfo *info = g_new0 (FontLangInfo, 1);
		info->key   = font_languages[i].key;
		info->title = _(font_languages[i].title);
		l = g_list_append (l, info);
	}
	return l;
}


static GList *
impl_get_font_list (GaleonEmbedShell *shell,
		    const char *langGroup,
		    const char *fontType)
{
	nsresult rv;

	nsCOMPtr<nsIFontEnumerator> mozFontEnumerator;
	mozFontEnumerator = do_CreateInstance("@mozilla.org/gfx/fontenumerator;1", &rv);
	NS_ENSURE_SUCCESS(rv, NULL);

	PRUint32    n_fonts;
	PRUnichar **fonts;
	rv = mozFontEnumerator->EnumerateFonts (langGroup, fontType,
			                        &n_fonts, &fonts);
	NS_ENSURE_SUCCESS(rv, NULL);

	GList *l = NULL;
	for (PRUint32 i = 0; i < n_fonts; i++)
	{
		char *str;

		str = g_strdup (GulCString (fonts[i]).get());
		l = g_list_prepend (l, str);
		nsMemory::Free (fonts[i]);
	}

	nsMemory::Free (fonts);

	return g_list_reverse (l);
}

static void
impl_set_permission(GaleonEmbedShell *shell,
		    const char *url, 
		    PermissionType type,
		    gboolean allow)
{
	nsresult rv;
	nsCOMPtr<nsIPermissionManager> permissionManager =
                        do_GetService(NS_PERMISSIONMANAGER_CONTRACTID);

	nsCOMPtr<nsIURI> uri;
	rv = GaleonUtils::NewURI(getter_AddRefs(uri), GulDependentCString (url));
	if (NS_FAILED(rv) || !uri) return;

	 permissionManager->Add(uri, gPermType[type],
				allow ? (PRUint32)nsIPermissionManager::ALLOW_ACTION
				: (PRUint32)nsIPermissionManager::DENY_ACTION);
}

static GList *
impl_list_permissions (GaleonEmbedShell *shell,
		       PermissionType type)
{
        nsresult result;

	GList *permissions = NULL;
	
	nsCOMPtr<nsIPermissionManager> permissionManager = 
                        do_GetService(NS_PERMISSIONMANAGER_CONTRACTID);
        nsCOMPtr<nsISimpleEnumerator> permissionEnumerator;
        result = permissionManager->GetEnumerator(getter_AddRefs(permissionEnumerator));
        if (NS_FAILED(result)) return NULL;
	
        PRBool enumResult;
        for (permissionEnumerator->HasMoreElements(&enumResult) ;
             enumResult == PR_TRUE ;
             permissionEnumerator->HasMoreElements(&enumResult))
        {
                nsCOMPtr<nsIPermission> nsPermission;
                result = permissionEnumerator->GetNext(getter_AddRefs(nsPermission));
                if (NS_FAILED(result)) return NULL;

		GulCString cType;
		nsPermission->GetType(cType);
		if (cType.Equals(gPermType[type]))
                {
                        PermissionInfo *b = g_new0(PermissionInfo, 1);
			GulCString host;
			nsPermission->GetHost(host);
			b->domain = g_strdup(host.get());

			PRUint32 cap;
			nsPermission->GetCapability(&cap);
			switch (cap)
			{
			  case nsIPermissionManager::ALLOW_ACTION :
			  	b->status = TRUE;
			  	break;
			  case nsIPermissionManager::DENY_ACTION :
			 	b->status = FALSE;
			 	break;
			  default :
			  	b->status = FALSE;
			}
                        permissions = g_list_prepend(permissions, b);
                }
        }

        return g_list_reverse(permissions);
}

static void
impl_remove_permissions(GaleonEmbedShell *shell,
		        PermissionType type,
		        GList *permissions)
{
	nsresult result;
        nsCOMPtr<nsIPermissionManager> permissionManager =
                        do_GetService(NS_PERMISSIONMANAGER_CONTRACTID);

        for (GList *l = permissions; l != NULL; l = l->next)
        {
                PermissionInfo *b = (PermissionInfo *)l->data;
                result = permissionManager->Remove(GulDependentCString (b->domain),
						   gPermType[type]);
                if (NS_FAILED(result)) return;
        };
}

static PermissionActionType
impl_test_permission (GaleonEmbedShell *shell,
		      const char *url, 
		      PermissionType type)
{
	nsresult rv;
	nsCOMPtr<nsIPermissionManager> permissionManager =
                        do_GetService(NS_PERMISSIONMANAGER_CONTRACTID);

	if (!url) return DENY_ACTION;

	nsCOMPtr<nsIURI> uri;
	rv = GaleonUtils::NewURI(getter_AddRefs(uri), GulDependentCString (url));
	if (NS_FAILED(rv) || !uri) return DENY_ACTION;

	PRUint32 action;
	rv = permissionManager->TestPermission(uri, gPermType[type], &action);

	switch (action)
	{
	  case nsIPermissionManager::UNKNOWN_ACTION :
		  return UNKNOWN_ACTION;
	  case nsIPermissionManager::ALLOW_ACTION :
		  return ALLOW_ACTION;
	  case nsIPermissionManager::DENY_ACTION :
		  return DENY_ACTION;
	}
	return DENY_ACTION;
}

static GList *
impl_list_cookies (GaleonEmbedShell *shell)
{
        nsresult result;
        nsCOMPtr<nsICookieManager> cookieManager = 
                        do_GetService(NS_COOKIEMANAGER_CONTRACTID);
        nsCOMPtr<nsISimpleEnumerator> cookieEnumerator;
        result = 
            cookieManager->GetEnumerator(getter_AddRefs(cookieEnumerator));
        if (NS_FAILED(result)) return NULL;

	GList *cookies = NULL;
        PRBool enumResult;
        for (cookieEnumerator->HasMoreElements(&enumResult) ;
             enumResult == PR_TRUE ;
             cookieEnumerator->HasMoreElements(&enumResult))
        {
                nsCOMPtr<nsICookie> nsCookie;
                result = cookieEnumerator->GetNext(getter_AddRefs(nsCookie));
                if (NS_FAILED(result)) return NULL;

                CookieInfo *c = mozilla_cookie_to_info(nsCookie);
                cookies = g_list_prepend(cookies, c);
        }       

	return g_list_reverse(cookies);
}

static void
impl_remove_cookies (GaleonEmbedShell *shell,
		     GList *cookies,
		     gboolean block)
{
	nsresult result;
        nsCOMPtr<nsICookieManager> cookieManager =
                        do_GetService(NS_COOKIEMANAGER_CONTRACTID);
	
        for (GList *l = cookies; l != NULL; l = l->next)
        {
                CookieInfo *c = (CookieInfo *)l->data;

                result = cookieManager->Remove (GulDependentCString(c->domain),
                                                GulDependentCString(c->name),
                                                GulDependentCString(c->path),
                                                block);
                if (NS_FAILED(result)) return;
        };

	return;
}
	
static GList *
impl_list_passwords (GaleonEmbedShell *shell,
		     PasswordType type)
{
#ifdef HAVE_NSIPASSWORD_H
        nsresult result = NS_ERROR_FAILURE;

        nsCOMPtr<nsIPasswordManager> passwordManager =
                        do_GetService(NS_PASSWORDMANAGER_CONTRACTID);
        nsCOMPtr<nsISimpleEnumerator> passwordEnumerator;
        if (type == PASSWORD_PASSWORD)
                result = passwordManager->GetEnumerator 
                                (getter_AddRefs(passwordEnumerator));
        else if (type == PASSWORD_REJECT)
                result = passwordManager->GetRejectEnumerator 
                                (getter_AddRefs(passwordEnumerator));
        if (NS_FAILED(result)) return NULL;

	GList *passwords = NULL;

        PRBool enumResult;
        for (passwordEnumerator->HasMoreElements(&enumResult) ;
             enumResult == PR_TRUE ;
             passwordEnumerator->HasMoreElements(&enumResult))
        {
                nsCOMPtr<nsIPassword> nsPassword;
                result = passwordEnumerator->GetNext 
                                        (getter_AddRefs(nsPassword));
                if (NS_FAILED(result)) return NULL;

                PasswordInfo *p = g_new0 (PasswordInfo, 1);

                GulCString transfer;
                nsPassword->GetHost (transfer);
                p->host = g_strdup (transfer.get());

                if (type == PASSWORD_PASSWORD)
                {
                        GulString unicodeName;
                        nsPassword->GetUser (unicodeName);
                        p->username = g_strdup (GulCString (unicodeName).get());
                }

		passwords = g_list_prepend (passwords, p);
        }       
	return g_list_reverse (passwords);

#elif defined(HAVE_NSILOGINMANAGER_H)
	nsresult rv;
	GList *passwords = NULL;
	PRUint32 count,i;

	nsCOMPtr<nsILoginManager> loginManager =
			do_GetService (NS_LOGINMANAGER_CONTRACTID);
	NS_ENSURE_TRUE (loginManager, NULL);

	nsCOMPtr<nsIIDNService> idnService
		(do_GetService ("@mozilla.org/network/idn-service;1"));
	NS_ENSURE_TRUE (idnService, NULL);

	if (type == PASSWORD_REJECT) {
		PRUnichar **hostnames = nsnull;

		rv = loginManager->GetAllDisabledHosts (&count, &hostnames);
		NS_ENSURE_SUCCESS(rv, NULL);

		for (i = 0; i < count; i++) {
			nsCString host;
			nsString transfer (hostnames[i]);
			idnService->ConvertACEtoUTF8 (NS_ConvertUTF16toUTF8(transfer), host);

	                PasswordInfo *p = g_new0 (PasswordInfo, 1);
	                p->host = g_strdup(host.get());

			passwords = g_list_prepend (passwords, p);
		}

		NS_FREE_XPCOM_ALLOCATED_POINTER_ARRAY (count, hostnames);
	} else {
		nsILoginInfo **logins = nsnull;
		nsresult rv;

		rv = loginManager->GetAllLogins (&count, &logins);
		NS_ENSURE_SUCCESS(rv, NULL);

		for (i=0; i < count; i++) {
			nsString transfer;
			nsString unicodeName;
			rv = logins[i]->GetHostname (transfer);
			if (NS_FAILED (rv)) continue;

			nsCString host;
			if (transfer.IsVoid())
                  	host.SetIsVoid(PR_TRUE);
                	else
                  	idnService->ConvertACEtoUTF8 (NS_ConvertUTF16toUTF8(transfer), host);

			rv = logins[i]->GetHttpRealm (unicodeName);
			if (NS_FAILED (rv)) continue;
			nsCString httpRealm;
			if (unicodeName.IsVoid())
                  	httpRealm.SetIsVoid(PR_TRUE);
                	else
                  	NS_UTF16ToCString (unicodeName,
                                    	NS_CSTRING_ENCODING_UTF8, httpRealm);

			rv = logins[i]->GetUsername (unicodeName);
			if (NS_FAILED (rv)) continue;
			nsCString userName;
			if (unicodeName.IsVoid())
                  	userName.SetIsVoid(PR_TRUE);
                	else
                  	NS_UTF16ToCString (unicodeName,
                                    	NS_CSTRING_ENCODING_UTF8, userName);

			rv = logins[i]->GetUsernameField (unicodeName);
			if (NS_FAILED (rv)) continue;
			nsCString usernameField;
			if (unicodeName.IsVoid())
                  	usernameField.SetIsVoid(PR_TRUE);
                	else
                  	NS_UTF16ToCString (unicodeName,
                                    	NS_CSTRING_ENCODING_UTF8, usernameField);

			rv = logins[i]->GetPassword (unicodeName);
			if (NS_FAILED (rv)) continue;
			nsCString userPassword;
			if (unicodeName.IsVoid())
                  	userPassword.SetIsVoid(PR_TRUE);
                	else
                  	NS_UTF16ToCString (unicodeName,
                                    	NS_CSTRING_ENCODING_UTF8, userPassword);

			rv = logins[i]->GetPasswordField (unicodeName);
			if (NS_FAILED (rv)) continue;
			nsCString passwordField;
			if (unicodeName.IsVoid())
                  	passwordField.SetIsVoid(PR_TRUE);
                	else
                  	NS_UTF16ToCString (unicodeName,
                                    	NS_CSTRING_ENCODING_UTF8, passwordField);

			rv = logins[i]->GetFormSubmitURL (unicodeName);
			if (NS_FAILED (rv)) continue;
			nsCString formSubmitURL;
			if (unicodeName.IsVoid())
                  	formSubmitURL.SetIsVoid(PR_TRUE);
                	else
                  	NS_UTF16ToCString (unicodeName,
                                    	NS_CSTRING_ENCODING_UTF8, formSubmitURL);

			PasswordInfo *p = g_new0(PasswordInfo, 1);
			p->host = host.IsVoid() ? NULL: g_strdup(host.get());
			p->username = userName.IsVoid() ? NULL: g_strdup(userName.get());
			p->password = userPassword.IsVoid() ? NULL: g_strdup(userPassword.get());
			p->httpRealm = httpRealm.IsVoid() ? NULL : g_strdup(httpRealm.get());
			p->usernameField = usernameField.IsVoid() ? NULL : g_strdup(usernameField.get());
			p->passwordField = passwordField.IsVoid() ? NULL : g_strdup(passwordField.get());
			p->formSubmitURL = formSubmitURL.IsVoid() ? NULL : g_strdup(formSubmitURL.get());

			passwords = g_list_prepend (passwords, p);
		}

		NS_FREE_XPCOM_ISUPPORTS_POINTER_ARRAY (count, logins);
	}
	return passwords;
#else
	return NULL;
#endif
}

static void
impl_remove_passwords (GaleonEmbedShell *shell,
		       GList *passwords, 
		       PasswordType type)
{
#ifdef HAVE_NSIPASSWORD_H
	nsresult result = NS_ERROR_FAILURE;
        nsCOMPtr<nsIPasswordManager> passwordManager =
                        do_GetService(NS_PASSWORDMANAGER_CONTRACTID);

        for (GList *l = passwords; l != NULL; l = l->next)
        {
                PasswordInfo *p = (PasswordInfo *)l->data;
                if (type == PASSWORD_PASSWORD)
                {
                        result = passwordManager->RemoveUser (GulDependentCString(p->host),
							      GulString (p->username));
                }
                else if (type == PASSWORD_REJECT)
                {
                        result = passwordManager->RemoveReject 
				(GulDependentCString(p->host));
                };

                if (NS_FAILED(result)) return;
        };

#elif defined (HAVE_NSILOGINMANAGER_H)
      	nsCOMPtr<nsILoginManager> loginManager =
		do_GetService (NS_LOGINMANAGER_CONTRACTID);
	NS_ENSURE_TRUE (loginManager, );

	if (type == PASSWORD_REJECT) {
	        for (GList *l = passwords; l != NULL; l = l->next)
        	{
                	PasswordInfo *info = (PasswordInfo *)l->data;

			nsString host;
			NS_CStringToUTF16(nsCString(info->host),
					  NS_CSTRING_ENCODING_UTF8, host);
			loginManager->SetLoginSavingEnabled(host, TRUE);
		}
	} else {
        	for (GList *l = passwords; l != NULL; l = l->next)
        	{
                	PasswordInfo *info = (PasswordInfo *)l->data;

			nsString userName;

	        	if (info->username)
	          	NS_CStringToUTF16 (nsCString(info->username),
        	                    	NS_CSTRING_ENCODING_UTF8, userName);
        		else
          		userName.SetIsVoid (PR_TRUE);

			nsString host;
			nsString userNameField;
			nsString password;
			nsString passwordField;
			nsString httpRealm;
			nsString formSubmitURL;

        		if (info->host)
          		NS_CStringToUTF16 (nsCString(info->host),
                            		NS_CSTRING_ENCODING_UTF8, host);
        		else
          		host.SetIsVoid (PR_TRUE);

        		if (info->usernameField)
          		NS_CStringToUTF16 (nsCString(info->usernameField),
                            		NS_CSTRING_ENCODING_UTF8, userNameField);
        		else
          		userNameField.SetIsVoid (PR_TRUE);

        		if (info->httpRealm)
          		NS_CStringToUTF16 (nsCString(info->httpRealm),
                            		NS_CSTRING_ENCODING_UTF8, httpRealm);
        		else
          		httpRealm.SetIsVoid (PR_TRUE);

        		if (info->password)
          		NS_CStringToUTF16 (nsCString(info->password),
                            		NS_CSTRING_ENCODING_UTF8, password);
        		else
          		password.SetIsVoid (PR_TRUE);

        		if (info->passwordField)
          		NS_CStringToUTF16 (nsCString(info->passwordField),
                            		NS_CSTRING_ENCODING_UTF8, passwordField);
        		else
          		passwordField.SetIsVoid (PR_TRUE);

        		if (info->formSubmitURL)
          		NS_CStringToUTF16 (nsCString(info->formSubmitURL),
                            		NS_CSTRING_ENCODING_UTF8, formSubmitURL);
        		else
          		formSubmitURL.SetIsVoid (PR_TRUE);

			nsCOMPtr<nsILoginInfo> login
				(do_CreateInstance(NS_LOGININFO_CONTRACTID));

			login->SetUsername(userName);
			login->SetUsernameField(userNameField);
			login->SetHostname(host);
			login->SetHttpRealm(httpRealm);
			login->SetFormSubmitURL(formSubmitURL);
			login->SetPassword(password);
			login->SetPasswordField(passwordField);

			loginManager->RemoveLogin(login);
		}
	}
#endif
}

static void
mozilla_embed_shell_class_init (MozillaEmbedShellClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GaleonEmbedShellClass *shell_class;
	
	shell_class = GALEON_EMBED_SHELL_CLASS (klass);
	
        object_class->finalize = mozilla_embed_shell_finalize;

	shell_class->initialize = impl_initialize;
	shell_class->clear_cache = impl_clear_cache;
	shell_class->set_offline_mode = impl_set_offline_mode;
	shell_class->show_java_console = impl_show_java_console;
	shell_class->get_encodings   = impl_get_encodings;
	shell_class->get_font_langs = impl_get_font_langs;
	shell_class->get_font_list = impl_get_font_list;
	shell_class->set_permission = impl_set_permission;
	shell_class->list_permissions = impl_list_permissions;
	shell_class->remove_permissions = impl_remove_permissions;
	shell_class->test_permission = impl_test_permission;
	shell_class->list_cookies = impl_list_cookies;
	shell_class->remove_cookies = impl_remove_cookies;
	shell_class->list_passwords = impl_list_passwords;
	shell_class->remove_passwords = impl_remove_passwords;

	g_type_class_add_private (klass, sizeof (MozillaEmbedShellPrivate));
}

GaleonEmbedShell  *
galeon_embed_shell_new()
{
	GaleonEmbedShell *shell;
	
	shell = GALEON_EMBED_SHELL (g_object_new (MOZILLA_TYPE_EMBED_SHELL, NULL));

	return shell;
}
