/*
 *  Copyright (C) 2002 Christophe Fergeau
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GUL_NOTEBOOK_H
#define GUL_NOTEBOOK_H

#include <glib.h>
#include <gtk/gtknotebook.h>

G_BEGIN_DECLS

typedef struct GulNotebookClass GulNotebookClass;

#define GUL_TYPE_NOTEBOOK             (gul_notebook_get_type ())
#define GUL_NOTEBOOK(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GUL_TYPE_NOTEBOOK, GulNotebook))
#define GUL_NOTEBOOK_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GUL_TYPE_NOTEBOOK, GulNotebookClass))
#define GUL_IS_NOTEBOOK(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GUL_TYPE_NOTEBOOK))
#define GUL_IS_NOTEBOOK_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GUL_NOTEBOOK))

typedef struct GulNotebook GulNotebook;
typedef struct GulNotebookPrivate GulNotebookPrivate;

enum
{
	GUL_NOTEBOOK_INSERT_LAST = -1,
	GUL_NOTEBOOK_INSERT_GROUPED = -2
};

struct GulNotebook 
{
	GtkNotebook parent;
        GulNotebookPrivate *priv;
};

struct GulNotebookClass
{
        GtkNotebookClass parent_class;

	/* Signals */
	void (* tab_added)	   (GulNotebook *notebook,
				    GtkWidget *child);

	void (* tab_removed)	   (GulNotebook *notebook,
				    GtkWidget *child);

	void (* tabs_reordered)	   (GulNotebook *notebook);

	void (* tab_detached)      (GulNotebook *dest,
				    gint cur_page,
				    gint root_x, gint root_y);

	void (* tab_delete)        (GulNotebook *dest,
				    GtkWidget *child);

};

GType 		gul_notebook_get_type 		(void);

GtkWidget      *gul_notebook_new 		(void);

void		gul_notebook_insert_page	(GulNotebook *nb,
						 GtkWidget *child,
						 int position,
						 gboolean jump_to);

void		gul_notebook_remove_page	(GulNotebook *nb,
						 GtkWidget *child);

void            gul_notebook_move_page          (GulNotebook *src, 
						 GulNotebook *dest,
						 GtkWidget *src_page, 
						 gint dest_page);

void		gul_notebook_set_page_color 	(GulNotebook *nb,
			      			 GtkWidget *child,
			      			 GdkColor *color);

void		gul_notebook_set_page_title 	(GulNotebook *nb,
			      			 GtkWidget *child,
			      			 const char *title);
 
void		gul_notebook_set_policy		(GulNotebook *nb,
		                                 GtkPolicyType policy);

void		gul_notebook_set_page_icon	(GulNotebook *nb,
						 GtkWidget *child,
						 GtkWidget *icon);

G_END_DECLS

#endif /* GUL_NOTEBOOK_H */
