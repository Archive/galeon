/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "eel-gconf-extensions.h"
#include "galeon-embed-utils.h"
#include "galeon-embed-shell.h"
#include "prefs-strings.h"
#include "gul-gui.h"
#include "gul-download.h"
#include "gul-string.h"
#include "galeon-config.h"
#include "gul-file-chooser.h"

#include <string.h>
#include <glib/gi18n.h>
#include <gtk/gtkmain.h>
#include <gtk/gtkdialog.h>
#include <gtk/gtkmessagedialog.h>
#include <libgnomevfs/gnome-vfs-uri.h>
#include <libgnomevfs/gnome-vfs-utils.h>

static gint stylesheet_cmp(gconstpointer a, gconstpointer b);

/* FIXME show a message about saved file in complete callback ?*/

void
galeon_embed_utils_save_event_property(GaleonEmbed *embed,
				       GaleonEmbedEvent *event,
				       gboolean always_ask_dest,
				       gboolean show_progress,
				       const char *fc_title,
				       const char *property)
{
	const char *location;
	const GValue *value;
	GtkWidget *window;
	GaleonEmbedPersist *persist;
	gint flags;
	
	g_return_if_fail (GALEON_IS_EMBED_EVENT(event));
	g_return_if_fail (GALEON_IS_EMBED(embed));
	
	value = galeon_embed_event_get_property (event, property);
	location = g_value_get_string (value);

	window = gtk_widget_get_toplevel (GTK_WIDGET(embed));
	g_return_if_fail(window != NULL);
	
	persist = galeon_embed_persist_new (embed);
	galeon_embed_persist_set_fc_parent (persist, window);
	galeon_embed_persist_set_source (persist, location);

	flags = EMBED_PERSIST_ADD_TO_RECENT;
	if (show_progress)
	{
		flags |= EMBED_PERSIST_SHOW_PROGRESS;
	}
	if (always_ask_dest)
	{
		flags |= EMBED_PERSIST_ASK_DESTINATION;
	}
	galeon_embed_persist_set_flags (persist, (EmbedPersistFlags)flags);

	if (fc_title)
	{
		galeon_embed_persist_set_fc_title (persist, fc_title);
	}

	galeon_embed_persist_set_user_time (persist, gtk_get_current_event_time());
	galeon_embed_persist_save (persist);
	g_object_unref (persist);
}

void
galeon_embed_utils_download_event_property(GaleonEmbed *embed,
					   GaleonEmbedEvent *event,
					   gboolean always_ask_dest,
					   const char *fc_title,
					   const char *property)
{
       if (eel_gconf_get_boolean(CONF_DOWNLOADING_EXTERNAL_DOWNLOADER) ==
            DOWNLOADER_EXTERNAL)
        {
		const char *location;
		const GValue *value;

		value = galeon_embed_event_get_property (event, property);
		location = g_value_get_string (value);

		gul_download_external_save_url (location);
        }
	else
	{
		galeon_embed_utils_save_event_property (embed,event,
							always_ask_dest,
							TRUE,
							fc_title,
							property);
	}
}

/**
 * galeon_embed_utils_handlernotfound_dialog_run:
 * @parent: the dialog parent window
 *
 * Show a dialog to warn the user that no application capable
 * to open the specified file are found. Used in the downloader
 * and in the mime type dialog.
 **/
void
galeon_embed_utils_nohandler_dialog_run (GtkWidget *parent)
{
        GtkWidget *dialog;
        
	/* FIXME mime db shortcut */
	
	dialog = gtk_message_dialog_new 
		(GTK_WINDOW(parent), 
                 GTK_DIALOG_MODAL,
                 GTK_MESSAGE_ERROR,
                 GTK_BUTTONS_OK,
                 _("No available applications to open "
                   "the specified file."));
        gtk_dialog_run (GTK_DIALOG(dialog));
	gtk_widget_destroy (dialog);
}

/**
 * galeon_embed_utils_free_page_properties
 * @props: Properties struct to free
 *
 * PageProperties is a complex struct whos members need freeing.
 * Here's a helper to do the job for you.
 **/
void
galeon_embed_utils_free_page_properties(EmbedPageProperties *props)
{
	/* FIXME: Will add fields as they get hooked up */

	g_free(props->content_type);
	g_free(props->encoding);
	g_free(props->referring_url);
	g_free(props->cipher_name);
	g_free(props->cert_issuer_name);

	if (props->metatags)
	{
		GList *i;
		for(i = props->metatags ; i ; i = i->next)
		{
			g_free(((EmbedPageMetaTag *)i->data)->name);
			g_free(((EmbedPageMetaTag *)i->data)->content);
			g_free(i->data);
		}
		g_list_free(props->metatags);
	}

	if (props->links)
	{
		GList *i;
		for(i = props->links ; i ; i = i->next)
		{
			g_free(((EmbedPageLink *)i->data)->url);
			g_free(((EmbedPageLink *)i->data)->title);
			g_free(((EmbedPageLink *)i->data)->rel);
			g_free(i->data);
		}
		g_list_free(props->links);
	}

	if (props->forms)
	{
		GList *i;
		for(i = props->forms ; i ; i = i->next)
		{
			g_free(((EmbedPageForm *)i->data)->name);
			g_free(((EmbedPageForm *)i->data)->method);
			g_free(((EmbedPageForm *)i->data)->action);
			g_free(i->data);
		}
		g_list_free(props->forms);
	}

	if (props->images)
	{
		GList *i;
		for(i = props->images ; i ; i = i->next)
		{
			g_free(((EmbedPageImage *)i->data)->url);
			g_free(((EmbedPageImage *)i->data)->alt);
			g_free(((EmbedPageImage *)i->data)->title);
			g_free(i->data);
		}
		g_list_free(props->images);
	}

	/* As these lists are properly defined, more
	 * complicated free procedures will be needed
	 */
	g_list_free(props->stylesheets);
	g_free(props);
}

gint
stylesheet_cmp(gconstpointer a, gconstpointer b)
{
   return g_utf8_collate(((EmbedStyleSheet *)a)->name,
			 ((EmbedStyleSheet *)b)->name);
}

/**
 * galeon_embed_utils_get_user_stylesheets
 *
 * Returns: GList of EmbedStyleSheets.
 *
 * Create a list of the available user stylesheets.
 * List is owned by caller.
 *
 **/
 
GList *
galeon_embed_utils_get_user_stylesheets(void)
{
	GList *retList = NULL;

	const gchar *file = NULL;
	gchar *path = g_build_filename(g_get_home_dir(),
				       GALEON_DIR, "stylesheets",
				       NULL);
	GDir *dir = g_dir_open(path, 0, NULL);
	g_free(path);
	if (!dir) return NULL;

	while ((file = g_dir_read_name(dir)))
	{
		if (g_str_has_suffix(file, "css"))
		{
			EmbedStyleSheet *sheet;
			char            *name;
		       
			/* FIXME: Use better description, like strip the .css
			 * extension, or read a tagline embedded in the
			 * stylesheet
			 */
			name = g_filename_to_utf8(file, -1, NULL, NULL, NULL);

			sheet = galeon_embed_stylesheet_new(STYLESHEET_USER, name);
			g_free(name);

			retList = g_list_insert_sorted(retList, sheet,
						       stylesheet_cmp);
		}
	}
	g_dir_close(dir);

	if (retList)
	{
		EmbedStyleSheet *sheet;

		sheet = galeon_embed_stylesheet_new(STYLESHEET_USER_NONE,
						    _("No User Stylesheet"));

		retList = g_list_prepend(retList, sheet);
	}

	return retList;
}

/**
 * galeon_embed_utils_get_user_sheet_path
 * @sheetfile: Filename of stylesheet to load
 *
 * Returns: full path of filename or NULL if file doesn't
 *          exist.
 *
 * Expand a user stylesheet filename to a full path.
 * Takes a utf8 filename and returns a utf8 path.
 *
 **/
gchar *
galeon_embed_utils_get_user_sheet_path(const gchar *sheetfile)
{
	gchar *utf8_path = NULL;
	gchar *filename = g_filename_from_utf8(sheetfile, -1,
					       NULL, NULL, NULL);
	gchar *path = g_build_filename("/", g_get_home_dir(),
				       GALEON_DIR, "stylesheets",
				       filename, NULL);
	g_free(filename);
	
	if (g_file_test(path, G_FILE_TEST_EXISTS))
	{
		utf8_path = g_filename_to_utf8(path, -1,
					       NULL, NULL, NULL);
	}
	g_free(path);

	return utf8_path;
}
