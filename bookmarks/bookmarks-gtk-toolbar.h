/*
 *  Copyright (C) 2003  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __bookmarks_gtk_toolbar_h
#define __bookmarks_gtk_toolbar_h

#include <gtk/gtktoolbar.h>
#include <gtk/gtkstatusbar.h>
#include "bookmarks.h"
#include "bookmarks-util.h"
#include "bookmarks-location-source.h"

/* object forward declarations */

typedef struct _GbGtkToolbar GbGtkToolbar;
typedef struct _GbGtkToolbarClass GbGtkToolbarClass;
typedef struct _GbGtkToolbarPrivate GbGtkToolbarPrivate;

/**
 * GbGtkToolbar object
 */

#define GB_TYPE_GTK_TOOLBAR			(gb_gtk_toolbar_get_type())
#define GB_GTK_TOOLBAR(object)			(G_TYPE_CHECK_INSTANCE_CAST((object), GB_TYPE_GTK_TOOLBAR,\
						 GbGtkToolbar))
#define GB_GTK_TOOLBAR_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), GB_TYPE_GTK_TOOLBAR,\
						 GbGtkToolbarClass))
#define GB_IS_GTK_TOOLBAR(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), GB_TYPE_GTK_TOOLBAR))
#define GB_IS_GTK_TOOLBAR_ITEM_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), GB_TYPE_GTK_TOOLBAR))
#define GB_GTK_TOOLBAR_GET_CLASS(obj) 		(G_TYPE_INSTANCE_GET_CLASS((obj), GB_TYPE_GTK_TOOLBAR,\
						 GbGtkToolbarClass))

struct _GbGtkToolbarClass
{
	GtkToolbarClass parent_class;

	/* signals */
	GbBookmarkActivatedCallback gb_gtk_toolbar_bookmark_activated;

};

/* Remember: fields are public read-only */
struct _GbGtkToolbar
{
	GtkToolbar parent_object;
	
	GbGtkToolbarPrivate *priv;
};

GType		gb_gtk_toolbar_get_type				(void);
GbGtkToolbar *	gb_gtk_toolbar_new				(GbFolder *root);
void		gb_gtk_toolbar_set_location_source		(GbGtkToolbar *gm, GbLocationSource *src);
void		gb_gtk_toolbar_set_statusbar			(GbGtkToolbar *gt, GtkStatusbar *statusbar);

#endif
