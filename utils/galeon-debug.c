/*
 *  Copyright (C) 2003 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "galeon-debug.h"

#include <glib.h>
#include <string.h>

#ifndef DISABLE_PROFILING

static GHashTable *galeon_profilers_hash = NULL;
static const char *galeon_profile_modules = NULL;

#endif

#ifndef DISABLE_LOGGING

static const char *galeon_log_modules;

static void
log_module (const gchar *log_domain,
	    GLogLevelFlags log_level,
	    const gchar *message,
	    gpointer user_data)
{
	gboolean should_log = FALSE;

	if (!galeon_log_modules) return;

	if (strcmp (galeon_log_modules, "all") != 0)
	{
		char **modules;
		int i;

		modules = g_strsplit (galeon_log_modules, ":", 100);

		for (i = 0; modules[i] != NULL; i++)
		{
			if (strstr (message, modules [i]) != NULL)
			{
				should_log = TRUE;
				break;
			}
		}

		g_strfreev (modules);
	}
	else
	{
		should_log = TRUE;
	}

	if (should_log)
	{
		g_print ("%s\n", message);
	}
}

#endif

void
galeon_debug_init (void)
{
#ifndef DISABLE_LOGGING
	galeon_log_modules = g_getenv ("GALEON_LOG_MODULES");

	g_log_set_handler (G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG, log_module, NULL);
#endif
#ifndef DISABLE_PROFILING
	galeon_profile_modules = g_getenv ("GALEON_PROFILE_MODULES");
#endif
}

#ifndef DISABLE_PROFILING

typedef struct
{
	GTimer *timer;
	char *name;
	char *module;
} GaleonProfiler;

static GaleonProfiler *
galeon_profiler_new (const char *name, const char *module)
{
	GaleonProfiler *profiler;

	profiler = g_new0 (GaleonProfiler, 1);
	profiler->timer = g_timer_new ();
	profiler->name  = g_strdup (name);
	profiler->module  = g_strdup (module);

	g_timer_start (profiler->timer);

	return profiler;
}

static gboolean
galeon_should_profile (const char *module)
{
	char **modules;
	int i;
	gboolean res = FALSE;

	if (!galeon_profile_modules) return FALSE;
	if (strcmp (galeon_profile_modules, "all") == 0) return TRUE;

	modules = g_strsplit (galeon_profile_modules, ":", 100);

	for (i = 0; modules[i] != NULL; i++)
	{
		if (strcmp (module, modules [i]) == 0)
		{
			res = TRUE;
			break;
		}
	}

	g_strfreev (modules);

	return res;
}

static void
galeon_profiler_dump (GaleonProfiler *profiler)
{
	double seconds;

	g_return_if_fail (profiler != NULL);

	seconds = g_timer_elapsed (profiler->timer, NULL);

	g_print ("[ %s ] %s %f s elapsed\n",
		 profiler->module, profiler->name,
		 seconds);
}

static void
galeon_profiler_free (GaleonProfiler *profiler)
{
	g_return_if_fail (profiler != NULL);

	g_timer_destroy (profiler->timer);
	g_free (profiler->name);
	g_free (profiler->module);
	g_free (profiler);
}

void
galeon_profiler_start (const char *name, const char *module)
{
	GaleonProfiler *profiler;

	if (galeon_profilers_hash == NULL)
	{
		galeon_profilers_hash =
			g_hash_table_new_full (g_str_hash, g_str_equal,
					       g_free, NULL);
	}

	if (!galeon_should_profile (module)) return;

	profiler = galeon_profiler_new (name, module);

	g_hash_table_insert (galeon_profilers_hash, g_strdup (name), profiler);
}

void
galeon_profiler_stop (const char *name)
{
	GaleonProfiler *profiler;

	profiler = g_hash_table_lookup (galeon_profilers_hash, name);
	if (profiler == NULL) return;
	g_hash_table_remove (galeon_profilers_hash, name);

	galeon_profiler_dump (profiler);
	galeon_profiler_free (profiler);
}

#endif
