/*
 *  Copyright (C) 2002 Jorn Baayen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <glib-object.h>
#include <gtk/gtkimage.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include "galeon-favicon-cache.h"

#ifndef __GALEON_FAVICON_H
#define __GALEON_FAVICON_H

G_BEGIN_DECLS

#define GALEON_TYPE_FAVICON         (galeon_favicon_get_type ())
#define GALEON_FAVICON(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), GALEON_TYPE_FAVICON, GaleonFavicon))
#define GALEON_FAVICON_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST ((k), GALEON_TYPE_FAVICON, GaleonFaviconClass))
#define GALEON_IS_FAVICON(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), GALEON_TYPE_FAVICON))
#define GALEON_IS_FAVICON_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), GALEON_TYPE_FAVICON))
#define GALEON_FAVICON_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), GALEON_TYPE_FAVICON, GaleonFaviconClass))

typedef struct GaleonFaviconPrivate GaleonFaviconPrivate;

typedef struct
{
	GtkImage parent;

	GaleonFaviconPrivate *priv;
} GaleonFavicon;

typedef struct
{
	GtkImageClass parent_class;

	void (*changed) (GaleonFavicon *favicon);
} GaleonFaviconClass;

GType       galeon_favicon_get_type (void);

GtkWidget  *galeon_favicon_new      (const char *url);

void        galeon_favicon_set_url  (GaleonFavicon *favicon,
				     const char *url);

char *      galeon_favicon_get_url  (GaleonFavicon *favicon);


gboolean    galeon_favicon_get_is_default (GaleonFavicon *favicon);

G_END_DECLS

#endif /* __GALEON_FAVICON_H */
