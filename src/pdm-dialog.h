/*
 *  Copyright (C) 2002 Jorn Baayen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef PDM_DIALOG_H
#define PDM_DIALOG_H

#include "galeon-dialog.h"
#include <glib.h>

G_BEGIN_DECLS

typedef struct PdmDialog PdmDialog;
typedef struct PdmDialogClass PdmDialogClass;

#define TYPE_PDM_DIALOG             (pdm_dialog_get_type ())
#define PDM_DIALOG(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_PDM_DIALOG, PdmDialog))
#define PDM_DIALOG_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), PDM_DIALOG, PdmDialogClass))
#define IS_PDM_DIALOG(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_PDM_DIALOG))
#define IS_PDM_DIALOG_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), PDM_DIALOG))

typedef struct PdmDialogPrivate PdmDialogPrivate;

typedef enum {
	PDM_PAGE_COOKIES,
	PDM_PAGE_COOKIE_SITES,
	PDM_PAGE_IMAGE_SITES,
	PDM_PAGE_PASSWORDS,
	PDM_PAGE_PASSWORD_SITES,
	PDM_PAGE_POPUP_SITES
} PdmDialogPage;

struct PdmDialog
{
        GaleonDialog parent;
        PdmDialogPrivate *priv;
};

struct PdmDialogClass
{
        GaleonDialogClass parent_class;
};

GType         pdm_dialog_get_type         (void);

GaleonDialog *pdm_dialog_new              (GtkWidget *window);

void          pdm_dialog_set_current_page (PdmDialog     *dialog,
		                           PdmDialogPage  page);

G_END_DECLS

#endif

