/*
 *  Copyright (C) 2000 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef NOT_PORTED
typedef enum
{
        DOWNLOADER_BUILTIN  = 0,
        DOWNLOADER_EXTERNAL = 1,
        DOWNLOADER_GTM      = 2
} Downloader;
#endif

typedef gboolean Downloader;
#define DOWNLOADER_BUILTIN FALSE
#define DOWNLOADER_EXTERNAL TRUE

void gul_download_external_save_url (const gchar *url);

#ifdef __cplusplus
}
#endif
