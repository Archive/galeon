/*
 *  Copyright © 2005 Gustavo Gama
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#ifndef GALEON_ACTIVATION_H
#define GALEON_ACTIVATION_H

#include "galeon-dbus.h"

G_BEGIN_DECLS

/* activation handlers */
gboolean galeon_activation_load_uri_list         (GaleonDbus *galeon_dbus,
						  char **uris,
						  char *options,
						  guint startup_id,
						  GError **error);

gboolean galeon_activation_load_session		 (GaleonDbus *galeon_dbus,
						  char *session_name,
						  guint user_time,
						  GError **error);

gboolean galeon_activation_quit                  (GaleonDbus *galeon_dbus,
						  gboolean exit_server,
						  GError **error);
G_END_DECLS

#endif 
