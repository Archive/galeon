/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GTKHTML_EMBED_PERSIST_H
#define GTKHTML_EMBED_PERSIST_H

#ifdef __cplusplus
extern "C" {
#endif

#include "galeon-embed-persist.h"
	
#include <glib-object.h>
#include <glib.h>

typedef struct GtkhtmlEmbedPersistClass GtkhtmlEmbedPersistClass;

#define GTKHTML_TYPE_EMBED_PERSIST             (gtkhtml_embed_persist_get_type ())
#define GTKHTML_EMBED_PERSIST(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTKHTML_TYPE_EMBED_PERSIST, GtkhtmlEmbedPersist))
#define GTKHTML_EMBED_PERSIST_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GTKHTML_PERSIST_SHELL, GtkhtmlEmbedPersistClass))
#define GTKHTML_IS_EMBED_PERSIST(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTKHTML_TYPE_EMBED_PERSIST))
#define GTKHTML_IS_EMBED_PERSIST_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GTKHTML_EMBED_PERSIST))
#define GTKHTML_EMBED_PERSIST_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GTKHTML_TYPE_EMBED_SHELL, GtkhtmlEmbedPersistClass))

typedef struct GtkhtmlEmbedPersist GtkhtmlEmbedPersist;
typedef struct GtkhtmlEmbedPersistPrivate GtkhtmlEmbedPersistPrivate;

struct GtkhtmlEmbedPersist 
{
        GaleonEmbedPersist parent;
        GtkhtmlEmbedPersistPrivate *priv;
};

struct GtkhtmlEmbedPersistClass
{
        GaleonEmbedPersistClass parent_class;
};

GType               gtkhtml_embed_persist_get_type   (void);

#ifdef __cplusplus
}
#endif
#endif
