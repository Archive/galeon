/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __galeon_autocompletion_window_h_
#define __galeon_autocompletion_window_h_

#include <glib-object.h>
#include <gtk/gtkwidget.h>
#include "galeon-autocompletion.h"

/* object forward declarations */

typedef struct _GaleonAutocompletionWindow GaleonAutocompletionWindow;
typedef struct _GaleonAutocompletionWindowClass GaleonAutocompletionWindowClass;
typedef struct _GaleonAutocompletionWindowPrivate GaleonAutocompletionWindowPrivate;

/**
 * Editor object
 */

#define GALEON_TYPE_AUTOCOMPLETION_WINDOW	(galeon_autocompletion_window_get_type())
#define GALEON_AUTOCOMPLETION_WINDOW(object)	(G_TYPE_CHECK_INSTANCE_CAST((object), \
						 GALEON_TYPE_AUTOCOMPLETION_WINDOW,\
						 GaleonAutocompletionWindow))
#define GALEON_AUTOCOMPLETION_WINDOW_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), \
						   GALEON_TYPE_AUTOCOMPLETION_WINDOW,\
					 	   GaleonAutocompletionWindowClass))
#define GALEON_IS_AUTOCOMPLETION_WINDOW(object)	(G_TYPE_CHECK_INSTANCE_TYPE((object), \
						 GALEON_TYPE_AUTOCOMPLETION_WINDOW))
#define GALEON_IS_AUTOCOMPLETION_WINDOW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), \
						      GALEON_TYPE_AUTOCOMPLETION_WINDOW))
#define GALEON_AUTOCOMPLETION_WINDOW_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), \
						     GALEON_TYPE_AUTOCOMPLETION_WINDOW,\
						     GaleonAutocompletionWindowClass))

struct _GaleonAutocompletionWindowClass 
{
	GObjectClass parent_class;

	/* signals */
	void		(*hidden)			(GaleonAutocompletionWindow *aw);
	void		(*url_activated)		(GaleonAutocompletionWindow *aw, 
							 const gchar *item,
							 gboolean new_tab_or_window);

};

/* Remember: fields are public read-only */
struct _GaleonAutocompletionWindow
{
	GObject parent_object;

	GaleonAutocompletionWindowPrivate *priv;
};

GType			galeon_autocompletion_window_get_type	(void);
GaleonAutocompletionWindow *galeon_autocompletion_window_new	(GaleonAutocompletion *ac, 
								 GtkWidget *parent);
void 			galeon_autocompletion_window_set_parent_widget (GaleonAutocompletionWindow *aw,
									GtkWidget *w);
void			galeon_autocompletion_window_set_autocompletion (GaleonAutocompletionWindow *aw, 
									 GaleonAutocompletion *ac);
void			galeon_autocompletion_window_show	(GaleonAutocompletionWindow *aw);
void			galeon_autocompletion_window_hide	(GaleonAutocompletionWindow *aw);
void			galeon_autocompletion_window_unselect	(GaleonAutocompletionWindow *aw);

#endif
