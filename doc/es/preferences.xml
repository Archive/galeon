<!-- <!DOCTYPE Chapter PUBLIC "-//GNOME//DTD DocBook PNG Variant V1.1//EN"> -->
<chapter id="preferences" lang="es">
  <title>Establecer preferencias de Galeon</title>
  <para>
    <application>Galeon</application> es muy configurable.  La ventana de
    preferencias es donde encontrará todas las opciones disponibles
    para personalizar su navegador.  Puede abrirlo pulsando el elemento
    <guimenuitem>Preferencias</guimenuitem> en el menú <link
    linkend="menubar-edit"><guimenu>Editar</guimenu></link>.  En el
    lado izquierdo, hay una una lista de varias categorías diferentes
    de opciones. Cuando se pulsa en una subcategoría, su página
    se mostrará en la ventana.
  </para>

  <para>
  <application>Galeon</application> usa la configuración de GNOME para controlar
  el tema para la barra de herramientas, iconos y logotipo giratorio. Debería poder cambiar
  esta configuración usando el <application>Centro de control de GNOME</application>.
  </para>

  <sect1 id="prefs-general">
    <title>General</title>

      <para>
        <variablelist>

          <varlistentry>
	    <term>Página inicial</term>
	    <listitem><para>
	      Cuando <application>Galeon</application> se inicia por primera vez, 
	      cargará automáticamente la página especificada aquí. Teclee una 
		dirección en la barra de entrada, o si está viendo una página actualmente
	      que quiera establecer como su página inicial permanente, pulse en 
	      botón <guibutton>Establecer a la actual</guibutton>.  Si quiere
              que la página «My Portal» sea su página inicial, teclee <userinput>myportal:</userinput>.
       	    </para></listitem>
	  </varlistentry>

          <varlistentry>
	    <term>En las páginas nuevas</term>
	    <listitem><para>
	      Al abrir una ventana nueva (o solapa), puede tener una página cargada
		automáticamente.  Puede elegir abrir su página inicial, la última página que vio,
	        o una página en blanco.
       	    </para></listitem>
	  </varlistentry>

          <varlistentry>
	    <term>Guardar sesión al salir</term>
	    <listitem><para>
	      Si se marca el botón <guibutton>Guardar sesión al salir</guibutton>
	      su sesión siempre se guardará cuando salga de <application>Galeon</application>.
       	    </para></listitem>
	  </varlistentry>

        </variablelist>
      </para>
    </sect1>

    <sect1 id="prefs-language">
    <title>Idioma</title>
      <para>
        <variablelist>
          <varlistentry>

	    <term>Idiomas</term>
	    <listitem><para>
	      Esta sección le permite gestionar los idiomas que 
	      <application>Galeon</application> solicita a los sitios web.
              Esta opción ayuda a un sitio web a devolver el idioma correcto.
            </para>
            <para>
              Para alterar la lista de idiomas, elija <guibutton>Editar</guibutton>.
              Esto abre un diálogo que permite reordenar la lista y añadir y quitar idiomas
	      nuevos.
       	    </para></listitem>
	  </varlistentry>

          <varlistentry>
	    <term>Conjuntos de códigos</term>
	    <listitem><para>
	      Puede seleccionar el conjunto de códigos que será usado 
	      para el texto aquí, y si esta configuración debería ser auto-detectada.
	    </para></listitem>
	  </varlistentry>
        </variablelist>
      </para>
    </sect1>

    <sect1 id="prefs-fonts">
    <title>Tipografía</title>
      <para>
        <variablelist>

          <varlistentry>
	    <term>Tipografía</term>
	    <listitem><para>
	      En esta sección, puede personalizar las tipografías que 
	      el motor de renderizado <application>Mozilla</application>
	  	usará al mostrar las páginas en <application>Galeon</application>.
	      Si el botón <guibutton>Siempre usar estas tipografías</guibutton> 
	      está marcado, estas tipografías serán usadas en todas las páginas; 
	      en otro caso, sólo se usarán en las páginas que no especifiquen
	      su propias tipografías.
	      Los tamaños de tipo seleccionadas en las cajas de diálogo abiertas por 
		los botones <guibutton>Serif</guibutton> y<guibutton>Monospace
		</guibutton> determinan los tamaños predeterminados
	      para todas las tipografías de anchura fija y variable,
	      respectivamente.  El botón incremental de <guibutton>Tamaño min.</guibutton>
	      permite especificar el tamaño de punto mínimo al que se muestran las 
		tipografías. Puede seleccionarse el tipo de tipografía predeterminado
	      Serif o Sans Serif.  Todas estas opciones pueden seleccionarse para cada 
	      una de las codificaciones listadas en la caja <guilabel>Codificación del idioma</guilabel>
	      <tip>
	        <title>Truco</title>
		<para>
		  Asegúrese de que selecciona el conjunto de caracteres apropiado en los 
		  visualizadores de tipografía para el idioma que está usando, 
		  (<literal>iso8859-15</literal> para los idiomas de Europa occidental).
		   Si no lo hace, su configuración puede ser ignorada.)
		</para>
	      </tip>
       	    </para></listitem>
	  </varlistentry>
        </variablelist>
      </para>
    </sect1>

    <sect1 id="prefs-tabs">
    <title>Solapas</title>
      <para>
        <variablelist>

          <varlistentry>
	    <term>Visualización con solapas</term>
	    <listitem>
	    <para>
	      <application>Galeon</application> funciona en un modo de visualización «híbrido».
	      Cada ventana del navegador contiene un clasificador que por sí mismo
	      puede mostrar páginas múltiples. Puedes encontrar más información acerca de esta
	      característica en <xref linkend="tabbed-browsing"/>.
	    </para>
	    <para>
	      La opción <guibutton>Abrir en solapas por omisión</guibutton> 
	      hace que los enlaces en los que se pulsa con el botón central del ratón
	      se abran en solapas nuevas, en vez de en  ventanas nuevas.
	      La opción <guibutton>Saltar a solapas nuevas automáticamente</guibutton> hace
	      que <application>Galeon</application> cambie automáticamente a las solapas
	      creadas.
              La opción <guibutton>Mostrar barra de solapas si sólo una solapa está abierta</guibutton> 
	  	hace que la lista de solapas se muestre incluso cuando sólo una solapa existe en la ventana,	 
	    </para>
	    <para>
	      La opción <guibutton>Abrir emergentes en solapas</guibutton> hace que las ventanas emergentes 
	      abiertas por las páginas aparezcan en solapas, en vez de en ventanas nuevas.
	    </para>
	    </listitem>
	  </varlistentry>

        </variablelist>
      </para>
    </sect1>

    <sect1 id="prefs-mouse">
    <title>Ratón</title>
      <para>
        <variablelist>

          <varlistentry>
	    <term>Botones del ratón</term> 
            <listitem><para> 
            Cuando pulse en un enlace con el botón central del ratón, el enlace 
	    se abrirá en una solapa o ventana nueva.Las pulsaciones del botón central
	    en cualquier otro lugar de la ventana del navegador, en cambio, puede configurarse
	    para activar gestos con el ratón, cargar una URL contenida en el portapapeles, activar
	    el desplazamiento manual o el desplazamiento automático
	    (como <application>Internet Explorer</application> o no hacer nada.
	    La segunda opción permite resaltar una URL, por ejemplo, en una ventana de terminal
	    y entonces simplemente pulsar con el botón central en la ventana del navegador para cargar
	   la URL. Puede encontrar información acerca de los gestos de ratón disponibles en el <xref linkend="mouse-gestures"/>.
	    </para>
           </listitem>
	  </varlistentry>

          <varlistentry>
	    <term>Rueda del ratón</term>
	    <listitem>
            <para>
	      Por omisión, la rueda del ratón desplaza 1 línea.  Esto puede ser personalizado
              para desplazar entre 1 y 100 líneas.
            </para>
            </listitem>
	  </varlistentry>

        </variablelist>
      </para>
    </sect1>
    
    <sect1 id="prefs-web-content">
    <title>Contenido web</title>
      <para>
        <variablelist>

          <varlistentry>
	    <term>Activar Java</term> 
             <listitem><para> Esto activa el soporte para Java, en el supuesto de que
	      tenga instalado el complemento apropiado.
             </para></listitem>
	  </varlistentry>
          <varlistentry>
	    <term>JavaScript</term> 
             <listitem><para> 
             JavaScript es un lenguaje simple que puede ser embebido en las páginas web
             para permitir interacción avanzada con el usuario. Puede desactivar el soporte 
             desmarcando la casilla <guibutton>Activar JavaScript</guibutton>.
             </para>
             <para>
             JavaScript también puede abrir ventanas emergentes, la mayoría de las ventanas 
	     emergentes no solicitadas contienen anuncios publicitarios no deseados, asó que 
	     se le avisa para que deje sin marcar la casilla <guibutton>Permitir ventanas emergentes no solicitadas
             </guibutton>. puede gestionar qué sitios se les permite abrir ventanas emergentes no solicitadas usando 
	     el botón <guibutton>Gestionar emergentes</guibutton>, esto abre la <link linkend="pdm-popup-sites">
	     Página de sitios emergentes del Gestor de datos personales</link>
             </para></listitem>
	  </varlistentry>

          <varlistentry>
	    <term>Carga de imágenes</term>
	    <listitem><para>
	      Cuando las imágenes están presentes en una página, puede elegir 
	      cargarlas siempre, cargarlas si provienen del servidor actual,
	      o no cargarlas nunca. Las imágenes publicitarias de los banners
	      normalmente se cargan desde un servidor diferente al de la página que
	      está viendo, así que seleccionar «Cargar sólo desde el servidor actual»
	      a menudo bloqueará estos anuncios publicitarios. Para abrir la 
	      <link linkend="pdm-image-sites">página de Sitios con Imágenes del
		administrador de datos persistentes</link>, permitiendo bloquear imágenes
		desde sitios específicos, pulse el botón <guibutton>Sitios con Imágenes
	      </guibutton>.
       	    </para></listitem>
	  </varlistentry>

          <varlistentry>
	    <term>Imágenes animadas</term>
	    <listitem>
            <para>
              Las imágenes animadas pueden ser muy molestas, así que puede elegir animar las imágenes
              continuamente, sólo animarlas una vez o nunca animarlas.
            </para>
      <note>
        <title>Otras técnicas</title>
        <para>
        Hay técnicas para mostrar imágenes animadas (ej. Flash o JavaScript) que no están
        cubiertas por esta opción.
        </para>
      </note>

            </listitem>
	  </varlistentry>

        </variablelist>
      </para>
    </sect1>
    

    <sect1 id="prefs-privacy">
      <title>Privacidad</title>
      <para>
        <variablelist>

          <varlistentry>
	    <term>Cookies</term>
	    <listitem>
	    <para>
	      Puede elegir aceptar todas las cookies, aceptar las cookies desde el servidor actual
	      únicamente, o no aceptar ninguna cookie. Si quiere ser notificado	
	      cuando un sitio intente almacenar una cookie en su computadora, 
	      seleccione la casilla <guibutton>Avisar antes de aceptar una cookie</guibutton>.
       	    </para>
	    <para>
	      Pulsando en el botón <guibutton>Cookies...</guibutton> o
	      <guibutton>Sitios con cookies...</guibutton> se abrirán las páginas correspondientes
	      del  <link linkend="pdm">Administrador de datos persistentes</link>, permitiendole
		gestionar las cookies que se almacenan y las directivas de cookies específicas de cada
		sitio.
	    </para>
	    </listitem>
	  </varlistentry>

          <varlistentry>
	    <term>Contraseñas</term>
	    <listitem>
	    <para>
	      <application>Galeon</application> puede recordar automáticamente 
	      las combinaciones de usuario/contraseña que usted introduzca. Para
	      activar esta funcionalidad, marque la casilla <guibutton>Recordar contraseñas
	      para sitios que requieran iniciar sesión</guibutton>.
       	    </para>
	    <para>
	      Al pulsar en los botones <guibutton>Contraseñas...</guibutton> o
	      <guibutton>Sitios con contraseña...</guibutton> se abrirán las correspondientes
	      páginas del <link linkend="pdm">Administrador de datos persistentes</link>, permitiéndole
	      gestionar las combinaciones de usuario/contraseña  que estén almacenadas y la lista de 
	      sitios para las que las combinaciones no deben almacenarse.
	    </para>
	    </listitem>
	  </varlistentry>

          <varlistentry>
	    <term>Certificados</term>
	    <listitem>
	    <para>
             Use el botón <guibutton>Certificados</guibutton> para gestionar los certificados
	     del usuario (como aquellos usados por la banca en línea), y las Autoridades de Certificación.
           </para>
           </listitem>
          </varlistentry>
      </variablelist>
     </para>
   </sect1>

  <sect1 id="prefs-network">
  <title>Red</title>
   <para>
       <variablelist>

          <varlistentry>
	    <term>Proxy de red</term>
	    <listitem><para>
              Galeon usa la configuración proxy de GNOME. Al pulsar en <guibutton>Configurar
              Proxy de red </guibutton> se abrirá el programa de <application>Preferencias de 
	      red de Gnome</application> donde puedes editar los detalles del proxy.
       	    </para></listitem>
	  </varlistentry>

          <varlistentry>
	    <term>Caché</term>
	    <listitem><para>
	      La caché de disco almacena una copia local de páginas e imágenes que ha visto
	      en su disco duro, con el fin de ver las páginas que ya ha visitado más rápido.
	      Si quisiera borrar todos los elementos almacenados, pulse en el botón
	      <guibutton>Limpiar caché</guibutton>.
       	    </para></listitem>
	  </varlistentry>

        </variablelist>
      </para>
    </sect1>
</chapter>
