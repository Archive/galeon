/*
 *  Copyright (C) 2005  Crispin Flowerday
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n.h>

#include "galeon-debug.h"
#include "galeon-simple-window.h"
#include "galeon-embed.h"
#include "galeon-shell.h"


/**
 * Private data
 */
#define GALEON_SIMPLE_WINDOW_GET_PRIVATE(object) (G_TYPE_INSTANCE_GET_PRIVATE ((object), \
				       GALEON_TYPE_SIMPLE_WINDOW, GaleonSimpleWindowPrivate))


struct _GaleonSimpleWindowPrivate
{
	GaleonEmbed *embed;
};

G_DEFINE_TYPE (GaleonSimpleWindow, galeon_simple_window, GTK_TYPE_WINDOW);

static void
galeon_simple_window_title_cb (GaleonEmbed *embed, GaleonSimpleWindow *window)
{
	gchar *title = NULL;

	title = galeon_embed_get_title (embed);

	if (title)
	{
		gtk_window_set_title (GTK_WINDOW (window), title);
	}

	g_free (title);
	

}

static GaleonEmbed *
galeon_simple_window_new_window_cb (GaleonEmbed *embed, EmbedChromeMask chromemask,
				    GaleonSimpleWindow *window)
{
	GaleonSimpleWindow *newwin;

	newwin = galeon_simple_window_new();

	return newwin->priv->embed;
}


static void
galeon_simple_window_destroy_brsr_cb (GaleonEmbed *embed, GaleonSimpleWindow *window)
{
	gtk_widget_destroy (GTK_WIDGET (window));
}

static void
galeon_simple_window_size_to_cb (GaleonEmbed *embed, gint width, gint height,
				 GaleonSimpleWindow *window)
{
	gtk_widget_set_size_request (GTK_WIDGET (window), width, height);
}

static void
galeon_simple_window_visibility_cb (GaleonEmbed *embed, gboolean visible,
				    GaleonSimpleWindow *window)
{
	if (visible)
	{
		gtk_widget_show (GTK_WIDGET (window));
	}
	else
	{
		gtk_widget_hide (GTK_WIDGET (window));
	}
}

void
galeon_simple_window_load_url (GaleonSimpleWindow *window, const char *url)
{
	g_return_if_fail (GALEON_IS_SIMPLE_WINDOW (window));
	
	galeon_embed_load_url (window->priv->embed, url);
}

GaleonEmbed *
galeon_simple_window_get_embed (GaleonSimpleWindow *window)
{
	g_return_val_if_fail (GALEON_IS_SIMPLE_WINDOW (window), NULL);

	return window->priv->embed;
}

static void
galeon_simple_window_finalize_impl(GObject *o)
{
	g_object_unref (galeon_shell);

	G_OBJECT_CLASS(galeon_simple_window_parent_class)->finalize (o);
}

static void 
galeon_simple_window_init(GaleonSimpleWindow *window)
{
	GaleonSimpleWindowPrivate *p = GALEON_SIMPLE_WINDOW_GET_PRIVATE (window);

	window->priv = p;

	p->embed = galeon_embed_new();

	gtk_container_add (GTK_CONTAINER (window), GTK_WIDGET (p->embed));
	gtk_widget_show (GTK_WIDGET (p->embed));

	g_signal_connect_object (p->embed, "ge_title",
				 G_CALLBACK (galeon_simple_window_title_cb),
				 window, 0);
	g_signal_connect_object (p->embed, "ge_destroy_brsr",
				 G_CALLBACK (galeon_simple_window_destroy_brsr_cb),
				 window, 0);
	g_signal_connect_object (p->embed, "ge_size_to",
				 G_CALLBACK (galeon_simple_window_size_to_cb),
				 window, 0);
	g_signal_connect_object (p->embed, "ge_new_window",
				 G_CALLBACK (galeon_simple_window_new_window_cb),
				 window, 0);
	g_signal_connect_object (p->embed, "ge_visibility",
				 G_CALLBACK (galeon_simple_window_visibility_cb),
				 window, 0);

	g_object_ref (galeon_shell);
}

static void
galeon_simple_window_class_init(GaleonSimpleWindowClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = galeon_simple_window_finalize_impl;

	g_type_class_add_private (klass, sizeof (GaleonSimpleWindowPrivate));
}


GaleonSimpleWindow *
galeon_simple_window_new (void)
{
	GaleonSimpleWindow *ret;

	ret = g_object_new (GALEON_TYPE_SIMPLE_WINDOW, NULL);

	return ret;
}
