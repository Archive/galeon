/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>

#include "galeon-embed-persist.h"
#include "galeon-debug.h"

#define GALEON_EMBED_PERSIST_GET_PRIVATE(object) (G_TYPE_INSTANCE_GET_PRIVATE ((object), \
						  GALEON_TYPE_EMBED_PERSIST, GaleonEmbedPersistPrivate))
enum
{
        COMPLETED,
        CANCELLED,
        LAST_SIGNAL
};

enum
{
	PROP_0,
	PROP_EMBED,
	PROP_SOURCE,
	PROP_DEST,
	PROP_MAX_SIZE,
	PROP_FLAGS,
	PROP_HANDLER,
	PROP_FC_PARENT,
	PROP_FC_TITLE,
	PROP_USER_TIME
};

struct GaleonEmbedPersistPrivate
{
	char *dir;
	char *src;
	GnomeVFSMimeApplication *handler;
	GaleonEmbed *embed;
	int max_size;
	EmbedPersistFlags flags;
	GtkWidget *fc_parent;
	char *fc_title;
	/* Time of event that caused the download (used for app opening) */
	guint32 user_time;
};


static guint galeon_embed_persist_signals[LAST_SIGNAL] = { 0 };

G_DEFINE_TYPE (GaleonEmbedPersist, galeon_embed_persist, G_TYPE_OBJECT);

void
galeon_embed_persist_set_source (GaleonEmbedPersist *persist,
				 const char *url)
{
	g_return_if_fail (GALEON_IS_EMBED_PERSIST (persist));

	persist->priv->src = g_strdup(url);

	g_object_notify (G_OBJECT(persist), "source");
}

const char *
galeon_embed_persist_get_source (GaleonEmbedPersist *persist)
{
	g_return_val_if_fail (GALEON_IS_EMBED_PERSIST (persist), NULL);

	return persist->priv->src;
}

void
galeon_embed_persist_set_dest (GaleonEmbedPersist *persist,
			       const char *dir)
{
	g_return_if_fail (GALEON_IS_EMBED_PERSIST (persist));

	persist->priv->dir = g_strdup (dir);

	g_object_notify (G_OBJECT(persist), "dest");
}

const char *
galeon_embed_persist_get_dest (GaleonEmbedPersist *persist)
{
	g_return_val_if_fail (GALEON_IS_EMBED_PERSIST (persist), NULL);

	return persist->priv->dir;
}
						     
void
galeon_embed_persist_set_max_size (GaleonEmbedPersist *persist,
			           int kb_size)
{
	g_return_if_fail (GALEON_IS_EMBED_PERSIST (persist));

	persist->priv->max_size = kb_size;

	g_object_notify (G_OBJECT(persist), "max_size");
}

void
galeon_embed_persist_set_embed (GaleonEmbedPersist *persist,
		                GaleonEmbed *embed)
{
	g_return_if_fail (GALEON_IS_EMBED_PERSIST (persist));

	persist->priv->embed = embed;

	g_object_notify (G_OBJECT(persist), "embed");
}

GaleonEmbed *
galeon_embed_persist_get_embed (GaleonEmbedPersist *persist)
{
	g_return_val_if_fail (GALEON_IS_EMBED_PERSIST (persist), NULL);

	return persist->priv->embed;
}

void
galeon_embed_persist_set_flags (GaleonEmbedPersist *persist,
		                EmbedPersistFlags flags)
{
	g_return_if_fail (GALEON_IS_EMBED_PERSIST (persist));

	persist->priv->flags = flags;

	g_object_notify (G_OBJECT(persist), "flags");
}

EmbedPersistFlags
galeon_embed_persist_get_flags (GaleonEmbedPersist *persist)
{
	g_return_val_if_fail (GALEON_IS_EMBED_PERSIST (persist), 0);

	return persist->priv->flags;
}

void
galeon_embed_persist_set_handler (GaleonEmbedPersist      *persist,
				  GnomeVFSMimeApplication *handler)
{
	g_return_if_fail (GALEON_IS_EMBED_PERSIST (persist));

	gnome_vfs_mime_application_free (persist->priv->handler);
	persist->priv->handler = gnome_vfs_mime_application_copy (handler);

	g_object_notify (G_OBJECT(persist), "handler");
}

const char *
galeon_embed_persist_get_fc_title (GaleonEmbedPersist *persist)
{
	g_return_val_if_fail (GALEON_IS_EMBED_PERSIST (persist), NULL);

	return persist->priv->fc_title;
}

void
galeon_embed_persist_set_fc_title (GaleonEmbedPersist *persist,
				   const char *title)
{
	g_return_if_fail (GALEON_IS_EMBED_PERSIST (persist));

	g_free (persist->priv->fc_title);
	persist->priv->fc_title = g_strdup (title);
}

GtkWidget *
galeon_embed_persist_get_fc_parent (GaleonEmbedPersist *persist)
{
	g_return_val_if_fail (GALEON_IS_EMBED_PERSIST (persist), NULL);

	return persist->priv->fc_parent;
}

void
galeon_embed_persist_set_fc_parent (GaleonEmbedPersist *persist,
				    GtkWidget *parent)
{
	g_return_if_fail (GALEON_IS_EMBED_PERSIST (persist));

	persist->priv->fc_parent = parent;
}

void
galeon_embed_persist_set_user_time (GaleonEmbedPersist *persist,
				    guint32 user_time)
{
	g_return_if_fail (GALEON_IS_EMBED_PERSIST (persist));

	persist->priv->user_time = user_time;
}

gboolean
galeon_embed_persist_save (GaleonEmbedPersist *persist)
{
	GaleonEmbedPersistClass *klass = GALEON_EMBED_PERSIST_GET_CLASS (persist);

	g_return_val_if_fail (GALEON_IS_EMBED_PERSIST (persist), FALSE);

	return klass->save (persist);
}

gboolean
galeon_embed_persist_stream (GaleonEmbedPersist *persist, GaleonEmbedPersistStreamFunc func,
			     gpointer data)
{
	GaleonEmbedPersistClass *klass = GALEON_EMBED_PERSIST_GET_CLASS (persist);

	g_return_val_if_fail (GALEON_IS_EMBED_PERSIST (persist), FALSE);

	return klass->stream (persist, func, data);
}


static void 
galeon_embed_persist_set_property (GObject *object,
                                   guint prop_id,
                                   const GValue *value,
                                   GParamSpec *pspec)
{
	GaleonEmbedPersist *persist;
	
	persist = GALEON_EMBED_PERSIST (object);
  
	switch (prop_id)
	{
		case PROP_EMBED:
			galeon_embed_persist_set_embed (persist,
							g_value_get_object (value));
			break;
		case PROP_SOURCE:
			galeon_embed_persist_set_source (persist,
							 g_value_get_string (value));
			break;
		case PROP_DEST:
			galeon_embed_persist_set_dest  (persist,
							 g_value_get_string (value));
			break;
		case PROP_MAX_SIZE:
			galeon_embed_persist_set_max_size  (persist,
							    g_value_get_int (value));
			break;
		case PROP_FLAGS:
			galeon_embed_persist_set_flags 
				(persist,
				(EmbedPersistFlags)g_value_get_int (value));
			break;
		case PROP_HANDLER:
			galeon_embed_persist_set_handler (persist,
							  g_value_get_pointer (value));
			break;
		case PROP_FC_TITLE:
			galeon_embed_persist_set_fc_title (persist,
							   g_value_get_string (value));
			break;

		case PROP_FC_PARENT:
			galeon_embed_persist_set_fc_parent (persist,
						  GTK_WIDGET (g_value_get_object (value)));
			break;
		case PROP_USER_TIME:
			galeon_embed_persist_set_user_time (persist,
							    g_value_get_uint(value));
			break;
	}
}

static void 
galeon_embed_persist_get_property (GObject *object,
                                   guint prop_id,
                                   GValue *value,
                                   GParamSpec *pspec)
{
	GaleonEmbedPersist *persist;

	persist = GALEON_EMBED_PERSIST (object);
  
	switch (prop_id)
	{
		case PROP_EMBED:
			g_value_set_object (value, persist->priv->embed);
			break;
		case PROP_SOURCE:
			g_value_set_string (value, persist->priv->src);
			break;
		case PROP_DEST:
			g_value_set_string (value, persist->priv->dir);
			break;
		case PROP_MAX_SIZE:
			g_value_set_int (value, persist->priv->max_size);
			break;
		case PROP_FLAGS:
			g_value_set_int (value, (int)persist->priv->flags);
			break;
		case PROP_HANDLER:
			g_value_set_pointer (value, persist->priv->handler);
			break;
	        case PROP_FC_TITLE:
			g_value_set_string (value, persist->priv->fc_title);
			break;
		case PROP_FC_PARENT:
			g_value_set_object (value, persist->priv->fc_parent);
			break;
		case PROP_USER_TIME:
			g_value_set_uint (value, persist->priv->user_time);
			break;
	}
}

static void
galeon_embed_persist_init (GaleonEmbedPersist *persist)
{
        persist->priv = GALEON_EMBED_PERSIST_GET_PRIVATE (persist);
	LOG ("GaleonEmbedPersist ctor (%p)", persist);
}

static void
galeon_embed_persist_finalize (GObject *object)
{
        GaleonEmbedPersist *persist = GALEON_EMBED_PERSIST (object);

	g_free (persist->priv->dir);
	g_free (persist->priv->src);
	g_free (persist->priv->fc_title);
	
	gnome_vfs_mime_application_free (persist->priv->handler);
	
	LOG ("GaleonEmbedPersist dtor (%p)", persist);
        G_OBJECT_CLASS (galeon_embed_persist_parent_class)->finalize (object);
}

static void
galeon_embed_persist_class_init (GaleonEmbedPersistClass *klass)
{
        GObjectClass *object_class = G_OBJECT_CLASS (klass);

	
        object_class->finalize = galeon_embed_persist_finalize;
        object_class->set_property = galeon_embed_persist_set_property;
	object_class->get_property = galeon_embed_persist_get_property;
	
	/* init signals */
        galeon_embed_persist_signals[COMPLETED] =
                g_signal_new ("completed",
                              G_OBJECT_CLASS_TYPE (object_class),
                              G_SIGNAL_RUN_LAST,
                              G_STRUCT_OFFSET (GaleonEmbedPersistClass, completed),
                              NULL, NULL,
                              g_cclosure_marshal_VOID__VOID,
                              G_TYPE_NONE,
                              0);

        galeon_embed_persist_signals[CANCELLED] =
                g_signal_new ("cancelled",
                              G_OBJECT_CLASS_TYPE (object_class),
                              G_SIGNAL_RUN_LAST,
                              G_STRUCT_OFFSET (GaleonEmbedPersistClass, cancelled),
                              NULL, NULL,
                              g_cclosure_marshal_VOID__VOID,
                              G_TYPE_NONE,
                              0);

	g_object_class_install_property (object_class,
                                         PROP_EMBED,
                                         g_param_spec_object ("embed",
                                                              "Embed",
                                                              "The embed containing the document",
                                                              G_TYPE_OBJECT,
                                                              G_PARAM_READWRITE));
	g_object_class_install_property (object_class,
                                         PROP_SOURCE,
                                         g_param_spec_string  ("source",
                                                               "Source",
                                                               "Url of the document to save",
                                                               NULL,
                                                               G_PARAM_READWRITE));
	
	g_object_class_install_property (object_class,
                                         PROP_DEST,
                                         g_param_spec_string ("dest",
                                                              "Destination",
                                                              "Destination directory",
                                                              NULL,
                                                              G_PARAM_READWRITE));

	g_object_class_install_property (object_class,
                                         PROP_MAX_SIZE,
                                         g_param_spec_int    ("max_size",
                                                              "Maxsize",
                                                              "Maximum size of the file",
                                                              0,
							      G_MAXINT,
							      0,
                                                              G_PARAM_READWRITE));

	g_object_class_install_property (object_class,
                                         PROP_FLAGS,
                                         g_param_spec_int    ("flags",
                                                              "Flags",
                                                              "Flags",
                                                              0,
							      G_MAXINT,
							      0,
                                                              G_PARAM_READWRITE));

	g_object_class_install_property (object_class,
                                         PROP_HANDLER,
                                         g_param_spec_pointer ("handler",
                                                              "Handler",
                                                              "Handler",
                                                              G_PARAM_READWRITE));


	g_object_class_install_property (object_class,
                                         PROP_FC_PARENT,
                                         g_param_spec_object ("fc_parent",
                                                              "File choose parent window",
                                                              "File choose parent window",
							      GTK_TYPE_WIDGET,
                                                              G_PARAM_READWRITE));

	g_object_class_install_property (object_class,
                                         PROP_FC_TITLE,
                                         g_param_spec_string ("fc_title",
                                                              "File chooser title",
                                                              "File chooser title",
                                                              NULL,
                                                              G_PARAM_READWRITE));

	g_object_class_install_property (object_class,
                                         PROP_USER_TIME,
                                         g_param_spec_uint    ("user_time",
							       "User Time",
							       "Time of event that caused the download",
							       0,
							       G_MAXUINT,
							       0,
							       G_PARAM_READWRITE));

	g_type_class_add_private (object_class, sizeof (GaleonEmbedPersistPrivate));
}
