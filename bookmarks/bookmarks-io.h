/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __bookmarks_io_h
#define __bookmarks_io_h

#include <glib.h>
#include <glib-object.h>
#include "bookmarks.h"

/**
 * IO object
 */

#define GB_TYPE_IO			(gb_io_get_type())
#define GB_IO(object)			(G_TYPE_CHECK_INSTANCE_CAST((object), GB_TYPE_IO, GbIO))
#define GB_IO_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), GB_TYPE_IO, GbIOClass))
#define GB_IS_IO(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), GB_TYPE_IO))
#define GB_IS_IO_CLASS(klass)		(G_TYPE_CHECK_CLASS_TYPE((klass), GB_TYPE_IO))
#define GB_IO_GET_CLASS(obj)		(G_TYPE_INSTANCE_GET_CLASS((obj), GB_TYPE_IO, GbIOClass))

struct _GbIOClass 
{
	GObjectClass parent_class;

	/* virtual methods (FIXME: error handling) */
	GbBookmarkSet *	(*gb_io_load_from_file)		(GbIO *io, const gchar *filename);
	GbBookmarkSet *	(*gb_io_load_from_string)	(GbIO *io, const gchar *data);
	gboolean	(*gb_io_save_to_file)		(GbIO *io, GbBookmarkSet *set,
							 const gchar *filename);
	gboolean	(*gb_io_save_to_string)		(GbIO *io, GbBookmarkSet *set, gchar **data);
	gchar *		(*gb_io_format_name)		(GbIO *io);
	GList *		(*gb_io_extensions)		(GbIO *io);
	
};

/* Remember: fields are public read-only */
struct _GbIO
{
	GObject parent_object;

};

GType		gb_io_get_type			(void);
GbBookmarkSet *	gb_io_load_from_file 		(GbIO *io, const gchar *filename);
GbBookmarkSet *	gb_io_load_from_string		(GbIO *io, const gchar *data);
gboolean	gb_io_save_to_file		(GbIO *io, GbBookmarkSet *set,
						 const gchar *filename);
gboolean	gb_io_save_to_string		(GbIO *io, GbBookmarkSet *set, gchar **data);
gchar *		gb_io_format_name		(GbIO *io);
GList *		gb_io_extensions		(GbIO *io);
GbIO *		gb_io_object_for_file		(const gchar *file);

#endif
