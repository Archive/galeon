/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __bookmarks_context_menu_h
#define __bookmarks_context_menu_h

#include <glib-object.h>
#include "bookmarks.h"
#include "bookmarks-util.h"
#include "bookmarks-location-source.h"

/* object forward declarations */

typedef struct _GbContextMenu GbContextMenu;
typedef struct _GbContextMenuClass GbContextMenuClass;
typedef struct _GbContextMenuPrivate GbContextMenuPrivate;

/**
 * Context menu object
 */

#define GB_TYPE_CONTEXT_MENU			(gb_context_menu_get_type())
#define GB_CONTEXT_MENU(object)			(G_TYPE_CHECK_INSTANCE_CAST((object), GB_TYPE_CONTEXT_MENU,\
						 GbContextMenu))
#define GB_CONTEXT_MENU_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), GB_TYPE_CONTEXT_MENU,\
						 GbContextMenuClass))
#define GB_IS_CONTEXT_MENU(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), GB_TYPE_CONTEXT_MENU))
#define GB_IS_CONTEXT_MENU_CLASS(klass)		(G_TYPE_CHECK_CLASS_TYPE((klass), GB_TYPE_CONTEXT_MENU))
#define GB_CONTEXT_MENU_GET_CLASS(obj)		(G_TYPE_INSTANCE_GET_CLASS((obj), GB_TYPE_CONTEXT_MENU,\
						 GbContextMenuClass))

struct _GbContextMenuClass 
{
	GObjectClass parent_class;

	/* signals */
	GbBookmarkActivatedCallback gb_context_menu_bookmark_activated;
	void		(*gb_context_menu_deactivated)		(GbContextMenu *cm);

};

/* Remember: fields are public read-only */
struct _GbContextMenu
{
	GObject parent_object;

	GbContextMenuPrivate *priv;
};

GType			gb_context_menu_get_type		(void);
GbContextMenu *		gb_context_menu_new			(void);
void			gb_context_menu_set_bookmark		(GbContextMenu *cm, GbBookmark *b);
void			gb_context_menu_popup			(GbContextMenu *cm, GdkEventButton *event);
void			gb_context_menu_set_location_source	(GbContextMenu *cm, GbLocationSource *src);
GbContextMenu *		gb_context_menu_new_quick		(GbBookmark *bm, 
								 GbLocationSource *location_source, 
								 GObject *creator);
void			gb_context_menu_quick			(GbBookmark *bm, GdkEventButton *event,
								 GbLocationSource *location_source, 
								 GObject *creator);

#endif
