/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __io_netscape_h
#define __io_netscape_h

#include <glib.h>
#include <glib-object.h>
#include "bookmarks-io.h"
#include "bookmarks.h"

/* object forward declarations */

typedef struct _GbIONetscape GbIONetscape;
typedef struct _GbIONetscapeClass GbIONetscapeClass;

/**
 * IO_NETSCAPE object
 */

#define GB_TYPE_IO_NETSCAPE		(gb_io_netscape_get_type())
#define GB_IO_NETSCAPE(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), GB_TYPE_IO_NETSCAPE, \
					 GbIONetscape))
#define GB_IO_NETSCAPE_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), GB_TYPE_IO_NETSCAPE, \
					 GbIONetscapeClass))
#define GB_IS_IO_NETSCAPE(object)	(G_TYPE_CHECK_INSTANCE_TYPE((object), GB_TYPE_IO_NETSCAPE))
#define GB_IS_IO_NETSCAPE_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), GB_TYPE_IO_NETSCAPE))
#define GB_IO_NETSCAPE_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), GB_TYPE_IO_NETSCAPE, \
					 GbIONetscapeClass))

struct _GbIONetscapeClass 
{
	GbIOClass parent_class;
};

struct _GbIONetscape
{
	GbIO parent_object;
};

GType			gb_io_netscape_get_type			(void);
GbIONetscape *		gb_io_netscape_new			(void);

#endif
