/*
 *  Copyright (C) 2001 Philip Langdale
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#ifndef MyportalProtocolHandler_h__
#define MyportalProtocolHandler_h__
    
#include <nsError.h>
#include <nsIAboutModule.h>
#include <nsIProtocolHandler.h>

#include "bookmarks.h"

#define G_MYPORTAL_PROTOCOL_CID				\
{ /* a3a7b6e5-7a92-431d-87e6-3bef8e7ada51*/		\
    0xa3a7b6e5,						\
    0x7a92,						\
    0x431d,						\
    {0x87, 0xe6, 0x3b, 0xef, 0x8e, 0x7a, 0xda, 0x51}	\
}
#define G_MYPORTAL_HANDLER_CONTRACTID NS_NETWORK_PROTOCOL_CONTRACTID_PREFIX "myportal"
#define G_MYPORTAL_HANDLER_CLASSNAME "Galeon's myportal protocol handler"
#define G_ABOUT_MYPORTAL_CONTRACTID NS_ABOUT_MODULE_CONTRACTID_PREFIX "myportal"
#define G_ABOUT_MYPORTAL_CLASSNAME "Galeon's about:myportal"

class nsIChannel;
class nsIOutputStream;
class nsIURI;

class GMyportalProtocolHandler : public nsIProtocolHandler,
				 public nsIAboutModule
{
  public:
	NS_DECL_ISUPPORTS
	NS_DECL_NSIPROTOCOLHANDLER

#ifdef HAVE_NSIABOUTMODULE_GETURIFLAGS
	// NS_DECL_NSIABOUTMODULE collides with NS_DECL_NSIPROTOCOLHANDLER
	NS_IMETHOD GetURIFlags(nsIURI *aURI, PRUint32 *_retval);
#endif

	GMyportalProtocolHandler (void);
	virtual ~GMyportalProtocolHandler();

 private:
	nsresult CreateMyportalPage (const char * path, nsIURI *aURI, nsIChannel **aChannel);
	nsresult RedirectURL (const char *path, nsIChannel **aChannel);
	nsresult HandleSmartBMSubmit (const char *path, nsIChannel **aChannel);

	nsresult RenderFromPath (const gchar *path);
	nsresult RenderItem (GbBookmark *b, const gchar *prefix, 
			     gint depth);
	
	nsresult RenderFolder (GbFolder *b,  const gchar *prefix, 
			     gint depth);

	nsresult RenderFolderContents (GbFolder *b, const gchar *prefix, 
				       gint depth);
	nsresult RenderURL (GbSite *b);
	nsresult RenderSmartURL (GbSmartSite *b);

	nsresult Write (const char* aStr);
	nsresult WriteHTMLEscape (const char * aStr);

	nsCOMPtr<nsIOutputStream> mStream;
};

#endif // MyportalProtocolHandler_h__
