/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GALEON_EMBED_H
#define GALEON_EMBED_H

#include "galeon-embed-types.h"
#include "galeon-embed-event.h"
#include "galeon-encodings.h"

#include <glib-object.h>
#include <glib.h>
#include <gtk/gtkwidget.h>

G_BEGIN_DECLS

typedef struct GaleonEmbedClass GaleonEmbedClass;

#define GALEON_TYPE_EMBED             (galeon_embed_get_type ())
#define GALEON_EMBED(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GALEON_TYPE_EMBED, GaleonEmbed))
#define GALEON_EMBED_CLASS(vtable)    (G_TYPE_CHECK_CLASS_CAST ((vtable), GALEON_TYPE_EMBED, GaleonEmbedClass))
#define GALEON_IS_EMBED(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GALEON_TYPE_EMBED))
#define GALEON_IS_EMBED_CLASS(vtable) (G_TYPE_CHECK_CLASS_TYPE ((vtable), GALEON_TYPE_EMBED))
#define GALEON_EMBED_GET_CLASS(inst)  (G_TYPE_INSTANCE_GET_INTERFACE ((inst), GALEON_TYPE_EMBED, GaleonEmbedClass))

typedef struct _GaleonEmbed GaleonEmbed;

typedef enum
{
	EMBED_STATE_UNKNOWN = 0,
	EMBED_STATE_START = 1 << 0,
  	EMBED_STATE_REDIRECTING = 1 << 1,
  	EMBED_STATE_TRANSFERRING = 1 << 2,
	EMBED_STATE_NEGOTIATING = 1 << 3,
  	EMBED_STATE_STOP = 1 << 4,
  
  	EMBED_STATE_IS_REQUEST = 1 << 5,
  	EMBED_STATE_IS_DOCUMENT = 1 << 6,
  	EMBED_STATE_IS_NETWORK = 1 << 7,
 	EMBED_STATE_IS_WINDOW = 1 << 8
} EmbedState;

typedef enum
{
	EMBED_RENDER_FULL_STANDARDS   = 1,
	EMBED_RENDER_ALMOST_STANDARDS = 2,
	EMBED_RENDER_QUIRKS           = 3
} EmbedPageRenderMode;

typedef enum
{
	EMBED_SOURCE_NOT_CACHED    = 0,
	EMBED_SOURCE_DISK_CACHE    = 1,
	EMBED_SOURCE_MEMORY_CACHE  = 2,
	EMBED_SOURCE_UNKNOWN_CACHE = 3
} EmbedPageSource;

typedef struct
{
	char *content_type;
	char *encoding;
	char *referring_url;

	gint size;
	GTime expiration_time;
	GTime modification_time;

	EmbedPageRenderMode rendering_mode;
	EmbedPageSource page_source;

	char *cipher_name;
	char *cert_issuer_name;
	gint key_length;
	gint secret_key_length;

	/* lists of hashtables with gvalues */
	GList *metatags;    /* name, value */
	GList *images;      /* url, alt, title, width, height */
	GList *forms;       /* name, method, action */
	GList *links;       /* url, title, relation */
	GList *stylesheets; /* url, title */
} EmbedPageProperties;

typedef struct
{
	char *name;
	char *content;
} EmbedPageMetaTag;

typedef struct
{
	char *name;
	char *method;
	char *action;
} EmbedPageForm;

typedef struct
{
	char *url;
	char *title;
	char *rel;
} EmbedPageLink;

typedef struct
{
	char *url;
	char *alt;
	char *title;
	gint width;
	gint height;
} EmbedPageImage;

typedef enum
{
	EMBED_RELOAD_NORMAL = 1 << 1,
  	EMBED_RELOAD_FORCE  = 1 << 2,
  	EMBED_RELOAD_FRAME  = 1 << 3
} EmbedReloadFlags;

typedef enum
{
        DISPLAY_AS_SOURCE = 1U,
        DISPLAY_NORMAL = 2U
} EmbedDisplayType;

typedef struct
{
        gboolean print_to_file;
        gchar *name;
        gboolean can_change_command;
        gchar *command;
        gchar *file;
        gint paper;
        GPtrArray *paper_array;
        GPtrArray *paper_desc_array;
        gdouble top_margin;
        gdouble bottom_margin;
        gdouble left_margin;
        gdouble right_margin;
        gint pages;
        gint from_page;
        gint to_page;
        gint frame_type;
        gint orientation;
        gboolean print_color;

         /* 
         * &T - title
         * &U - Document URL
         * &D - Date/Time
         * &P - Page Number
         * &PT - Page Number with total Number of Pages (example: 1 of 34)
         *
         * So, if headerLeftStr = "&T" the title and the document URL
         * will be printed out on the top left-hand side of each page.
         */
        gchar *header_left_string;
        gchar *header_center_string;
        gchar *header_right_string;
        gchar *footer_left_string;
        gchar *footer_center_string;
        gchar *footer_right_string;

	gboolean preview;
}
EmbedPrintInfo;

typedef enum
{
	PRINTPREVIEW_GOTO_PAGENUM = 0,
	PRINTPREVIEW_PREV_PAGE = 1,
	PRINTPREVIEW_NEXT_PAGE = 2,
	PRINTPREVIEW_HOME = 3,
	PRINTPREVIEW_END = 4
} EmbedPrintPreviewNavType;

typedef enum
{
	EMBED_SCROLL_UP,
	EMBED_SCROLL_DOWN,
	EMBED_SCROLL_LEFT,
	EMBED_SCROLL_RIGHT
} EmbedScrollDirection;

typedef enum
{
	STATE_IS_UNKNOWN,
	STATE_IS_INSECURE,
	STATE_IS_BROKEN,
	STATE_IS_SECURE_MED,
	STATE_IS_SECURE_LOW,
	STATE_IS_SECURE_HIGH
} EmbedSecurityLevel;

typedef enum
{
	STYLESHEET_NONE,      /* no stylesheet */
	STYLESHEET_BASIC,     /* no named stylesheets */
	STYLESHEET_NAMED,     /* Named stylesheet */
        STYLESHEET_USER_NONE, /* No User stylesheet */
        STYLESHEET_USER       /* User specified stylesheet */
} EmbedStyleSheetType;
                                                                                                                                              
typedef struct
{
	gchar *name;    /* stylesheet name */
	gpointer sheet; /* opaque pointer to the underlying sheet object */
	EmbedStyleSheetType type;
} EmbedStyleSheet;


struct GaleonEmbedClass
{
        GTypeInterface base_iface;

  	void (* link_message)    (GaleonEmbed *embed,
				  const char *link);
  	void (* js_status)       (GaleonEmbed *embed,
				  const char *status);
  	void (* location)        (GaleonEmbed *embed);
  	void (* title)           (GaleonEmbed *embed);
  	void (* progress)        (GaleonEmbed *embed, 
				  const char *uri,
                	          gint curprogress, 
				  gint maxprogress);
  	void (* net_state)       (GaleonEmbed *embed, 
				  const char *uri,
			          EmbedState state);
  	GaleonEmbed* (* new_window)      (GaleonEmbed *embed, 
					  EmbedChromeMask chromemask);
  	void (* visibility)      (GaleonEmbed *embed, 
			          gboolean visibility);
  	void (* destroy_brsr)    (GaleonEmbed *embed);
  	gint (* open_uri)        (GaleonEmbed *embed, 
			          const char *uri);
  	void (* size_to)         (GaleonEmbed *embed, 
			          gint width, 
			          gint height);
  	gint (* dom_mouse_click) (GaleonEmbed *embed, 
			          GaleonEmbedEvent *event);
	gint (* dom_mouse_down)  (GaleonEmbed *embed,
				  GaleonEmbedEvent *event);
	void (* security_change) (GaleonEmbed *embed, 
                                  EmbedSecurityLevel level);
	void (* zoom_change) 	 (GaleonEmbed *embed, 
                                  guint new_zoom);
	void (* favicon)	 (GaleonEmbed *embed,
			          const char *uri);

	void (* popupblocked)	 (GaleonEmbed *embed,
				  const char *uri);

	void (* contextmenu)	 (GaleonEmbed *embed,
			          GaleonEmbedEvent *event);

  	void (* net_stop)        (GaleonEmbed *embed);
  	void (* net_start)       (GaleonEmbed *embed);

	gboolean (* modal_alert)	(GaleonEmbed *embed);
	void	 (* modal_alert_closed)	(GaleonEmbed *embed);
	void	 (* content_change)	(GaleonEmbed *embed,
					 const char *uri);

	void     (*show_js_console)     (GaleonEmbed *embed);

	gboolean (* search_key_press)   (GaleonEmbed *embed,
					 GdkEventKey *event);

	/* Methods  */
	void      (* load_url)             (GaleonEmbed *embed, 
					    const char *url);
	void      (* stop_load)            (GaleonEmbed *embed);
	gboolean  (* can_go_back)          (GaleonEmbed *embed);
	gboolean  (* can_go_forward)       (GaleonEmbed *embed);
	gboolean  (* can_go_up)            (GaleonEmbed *embed);
	GSList *  (* get_go_up_list)       (GaleonEmbed *embed);
	void      (* go_back)              (GaleonEmbed *embed);
	void      (* go_forward)           (GaleonEmbed *embed);
	void      (* go_up)                (GaleonEmbed *embed);
	gboolean  (* render_data)          (GaleonEmbed *embed, 
					    const char *data,
					    guint32 len,
					    const char *base_uri, 
					    const char *mime_type);
	gboolean  (* open_stream)          (GaleonEmbed *embed,
					    const char *base_uri,
					    const char *mime_type);
	void      (* append_data)          (GaleonEmbed *embed,
					    const char *data, 
					    guint32 len);
        void      (* close_stream)         (GaleonEmbed *embed);
	char *    (* get_title)            (GaleonEmbed *embed);
	char *    (* get_location)         (GaleonEmbed *embed, 
				            gboolean toplevel,
				            gboolean requested);

	void      (* reload)               (GaleonEmbed *embed, 
				       	    EmbedReloadFlags flags);
	void      (* copy_page)		   (GaleonEmbed *dest,
					    GaleonEmbed *source,
					    EmbedDisplayType display_type);
	GList *   (* get_link_tags)        (GaleonEmbed *embed,
					    const char *link_type);
	void      (* zoom_set)             (GaleonEmbed *embed, 
					    int zoom);
	int       (* zoom_get)             (GaleonEmbed *embed);

	gboolean  (* selection_can_cut)    (GaleonEmbed *embed);
	gboolean  (* selection_can_copy)   (GaleonEmbed *embed);
	gboolean  (* can_paste)            (GaleonEmbed *embed);
	void      (* selection_cut)        (GaleonEmbed *embed);
	void      (* selection_copy)       (GaleonEmbed *embed);
	void      (* paste)                (GaleonEmbed *embed);
	void      (* select_all)           (GaleonEmbed *embed);
	int       (* shistory_count)	   (GaleonEmbed *embed);
	gboolean  (* shistory_get_nth)     (GaleonEmbed *embed, 
				            int nth,
				            gboolean is_relative,
				            char **url,
				            char **title);
	int       (* shistory_get_pos)     (GaleonEmbed *embed);
	gboolean  (* shistory_go_nth)      (GaleonEmbed *embed,
					    int nth);
	void      (* shistory_copy)	   (GaleonEmbed *source,
					    GaleonEmbed *dest,
					    gboolean back_history,
					    gboolean forward_history,
					    gboolean set_current);
        void      (* shistory_clear)       (GaleonEmbed *embed);
	void      (* scroll)               (GaleonEmbed *embed, 
		  			    EmbedScrollDirection direction);

	void      (* scroll_page)          (GaleonEmbed *embed, 
		  			    EmbedScrollDirection direction);
	void	  (* fine_scroll)	   (GaleonEmbed *embed, 
					    int horiz, int vert);
	gboolean  (* get_security_level)   (GaleonEmbed *embed, 
					     EmbedSecurityLevel *level, 
					     char **description);
	
	gboolean  (* print)                (GaleonEmbed *embed);
	void	  (* print_preview_close)  (GaleonEmbed *embed);
	int       (* print_preview_num_pages)	(GaleonEmbed *embed);

	gboolean  (* print_preview_navigate) (GaleonEmbed *embed,
					      EmbedPrintPreviewNavType navType,
					      gint pageNum);
	gboolean  (* set_encoding)           (GaleonEmbed *embed,
					      const char *charset);
	GaleonEncodingPageInfo * (* get_encoding_info)	  (GaleonEmbed *embed);
	EmbedPageProperties *    (* get_page_properties)  (GaleonEmbed *embed);
	void      (* show_page_certificate) (GaleonEmbed *embed);

	GList *   (* get_stylesheets)	    (GaleonEmbed *embed);
	EmbedStyleSheet * (* get_selected_stylesheet)   (GaleonEmbed *embed);

	void      (* set_stylesheet)	      (GaleonEmbed *embed,
					       EmbedStyleSheet *sheet);
	gboolean  (* apply_user_stylesheet)   (GaleonEmbed *embed,
					       const gchar *sheetfile,
					       EmbedStyleSheet **retSheet);
	void      (* remove_user_stylesheet)  (GaleonEmbed *embed,
					       EmbedStyleSheet *sheet);
	gboolean  (* has_modified_forms)      (GaleonEmbed *embed);

	gboolean  (* can_view_source)         (GaleonEmbed *embed);

	void      (* evaluate_javascript)     (GaleonEmbed *embed, 
					       const char *script);
};	
	
GType         galeon_embed_get_type             (void);

/* Base */

GaleonEmbed  *galeon_embed_new                  (void);

void          galeon_embed_load_url             (GaleonEmbed *embed, 
					         const char *url);

void          galeon_embed_stop_load            (GaleonEmbed *embed);

gboolean      galeon_embed_can_go_back          (GaleonEmbed *embed);

gboolean      galeon_embed_can_go_forward       (GaleonEmbed *embed);

gboolean      galeon_embed_can_go_up            (GaleonEmbed *embed);

GSList *      galeon_embed_get_go_up_list       (GaleonEmbed *embed);

void          galeon_embed_go_back              (GaleonEmbed *embed);

void          galeon_embed_go_forward           (GaleonEmbed *embed);

void          galeon_embed_go_up                (GaleonEmbed *embed);

gboolean      galeon_embed_render_data          (GaleonEmbed *embed, 
					         const char *data,
					         guint32 len,
					         const char *base_uri, 
					         const char *mime_type);

gboolean       galeon_embed_open_stream          (GaleonEmbed *embed,
					         const char *base_uri,
					         const char *mime_type);

void           galeon_embed_append_data          (GaleonEmbed *embed,
					         const char *data, 
						 guint32 len);

void          galeon_embed_close_stream         (GaleonEmbed *embed);

char *        galeon_embed_get_title            (GaleonEmbed *embed);

char *        galeon_embed_get_location         (GaleonEmbed *embed, 
						 gboolean toplevel,
						 gboolean requested);

void          galeon_embed_reload               (GaleonEmbed *embed, 
				       	         EmbedReloadFlags flags);

void	      galeon_embed_copy_page		(GaleonEmbed *dest,
						 GaleonEmbed *source,
						 EmbedDisplayType display_type);

gboolean      galeon_embed_can_view_source      (GaleonEmbed *embed);

void          galeon_embed_evaluate_javascript  (GaleonEmbed *embed,
						 const char *script);

/* Link */
GList *       galeon_embed_get_link_tags        (GaleonEmbed *embed,
						 const char *link_type);

/* Zoom */
void          galeon_embed_zoom_set             (GaleonEmbed *embed, 
					         int zoom);

int           galeon_embed_zoom_get             (GaleonEmbed *embed);

/* Clipboard */
gboolean       galeon_embed_selection_can_cut    (GaleonEmbed *embed);

gboolean       galeon_embed_selection_can_copy   (GaleonEmbed *embed);

gboolean       galeon_embed_can_paste            (GaleonEmbed *embed);

void           galeon_embed_selection_cut        (GaleonEmbed *embed);

void           galeon_embed_selection_copy       (GaleonEmbed *embed);

void           galeon_embed_paste                (GaleonEmbed *embed);

void           galeon_embed_select_all           (GaleonEmbed *embed);

/* Session history */
int            galeon_embed_shistory_count       (GaleonEmbed *embed);

gboolean       galeon_embed_shistory_get_nth     (GaleonEmbed *embed, 
						 int nth,
						 gboolean is_relative,
						 char **url,
						 char **title);

int            galeon_embed_shistory_get_pos     (GaleonEmbed *embed);

gboolean       galeon_embed_shistory_go_nth      (GaleonEmbed *embed, 
						 int nth);

void           galeon_embed_shistory_copy	(GaleonEmbed *source,
						 GaleonEmbed *dest,
						 gboolean back_history,
						 gboolean forward_history,
						 gboolean set_current);

void           galeon_embed_shistory_clear	(GaleonEmbed *embed);

/* Utils */

void           galeon_embed_scroll               (GaleonEmbed *embed, 
		  			         EmbedScrollDirection direction);

void           galeon_embed_scroll_page         (GaleonEmbed *embed, 
		  			         EmbedScrollDirection direction);

void           galeon_embed_fine_scroll          (GaleonEmbed *embed, 
		  			         int horiz, int vert);

gboolean      galeon_embed_get_security_level   (GaleonEmbed *embed, 
						  EmbedSecurityLevel *level, 
						  char **description);

gboolean      galeon_embed_set_encoding         (GaleonEmbed *embed,
					         const char *charset);
GaleonEncodingPageInfo * galeon_embed_get_encoding_info (GaleonEmbed *embed);

gboolean     galeon_embed_has_modified_forms   (GaleonEmbed *embed);
/* Printing */

gboolean      galeon_embed_print                (GaleonEmbed *embed);

void	      galeon_embed_print_preview_close  (GaleonEmbed *embed);

int           galeon_embed_print_preview_num_pages (GaleonEmbed *embed);

gboolean      galeon_embed_print_preview_navigate  (GaleonEmbed *embed,
						    EmbedPrintPreviewNavType navType,
						    gint pageNum);

/* Misc */
EmbedPageProperties*  galeon_embed_get_page_properties (GaleonEmbed *embed);

void          galeon_embed_show_page_certificate (GaleonEmbed *embed);


/* Stylesheets */
							 
GList *   galeon_embed_get_stylesheets		(GaleonEmbed *embed);

EmbedStyleSheet * galeon_embed_get_selected_stylesheet	(GaleonEmbed *embed);

void      galeon_embed_set_stylesheet		(GaleonEmbed *embed,
						 EmbedStyleSheet *sheet);
gboolean   galeon_embed_apply_user_stylesheet	(GaleonEmbed *embed,
						 const gchar *sheetfile,
						 EmbedStyleSheet **retSheet);
void      galeon_embed_remove_user_stylesheet	(GaleonEmbed *embed,
						 EmbedStyleSheet *sheet);

/* helper functions */

EmbedStyleSheet *galeon_embed_stylesheet_new	(EmbedStyleSheetType type, const char *name);

gboolean  galeon_embed_stylesheet_compare	(EmbedStyleSheet *ess1, EmbedStyleSheet *ess2);
void	  galeon_embed_stylesheet_free		(EmbedStyleSheet *ess);
void	  galeon_embed_stylesheet_list_free	(GList *l);


G_END_DECLS

#endif
