/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GALEON_EMBED_EVENT_H
#define GALEON_EMBED_EVENT_H

#include "galeon-embed-types.h"

#include <glib-object.h>
#include <glib.h>

G_BEGIN_DECLS

typedef struct GaleonEmbedEventClass GaleonEmbedEventClass;

#define GALEON_TYPE_EMBED_EVENT             (galeon_embed_event_get_type ())
#define GALEON_EMBED_EVENT(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GALEON_TYPE_EMBED_EVENT, GaleonEmbedEvent))
#define GALEON_EMBED_EVENT_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GALEON_PERSIST_SHELL, GaleonEmbedEventClass))
#define GALEON_IS_EMBED_EVENT(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GALEON_TYPE_EMBED_EVENT))
#define GALEON_IS_EMBED_EVENT_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GALEON_EMBED_EVENT))
#define GALEON_EMBED_EVENT_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GALEON_TYPE_EMBED_SHELL, GaleonEmbedEventClass))

typedef struct GaleonEmbedEvent GaleonEmbedEvent;
typedef struct GaleonEmbedEventPrivate GaleonEmbedEventPrivate;

typedef enum
{
	EMBED_CONTEXT_NONE     = 0,
        EMBED_CONTEXT_DEFAULT  = 1 << 1,
        EMBED_CONTEXT_LINK     = 1 << 2,
        EMBED_CONTEXT_IMAGE    = 1 << 3,
        EMBED_CONTEXT_DOCUMENT = 1 << 4,
        EMBED_CONTEXT_INPUT    = 1 << 5,
        EMBED_CONTEXT_XUL      = 1 << 7,
	EMBED_CONTEXT_EMAIL_LINK = 1 << 8,
	EMBED_CONTEXT_SIDEBAR = 1 << 9
} EmbedEventContext;

struct GaleonEmbedEvent 
{
        GObject parent;
        GaleonEmbedEventPrivate *priv;

	/* Public to the embed implementations */
	guint modifier;
	guint context;
	guint x, y;
	guint mouse_button;
	guint keycode;
};

struct GaleonEmbedEventClass
{
        GObjectClass parent_class;
};

GType             galeon_embed_event_get_type    	(void);

GaleonEmbedEvent *galeon_embed_event_new         	(void);

guint		  galeon_embed_event_get_modifier	(GaleonEmbedEvent *event);

guint		  galeon_embed_event_get_mouse_button	(GaleonEmbedEvent *event);

void		  galeon_embed_event_get_coords		(GaleonEmbedEvent *event,
							 guint *x, guint *y);

EmbedEventContext  galeon_embed_event_get_context 	(GaleonEmbedEvent *event);

void		  galeon_embed_event_set_property 	(GaleonEmbedEvent *event,
							 const char *name,
							 GValue *value);

const GValue *	  galeon_embed_event_get_property	(GaleonEmbedEvent *event,
							 const char *name);

gboolean	  galeon_embed_event_has_property	(GaleonEmbedEvent *event,
							 const char *name);


G_END_DECLS

#endif
