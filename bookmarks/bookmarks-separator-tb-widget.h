/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __bookmarks_separator_tb_widget_h
#define __bookmarks_separator_tb_widget_h

#include <glib.h>
#include <glib-object.h>
#include "bookmarks-util.h"
#include "bookmarks-tb-widget.h"

/* object forward declarations */

typedef struct _GbSeparatorTbWidget GbSeparatorTbWidget;
typedef struct _GbSeparatorTbWidgetClass GbSeparatorTbWidgetClass;
typedef struct _GbSeparatorTbWidgetPrivate GbSeparatorTbWidgetPrivate;

/**
 * GbSeparatorTbWidget object
 */

#define GB_TYPE_SEPARATOR_TB_WIDGET		(gb_separator_tb_widget_get_type())
#define GB_SEPARATOR_TB_WIDGET(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), \
						 GB_TYPE_SEPARATOR_TB_WIDGET,\
					 	 GbSeparatorTbWidget))
#define GB_SEPARATOR_TB_WIDGET_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), \
						 GB_TYPE_SEPARATOR_TB_WIDGET,\
					 	 GbSeparatorTbWidgetClass))
#define GB_IS_SEPARATOR_TB_WIDGET(object)	(G_TYPE_CHECK_INSTANCE_TYPE((object), \
						 GB_TYPE_SEPARATOR_TB_WIDGET))
#define GB_IS_SEPARATOR_TB_WIDGET_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), \
						 GB_TYPE_SEPARATOR_TB_WIDGET))
#define GB_SEPARATOR_TB_WIDGET_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj),\
						 GB_TYPE_SEPARATOR_TB_WIDGET,\
						 GbSeparatorTbWidgetClass))

struct _GbSeparatorTbWidgetClass 
{
	GbTbWidgetClass parent_class;
};

/* Remember: fields are public read-only */
struct _GbSeparatorTbWidget
{
	GbTbWidget parent_object;

	GbSeparatorTbWidgetPrivate *priv;
};

GType			gb_separator_tb_widget_get_type	(void);
GbSeparatorTbWidget *	gb_separator_tb_widget_new	(GbSeparator *separator);

#endif
