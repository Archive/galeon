/*
 *  Copyright (C) 2003 Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtktoolitem.h>

#include "galeon-action-favicon.h"
#include "galeon-embed-favicon.h"
#include "galeon-shell.h"
#include "galeon-dnd.h"

#include <glib/gi18n.h>
#include <gtk/gtkeventbox.h>
#include <string.h>

static void		galeon_action_favicon_finalize			(GObject *object);
static void		galeon_action_favicon_connect_proxy		(GtkAction *action, GtkWidget *proxy);
static void		galeon_action_favicon_update			(GaleonAction *action);
static void		galeon_action_favicon_current_embed_changed	(GaleonAction *action, 
									 GaleonEmbed *old_embed, GaleonEmbed *new_embed);
static GtkWidget *	galeon_action_favicon_create_toolitem		(GtkAction *action);


G_DEFINE_TYPE (GaleonActionFavicon, galeon_action_favicon, GALEON_TYPE_ACTION);


static void
galeon_action_favicon_class_init (GaleonActionFaviconClass *class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);
	GtkActionClass *action_class = GTK_ACTION_CLASS (class);
	GaleonActionClass *galeon_action_class = GALEON_ACTION_CLASS (class);

	object_class->finalize = galeon_action_favicon_finalize;

	action_class->create_tool_item = galeon_action_favicon_create_toolitem;
	action_class->connect_proxy = galeon_action_favicon_connect_proxy;

	galeon_action_class->update = galeon_action_favicon_update;
	galeon_action_class->current_embed_changed = galeon_action_favicon_current_embed_changed;

}

static void
galeon_action_favicon_init (GaleonActionFavicon *action)
{
	g_object_set (G_OBJECT (action), "tooltip", _("Drag handle"), NULL);
	g_object_set (G_OBJECT (action), "label", _("Drag handle"), NULL);
}

static void
galeon_action_favicon_finalize (GObject *object)
{
	G_OBJECT_CLASS (galeon_action_favicon_parent_class)->finalize (object);
}

static void
galeon_action_favicon_update (GaleonAction *action)
{
	GSList *sli;
	GaleonEmbed *embed = galeon_action_get_embed (action);
	
	for (sli = gtk_action_get_proxies (GTK_ACTION (action)); sli; sli = sli->next)
	{
		GaleonEmbedFavicon *gfi = GALEON_EMBED_FAVICON (GTK_BIN (GTK_BIN (sli->data)->child)->child);
		galeon_embed_favicon_set_embed (gfi, embed);
	}
}

static void
galeon_action_favicon_current_embed_changed (GaleonAction *action, 
					      GaleonEmbed *old_embed, GaleonEmbed *new_embed)
{
	if (old_embed)
	{
		g_signal_handlers_disconnect_matched (old_embed, G_SIGNAL_MATCH_DATA, 0, 0, NULL, NULL, action);
	}

	if (new_embed)
	{
	}

	galeon_action_favicon_update (action);
}

static void
each_url_get_data_binder (GaleonDragEachSelectedItemDataGet iteratee, 
        		  gpointer iterator_context, gpointer data)
{
	const char *location, *title;
	GaleonTab *tab;
	GaleonWindow *window = GALEON_WINDOW (iterator_context);

	tab = galeon_window_get_active_tab (window);
	location = galeon_tab_get_location (tab);
	title = galeon_tab_get_title (tab);

	if (strcmp (location, "about:blank"))
	{
		iteratee (location, title, -1, -1, -1, -1, data);
	}
}


static void
galeon_action_favicon_drag_data_get_cb (GtkWidget *widget,
					GdkDragContext *context,
					GtkSelectionData *selection_data,
					guint info,
					guint32 time,
					GaleonAction *action)
{
	GaleonWindow *window = galeon_action_get_window (action);

        g_assert (widget != NULL);
        g_return_if_fail (context != NULL);

        galeon_dnd_drag_data_get (widget, context, selection_data,
				  info, time, window, each_url_get_data_binder);
}

static gboolean
toolitem_create_menu_proxy_cb (GtkToolItem *toolitem, gpointer dummy)
{
	/* Don't show item in overflow menu */
	gtk_tool_item_set_proxy_menu_item (toolitem, "galeon-favicon-menu-item", NULL);
	return TRUE;
}

static GtkWidget *
galeon_action_favicon_create_toolitem (GtkAction *action)
{
	GtkToolItem *ti = gtk_tool_item_new ();
	GtkWidget *eieb = gtk_event_box_new ();
	GtkWidget *gfi = galeon_embed_favicon_new (NULL);
	gtk_event_box_set_visible_window (GTK_EVENT_BOX (eieb), FALSE);
	gtk_container_add (GTK_CONTAINER (ti), eieb);
	gtk_container_add (GTK_CONTAINER (eieb), gfi);
	gtk_container_set_border_width (GTK_CONTAINER (eieb), 2);
	gtk_widget_show (gfi);
	gtk_widget_show (eieb);

	galeon_dnd_url_drag_source_set (GTK_WIDGET (eieb));
	g_signal_connect (eieb, "drag_data_get",
			  G_CALLBACK (galeon_action_favicon_drag_data_get_cb), action);
	g_signal_connect (ti, "create-menu-proxy",
			  G_CALLBACK (toolitem_create_menu_proxy_cb), NULL);

	return GTK_WIDGET (ti);
}

static void
galeon_action_favicon_connect_proxy (GtkAction *action, GtkWidget *proxy)
{
	(* GTK_ACTION_CLASS (galeon_action_favicon_parent_class)->connect_proxy) (action, proxy);
}

