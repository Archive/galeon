/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n.h>
#include "bookmarks-import-druid-mozilla.h"
#include "gul-string.h"
#include "gul-general.h"

#include "bookmarks-mozilla.h"

static GHashTable *strings = NULL;

/**
 * Private functions, only availble from this file
 */
static void		gb_import_druid_mozilla_finalize_impl		(GObject *o);
static void		gb_import_druid_mozilla_init_strings 		(void);
static const char *	gb_import_druid_mozilla_get_string_impl		(GbImportDruid *d, 
									 const char *string_id);
static GSList *		gb_import_druid_mozilla_get_locations_impl	(GbImportDruid *d);
static GbIO *		gb_import_druid_mozilla_get_io_impl		(GbImportDruid *d);


/**
 * ImportDruidMozilla object
 */

G_DEFINE_TYPE (GbImportDruidMozilla, gb_import_druid_mozilla, GB_TYPE_IMPORT_DRUID);

static void
gb_import_druid_mozilla_class_init (GbImportDruidMozillaClass *klass)
{
	GB_IMPORT_DRUID_CLASS (klass)->get_locations = gb_import_druid_mozilla_get_locations_impl;
	GB_IMPORT_DRUID_CLASS (klass)->get_io = gb_import_druid_mozilla_get_io_impl;
	GB_IMPORT_DRUID_CLASS (klass)->get_string = gb_import_druid_mozilla_get_string_impl;
	G_OBJECT_CLASS (klass)->finalize = gb_import_druid_mozilla_finalize_impl;
}

static void 
gb_import_druid_mozilla_init (GbImportDruidMozilla *e)
{
}

static void
gb_import_druid_mozilla_finalize_impl (GObject *o)
{
	G_OBJECT_CLASS (gb_import_druid_mozilla_parent_class)->finalize (o);
}

GbImportDruidMozilla *
gb_import_druid_mozilla_new (void)
{
	GbImportDruidMozilla *ret = g_object_new (GB_TYPE_IMPORT_DRUID_MOZILLA, NULL);
	return ret;
}

static const char *
gb_import_druid_mozilla_get_string_impl (GbImportDruid *d, 
					 const char *string_id)
{
	gb_import_druid_mozilla_init_strings ();
	return _(g_hash_table_lookup (strings, string_id));
}

static void
gb_import_druid_mozilla_init_strings (void)
{
	if (!strings)
	{
		static struct 
		{
			const char *id;
			const char *text;
		} strs[] = {
			{ "window title", N_("Mozilla Bookmarks Import Druid") },
			{ "page 1 title", N_("Mozilla Bookmarks Import") },
			{NULL, NULL}
		};			
		int i;
		strings = g_hash_table_new (g_str_hash, g_str_equal);
		for (i = 0; strs[i].id != NULL; ++i)
		{
			g_hash_table_insert (strings, (gpointer) strs[i].id, (gpointer) strs[i].text);
		}
	}
}

static GSList *
gb_import_druid_mozilla_get_locations_impl (GbImportDruid *druid)
{
	GSList *l;
	gchar *dir = g_build_filename (g_get_home_dir (), ".mozilla", NULL);
	l = gul_find_file  (dir, "bookmarks.html", 4);
	g_free (dir);
	return l;
}

static GbIO *
gb_import_druid_mozilla_get_io_impl (GbImportDruid *druid)
{
	GbIOMozilla *io = gb_io_mozilla_new ();
	return GB_IO (io);
}

