/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __bookmarks_export_druid_konqueror_h
#define __bookmarks_export_druid_konqueror_h

#include <glib-object.h>
#include <bookmarks-export-druid.h>

/* object forward declarations */

typedef struct _GbExportDruidKonqueror GbExportDruidKonqueror;
typedef struct _GbExportDruidKonquerorClass GbExportDruidKonquerorClass;
typedef struct _GbExportDruidKonquerorPrivate GbExportDruidKonquerorPrivate;

/**
 * Druid object
 */

#define GB_TYPE_EXPORT_DRUID_KONQUEROR		(gb_export_druid_konqueror_get_type())
#define GB_EXPORT_DRUID_KONQUEROR(object)	(G_TYPE_CHECK_INSTANCE_CAST((object), \
						 GB_TYPE_EXPORT_DRUID_KONQUEROR,\
						 GbExportDruidKonqueror))
#define GB_EXPORT_DRUID_KONQUEROR_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), \
						 GB_TYPE_EXPORT_DRUID_KONQUEROR,\
						 GbExportDruidKonquerorClass))
#define GB_IS_EXPORT_DRUID_KONQUEROR(object)	(G_TYPE_CHECK_INSTANCE_TYPE((object), \
						 GB_TYPE_EXPORT_DRUID_KONQUEROR))
#define GB_IS_EXPORT_DRUID_KONQUEROR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), \
						   GB_TYPE_EXPORT_DRUID_KONQUEROR))
#define GB_EXPORT_DRUID_KONQUEROR_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), \
						  GB_TYPE_EXPORT_DRUID_KONQUEROR,\
						  GbExportDruidKonquerorClass))

struct _GbExportDruidKonquerorClass 
{
	GbExportDruidClass parent_class;

};

/* Remember: fields are public read-only */
struct _GbExportDruidKonqueror
{
	GbExportDruid parent_object;
};

GType				gb_export_druid_konqueror_get_type		(void);
GbExportDruidKonqueror *	gb_export_druid_konqueror_new			(void);

#endif
