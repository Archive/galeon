/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __bookmarks_import_druid_netscape_h
#define __bookmarks_import_druid_netscape_h

#include <glib-object.h>
#include <bookmarks-import-druid.h>

/* object forward declarations */

typedef struct _GbImportDruidNetscape GbImportDruidNetscape;
typedef struct _GbImportDruidNetscapeClass GbImportDruidNetscapeClass;
typedef struct _GbImportDruidNetscapePrivate GbImportDruidNetscapePrivate;

/**
 * Druid object
 */

#define GB_TYPE_IMPORT_DRUID_NETSCAPE		(gb_import_druid_netscape_get_type())
#define GB_IMPORT_DRUID_NETSCAPE(object)	(G_TYPE_CHECK_INSTANCE_CAST((object), \
						 GB_TYPE_IMPORT_DRUID_NETSCAPE,\
						 GbImportDruidNetscape))
#define GB_IMPORT_DRUID_NETSCAPE_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), \
						 GB_TYPE_IMPORT_DRUID_NETSCAPE,\
						 GbImportDruidNetscapeClass))
#define GB_IS_IMPORT_DRUID_NETSCAPE(object)	(G_TYPE_CHECK_INSTANCE_TYPE((object), \
						 GB_TYPE_IMPORT_DRUID_NETSCAPE))
#define GB_IS_IMPORT_DRUID_NETSCAPE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), \
						  GB_TYPE_IMPORT_DRUID_NETSCAPE))
#define GB_IMPORT_DRUID_NETSCAPE_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), \
						 GB_TYPE_IMPORT_DRUID_NETSCAPE,\
						 GbImportDruidNetscapeClass))

struct _GbImportDruidNetscapeClass 
{
	GbImportDruidClass parent_class;

};

/* Remember: fields are public read-only */
struct _GbImportDruidNetscape
{
	GbImportDruid parent_object;
};

GType			gb_import_druid_netscape_get_type		(void);
GbImportDruidNetscape *	gb_import_druid_netscape_new			(void);

#endif
