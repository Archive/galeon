/*
 *  Copyright (C) 2003 Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GALEON_SPINNER_ACTION_H
#define GALEON_SPINNER_ACTION_H


#include "galeon-action.h"

#define GALEON_TYPE_ACTION_SPINNER		(galeon_action_spinner_get_type ())
#define GALEON_ACTION_SPINNER(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), GALEON_TYPE_ACTION_SPINNER,\
						 GaleonActionSpinner))
#define GALEON_ACTION_SPINNER_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), GALEON_TYPE_ACTION_SPINNER,\
						 GaleonActionSpinnerClass))
#define GALEON_IS_ACTION_SPINNER(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GALEON_TYPE_ACTION_SPINNER))
#define GALEON_IS_ACTION_SPINNER_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((obj), GALEON_TYPE_ACTION_SPINNER))
#define GALEON_ACTION_SPINNER_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), GALEON_TYPE_ACTION_SPINNER,\
						 GaleonActionSpinnerClass))

typedef struct _GaleonActionSpinner		GaleonActionSpinner;
typedef struct _GaleonActionSpinnerClass	GaleonActionSpinnerClass;
typedef struct GaleonActionSpinnerPrivate	GaleonActionSpinnerPrivate;

struct _GaleonActionSpinner
{
	GaleonAction parent;
};

struct _GaleonActionSpinnerClass
{
	GaleonActionClass parent_class;

};

GType		galeon_action_spinner_get_type			(void);

#endif
