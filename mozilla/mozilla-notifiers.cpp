/*
 *  Copyright (C) 2000 Nate Case 
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, OR (AT YOUR OPTION)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "galeon-embed-shell.h"
#include "mozilla-notifiers.h"
#include "eel-gconf-extensions.h"
#include "mozilla-prefs.h"
#include "prefs-strings.h"
#include "galeon-embed-prefs.h"
#include "mozilla-i18n.h"

#include <glib/gi18n.h>
#include <stdio.h>
#include <string.h>

static void
mozilla_own_colors_notifier(GConfClient *client,
			    guint cnxn_id,
			    GConfEntry *entry,
			    gpointer user_data);
static void
mozilla_own_fonts_notifier(GConfClient *client,
			   guint cnxn_id,
			   GConfEntry *entry,
			   gpointer user_data);

static void
mozilla_animate_notifier(GConfClient *client,
			 guint cnxn_id,
			 GConfEntry *entry,
			 gpointer user_data);
static void
generic_mozilla_string_notifier(GConfClient *client,
				guint cnxn_id,
				GConfEntry *entry,
				const char *pref_name);
static void
generic_mozilla_int_notifier(GConfClient *client,
			     guint cnxn_id,
			     GConfEntry *entry,
			     const char *pref_name);
static void
generic_mozilla_bool_notifier(GConfClient *client,
			      guint cnxn_id,
			      GConfEntry *entry,
			      const char *pref_name);
static void
mozilla_http_version_notifier(GConfClient *client,
			      guint cnxn_id,
			      GConfEntry *entry,
			      gpointer user_data);

static void
mozilla_allow_popups_notifier(GConfClient *client,
			      guint cnxn_id,
			      GConfEntry *entry,
			      gpointer user_data);

static void
mozilla_language_notifier(GConfClient *client,
			  guint cnxn_id,
			  GConfEntry *entry,
			  gpointer user_data);

static void
mozilla_autodetect_charset_notifier(GConfClient *client,
				    guint cnxn_id,
				    GConfEntry *entry,
				    GaleonEmbedShell *shell);

static void
mozilla_default_font_notifier(GConfClient *client,
			      guint cnxn_id,
			      GConfEntry *entry,
			      gpointer user_data);

static void
mozilla_proxy_mode_notifier (GConfClient *client,
			     guint cnxn_id,
			     GConfEntry *entry,
			     char *pref);

static void
mozilla_proxy_ignore_notifier (GConfClient *client,
			     guint cnxn_id,
			     GConfEntry *entry,
			     char *pref);

static void
mozilla_user_agent_notifier(GConfClient *client,
			    guint cnxn_id,
			    GConfEntry *entry,
			    gpointer user_data);

static void
mozilla_socks_version_notifier (GConfClient *client,
				guint cnxn_id,
				GConfEntry *entry,
				gpointer user_data);

/* Keeps the list of the notifiers we installed for mozilla prefs */
/* to be able to remove them when exiting */
GList *mozilla_notifiers = NULL;
GList *font_infos = NULL;

enum
{
	BOOL_PREF,
	INT_PREF,
	STRING_PREF
};

static const struct 
{
	const char *gconf_key;
	guint pref_type;
	const char *mozilla_key;
}
conversion_table [] =
{
	{ CONF_FILTERING_JAVA_ENABLED, BOOL_PREF, "security.enable_java"},
	{ CONF_FILTERING_JAVASCRIPT_ENABLED, BOOL_PREF, "javascript.enabled"},
	{ CONF_FILTERING_IMAGE_LOADING_TYPE, INT_PREF, "permissions.default.image"},
	{ CONF_RENDERING_BG_COLOR, STRING_PREF, "browser.display.background_color"},
	{ CONF_RENDERING_TEXT_COLOR, STRING_PREF, "browser.display.foreground_color"},
	{ CONF_RENDERING_UNVISITED_LINKS, STRING_PREF, "browser.anchor_color"},
	{ CONF_RENDERING_VISITED_LINKS, STRING_PREF, "browser.visited_color"},
	{ CONF_RENDERING_UNDERLINE_LINKS, BOOL_PREF, "browser.underline_anchors"},
  	{ CONF_LANGUAGE_DEFAULT_CHARSET, STRING_PREF, "intl.charset.default" },
	{ CONF_NETWORK_PROXY_AUTO_URL, STRING_PREF, "network.proxy.autoconfig_url"},
	{ CONF_NETWORK_HTTP_PROXY, STRING_PREF, "network.proxy.http"},
	{ CONF_NETWORK_FTP_PROXY, STRING_PREF, "network.proxy.ftp"},
	{ CONF_NETWORK_SSL_PROXY, STRING_PREF, "network.proxy.ssl"},
	{ CONF_NETWORK_SOCKS_PROXY, STRING_PREF, "network.proxy.socks"},
	{ CONF_NETWORK_HTTP_PROXY_PORT, INT_PREF, "network.proxy.http_port"},
	{ CONF_NETWORK_FTP_PROXY_PORT, INT_PREF, "network.proxy.ftp_port"},
	{ CONF_NETWORK_SSL_PROXY_PORT, INT_PREF, "network.proxy.ssl_port"},
	{ CONF_NETWORK_SOCKS_PROXY_PORT, INT_PREF, "network.proxy.socks_port"},
	{ CONF_NETWORK_PROXY_KA, BOOL_PREF, "network.http.proxy.keep-alive"},
	{ CONF_NETWORK_DISK_CACHE, INT_PREF, "browser.cache.disk.capacity"},
	{ CONF_NETWORK_CACHE_COMPARE, INT_PREF, "browser.cache.check_doc_frequency"},
	{ CONF_MOUSE_WHEEL_NOKEY_ACTION, INT_PREF, "mousewheel.withnokey.action"},
	{ CONF_MOUSE_WHEEL_NOKEY_DEF_STEP, BOOL_PREF, "mousewheel.withnokey.sysnumlines"},
	{ CONF_MOUSE_WHEEL_NOKEY_STEP, INT_PREF, "mousewheel.withnokey.numlines"},
	{ CONF_PERSISTENT_COOKIES_BEHAVIOR, INT_PREF, "network.cookie.cookieBehavior"},
	{ CONF_PERSISTENT_COOKIES_LIFETIME, INT_PREF, "network.cookie.lifetimePolicy"},
	{ CONF_PERSISTENT_COOKIE_ACCEPT_SESSION, BOOL_PREF, "network.cookie.alwaysAcceptSessionCookies"},
	{ CONF_PERSISTENT_PASSWORDS_SAVE, BOOL_PREF, "signon.rememberSignons"},
	{ CONF_RENDERING_USE_SYSTEM_COLORS, BOOL_PREF, "browser.display.use_system_colors"},
	{ CONF_DOWNLOADING_DIR, STRING_PREF, "browser.download.dir"},
	{ NULL, 0, NULL }
};

static const struct 
{
	const char *gconf_key;
	GConfClientNotifyFunc func;
}
custom_notifiers [] =
{
	{ CONF_NETWORK_USER_AGENT, 
	  (GConfClientNotifyFunc) mozilla_user_agent_notifier },
	{ CONF_FILTERING_ANIMATE_TYPE, 
	  (GConfClientNotifyFunc) mozilla_animate_notifier },
	{ CONF_RENDERING_USE_OWN_COLORS, 
	  (GConfClientNotifyFunc) mozilla_own_colors_notifier },
	{ CONF_RENDERING_USE_OWN_FONTS, 
	  (GConfClientNotifyFunc) mozilla_own_fonts_notifier },
	{ CONF_NETWORK_HTTP_VERSION, 
	  (GConfClientNotifyFunc) mozilla_http_version_notifier },
	{ CONF_FILTERING_ALLOW_POPUPS, 
	  (GConfClientNotifyFunc) mozilla_allow_popups_notifier },
	{ CONF_RENDERING_LANGUAGE, 
	  (GConfClientNotifyFunc) mozilla_language_notifier },
	{ CONF_LANGUAGE_AUTODETECT_CHARSET, 
	  (GConfClientNotifyFunc) mozilla_autodetect_charset_notifier },
	{ CONF_RENDERING_DEFAULT_FONT, 
	  (GConfClientNotifyFunc) mozilla_default_font_notifier },
	{ CONF_NETWORK_SOCKS_PROXY_VERSION, 
	  (GConfClientNotifyFunc) mozilla_socks_version_notifier },
	{ CONF_NETWORK_PROXY_MODE,
	  (GConfClientNotifyFunc) mozilla_proxy_mode_notifier },
	{ CONF_NETWORK_PROXY_IGNORE_HOSTS,
	  (GConfClientNotifyFunc) mozilla_proxy_ignore_notifier },
	{NULL, NULL}
};

static void
mozilla_proxy_mode_notifier (GConfClient *client,
		             guint cnxn_id,
		             GConfEntry *entry,
		             char *pref)
{
	char *mode;
	int mozilla_mode = 0;
	
	mode = eel_gconf_get_string (entry->key);
	if (mode == NULL) return;
	
	if (strcmp (mode, "manual") == 0)
	{
		mozilla_mode = 1;
	}
	else if (strcmp (mode, "auto") == 0)
	{
		mozilla_mode = 2;
	}

	mozilla_prefs_set_int ("network.proxy.type", mozilla_mode);

	g_free (mode);
}

static void
mozilla_proxy_ignore_notifier (GConfClient *client,
		             guint cnxn_id,
		             GConfEntry *entry,
		             char *pref)
{
	GSList *hosts, *l;
	char **strings, *mozilla_ignore_list;
	int i = 0;

	hosts = eel_gconf_get_string_list (entry->key);

	strings = g_new (gchar*, g_slist_length (hosts) + 1);
	for (l = hosts; l != NULL; l = l->next)
	{
		char *item = (char *) l->data;

		if (item && item[0] != '\0')
		{
			strings[i] = item;
			i++;
		}
	}
	strings[i] = NULL;

	mozilla_ignore_list = g_strjoinv (", ", strings);
	mozilla_prefs_set_string ("network.proxy.no_proxies_on",
				  mozilla_ignore_list);

	g_free (mozilla_ignore_list);
	g_free (strings);
	g_slist_foreach (hosts, (GFunc) g_free, NULL);
	g_slist_free (hosts);
}

static void
mozilla_font_notifier (GConfClient *client,
		       guint cnxn_id,
		       GConfEntry *entry,
		       char *pref)
{
	char key[255];
	char *name;

	if (entry->value == NULL) return;
	
	sprintf (key, "font.name.%s", pref);

	name = eel_gconf_get_string (entry->key);
	if (name)
	{
		mozilla_prefs_set_string (key, name);
		g_free (name);
	}
}

static void
add_notification_and_notify (GConfClient		*client,
			     const char 		*key,
			     GConfClientNotifyFunc	 func,
			     gpointer			 user_data)
{
	GConfEntry *entry;
	GError *error = NULL;
	guint cnxn_id;

	cnxn_id = gconf_client_notify_add (client, key, func, user_data, NULL, &error);
	if (eel_gconf_handle_error (&error))
	{
		if (cnxn_id != EEL_GCONF_UNDEFINED_CONNECTION)
		{
			gconf_client_notify_remove (client, cnxn_id);
		}
		return;
	}

	mozilla_notifiers = g_list_append (mozilla_notifiers,
			                   GUINT_TO_POINTER (cnxn_id));

	entry = gconf_client_get_entry (client, key, NULL, TRUE, &error);
	if (eel_gconf_handle_error (&error))
	{
		return;
	}
	g_return_if_fail (entry != NULL);

	if (entry->value != NULL)
	{
		func (client, cnxn_id, entry, user_data);
	}
	gconf_entry_unref (entry);
}

void 
mozilla_notifiers_init(MozillaEmbedShell *shell) 
{
	GConfClient *client = eel_gconf_client_get_global ();
	int i;

	for (i = 0; conversion_table[i].gconf_key != NULL; i++)
	{
		GConfClientNotifyFunc func = NULL;

		switch (conversion_table[i].pref_type)
		{
		case INT_PREF:
			func = (GConfClientNotifyFunc) generic_mozilla_int_notifier;
			break;
		case BOOL_PREF:
			func = (GConfClientNotifyFunc) generic_mozilla_bool_notifier;
			break;
		case STRING_PREF:
			func = (GConfClientNotifyFunc) generic_mozilla_string_notifier;
			break;
		}

		g_assert (func != NULL);
		
		add_notification_and_notify (
			client,
			conversion_table[i].gconf_key,
			func,
			(gpointer)conversion_table[i].mozilla_key);

	}

	for (i = 0; custom_notifiers[i].gconf_key != NULL; i++)
	{
		add_notification_and_notify (
			client,
			custom_notifiers[i].gconf_key,
			custom_notifiers[i].func,
			(gpointer)shell);
	}

	/* fonts notifiers */
	for (i = 0; i < num_font_languages; i++)
	{
		int k;
		const char *types [] = { "serif", "sans-serif", "cursive", "fantasy", "monospace" };
		char key[255];
		char *info;
		const char* language = font_languages[i].key;

		for (k = 0; k < 5; k++)
		{
			info = g_strconcat (types[k], ".", language, NULL);
			
			sprintf (key, "%s_%s_%s", CONF_RENDERING_FONT, 
			 	 types[k], language);
			add_notification_and_notify (client,
						     key,
						     (GConfClientNotifyFunc)mozilla_font_notifier,
						     info);
			font_infos = g_list_append (font_infos, info);
		}

		sprintf (key, "%s_%s", CONF_RENDERING_FONT_MIN_SIZE, language);
		info = g_strconcat ("font.minimum-size", ".", language, NULL);
		add_notification_and_notify (client,
					     key,
					     (GConfClientNotifyFunc)generic_mozilla_int_notifier,
					     info);
		font_infos = g_list_append (font_infos, info);

		sprintf (key, "%s_%s", CONF_RENDERING_FONT_FIXED_SIZE, language);
		info = g_strconcat ("font.size.fixed", ".", language, NULL);
		add_notification_and_notify (client,
					     key,
					     (GConfClientNotifyFunc)generic_mozilla_int_notifier,
					     info);
		font_infos = g_list_append (font_infos, info);

		sprintf (key, "%s_%s", CONF_RENDERING_FONT_VAR_SIZE, language);
		info = g_strconcat ("font.size.variable", ".", language, NULL);
		add_notification_and_notify (client,
					     key,
					     (GConfClientNotifyFunc)generic_mozilla_int_notifier,
					     info);
		font_infos = g_list_append (font_infos, info);		
	}
}

void 
mozilla_notifiers_free (void)
{
	GList *l;
	
	galeon_notification_remove (&mozilla_notifiers);

	for (l = font_infos; l != NULL; l = l->next)
	{
		g_free (l->data);
	}
	
	g_list_free (font_infos);
}

/**
 * generic_mozilla_string_notify: update mozilla pref to match galeon prefs.
 * 	user_data should match the mozilla key
 */
static void
generic_mozilla_string_notifier(GConfClient *client,
				guint cnxn_id,
				GConfEntry *entry,
				const char *pref_name)
{
	char *value;

	value = eel_gconf_get_string (entry->key);
	if (value)
	{
		mozilla_prefs_set_string (pref_name, value);
		g_free (value);
	}
}


/**
 * generic_mozilla_int_notify: update mozilla pref to match galeon prefs.
 * 	user_data should match the mozilla key
 */
static void
generic_mozilla_int_notifier(GConfClient *client,
			     guint cnxn_id,
			     GConfEntry *entry,
			     const char *pref_name)
{
	int value;

	value = eel_gconf_get_integer (entry->key);
	mozilla_prefs_set_int (pref_name, value);
}


/**
 * generic_mozilla_bool_notify: update mozilla pref to match galeon prefs.
 * 	user_data should match the mozilla key
 */
static void
generic_mozilla_bool_notifier(GConfClient *client,
			      guint cnxn_id,
			      GConfEntry *entry,
			      const char *pref_name)
{
	gboolean value;

	value = eel_gconf_get_boolean (entry->key);
	mozilla_prefs_set_boolean (pref_name, value);
}

static void
mozilla_own_colors_notifier(GConfClient *client,
			    guint cnxn_id,
			    GConfEntry *entry,
			    gpointer user_data)
{
	gboolean value;

	value = eel_gconf_get_boolean (entry->key);
	mozilla_prefs_set_boolean ("browser.display.use_document_colors", !value);
}

static void
mozilla_own_fonts_notifier(GConfClient *client,
			   guint cnxn_id,
			   GConfEntry *entry,
			   gpointer user_data)
{
	int value;

	value = eel_gconf_get_boolean (entry->key) ? 0 : 1;
	mozilla_prefs_set_int("browser.display.use_document_fonts", value);
}

static void
mozilla_animate_notifier(GConfClient *client,
			 guint cnxn_id,
			 GConfEntry *entry,
			 gpointer user_data)
{
	int value;
	static const gchar *type[] =
	{
		"normal",
		"once",
		"none"
	};

        value = eel_gconf_get_integer (entry->key);
        if (value > 2 || value < 0) value = 0;

	mozilla_prefs_set_string ("image.animation_mode", type[value]);
}

static void
mozilla_http_version_notifier(GConfClient *client,
			      guint cnxn_id,
			      GConfEntry *entry,
			      gpointer user_data)
{
	gchar *http_version = NULL;
	int value;

	value = eel_gconf_get_integer (CONF_NETWORK_HTTP_VERSION);
	switch (value)
	{
		case 0: http_version = g_strdup ("1.0");
			break;
		case 1: http_version = g_strdup ("1.1");
			break;
		default:
			g_warning ("mozilla_http_version_notifier: "
				   "unsupported value: %d", value);
			return;
	}

	mozilla_prefs_set_string ("network.http.version", http_version);

	g_free (http_version);
}

static void
mozilla_allow_popups_notifier(GConfClient *client,
			      guint cnxn_id,
			      GConfEntry *entry,
			      gpointer user_data)
{
	gboolean new_val = eel_gconf_get_boolean(CONF_FILTERING_ALLOW_POPUPS);
	mozilla_prefs_set_boolean ("dom.disable_open_during_load", 
					!new_val);
}

static void
mozilla_language_notifier(GConfClient *client,
			  guint cnxn_id,
			  GConfEntry *entry,
			  gpointer user_data)
{
	gchar *languages;
	GSList *language_list ,*cur_lang_list;
	
	language_list = eel_gconf_get_string_list (CONF_RENDERING_LANGUAGE);

	languages = NULL;
	cur_lang_list = language_list;
	while (cur_lang_list != NULL) {
		char *lang, *tmp;

		lang = g_strdup((char *)cur_lang_list->data);
		
		if (languages == NULL)
			languages = lang;
		else {
			tmp = languages;
			languages = g_strconcat(languages, ",", lang, NULL);
			g_free(lang);
			g_free(tmp);
		}
		g_free(cur_lang_list->data);
		cur_lang_list = cur_lang_list->next;
	}

	if (languages == NULL)
	{
		languages = g_strdup ("");
	}

	mozilla_prefs_set_string ("intl.accept_languages", languages);
	g_free (languages);

	g_slist_free(language_list);
}

static const char *autodetect_charset_prefs[] =
{
        "",
        "zh_parallel_state_machine",
        "cjk_parallel_state_machine",
        "ja_parallel_state_machine",
        "ko_parallel_state_machine",
        "ruprob",
        "zhcn_parallel_state_machine",
        "zhtw_parallel_state_machine",
        "ukprob",
        "universal_charset_detector"
};

static void
mozilla_autodetect_charset_notifier(GConfClient *client,
				    guint cnxn_id,
				    GConfEntry *entry,
				    GaleonEmbedShell *shell)
{
	int charset = eel_gconf_get_integer (CONF_LANGUAGE_AUTODETECT_CHARSET);
	if (charset < 0 || 
	    charset >= (int)(sizeof(autodetect_charset_prefs)
		             / sizeof(autodetect_charset_prefs[0])))
	{
		g_warning ("mozilla_autodetect_charset_notifier: "
			   "unsupported value: %d", charset);
		return;
	}
	mozilla_prefs_set_string ("intl.charset.detector", 
				  autodetect_charset_prefs[charset]);
}

static void
mozilla_default_font_notifier(GConfClient *client,
			      guint cnxn_id,
			      GConfEntry *entry,
			      gpointer user_data)
{
	const gchar *font_types [] = {"serif","sans-serif"};
	int default_font;

	default_font = eel_gconf_get_integer (CONF_RENDERING_DEFAULT_FONT);
	if (default_font < 0 || 
	    default_font >= (int)(sizeof(font_types) / sizeof(font_types[0])))
	{
		g_warning ("mozilla_default_font_notifier: "
			   "unsupported value: %d", default_font);
		return;
	}
	mozilla_prefs_set_string ("font.default", font_types[default_font]);
}

static void
mozilla_prefs_set_user_agent ()
{
        gchar *value = eel_gconf_get_string (CONF_NETWORK_USER_AGENT);
        
        /* now, build a valid user agent string */
        if (!value || !strcmp ("", value) 
                   || !strcmp ("default", value) 
                   || !strcmp ("Galeon", value)
                   || !strcmp (_("Default (recommended)"), value)
                   || !strcmp ("Default (recommended)", value))
        {
		mozilla_prefs_remove ("general.useragent.override");
        }
        else
        {
		mozilla_prefs_set_string ("general.useragent.override", value);
        }

	g_free (value);
}

static void
mozilla_user_agent_notifier (GConfClient *client,
			     guint cnxn_id,
			     GConfEntry *entry,
			     gpointer user_data)
{
	mozilla_prefs_set_user_agent ();
}

static void
mozilla_socks_version_notifier (GConfClient *client,
				guint cnxn_id,
				GConfEntry *entry,
				gpointer user_data)
{
	int version;
	version = eel_gconf_get_integer (entry->key) + 4;
	mozilla_prefs_set_int ("network.proxy.socks_version", 
			       version);
}
