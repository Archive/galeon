/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "galeon-dialog.h"
#include "gul-glade.h"
#include "gul-prefs-utils.h"
#include "gul-gui.h"

#include <gtk/gtktogglebutton.h>
#include <gtk/gtkcombobox.h>
#include <gtk/gtkspinbutton.h>
#include <gtk/gtkentry.h>
#include <gtk/gtkwindow.h>
#include <gtk/gtkdialog.h>
#include <gtk/gtkexpander.h>

#include <string.h>

static void
galeon_dialog_class_init (GaleonDialogClass *klass);
static void
galeon_dialog_init (GaleonDialog *window);
static void
galeon_dialog_finalize (GObject *object);
static void
galeon_dialog_get_property (GObject *object,
             	            guint prop_id,
                            GValue *value,
                            GParamSpec *pspec);
static void
galeon_dialog_set_property (GObject *object,
             	            guint prop_id,
                            const GValue *value,
                            GParamSpec *pspec);

static void
galeon_dialog_set_parent (GaleonDialog *dialog, 
			  GtkWidget *parent);

static void 
impl_construct (GaleonDialog *dialog,
		const GaleonDialogProperty *properties,
		const char *file,
		const char *name);

static void 
impl_destruct (GaleonDialog *dialog);

static GtkWidget *
impl_get_control (GaleonDialog *dialog,
                  int property_id);
static void 
impl_get_value (GaleonDialog *dialog,
                int property_id,
                GValue *value);
static gint 
impl_run (GaleonDialog *dialog);
static void
impl_show (GaleonDialog *dialog);
void
galeon_dialog_destroy_cb (GtkWidget *widget, 
			  GaleonDialog *dialog);

enum
{
	PROP_0,
	PROP_PARENT_WINDOW,
	PROP_MODAL
};

typedef enum
{
        PT_TOGGLEBUTTON,
        PT_RADIOBUTTON,
        PT_SPINBUTTON,
        PT_COMBOBOX,
        PT_ENTRY,
	PT_EXPANDER,
	PT_UNKNOWN
} PrefType;

typedef struct
{
	int id;
	GtkWidget *widget;
	const char *pref;
	int *sg;
	PropertyType type;
} PropertyInfo;

#define GALEON_DIALOG_GET_PRIVATE(object) (G_TYPE_INSTANCE_GET_PRIVATE ((object), \
				       GALEON_TYPE_DIALOG, GaleonDialogPrivate))


struct GaleonDialogPrivate
{
	GtkWidget *parent;
	GtkWidget *dialog;
	GtkWidget *container;
	PropertyInfo *props;
	gboolean modal;
	char *name;
	
	int spin_item_id;
	GTimer *spin_timer;
	gboolean initialized;
};

#define SPIN_DELAY 0.20

static GObjectClass *parent_class = NULL;

GType 
galeon_dialog_get_type (void)
{
        static GType galeon_dialog_type = 0;

        if (galeon_dialog_type == 0)
        {
                static const GTypeInfo our_info =
                {
                        sizeof (GaleonDialogClass),
                        NULL, /* base_init */
                        NULL, /* base_finalize */
                        (GClassInitFunc) galeon_dialog_class_init,
                        NULL,
                        NULL, /* class_data */
                        sizeof (GaleonDialog),
                        0, /* n_preallocs */
                        (GInstanceInitFunc) galeon_dialog_init
                };

                galeon_dialog_type = g_type_register_static (G_TYPE_OBJECT,
                                                             "GaleonDialog",
                                                             &our_info, 0);
        }

        return galeon_dialog_type;
}

static void
galeon_dialog_class_init (GaleonDialogClass *klass)
{
        GObjectClass *object_class = G_OBJECT_CLASS (klass);

        parent_class = g_type_class_peek_parent (klass);

        object_class->finalize = galeon_dialog_finalize;
	object_class->set_property = galeon_dialog_set_property;
	object_class->get_property = galeon_dialog_get_property;
	
	klass->construct = impl_construct;
	klass->get_control = impl_get_control;
	klass->get_value = impl_get_value;
	klass->run = impl_run;
	klass->show = impl_show;
	klass->destruct = impl_destruct;

	g_object_class_install_property (object_class,
                                         PROP_PARENT_WINDOW,
                                         g_param_spec_object ("ParentWindow",
                                                              "ParentWindow",
                                                              "Parent window",
                                                              GTK_TYPE_WIDGET,
                                                              G_PARAM_READWRITE));
	
	g_object_class_install_property (object_class,
                                         PROP_MODAL,
                                         g_param_spec_boolean ("Modal",
                                                               "Modal",
                                                               "Modal dialog",
                                                               FALSE,
                                                               G_PARAM_READWRITE));

	g_type_class_add_private (klass, sizeof (GaleonDialogPrivate));
}

static PrefType
get_pref_type_from_widget (GtkWidget *widget)
{
	if (GTK_IS_COMBO_BOX (widget))
	{
		return PT_COMBOBOX;
	}
	else if (GTK_IS_SPIN_BUTTON (widget))
	{
		return PT_SPINBUTTON;
	}
	else if (GTK_IS_RADIO_BUTTON (widget))
	{
		return PT_RADIOBUTTON;
	}
	else if (GTK_IS_TOGGLE_BUTTON (widget))
	{
		return PT_TOGGLEBUTTON;
	}
	else if (GTK_IS_ENTRY (widget))
	{
		return PT_ENTRY;
	}
	else if (GTK_IS_EXPANDER (widget))
	{
		return PT_EXPANDER;
	}

	return PT_UNKNOWN;
}

static int *
set_controls_sensitivity (GaleonDialog *dialog,
			  int *sg, gboolean s)
{
	GtkWidget *widget;
	
	while (*sg != SY_END_GROUP)
	{
		widget = galeon_dialog_get_control (dialog,
						    *sg);
		gtk_widget_set_sensitive (widget, s);

		sg++;
	}
	
	return sg;
}

static void
prefs_set_group_sensitivity (GtkWidget *widget,
			     PrefType type, int *sg)
{
	int group = -1;
	GaleonDialog *dialog;
	int i = 0;

	if (sg == NULL) return;
	
	dialog = GALEON_DIALOG (g_object_get_data 
				(G_OBJECT(widget), "dialog"));
	
	if (GTK_IS_RADIO_BUTTON (widget))
	{
		group = gul_gui_gtk_radio_button_get 
			(GTK_RADIO_BUTTON(widget));
	}
	else if (GTK_IS_TOGGLE_BUTTON (widget))
	{
		group = !gtk_toggle_button_get_active
			(GTK_TOGGLE_BUTTON(widget));
	}
	else
	{
		g_assert (FALSE);
	}

	while (*sg != SY_END)
	{
		if ((*sg == SY_BEGIN_GROUP) ||
		    (*sg == SY_BEGIN_GROUP_INVERSE))
		{
			gboolean b;

			b = (i == group);
			if (*sg == SY_BEGIN_GROUP_INVERSE) b = !b;
			
			sg++;
			sg = set_controls_sensitivity 
				(dialog, sg, b);
		}

		i++;
		sg++;
	}
}

static void 
prefs_togglebutton_clicked_cb (GtkWidget *widget, PropertyInfo *pi)
{
	if (pi->type == PT_AUTOAPPLY)
	{
		gul_pu_set_config_from_togglebutton (widget, pi->pref);
	}

	prefs_set_group_sensitivity (widget, pi->type, pi->sg);
}

static void 
prefs_radiobutton_clicked_cb (GtkWidget *widget, PropertyInfo *pi)
{
	if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(widget)))
	{
		return;
	}

	if (pi->type == PT_AUTOAPPLY)
	{
		gul_pu_set_config_from_radiobuttongroup (widget, pi->pref);
	}

	prefs_set_group_sensitivity (widget, pi->type, pi->sg);
}

static gint 
prefs_spinbutton_timeout_cb (GaleonDialog *dialog)
{
	PropertyInfo pi = dialog->priv->props[dialog->priv->spin_item_id];
	
        /* timer still valid? */
        if (dialog->priv->spin_timer == NULL)
        {
                return FALSE;
        }

        /* okay, we're ready to set */
        if (g_timer_elapsed (dialog->priv->spin_timer, NULL) >= SPIN_DELAY)
        {
                /* kill off the timer */
                g_timer_destroy (dialog->priv->spin_timer);
                dialog->priv->spin_timer = NULL;
		
		/* HACK update the spinbutton here so that the
		 * changes made directly in the entry are accepted
		 * and set in the pref. Otherwise the old value is used */
		gtk_spin_button_update (GTK_SPIN_BUTTON(pi.widget));
       
		/* set */
                gul_pu_set_config_from_spin_button (pi.widget, 
						    pi.pref);

                /* done now */
                return FALSE;
        }

        /* call me again */
        return TRUE;
}

static void 
prefs_spinbutton_changed_cb (GtkWidget *widget, PropertyInfo *pi)
{
	GaleonDialog *dialog;

	if (pi->type != PT_AUTOAPPLY) return;

	dialog = GALEON_DIALOG (g_object_get_data 
				(G_OBJECT(widget), "dialog"));

	dialog->priv->spin_item_id = pi->id;

	/* destroy any existing timer */
	if (dialog->priv->spin_timer != NULL)
	{
		g_timer_destroy (dialog->priv->spin_timer);
	}

	/* start the new one */
	dialog->priv->spin_timer = g_timer_new();
	g_timer_start (dialog->priv->spin_timer);
	g_timeout_add (50, (GSourceFunc) prefs_spinbutton_timeout_cb,
		       dialog);
}

static void 
prefs_entry_changed_cb (GtkWidget *widget, PropertyInfo *pi) 
{
	if (pi->type == PT_AUTOAPPLY)
	{
		gul_pu_set_config_from_editable (widget,  pi->pref);
	}
}

static void 
prefs_combobox_selected_cb (GtkWidget *widget, PropertyInfo *pi)  
{
	if (pi->type == PT_AUTOAPPLY)
	{
		gul_pu_set_config_from_combobox (widget, pi->pref);
	}
}

static void
prefs_connect_signals (GaleonDialog *dialog)
{
	int i;
	const GSList *list;
	PropertyInfo *props = dialog->priv->props;
	
	for (i = 0; props[i].widget != NULL; i++)
	{
		PrefType type;
		PropertyInfo *info;

		if ((props[i].type != PT_AUTOAPPLY) &&
		    (props[i].sg == NULL))
			continue;
		
		info = &dialog->priv->props[i];
		type = get_pref_type_from_widget 
			(dialog->priv->props[i].widget);

		switch (type)
		{
		case PT_TOGGLEBUTTON:
			g_object_set_data (G_OBJECT(info->widget), "dialog", dialog);
			g_signal_connect (G_OBJECT (info->widget), "clicked", 
					  G_CALLBACK(prefs_togglebutton_clicked_cb), 
					  (gpointer)info);
			break;
		case PT_RADIOBUTTON:
			list = gtk_radio_button_get_group 
				(GTK_RADIO_BUTTON(info->widget));
			for (; list != NULL; list = list->next)
			{
				g_object_set_data (G_OBJECT(list->data), 
						   "dialog", dialog);
				g_signal_connect 
					(G_OBJECT (list->data), "clicked", 
					 G_CALLBACK(prefs_radiobutton_clicked_cb), 
					 (gpointer)info);
			}
			break;
		case PT_SPINBUTTON:
			g_object_set_data (G_OBJECT(info->widget), "dialog", dialog);
			g_signal_connect (G_OBJECT (info->widget), "changed", 
					  G_CALLBACK(prefs_spinbutton_changed_cb), 
					  (gpointer)info);
			break;
		case PT_COMBOBOX:
			g_signal_connect (G_OBJECT (info->widget), 
					  "changed", 
					  G_CALLBACK(prefs_combobox_selected_cb), 
					  (gpointer)info);
			break;
		case PT_ENTRY:
			g_signal_connect (G_OBJECT (info->widget), "changed", 
					  G_CALLBACK(prefs_entry_changed_cb), 
					  (gpointer)info);
			break;

		case PT_EXPANDER:
		case PT_UNKNOWN:
			break;
		}
	}
}

static void
galeon_dialog_init (GaleonDialog *dialog)
{
        dialog->priv = GALEON_DIALOG_GET_PRIVATE (dialog);

	dialog->priv->parent = NULL;
	dialog->priv->dialog = NULL;
	dialog->priv->props = NULL;
	dialog->priv->spin_timer = NULL;
	dialog->priv->name = NULL;
	dialog->priv->initialized = FALSE;
}

static void
prefs_set_sensitivity (PropertyInfo *props)
{
	int i;

	for (i=0 ; props[i].id >= 0; i++)
	{
		if (props[i].sg == NULL) continue;
	
		g_return_if_fail (props[i].widget != NULL);

		if (GTK_IS_RADIO_BUTTON(props[i].widget) ||
		    GTK_IS_TOGGLE_BUTTON(props[i].widget))
		{
			prefs_set_group_sensitivity (props[i].widget, 
						     props[i].type, 
						     props[i].sg);
		}
	}
}

static void
load_props (PropertyInfo *props)
{
	int i;

	for (i=0 ; props[i].id >= 0; i++)
	{
		if (props[i].pref == NULL) continue;
	
		g_return_if_fail (props[i].widget != NULL);

		if (GTK_IS_SPIN_BUTTON(props[i].widget))
		{
			gul_pu_set_spin_button_from_config (props[i].widget,
				 	     	            props[i].pref);
		}
		else if (GTK_IS_RADIO_BUTTON(props[i].widget))
		{
			gul_pu_set_radiobuttongroup_from_config (props[i].widget,
					     	                 props[i].pref);
		}
		else if (GTK_IS_TOGGLE_BUTTON(props[i].widget))
		{
			 gul_pu_set_togglebutton_from_config (props[i].widget,
							      props[i].pref);
		}
		else if (GTK_IS_EXPANDER(props[i].widget))
		{
			 gul_pu_set_expander_from_config (props[i].widget,
							  props[i].pref);
		}
		else if (GTK_IS_EDITABLE(props[i].widget))
		{
			gul_pu_set_editable_from_config (props[i].widget,
						         props[i].pref);
		}
		else if (GTK_IS_COMBO_BOX(props[i].widget))
		{
			gul_pu_set_combobox_from_config (props[i].widget,
							 props[i].pref);
		}
	}
}

static void
save_props (PropertyInfo *props)
{
	int i;
	
	for (i=0 ; props[i].id >= 0; i++)
	{
		if ((props[i].pref == NULL) ||
		    (props[i].type != PT_NORMAL)) continue;
		g_return_if_fail (props[i].widget != NULL);

		if (GTK_IS_SPIN_BUTTON(props[i].widget))
		{
			gul_pu_set_config_from_spin_button (props[i].widget,
					   	            props[i].pref);
		}
		else if (GTK_IS_RADIO_BUTTON(props[i].widget))
		{
			gul_pu_set_config_from_radiobuttongroup (props[i].widget,
					   	                 props[i].pref);
		}
		else if (GTK_IS_TOGGLE_BUTTON(props[i].widget))
		{
			 gul_pu_set_config_from_togglebutton (props[i].widget,
							      props[i].pref);
		}
		else if (GTK_IS_EXPANDER(props[i].widget))
		{
			 gul_pu_set_config_from_expander (props[i].widget,
							  props[i].pref);
		}
		else if (GTK_IS_EDITABLE(props[i].widget))
		{
			gul_pu_set_config_from_editable (props[i].widget,
						         props[i].pref);
		}
		else if (GTK_IS_COMBO_BOX(props[i].widget))
		{
			gul_pu_set_config_from_combobox (props[i].widget,
					  	         props[i].pref);
		}
	}
}

static void
galeon_dialog_finalize (GObject *object)
{
        GaleonDialog *dialog;

        g_return_if_fail (object != NULL);
        g_return_if_fail (GALEON_IS_DIALOG (object));

        dialog = GALEON_DIALOG (object);

        g_return_if_fail (dialog->priv != NULL);

	if (dialog->priv->dialog)
	{
		galeon_dialog_destruct (dialog);
	}
	
	g_free (dialog->priv->name);
	g_free (dialog->priv->props);

        G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
galeon_dialog_set_property (GObject *object,
                            guint prop_id,
                            const GValue *value,
                            GParamSpec *pspec)
{
        GaleonDialog *d = GALEON_DIALOG (object);

        switch (prop_id)
        {
                case PROP_PARENT_WINDOW:
                        galeon_dialog_set_parent (d, g_value_get_object (value));
                        break;
		case PROP_MODAL:
			galeon_dialog_set_modal (d, g_value_get_boolean (value));
			break;
        }
}

static void
galeon_dialog_get_property (GObject *object,
             	            guint prop_id,
                            GValue *value,
                            GParamSpec *pspec)
{
        GaleonDialog *d = GALEON_DIALOG (object);

        switch (prop_id)
        {
                case PROP_PARENT_WINDOW:
                        g_value_set_object (value, d->priv->parent);
                        break;
		case PROP_MODAL:
			g_value_set_boolean (value, d->priv->modal);
        }
}

static void
galeon_dialog_set_parent (GaleonDialog *dialog, 
			  GtkWidget *parent)
{
	g_return_if_fail (dialog->priv->parent == NULL);
        g_return_if_fail (GTK_IS_WINDOW(parent));
	g_return_if_fail (GTK_IS_WINDOW(dialog->priv->dialog));
	
        dialog->priv->parent = parent;

	gtk_window_set_transient_for (GTK_WINDOW (dialog->priv->dialog),
                                      GTK_WINDOW (parent));
}

GaleonDialog *
galeon_dialog_new (void)
{
	return GALEON_DIALOG (g_object_new (GALEON_TYPE_DIALOG, NULL));
}

GaleonDialog *
galeon_dialog_new_with_parent (GtkWidget *parent_window)
{
	return GALEON_DIALOG (g_object_new (GALEON_TYPE_DIALOG,
					    "ParentWindow", parent_window,
					    NULL));
}

static void
check_connect_props (GaleonDialog *dialog)
{
	if (dialog->priv->props && !dialog->priv->initialized)
	{
		load_props (dialog->priv->props);
		prefs_connect_signals (dialog);
		prefs_set_sensitivity (dialog->priv->props);
		dialog->priv->initialized = TRUE;
	}
}

void galeon_dialog_show_embedded (GaleonDialog *dialog,
				  GtkWidget *container)
{
	check_connect_props (dialog);

	dialog->priv->container = container;
	gtk_container_add (GTK_CONTAINER(container),
			   dialog->priv->dialog);
	gtk_widget_show (dialog->priv->dialog);
}

void
galeon_dialog_remove_embedded (GaleonDialog *dialog)
{
	g_object_ref (G_OBJECT(dialog->priv->dialog));
	gtk_container_remove (GTK_CONTAINER(dialog->priv->container),
			      dialog->priv->dialog);
}

static PropertyInfo *
init_props (const GaleonDialogProperty *properties, GladeXML *gxml)
{
	PropertyInfo *props;
	int i;
	
	for (i=0 ; properties[i].control_name != NULL; i++);
	
	props = g_new0 (PropertyInfo, i+1);
	
	for (i=0 ; properties[i].control_name != NULL; i++)
	{
		props[i].id = properties[i].id;
		props[i].widget = glade_xml_get_widget 
					(gxml, properties[i].control_name);
		props[i].pref = properties[i].state_pref;
		props[i].sg = properties[i].sg;
		props[i].type = properties[i].type;
	}

	props[i].id = -1;
	props[i].widget = NULL;
	props[i].pref = NULL;
	props[i].sg = NULL;
	props[i].type = 0;

	return props;
}

static void
dialog_destruction_notify (GaleonDialog *dialog,
                           GObject *where_the_object_was)
{
	dialog->priv->dialog = NULL;
	galeon_dialog_destruct (dialog);
	g_object_unref (dialog);
}

static void
dialog_destroy_cb (GtkWidget *widget, GaleonDialog *dialog)
{
	if (dialog->priv->props && 
	    dialog->priv->dialog)
	{
		save_props (dialog->priv->props);
	}
	dialog->priv->initialized = FALSE;
}

static void 
impl_construct (GaleonDialog *dialog,
		const GaleonDialogProperty *properties,
		const char *file,
		const char *name)
{
	GladeXML *gxml;

	gxml = gul_glade_widget_new 
		(file, name, &(dialog->priv->dialog), dialog);

	if (dialog->priv->name == NULL)
	{
		dialog->priv->name = g_strdup (name);
	}
	
	if (properties)
	{
		dialog->priv->props = init_props (properties, gxml);
	}

	g_signal_connect (dialog->priv->dialog,
			  "destroy",
			  G_CALLBACK(dialog_destroy_cb),
			  dialog);

	g_object_unref (gxml);

	g_object_weak_ref (G_OBJECT(dialog->priv->dialog),
			   (GWeakNotify)dialog_destruction_notify,
			   dialog);

}

static void 
impl_destruct (GaleonDialog *dialog)
{
	if (dialog->priv->dialog)
	{
		g_object_weak_unref (G_OBJECT(dialog->priv->dialog),
			   	     (GWeakNotify)dialog_destruction_notify,
			   	     dialog);
		gtk_widget_destroy (dialog->priv->dialog);
		dialog->priv->dialog = NULL;
	}
}

static GtkWidget *
impl_get_control (GaleonDialog *dialog,
                  int property_id)
{
	return dialog->priv->props[property_id].widget;
}

static void 
impl_get_value (GaleonDialog *dialog,
                int property_id,
                GValue *value)
{
	GtkWidget *widget = galeon_dialog_get_control (dialog,
						       property_id);

	if (GTK_IS_SPIN_BUTTON (widget))
	{
		float val;
		g_value_init (value, G_TYPE_FLOAT);
		val = gtk_spin_button_get_value (GTK_SPIN_BUTTON(widget));
		g_value_set_float (value, val);
	}
	else if (GTK_IS_RADIO_BUTTON (widget))
	{
		int val;
		g_value_init (value, G_TYPE_INT);
		val = gul_gui_gtk_radio_button_get (GTK_RADIO_BUTTON(widget));
		g_value_set_int (value, val);
	}
	else if (GTK_IS_TOGGLE_BUTTON (widget))
	{
		g_value_init (value, G_TYPE_BOOLEAN);
		g_value_set_boolean (value, gtk_toggle_button_get_active
	  	                             (GTK_TOGGLE_BUTTON(widget)));
	}
	else if (GTK_IS_EDITABLE (widget))
	{
		gchar *text = gtk_editable_get_chars (GTK_EDITABLE (widget), 0, -1);
		g_value_init (value, G_TYPE_STRING);
		g_value_set_string (value, text);
		g_free (text);
	}
        else if (GTK_IS_COMBO_BOX(widget))
	{
		int val;
		g_value_init(value, G_TYPE_INT);
		val = gtk_combo_box_get_active(GTK_COMBO_BOX(widget));
		g_value_set_int(value, val);
	}
}

static gint
impl_run (GaleonDialog *dialog)
{	
	galeon_dialog_show (dialog);

	return gtk_dialog_run (GTK_DIALOG(dialog->priv->dialog));
}

static void
impl_show (GaleonDialog *dialog)
{
	check_connect_props (dialog);

	if (dialog->priv->parent)
	{
		/* Make the dialog transient again, because it seems to get
		 * forgotten after gtk_widget_hide.  Stray dialogs aren't of
		 * much use (and we may also risk a crash) so let's make sure
		 * our dialog gets destroyed along with its parent.
		 */
		gtk_window_set_transient_for (GTK_WINDOW (dialog->priv->dialog),
					      GTK_WINDOW (dialog->priv->parent));
		gtk_window_set_destroy_with_parent 
			(GTK_WINDOW(dialog->priv->dialog), TRUE);
	}
	gtk_window_present (GTK_WINDOW(dialog->priv->dialog));
}

void
galeon_dialog_set_modal (GaleonDialog *dialog,
			 gboolean is_modal)
{
	dialog->priv->modal = is_modal;

	gtk_window_set_modal (GTK_WINDOW(dialog->priv->dialog), 
			      is_modal);
}

void 
galeon_dialog_construct (GaleonDialog *dialog,
			 const GaleonDialogProperty *properties,
			 const char *file,
			 const char *name)
{
	GaleonDialogClass *klass = GALEON_DIALOG_GET_CLASS (dialog);
        klass->construct (dialog, properties, file, name);
}

void 
galeon_dialog_destruct (GaleonDialog *dialog)
{
	GaleonDialogClass *klass = GALEON_DIALOG_GET_CLASS (dialog);
        klass->destruct (dialog);
}

gint 
galeon_dialog_run (GaleonDialog *dialog)
{
	GaleonDialogClass *klass = GALEON_DIALOG_GET_CLASS (dialog);
        return klass->run (dialog);
}

void
galeon_dialog_show (GaleonDialog *dialog)
{
	GaleonDialogClass *klass = GALEON_DIALOG_GET_CLASS (dialog);
        klass->show (dialog);
}

GtkWidget *
galeon_dialog_get_control (GaleonDialog *dialog,
			   int property_id)
{
	GaleonDialogClass *klass = GALEON_DIALOG_GET_CLASS (dialog);
        return klass->get_control (dialog, property_id);
}

void  
galeon_dialog_get_value (GaleonDialog *dialog,
		 	 int property_id,
			 GValue *value)
{
	GaleonDialogClass *klass = GALEON_DIALOG_GET_CLASS (dialog);
        klass->get_value (dialog, property_id, value);
}

