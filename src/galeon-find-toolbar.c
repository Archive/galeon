/*
 *  Copyright (C) 2004 Tommi Komulainen
 *  Copyright (C) 2004, 2005 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "config.h"

#include "galeon-find-toolbar.h"
#include "galeon-embed-find.h"
#include "galeon-debug.h"

#include <gdk/gdkkeysyms.h>
#include <glib/gi18n.h>
#include <gtk/gtkarrow.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtkimage.h>
#include <gtk/gtkentry.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkseparatortoolitem.h>
#include <gtk/gtkstock.h>
#include <gtk/gtktoolbutton.h>
#include <gtk/gtkalignment.h>
#include <gtk/gtkmain.h>
#include <gtk/gtkversion.h>
#include <string.h>

#define GALEON_FIND_TOOLBAR_GET_PRIVATE(object) (G_TYPE_INSTANCE_GET_PRIVATE ((object),GALEON_TYPE_FIND_TOOLBAR, GaleonFindToolbarPrivate))

struct _GaleonFindToolbarPrivate
{
	GaleonEmbedFind *find;
	GaleonWindow *window;
	GaleonEmbed *embed;
	GtkWidget *entry;
	GtkToolItem *next;
	GtkToolItem *prev;
	GtkToolItem *sep;
	GtkToolItem *status_item;
	GtkWidget *status_label;
	gulong set_focus_handler;
	guint preedit_changed : 1;
	guint prevent_activate : 1;
	guint activated : 1;
	guint links_only : 1;
	guint typing_ahead : 1;
};

enum
{
	PROP_0,
	PROP_WINDOW
};

enum
{
	NEXT,
	PREVIOUS,
	CLOSE,
	LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

static GObjectClass *parent_class = NULL;

/* private functions */

static GaleonEmbedFind *
get_find (GaleonFindToolbar *toolbar)
{
	GaleonFindToolbarPrivate *priv = toolbar->priv;

	if (priv->find == NULL)
	{
		LOG ("Creating the finder now");

		priv->find = galeon_embed_find_new ();

		g_return_val_if_fail (priv->embed == NULL || gtk_widget_get_realized (GTK_WIDGET (priv->embed)), priv->find);

		galeon_embed_find_set_embed (priv->find, priv->embed);
	}

	return priv->find;
}

static void
set_controls (GaleonFindToolbar *toolbar,
	      gboolean can_find_next,
	      gboolean can_find_prev)
{
	GaleonFindToolbarPrivate *priv = toolbar->priv;

	gtk_widget_set_sensitive (GTK_WIDGET (priv->next), can_find_next);
	gtk_widget_set_sensitive (GTK_WIDGET (priv->prev), can_find_prev);
}

static void
set_status (GaleonFindToolbar *toolbar,
	    GaleonEmbedFindResult result)
{
	GaleonFindToolbarPrivate *priv = toolbar->priv;
	char *text = NULL;

	switch (result)
	{
		case GALEON_EMBED_FIND_FOUND:
			text = NULL;
			break;
		case GALEON_EMBED_FIND_NOTFOUND:
			text = _("Not found");
			break;
		case GALEON_EMBED_FIND_FOUNDWRAPPED:
			text = _("Wrapped");
			break;
	}

	gtk_label_set_text (GTK_LABEL (priv->status_label),
			    text != NULL ? text : "");

	g_object_set (priv->sep, "visible", text != NULL, NULL);
	g_object_set (priv->status_item, "visible", text != NULL, NULL);
}

static void
clear_status (GaleonFindToolbar *toolbar)
{
	GaleonFindToolbarPrivate *priv = toolbar->priv;

	gtk_widget_hide (GTK_WIDGET (priv->sep));
	gtk_widget_hide (GTK_WIDGET (priv->status_item));
	gtk_label_set_text (GTK_LABEL (priv->status_label), "");
}

static void
tab_content_changed_cb (GaleonEmbed *embed,
			const char *uri,
			GaleonFindToolbar *toolbar)
{
	set_controls (toolbar, TRUE, TRUE);
}


/* Code adapted from gtktreeview.c:gtk_tree_view_key_press() and
 * gtk_tree_view_real_start_interactive_seach()
 */
static gboolean
tab_search_key_press_cb (GaleonEmbed *embed,
			 GdkEventKey *event,
			 GaleonFindToolbar *toolbar)
{
	GaleonFindToolbarPrivate *priv = toolbar->priv;
	GtkWidget *widget = (GtkWidget *) toolbar;

	g_return_val_if_fail (event != NULL, FALSE);

	/* don't do anything in PPV mode */
	if (galeon_window_get_chrome(priv->window) & EMBED_CHROME_PPVIEWTOOLBARON)
	{
		return FALSE;
	}

	/* check for / and ' which open the find toolbar in text resp. link mode */
	if (gtk_widget_get_visible (widget) == FALSE)
	{
		gboolean autostart, linksonly;
		galeon_embed_find_get_prefs (get_find (toolbar), 
					     &autostart, &linksonly);

		if (event->keyval == GDK_slash)
		{
			galeon_find_toolbar_open (toolbar, FALSE, TRUE);
			return TRUE;
		}
		else if (event->keyval == GDK_apostrophe)
		{
			galeon_find_toolbar_open (toolbar, TRUE, TRUE);
			return TRUE;
		}
		else if (autostart &&
			 event->keyval != GDK_space && 
			 event->keyval != GDK_KP_Enter &&
			 event->keyval != GDK_ISO_Enter &&
			 event->keyval != GDK_Return &&
			 event->length)
		{
			GdkEvent *new_event;
			GdkWindow *old_window;
			galeon_find_toolbar_open (toolbar, linksonly, TRUE);

			/* Dispatch this event to the GtkEntry */
			new_event = gdk_event_copy ((GdkEvent *) event);
			old_window = ((GdkEventKey *) new_event)->window;
			((GdkEventKey *) new_event)->window = priv->entry->window;
			gtk_widget_event (priv->entry, (GdkEvent*) event);
			((GdkEventKey *) new_event)->window = old_window;
			gdk_event_free (new_event);
			return TRUE;
		}
	}

	return FALSE;
}

static void
find_next_cb (GaleonFindToolbar *toolbar)
{
	g_signal_emit (toolbar, signals[NEXT], 0);
}

static void
find_prev_cb (GaleonFindToolbar *toolbar)
{
	g_signal_emit (toolbar, signals[PREVIOUS], 0);
}

static void
entry_changed_cb (GtkEntry *entry,
		  GaleonFindToolbar *toolbar)
{
	GaleonFindToolbarPrivate *priv = toolbar->priv;
	const char *text;
	char *lowercase;
	GaleonEmbedFindResult result;
	gboolean found = TRUE, case_sensitive;

	text = gtk_entry_get_text (GTK_ENTRY (priv->entry));

	/* Search case-sensitively iff the string includes 
	 * non-lowercase character.
	 */
	lowercase = g_utf8_strdown (text, -1);
	case_sensitive = g_utf8_collate (text, lowercase) != 0;
	g_free (lowercase);

	galeon_embed_find_set_properties (get_find (toolbar), text, case_sensitive);
	result = galeon_embed_find_find (get_find (toolbar), text, priv->links_only);

	found = result != GALEON_EMBED_FIND_NOTFOUND;
	set_status (toolbar, result);
	set_controls (toolbar, found, found);
}

static gboolean
entry_key_press_event_cb (GtkEntry *entry,
			  GdkEventKey *event,
			  GaleonFindToolbar *toolbar)
{
	GaleonFindToolbarPrivate *priv = toolbar->priv;
	guint mask = gtk_accelerator_get_default_mod_mask ();
	gboolean handled = FALSE;

	if ((event->state & mask) == 0)
	{
		handled = TRUE;
		switch (event->keyval)
		{
		case GDK_Up:
		case GDK_KP_Up:
			galeon_embed_scroll (priv->embed, EMBED_SCROLL_UP);
			break;
		case GDK_Down:
		case GDK_KP_Down:
			galeon_embed_scroll (priv->embed, EMBED_SCROLL_DOWN);
			break;
		case GDK_Page_Up:
		case GDK_KP_Page_Up:
			galeon_embed_scroll_page (priv->embed, EMBED_SCROLL_UP);
			break;
		case GDK_Page_Down:
		case GDK_KP_Page_Down:
			galeon_embed_scroll_page (priv->embed, EMBED_SCROLL_DOWN);
			break;
		case GDK_Escape:
			/* Hide the toolbar when ESC is pressed */
			galeon_find_toolbar_request_close (toolbar);
			break;
		default:
			handled = FALSE;
			break;
		}
	}
	else if ((event->state & mask) == GDK_CONTROL_MASK &&
		 (event->keyval == GDK_Return || event->keyval == GDK_KP_Enter))
	{
		handled = galeon_embed_find_activate_link (get_find (toolbar), event->state);
	}

	return handled;
}

static void
entry_activate_cb (GtkWidget *entry,
		   GaleonFindToolbar *toolbar)
{
	GaleonFindToolbarPrivate *priv = toolbar->priv;

	if (priv->typing_ahead)
	{
		galeon_embed_find_activate_link (get_find (toolbar), 0);
	}
	else
	{
		g_signal_emit (toolbar, signals[NEXT], 0);
	}
}

static void
set_focus_cb (GaleonWindow *window,
	      GtkWidget *widget,
	      GaleonFindToolbar *toolbar)
{
	GtkWidget *wtoolbar = GTK_WIDGET (toolbar);

	while (widget != NULL && widget != wtoolbar)
	{
		widget = widget->parent;
	}

	/* if widget == toolbar, the new focus widget is in the toolbar */
	if (widget != wtoolbar)
	{
		galeon_find_toolbar_request_close (toolbar);
	}
}

static void
galeon_find_toolbar_set_window (GaleonFindToolbar *toolbar,
			      GaleonWindow *window)
{
	GaleonFindToolbarPrivate *priv = toolbar->priv;

	priv->window = window;
}

static void
galeon_find_toolbar_parent_set (GtkWidget *widget,
			      GtkWidget *previous_parent)
{
	GaleonFindToolbar *toolbar = GALEON_FIND_TOOLBAR (widget);
	GaleonFindToolbarPrivate *priv = toolbar->priv;
	GtkWidget *toplevel;

	if (widget->parent != NULL && priv->set_focus_handler == 0)
	{
		toplevel = gtk_widget_get_toplevel (widget);
		priv->set_focus_handler =
			g_signal_connect (toplevel, "set-focus",
					  G_CALLBACK (set_focus_cb), toolbar);
	}
}

static void
galeon_find_toolbar_grab_focus (GtkWidget *widget)
{
	GaleonFindToolbar *toolbar = GALEON_FIND_TOOLBAR (widget);
	GaleonFindToolbarPrivate *priv = toolbar->priv;

	gtk_widget_grab_focus (GTK_WIDGET (priv->entry));
}

static void
galeon_find_toolbar_init (GaleonFindToolbar *toolbar)
{
	GaleonFindToolbarPrivate *priv;
	GtkToolbar *gtoolbar;
	GtkToolItem *item;
	GtkWidget *alignment, *arrow, *box, *label;

	priv = toolbar->priv = GALEON_FIND_TOOLBAR_GET_PRIVATE (toolbar);
	gtoolbar = GTK_TOOLBAR (toolbar);

	gtk_toolbar_set_style (gtoolbar, GTK_TOOLBAR_BOTH_HORIZ);

	/* Find: |_____| */
	alignment = gtk_alignment_new (0.0, 0.5, 1.0, 0.0);
	gtk_alignment_set_padding (GTK_ALIGNMENT (alignment), 0, 0, 2, 2);

	box = gtk_hbox_new (FALSE, 12);
	gtk_container_add (GTK_CONTAINER (alignment), box);

	label = gtk_label_new (NULL);
	gtk_label_set_markup (GTK_LABEL (label), _("Find:"));
	gtk_box_pack_start (GTK_BOX (box), label, FALSE, FALSE, 0);

	priv->entry = gtk_entry_new ();
	gtk_entry_set_width_chars (GTK_ENTRY (priv->entry), 32);
	gtk_entry_set_max_length (GTK_ENTRY (priv->entry), 512);
	gtk_box_pack_start (GTK_BOX (box), priv->entry, TRUE, TRUE, 0);

	item = gtk_tool_item_new ();
	gtk_container_add (GTK_CONTAINER (item), alignment);
	//gtk_tool_item_set_expand (item, TRUE);
	gtk_toolbar_insert (GTK_TOOLBAR (toolbar), item, -1);
	gtk_widget_show_all (GTK_WIDGET (item));

	/* Prev */
	arrow = gtk_arrow_new (GTK_ARROW_LEFT, GTK_SHADOW_NONE);
	label = gtk_label_new (_("Find Previous"));
	priv->prev = gtk_tool_button_new (arrow, _("Find Previous"));
	gtk_tool_item_set_is_important (priv->prev, TRUE);
	gtk_tool_item_set_tooltip_text (priv->prev,
                                        _("Find previous occurrence of the search string"));
	gtk_toolbar_insert (GTK_TOOLBAR (toolbar), priv->prev, -1);
	gtk_widget_show_all (GTK_WIDGET (priv->prev));

	/* Next */
	arrow = gtk_arrow_new (GTK_ARROW_RIGHT, GTK_SHADOW_NONE);
	label = gtk_label_new (_("Find Next"));
	priv->next = gtk_tool_button_new (arrow, _("Find Next"));
	gtk_tool_item_set_is_important (priv->next, TRUE);
	gtk_tool_item_set_tooltip_text (priv->next,
                                        _("Find next occurrence of the search string"));
	gtk_toolbar_insert (GTK_TOOLBAR (toolbar), priv->next, -1);
	gtk_widget_show_all (GTK_WIDGET (priv->next));

	priv->sep = gtk_separator_tool_item_new ();
	gtk_toolbar_insert (GTK_TOOLBAR (toolbar), priv->sep, -1);
	
	priv->status_item = gtk_tool_item_new ();
	gtk_tool_item_set_expand (priv->status_item, TRUE);
	priv->status_label = gtk_label_new ("");
	gtk_misc_set_alignment (GTK_MISC (priv->status_label), 0.0, 0.5);
#if GTK_CHECK_VERSION(2,6,0)
	gtk_label_set_ellipsize (GTK_LABEL (priv->status_label), PANGO_ELLIPSIZE_END);
#endif
	gtk_container_add (GTK_CONTAINER (priv->status_item), priv->status_label);
	gtk_widget_show (priv->status_label);
	gtk_toolbar_insert (GTK_TOOLBAR (toolbar), priv->status_item, -1);

	/* connect signals */
	g_signal_connect (priv->entry, "key-press-event",
			  G_CALLBACK (entry_key_press_event_cb), toolbar);
	g_signal_connect_after (priv->entry, "changed",
				G_CALLBACK (entry_changed_cb), toolbar);
	g_signal_connect (priv->entry, "activate",
			  G_CALLBACK (entry_activate_cb), toolbar);
	g_signal_connect_swapped (priv->next, "clicked",
				  G_CALLBACK (find_next_cb), toolbar);
	g_signal_connect_swapped (priv->prev, "clicked",
				  G_CALLBACK (find_prev_cb), toolbar);
}

static void
galeon_find_toolbar_finalize (GObject *object)
{
	GaleonFindToolbar *toolbar = GALEON_FIND_TOOLBAR (object);
	GaleonFindToolbarPrivate *priv = toolbar->priv;

	if (priv->find != NULL)
	{
		g_object_unref (priv->find);
	}

	parent_class->finalize (object);
}

static void
galeon_find_toolbar_get_property (GObject *object,
				guint prop_id,
				GValue *value,
				GParamSpec *pspec)
{
	/* no readable properties */
	g_assert_not_reached ();
}

static void
galeon_find_toolbar_set_property (GObject *object,
				guint prop_id,
				const GValue *value,
				GParamSpec *pspec)
{
	GaleonFindToolbar *toolbar = GALEON_FIND_TOOLBAR (object);

	switch (prop_id)
	{
		case PROP_WINDOW:
			galeon_find_toolbar_set_window (toolbar, (GaleonWindow *) g_value_get_object (value));
			break;
	}
}

static void
galeon_find_toolbar_class_init (GaleonFindToolbarClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
        GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

        parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = galeon_find_toolbar_finalize;
	object_class->get_property = galeon_find_toolbar_get_property;
	object_class->set_property = galeon_find_toolbar_set_property;

	widget_class->parent_set = galeon_find_toolbar_parent_set;
	widget_class->grab_focus = galeon_find_toolbar_grab_focus;

	klass->next = galeon_find_toolbar_find_next;
	klass->previous = galeon_find_toolbar_find_previous;

	signals[NEXT] =
		g_signal_new ("next",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (GaleonFindToolbarClass, next),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);

	signals[PREVIOUS] =
		g_signal_new ("previous",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (GaleonFindToolbarClass, previous),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);

	signals[CLOSE] =
		g_signal_new ("close",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (GaleonFindToolbarClass, close),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);

	g_object_class_install_property
		(object_class,
		 PROP_WINDOW,
		 g_param_spec_object ("window",
				      "Window",
				      "Parent window",
				      GALEON_TYPE_WINDOW,
				      (GParamFlags) (G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY)));

	g_type_class_add_private (klass, sizeof (GaleonFindToolbarPrivate));
}

/* public functions */

GType
galeon_find_toolbar_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0))
	{
		static const GTypeInfo our_info =
		{
			sizeof (GaleonFindToolbarClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) galeon_find_toolbar_class_init,
			NULL,
			NULL, /* class_data */
			sizeof (GaleonFindToolbar),
			0, /* n_preallocs */
			(GInstanceInitFunc) galeon_find_toolbar_init
		};
	
		type = g_type_register_static (GTK_TYPE_TOOLBAR,
					       "GaleonFindToolbar",
					       &our_info, 0);
	}

	return type;
}

GaleonFindToolbar *
galeon_find_toolbar_new (GaleonWindow *window)
{
	return GALEON_FIND_TOOLBAR (g_object_new (GALEON_TYPE_FIND_TOOLBAR,
				  		"window", window,
						NULL));
}

const char *
galeon_find_toolbar_get_text (GaleonFindToolbar *toolbar)
{
	GaleonFindToolbarPrivate *priv = toolbar->priv;

	return gtk_entry_get_text (GTK_ENTRY (priv->entry));
}

void
galeon_find_toolbar_set_embed (GaleonFindToolbar *toolbar,
			     GaleonEmbed *embed)
{
	GaleonFindToolbarPrivate *priv = toolbar->priv;

	if (priv->embed == embed) return;

	if (priv->embed != NULL)
	{
		g_signal_handlers_disconnect_matched (embed, G_SIGNAL_MATCH_DATA,
						      0, 0, NULL, NULL, toolbar);
	}

	priv->embed = embed;
	if (embed != NULL)
	{
		set_controls (toolbar, TRUE, TRUE);
		clear_status (toolbar);

		g_signal_connect_object (embed, "ge-content-change",
					 G_CALLBACK (tab_content_changed_cb),
					 toolbar, G_CONNECT_AFTER);
		g_signal_connect_object (embed, "ge-search-key-press",
					 G_CALLBACK (tab_search_key_press_cb),
					 toolbar, 0);

		if (priv->find != NULL)
		{
			g_return_if_fail (gtk_widget_get_realized (GTK_WIDGET (priv->embed)));

			galeon_embed_find_set_embed (priv->find, embed);
		}
	}
}

void
galeon_find_toolbar_find_next (GaleonFindToolbar *toolbar)
{
	GaleonFindToolbarPrivate *priv = toolbar->priv;
	GaleonEmbedFindResult result;
	gboolean found;

	result = galeon_embed_find_find_again (get_find (toolbar), TRUE, priv->links_only);

	found = result != GALEON_EMBED_FIND_NOTFOUND;
	set_controls (toolbar, found, found);
	set_status (toolbar, result);
}

void
galeon_find_toolbar_find_previous (GaleonFindToolbar *toolbar)
{
	GaleonFindToolbarPrivate *priv = toolbar->priv;
	GaleonEmbedFindResult result;
	gboolean found;

	result = galeon_embed_find_find_again (get_find (toolbar), FALSE, priv->links_only);
	found = result != GALEON_EMBED_FIND_NOTFOUND;
	set_controls (toolbar, found, found);
	set_status (toolbar, result);
}

void
galeon_find_toolbar_open (GaleonFindToolbar *toolbar,
			gboolean links_only,
			gboolean typing_ahead)
{
	GaleonFindToolbarPrivate *priv = toolbar->priv;
	gboolean clear_search = typing_ahead;

	g_return_if_fail (priv->embed != NULL);

	priv->typing_ahead = typing_ahead;
	priv->links_only = links_only;

	clear_status (toolbar);

	if (clear_search)
	{
		gtk_entry_set_text (GTK_ENTRY (priv->entry), "");
	}
	else
	{
		gtk_editable_select_region (GTK_EDITABLE (priv->entry), 0, -1);
	}

	gtk_widget_show (GTK_WIDGET (toolbar));

	gtk_widget_grab_focus (GTK_WIDGET (toolbar));
}

void
galeon_find_toolbar_close (GaleonFindToolbar *toolbar)
{
	GaleonFindToolbarPrivate *priv = toolbar->priv;

	gtk_widget_hide (GTK_WIDGET (toolbar));

	if (priv->embed == NULL || priv->find == NULL) return;
	galeon_embed_find_set_selection (get_find (toolbar), FALSE);
}

void
galeon_find_toolbar_request_close (GaleonFindToolbar *toolbar)
{
	if (gtk_widget_get_visible (GTK_WIDGET (toolbar)))
	{
		g_signal_emit (toolbar, signals[CLOSE], 0);
	}
}
