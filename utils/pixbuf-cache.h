/*
 *  Copyright (C) 2001-2002 Jorn Baayen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

G_BEGIN_DECLS

/* pixbuf-cache.c */
GdkPixbuf *gul_pixbuf_cache_get (const gchar *filename);
void gul_pixbuf_cache_reset (void);

/* stock icons */
void gul_pixbuf_cache_register_stocks (void);

#define GALEON_STOCK_FOLDER         "galeon-folder"
#define GALEON_STOCK_DEFAULT        "galeon-default"
#define GALEON_STOCK_SEPARATOR      "galeon-separator"
#define GALEON_STOCK_HISTORY        "galeon-history"
#define GALEON_STOCK_FILTER         "galeon-filter"
#define GALEON_STOCK_POPUP_BLOCKED  "galeon-popup-blocked"
#define GALEON_STOCK_SECURE         "galeon-secure"
#define GALEON_STOCK_INSECURE       "galeon-insecure"
#define GALEON_STOCK_DOWNLOAD       "galeon-download"
#define GALEON_STOCK_ENTRY          "galeon-entry"

#define STOCK_ZOOM	           "stock_zoom"
#define STOCK_NEW_TAB		   "stock_new-tab"
#define STOCK_FULLSCREEN	   "stock_fullscreen"
#define STOCK_VIEW_SOURCE	   "stock_view-html-source"
#define STOCK_SEND_MAIL		   "stock_mail-send"
#define STOCK_ADD_BOOKMARK	   "stock_add-bookmark"
#define STOCK_SPINNER_REST         "gnome-spinner-rest"
#define STOCK_SELECT_ALL           "stock_select-all"
#define STOCK_EDIT_BOOKMARK        "stock_edit-bookmark"
#define STOCK_CONNECT              "stock_connect"
#define STOCK_DISCONNECT           "stock_disconnect"
#define STOCK_LOCK_BROKEN	   "stock_lock-broken"

G_END_DECLS
