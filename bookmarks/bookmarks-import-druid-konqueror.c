/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n.h>
#include "bookmarks-import-druid-konqueror.h"
#include "gul-string.h"
#include "gul-general.h"

#include "xbel.h"


static GHashTable *strings = NULL;

/**
 * Private functions, only availble from this file
 */
static void		gb_import_druid_konqueror_finalize_impl		(GObject *o);
static void		gb_import_druid_konqueror_init_strings 		(void);
static const char *	gb_import_druid_konqueror_get_string_impl	(GbImportDruid *d, 
									 const char *string_id);
static GSList *		gb_import_druid_konqueror_get_locations_impl	(GbImportDruid *d);
static GbIO *		gb_import_druid_konqueror_get_io_impl		(GbImportDruid *d);


/**
 * ImportDruidKonqueror object
 */

G_DEFINE_TYPE (GbImportDruidKonqueror, gb_import_druid_konqueror, GB_TYPE_IMPORT_DRUID);

static void
gb_import_druid_konqueror_class_init (GbImportDruidKonquerorClass *klass)
{
	GB_IMPORT_DRUID_CLASS (klass)->get_locations = gb_import_druid_konqueror_get_locations_impl;
	GB_IMPORT_DRUID_CLASS (klass)->get_io = gb_import_druid_konqueror_get_io_impl;
	GB_IMPORT_DRUID_CLASS (klass)->get_string = gb_import_druid_konqueror_get_string_impl;
	G_OBJECT_CLASS (klass)->finalize = gb_import_druid_konqueror_finalize_impl;
}

static void 
gb_import_druid_konqueror_init (GbImportDruidKonqueror *e)
{
}

static void
gb_import_druid_konqueror_finalize_impl (GObject *o)
{
	G_OBJECT_CLASS (gb_import_druid_konqueror_parent_class)->finalize (o);
}

GbImportDruidKonqueror *
gb_import_druid_konqueror_new (void)
{
	GbImportDruidKonqueror *ret = g_object_new (GB_TYPE_IMPORT_DRUID_KONQUEROR, NULL);
	return ret;
}

static const char *
gb_import_druid_konqueror_get_string_impl (GbImportDruid *d, 
					 const char *string_id)
{
	gb_import_druid_konqueror_init_strings ();
	return _(g_hash_table_lookup (strings, string_id));
}

static void
gb_import_druid_konqueror_init_strings (void)
{
	if (!strings)
	{
		static struct 
		{
			const char *id;
			const char *text;
		} strs[] = {
			{ "window title", N_("XBEL (Galeon and Konqueror) Bookmarks Import Druid") },
			{ "page 1 title", N_("XBEL (Galeon and Konqueror) Bookmarks Import") },
			{NULL, NULL}
		};			
		int i;
		strings = g_hash_table_new (g_str_hash, g_str_equal);
		for (i = 0; strs[i].id != NULL; ++i)
		{
			g_hash_table_insert (strings, (gpointer) strs[i].id, (gpointer) strs[i].text);
		}
	}
}

static GSList *
gb_import_druid_konqueror_get_locations_impl (GbImportDruid *druid)
{
	GSList *l;
	GSList *ret = NULL;
	gchar *dir;
	gchar *fname;
	guint i;
	
	static const gchar *galeon_dirs[] = { ".galeon", ".galeon1" , ".galeon2" };
	static const gchar *kde_dirs[] = { ".kde", ".kde2", ".kde3", ".konqueror" };
	
	/* galeon locations */
	for (i = 0; i < G_N_ELEMENTS (galeon_dirs); ++i)
	{
		guint j;
		dir = g_build_filename (g_get_home_dir (), galeon_dirs[i], NULL);
		l = gul_find_file  (dir, "bookmarks.xbel", 5);
		ret = g_slist_concat (ret, l);
		for (j = 0; j < 10; ++j)
		{
			fname = g_strdup_printf ("bookmarks.xbel.%d", j);
			l = gul_find_file  (dir, fname, 5);
			ret = g_slist_concat (ret, l);
			g_free (fname);
		}
		g_free (dir);
	}
	
	fname = g_build_filename (SHARE_DIR, "default-bookmarks.xbel", NULL);
	ret = g_slist_append (ret, fname);
	
	/* konqueror locations */
	for (i = 0; i < G_N_ELEMENTS (kde_dirs); ++i)
	{
		dir = g_build_filename (g_get_home_dir (), kde_dirs[i], NULL);
		l = gul_find_file  (dir, "bookmarks.xbel", 5);
		ret = g_slist_concat (ret, l);
		l = gul_find_file (dir, "bookmarks.xml", 5);
		ret = g_slist_concat (ret, l);
		g_free (dir);
	}

	return ret;
}

static GbIO *
gb_import_druid_konqueror_get_io_impl (GbImportDruid *druid)
{
	GbXBEL *io = gb_xbel_new ();
	return GB_IO (io);
}

