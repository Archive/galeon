/*
 *  Copyright (C) 2003 Tommi Komulainen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */
#include "config.h"

#include "gul-state.h"

#include <gtk/gtkpaned.h>
#include <gtk/gtkwindow.h>
#include <gtk/gtkexpander.h>
#include <glib/gi18n.h>

#if GLIB_CHECK_VERSION(2,5,4)
# include <glib/gkeyfile.h>
# define USE_GKEYFILE
#else
# include <libgnome/gnome-config.h>
#endif

#include <string.h>
#include <stdio.h>

#define CONFIG_SYNC_INTERVAL (60 * 5 * 1000)

static guint timeout_id = 0;

#ifdef USE_GKEYFILE
static GKeyFile* state_file = NULL;
#endif
static char * filename = NULL;

static void
_gul_state_save_file (void)
{
#ifdef USE_GKEYFILE
	if (state_file)
	{
		FILE *file;
		char *data;

		if (!(file = fopen (filename, "w"))) 
		{
			g_warning (_("Could not open file: %s"), filename);
			return;
		}
		data = g_key_file_to_data (state_file, NULL, NULL);
		fputs (data, file);
		fclose (file);
		g_free (data);
	}
#else
	gnome_config_sync ();
#endif
}


static gboolean
config_periodic_sync_cb (gpointer dummy)
{
	_gul_state_save_file ();
	return TRUE;
}


/**
 * gul_state_init:
 *
 * Prepares gnome-config for saving state in a single file.  Everyone can then
 * just use relative keys ('section/key') when calling gnome_config_get_* and
 * gnome_config_set_* functions, without needing to worry about the location of
 * the file.
 */
void
gul_state_init (void)
{
#ifndef USE_GKEYFILE	
	char *config_prefix;
#endif

	/* Using ~/.galeon/state.ini to keep all files together. */
	filename = g_build_filename (g_get_home_dir(),
				     ".galeon", "state.ini", NULL);

#ifdef USE_GKEYFILE
	state_file = g_key_file_new();
	g_key_file_load_from_file (state_file, filename, G_KEY_FILE_NONE, NULL);

#else
	config_prefix = g_strconcat ("=", filename, "=/", NULL);

	gnome_config_push_prefix (config_prefix);
	g_free (config_prefix);
#endif

	timeout_id = g_timeout_add (CONFIG_SYNC_INTERVAL, 
				    config_periodic_sync_cb, 
				    NULL);
}

/**
 * gul_state_shutdown:
 *
 * Ensures that any state changes are synced to disk
 */
void
gul_state_shutdown (void)
{
	_gul_state_save_file ();
	if (timeout_id)
	{
		g_source_remove (timeout_id);
		timeout_id = 0;
	}
}


/**
 * gul_state_set_string:
 * @key: a gnome-config key
 * @val: the value
 * 
 * Set a string value in the config file
 */
void
gul_state_set_string (const char * key, const char * val)
{
#ifdef USE_GKEYFILE
	gchar * group, *slash;

	g_return_if_fail (key != NULL);
	g_return_if_fail (val != NULL);
	g_return_if_fail (strchr(key, '/') != NULL);
	g_return_if_fail (state_file);

	group = g_strdup (key);
	slash = strchr(group, '/');
	*slash = '\0';

	g_key_file_set_string (state_file, group, slash+1, val);

	g_free (group);
#else
	g_return_if_fail (key != NULL);
	g_return_if_fail (val != NULL);
	g_return_if_fail (strchr(key, '/') != NULL);

	gnome_config_set_string (key, val);
#endif
}

/**
 * gul_state_get_string:
 * @key: a gnome-config key
 * @default_val: the default val, or NULL
 *
 * Return a string, or if it doesn't exist the default
 */
char *
gul_state_get_string (const char * key, const char * default_val)
{
	char * path;
	char * ret;

	g_return_val_if_fail (key != NULL, NULL);
	g_return_val_if_fail (strchr(key, '/') != NULL, NULL);

#ifdef USE_GKEYFILE
	g_return_val_if_fail (state_file, NULL);

	path = g_strdup (key);
	ret = strchr(path, '/'); /* abuse ret */
	*ret = '\0';

	ret = g_key_file_get_string (state_file, path, ret+1, NULL);
	g_free (path);
	    
#else
	path = g_strdup_printf ("%s=", key);

	ret = gnome_config_get_string (path);
	
	g_free (path);
#endif
	if (!ret && default_val)
	{
		ret = g_strdup (default_val);
	}
	return ret;
}


/**
 * gul_state_set_boolean:
 * @key: a gnome-config key
 * @val: the value
 * 
 * Set a boolean value in the config file
 */
void
gul_state_set_boolean (const char * key, gboolean val)
{
#ifdef USE_GKEYFILE
	gchar * group, *slash;

	g_return_if_fail (key != NULL);
	g_return_if_fail (strchr(key, '/') != NULL);
	g_return_if_fail (state_file);

	group = g_strdup (key);
	slash = strchr(group, '/');
	*slash = '\0';

	g_key_file_set_boolean (state_file, group, slash+1, val);

	g_free (group);
#else
	g_return_if_fail (key != NULL);
	g_return_if_fail (strchr(key, '/') != NULL);

	gnome_config_set_bool (key, val);
#endif
}

/**
 * gul_state_get_boolean:
 * @key: a gnome-config key
 * @default_val: the default val
 *
 * Return a boolean, or if it doesn't exist the default
 */
gboolean
gul_state_get_boolean (const char * key, gboolean default_val)
{
	char * path;
	gboolean ret;
#ifdef USE_GKEYFILE
	GError *err = NULL;
	char * slash;
#endif

	g_return_val_if_fail (key != NULL, default_val);
	g_return_val_if_fail (strchr(key, '/') != NULL, default_val);

#ifdef USE_GKEYFILE
	g_return_val_if_fail (state_file, default_val);

	path = g_strdup (key);
	slash = strchr(path, '/');
	*slash = '\0';

	ret = g_key_file_get_boolean (state_file, path, slash+1, &err);
	g_free (path);

	if (err)
	{
		ret = default_val;
		g_error_free (err);
	}

#else
	path = g_strdup_printf ("%s=%s", key, default_val ? "true" : "false");
	ret = gnome_config_get_bool (path);
	g_free (path);
#endif

	return ret;
}



static void
gul_state_window_load (GtkWindow  *window,
		       const char *name,
		       int         default_width,
		       int         default_height)
{
	int      width, height;
	gboolean maximized;

#ifdef USE_GKEYFILE
	GError *error = NULL;

	width = g_key_file_get_integer (state_file, name, "width", &error);
	if (error)
	{
		width = default_width;
		g_error_free (error);
		error = NULL;
	}

	height = g_key_file_get_integer (state_file, name, "height", &error);
	if (error)
	{
		height = default_height;
		g_error_free (error);
		error = NULL;
	}

	maximized = g_key_file_get_boolean (state_file, name, "maximized", &error);
	if (error)
	{
		maximized = FALSE;
		g_error_free (error);
		error = NULL;
	}

#else
	GString *key;
	key = g_string_new ("");

	g_string_printf (key, "%s/width=%d", name, default_width);
	width = gnome_config_get_int (key->str);

	g_string_printf (key, "%s/height=%d", name, default_height);
	height = gnome_config_get_int (key->str);

	g_string_printf (key, "%s/maximized=false", name);
	maximized = gnome_config_get_bool (key->str);

	g_string_free (key, TRUE);
#endif

	if (width > 0 && height > 0)
	{
		gtk_window_set_default_size (window, width, height);
	}

	if (maximized)
	{
		gtk_window_maximize (window);
	}
}

static void
gul_state_window_save (GtkWindow  *window,
		       const char *name)
{
	GdkWindowState  state;
	gboolean        maximized;

#ifndef USE_GKEYFILE
	GString        *key;
	key = g_string_new ("");
#endif

	state = gdk_window_get_state (GTK_WIDGET(window)->window);
	maximized = (state & GDK_WINDOW_STATE_MAXIMIZED);

#ifdef USE_GKEYFILE
	g_key_file_set_boolean (state_file, name, "maximized", maximized);
#else

	g_string_printf (key, "%s/maximized", name);
	gnome_config_set_bool (key->str, maximized);
#endif

	if (!maximized)
	{
		int width, height;

		gtk_window_get_size (window, &width, &height);

#ifdef USE_GKEYFILE
		g_key_file_set_integer (state_file, name, "width", width);
		g_key_file_set_integer (state_file, name, "height", height);
#else
		g_string_printf (key, "%s/width", name);
		gnome_config_set_int (key->str, width);

		g_string_printf (key, "%s/height", name);
		gnome_config_set_int (key->str, height);
#endif
	}

#ifndef USE_GKEYFILE
	g_string_free (key, TRUE);
#endif

	/* don't sync after every mouse move */
}

static gboolean
on_configure_event (GtkWidget         *widget, 
		    GdkEventConfigure *event,
		    const char        *name)
{
	GdkWindowState state;

	state = gdk_window_get_state (widget->window);

	if (!(state & GDK_WINDOW_STATE_FULLSCREEN))
	{
		gul_state_window_save (GTK_WINDOW(widget), name);
	}

	return FALSE;
}

static gboolean
on_window_state_event (GtkWidget           *widget,
		       GdkEventWindowState *event,
		       const char          *name)
{
	if (!(event->new_window_state & GDK_WINDOW_STATE_FULLSCREEN))
	{
		gul_state_window_save (GTK_WINDOW(widget), name);
	}
	return FALSE;
}

/** gul_state_monitor_window:
 * @widget: a #GtkWindow
 * @name: name of the window
 * @default_width: default width for the window in pixels, or -1
 * @default_height: default height for the window in pixels, or -1
 *
 * Monitor the size and state changes of a #GtkWindow and save the current
 * values in the state under the section @name.  The keys used in the section
 * are "width", "height", and "maximized".
 *
 * The initial size and state for the @window is set from previous state, or if
 * state is not found @default_width and @default_height are used.
 */
void
gul_state_monitor_window (GtkWidget  *window, 
		          const char *name,
		          int         default_width,
			  int         default_height)
{
	g_return_if_fail (GTK_IS_WINDOW(window));
	g_return_if_fail (name != NULL);
	g_return_if_fail (strchr(name, '/') == NULL);
	g_return_if_fail (default_width > 0 || default_width == -1);
	g_return_if_fail (default_height > 0 || default_height == -1);

	gul_state_window_load (GTK_WINDOW(window), name,
			       default_width, default_height);

	g_signal_connect_data (G_OBJECT(window), "configure-event",
			       G_CALLBACK(on_configure_event),
			       g_strdup (name), (GClosureNotify)g_free, 0);
	g_signal_connect_data (G_OBJECT(window), "window-state-event",
			       G_CALLBACK(on_window_state_event),
			       g_strdup (name), (GClosureNotify)g_free, 0);
}

static void
on_size_allocate (GtkPaned      *paned,
		  GtkAllocation *allocation,
		  const char    *key)
{
	int size;
#ifdef USE_GKEYFILE
	char * group, *slash;
#endif
	size = gtk_paned_get_position (paned);

#ifdef USE_GKEYFILE
	group = g_strdup (key);
	slash = strchr (group, '/');
	*slash = '\0';

	g_key_file_set_integer (state_file, group, slash+1, size);
#else
	gnome_config_set_int (key, size);
#endif

	/* don't sync after every mouse move */
}

/** gul_state_monitor_paned:
 * @paned: a #GtkPaned
 * @key: a gnome-config key
 * @default_size: default size for the paned in pixels
 *
 * Monitor the size changes of a #GtkPaned and save current size in the state
 * using the key @key.  The initial size for the @paned is set from previous
 * state, or if state is not found @default_size is used.
 */
void
gul_state_monitor_paned (GtkWidget  *paned,
		         const char *key,
			 int         default_size)
{
	char *tmp;
	int   size;
#ifdef USE_GKEYFILE
	char * slash;
	GError *err = NULL;
#endif

	g_return_if_fail (GTK_IS_PANED(paned));
	g_return_if_fail (key != NULL);
	g_return_if_fail (strchr (key, '/') != NULL);
	g_return_if_fail (default_size > 0);

#ifdef USE_GKEYFILE
	tmp = g_strdup (key);
	slash = strchr (tmp, '/');
	*slash = '\0';

	size = g_key_file_get_integer (state_file, tmp, slash+1, &err);
	if (err)
	{
		size = default_size;
		g_error_free (err);
	}
#else
	tmp = g_strdup_printf ("%s=%d", key, default_size);
	size = gnome_config_get_int (tmp);
#endif
	g_free (tmp);

	if (size > 0)
	{
		gtk_paned_set_position (GTK_PANED(paned), size);
	}

	g_signal_connect_data (G_OBJECT(paned), "size-allocate",
			       G_CALLBACK(on_size_allocate),
			       g_strdup(key), (GClosureNotify)g_free, 
			       G_CONNECT_AFTER);
}


static void
on_expander_toggle (GtkExpander *expander, GParamSpec *pspec, char *key)
{
	gul_state_set_boolean (key, gtk_expander_get_expanded (expander));
}

/** gul_state_monitor_expander:
 *  @expander: a #GtkExpander
 *  @key: a gnome-config key
 *  @default_visibility: the default visiblity for the expander
 *
 *  Monitor the visibilty of an #GtkExpander and save the current
 *  visibility using the key @key.
 *
 */
void
gul_state_monitor_expander (GtkWidget *expander,
			    const char *key,
			    gboolean default_visibility)
{
	gboolean visible;

	g_return_if_fail (GTK_IS_EXPANDER (expander));
	g_return_if_fail (key != NULL);
	g_return_if_fail (strchr (key, '/') != NULL);

	visible = gul_state_get_boolean (key, default_visibility);

	gtk_expander_set_expanded (GTK_EXPANDER (expander), visible);

	g_signal_connect_data (G_OBJECT (expander),
			       "notify::expanded",
			       G_CALLBACK (on_expander_toggle),
			       g_strdup (key), (GClosureNotify)g_free, 
			       G_CONNECT_AFTER);
}
