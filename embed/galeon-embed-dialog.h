/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GALEON_EMBED_DIALOG_H
#define GALEON_EMBED_DIALOG_H

#include "galeon-embed.h"
#include "galeon-dialog.h"

#include <glib-object.h>
#include <glib.h>
#include <gtk/gtkwidget.h>
	
G_BEGIN_DECLS

typedef struct GaleonEmbedDialogClass GaleonEmbedDialogClass;

#define GALEON_TYPE_EMBED_DIALOG             (galeon_embed_dialog_get_type ())
#define GALEON_EMBED_DIALOG(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GALEON_TYPE_EMBED_DIALOG, GaleonEmbedDialog))
#define GALEON_EMBED_DIALOG_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GALEON_TYPE_EMBED_DIALOG, GaleonEmbedDialogClass))
#define GALEON_IS_EMBED_DIALOG(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GALEON_TYPE_EMBED_DIALOG))
#define GALEON_IS_EMBED_DIALOG_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GALEON_EMBED_DIALOG))
#define GALEON_EMBED_DIALOG_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GALEON_TYPE_EMBED_DIALOG, GaleonEmbedDialogClass))

typedef struct GaleonEmbedDialog GaleonEmbedDialog;
typedef struct GaleonEmbedDialogPrivate GaleonEmbedDialogPrivate;

struct GaleonEmbedDialog 
{
        GaleonDialog parent;
        GaleonEmbedDialogPrivate *priv;
};

struct GaleonEmbedDialogClass
{
        GaleonDialogClass parent_class;
};

GType         	   galeon_embed_dialog_get_type         (void);

GaleonEmbedDialog *galeon_embed_dialog_new	        (GaleonEmbed *embed);

GaleonEmbedDialog *galeon_embed_dialog_new_with_parent  (GtkWidget *parent_window,
							 GaleonEmbed *embed);

void		   galeon_embed_dialog_set_embed   	(GaleonEmbedDialog *dialog,
							 GaleonEmbed *embed);

GaleonEmbed *	   galeon_embed_dialog_get_embed   	(GaleonEmbedDialog *dialog);

G_END_DECLS

#endif

