/*
 *  Copyright (C) 2003  Tommi Komulainen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation. 
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef GALEON_LANGUAGE_EDITOR_H
#define GALEON_LANGUAGE_EDITOR_H 1

#include <gtk/gtkdialog.h>
#include "gul-gui-option.h"
#include <gtk/gtkliststore.h>

G_BEGIN_DECLS

#define GALEON_TYPE_LANGUAGE_EDITOR (galeon_language_editor_get_type ())
#define GALEON_LANGUAGE_EDITOR(o)   (G_TYPE_CHECK_INSTANCE_CAST((o), GALEON_TYPE_LANGUAGE_EDITOR, GaleonLanguageEditor))

typedef struct _GaleonLanguageEditor      GaleonLanguageEditor;
typedef struct _GaleonLanguageEditorClass GaleonLanguageEditorClass;

struct _GaleonLanguageEditor
{
	GtkDialog parent;

	/*< private >*/
	GtkWidget    *combobox;
	GtkWidget    *treeview;
	GtkWidget    *add;
	GtkWidget    *remove;
	GtkWidget    *up;
	GtkWidget    *down;
};

struct _GaleonLanguageEditorClass
{
	GtkDialogClass parent_class;

	/* signals */

	void  (* changed) (GaleonLanguageEditor *self);

	char* (* title)   (GaleonLanguageEditor *self, const char *value);
};

GType      galeon_language_editor_get_type      (void) G_GNUC_CONST;

GtkWidget *galeon_language_editor_new           (void);

void       galeon_language_editor_set_available (GaleonLanguageEditor *self,
		                                 const GulGuiOption   *languages,
					         guint                 n_languages);

void       galeon_language_editor_set_selected  (GaleonLanguageEditor *self,
		                                 GSList               *values);

void       galeon_language_editor_get_selected  (GaleonLanguageEditor  *self,
		                                 GSList               **values,
					         GSList               **titles);

G_END_DECLS

#endif /* GALEON_LANGUAGE_EDITOR_H */
