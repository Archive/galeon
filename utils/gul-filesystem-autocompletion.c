/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "galeon-autocompletion-source.h"
#include "gul-filesystem-autocompletion.h"
#include "galeon-debug.h"
#include <string.h>

#include  <libgnomevfs/gnome-vfs-async-ops.h>

/**
 * Private data
 */
#define GUL_FILESYSTEM_AUTOCOMPLETION_GET_PRIVATE(object) (G_TYPE_INSTANCE_GET_PRIVATE ((object), \
				       GUL_TYPE_FILESYSTEM_AUTOCOMPLETION, GulFilesystemAutocompletionPrivate))


struct _GulFilesystemAutocompletionPrivate {
	gchar *current_dir;
	gchar *base_dir;
	GnomeVFSURI *base_dir_uri;
	gchar *basic_key;
	gchar *basic_key_dir;
	GSList *files;

	guint score;
	GnomeVFSAsyncHandle *load_handle;
};

/**
 * Private functions, only availble from this file
 */
static void		gul_filesystem_autocompletion_class_init	(GulFilesystemAutocompletionClass *klass);
static void		gul_filesystem_autocompletion_init		(GulFilesystemAutocompletion *as);
static void		gul_filesystem_autocompletion_finalize_impl	(GObject *o);
static void		gul_filesystem_autocompletion_autocompletion_source_init (GaleonAutocompletionSourceIface *iface);
static void		gul_filesystem_autocompletion_autocompletion_source_foreach (GaleonAutocompletionSource *source,
										     const gchar *current_text,
										     GaleonAutocompletionSourceForeachFunc func,
										     gpointer data);
void			gul_filesystem_autocompletion_autocompletion_source_set_basic_key (GaleonAutocompletionSource *source,
											   const gchar *basic_key);
static void		gul_filesystem_autocompletion_emit_autocompletion_source_data_changed (GulFilesystemAutocompletion *gh);
static void		gul_filesystem_autocompletion_set_current_dir	(GulFilesystemAutocompletion *fa, const gchar *d);


static gpointer g_object_class;

/**
 * FilesystemAutocompletion object
 */
G_DEFINE_TYPE_WITH_CODE (GulFilesystemAutocompletion,gul_filesystem_autocompletion, 
			 G_TYPE_OBJECT,
			 G_IMPLEMENT_INTERFACE (GALEON_TYPE_AUTOCOMPLETION_SOURCE,
			       gul_filesystem_autocompletion_autocompletion_source_init));
			 
static void
gul_filesystem_autocompletion_autocompletion_source_init (GaleonAutocompletionSourceIface *iface)
{
	iface->foreach = gul_filesystem_autocompletion_autocompletion_source_foreach;
	iface->set_basic_key = gul_filesystem_autocompletion_autocompletion_source_set_basic_key;
}

static void
gul_filesystem_autocompletion_class_init (GulFilesystemAutocompletionClass *klass)
{
	G_OBJECT_CLASS (klass)->finalize = gul_filesystem_autocompletion_finalize_impl;

	g_object_class = g_type_class_peek_parent (klass);

	g_type_class_add_private (klass, sizeof (GulFilesystemAutocompletionPrivate));
}

static void 
gul_filesystem_autocompletion_init (GulFilesystemAutocompletion *e)
{
	GulFilesystemAutocompletionPrivate *p = GUL_FILESYSTEM_AUTOCOMPLETION_GET_PRIVATE (e);
	e->priv = p;

	p->score = G_MAXINT / 2;
	p->base_dir = g_strdup ("");
}

static void
gul_filesystem_autocompletion_finalize_impl (GObject *o)
{
	GulFilesystemAutocompletion *as = GUL_FILESYSTEM_AUTOCOMPLETION (o);
	GulFilesystemAutocompletionPrivate *p = as->priv;

	LOG ("in gul_filesystem_autocompletion_finalize_impl");

	g_free (p->basic_key);
	g_free (p->basic_key_dir);
	g_free (p->current_dir);
	g_free (p->base_dir);
	if (p->base_dir_uri) 
	{
		gnome_vfs_uri_unref (p->base_dir_uri);
	}


	G_OBJECT_CLASS (g_object_class)->finalize (o);
}

GulFilesystemAutocompletion *
gul_filesystem_autocompletion_new (void)
{
	GulFilesystemAutocompletion *ret = g_object_new (GUL_TYPE_FILESYSTEM_AUTOCOMPLETION, NULL);
	return ret;
}


static gchar *
gfa_get_nearest_dir (const gchar *path)
{
	gchar *ret;
	const gchar *lastslash = rindex (path, '/');
	
	if (lastslash)
	{
		if (!strcmp (path, "file://"))
		{
			/* without this, gnome-vfs does not recognize it as a dir */
			ret = g_strdup ("file:///");
		}
		else
		{
			ret = g_strndup (path, lastslash - path + 1);
		}
	}
	else
	{
		ret = g_strdup ("");
	}

	return ret;
}

static void
gul_filesystem_autocompletion_autocompletion_source_foreach (GaleonAutocompletionSource *source,
							     const gchar *basic_key,
							     GaleonAutocompletionSourceForeachFunc func,
							     gpointer data)
{
	GulFilesystemAutocompletion *fa = GUL_FILESYSTEM_AUTOCOMPLETION (source);
	GulFilesystemAutocompletionPrivate *p = fa->priv;
	GSList *li;

	gul_filesystem_autocompletion_autocompletion_source_set_basic_key (source, basic_key);

	for (li = p->files; li; li = li->next)
	{
		func (source, li->data, "", p->score, data);
	}
	
}

static void
gul_filesystem_autocompletion_emit_autocompletion_source_data_changed (GulFilesystemAutocompletion *fa)
{
	g_signal_emit_by_name (fa, "data-changed");
}

static void
gfa_load_directory_cb (GnomeVFSAsyncHandle *handle,
		       GnomeVFSResult result,
		       GList *list,
		       guint entries_read,
		       gpointer callback_data)
{
	GulFilesystemAutocompletion *fa = callback_data;
	GulFilesystemAutocompletionPrivate *p = fa->priv;
	GList *li;
	gchar *cd;

	g_return_if_fail (p->load_handle == handle);

	LOG ("gfa_load_directory_cb, entries_read == %d", entries_read);

	if (entries_read <= 0)
	{
		return;
	}
	
	if (p->basic_key_dir[strlen (p->basic_key_dir) - 1] == G_DIR_SEPARATOR
	    || p->basic_key_dir[0] == '\0')
	{
		cd = g_strdup (p->basic_key_dir);
	}
	else
	{
		cd = g_strconcat (p->basic_key_dir, G_DIR_SEPARATOR_S, NULL);
	}

	for (li = list; li; li = li->next)
	{
		GnomeVFSFileInfo *i = li->data;
		if (!(i->name[0] == '.' 
		      && (i->name[1] == '\0'
			  || (i->name[1] == '.'
			      && i->name[2] == '\0'))))
		{
			gchar *f = g_strconcat (cd, i->name, NULL);
			p->files = g_slist_prepend (p->files, f);
			
			LOG ("+ %s", f);
		}
	}

	g_free (cd);

	gul_filesystem_autocompletion_emit_autocompletion_source_data_changed (fa);
}

static void
gul_filesystem_autocompletion_set_current_dir (GulFilesystemAutocompletion *fa, const gchar *d)
{
	GulFilesystemAutocompletionPrivate *p = fa->priv;
	GnomeVFSURI *cd_uri;

	if (p->load_handle)
	{
		gnome_vfs_async_cancel (p->load_handle);
		p->load_handle = NULL;
	}

	if (p->files)
	{
		g_slist_foreach (p->files, (GFunc) g_free, NULL);
		g_slist_free (p->files);
		p->files = NULL;

		gul_filesystem_autocompletion_emit_autocompletion_source_data_changed (fa);
	}

	if (!g_str_has_prefix (d, "file:") && d[0] != '/')
	{
		/* Don't attempt to autocomplete URI's other than files:
		 * URI's, or real paths. It will cause things like
		 * PROPFIND requests for http:// sites */
		LOG ("Not a file '%s', not using gnome-vfs autocompletion", d);
		return;
	}

	if (p->base_dir_uri)
	{
		/* XXX: I think this will break when given file: URI's
		 * - crispin 12/02/2004 */
		cd_uri = gnome_vfs_uri_append_path (p->base_dir_uri, d);
	}
	else
	{
		cd_uri = gnome_vfs_uri_new (d);
	}

	if (!cd_uri)
	{
		LOG ("Can't load dir %s", d);
		return;
	}

	g_free (p->current_dir);
	p->current_dir = gnome_vfs_uri_to_string (cd_uri, GNOME_VFS_URI_HIDE_NONE);

	LOG ("Loading dir: %s", p->current_dir);

	gnome_vfs_async_load_directory_uri (&p->load_handle,
					    cd_uri,
					    GNOME_VFS_FILE_INFO_DEFAULT,
					    100,
					    0,
					    gfa_load_directory_cb,
					    fa);

	gnome_vfs_uri_unref (cd_uri);
}

void
gul_filesystem_autocompletion_autocompletion_source_set_basic_key (GaleonAutocompletionSource *source,
								   const gchar *basic_key)
{
	GulFilesystemAutocompletion *fa = GUL_FILESYSTEM_AUTOCOMPLETION (source);
	GulFilesystemAutocompletionPrivate *p = fa->priv;
	gchar *new_basic_key_dir;
	
	if (p->basic_key && !strcmp (p->basic_key, basic_key))
	{
		return;
	}

	g_free (p->basic_key);
	p->basic_key = g_strdup (basic_key);

	new_basic_key_dir = gfa_get_nearest_dir (basic_key);
	if (p->basic_key_dir && !strcmp (p->basic_key_dir, new_basic_key_dir))
	{
		g_free (new_basic_key_dir);
	}
	else
	{
		g_free (p->basic_key_dir);
		p->basic_key_dir = new_basic_key_dir;
		gul_filesystem_autocompletion_set_current_dir (fa, p->basic_key_dir);
	}
}

void
gul_filesystem_autocompletion_set_base_dir (GulFilesystemAutocompletion *fa, const gchar *d)
{
	GulFilesystemAutocompletionPrivate *p = fa->priv;

	g_free (p->base_dir);
	p->base_dir = g_strdup (d);

	if (p->base_dir_uri) 
	{
		gnome_vfs_uri_unref (p->base_dir_uri);
	}

	if (p->base_dir[0])
	{
		p->base_dir_uri = gnome_vfs_uri_new (p->base_dir);
	}
	else
	{
		p->base_dir_uri = NULL;
	}

	if (p->base_dir_uri)
	{
		gchar *t = gnome_vfs_uri_to_string (p->base_dir_uri, GNOME_VFS_URI_HIDE_NONE);
		LOG ("base_dir: %s", t);
		g_free (t);
	}
}

