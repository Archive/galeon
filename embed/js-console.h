/*
 *  Copyright (C) 2002 Jorn Baayen
 *  Copyright (C) 2004 Tommi Komulainen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef JS_CONSOLE_H
#define JS_CONSOLE_H

#include <gtk/gtkdialog.h>

G_BEGIN_DECLS

typedef struct GaleonJSConsole	    GaleonJSConsole;
typedef struct GaleonJSConsoleClass GaleonJSConsoleClass;

#define GALEON_TYPE_JS_CONSOLE             (galeon_js_console_get_type ())
#define GALEON_JS_CONSOLE(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GALEON_TYPE_JS_CONSOLE, GaleonJSConsole))
#define GALEON_JS_CONSOLE_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GALEON_TYPE_JS_CONSOLE, GaleonJSConsoleClass))
#define GALEON_IS_JS_CONSOLE(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GALEON_TYPE_JS_CONSOLE))
#define GALEON_IS_JS_CONSOLE_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GALEON_TYPE_JS_CONSOLE))

typedef enum
{
        GALEON_JS_CONSOLE_MESSAGE = 0,
        GALEON_JS_CONSOLE_ERROR   = 1,
        GALEON_JS_CONSOLE_WARNING = 2
} GaleonJSConsoleMessageType;

struct GaleonJSConsole
{
        GtkDialog parent;
};

struct GaleonJSConsoleClass
{
        GtkDialogClass parent_class;

	void (* evaluate)    (GaleonJSConsole *dialog,
			      const char      *command);
};

GType           galeon_js_console_get_type	(void) G_GNUC_CONST;

GtkWidget      *galeon_js_console_new		(void);

void            galeon_js_console_error	        (GaleonJSConsole           *self, 
						 const char                *message, 
						 GaleonJSConsoleMessageType type, 
						 const char                *sourcename,
						 guint                      linenumber,
						 const char                *sourceline,
						 guint                      column);

void            galeon_js_console_message	(GaleonJSConsole           *self, 
						 const gchar               *text);

G_END_DECLS

#endif

