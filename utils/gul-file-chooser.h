/*
 *  Copyright (C) 2003 Christian Persch
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef GUL_FILE_CHOOSER_H
#define GUL_FILE_CHOOSER_H

#include <glib-object.h>

#include <gtk/gtkwidget.h>
#include <gtk/gtkfilechooserdialog.h>

G_BEGIN_DECLS

#define GUL_TYPE_FILE_CHOOSER		(gul_file_chooser_get_type ())
#define GUL_FILE_CHOOSER(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), GUL_TYPE_FILE_CHOOSER, GulFileChooser))
#define GUL_FILE_CHOOSER_CLASS(k)	(G_TYPE_CHECK_CLASS_CAST ((k), GUL_TYPE_FILE_CHOOSER, GulFileChooserClass))
#define GUL_IS_FILE_CHOOSER(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), GUL_TYPE_FILE_CHOOSER))
#define GUL_IS_FILE_CHOOSER_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), GUL_TYPE_FILE_CHOOSER))
#define GUL_FILE_CHOOSER_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), GUL_TYPE_FILE_CHOOSER, GulFileChooserClass))

typedef struct GulFileChooserPrivate GulFileChooserPrivate;

typedef struct
{
	GtkFileChooserDialog parent;

	/*< private >*/
	GulFileChooserPrivate *priv;
} GulFileChooser;

typedef struct
{
	GtkFileChooserDialogClass parent_class;
} GulFileChooserClass;

GType		 gul_file_chooser_get_type		(void);

GulFileChooser	*gul_file_chooser_new			(const char *title,
							 GtkWidget *parent,
							 GtkFileChooserAction action,
							 const char *persist_key);

void		 gul_file_chooser_set_persist_key	(GulFileChooser *dialog,
							 const char *key);

const char	*gul_file_chooser_get_persist_key	(GulFileChooser *dialog);

G_END_DECLS

#endif
