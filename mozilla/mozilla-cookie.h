/*
 * mozilla-cookie.h
 *
 * Copyright (C) 2003 Tommi Komulainen <tommi.komulainen@iki.fi>
 * Available under the terms of the GNU General Public License version 2.
 */ 

#ifndef MOZILLA_COOKIE_H
#define MOZILLA_COOKIE_H 1

#include "cookie-info.h"

class nsICookie;

CookieInfo	*mozilla_cookie_to_info (nsICookie *aCookie);

#endif /* MOZILLA_COOKIE_H */
