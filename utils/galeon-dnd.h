/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <glib.h>
#include <gtk/gtkwidget.h>
#include <gtk/gtkdnd.h>

G_BEGIN_DECLS

/* Drag & Drop target names. */
#define GALEON_DND_URI_LIST_TYPE         "text/uri-list"
#define GALEON_DND_TEXT_TYPE             "text/plain"
#define GALEON_DND_URL_TYPE              "_NETSCAPE_URL"
#define GALEON_DND_GALEON_URL_TYPE	 "GALEON_URL"
#define GALEON_DND_GALEON_BOOKMARK_TYPE	 "GALEON_BOOKMARK"

/* Standard Drag & Drop types. */
typedef enum {
        GALEON_DND_URI_LIST,
        GALEON_DND_URL,
        GALEON_DND_TEXT,
} GaleonIconDndTargetType;

typedef void (* GaleonDragEachSelectedItemDataGet) (const char *url, const char *title,
                                                    int x, int y, int w, int h, 
                                                    gpointer data);

typedef void (* GaleonDragEachSelectedItemIterator) (GaleonDragEachSelectedItemDataGet iteratee, 
                                                     gpointer iterator_context, 
                                                     gpointer data);

typedef void (* GaleonDragEachReceivedItemIterator) (const char * url, const char * title,
                                                     gpointer data);

gboolean galeon_dnd_drag_data_get 		(GtkWidget *widget,
                                    		 GdkDragContext *context,
                                    		 GtkSelectionData *selection_data,
                                    		 guint info,
                                    		 guint32 time,
                                    		 gpointer container_context,
                                    		 GaleonDragEachSelectedItemIterator each_selected_item_iterator);

void     galeon_dnd_url_drag_source_set 	(GtkWidget *widget); 


gboolean galeon_dnd_drag_data_receive 		(GtkWidget *widget,
                                    		 GdkDragContext *context,
						 gint x,
						 gint y,
                                    		 GtkSelectionData *selection_data,
                                    		 guint info,
                                    		 guint32 time,
                                    		 gpointer container_context,
                                    		 GaleonDragEachReceivedItemIterator each_received_item_iterator);



void	 galeon_dnd_url_drag_dest_set		(GtkWidget *widget);
void	 galeon_dnd_url_drag_dest_set_with_flags (GtkWidget *widget, GtkDestDefaults flags);

void	 galeon_dnd_url_list_drag_dest_set	(GtkWidget *widget);
void	 galeon_dnd_url_list_drag_dest_set_with_flags (GtkWidget *widget, GtkDestDefaults flags);

G_END_DECLS
