/* -*- mode: c c-style: k&r c-basic-offset: 8 -*- */
/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __bookmarks_single_editor_h
#define __bookmarks_single_editor_h

#include <glib-object.h>
#include <bookmarks.h>

/* object forward declarations */

typedef struct _GbSingleEditor GbSingleEditor;
typedef struct _GbSingleEditorClass GbSingleEditorClass;
typedef struct _GbSingleEditorPrivate GbSingleEditorPrivate;

/**
 * Editor object
 */

#define GB_TYPE_SINGLE_EDITOR			(gb_single_editor_get_type())
#define GB_SINGLE_EDITOR(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), GB_TYPE_SINGLE_EDITOR,\
						 GbSingleEditor))
#define GB_SINGLE_EDITOR_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), GB_TYPE_SINGLE_EDITOR,\
						 GbSingleEditorClass))
#define GB_IS_SINGLE_EDITOR(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), GB_TYPE_SINGLE_EDITOR))
#define GB_IS_SINGLE_EDITOR_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), GB_TYPE_SINGLE_EDITOR))
#define GB_SINGLE_EDITOR_GET_CLASS(obj)		(G_TYPE_INSTANCE_GET_CLASS((obj), GB_TYPE_SINGLE_EDITOR,\
						 GbSingleEditorClass))

struct _GbSingleEditorClass 
{
	GObjectClass parent_class;

	/* virtual methods */
	void (*init_widgets) (GbSingleEditor *e);
};

/* Remember: fields are public read-only */
struct _GbSingleEditor
{
	GObject parent_object;

	GbSingleEditorPrivate *priv;
	
	/* protected */
	GtkWidget *name_label;
	GtkWidget *name_entry;
	GtkWidget *smarturl_label;
	GtkWidget *smarturl_entry;
	GtkWidget *date_added_label;
	GtkWidget *date_added_value_label;
	GtkWidget *date_modified_label;
	GtkWidget *date_modified_value_label;
	GtkWidget *date_visited_label;
	GtkWidget *date_visited_value_label;
	GtkWidget *smart_entry_width_label;
	GtkWidget *smart_entry_width_spinbutton;
	GtkWidget *parameter_encoding_label;
	GtkWidget *parameter_encoding_combo;
	GtkWidget *location_label;
	GtkWidget *location_entry;
	GtkWidget *notes_label;
	GtkWidget *notes_textview;
	GtkWidget *nicks_label;
	GtkWidget *nicks_entry;
	GtkWidget *search_text_label;
	GtkWidget *search_text_entry;
	GtkWidget *search_recently_visited_checkbox;
	GtkWidget *search_recently_visited_spin;
	GtkWidget *search_recently_created_checkbox;
	GtkWidget *search_recently_created_spin;
	GtkWidget *search_options;
	GtkWidget *search_b_f_match_case;
	GtkWidget *search_b_f_include_folders;
	GtkWidget *search_b_f_include_sites;
	GtkWidget *search_b_f_look_in_name;
	GtkWidget *search_b_f_look_in_location;
	GtkWidget *search_b_f_look_in_notes;
	GtkWidget *search_b_f_several_and;
	GtkWidget *search_b_f_several_or;
	GtkWidget *search_b_f_several_exact;

	GtkWidget *autobm_search_text_label;
	GtkWidget *autobm_search_text_entry;
	GtkWidget *autobm_options;
	GtkWidget *autobm_search_b_f_match_case;
	GtkWidget *autobm_group_by_host_checkbutton;
	GtkWidget *autobm_search_b_f_look_in_title;
	GtkWidget *autobm_search_b_f_look_in_url;
	GtkWidget *autobm_search_b_f_several_and;
	GtkWidget *autobm_search_b_f_several_or;
	GtkWidget *autobm_search_b_f_several_exact;
	GtkWidget *autobm_score_more_recently_used;
	GtkWidget *autobm_score_more_frequently_used;
	GtkWidget *autobm_score_both;

	GtkWidget *create_toolbar_checkbox;
	GtkWidget *add_to_context_checkbox;
	GtkWidget *image_label;
	GtkWidget *image_pixmapentry;
	GtkWidget *more_properties_button;
};

typedef enum
{
	GB_SINGLE_EDITOR_DISABLE_POLICY_HIDE,
	GB_SINGLE_EDITOR_DISABLE_POLICY_DISABLE
} GbSingleEditorDisablePolicy;

GType			gb_single_editor_get_type		(void);
GbSingleEditor *	gb_single_editor_new			(void);
void			gb_single_editor_set_bookmark		(GbSingleEditor *editor, GbBookmark *b);
GbBookmark *		gb_single_editor_get_bookmark		(GbSingleEditor *editor);
void			gb_single_editor_show			(GbSingleEditor *editor);
void	 		gb_single_editor_init_widgets_and_signals (GbSingleEditor *e);
void	 		gb_single_editor_set_disable_policy	(GbSingleEditor *e, GbSingleEditorDisablePolicy p);
void 			gb_single_editor_focus_name_entry	(GbSingleEditor *e);

/* protected */
void			gb_single_editor_init_widgets		(GbSingleEditor *e);

#endif
