/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GALEON_DIALOG_H
#define GALEON_DIALOG_H

#include <glib-object.h>
#include <glib.h>
#include <gtk/gtkwidget.h>
	
G_BEGIN_DECLS

typedef struct GaleonDialogClass GaleonDialogClass;

#define GALEON_TYPE_DIALOG             (galeon_dialog_get_type ())
#define GALEON_DIALOG(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GALEON_TYPE_DIALOG, GaleonDialog))
#define GALEON_DIALOG_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GALEON_TYPE_DIALOG, GaleonDialogClass))
#define GALEON_IS_DIALOG(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GALEON_TYPE_DIALOG))
#define GALEON_IS_DIALOG_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GALEON_DIALOG))
#define GALEON_DIALOG_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GALEON_TYPE_DIALOG, GaleonDialogClass))

typedef struct GaleonDialog GaleonDialog;
typedef struct GaleonDialogPrivate GaleonDialogPrivate;

#define SY_BEGIN_GROUP -20
#define SY_END_GROUP -21
#define SY_END -22
#define SY_BEGIN_GROUP_INVERSE -23

struct GaleonDialog 
{
        GObject parent;
        GaleonDialogPrivate *priv;
};

typedef enum
{
	PT_NORMAL,
	PT_AUTOAPPLY
} PropertyType;

typedef struct
{
	int id;
	const char *control_name;
	const char *state_pref;
	PropertyType type;
	int *sg;
} GaleonDialogProperty;

struct GaleonDialogClass
{
        GObjectClass parent_class;

	void        (* construct)       (GaleonDialog *dialog,
					 const GaleonDialogProperty *properties,
					 const char *file,
			                 const char *name);
	void	    (* destruct)	(GaleonDialog *dialog);
	gint        (* run)  		(GaleonDialog *dialog);
	void        (* show)  		(GaleonDialog *dialog);
	GtkWidget * (* get_control)     (GaleonDialog *dialog,
				         int property_id);
	void        (* get_value)       (GaleonDialog *dialog,
					 int property_id,
					 GValue *value);
};

GType         galeon_dialog_get_type        (void);

GaleonDialog *galeon_dialog_new		    (void);

GaleonDialog *galeon_dialog_new_with_parent (GtkWidget *parent_window);

void	      galeon_dialog_construct       (GaleonDialog *dialog,
					     const GaleonDialogProperty *properties,
					     const char *file,
					     const char *name);

void	      galeon_dialog_destruct	    (GaleonDialog *dialog);

gint          galeon_dialog_run		    (GaleonDialog *dialog);

void          galeon_dialog_show	    (GaleonDialog *dialog);

void          galeon_dialog_show_embedded   (GaleonDialog *dialog,
					     GtkWidget *container);

void	      galeon_dialog_remove_embedded (GaleonDialog *dialog);

void          galeon_dialog_set_modal       (GaleonDialog *dialog,
			                     gboolean is_modal);

GtkWidget    *galeon_dialog_get_control     (GaleonDialog *dialog,
					     int property_id);

void          galeon_dialog_get_value       (GaleonDialog *dialog,
					     int property_id,
					     GValue *value);

G_END_DECLS

#endif

