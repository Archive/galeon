/*
 * cookie-info.h
 *
 * Copyright (C) 2003 Tommi Komulainen <tommi.komulainen@iki.fi>
 * Available under the terms of the GNU General Public License version 2.
 */ 

#ifndef COOKIE_INFO_H
#define COOKIE_INFO_H 1

#include <gtk/gtkwidget.h>

G_BEGIN_DECLS

/**
 * Cookie: the type of cookies
 */
typedef struct
{
	char     *domain;
        char     *name;
        char     *value;
        char     *path;
        gboolean  secure;
        char     *expire;
} CookieInfo;

void	   cookie_info_free		(CookieInfo *cookie);

GtkWidget *cookie_info_table_new 	(const CookieInfo *cookie);

G_END_DECLS

#endif /* COOKIE_INFO_TABLE_H */
