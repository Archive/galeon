/*
 *  Copyright (C) 2002 Philip Langdale
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "PrintProgressListener.h"

#include <nsIInterfaceRequestorUtils.h>
#include <nsIPrintSettings.h>
#include <nsIRequest.h>
#include <nsIWebBrowserPrint.h>
#include <nsIWebProgress.h>
#include <nsNetError.h>                 // for NS_BINDING_ABORTED
#include <nsMemory.h>

#include "gul-glade.h"
#include "galeon-debug.h"
#include "egg-recent-model.h"

/* see the FIXME below */
#include <locale.h>

#include <gtk/gtkdialog.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkmain.h>
#include <gtk/gtkprogressbar.h>
#include <glib/gi18n.h>


NS_IMPL_ISUPPORTS1(GPrintListener, nsIWebProgressListener)


static void
set_progress (GtkProgressBar *progress, guint current, guint total, gint remaining)
{
	gdouble fraction = (gdouble)current / total;
	char   *text;

	if (current >= total)
	{
		text = g_strdup (_("Completed"));
		fraction = 1.0;
	}
	else if (remaining >= 0)
	{
		text = g_strdup_printf (_("%d of %d pages printed, about %d seconds left"),
					current, total, remaining);
	}
	else
	{
		text = g_strdup_printf (_("%d of %d pages printed"), current, total);
	}

	gtk_progress_bar_set_fraction (progress, fraction);
	gtk_progress_bar_set_text (progress, text);
	g_free (text);
}


GPrintListener::GPrintListener(nsIWebBrowserPrint *aPrint, nsIPrintSettings *aSettings, GtkWindow *aParent)
	: mStartTime(PR_Now())
	, mWeakPrint(getter_AddRefs(NS_GetWeakReference(aPrint)))
{
	LOG ("GPrintListener() = %p", this);

	GladeXML *gxml;

	gxml = gul_glade_widget_new ("print.glade",
				     "print_progress_dialog", &mDialog,
				     NULL);
	g_object_add_weak_pointer (G_OBJECT (mDialog), (gpointer*)&mDialog);
	gtk_window_set_transient_for (GTK_WINDOW (mDialog), aParent);
	if (!aParent->group)
	{
		GtkWindowGroup *group = gtk_window_group_new ();
		gtk_window_group_add_window (group, aParent);
		g_object_unref (group);
	}
	gtk_window_group_add_window (aParent->group, GTK_WINDOW (mDialog));

	// Since there's no way to cancel printing (nsIWebBrowserPrint::Cancel
	// always fails for me...) don't give false impression that closing the
	// dialog would do it.
	g_signal_connect (mDialog, "delete-event", G_CALLBACK (gtk_true), 0);

	GtkWidget *document;
	GtkWidget *label;
	GtkWidget *destination;

	gul_glade_get_widgets (gxml,
			       "document",          &document,
			       "destination_label", &label,
			       "destination",       &destination,
			       "progressbar",       &mProgress,
			       NULL);
	g_object_add_weak_pointer (G_OBJECT (mProgress), (gpointer*)&mProgress);
       
	g_object_unref (gxml);


	PRUnichar *wstring = nsnull;

	PRBool printToFile;
	aSettings->GetPrintToFile(&printToFile);
	if (printToFile)
	{
		aSettings->GetToFileName(&wstring);

		mFilename = wstring;

		char *markup = g_strdup_printf ("<b>%s</b>", _("To file:"));
		char *basename = g_path_get_basename (mFilename.get());
		gtk_label_set_markup (GTK_LABEL (label), markup);
		gtk_label_set_text (GTK_LABEL (destination), basename);
		g_free (basename);
		g_free (markup);
	}
	else
	{
		aSettings->GetPrinterName(&wstring);

		char *markup = g_strdup_printf ("<b>%s</b>", _("To printer:"));
		gtk_label_set_markup (GTK_LABEL (label), markup);
		gtk_label_set_text (GTK_LABEL (destination), GulCString (wstring).get());
		g_free (markup);
	}
	nsMemory::Free(wstring);

	aSettings->GetTitle(&wstring);
	gtk_label_set_text (GTK_LABEL (document), GulCString (wstring).get());
	nsMemory::Free(wstring);


	// set the progress bar text temporarily to get the size request as
	// wide as it can possibly get to avoid unnecessary width changes while
	// printing (the addition of time estimate)
	set_progress (GTK_PROGRESS_BAR(mProgress), 998, 999, 1000);
}

GPrintListener::~GPrintListener()
{
	LOG ("~GPrintListener(%p)", this);

	if (mDialog)
	{
		gtk_widget_destroy (mDialog);
	}
}

static gboolean
destroy_timeout_cb (gpointer user_data)
{
	GtkWidget *widget = GTK_WIDGET(user_data);
	gtk_widget_destroy (widget);
	return FALSE;
}

/* void onStateChange (in nsIWebProgress aWebProgress, in nsIRequest aRequest, in long aStateFlags, in unsigned long aStatus); */
NS_IMETHODIMP GPrintListener::OnStateChange(nsIWebProgress *aWebProgress,
					    nsIRequest *aRequest,
					    PRUint32 aStateFlags,
					    PRUint32 aStatus)
{
	if (!(aStateFlags & nsIWebProgressListener::STATE_STOP)) return NS_OK;

	LOG ("[%p] GPrintListener::OnStateChange(%x, %x (%s))", this,
	     aStateFlags, aStatus, NS_SUCCEEDED(aStatus) ? "SUCCEEDED" : "FAILED");

	// keep the dialog visible for a while after completion to display the
	// "completed" message and avoid annoying flash when the file is very
	// small
	if (mDialog)
	{
		g_object_remove_weak_pointer (G_OBJECT(mDialog), (gpointer*)&mDialog);
		g_object_remove_weak_pointer (G_OBJECT(mProgress), (gpointer*)&mProgress);
		gtk_window_set_destroy_with_parent (GTK_WINDOW(mDialog), FALSE);
		g_timeout_add (1000, destroy_timeout_cb, mDialog);
		mDialog = NULL;
	}

	/* FIXME(MOZILLA) ugly workaround for a mozilla problem with 
	 * reseting the LC_* environment when printing */
	setlocale(LC_ALL,"");

	// add to recent files if printing to file was successful
	if (NS_SUCCEEDED(aStatus) && !mFilename.IsEmpty())
	{
		EggRecentModel *model;
		EggRecentItem  *item;

	        model = egg_recent_model_new (EGG_RECENT_MODEL_SORT_NONE);

	        item = egg_recent_item_new_from_uri (mFilename.get());

		// keep in sync with ProgressListener.cpp
		egg_recent_item_add_group (item, "Galeon");
		egg_recent_item_add_group (item, "Web Browser");

		egg_recent_model_add_full (model, item);
		egg_recent_item_unref (item);

		g_object_unref (model);
	}

	return NS_OK;
}

/* void onProgressChange (in nsIWebProgress aWebProgress, in nsIRequest aRequest, in long aCurSelfProgress, in long aMaxSelfProgress, in long aCurTotalProgress, in long aMaxTotalProgress); */
NS_IMETHODIMP GPrintListener::OnProgressChange(nsIWebProgress *aWebProgress,
					       nsIRequest *aRequest,
					       PRInt32 aCurSelfProgress,
					       PRInt32 aMaxSelfProgress,
					       PRInt32 aCurTotalProgress,
					       PRInt32 aMaxTotalProgress)
{
	LOG ("[%p] GPrintListener::OnProgressChange(%d, %d, %d, %d)", this,
	     aCurSelfProgress, aMaxSelfProgress, aCurTotalProgress, aMaxTotalProgress);

	if (!mProgress) return NS_OK;

	if (mDialog) gtk_widget_show (mDialog);

	PRInt32 remaining;
	if (aCurTotalProgress >= 5)
	{
		PRTime elapsed  = PR_Now() - mStartTime;
		PRTime estimate = elapsed * aMaxTotalProgress / aCurTotalProgress;
		remaining = (PRInt32)((estimate - elapsed) / PR_USEC_PER_SEC);
	}
	else
		remaining = -1;

	set_progress (GTK_PROGRESS_BAR(mProgress), aCurTotalProgress, aMaxTotalProgress, remaining);

	return NS_OK;
}

/* void onLocationChange (in nsIWebProgress aWebProgress, in nsIRequest aRequest, in nsIURI location); */
NS_IMETHODIMP GPrintListener::OnLocationChange(nsIWebProgress *aWebProgress,
					       nsIRequest *aRequest,
					       nsIURI *location)
{
	return NS_OK;
}

/* void onStatusChange (in nsIWebProgress aWebProgress, in nsIRequest aRequest, in nsresult aStatus, in wstring aMessage); */
NS_IMETHODIMP GPrintListener::OnStatusChange(nsIWebProgress *aWebProgress,
					     nsIRequest *aRequest,
					     nsresult aStatus,
					     const PRUnichar *aMessage)
{
	LOG ("[%p] GPrintListener::OnStatusChange(%x (%s), '%s')", this,
	     aStatus, NS_SUCCEEDED(aStatus) ? "SUCCEEDED" : "FAILED",
	     GulCString (aMessage).get());
	return NS_OK;
}

/* void onSecurityChange (in nsIWebProgress aWebProgress, in nsIRequest aRequest, in long state); */
NS_IMETHODIMP GPrintListener::OnSecurityChange(nsIWebProgress *aWebProgress,
					       nsIRequest *aRequest,
					       PRUint32 state)
{
	return NS_OK;
}
