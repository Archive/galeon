/*
 *  Copyright (C) 2003 Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GALEON_HOME_ACTION_H
#define GALEON_HOME_ACTION_H


#include "galeon-action.h"

#define GALEON_TYPE_ACTION_HOME				(galeon_action_home_get_type ())
#define GALEON_ACTION_HOME(obj)				(G_TYPE_CHECK_INSTANCE_CAST ((obj), GALEON_TYPE_ACTION_HOME,\
							 GaleonActionHome))
#define GALEON_ACTION_HOME_CLASS(klass)			(G_TYPE_CHECK_CLASS_CAST ((klass), GALEON_TYPE_ACTION_HOME,\
							 GaleonActionHomeClass))
#define GALEON_IS_ACTION_HOME(obj)			(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GALEON_TYPE_ACTION_HOME))
#define GALEON_IS_ACTION_HOME_CLASS(klass)		(G_TYPE_CHECK_CLASS_TYPE ((obj), GALEON_TYPE_ACTION_HOME))
#define GALEON_ACTION_HOME_GET_CLASS(obj)		(G_TYPE_INSTANCE_GET_CLASS((obj), GALEON_TYPE_ACTION_HOME,\
							 GaleonActionHomeClass))

typedef struct _GaleonActionHome		GaleonActionHome;
typedef struct _GaleonActionHomeClass	GaleonActionHomeClass;
typedef struct GaleonActionHomePrivate	GaleonActionHomePrivate;

struct _GaleonActionHome
{
	GaleonAction parent;
};

struct _GaleonActionHomeClass
{
	GaleonActionClass parent_class;

};

GType		galeon_action_home_get_type		(void);

#endif
