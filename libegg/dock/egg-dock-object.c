/*
 * egg-dock-object.c - Abstract base class for all dock related objects
 *
 * Copyright (C) 2002 Gustavo Gir�ldez <gustavo.giraldez@gmx.net>
 * Copyright (C) 2003 Biswapesh Chattopadhyay <biswapesh_chatterjee@tcscal.co.in>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <eggintl.h>
#include <egg-macros.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtkobject.h>

#include "egg-dock-object.h"
#include "egg-dock-master.h"
#include "libeggdocktypebuiltins.h"
#include <eggmarshalers.h>

/* for later use by the registry */
#include "egg-dock.h"
#include "egg-dock-item.h"
#include "egg-dock-paned.h"
#include "egg-dock-box.h"
#include "egg-dock-notebook.h"
#include "egg-dock-placeholder.h"

struct _EggDockObjectPrivate {
	gpointer unused;
};


/* ----- Private prototypes ----- */

static void     egg_dock_object_class_init         (EggDockObjectClass *klass);
static void     egg_dock_object_instance_init      (EggDockObject      *object);

static void     egg_dock_object_set_property       (GObject            *g_object,
                                                    guint               prop_id,
                                                    const GValue       *value,
                                                    GParamSpec         *pspec);
static void     egg_dock_object_get_property       (GObject            *g_object,
                                                    guint               prop_id,
                                                    GValue             *value,
                                                    GParamSpec         *pspec);
static void     egg_dock_object_finalize           (GObject            *g_object);

static void     egg_dock_object_destroy            (GtkObject          *gtk_object);

static void     egg_dock_object_show               (GtkWidget          *widget);
static void     egg_dock_object_hide               (GtkWidget          *widget);

static void     egg_dock_object_real_detach        (EggDockObject      *object,
                                                    gboolean            recursive);
static void     egg_dock_object_real_reduce        (EggDockObject      *object);
static void     egg_dock_object_dock_unimplemented (EggDockObject     *object,
                                                    EggDockObject     *requestor,
                                                    EggDockPlacement   position,
                                                    GValue            *other_data);
static void     egg_dock_object_real_present       (EggDockObject     *object,
                                                    EggDockObject     *child);


/* ----- Private data types and variables ----- */

enum {
    PROP_0,
    PROP_NAME,
    PROP_LONG_NAME,
    PROP_MASTER,
    PROP_EXPORT_PROPERTIES
};

enum {
    DETACH,
    DOCK,
    LAST_SIGNAL
};

static guint egg_dock_object_signals [LAST_SIGNAL] = { 0 };

/* ----- Private interface ----- */

EGG_CLASS_BOILERPLATE (EggDockObject, egg_dock_object, GtkContainer, GTK_TYPE_CONTAINER);

static void
egg_dock_object_class_init (EggDockObjectClass *klass)
{
    GObjectClass      *g_object_class;
    GtkObjectClass    *object_class;
    GtkWidgetClass    *widget_class;
    GtkContainerClass *container_class;

    g_object_class = G_OBJECT_CLASS (klass);
    object_class = GTK_OBJECT_CLASS (klass);
    widget_class = GTK_WIDGET_CLASS (klass);
    container_class = GTK_CONTAINER_CLASS (klass);

    g_object_class->set_property = egg_dock_object_set_property;
    g_object_class->get_property = egg_dock_object_get_property;
    g_object_class->finalize = egg_dock_object_finalize;

    g_object_class_install_property (
        g_object_class, PROP_NAME,
        g_param_spec_string (EGG_DOCK_NAME_PROPERTY, _("Name"),
                             _("Unique name for identifying the dock object"),
                             NULL,
                             G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY |
                             EGG_DOCK_PARAM_EXPORT));

    g_object_class_install_property (
        g_object_class, PROP_LONG_NAME,
        g_param_spec_string ("long_name", _("Long name"),
                             _("Human readable name for the dock object"),
                             NULL,
                             G_PARAM_READWRITE));

    g_object_class_install_property (
        g_object_class, PROP_MASTER,
        g_param_spec_object ("master", _("Dock master"),
                             _("Dock master this dock object is bound to"),
                             EGG_TYPE_DOCK_MASTER,
                             G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
    
    object_class->destroy = egg_dock_object_destroy;
    
    widget_class->show = egg_dock_object_show;
    widget_class->hide = egg_dock_object_hide;
    
    klass->is_compound = TRUE;
    
    klass->detach = egg_dock_object_real_detach;
    klass->reduce = egg_dock_object_real_reduce;
    klass->dock_request = NULL;
    klass->dock = egg_dock_object_dock_unimplemented;
    klass->reorder = NULL;
    klass->present = egg_dock_object_real_present;
    klass->child_placement = NULL;
    
    egg_dock_object_signals [DETACH] =
        g_signal_new ("detach",
                      G_TYPE_FROM_CLASS (klass),
                      G_SIGNAL_RUN_LAST,
                      G_STRUCT_OFFSET (EggDockObjectClass, detach),
                      NULL,
                      NULL,
                      _egg_marshal_VOID__BOOLEAN,
                      G_TYPE_NONE,
                      1,
                      G_TYPE_BOOLEAN);

    egg_dock_object_signals [DOCK] =
        g_signal_new ("dock",
                      G_TYPE_FROM_CLASS (klass),
                      G_SIGNAL_RUN_FIRST,
                      G_STRUCT_OFFSET (EggDockObjectClass, dock),
                      NULL,
                      NULL,
                      _egg_marshal_VOID__OBJECT_ENUM_BOXED,
                      G_TYPE_NONE,
                      3,
                      EGG_TYPE_DOCK_OBJECT,
                      EGG_TYPE_DOCK_PLACEMENT,
                      G_TYPE_VALUE);
}

static void
egg_dock_object_instance_init (EggDockObject *object)
{
    object->flags = EGG_DOCK_AUTOMATIC;
    object->freeze_count = 0;
}

static void
egg_dock_object_set_property  (GObject      *g_object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
    EggDockObject *object = EGG_DOCK_OBJECT (g_object);

    switch (prop_id) {
    case PROP_NAME:
        g_free (object->name);
        object->name = g_value_dup_string (value);
        break;
    case PROP_LONG_NAME:
        g_free (object->long_name);
        object->long_name = g_value_dup_string (value);
        break;
    case PROP_MASTER:
        if (g_value_get_object (value)) 
            egg_dock_object_bind (object, g_value_get_object (value));
        else
            egg_dock_object_unbind (object);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
egg_dock_object_get_property  (GObject      *g_object,
                               guint         prop_id,
                               GValue       *value,
                               GParamSpec   *pspec)
{
    EggDockObject *object = EGG_DOCK_OBJECT (g_object);

    switch (prop_id) {
    case PROP_NAME:
        g_value_set_string (value, object->name);
        break;
    case PROP_LONG_NAME:
        g_value_set_string (value, object->long_name);
        break;
    case PROP_MASTER:
        g_value_set_object (value, object->master);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
egg_dock_object_finalize (GObject *g_object)
{
    EggDockObject *object;
    
    g_return_if_fail (g_object != NULL && EGG_IS_DOCK_OBJECT (g_object));

    object = EGG_DOCK_OBJECT (g_object);

    g_free (object->name);
    object->name = NULL;
    g_free (object->long_name);
    object->long_name = NULL;

    EGG_CALL_PARENT (G_OBJECT_CLASS, finalize, (g_object));
}

static void
egg_dock_object_foreach_detach (EggDockObject *object,
                                gpointer       user_data)
{
    egg_dock_object_detach (object, TRUE);
}

static void
egg_dock_object_destroy (GtkObject *gtk_object)
{
    EggDockObject *object;

    g_return_if_fail (EGG_IS_DOCK_OBJECT (gtk_object));

    object = EGG_DOCK_OBJECT (gtk_object);
    if (egg_dock_object_is_compound (object)) {
        /* detach our dock object children if we have some, and even
           if we are not attached, so they can get notification */
        egg_dock_object_freeze (object);
        gtk_container_foreach (GTK_CONTAINER (object),
                               (GtkCallback) egg_dock_object_foreach_detach,
                               NULL);
        object->reduce_pending = FALSE;
        egg_dock_object_thaw (object);
    }
    if (EGG_DOCK_OBJECT_ATTACHED (object)) {
        /* detach ourselves */
        egg_dock_object_detach (object, FALSE);
    }
    
    /* finally unbind us */
    if (object->master)
        egg_dock_object_unbind (object);
        
    EGG_CALL_PARENT (GTK_OBJECT_CLASS, destroy, (gtk_object));
}

static void
egg_dock_object_foreach_automatic (EggDockObject *object,
                                   gpointer       user_data)
{
    void (* function) (GtkWidget *) = user_data;

    if (EGG_DOCK_OBJECT_AUTOMATIC (object))
        (* function) (GTK_WIDGET (object));
}

static void
egg_dock_object_show (GtkWidget *widget)
{
    if (egg_dock_object_is_compound (EGG_DOCK_OBJECT (widget))) {
        gtk_container_foreach (GTK_CONTAINER (widget),
                               (GtkCallback) egg_dock_object_foreach_automatic,
                               gtk_widget_show);
    }
    EGG_CALL_PARENT (GTK_WIDGET_CLASS, show, (widget));
}

static void
egg_dock_object_hide (GtkWidget *widget)
{
    if (egg_dock_object_is_compound (EGG_DOCK_OBJECT (widget))) {
        gtk_container_foreach (GTK_CONTAINER (widget),
                               (GtkCallback) egg_dock_object_foreach_automatic,
                               gtk_widget_hide);
    }
    EGG_CALL_PARENT (GTK_WIDGET_CLASS, hide, (widget));
}

static void
egg_dock_object_real_detach (EggDockObject *object,
                             gboolean       recursive)
{
    EggDockObject *parent;
    GtkWidget     *widget;
    
    g_return_if_fail (object != NULL);

    /* detach children */
    if (recursive && egg_dock_object_is_compound (object)) {
        gtk_container_foreach (GTK_CONTAINER (object),
                               (GtkCallback) egg_dock_object_detach,
                               (gpointer) recursive);
    }
    
    /* detach the object itself */
    EGG_DOCK_OBJECT_UNSET_FLAGS (object, EGG_DOCK_ATTACHED);
    parent = egg_dock_object_get_parent_object (object);
    widget = GTK_WIDGET (object);
    if (widget->parent)
        gtk_container_remove (GTK_CONTAINER (widget->parent), widget);
    if (parent)
        egg_dock_object_reduce (parent);
}

static void
egg_dock_object_real_reduce (EggDockObject *object)
{
    EggDockObject *parent;
    GList         *children;
    
    g_return_if_fail (object != NULL);

    if (!egg_dock_object_is_compound (object))
        return;

    parent = egg_dock_object_get_parent_object (object);
    children = gtk_container_get_children (GTK_CONTAINER (object));
    if (g_list_length (children) <= 1) {
        GList *l;
        
        /* detach ourselves and then re-attach our children to our
           current parent.  if we are not currently attached, the
           children are detached */
        if (parent)
            egg_dock_object_freeze (parent);
        egg_dock_object_freeze (object);
        egg_dock_object_detach (object, FALSE);
        for (l = children; l; l = l->next) {
            EggDockObject *child = EGG_DOCK_OBJECT (l->data);

            g_object_ref (child);
            EGG_DOCK_OBJECT_SET_FLAGS (child, EGG_DOCK_IN_REFLOW);
            egg_dock_object_detach (child, FALSE);
            if (parent)
                gtk_container_add (GTK_CONTAINER (parent), GTK_WIDGET (child));
            EGG_DOCK_OBJECT_UNSET_FLAGS (child, EGG_DOCK_IN_REFLOW);
            g_object_unref (child);
        }
        /* sink the widget, so any automatic floating widget is destroyed */
        g_object_ref_sink (object);
        /* don't reenter */
        object->reduce_pending = FALSE;
        egg_dock_object_thaw (object);
        if (parent)
            egg_dock_object_thaw (parent);
    }
    g_list_free (children);
}

static void
egg_dock_object_dock_unimplemented (EggDockObject    *object,
                                    EggDockObject    *requestor,
                                    EggDockPlacement  position,
                                    GValue           *other_data)
{
    g_warning (_("Call to egg_dock_object_dock in a dock object %p "
                 "(object type is %s) which hasn't implemented this method"),
               object, G_OBJECT_TYPE_NAME (object));
}

static void 
egg_dock_object_real_present (EggDockObject *object,
                              EggDockObject *child)
{
    gtk_widget_show (GTK_WIDGET (object));
}


/* ----- Public interface ----- */

gboolean
egg_dock_object_is_compound (EggDockObject *object)
{
    EggDockObjectClass *klass;

    g_return_val_if_fail (object != NULL, FALSE);
    g_return_val_if_fail (EGG_IS_DOCK_OBJECT (object), FALSE);

    klass = EGG_DOCK_OBJECT_GET_CLASS (object);
    return klass->is_compound;
}

void
egg_dock_object_detach (EggDockObject *object,
                        gboolean       recursive)
{
    g_return_if_fail (object != NULL);

    if (!EGG_DOCK_OBJECT_ATTACHED (object))
        return;
    
    /* freeze the object to avoid reducing while detaching children */
    egg_dock_object_freeze (object);
    EGG_DOCK_OBJECT_SET_FLAGS (object, EGG_DOCK_IN_DETACH);
    g_signal_emit (object, egg_dock_object_signals [DETACH], 0, recursive);
    EGG_DOCK_OBJECT_UNSET_FLAGS (object, EGG_DOCK_IN_DETACH);
    egg_dock_object_thaw (object);
}

EggDockObject *
egg_dock_object_get_parent_object (EggDockObject *object)
{
    GtkWidget *parent;
    
    g_return_val_if_fail (object != NULL, NULL);

    parent = GTK_WIDGET (object)->parent;
    while (parent && !EGG_IS_DOCK_OBJECT (parent)) {
        parent = parent->parent;
    }
    
    return parent ? EGG_DOCK_OBJECT (parent) : NULL;
}

void
egg_dock_object_freeze (EggDockObject *object)
{
    g_return_if_fail (object != NULL);
    
    if (object->freeze_count == 0) {
        g_object_ref (object);   /* dock objects shouldn't be
                                    destroyed if they are frozen */
    }
    object->freeze_count++;
}

void
egg_dock_object_thaw (EggDockObject *object)
{
    g_return_if_fail (object != NULL);
    g_return_if_fail (object->freeze_count > 0);
    
    object->freeze_count--;
    if (object->freeze_count == 0) {
        if (object->reduce_pending) {
            object->reduce_pending = FALSE;
            egg_dock_object_reduce (object);
        }
        g_object_unref (object);
    }
}

void
egg_dock_object_reduce (EggDockObject *object)
{
    g_return_if_fail (object != NULL);

    if (EGG_DOCK_OBJECT_FROZEN (object)) {
        object->reduce_pending = TRUE;
        return;
    }

    EGG_CALL_VIRTUAL (object, EGG_DOCK_OBJECT_GET_CLASS, reduce, (object));
}

gboolean
egg_dock_object_dock_request (EggDockObject  *object,
                              gint            x,
                              gint            y,
                              EggDockRequest *request)
{
    g_return_val_if_fail (object != NULL && request != NULL, FALSE);
    
    return EGG_CALL_VIRTUAL_WITH_DEFAULT (object,
                                          EGG_DOCK_OBJECT_GET_CLASS,
                                          dock_request,
                                          (object, x, y, request),
                                          FALSE);
}

void
egg_dock_object_dock (EggDockObject    *object,
                      EggDockObject    *requestor,
                      EggDockPlacement  position,
                      GValue           *other_data)
{
    EggDockObject *parent;
    
    g_return_if_fail (object != NULL && requestor != NULL);
        
    if (object == requestor)
        return;
    
    if (!object->master)
        g_warning (_("Dock operation requested in a non-bound object %p. "
                     "The application might crash"), object);
        
    if (!egg_dock_object_is_bound (requestor))
        egg_dock_object_bind (requestor, object->master);

    if (requestor->master != object->master) {
        g_warning (_("Cannot dock %p to %p because they belong to different masters"),
                   requestor, object);
        return;
    }

    /* first, see if we can optimize things by reordering */
    if (position != EGG_DOCK_NONE) {
        parent = egg_dock_object_get_parent_object (object);
        if (egg_dock_object_reorder (object, requestor, position, other_data) ||
            (parent && egg_dock_object_reorder (parent, requestor, position, other_data)))
            return;
    }
    
    /* freeze the object, since under some conditions it might be destroyed when
       detaching the requestor */
    egg_dock_object_freeze (object);

    /* detach the requestor before docking */
    g_object_ref (requestor);
    if (EGG_DOCK_OBJECT_ATTACHED (requestor))
        egg_dock_object_detach (requestor, FALSE);
    
    if (position != EGG_DOCK_NONE)
        g_signal_emit (object, egg_dock_object_signals [DOCK], 0,
                       requestor, position, other_data);

    g_object_unref (requestor);
    egg_dock_object_thaw (object);
}

void
egg_dock_object_bind (EggDockObject *object,
                      GObject       *master)
{
    g_return_if_fail (object != NULL && master != NULL);
    g_return_if_fail (EGG_IS_DOCK_MASTER (master));
    
    if (object->master == master)
        /* nothing to do here */
        return;
    
    if (object->master) {
        g_warning (_("Attempt to bind to %p an already bound dock object %p "
                     "(current master: %p)"), master, object, object->master);
        return;
    }

    egg_dock_master_add (EGG_DOCK_MASTER (master), object);
    object->master = master;
    g_object_add_weak_pointer (master, (gpointer *) &object->master);

    g_object_notify (G_OBJECT (object), "master");
}

void
egg_dock_object_unbind (EggDockObject *object)
{
    g_return_if_fail (object != NULL);

    g_object_ref (object);

    /* detach the object first */
    if (EGG_DOCK_OBJECT_ATTACHED (object))
        egg_dock_object_detach (object, TRUE);
    
    if (object->master) {
        GObject *master = object->master;
        g_object_remove_weak_pointer (master, (gpointer *) &object->master);
        object->master = NULL;
        egg_dock_master_remove (EGG_DOCK_MASTER (master), object);
        g_object_notify (G_OBJECT (object), "master");
    }
    g_object_unref (object);
}

gboolean
egg_dock_object_is_bound (EggDockObject *object)
{
    g_return_val_if_fail (object != NULL, FALSE);
    return (object->master != NULL);
}

gboolean
egg_dock_object_reorder (EggDockObject    *object,
                         EggDockObject    *child,
                         EggDockPlacement  new_position,
                         GValue           *other_data)
{
    g_return_val_if_fail (object != NULL && child != NULL, FALSE);

    return EGG_CALL_VIRTUAL_WITH_DEFAULT (object,
                                          EGG_DOCK_OBJECT_GET_CLASS,
                                          reorder,
                                          (object, child, new_position, other_data),
                                          FALSE);
}

void 
egg_dock_object_present (EggDockObject *object,
                         EggDockObject *child)
{
    EggDockObject *parent;
    
    g_return_if_fail (object != NULL && EGG_IS_DOCK_OBJECT (object));

    parent = egg_dock_object_get_parent_object (object);
    if (parent)
        /* chain the call to our parent */
        egg_dock_object_present (parent, object);

    EGG_CALL_VIRTUAL (object, EGG_DOCK_OBJECT_GET_CLASS, present, (object, child));
}

/**
 * egg_dock_object_child_placement:
 * @object: the dock object we are asking for child placement
 * @child: the child of the @object we want the placement for
 * @placement: where to return the placement information
 *
 * This function returns information about placement of a child dock
 * object inside another dock object.  The function returns %TRUE if
 * @child is effectively a child of @object.  @placement should
 * normally be initially setup to %EGG_DOCK_NONE.  If it's set to some
 * other value, this function will not touch the stored value if the
 * specified placement is "compatible" with the actual placement of
 * the child.
 *
 * @placement can be %NULL, in which case the function simply tells if
 * @child is attached to @object.
 *
 * Returns: %TRUE if @child is a child of @object.
 */
gboolean 
egg_dock_object_child_placement (EggDockObject    *object,
                                 EggDockObject    *child,
                                 EggDockPlacement *placement)
{
    g_return_val_if_fail (object != NULL && child != NULL, FALSE);

    /* simple case */
    if (!egg_dock_object_is_compound (object))
        return FALSE;
    
    return EGG_CALL_VIRTUAL_WITH_DEFAULT (object, EGG_DOCK_OBJECT_GET_CLASS,
                                          child_placement,
                                          (object, child, placement),
                                          FALSE);
}


/* ----- dock param type functions start here ------ */

static void 
egg_dock_param_export_int (const GValue *src,
                           GValue       *dst)
{
    dst->data [0].v_pointer = g_strdup_printf ("%d", src->data [0].v_int);
}

static void 
egg_dock_param_export_uint (const GValue *src,
                            GValue       *dst)
{
    dst->data [0].v_pointer = g_strdup_printf ("%u", src->data [0].v_uint);
}

static void 
egg_dock_param_export_string (const GValue *src,
                              GValue       *dst)
{
    dst->data [0].v_pointer = g_strdup (src->data [0].v_pointer);
}

static void 
egg_dock_param_export_bool (const GValue *src,
                            GValue       *dst)
{
    dst->data [0].v_pointer = g_strdup_printf ("%s", src->data [0].v_int ? "yes" : "no");
}

static void 
egg_dock_param_export_placement (const GValue *src,
                                 GValue       *dst)
{
    switch (src->data [0].v_int) {
        case EGG_DOCK_NONE:
            dst->data [0].v_pointer = g_strdup ("");
            break;
        case EGG_DOCK_TOP:
            dst->data [0].v_pointer = g_strdup ("top");
            break;
        case EGG_DOCK_BOTTOM:
            dst->data [0].v_pointer = g_strdup ("bottom");
            break;
        case EGG_DOCK_LEFT:
            dst->data [0].v_pointer = g_strdup ("left");
            break;
        case EGG_DOCK_RIGHT:
            dst->data [0].v_pointer = g_strdup ("right");
            break;
        case EGG_DOCK_CENTER:
            dst->data [0].v_pointer = g_strdup ("center");
            break;
        case EGG_DOCK_FLOATING:
            dst->data [0].v_pointer = g_strdup ("floating");
            break;
    }
}

static void 
egg_dock_param_import_int (const GValue *src,
                           GValue       *dst)
{
    dst->data [0].v_int = atoi (src->data [0].v_pointer);
}

static void 
egg_dock_param_import_uint (const GValue *src,
                            GValue       *dst)
{
    dst->data [0].v_uint = (guint) atoi (src->data [0].v_pointer);
}

static void 
egg_dock_param_import_string (const GValue *src,
                              GValue       *dst)
{
    dst->data [0].v_pointer = g_strdup (src->data [0].v_pointer);
}

static void 
egg_dock_param_import_bool (const GValue *src,
                            GValue       *dst)
{
    dst->data [0].v_int = !strcmp (src->data [0].v_pointer, "yes");
}

static void 
egg_dock_param_import_placement (const GValue *src,
                                 GValue       *dst)
{
    if (!strcmp (src->data [0].v_pointer, "top"))
        dst->data [0].v_int = EGG_DOCK_TOP;
    else if (!strcmp (src->data [0].v_pointer, "bottom"))
        dst->data [0].v_int = EGG_DOCK_BOTTOM;
    else if (!strcmp (src->data [0].v_pointer, "center"))
        dst->data [0].v_int = EGG_DOCK_CENTER;
    else if (!strcmp (src->data [0].v_pointer, "left"))
        dst->data [0].v_int = EGG_DOCK_LEFT;
    else if (!strcmp (src->data [0].v_pointer, "right"))
        dst->data [0].v_int = EGG_DOCK_RIGHT;
    else if (!strcmp (src->data [0].v_pointer, "floating"))
        dst->data [0].v_int = EGG_DOCK_FLOATING;
    else
        dst->data [0].v_int = EGG_DOCK_NONE;
}

GType
egg_dock_param_get_type (void)
{
    static GType our_type = 0;

    if (our_type == 0) {
        GTypeInfo tinfo = { 0, };
        our_type = g_type_register_static (G_TYPE_STRING, "EggDockParam", &tinfo, 0);

        /* register known transform functions */
        /* exporters */
        g_value_register_transform_func (G_TYPE_INT, our_type, egg_dock_param_export_int);
        g_value_register_transform_func (G_TYPE_UINT, our_type, egg_dock_param_export_uint);
        g_value_register_transform_func (G_TYPE_STRING, our_type, egg_dock_param_export_string);
        g_value_register_transform_func (G_TYPE_BOOLEAN, our_type, egg_dock_param_export_bool);
        g_value_register_transform_func (EGG_TYPE_DOCK_PLACEMENT, our_type, egg_dock_param_export_placement);
        /* importers */
        g_value_register_transform_func (our_type, G_TYPE_INT, egg_dock_param_import_int);
        g_value_register_transform_func (our_type, G_TYPE_UINT, egg_dock_param_import_uint);
        g_value_register_transform_func (our_type, G_TYPE_STRING, egg_dock_param_import_string);
        g_value_register_transform_func (our_type, G_TYPE_BOOLEAN, egg_dock_param_import_bool);
        g_value_register_transform_func (our_type, EGG_TYPE_DOCK_PLACEMENT, egg_dock_param_import_placement);
    }

    return our_type;
}

/* -------------- nick <-> type conversion functions --------------- */

static GRelation *dock_register = NULL;

enum {
    INDEX_NICK = 0,
    INDEX_TYPE
};

static void
egg_dock_object_register_init (void)
{
    if (dock_register)
        return;
    
    /* FIXME: i don't know if GRelation is efficient */
    dock_register = g_relation_new (2);
    g_relation_index (dock_register, INDEX_NICK, g_str_hash, g_str_equal);
    g_relation_index (dock_register, INDEX_TYPE, g_direct_hash, g_direct_equal);

    /* add known types */
    g_relation_insert (dock_register, "dock", (gpointer) EGG_TYPE_DOCK);
    g_relation_insert (dock_register, "item", (gpointer) EGG_TYPE_DOCK_ITEM);
    g_relation_insert (dock_register, "paned", (gpointer) EGG_TYPE_DOCK_PANED);
    g_relation_insert (dock_register, "box", (gpointer) EGG_TYPE_DOCK_BOX);
    g_relation_insert (dock_register, "notebook", (gpointer) EGG_TYPE_DOCK_NOTEBOOK);
    g_relation_insert (dock_register, "placeholder", (gpointer) EGG_TYPE_DOCK_PLACEHOLDER);
}

G_CONST_RETURN gchar *
egg_dock_object_nick_from_type (GType type)
{
    GTuples *tuples;
    gchar *nick = NULL;
    
    if (!dock_register)
        egg_dock_object_register_init ();

    if (g_relation_count (dock_register, (gpointer) type, INDEX_TYPE) > 0) {
        tuples = g_relation_select (dock_register, (gpointer) type, INDEX_TYPE);
        nick = (gchar *) g_tuples_index (tuples, 0, INDEX_NICK);
        g_tuples_destroy (tuples);
    }
    
    return nick ? nick : g_type_name (type);
}

GType
egg_dock_object_type_from_nick (const gchar *nick)
{
    GTuples *tuples;
    GType type = G_TYPE_NONE;
    
    if (!dock_register)
        egg_dock_object_register_init ();

    if (g_relation_count (dock_register, (gpointer) nick, INDEX_NICK) > 0) {
        tuples = g_relation_select (dock_register, (gpointer) nick, INDEX_NICK);
        type = (GType) g_tuples_index (tuples, 0, INDEX_TYPE);
        g_tuples_destroy (tuples);
    }
    else {
        /* try searching in the glib type system */
        type = g_type_from_name (nick);
    }
    
    return type;
}

GType
egg_dock_object_set_type_for_nick (const gchar *nick,
                                   GType        type)
{
    GType old_type = G_TYPE_NONE;
    
    if (!dock_register)
        egg_dock_object_register_init ();

    g_return_val_if_fail (g_type_is_a (type, EGG_TYPE_DOCK_OBJECT), G_TYPE_NONE);
    
    if (g_relation_count (dock_register, (gpointer) nick, INDEX_NICK) > 0) {
        old_type = egg_dock_object_type_from_nick (nick);
        g_relation_delete (dock_register, (gpointer) nick, INDEX_NICK);
    }
    
    g_relation_insert (dock_register, nick, type);

    return old_type;
}
