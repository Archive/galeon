/*
 *  Copyright (C) 2003 Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "galeon-action-new.h"
#include "galeon-shell.h"
#include "eel-gconf-extensions.h"
#include "prefs-strings.h"
#include "gul-toolbutton.h"
#include "gul-string.h"
#include "gul-gui.h"
#include "galeon-dnd.h"

#include <gtk/gtkstock.h>
#include <gtk/gtkimagemenuitem.h>
#include <gtk/gtkclipboard.h>
#include <glib/gi18n.h>
#include <string.h>

static void		galeon_action_new_finalize		(GObject *object);
static void		galeon_action_new_connect_proxy		(GtkAction *action, GtkWidget *proxy);
static void		galeon_action_new_activate		(GtkAction *action);



G_DEFINE_TYPE (GaleonActionNew, galeon_action_new, GALEON_TYPE_ACTION);


static void
galeon_action_new_class_init (GaleonActionNewClass *class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);
	GtkActionClass *action_class = GTK_ACTION_CLASS (class);

	object_class->finalize = galeon_action_new_finalize;

	action_class->connect_proxy = galeon_action_new_connect_proxy;
	action_class->menu_item_type    = GTK_TYPE_IMAGE_MENU_ITEM;
	action_class->toolbar_item_type = GUL_TYPE_TOOLBUTTON;
	action_class->activate          = galeon_action_new_activate;

}

static void
galeon_action_new_init (GaleonActionNew *action)
{
	g_object_set (G_OBJECT (action), "stock_id", GTK_STOCK_NEW, NULL);
	g_object_set (G_OBJECT (action), "tooltip", _("Open a new tab or window"), NULL);
}

static void
galeon_action_new_finalize (GObject *object)
{
	G_OBJECT_CLASS (galeon_action_new_parent_class)->finalize (object);
}

static void
galeon_action_new_activate (GtkAction *action)
{
	GaleonWindow *window = galeon_action_get_window (GALEON_ACTION (action));
	GaleonTab *tab;

	g_return_if_fail (GALEON_IS_WINDOW (window));

	tab = galeon_window_get_active_tab (window);
	
	galeon_shell_new_tab (galeon_shell, window, tab, NULL, 
			      GALEON_NEW_TAB_HOMEPAGE
			      | GALEON_NEW_TAB_JUMP
			      | GALEON_NEW_TAB_APPEND);
}

static void
each_url_receive_data_binder (const char * url, const char * title, 
			      gpointer data )
{
	char ** urlp = data;
	if (*urlp == NULL)
	{
		*urlp = g_strdup (url);
	}
}	

static void
galeon_action_new_drag_data_received_cb (GtkWidget* widget, 
					 GdkDragContext *dc,
					 gint x, gint y, 
					 GtkSelectionData *selection_data,
					 guint info, guint time, 
					 GaleonActionNew *b)
{
	GaleonWindow *window = galeon_action_get_window (GALEON_ACTION (b));
	char *url = NULL;

	g_return_if_fail (GALEON_IS_WINDOW (window));

	galeon_dnd_drag_data_receive (widget, dc, x, y, selection_data,
				      info, time, &url, each_url_receive_data_binder);
	
	if (url)
	{
		GaleonTab *tab;
		
		tab = galeon_window_get_active_tab (window);
		
		galeon_shell_new_tab (galeon_shell, window, tab, url, 0);
		g_free (url);
	}
}

static void
galeon_action_new_new_window_activate_cb (GtkMenuItem *mi, GaleonActionNew *b)
{
	GaleonWindow *window = galeon_action_get_window (GALEON_ACTION (b));
	GaleonTab *tab;

	g_return_if_fail (GALEON_IS_WINDOW (window));

	tab = galeon_window_get_active_tab (window);
	
	galeon_shell_new_tab (galeon_shell, window, tab, NULL, 
			      GALEON_NEW_TAB_HOMEPAGE
			      | GALEON_NEW_TAB_JUMP
			      | GALEON_NEW_TAB_IN_NEW_WINDOW);
}

static void
galeon_action_new_new_tab_activate_cb (GtkMenuItem *mi, GaleonActionNew *b)
{
	GaleonWindow *window = galeon_action_get_window (GALEON_ACTION (b));
	GaleonTab *tab;

	g_return_if_fail (GALEON_IS_WINDOW (window));

	tab = galeon_window_get_active_tab (window);
	
	galeon_shell_new_tab (galeon_shell, window, tab, NULL, 
			      GALEON_NEW_TAB_HOMEPAGE
			      | GALEON_NEW_TAB_JUMP
			      | GALEON_NEW_TAB_IN_EXISTING_WINDOW
			      | GALEON_NEW_TAB_APPEND);
}

static gboolean
galeon_action_new_button_press_event_cb (GtkWidget *widget, GdkEventButton *event, 
					 GaleonActionNew *b)
{
	GaleonWindow *window = galeon_action_get_window (GALEON_ACTION (b));

	if (event->button == 2 && GALEON_IS_WINDOW (window))
	{
		static GtkClipboard *cb = NULL;
		gchar *url;

		if (!cb)
		{
			cb = gtk_clipboard_get (GDK_SELECTION_PRIMARY);
		}
		
		url = gtk_clipboard_wait_for_text (cb);

		if (url)
		{
			GaleonTab *tab = galeon_window_get_active_tab (window);
			galeon_shell_new_tab (galeon_shell, window, tab, url, 0);
			g_free (url);
		}
		return TRUE;
	}
	
	return FALSE;
}

static void
galeon_action_new_connect_proxy (GtkAction *a, GtkWidget *proxy)
{
	GtkWidget *widget = proxy;

	if (GUL_IS_TOOLBUTTON (proxy))
	{
		GtkMenuShell *ms;
		widget = gul_toolbutton_get_button (GUL_TOOLBUTTON (proxy));

		galeon_dnd_url_drag_dest_set (proxy);
		g_signal_connect (proxy, "drag_data_received",
				  G_CALLBACK(galeon_action_new_drag_data_received_cb), a);

		ms = gul_toolbutton_get_menu (GUL_TOOLBUTTON (proxy));
	
		gul_gui_append_new_menuitem (GTK_WIDGET (ms), _("New _Window"), 
					     G_CALLBACK (galeon_action_new_new_window_activate_cb), a);
		gul_gui_append_new_menuitem (GTK_WIDGET (ms), _("New _Tab"), 
					     G_CALLBACK (galeon_action_new_new_tab_activate_cb), a);
	}

	g_signal_connect (widget, "button-press-event",
			  G_CALLBACK (galeon_action_new_button_press_event_cb), a);

	(* GTK_ACTION_CLASS (galeon_action_new_parent_class)->connect_proxy) (a, proxy);
}

