/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __galeon_autocompletion_h__
#define __galeon_autocompletion_h__

#include <glib-object.h>
#include "galeon-autocompletion-source.h"

/* object forward declarations */

typedef struct _GaleonAutocompletion GaleonAutocompletion;
typedef struct _GaleonAutocompletionClass GaleonAutocompletionClass;
typedef struct _GaleonAutocompletionPrivate GaleonAutocompletionPrivate;
typedef struct _GaleonAutocompletionMatch GaleonAutocompletionMatch;

/**
 * GaleonAutocompletion object
 */

#define GALEON_TYPE_AUTOCOMPLETION		(galeon_autocompletion_get_type())
#define GALEON_AUTOCOMPLETION(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), \
						 GALEON_TYPE_AUTOCOMPLETION,\
						 GaleonAutocompletion))
#define GALEON_AUTOCOMPLETION_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), \
						 GALEON_TYPE_AUTOCOMPLETION,\
						 GaleonAutocompletionClass))
#define GALEON_IS_AUTOCOMPLETION(object)	(G_TYPE_CHECK_INSTANCE_TYPE((object), \
						 GALEON_TYPE_AUTOCOMPLETION))
#define GALEON_IS_AUTOCOMPLETION_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), \
						 GALEON_TYPE_AUTOCOMPLETION))
#define GALEON_AUTOCOMPLETION_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), \
						 GALEON_TYPE_AUTOCOMPLETION,\
						 GaleonAutocompletionClass))

struct _GaleonAutocompletionClass 
{
	GObjectClass parent_class;

	/* signals */
	void	(*sources_changed)	(GaleonAutocompletion *ac);
};

/* Remember: fields are public read-only */
struct _GaleonAutocompletion
{
	GObject parent_object;

	GaleonAutocompletionPrivate *priv;
};

struct _GaleonAutocompletionMatch 
{
	const gchar *match;
	const gchar *title;
	guint offset;
	gint32 score;
};

/* this is a set of usual prefixes for web browsing */
#define GALEON_AUTOCOMPLETION_USUAL_WEB_PREFIXES \
	"http://www.", \
	"http://", \
	"https://www.", \
	"https://", \
	"file://", \
	"www." 

GType				galeon_autocompletion_get_type		(void);
GaleonAutocompletion *		galeon_autocompletion_new		(void);
void				galeon_autocompletion_add_source	(GaleonAutocompletion *ac,
									 GaleonAutocompletionSource *s);
void				galeon_autocompletion_set_prefixes	(GaleonAutocompletion *ac,
									 const gchar **prefixes);
void				galeon_autocompletion_set_key		(GaleonAutocompletion *ac, 
									 const gchar *key);
gchar *				galeon_autocompletion_get_common_prefix	(GaleonAutocompletion *ac);
const GaleonAutocompletionMatch *galeon_autocompletion_get_matches	(GaleonAutocompletion *ac);
const GaleonAutocompletionMatch *galeon_autocompletion_get_matches_sorted_by_score 
									(GaleonAutocompletion *ac);
guint				galeon_autocompletion_get_num_matches	(GaleonAutocompletion *ac);

#endif
