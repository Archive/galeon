/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GLOBAL_HISTORY_H
#define GLOBAL_HISTORY_H

#include <glib-object.h>

G_BEGIN_DECLS

typedef struct GlobalHistory GlobalHistory;
typedef struct GlobalHistoryClass GlobalHistoryClass;

#define TYPE_GLOBAL_HISTORY             (global_history_get_type ())
#define GLOBAL_HISTORY(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_GLOBAL_HISTORY, \
					 GlobalHistory))
#define GLOBAL_HISTORY_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GLOBAL_HISTORY, \
					 GlobalHistoryClass))
#define IS_GLOBAL_HISTORY(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_GLOBAL_HISTORY))
#define IS_GLOBAL_HISTORY_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GLOBAL_HISTORY))
#define GLOBAL_HISTORY_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_GLOBAL_HISTORY, \
					 GlobalHistoryClass))

typedef struct GlobalHistoryPrivate GlobalHistoryPrivate;
typedef struct _HistoryItem HistoryItem;
typedef struct HistoryItemPrivate HistoryItemPrivate;

#define CONF_HISTORY_EXPIRE "/apps/galeon/Browsing/History/expire"

struct GlobalHistory
{
        GObject parent;
        GlobalHistoryPrivate *priv;
};

struct GlobalHistoryClass
{
        GObjectClass parent_class;

	void (* add) (HistoryItem *item);
	void (* remove) (HistoryItem *item);
	void (* update) (HistoryItem *item);
};

/**
 * HistoryItem: an item in the history
 */
struct _HistoryItem
{
	char *url;
	char *title;
	GTime first;
	GTime last;
	int visits;
	guint flags; /* Opaque renderer hint flags */

	HistoryItemPrivate *priv;
};

typedef struct
{
	const char *word;
	int type;
} HistoryFilter;

GType          global_history_get_type           (void);

GlobalHistory *global_history_new                (void);

void           global_history_visited            (GlobalHistory *gh,
						  const char *url,
						  gboolean redirect,
						  gboolean toplevel);

gboolean       global_history_is_visited         (GlobalHistory *gh,
						  const char *url);

const char    *global_history_get_last_page      (GlobalHistory *gh);

gboolean       global_history_set_page_title     (GlobalHistory *gh,
						  const char *url, 
						  const char *title);

const char    *global_history_get_page_title     (GlobalHistory *gh,
						  const char *url);

gboolean       global_history_set_page_flags     (GlobalHistory *gh,
                                                  const char *url,
                                                  guint flags);

guint          global_history_get_page_flags     (GlobalHistory *gh,
                                                  const char *url);

gboolean       global_history_set_host_zoom      (GlobalHistory *gh,
						  const char *url,
						  gint zoom);

gint           global_history_get_host_zoom      (GlobalHistory *gh,
						  const char *url);

gboolean       global_history_remove_url         (GlobalHistory *gh,
						  const char *url);

gboolean       global_history_remove_host        (GlobalHistory *gh,
						  const char *host);

void           global_history_clear              (GlobalHistory *gh);

GList         *global_history_get_host_list      (GlobalHistory *gh);

GList	      *global_history_get_urls_list	 (GlobalHistory *gh,
						  HistoryItem *item,
						  HistoryFilter *filter);

HistoryItem   *global_history_get_host_from_site (GlobalHistory *gh,
						  HistoryItem *site); 

void 	       global_history_save_if_needed	 (GlobalHistory *gh);

G_END_DECLS

#endif
