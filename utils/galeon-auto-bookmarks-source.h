/* -*- mode: c; c-style: k&r; c-basic-offset: 8 -*- */
/* 
 *  Copyright (C) 2002  Ricardo Fernándezs Pascual <ric@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GALEON_AUTO_BOOKMARKS_SOURCE_H__
#define __GALEON_AUTO_BOOKMARKS_SOURCE_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define GALEON_TYPE_AUTO_BOOKMARKS_SOURCE		(galeon_auto_bookmarks_source_get_type ())
#define GALEON_AUTO_BOOKMARKS_SOURCE(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), \
							 GALEON_TYPE_AUTO_BOOKMARKS_SOURCE, \
						 	 GaleonAutoBookmarksSource))
#define GALEON_IS_AUTO_BOOKMARKS_SOURCE(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
							 GALEON_TYPE_AUTO_BOOKMARKS_SOURCE))
#define GALEON_AUTO_BOOKMARKS_SOURCE_GET_IFACE(obj)	(G_TYPE_INSTANCE_GET_INTERFACE ((obj), \
							 GALEON_TYPE_AUTO_BOOKMARKS_SOURCE, \
							 GaleonAutoBookmarksSourceIface))

typedef struct _GaleonAutoBookmarksSource	GaleonAutoBookmarksSource;
typedef struct _GaleonAutoBookmarksSourceIface	GaleonAutoBookmarksSourceIface;

typedef gboolean (*GaleonAutoBookmarksSourceIteratorFunc) (const gchar *title, const gchar *url, gpointer data);
typedef gboolean (*GaleonAutoBookmarksSourceFilterFunc) (const gchar *title, const gchar *url, gpointer data);

typedef enum
{
	GALEON_AUTO_BOOKMARKS_SCORING_RECENTLY_VISITED,
	GALEON_AUTO_BOOKMARKS_SCORING_FRECUENTLY_VISITED,
	GALEON_AUTO_BOOKMARKS_SCORING_BOTH,
} GaleonAutoBookmarksScoringMethod;

struct _GaleonAutoBookmarksSourceIface
{
	GTypeInterface g_iface;
	
	/* Virtual Table */
	void		(* get_autobookmarks)	(GaleonAutoBookmarksSource *source, 
						 GaleonAutoBookmarksSourceIteratorFunc iterator,
						 gpointer iterator_data,
						 GaleonAutoBookmarksSourceFilterFunc filter,
						 gpointer filter_data,
						 GaleonAutoBookmarksScoringMethod scoring,
						 gboolean group_by_host);
};

GType	galeon_auto_bookmarks_source_get_type		(void);
void	galeon_auto_bookmarks_source_get_autobookmarks	(GaleonAutoBookmarksSource *source, 
							 GaleonAutoBookmarksSourceIteratorFunc iterator,
							 gpointer iterator_data,
							 GaleonAutoBookmarksSourceFilterFunc filter,
							 gpointer filter_data,
							 GaleonAutoBookmarksScoringMethod scoring,
							 gboolean group_by_host);

G_END_DECLS

#endif

