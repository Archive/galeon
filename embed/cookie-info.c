/*
 * cookie-info.c
 *
 * Copyright (C) 2003 Tommi Komulainen <tommi.komulainen@iki.fi>
 * Available under the terms of the GNU General Public License version 2.
 */ 

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cookie-info.h"
#include "gul-ellipsizing-label.h"
#include <gtk/gtktable.h>
#include <glib/gi18n.h>

void
cookie_info_free	(CookieInfo *info)
{
	g_free (info->domain);
	g_free (info->name);
	g_free (info->value);
	g_free (info->path);
	g_free (info->expire);
	g_free (info);
}

static void
set_table_row (GtkWidget *table, int row, const char *title, GtkWidget *label)
{
	GtkWidget *header;
	char buf[64];

	g_snprintf (buf, sizeof(buf), "<b>%s:</b>", title);
	header = gtk_label_new (buf);
	gtk_label_set_use_markup (GTK_LABEL(header), TRUE);
	gtk_misc_set_alignment (GTK_MISC(header), 0, 0);
	gtk_widget_show (header);
	gtk_table_attach (GTK_TABLE (table), header, 0, 1, row, row+1,
			  GTK_FILL, GTK_FILL, 0, 0);
	
	gtk_misc_set_alignment (GTK_MISC(label), 0, 0);
	gtk_label_set_selectable (GTK_LABEL (label), TRUE);
	gtk_widget_show (label);
	gtk_table_attach_defaults (GTK_TABLE (table), label, 1, 2, row, row+1);
}

GtkWidget *
cookie_info_table_new (const CookieInfo *cookie)
{
	GtkWidget *table;
	GtkWidget *label;

	table = gtk_table_new (2, 5, FALSE);
	gtk_container_set_border_width (GTK_CONTAINER(table), 6);
	gtk_table_set_row_spacings (GTK_TABLE(table), 6);
	gtk_table_set_col_spacings (GTK_TABLE(table), 6);
	gtk_widget_show (table);
	
	label = gul_ellipsizing_label_new (cookie->name);
	set_table_row (table, 0, _("Name"), label);

	label = gul_ellipsizing_label_new (cookie->value);
	set_table_row (table, 1, _("Value"), label);

	label = gtk_label_new (cookie->path);
	set_table_row (table, 2, _("Path"), label);

	label = gtk_label_new (cookie->secure ? _("Yes") : _("No"));
	set_table_row (table, 3, _("Secure"), label);

	label = gtk_label_new (cookie->expire);
	set_table_row (table, 4, _("Expires"), label);

	return table;
}

