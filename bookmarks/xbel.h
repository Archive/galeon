/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __xbel_h
#define __xbel_h

#include <glib.h>
#include <glib-object.h>
#include "bookmarks-io.h"
#include "bookmarks.h"

/* object forward declarations */

typedef struct _GbXBEL GbXBEL;
typedef struct _GbXBELClass GbXBELClass;

/**
 * XBEL object
 */

#define GB_TYPE_XBEL			(gb_xbel_get_type())
#define GB_XBEL(object)			(G_TYPE_CHECK_INSTANCE_CAST((object), GB_TYPE_XBEL, GbXBEL))
#define GB_XBEL_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), GB_TYPE_XBEL, GbXBELClass))
#define GB_IS_XBEL(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), GB_TYPE_XBEL))
#define GB_IS_XBEL_CLASS(klass)		(G_TYPE_CHECK_CLASS_TYPE((klass), GB_TYPE_XBEL))
#define GB_XBEL_GET_CLASS(obj)		(G_TYPE_INSTANCE_GET_CLASS((obj), GB_TYPE_XBEL, GbXBELClass))

struct _GbXBELClass 
{
	GbIOClass parent_class;
};

struct _GbXBEL
{
	GbIO parent_object;
};

GType		gb_xbel_get_type		(void);
GbXBEL *	gb_xbel_new			(void);

#endif
