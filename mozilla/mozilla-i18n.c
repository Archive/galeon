/*
 *  Copyright (C) 2000 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mozilla-i18n.h"

#include <glib/gi18n.h>

const FontLangInfo font_languages[] = {
	{ N_("Arabic"),              "ar"             },
	{ N_("Greek"),               "el"             },
	{ N_("Hebrew"),              "he"             },
	{ N_("Japanese"),            "ja"             },
	{ N_("Korean"),              "ko"             },
	{ N_("Thai"),                "th"             },
	{ N_("Turkish"),             "tr"             },
	{ N_("Baltic"),              "x-baltic"       },
	{ N_("Central European"),    "x-central-euro" },
	{ N_("Cyrillic"),            "x-cyrillic"     },
	{ N_("Devanagari"),	     "x-devanagari"   },
	{ N_("Tamil"),               "x-tamil"        },
	{ N_("Unicode"),             "x-unicode"      },
	{ N_("User Defined"),        "x-user-def"     },
	{ N_("Western"),             "x-western"      },
	{ N_("Simplified Chinese"),  "zh-CN"          },
	{ N_("Traditional Chinese"), "zh-TW"          },
};

const int num_font_languages = G_N_ELEMENTS(font_languages);
