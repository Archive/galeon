/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __bookmarks_clipboard_h
#define __bookmarks_clipboard_h

#include <glib-object.h>
#include <bookmarks.h>

/* object forward declarations */

typedef struct _GbClipboard GbClipboard;
typedef struct _GbClipboardClass GbClipboardClass;
typedef struct _GbClipboardPrivate GbClipboardPrivate;

/**
 * Editor object
 */

#define GB_TYPE_CLIPBOARD		      	(gb_clipboard_get_type())
#define GB_CLIPBOARD(object)			(G_TYPE_CHECK_INSTANCE_CAST((object), GB_TYPE_CLIPBOARD,\
						 GbClipboard))
#define GB_CLIPBOARD_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), GB_TYPE_CLIPBOARD,\
						 GbClipboardClass))
#define GB_IS_CLIPBOARD(object)			(G_TYPE_CHECK_INSTANCE_TYPE((object), GB_TYPE_CLIPBOARD))
#define GB_IS_CLIPBOARD_CLASS(klass)		(G_TYPE_CHECK_CLASS_TYPE((klass), GB_TYPE_CLIPBOARD))
#define GB_CLIPBOARD_GET_CLASS(obj)		(G_TYPE_INSTANCE_GET_CLASS((obj), GB_TYPE_CLIPBOARD,\
						 GbClipboardClass))

struct _GbClipboardClass 
{
	GObjectClass parent_class;
	

};

/* Remember: fields are public read-only */
struct _GbClipboard
{
	GObject parent_object;

	GbClipboardPrivate *priv;
};

GType			gb_clipboard_get_type		(void);
GbClipboard *		gb_clipboard_new		(void);
GbClipboard *		gb_clipboard_get_shared		(void);
void 			gb_clipboard_clear		(GbClipboard *cb);
void 			gb_clipboard_add		(GbClipboard *cb, GbBookmark *b);
gboolean		gb_clipboard_can_paste		(GbClipboard *cb);
GbBookmarkSet *		gb_clipboard_paste		(GbClipboard *cb);

#endif

