/* -*- mode: c c-style: k&r c-basic-offset: 8 -*- */
/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/* undefine for gnome-file-entry / gnome-pixmap-entry */
#undef GTK_DISABLE_DEPRECATED
#undef GNOME_DISABLE_DEPRECATED

#include <glib/gi18n.h>
#include "gul-glade.h"
#include "bookmarks-single-editor.h"
#include "gul-string.h"
#include "gul-state.h"
#include "eel-gconf-extensions.h"
#include "prefs-strings.h"
#include <gtk/gtktextbuffer.h>
#include <gtk/gtktextview.h>
#include <gtk/gtktogglebutton.h>
#include <gtk/gtkspinbutton.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkcomboboxentry.h>
#include <libgnomeui/gnome-pixmap-entry.h>
#include "bookmarks-tree-view.h"
#include "hig-alert.h"

#include <gdk/gdkkeysyms.h>
#include <string.h>
#include <time.h>

/**
 * Private data
 */
#define GB_SINGLE_EDITOR_GET_PRIVATE(object) (G_TYPE_INSTANCE_GET_PRIVATE ((object), \
				       GB_TYPE_SINGLE_EDITOR, GbSingleEditorPrivate))


struct _GbSingleEditorPrivate {
	GbBookmark *bm;

	gboolean widgets_and_signals_inited;
	guint no_update;

	GbSingleEditorDisablePolicy disable_policy;

	GtkWidget *window;
	GtkWidget *parents_scrolledwindow;
	GbTreeView *parents_treeview;
	GbTreeModel *parents_treemodel;
	GtkWidget *alias_label;
	GtkWidget *smart_entry_width_hbox;

	GtkWidget *close_button;
};

/**
 * Private functions, only availble from this file
 */
static void		gb_single_editor_init_widgets_impl	(GbSingleEditor *e);
static void		gb_single_editor_bookmark_changed_cb	(GbBookmark *b, GbSingleEditor *e);
static void		gb_single_editor_bookmark_replaced_cb	(GbBookmark *b, GbBookmark *replacement, 
								 GbSingleEditor *e);
static void		gb_single_editor_finalize_impl		(GObject *o);
static void		gb_single_editor_update_controls	(GbSingleEditor *e);
static void		gb_single_editor_set_bookmark_internal	(GbSingleEditor *e, GbBookmark *b);


static void		gb_single_editor_name_entry_changed_cb	(GtkWidget *edited,
								 GbSingleEditor *e);
static void		gb_single_editor_smarturl_entry_changed_cb (GtkWidget *edited,
								    GbSingleEditor *e);
static void		gb_single_editor_smart_entry_width_spinbutton_value_changed_cb (GtkWidget *edited, 
											GbSingleEditor *e);
static void		gb_single_editor_location_entry_changed_cb (GtkWidget *edited,
								    GbSingleEditor *e);
static void		gb_single_editor_notes_text_buffer_changed_cb (GtkWidget *edited,
								       GbSingleEditor *e);
static void		gb_single_editor_nicks_entry_changed_cb	(GtkWidget *edited,
								 GbSingleEditor *e);
static void		gb_single_editor_search_text_entry_changed_cb	(GtkWidget *edited,
									 GbSingleEditor *e);
static void		gb_single_editor_create_toolbar_checkbox_toggled_cb (GtkWidget *edited,
									     GbSingleEditor *e);
static void		gb_single_editor_add_to_context_checkbox_toggled_cb (GtkWidget *edited,
									     GbSingleEditor *e);
static void
gb_single_editor_search_recently_visited_checkbox_toggled_cb (GtkWidget *edited, GbSingleEditor *e);
static void
gb_single_editor_search_recently_visited_spin_value_changed_cb (GtkWidget *edited, GbSingleEditor *e);
static void
gb_single_editor_search_recently_created_checkbox_toggled_cb (GtkWidget *edited, GbSingleEditor *e);
static void
gb_single_editor_search_recently_created_spin_value_changed_cb (GtkWidget *edited, GbSingleEditor *e);
static void
gb_single_editor_search_b_f_match_case_toggled_cb (GtkWidget *edited, GbSingleEditor *e);
static void
gb_single_editor_search_b_f_include_folders_toggled_cb (GtkWidget *edited, GbSingleEditor *e);
static void
gb_single_editor_search_b_f_include_sites_toggled_cb (GtkWidget *edited, GbSingleEditor *e);
static void
gb_single_editor_search_b_f_look_in_name_toggled_cb (GtkWidget *edited, GbSingleEditor *e);
static void
gb_single_editor_search_b_f_look_in_location_toggled_cb (GtkWidget *edited, GbSingleEditor *e);
static void
gb_single_editor_search_b_f_look_in_notes_toggled_cb (GtkWidget *edited, GbSingleEditor *e);
static void
gb_single_editor_search_b_f_several_and_toggled_cb (GtkWidget *edited, GbSingleEditor *e);
static void
gb_single_editor_search_b_f_several_or_toggled_cb (GtkWidget *edited, GbSingleEditor *e);
static void
gb_single_editor_search_b_f_several_exact_toggled_cb (GtkWidget *edited, GbSingleEditor *e);

static void 		gb_single_editor_image_pixmapentry_changed_cb (GtkWidget *edited,
								      GbSingleEditor *e);
static void		gb_single_editor_parameter_encoding_combo_changed_cb (GtkWidget *edited,
									      GbSingleEditor *e);
static void		gb_single_editor_autobm_search_text_entry_changed_cb	(GtkWidget *edited,
										 GbSingleEditor *e);
static void
gb_single_editor_autobm_search_b_f_match_case_toggled_cb (GtkWidget *edited, GbSingleEditor *e);
static void
gb_single_editor_autobm_group_by_host_checkbutton_toggled_cb (GtkWidget *edited, GbSingleEditor *e);
static void
gb_single_editor_autobm_search_b_f_look_in_title_toggled_cb (GtkWidget *edited, GbSingleEditor *e);
static void
gb_single_editor_autobm_search_b_f_look_in_url_toggled_cb (GtkWidget *edited, GbSingleEditor *e);
static void
gb_single_editor_autobm_search_b_f_several_and_toggled_cb (GtkWidget *edited, GbSingleEditor *e);
static void
gb_single_editor_autobm_search_b_f_several_or_toggled_cb (GtkWidget *edited, GbSingleEditor *e);
static void
gb_single_editor_autobm_search_b_f_several_exact_toggled_cb (GtkWidget *edited, GbSingleEditor *e);
static void
gb_single_editor_autobm_score_more_recently_used_toggled_cb (GtkWidget *edited, GbSingleEditor *e);
static void
gb_single_editor_autobm_score_more_frequently_used_toggled_cb (GtkWidget *edited, GbSingleEditor *e);
static void
gb_single_editor_autobm_score_both_toggled_cb (GtkWidget *edited, GbSingleEditor *e);



/**
 * SingleEditor object
 */

G_DEFINE_TYPE (GbSingleEditor, gb_single_editor, G_TYPE_OBJECT);

static void
gb_single_editor_class_init (GbSingleEditorClass *klass)
{
	G_OBJECT_CLASS (klass)->finalize = gb_single_editor_finalize_impl;
	klass->init_widgets = gb_single_editor_init_widgets_impl;


	g_type_class_add_private (klass, sizeof (GbSingleEditorPrivate));
}

static void 
gb_single_editor_init (GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = GB_SINGLE_EDITOR_GET_PRIVATE (e);
	e->priv = p;
	p->disable_policy = GB_SINGLE_EDITOR_DISABLE_POLICY_HIDE;
}

static void
gb_single_editor_finalize_impl (GObject *o)
{
	GbSingleEditor *e = GB_SINGLE_EDITOR (o);
	GbSingleEditorPrivate *p = e->priv;

	gb_single_editor_set_bookmark_internal (e, NULL);

	if (p->parents_treemodel)
	{
		g_object_unref (p->parents_treemodel);
	}
	
	if (e->name_label) g_object_unref (e->name_label);
	if (e->name_entry) g_object_unref (e->name_entry);
	if (e->smarturl_label) g_object_unref (e->smarturl_label);
	if (e->smarturl_entry) g_object_unref (e->smarturl_entry);
	if (e->date_added_label) g_object_unref (e->date_added_label);
	if (e->date_added_value_label) g_object_unref (e->date_added_value_label);
	if (e->date_modified_label) g_object_unref (e->date_modified_label);
	if (e->date_modified_value_label) g_object_unref (e->date_modified_value_label);
	if (e->date_visited_label) g_object_unref (e->date_visited_label);
	if (e->date_visited_value_label) g_object_unref (e->date_visited_value_label);
	if (e->smart_entry_width_label) g_object_unref (e->smart_entry_width_label);
	if (e->smart_entry_width_spinbutton) g_object_unref (e->smart_entry_width_spinbutton);
	if (p->smart_entry_width_hbox) g_object_unref (p->smart_entry_width_hbox);
	if (e->parameter_encoding_label) g_object_unref (e->parameter_encoding_label);
	if (e->parameter_encoding_combo) g_object_unref (e->parameter_encoding_combo);
	if (e->location_label) g_object_unref (e->location_label);
	if (e->location_entry) g_object_unref (e->location_entry);
	if (e->notes_textview) g_object_unref (e->notes_textview);
	if (e->nicks_label) g_object_unref (e->nicks_label);
	if (e->nicks_entry) g_object_unref (e->nicks_entry);
	if (e->search_text_entry) g_object_unref (e->search_text_entry);
	if (e->search_recently_visited_checkbox) g_object_unref (e->search_recently_visited_checkbox);
	if (e->search_recently_visited_spin) g_object_unref (e->search_recently_visited_spin);
	if (e->search_recently_created_checkbox) g_object_unref (e->search_recently_created_checkbox);
	if (e->search_recently_created_spin) g_object_unref (e->search_recently_created_spin);
	if (e->search_options) g_object_unref (e->search_options);
	if (e->search_b_f_match_case) g_object_unref (e->search_b_f_match_case);
	if (e->search_b_f_include_folders) g_object_unref (e->search_b_f_include_folders);
	if (e->search_b_f_include_sites) g_object_unref (e->search_b_f_include_sites);
	if (e->search_b_f_look_in_name) g_object_unref (e->search_b_f_look_in_name);
	if (e->search_b_f_look_in_location) g_object_unref (e->search_b_f_look_in_location);
	if (e->search_b_f_look_in_notes) g_object_unref (e->search_b_f_look_in_notes);
	if (e->search_b_f_several_and) g_object_unref (e->search_b_f_several_and);
	if (e->search_b_f_several_or) g_object_unref (e->search_b_f_several_or);
	if (e->search_b_f_several_exact) g_object_unref (e->search_b_f_several_exact);
	if (e->create_toolbar_checkbox) g_object_unref (e->create_toolbar_checkbox);
	if (e->add_to_context_checkbox) g_object_unref (e->add_to_context_checkbox);
	if (e->image_label) g_object_unref (e->image_label);
	if (e->image_pixmapentry) g_object_unref (e->image_pixmapentry);
	if (e->autobm_search_text_label) g_object_unref (e->autobm_search_text_label);
	if (e->autobm_search_text_entry) g_object_unref (e->autobm_search_text_entry);
	if (e->autobm_options) g_object_unref (e->autobm_options);
	if (e->autobm_search_b_f_match_case) g_object_unref (e->autobm_search_b_f_match_case);
	if (e->autobm_group_by_host_checkbutton) g_object_unref (e->autobm_group_by_host_checkbutton);
	if (e->autobm_search_b_f_look_in_title) g_object_unref (e->autobm_search_b_f_look_in_title);
	if (e->autobm_search_b_f_look_in_url) g_object_unref (e->autobm_search_b_f_look_in_url);
	if (e->autobm_search_b_f_several_and) g_object_unref (e->autobm_search_b_f_several_and);
	if (e->autobm_search_b_f_several_or) g_object_unref (e->autobm_search_b_f_several_or);
	if (e->autobm_search_b_f_several_exact) g_object_unref (e->autobm_search_b_f_several_exact);
	if (e->autobm_score_more_recently_used) g_object_unref (e->autobm_score_more_recently_used);
	if (e->autobm_score_more_frequently_used) g_object_unref (e->autobm_score_more_frequently_used);
	if (e->autobm_score_both) g_object_unref (e->autobm_score_both);
	if (p->parents_scrolledwindow) g_object_unref (p->parents_scrolledwindow);
	if (p->alias_label) g_object_unref (p->alias_label);
	if (p->close_button) g_object_unref (p->close_button);

	G_OBJECT_CLASS (gb_single_editor_parent_class)->finalize (o);

}

void
gb_single_editor_init_widgets (GbSingleEditor *e)
{
	GbSingleEditorClass *klass = GB_SINGLE_EDITOR_GET_CLASS (e);
	klass->init_widgets (e);
}

static void
parents_treeview_bookmark_check_toggled (GbTreeView *tv, GbBookmark *b, GbSingleEditor *e)
{
	GbSingleEditorPrivate *p;
	gboolean current;

	g_return_if_fail (GB_IS_SINGLE_EDITOR (e));
	g_return_if_fail (GB_IS_FOLDER (b));
	p = e->priv;
	g_return_if_fail (GB_IS_BOOKMARK (p->bm));

	if (gb_folder_is_autogenerated (GB_FOLDER (b)))
	{
		GtkWidget *dialog = hig_alert_new
			(GTK_WINDOW (p->window),
			 GTK_DIALOG_MODAL,
			 HIG_ALERT_ERROR,
			 _("Autogenerated folder."),
			 NULL,
			 GTK_STOCK_OK,
			 GTK_RESPONSE_OK,
			 NULL);
		
		hig_alert_set_secondary_printf (HIG_ALERT (dialog),
						_("The contents of folder \"%s\" are autogenerated. "
						  "You cannot add or remove bookmarks under it manually."),
						b->name);
		
		gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);
			
		gtk_dialog_run (GTK_DIALOG (dialog));
		gtk_widget_destroy (dialog);
		return;
	}

	current = gb_folder_has_child_or_alias (GB_FOLDER (b), p->bm);
	if (current)
	{
		gb_folder_remove_child_or_aliases (GB_FOLDER (b), p->bm);
	}
	else
	{
		gb_bookmark_ensure_alias_under (p->bm, GB_FOLDER (b));
	}
}

static gboolean
gb_single_editor_close_on_ctrl_enter (GtkWidget *widget, GdkEventKey *event,
				      GbSingleEditor *e)
{
	if ((event->state & GDK_CONTROL_MASK) && 
	    ((event->keyval == GDK_Return) ||  (event->keyval == GDK_KP_Enter)))
	{
		gtk_widget_destroy (e->priv->window);
		return TRUE;
	}
	return FALSE;
}


static void
gb_single_editor_move_to_next (GtkWidget *widget, GbSingleEditor *e)
{
	gtk_widget_child_focus (e->priv->window, GTK_DIR_TAB_FORWARD);
}

static void
gb_single_editor_init_widgets_impl (GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	WidgetLookup lookups[] = {
		{ "bookmarks_properties_window", &p->window },
		{ "bookmarks_properties_name_label", &e->name_label },
		{ "bookmarks_properties_name_entry", &e->name_entry },
		{ "bookmarks_properties_smarturl_label", &e->smarturl_label },
		{ "bookmarks_properties_smarturl_entry", &e->smarturl_entry },
		{ "bookmarks_properties_date_added_label", &e->date_added_label },
		{ "bookmarks_properties_date_added_value_label", &e->date_added_value_label },
		{ "bookmarks_properties_date_modified_label", &e->date_modified_label },
		{ "bookmarks_properties_date_modified_value_label", &e->date_modified_value_label },
		{ "bookmarks_properties_date_visited_label", &e->date_visited_label },
		{ "bookmarks_properties_date_visited_value_label", &e->date_visited_value_label },
		{ "bookmarks_properties_smart_entry_width_label", &e->smart_entry_width_label },
		{ "bookmarks_properties_smart_entry_width_spinbutton", &e->smart_entry_width_spinbutton },
		{ "bookmarks_properties_smart_entry_width_hbox", &p->smart_entry_width_hbox },
		{ "bookmarks_properties_parameter_encoding_label", &e->parameter_encoding_label },
		{ "bookmarks_properties_parameter_encoding_combo", &e->parameter_encoding_combo },
		{ "bookmarks_properties_location_label", &e->location_label },
		{ "bookmarks_properties_location_entry", &e->location_entry },
		{ "bookmarks_properties_notes_textview", &e->notes_textview },
		{ "bookmarks_properties_nicks_label", &e->nicks_label },
		{ "bookmarks_properties_nicks_entry", &e->nicks_entry },
		{ "bookmarks_properties_search_text_entry", &e->search_text_entry },
		{ "bookmarks_properties_search_recently_visited_checkbox", &e->search_recently_visited_checkbox },
		{ "bookmarks_properties_search_recently_visited_spin", &e->search_recently_visited_spin },
		{ "bookmarks_properties_search_recently_created_checkbox", &e->search_recently_created_checkbox },
		{ "bookmarks_properties_search_recently_created_spin", &e->search_recently_created_spin },
		{ "bookmarks_properties_search_options", &e->search_options },
		{ "b-f-match-case", &e->search_b_f_match_case },
		{ "b-f-include-folders", &e->search_b_f_include_folders },
		{ "b-f-include-sites", &e->search_b_f_include_sites },
		{ "b-f-look-in-name", &e->search_b_f_look_in_name },
		{ "b-f-look-in-location", &e->search_b_f_look_in_location },
		{ "b-f-look-in-notes", &e->search_b_f_look_in_notes },
		{ "b-f-several-and", &e->search_b_f_several_and },
		{ "b-f-several-or", &e->search_b_f_several_or },
		{ "b-f-several-exact", &e->search_b_f_several_exact },
		{ "bookmarks_properties_create_toolbar_checkbox", &e->create_toolbar_checkbox },
		{ "bookmarks_properties_add_to_context_checkbox", &e->add_to_context_checkbox },
		{ "bookmarks_properties_image_label", &e->image_label },
		{ "bookmarks_properties_image_pixmapentry", &e->image_pixmapentry },
		{ "bookmarks_properties_parents_scrolledwindow", &p->parents_scrolledwindow },
		{ "bookmarks_properties_alias_label", &p->alias_label },
		{ "bookmarks_properties_close_button", &p->close_button },
		{ "bookmarks_properties_autobm_search_text_label", &e->autobm_search_text_label },
		{ "bookmarks_properties_autobm_search_text_entry", &e->autobm_search_text_entry },
		{ "bookmarks_properties_autobm_options", &e->autobm_options },
		{ "bookmarks_properties_autobm_search_b_f_match_case", &e->autobm_search_b_f_match_case },
		{ "bookmarks_properties_autobm_group_by_host_checkbutton", &e->autobm_group_by_host_checkbutton },
		{ "bookmarks_properties_autobm_search_b_f_look_in_title", &e->autobm_search_b_f_look_in_title },
		{ "bookmarks_properties_autobm_search_b_f_look_in_url", &e->autobm_search_b_f_look_in_url },
		{ "bookmarks_properties_autobm_search_b_f_several_and", &e->autobm_search_b_f_several_and },
		{ "bookmarks_properties_autobm_search_b_f_several_or", &e->autobm_search_b_f_several_or },
		{ "bookmarks_properties_autobm_search_b_f_several_exact", &e->autobm_search_b_f_several_exact },
		{ "bookmarks_properties_autobm_score_more_recently_used", &e->autobm_score_more_recently_used },
		{ "bookmarks_properties_autobm_score_more_frequently_used", &e->autobm_score_more_frequently_used },
		{ "bookmarks_properties_autobm_score_both", &e->autobm_score_both },

		{ NULL, NULL }
	};
	GladeXML *gxml = gul_glade_widget_new ("bookmarks-editor.glade", "bookmarks_properties_window",
					       NULL, e);
	gul_glade_lookup_widgets (gxml, lookups);
	g_object_unref (gxml);
	
	g_object_weak_ref (G_OBJECT (p->window), (GWeakNotify) g_object_unref, e);
	gul_state_monitor_window (p->window, "Bookmark Properties", 370, 390);

	p->parents_treeview = gb_tree_view_new ();
	gb_tree_view_set_check_visible (p->parents_treeview, TRUE);
	gb_tree_view_set_location_visible (p->parents_treeview, FALSE);
	gb_tree_view_set_double_click_action (p->parents_treeview, GB_TV_NONE);
	g_signal_connect (p->parents_treeview, "bookmark-check-toggled",
			  G_CALLBACK (parents_treeview_bookmark_check_toggled), e);
	gtk_widget_show (GTK_WIDGET (p->parents_treeview));
	gtk_container_add (GTK_CONTAINER (p->parents_scrolledwindow), GTK_WIDGET (p->parents_treeview));
	gtk_label_set_mnemonic_widget (GTK_LABEL (p->alias_label), GTK_WIDGET (p->parents_treeview));

	/* Hook up the close button */
	g_signal_connect_swapped (p->close_button, "clicked",
				  G_CALLBACK (gtk_widget_destroy), e->priv->window);

	/* Setup CTRL+Enter to close the window */
	g_signal_connect (e->name_entry, "key-press-event",
			  G_CALLBACK (gb_single_editor_close_on_ctrl_enter), e);
	g_signal_connect (e->smarturl_entry, "key-press-event",
			  G_CALLBACK (gb_single_editor_close_on_ctrl_enter), e);
	g_signal_connect (e->location_entry, "key-press-event",
			  G_CALLBACK (gb_single_editor_close_on_ctrl_enter), e);
	g_signal_connect (e->nicks_entry, "key-press-event",
			  G_CALLBACK (gb_single_editor_close_on_ctrl_enter), e);
	g_signal_connect (e->search_text_entry, "key-press-event",
			  G_CALLBACK (gb_single_editor_close_on_ctrl_enter), e);
	g_signal_connect (e->search_recently_visited_spin, "key-press-event",
			  G_CALLBACK (gb_single_editor_close_on_ctrl_enter), e);
	g_signal_connect (e->search_recently_created_spin, "key-press-event",
			  G_CALLBACK (gb_single_editor_close_on_ctrl_enter), e);
	g_signal_connect (p->parents_treeview, "key-press-event",
			  G_CALLBACK (gb_single_editor_close_on_ctrl_enter), e);
	g_signal_connect (e->smart_entry_width_spinbutton, "key-press-event",
			  G_CALLBACK (gb_single_editor_close_on_ctrl_enter), e);
	g_signal_connect (e->notes_textview, "key-press-event",
			  G_CALLBACK (gb_single_editor_close_on_ctrl_enter), e);
	g_signal_connect (GTK_BIN (e->parameter_encoding_combo)->child, "key-press-event",
			  G_CALLBACK (gb_single_editor_close_on_ctrl_enter), e);

	/* Setup Enter to move to the next widget */
	g_signal_connect (GTK_ENTRY (e->name_entry), "activate", 
			  G_CALLBACK (gb_single_editor_move_to_next), e);
	g_signal_connect (GTK_ENTRY (e->nicks_entry), "activate", 
			  G_CALLBACK (gb_single_editor_move_to_next), e);
	g_signal_connect (GTK_ENTRY (e->smarturl_entry), "activate", 
			  G_CALLBACK (gb_single_editor_move_to_next), e);
	g_signal_connect (GTK_ENTRY (e->location_entry), "activate", 
			  G_CALLBACK (gb_single_editor_move_to_next), e);
	g_signal_connect (GTK_ENTRY (e->search_text_entry), "activate", 
			  G_CALLBACK (gb_single_editor_move_to_next), e);
	g_signal_connect (GTK_ENTRY (e->search_recently_visited_spin), "activate", 
			  G_CALLBACK (gb_single_editor_move_to_next), e);
	g_signal_connect (GTK_ENTRY (e->search_recently_created_spin), "activate", 
			  G_CALLBACK (gb_single_editor_move_to_next), e);
	g_signal_connect (GTK_ENTRY (GTK_BIN (e->parameter_encoding_combo)->child), "activate", 
			  G_CALLBACK (gb_single_editor_move_to_next), e);

	/* ref the widgets */
	if (e->name_label) g_object_ref (e->name_label);
	if (e->name_entry) g_object_ref (e->name_entry);
	if (e->smarturl_label) g_object_ref (e->smarturl_label);
	if (e->smarturl_entry) g_object_ref (e->smarturl_entry);
	if (e->date_added_label) g_object_ref (e->date_added_label);
	if (e->date_added_value_label) g_object_ref (e->date_added_value_label);
	if (e->date_modified_label) g_object_ref (e->date_modified_label);
	if (e->date_modified_value_label) g_object_ref (e->date_modified_value_label);
	if (e->date_visited_label) g_object_ref (e->date_visited_label);
	if (e->date_visited_value_label) g_object_ref (e->date_visited_value_label);
	if (e->smart_entry_width_label) g_object_ref (e->smart_entry_width_label);
	if (e->smart_entry_width_spinbutton) g_object_ref (e->smart_entry_width_spinbutton);
	if (p->smart_entry_width_hbox) g_object_ref (p->smart_entry_width_hbox);
	if (e->parameter_encoding_label) g_object_ref (e->parameter_encoding_label);
	if (e->parameter_encoding_combo) g_object_ref (e->parameter_encoding_combo);
	if (e->location_label) g_object_ref (e->location_label);
	if (e->location_entry) g_object_ref (e->location_entry);
	if (e->notes_textview) g_object_ref (e->notes_textview);
	if (e->nicks_label) g_object_ref (e->nicks_label);
	if (e->nicks_entry) g_object_ref (e->nicks_entry);
	if (e->search_text_entry) g_object_ref (e->search_text_entry);
	if (e->search_recently_visited_checkbox) g_object_ref (e->search_recently_visited_checkbox);
	if (e->search_recently_visited_spin) g_object_ref (e->search_recently_visited_spin);
	if (e->search_recently_created_checkbox) g_object_ref (e->search_recently_created_checkbox);
	if (e->search_recently_created_spin) g_object_ref (e->search_recently_created_spin);
	if (e->search_options) g_object_ref (e->search_options);
	if (e->search_b_f_match_case) g_object_ref (e->search_b_f_match_case);
	if (e->search_b_f_include_folders) g_object_ref (e->search_b_f_include_folders);
	if (e->search_b_f_include_sites) g_object_ref (e->search_b_f_include_sites);
	if (e->search_b_f_look_in_name) g_object_ref (e->search_b_f_look_in_name);
	if (e->search_b_f_look_in_location) g_object_ref (e->search_b_f_look_in_location);
	if (e->search_b_f_look_in_notes) g_object_ref (e->search_b_f_look_in_notes);
	if (e->search_b_f_several_and) g_object_ref (e->search_b_f_several_and);
	if (e->search_b_f_several_or) g_object_ref (e->search_b_f_several_or);
	if (e->search_b_f_several_exact) g_object_ref (e->search_b_f_several_exact);
	if (e->autobm_search_text_label) g_object_ref (e->autobm_search_text_label);
	if (e->autobm_search_text_entry) g_object_ref (e->autobm_search_text_entry);
	if (e->autobm_options) g_object_ref (e->autobm_options);
	if (e->autobm_search_b_f_match_case) g_object_ref (e->autobm_search_b_f_match_case);
	if (e->autobm_group_by_host_checkbutton) g_object_ref (e->autobm_group_by_host_checkbutton);
	if (e->autobm_search_b_f_look_in_title) g_object_ref (e->autobm_search_b_f_look_in_title);
	if (e->autobm_search_b_f_look_in_url) g_object_ref (e->autobm_search_b_f_look_in_url);
	if (e->autobm_search_b_f_several_and) g_object_ref (e->autobm_search_b_f_several_and);
	if (e->autobm_search_b_f_several_or) g_object_ref (e->autobm_search_b_f_several_or);
	if (e->autobm_search_b_f_several_exact) g_object_ref (e->autobm_search_b_f_several_exact);
	if (e->autobm_score_more_recently_used) g_object_ref (e->autobm_score_more_recently_used);
	if (e->autobm_score_more_frequently_used) g_object_ref (e->autobm_score_more_frequently_used);
	if (e->autobm_score_both) g_object_ref (e->autobm_score_both);
	if (e->create_toolbar_checkbox) g_object_ref (e->create_toolbar_checkbox);
	if (e->add_to_context_checkbox) g_object_ref (e->add_to_context_checkbox);
	if (e->image_label) g_object_ref (e->image_label);
	if (e->image_pixmapentry) g_object_ref (e->image_pixmapentry);
	if (p->parents_scrolledwindow) g_object_ref (p->parents_scrolledwindow);
	if (p->alias_label) g_object_ref (p->alias_label);
	if (p->close_button) g_object_ref (p->close_button);
}

void 
gb_single_editor_init_widgets_and_signals (GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;

	if (p->widgets_and_signals_inited)
	{
		return;
	}
	
	p->widgets_and_signals_inited = TRUE;
	
	gb_single_editor_init_widgets (e);

	if (e->notes_textview)
	{
		GtkTextBuffer *buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (e->notes_textview));
		g_signal_connect (buffer, "changed",
				  G_CALLBACK (gb_single_editor_notes_text_buffer_changed_cb), e);
	}
	
	if (e->name_entry)
	{
		g_signal_connect (e->name_entry, "changed",
				  G_CALLBACK (gb_single_editor_name_entry_changed_cb), e);
	}

	if (e->smarturl_entry)
	{
		g_signal_connect (e->smarturl_entry, "changed",
				  G_CALLBACK (gb_single_editor_smarturl_entry_changed_cb), e);
	}
	
	if (e->smart_entry_width_spinbutton)
	{
		g_signal_connect (e->smart_entry_width_spinbutton, "value-changed",
				  G_CALLBACK (gb_single_editor_smart_entry_width_spinbutton_value_changed_cb), e);
	}

	if (e->location_entry)
	{
		g_signal_connect (e->location_entry, "changed",
				  G_CALLBACK (gb_single_editor_location_entry_changed_cb), e);
	}
	
	if (e->nicks_entry)
	{
		g_signal_connect (e->nicks_entry, "changed", 
				  G_CALLBACK (gb_single_editor_nicks_entry_changed_cb), e);
	}
	
	if (e->search_text_entry)
	{
		g_signal_connect (e->search_text_entry, "changed", 
				  G_CALLBACK (gb_single_editor_search_text_entry_changed_cb), e);
	}


	if (e->search_recently_visited_checkbox)
	{
		g_signal_connect (e->search_recently_visited_checkbox, "toggled",
				  G_CALLBACK (gb_single_editor_search_recently_visited_checkbox_toggled_cb), e);
	}

	if (e->search_recently_visited_spin)
	{
		g_signal_connect (e->search_recently_visited_spin, "value-changed",
				  G_CALLBACK (gb_single_editor_search_recently_visited_spin_value_changed_cb), e);
	}

	if (e->search_recently_created_checkbox)
	{
		g_signal_connect (e->search_recently_created_checkbox, "toggled",
				  G_CALLBACK (gb_single_editor_search_recently_created_checkbox_toggled_cb), e);
	}

	if (e->search_recently_created_spin)
	{
		g_signal_connect (e->search_recently_created_spin, "value-changed",
				  G_CALLBACK (gb_single_editor_search_recently_created_spin_value_changed_cb), e);
	}

	if (e->search_b_f_match_case)
	{
		g_signal_connect (e->search_b_f_match_case, "toggled",
				  G_CALLBACK (gb_single_editor_search_b_f_match_case_toggled_cb), e);
	}

	if (e->search_b_f_include_folders)
	{
		g_signal_connect (e->search_b_f_include_folders, "toggled",
				  G_CALLBACK (gb_single_editor_search_b_f_include_folders_toggled_cb), e);
	}

	if (e->search_b_f_include_sites)
	{
		g_signal_connect (e->search_b_f_include_sites, "toggled",
				  G_CALLBACK (gb_single_editor_search_b_f_include_sites_toggled_cb), e);
	}

	if (e->search_b_f_look_in_name)
	{
		g_signal_connect (e->search_b_f_look_in_name, "toggled",
				  G_CALLBACK (gb_single_editor_search_b_f_look_in_name_toggled_cb), e);
	}

	if (e->search_b_f_look_in_location)
	{
		g_signal_connect (e->search_b_f_look_in_location, "toggled",
				  G_CALLBACK (gb_single_editor_search_b_f_look_in_location_toggled_cb), e);
	}

	if (e->search_b_f_look_in_notes)
	{
		g_signal_connect (e->search_b_f_look_in_notes, "toggled",
				  G_CALLBACK (gb_single_editor_search_b_f_look_in_notes_toggled_cb), e);
	}

	if (e->search_b_f_several_and)
	{
		g_signal_connect (e->search_b_f_several_and, "toggled",
				  G_CALLBACK (gb_single_editor_search_b_f_several_and_toggled_cb), e);
	}

	if (e->search_b_f_several_or)
	{
		g_signal_connect (e->search_b_f_several_or, "toggled",
				  G_CALLBACK (gb_single_editor_search_b_f_several_or_toggled_cb), e);
	}

	if (e->search_b_f_several_exact)
	{
		g_signal_connect (e->search_b_f_several_exact, "toggled",
				  G_CALLBACK (gb_single_editor_search_b_f_several_exact_toggled_cb), e);
	}

	if (e->create_toolbar_checkbox)
	{
		g_signal_connect (e->create_toolbar_checkbox, "toggled",
				  G_CALLBACK (gb_single_editor_create_toolbar_checkbox_toggled_cb), e);
	}
	
	if (e->add_to_context_checkbox)
	{
		g_signal_connect (e->add_to_context_checkbox, "toggled",
				  G_CALLBACK (gb_single_editor_add_to_context_checkbox_toggled_cb), e);
	}
	
	if (e->image_pixmapentry)
	{
		g_signal_connect (e->image_pixmapentry, "changed",
				  G_CALLBACK (gb_single_editor_image_pixmapentry_changed_cb), e);
		gtk_label_set_mnemonic_widget (GTK_LABEL (e->image_label), e->image_pixmapentry);
	}
	
	if (GTK_COMBO_BOX_ENTRY (e->parameter_encoding_combo))
	{
		g_signal_connect (GTK_BIN (e->parameter_encoding_combo)->child, "changed",
				  G_CALLBACK (gb_single_editor_parameter_encoding_combo_changed_cb), e);
	}

	
	if (e->autobm_search_text_entry)
	{
		g_signal_connect (e->autobm_search_text_entry, "changed", 
				  G_CALLBACK (gb_single_editor_autobm_search_text_entry_changed_cb), e);
	}

	if (e->autobm_search_b_f_match_case)
	{
		g_signal_connect (e->autobm_search_b_f_match_case, "toggled",
				  G_CALLBACK (gb_single_editor_autobm_search_b_f_match_case_toggled_cb), e);
	}

	if (e->autobm_group_by_host_checkbutton)
	{
		g_signal_connect (e->autobm_group_by_host_checkbutton, "toggled",
				  G_CALLBACK (gb_single_editor_autobm_group_by_host_checkbutton_toggled_cb), e);
	}

	if (e->autobm_search_b_f_look_in_title)
	{
		g_signal_connect (e->autobm_search_b_f_look_in_title, "toggled",
				  G_CALLBACK (gb_single_editor_autobm_search_b_f_look_in_title_toggled_cb), e);
	}

	if (e->autobm_search_b_f_look_in_url)
	{
		g_signal_connect (e->autobm_search_b_f_look_in_url, "toggled",
				  G_CALLBACK (gb_single_editor_autobm_search_b_f_look_in_url_toggled_cb), e);
	}

	if (e->autobm_search_b_f_several_and)
	{
		g_signal_connect (e->autobm_search_b_f_several_and, "toggled",
				  G_CALLBACK (gb_single_editor_autobm_search_b_f_several_and_toggled_cb), e);
	}

	if (e->autobm_search_b_f_several_or)
	{
		g_signal_connect (e->autobm_search_b_f_several_or, "toggled",
				  G_CALLBACK (gb_single_editor_autobm_search_b_f_several_or_toggled_cb), e);
	}

	if (e->autobm_search_b_f_several_exact)
	{
		g_signal_connect (e->autobm_search_b_f_several_exact, "toggled",
				  G_CALLBACK (gb_single_editor_autobm_search_b_f_several_exact_toggled_cb), e);
	}

	if (e->autobm_score_more_recently_used)
	{
		g_signal_connect (e->autobm_score_more_recently_used, "toggled",
				  G_CALLBACK (gb_single_editor_autobm_score_more_recently_used_toggled_cb), e);
	}

	if (e->autobm_score_more_frequently_used)
	{
		g_signal_connect (e->autobm_score_more_frequently_used, "toggled",
				  G_CALLBACK (gb_single_editor_autobm_score_more_frequently_used_toggled_cb), e);
	}

	if (e->autobm_score_both)
	{
		g_signal_connect (e->autobm_score_both, "toggled",
				  G_CALLBACK (gb_single_editor_autobm_score_both_toggled_cb), e);
	}
}

GbSingleEditor *
gb_single_editor_new (void)
{
	GbSingleEditor *ret = g_object_new (GB_TYPE_SINGLE_EDITOR, NULL);
	return ret;
}

void
gb_single_editor_show (GbSingleEditor *editor)
{
	gb_single_editor_init_widgets_and_signals (editor);
	gtk_widget_show (GTK_WIDGET (editor->priv->window));
}

GbBookmark *
gb_single_editor_get_bookmark (GbSingleEditor *e)
{
	return e->priv->bm;
}

static void
gb_single_editor_set_bookmark_internal (GbSingleEditor *e, GbBookmark *b)
{
	if (e->priv->bm)
	{
		g_signal_handlers_disconnect_matched (e->priv->bm, G_SIGNAL_MATCH_DATA, 0, 0, 
						      NULL, NULL, e);
		g_object_unref (e->priv->bm);
	}

	e->priv->bm = b;

	if (b)
	{
		g_object_ref (b);
		g_signal_connect (b, "modified", G_CALLBACK (gb_single_editor_bookmark_changed_cb), e);
		g_signal_connect (b, "replaced", G_CALLBACK (gb_single_editor_bookmark_replaced_cb), e);
	}
}

void
gb_single_editor_set_bookmark (GbSingleEditor *e, GbBookmark *b)
{
	GbSingleEditorPrivate *p = e->priv;

	gb_single_editor_set_bookmark_internal (e, b);
	gb_single_editor_update_controls (e);
	
	if (p->parents_treemodel)
	{
		g_object_unref (p->parents_treemodel);
		p->parents_treemodel = NULL;
	}

	if (p->parents_treeview)
	{
		if (b)
		{
			GSList *parents = gb_bookmark_get_all_alias_parents (b);
			GSList *li;
			p->parents_treemodel = gb_tree_model_new_for_set (b->set);
			gb_tree_model_set_only_folders (p->parents_treemodel, TRUE);
			gb_tree_model_set_check (p->parents_treemodel,
						 (GbTreeModelCheckFunc) gb_folder_has_child_or_alias, b);
			gb_tree_view_set_model (p->parents_treeview, p->parents_treemodel);
			for (li = parents; li; li = li->next)
			{
				gb_tree_view_ensure_expanded (p->parents_treeview, li->data);
			}
			g_slist_free (parents);
		}
	}
}

void
gb_single_editor_set_disable_policy (GbSingleEditor *e, GbSingleEditorDisablePolicy policy)
{
	GbSingleEditorPrivate *p = e->priv;
	p->disable_policy = policy;
}

static void
safe_gtk_widget_disable (GtkWidget *w)
{
	if (w)
	{
		gtk_widget_set_sensitive (w, FALSE);
	}
}

static void
safe_gtk_widget_enable (GtkWidget *w)
{
	if (w)
	{
		gtk_widget_set_sensitive (w, TRUE);
	}
}

static void
safe_gtk_widget_show (GtkWidget *w)
{
	if (w)
	{
		gtk_widget_show (w);
	}
}

static void
safe_gtk_widget_hide (GtkWidget *w)
{
	if (w)
	{
		gtk_widget_hide (w);
	}
}

static gchar *
gb_single_editor_format_time (GTime t)
{
	if (t > 0)
	{
		gchar buffer[256];
		int length;
		struct tm stm;
		time_t tt = t;

		/* Convert to local time */
		localtime_r (&tt, &stm);
		length = strftime (buffer, sizeof(buffer), _("%a, %b %-d %Y at %-I:%M %p"),
				   &stm);
		buffer[length] = '\0';		

		return g_locale_to_utf8 (buffer, -1, NULL, NULL, NULL);
	}
	return NULL;
}


static void
gb_single_editor_update_controls (GbSingleEditor *e)
{
	/* TODO: blank all fields when disabling them. */
	
	GbSingleEditorPrivate *p = e->priv;

	void (*disable) (GtkWidget *) = p->disable_policy == GB_SINGLE_EDITOR_DISABLE_POLICY_HIDE 
		? safe_gtk_widget_hide : safe_gtk_widget_disable;
	void (*enable) (GtkWidget *) = p->disable_policy == GB_SINGLE_EDITOR_DISABLE_POLICY_HIDE
		? safe_gtk_widget_show : safe_gtk_widget_enable;

	gb_single_editor_init_widgets_and_signals (e);

	p->no_update++;

	if (!p->bm)
	{
		if (e->notes_textview)
		{
			GtkTextBuffer *buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (e->notes_textview));
			gtk_text_buffer_set_text (buffer, "", -1);
		}

		if (e->name_entry)
		{
			gtk_entry_set_text (GTK_ENTRY (e->name_entry), "");
		}

		if (e->nicks_entry)
		{
			gtk_entry_set_text (GTK_ENTRY (e->nicks_entry), "");
		}

		if (e->search_text_entry)
		{
			gtk_entry_set_text (GTK_ENTRY (e->search_text_entry), "");
		}

		if (e->search_recently_created_spin)
		{
		     gtk_spin_button_set_value (GTK_SPIN_BUTTON (e->search_recently_created_spin), 0);
		}

		if (e->search_recently_visited_spin)
		{
		     gtk_spin_button_set_value (GTK_SPIN_BUTTON (e->search_recently_created_spin), 0);
		}

		if (e->search_recently_created_checkbox)
		{
		     gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (e->search_recently_created_checkbox), FALSE);
		}

		if (e->search_recently_visited_checkbox)
		{
		     gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (e->search_recently_visited_checkbox), FALSE);
		}

		if (e->smarturl_entry)
		{
			gtk_entry_set_text (GTK_ENTRY (e->smarturl_entry), "");
		}

		if (e->location_entry)
		{
			gtk_entry_set_text (GTK_ENTRY (e->location_entry), "");
		}

		disable (e->name_entry);
		disable (e->name_label);
		disable (e->nicks_entry);
		disable (e->nicks_label);
		disable (e->search_text_entry);
		disable (e->search_text_label);
		disable (e->search_recently_visited_checkbox);
		disable (e->search_recently_visited_spin);
		disable (e->search_recently_created_checkbox);
		disable (e->search_recently_created_spin);
		disable (e->notes_label);
		disable (e->notes_textview);
		disable (e->location_entry);
		disable (e->location_label);
		disable (e->date_visited_value_label);
		disable (e->date_visited_label);
		disable (e->smart_entry_width_spinbutton);
		disable (e->smart_entry_width_label);
		disable (e->smarturl_entry);
		disable (e->smarturl_label);
		disable (e->parameter_encoding_combo);
		disable (e->parameter_encoding_label);
		disable (e->create_toolbar_checkbox);
		disable (e->more_properties_button);
		disable (e->autobm_search_text_label);
		disable (e->autobm_search_text_entry);
		disable (e->autobm_options);
		disable (e->autobm_search_b_f_match_case);
		disable (e->autobm_group_by_host_checkbutton);
		disable (e->autobm_search_b_f_look_in_title);
		disable (e->autobm_search_b_f_look_in_url);
		disable (e->autobm_search_b_f_several_and);
		disable (e->autobm_search_b_f_several_or);
		disable (e->autobm_search_b_f_several_exact);
		disable (e->autobm_score_more_recently_used);
		disable (e->autobm_score_more_frequently_used);
		disable (e->autobm_score_both);

		p->no_update--;
		return;		
	}

	enable (e->more_properties_button);

	if (e->name_entry)
	{
		gtk_entry_set_text (GTK_ENTRY (e->name_entry), p->bm->name);
		enable (e->name_entry);
		enable (e->name_label);
	}

	if (e->notes_textview)
	{
		GtkTextBuffer *buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (e->notes_textview));
		GtkTextIter begin, end;
		char * text;

		/* Fix bug 97262, don't update the text if it won't change it, 
		 * this stops the cursor being moved to the end */
		gtk_text_buffer_get_start_iter (buffer, &begin);
		gtk_text_buffer_get_end_iter (buffer, &end);
		text = gtk_text_buffer_get_text (buffer, &begin, &end, TRUE);
		if (strcmp (text, p->bm->notes))
		{
			gtk_text_buffer_set_text (buffer, p->bm->notes, -1);
		}

		g_free (text);
		enable (e->notes_textview);
		enable (e->notes_label);
	}

	if (e->add_to_context_checkbox)
	{
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (e->add_to_context_checkbox), 
					      p->bm->add_to_context_menu);
		enable (e->add_to_context_checkbox);
	}

	if (e->date_added_value_label)
	{
		char *s = gb_single_editor_format_time (p->bm->time_added);
		if (!s) s = g_strdup_printf( "<i>&lt;%s&gt;</i>", _("Not specified"));
		gtk_label_set_markup (GTK_LABEL (e->date_added_value_label), s);
		g_free (s);
		enable (e->date_added_value_label);
	}

	if (e->date_modified_value_label)
	{
		/* The entry was 'modified' when it was added */
		GTime t = p->bm->time_modified ? p->bm->time_modified : p->bm->time_added;
		gchar *s = gb_single_editor_format_time (t);
		if (!s) s = g_strdup_printf( "<i>&lt;%s&gt;</i>", _("Not specified"));
		gtk_label_set_markup (GTK_LABEL (e->date_modified_value_label), s);
		g_free (s);
		enable (e->date_modified_value_label);
	}

	if (e->image_pixmapentry)
	{
		gnome_file_entry_set_filename (GNOME_FILE_ENTRY (e->image_pixmapentry),
					       p->bm->pixmap_file);
		enable (e->image_pixmapentry);
	}
	
	if (GB_IS_SITE (p->bm))
	{
		if (e->location_entry)
		{
			gtk_entry_set_text (GTK_ENTRY (e->location_entry), GB_SITE (p->bm)->url);
			enable (e->location_entry);
			enable (e->location_label);
		}

		if (e->date_visited_value_label)
		{
			gchar *s = gb_single_editor_format_time (GB_SITE (p->bm)->time_visited);
			if (!s) s = g_strdup_printf( "<i>&lt;%s&gt;</i>", _("Not specified"));
			gtk_label_set_markup (GTK_LABEL (e->date_visited_value_label), s);
			g_free (s);
			enable (e->date_visited_value_label);
		}
		if (e->nicks_entry)
		{
			gtk_entry_set_text (GTK_ENTRY (e->nicks_entry), p->bm->nick);
			enable (e->nicks_entry);
			enable (e->nicks_label);
		}
		if (e->smarturl_entry)
		{
			gchar *smarturl = GB_IS_SMART_SITE (p->bm) 
				? gb_smart_site_get_smarturl (GB_SMART_SITE (p->bm))
				: g_strdup ("");
			gtk_entry_set_text (GTK_ENTRY (e->smarturl_entry), smarturl);
			g_free (smarturl);
			enable (e->smarturl_entry);
			enable (e->smarturl_label);
		}

		if (e->parameter_encoding_combo)
		{
			gchar *encoding;
			enable (e->parameter_encoding_combo);
			enable (e->parameter_encoding_label);
			if (GB_IS_SMART_SITE (p->bm))
			{
				encoding = gb_smart_site_get_encoding (GB_SMART_SITE (p->bm));
				safe_gtk_widget_enable (e->parameter_encoding_label);
				safe_gtk_widget_enable (e->parameter_encoding_combo);
			}
			else
			{
				encoding = g_strdup ("");
				safe_gtk_widget_disable (e->parameter_encoding_label);
				safe_gtk_widget_disable (e->parameter_encoding_combo);
			}
			gtk_entry_set_text (GTK_ENTRY (GTK_BIN (e->parameter_encoding_combo)->child), encoding);
			g_free (encoding);
		}
	}
	else
	{
		if (e->location_entry)
		{
			gtk_entry_set_text (GTK_ENTRY (e->location_entry), "");
		}
		if (e->nicks_entry)
		{
			gtk_entry_set_text (GTK_ENTRY (e->nicks_entry), "");
		}
		disable (e->location_entry);
		disable (e->location_label);
		disable (e->date_visited_value_label);
		disable (e->date_visited_label);
		disable (e->nicks_entry);
		disable (e->nicks_label);
		disable (e->smarturl_entry);
		disable (e->smarturl_label);
		disable (e->parameter_encoding_combo);
		disable (e->parameter_encoding_label);
	}

	if (GB_IS_SMART_SITE (p->bm))
	{
		if (e->smart_entry_width_spinbutton)
		{
			guint width = gb_smart_site_get_entry_size (GB_SMART_SITE (p->bm), 0);
			gtk_spin_button_set_value (GTK_SPIN_BUTTON (e->smart_entry_width_spinbutton), width);
			enable (e->smart_entry_width_spinbutton);
			enable (e->smart_entry_width_label);
			enable (p->smart_entry_width_hbox);
		}
	}
	else
	{
		disable (e->smart_entry_width_spinbutton);
		disable (e->smart_entry_width_label);
		disable (p->smart_entry_width_hbox);
		if (e->smarturl_entry)
		{
			gtk_entry_set_text (GTK_ENTRY (e->smarturl_entry), "");
		}
	}

	if (GB_IS_FOLDER (p->bm))
	{
		if (e->create_toolbar_checkbox)
		{
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (e->create_toolbar_checkbox), 
						      GB_FOLDER (p->bm)->create_toolbar);
			enable (e->create_toolbar_checkbox);
		}
		
		if (GB_IS_V_FOLDER (p->bm) && e->search_text_entry)
		{
			gint days;
			gtk_entry_set_text (GTK_ENTRY (e->search_text_entry), 
					    gb_v_folder_get_search_text (GB_V_FOLDER (p->bm)));
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (e->search_recently_visited_checkbox),
						      gb_v_folder_get_rencently_visited (GB_V_FOLDER (p->bm), &days));
			gtk_spin_button_set_value (GTK_SPIN_BUTTON (e->search_recently_visited_spin), days);
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (e->search_recently_created_checkbox),
						      gb_v_folder_get_rencently_created (GB_V_FOLDER (p->bm), &days));
			gtk_spin_button_set_value (GTK_SPIN_BUTTON (e->search_recently_created_spin), days);
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (e->search_b_f_match_case),
						      gb_v_folder_get_match_case (GB_V_FOLDER (p->bm)));
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (e->search_b_f_include_folders),
						      gb_v_folder_get_include_folders (GB_V_FOLDER (p->bm)));
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (e->search_b_f_include_sites),
						      gb_v_folder_get_include_sites (GB_V_FOLDER (p->bm)));
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (e->search_b_f_look_in_name),
						      gb_v_folder_get_look_in_name (GB_V_FOLDER (p->bm)));
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (e->search_b_f_look_in_location),
						      gb_v_folder_get_look_in_location (GB_V_FOLDER (p->bm)));
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (e->search_b_f_look_in_notes),
						      gb_v_folder_get_look_in_notes (GB_V_FOLDER (p->bm)));
			switch (gb_v_folder_get_match_type (GB_V_FOLDER (p->bm)))
			{
			case GB_MATCH_TYPE_AND:
				gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (e->search_b_f_several_and), TRUE);
				break;
			case GB_MATCH_TYPE_OR:
				gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (e->search_b_f_several_or), TRUE);
				break;
			case GB_MATCH_TYPE_EXACT:
				gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (e->search_b_f_several_exact), TRUE);
				break;
			}

			enable (e->search_text_label);
			enable (e->search_text_entry);
			enable (e->search_options);
			enable (e->search_recently_visited_checkbox);
			enable (e->search_recently_visited_spin);
			enable (e->search_recently_created_checkbox);
			enable (e->search_recently_created_spin);

			if (GB_V_FOLDER (p->bm)->search_text[0] != '\0')
			{
				gtk_widget_set_sensitive (e->search_b_f_match_case, TRUE);
				gtk_widget_set_sensitive (e->search_b_f_look_in_name, TRUE);
				gtk_widget_set_sensitive (e->search_b_f_look_in_location, TRUE);
				gtk_widget_set_sensitive (e->search_b_f_look_in_notes, TRUE);
				gtk_widget_set_sensitive (e->search_b_f_several_and, TRUE);
				gtk_widget_set_sensitive (e->search_b_f_several_or, TRUE);
				gtk_widget_set_sensitive (e->search_b_f_several_exact, TRUE);
			}
			else
			{
				gtk_widget_set_sensitive (e->search_b_f_match_case, FALSE);
				gtk_widget_set_sensitive (e->search_b_f_look_in_name, FALSE);
				gtk_widget_set_sensitive (e->search_b_f_look_in_location, FALSE);
				gtk_widget_set_sensitive (e->search_b_f_look_in_notes, FALSE);
				gtk_widget_set_sensitive (e->search_b_f_several_and, FALSE);
				gtk_widget_set_sensitive (e->search_b_f_several_or, FALSE);
				gtk_widget_set_sensitive (e->search_b_f_several_exact, FALSE);
			}
		}
		else
		{
			disable (e->search_text_label);
			disable (e->search_text_entry);
			disable (e->search_options);
			disable (e->search_recently_visited_checkbox);
			disable (e->search_recently_visited_spin);
			disable (e->search_recently_created_checkbox);
			disable (e->search_recently_created_spin);
		}

		if (GB_IS_AUTO_FOLDER (p->bm) && e->autobm_search_text_entry)
		{
			gtk_entry_set_text (GTK_ENTRY (e->autobm_search_text_entry), 
					    gb_auto_folder_get_search_text (GB_AUTO_FOLDER (p->bm)));
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (e->autobm_search_b_f_match_case),
						      gb_auto_folder_get_match_case (GB_AUTO_FOLDER (p->bm)));
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (e->autobm_group_by_host_checkbutton),
						      gb_auto_folder_get_group_by_host (GB_AUTO_FOLDER (p->bm)));
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (e->autobm_search_b_f_look_in_url),
						      gb_auto_folder_get_look_in_url (GB_AUTO_FOLDER (p->bm)));
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (e->autobm_search_b_f_look_in_title),
						      gb_auto_folder_get_look_in_title (GB_AUTO_FOLDER (p->bm)));

			switch (gb_auto_folder_get_match_type (GB_AUTO_FOLDER (p->bm)))
			{
			case GB_MATCH_TYPE_AND:
				gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (e->autobm_search_b_f_several_and), TRUE);
				break;
			case GB_MATCH_TYPE_OR:
				gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (e->autobm_search_b_f_several_or), TRUE);
				break;
			case GB_MATCH_TYPE_EXACT:
				gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (e->autobm_search_b_f_several_exact), TRUE);
				break;
			}

			switch (gb_auto_folder_get_scoring_method (GB_AUTO_FOLDER (p->bm)))
			{
			case GALEON_AUTO_BOOKMARKS_SCORING_RECENTLY_VISITED:
				gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (e->autobm_score_more_recently_used), TRUE);
				break;
			case GALEON_AUTO_BOOKMARKS_SCORING_FRECUENTLY_VISITED:
				gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (e->autobm_score_more_frequently_used), TRUE);
				break;
			case GALEON_AUTO_BOOKMARKS_SCORING_BOTH:
				gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (e->autobm_score_both), TRUE);
				break;
			}
			
			enable (e->autobm_search_text_label);
			enable (e->autobm_search_text_entry);
			enable (e->autobm_options);
			enable (e->autobm_search_b_f_match_case);
			enable (e->autobm_group_by_host_checkbutton);
			enable (e->autobm_search_b_f_look_in_title);
			enable (e->autobm_search_b_f_look_in_url);
			enable (e->autobm_search_b_f_several_and);
			enable (e->autobm_search_b_f_several_or);
			enable (e->autobm_search_b_f_several_exact);
			enable (e->autobm_score_more_recently_used);
			enable (e->autobm_score_more_frequently_used);
			enable (e->autobm_score_both);

			if (GB_AUTO_FOLDER (p->bm)->search_text[0] != '\0')
			{
				gtk_widget_set_sensitive (e->autobm_search_b_f_match_case, TRUE);
				gtk_widget_set_sensitive (e->autobm_search_b_f_look_in_url, TRUE);
				gtk_widget_set_sensitive (e->autobm_search_b_f_look_in_title, TRUE);
				gtk_widget_set_sensitive (e->autobm_search_b_f_several_and, TRUE);
				gtk_widget_set_sensitive (e->autobm_search_b_f_several_or, TRUE);
				gtk_widget_set_sensitive (e->autobm_search_b_f_several_exact, TRUE);
			}
			else
			{
				gtk_widget_set_sensitive (e->autobm_search_b_f_match_case, FALSE);
				gtk_widget_set_sensitive (e->autobm_search_b_f_look_in_title, FALSE);
				gtk_widget_set_sensitive (e->autobm_search_b_f_look_in_url, FALSE);
				gtk_widget_set_sensitive (e->autobm_search_b_f_several_and, FALSE);
				gtk_widget_set_sensitive (e->autobm_search_b_f_several_or, FALSE);
				gtk_widget_set_sensitive (e->autobm_search_b_f_several_exact, FALSE);
			}

		}
		else
		{
			disable (e->autobm_search_text_label);
			disable (e->autobm_search_text_entry);
			disable (e->autobm_options);
			disable (e->autobm_search_b_f_match_case);
			disable (e->autobm_group_by_host_checkbutton);
			disable (e->autobm_search_b_f_look_in_title);
			disable (e->autobm_search_b_f_look_in_url);
			disable (e->autobm_search_b_f_several_and);
			disable (e->autobm_search_b_f_several_or);
			disable (e->autobm_search_b_f_several_exact);
			disable (e->autobm_score_more_recently_used);
			disable (e->autobm_score_more_frequently_used);
			disable (e->autobm_score_both);
		}
	}
	else
	{
		disable (e->create_toolbar_checkbox);
		disable (e->search_text_label);
		disable (e->search_text_entry);
		disable (e->search_options);
		disable (e->search_recently_visited_checkbox);
		disable (e->search_recently_visited_spin);
		disable (e->search_recently_created_checkbox);
		disable (e->search_recently_created_spin);
		disable (e->autobm_search_text_label);
		disable (e->autobm_search_text_entry);
		disable (e->autobm_options);
		disable (e->autobm_search_b_f_match_case);
		disable (e->autobm_group_by_host_checkbutton);
		disable (e->autobm_search_b_f_look_in_title);
		disable (e->autobm_search_b_f_look_in_url);
		disable (e->autobm_search_b_f_several_and);
		disable (e->autobm_search_b_f_several_or);
		disable (e->autobm_search_b_f_several_exact);
		disable (e->autobm_score_more_recently_used);
		disable (e->autobm_score_more_frequently_used);
		disable (e->autobm_score_both);
	}

	p->no_update--;
}

static void
gb_single_editor_bookmark_changed_cb (GbBookmark *b, GbSingleEditor *e)
{
	g_return_if_fail (e->priv->bm == b);
	
	gb_single_editor_update_controls (e);
}

static void
gb_single_editor_bookmark_replaced_cb (GbBookmark *b, GbBookmark *replacement, 
				       GbSingleEditor *e)
{
	gb_single_editor_set_bookmark (e, replacement);
}


static void
gb_single_editor_name_entry_changed_cb	(GtkWidget *edited,
					 GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gchar *s;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_BOOKMARK (p->bm));

	s = gtk_editable_get_chars (GTK_EDITABLE (e->name_entry), 0, -1);
	gb_bookmark_set_name (p->bm, s);
	gb_bookmark_set_time_modified_now (p->bm);
	g_free (s);
}

static void
gb_single_editor_smarturl_entry_changed_cb (GtkWidget *edited,
					    GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gchar *s;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_SITE (p->bm));

	s = gtk_editable_get_chars (GTK_EDITABLE (e->smarturl_entry), 0, -1);
	if (s[0])
	{
		if (GB_IS_SMART_SITE (p->bm))
		{
			gb_smart_site_set_smarturl (GB_SMART_SITE (p->bm), s);
			gb_bookmark_set_time_modified_now (p->bm);
		}
		else
		{
			GbBookmark *b = (GbBookmark *) gb_site_make_smart (GB_SITE (p->bm), s);
			gb_bookmark_set_time_modified_now (b);
		}
	}
	else
	{
		if (GB_IS_SMART_SITE (p->bm))
		{
			GbBookmark *b = (GbBookmark *) gb_smart_site_make_dumb (GB_SMART_SITE (p->bm));
			gb_bookmark_set_time_modified_now (b);
		}
		else
		{
			/* nothing, actually this should not happen */
		}
	}
	g_free (s);
}

static void
gb_single_editor_location_entry_changed_cb (GtkWidget *edited,
					    GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gchar *s;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_SITE (p->bm));

	s = gtk_editable_get_chars (GTK_EDITABLE (e->location_entry), 0, -1);
	gb_site_set_url (GB_SITE (p->bm), s);
	gb_bookmark_set_time_modified_now (p->bm);
	g_free (s);
}

static void
gb_single_editor_smart_entry_width_spinbutton_value_changed_cb (GtkWidget *spinbutton, GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	guint width;

	if (p->no_update > 0) return;

	g_return_if_fail (GB_IS_SMART_SITE (p->bm));

	width = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (e->smart_entry_width_spinbutton));

	gb_smart_site_set_entry_size (GB_SMART_SITE (p->bm), 0, width);
	gb_bookmark_set_time_modified_now (p->bm);
}

static void
gb_single_editor_notes_text_buffer_changed_cb (GtkWidget *edited,
					       GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	GtkTextBuffer *b;
	GtkTextIter begin, end;
	gchar *notes;

	if (p->no_update > 0) return;

	g_return_if_fail (GB_IS_BOOKMARK (p->bm));

	b = gtk_text_view_get_buffer (GTK_TEXT_VIEW (e->notes_textview));
	gtk_text_buffer_get_start_iter (b, &begin);
	gtk_text_buffer_get_end_iter (b, &end);
	notes = gtk_text_buffer_get_text (b, &begin, &end, TRUE);
	gb_bookmark_set_notes (p->bm, notes);
	gb_bookmark_set_time_modified_now (p->bm);
	g_free (notes);

}

static void
gb_single_editor_nicks_entry_changed_cb	(GtkWidget *edited,
					 GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gchar *s;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_BOOKMARK (p->bm));

	s = gtk_editable_get_chars (GTK_EDITABLE (e->nicks_entry), 0, -1);
	gb_bookmark_set_nick (p->bm, s);
	gb_bookmark_set_time_modified_now (p->bm);
	g_free (s);

}

static void
gb_single_editor_search_text_entry_changed_cb	(GtkWidget *edited,
						 GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gchar *s;

	if (p->no_update > 0) return;

	g_return_if_fail (GB_IS_V_FOLDER (p->bm));
	
	s = gtk_editable_get_chars (GTK_EDITABLE (e->search_text_entry), 0, -1);
	gb_v_folder_set_search_text (GB_V_FOLDER (p->bm), s);
	gb_bookmark_set_time_modified_now (p->bm);
	g_free (s);
}

static void
gb_single_editor_search_recently_visited_checkbox_toggled_cb (GtkWidget *edited, GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gboolean val;
	gint days;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_V_FOLDER (p->bm));

	gb_v_folder_get_rencently_visited (GB_V_FOLDER (p->bm), &days);

	val = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (e->search_recently_visited_checkbox));
	gb_v_folder_set_rencently_visited (GB_V_FOLDER (p->bm), val, days);
	gb_bookmark_set_time_modified_now (p->bm);
}

static void
gb_single_editor_search_recently_visited_spin_value_changed_cb (GtkWidget *edited, GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	guint days;
	gboolean v;

	if (p->no_update > 0) return;

	g_return_if_fail (GB_IS_V_FOLDER (p->bm));

	v = gb_v_folder_get_rencently_visited (GB_V_FOLDER (p->bm), NULL);
	days = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (e->search_recently_visited_spin));
	if (days > 0)
	{
		v = TRUE;
	}
	gb_v_folder_set_rencently_visited (GB_V_FOLDER (p->bm), v, days);
	gb_bookmark_set_time_modified_now (p->bm);
}

static void
gb_single_editor_search_recently_created_checkbox_toggled_cb (GtkWidget *edited, GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gboolean val;
	gint days;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_V_FOLDER (p->bm));

	gb_v_folder_get_rencently_created (GB_V_FOLDER (p->bm), &days);

	val = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (e->search_recently_created_checkbox));
	gb_v_folder_set_rencently_created (GB_V_FOLDER (p->bm), val, days);
	gb_bookmark_set_time_modified_now (p->bm);
}

static void
gb_single_editor_search_recently_created_spin_value_changed_cb (GtkWidget *edited, GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	guint days;
	gboolean v;

	if (p->no_update > 0) return;

	g_return_if_fail (GB_IS_V_FOLDER (p->bm));

	v = gb_v_folder_get_rencently_created (GB_V_FOLDER (p->bm), NULL);
	days = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (e->search_recently_created_spin));
	if (days > 0)
	{
		v = TRUE;
	}
	gb_v_folder_set_rencently_created (GB_V_FOLDER (p->bm), v, days);
	gb_bookmark_set_time_modified_now (p->bm);
}

static void
gb_single_editor_search_b_f_match_case_toggled_cb (GtkWidget *edited, GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gboolean val;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_V_FOLDER (p->bm));

	val = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (e->search_b_f_match_case));
	gb_v_folder_set_match_case (GB_V_FOLDER (p->bm), val);
	gb_bookmark_set_time_modified_now (p->bm);
}

static void
gb_single_editor_search_b_f_include_folders_toggled_cb (GtkWidget *edited, GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gboolean val;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_V_FOLDER (p->bm));

	val = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (e->search_b_f_include_folders));
	gb_v_folder_set_include_folders (GB_V_FOLDER (p->bm), val);
	gb_bookmark_set_time_modified_now (p->bm);
}

static void
gb_single_editor_search_b_f_include_sites_toggled_cb (GtkWidget *edited, GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gboolean val;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_V_FOLDER (p->bm));

	val = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (e->search_b_f_include_sites));
	gb_v_folder_set_include_sites (GB_V_FOLDER (p->bm), val);
	gb_bookmark_set_time_modified_now (p->bm);
}

static void
gb_single_editor_search_b_f_look_in_name_toggled_cb (GtkWidget *edited, GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gboolean val;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_V_FOLDER (p->bm));

	val = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (e->search_b_f_look_in_name));
	gb_v_folder_set_look_in_name (GB_V_FOLDER (p->bm), val);
	gb_bookmark_set_time_modified_now (p->bm);
}

static void
gb_single_editor_search_b_f_look_in_location_toggled_cb (GtkWidget *edited, GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gboolean val;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_V_FOLDER (p->bm));

	val = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (e->search_b_f_look_in_location));
	gb_v_folder_set_look_in_location (GB_V_FOLDER (p->bm), val);
	gb_bookmark_set_time_modified_now (p->bm);
}

static void
gb_single_editor_search_b_f_look_in_notes_toggled_cb (GtkWidget *edited, GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gboolean val;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_V_FOLDER (p->bm));

	val = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (e->search_b_f_look_in_notes));
	gb_v_folder_set_look_in_notes (GB_V_FOLDER (p->bm), val);
	gb_bookmark_set_time_modified_now (p->bm);
}

static void
gb_single_editor_search_b_f_several_and_toggled_cb (GtkWidget *edited, GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gboolean val;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_V_FOLDER (p->bm));

	val = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (e->search_b_f_several_and));
	if (val)
	{
		p->no_update++;
		if (gb_v_folder_get_match_type (GB_V_FOLDER (p->bm)) != GB_MATCH_TYPE_AND)
		{
			gb_v_folder_set_match_type (GB_V_FOLDER (p->bm), GB_MATCH_TYPE_AND);
			gb_bookmark_set_time_modified_now (p->bm);
		}
		p->no_update--;
	}
}

static void
gb_single_editor_search_b_f_several_or_toggled_cb (GtkWidget *edited, GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gboolean val;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_V_FOLDER (p->bm));

	val = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (e->search_b_f_several_or));
	if (val)
	{
		p->no_update++;
		if (gb_v_folder_get_match_type (GB_V_FOLDER (p->bm)) != GB_MATCH_TYPE_OR)
		{
			gb_v_folder_set_match_type (GB_V_FOLDER (p->bm), GB_MATCH_TYPE_OR);
			gb_bookmark_set_time_modified_now (p->bm);
		}
		p->no_update--;
	}
}

static void
gb_single_editor_search_b_f_several_exact_toggled_cb (GtkWidget *edited, GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gboolean val;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_V_FOLDER (p->bm));

	val = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (e->search_b_f_several_exact));
	if (val)
	{
		p->no_update++;
		if (gb_v_folder_get_match_type (GB_V_FOLDER (p->bm)) != GB_MATCH_TYPE_EXACT)
		{
			gb_v_folder_set_match_type (GB_V_FOLDER (p->bm), GB_MATCH_TYPE_EXACT);
			gb_bookmark_set_time_modified_now (p->bm);
		}
		p->no_update--;
	}
}

static void
gb_single_editor_create_toolbar_checkbox_toggled_cb (GtkWidget *edited,
						     GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gboolean val;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_FOLDER (p->bm));

	val = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (e->create_toolbar_checkbox));
	gb_folder_set_create_toolbar (GB_FOLDER (p->bm), val);
	gb_bookmark_set_time_modified_now (p->bm);

}

static void
gb_single_editor_add_to_context_checkbox_toggled_cb (GtkWidget *edited,
						     GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gboolean val;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_BOOKMARK (p->bm));

	val = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (e->add_to_context_checkbox));
	gb_bookmark_set_add_to_context_menu (p->bm, val);
	gb_bookmark_set_time_modified_now (p->bm);

}

static void
gb_single_editor_image_pixmapentry_changed_cb (GtkWidget *edited,
						GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gchar *s;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_BOOKMARK (p->bm));

	s = gtk_editable_get_chars (GTK_EDITABLE (e->image_pixmapentry), 0, -1);
	gb_bookmark_set_pixmap (p->bm, s);
	gb_bookmark_set_time_modified_now (p->bm);
	g_free (s);

}

static void
gb_single_editor_parameter_encoding_combo_changed_cb (GtkWidget *edited,
						      GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gchar *s;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_SMART_SITE (p->bm));

	s = gtk_editable_get_chars (GTK_EDITABLE (GTK_BIN (e->parameter_encoding_combo)->child), 0, -1);
	gb_smart_site_set_encoding (GB_SMART_SITE (p->bm), s);
	gb_bookmark_set_time_modified_now (p->bm);
	g_free (s);
}

static void
gb_single_editor_autobm_search_text_entry_changed_cb (GtkWidget *edited,
						      GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gchar *s;

	if (p->no_update > 0) return;

	g_return_if_fail (GB_IS_AUTO_FOLDER (p->bm));
	
	s = gtk_editable_get_chars (GTK_EDITABLE (e->autobm_search_text_entry), 0, -1);
	gb_auto_folder_set_search_text (GB_AUTO_FOLDER (p->bm), s);
	gb_bookmark_set_time_modified_now (p->bm);
	g_free (s);
}


static void
gb_single_editor_autobm_search_b_f_match_case_toggled_cb (GtkWidget *edited, GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gboolean val;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_AUTO_FOLDER (p->bm));

	val = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (e->autobm_search_b_f_match_case));
	gb_auto_folder_set_match_case (GB_AUTO_FOLDER (p->bm), val);
	gb_bookmark_set_time_modified_now (p->bm);
}

static void
gb_single_editor_autobm_group_by_host_checkbutton_toggled_cb (GtkWidget *edited, GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gboolean val;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_AUTO_FOLDER (p->bm));

	val = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (e->autobm_group_by_host_checkbutton));
	gb_auto_folder_set_group_by_host (GB_AUTO_FOLDER (p->bm), val);
	gb_bookmark_set_time_modified_now (p->bm);
}

static void
gb_single_editor_autobm_search_b_f_look_in_title_toggled_cb (GtkWidget *edited, GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gboolean val;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_AUTO_FOLDER (p->bm));

	val = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (e->autobm_search_b_f_look_in_title));
	gb_auto_folder_set_look_in_title (GB_AUTO_FOLDER (p->bm), val);
	gb_bookmark_set_time_modified_now (p->bm);
}

static void
gb_single_editor_autobm_search_b_f_look_in_url_toggled_cb (GtkWidget *edited, GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gboolean val;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_AUTO_FOLDER (p->bm));

	val = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (e->autobm_search_b_f_look_in_url));
	gb_auto_folder_set_look_in_url (GB_AUTO_FOLDER (p->bm), val);
	gb_bookmark_set_time_modified_now (p->bm);
}

static void
gb_single_editor_autobm_search_b_f_several_and_toggled_cb (GtkWidget *edited, GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gboolean val;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_AUTO_FOLDER (p->bm));

	val = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (e->autobm_search_b_f_several_and));
	if (val)
	{
		p->no_update++;
		if (gb_auto_folder_get_match_type (GB_AUTO_FOLDER (p->bm)) != GB_MATCH_TYPE_AND)
		{
			gb_auto_folder_set_match_type (GB_AUTO_FOLDER (p->bm), GB_MATCH_TYPE_AND);
			gb_bookmark_set_time_modified_now (p->bm);
		}
		p->no_update--;
	}
}

static void
gb_single_editor_autobm_search_b_f_several_or_toggled_cb (GtkWidget *edited, GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gboolean val;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_AUTO_FOLDER (p->bm));

	val = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (e->autobm_search_b_f_several_or));
	if (val)
	{
		p->no_update++;
		if (gb_auto_folder_get_match_type (GB_AUTO_FOLDER (p->bm)) != GB_MATCH_TYPE_OR)
		{
			gb_auto_folder_set_match_type (GB_AUTO_FOLDER (p->bm), GB_MATCH_TYPE_OR);
			gb_bookmark_set_time_modified_now (p->bm);
		}
		p->no_update--;
	}
}

static void
gb_single_editor_autobm_search_b_f_several_exact_toggled_cb (GtkWidget *edited, GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gboolean val;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_AUTO_FOLDER (p->bm));

	val = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (e->autobm_search_b_f_several_exact));
	if (val)
	{
		p->no_update++;
		if (gb_auto_folder_get_match_type (GB_AUTO_FOLDER (p->bm)) != GB_MATCH_TYPE_EXACT)
		{
			gb_auto_folder_set_match_type (GB_AUTO_FOLDER (p->bm), GB_MATCH_TYPE_EXACT);
			gb_bookmark_set_time_modified_now (p->bm);
		}
		p->no_update--;
	}
}

static void
gb_single_editor_autobm_score_more_recently_used_toggled_cb (GtkWidget *edited, GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gboolean val;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_AUTO_FOLDER (p->bm));

	val = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (e->autobm_score_more_recently_used));
	if (val)
	{
		p->no_update++;
		if (gb_auto_folder_get_scoring_method (GB_AUTO_FOLDER (p->bm)) != GALEON_AUTO_BOOKMARKS_SCORING_RECENTLY_VISITED)
		{
			gb_auto_folder_set_scoring_method (GB_AUTO_FOLDER (p->bm), GALEON_AUTO_BOOKMARKS_SCORING_RECENTLY_VISITED);
			gb_bookmark_set_time_modified_now (p->bm);
		}
		p->no_update--;
	}
}

static void
gb_single_editor_autobm_score_more_frequently_used_toggled_cb (GtkWidget *edited, GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gboolean val;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_AUTO_FOLDER (p->bm));

	val = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (e->autobm_score_more_frequently_used));
	if (val)
	{
		p->no_update++;
		if (gb_auto_folder_get_scoring_method (GB_AUTO_FOLDER (p->bm)) != GALEON_AUTO_BOOKMARKS_SCORING_FRECUENTLY_VISITED)
		{
			gb_auto_folder_set_scoring_method (GB_AUTO_FOLDER (p->bm), GALEON_AUTO_BOOKMARKS_SCORING_FRECUENTLY_VISITED);
			gb_bookmark_set_time_modified_now (p->bm);
		}
		p->no_update--;
	}
}

static void
gb_single_editor_autobm_score_both_toggled_cb (GtkWidget *edited, GbSingleEditor *e)
{
	GbSingleEditorPrivate *p = e->priv;
	gboolean val;

	if (p->no_update > 0) return;
	
	g_return_if_fail (GB_IS_AUTO_FOLDER (p->bm));

	val = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (e->autobm_score_both));
	if (val)
	{
		p->no_update++;
		if (gb_auto_folder_get_scoring_method (GB_AUTO_FOLDER (p->bm)) != GALEON_AUTO_BOOKMARKS_SCORING_BOTH)
		{
			gb_auto_folder_set_scoring_method (GB_AUTO_FOLDER (p->bm), GALEON_AUTO_BOOKMARKS_SCORING_BOTH);
			gb_bookmark_set_time_modified_now (p->bm);
		}
		p->no_update--;
	}
}

void
gb_single_editor_focus_name_entry (GbSingleEditor *e)
{
	if (e->name_entry)
	{
		gtk_widget_grab_focus (e->name_entry);
	}
}

