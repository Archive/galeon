/*
 *  Copyright (C) 2004 Crispin Flowerday
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "galeon-sidebar-embed.h"
#include "galeon-embed-event.h"
#include "galeon-embed-utils.h"
#include "galeon-shell.h"

#include "galeon-debug.h"

#include <glib/gi18n.h>

#include <string.h>

#define GALEON_SIDEBAR_EMBED_GET_PRIVATE(object) (G_TYPE_INSTANCE_GET_PRIVATE ((object),\
				   GALEON_TYPE_SIDEBAR_EMBED, GaleonSidebarEmbedPrivate))

struct _GaleonSidebarEmbedPrivate
{
	GaleonWindow *window;
	char * url;
};

static void galeon_sidebar_embed_load_url (GaleonSidebarEmbed *sbembed);

G_DEFINE_TYPE (GaleonSidebarEmbed, galeon_sidebar_embed, GTK_TYPE_BIN);

static GaleonWindow *
galeon_sidebar_embed_get_window (GaleonSidebarEmbed *sbembed)
{
	GtkWidget *toplevel;

	toplevel = gtk_widget_get_toplevel (GTK_WIDGET (sbembed));
	g_return_val_if_fail (GALEON_IS_WINDOW (toplevel), NULL);

	return GALEON_WINDOW (toplevel);
}

void
galeon_sidebar_embed_set_url (GaleonSidebarEmbed *sbembed,
			    const char * url)
{
	g_free (sbembed->priv->url);
	sbembed->priv->url = g_strdup (url);

	if (GTK_BIN (sbembed)->child != NULL)
	{
		galeon_sidebar_embed_load_url (sbembed);
	}
}

static gboolean
embed_contextmenu_cb (GaleonEmbed *embed,
		      GaleonEmbedEvent *event,
		      GaleonSidebarEmbed *sbembed)
{
	GaleonPopup *popup;
	GaleonWindow *window;

	event->context |= EMBED_CONTEXT_SIDEBAR; 

	window = galeon_sidebar_embed_get_window (sbembed);

	popup = GALEON_POPUP (galeon_window_get_popup_factory (window));
	galeon_popup_set_event (popup, event);
	galeon_popup_show (popup, embed);

	return TRUE;
}

static gboolean
embed_mouse_click_cb (GaleonEmbed *embed,
		      GaleonEmbedEvent *event,
		      GaleonSidebarEmbed *sbembed)
{
	EmbedEventContext context;
	guint modifier, button;
	const GValue *targetValue;
	GaleonWindow *window;

	g_return_val_if_fail (GALEON_IS_EMBED_EVENT(event), FALSE);

	event->context |= EMBED_CONTEXT_SIDEBAR; 

	button = galeon_embed_event_get_mouse_button (event);
	context = galeon_embed_event_get_context (event);
	modifier = galeon_embed_event_get_modifier (event);

	targetValue = galeon_embed_event_get_property (event, "link_target");
	window = galeon_sidebar_embed_get_window (sbembed);

	if (button == 1 && (context & EMBED_CONTEXT_LINK) &&
	    (modifier & GDK_SHIFT_MASK))
	{
		galeon_embed_utils_download_event_property (embed, event, 
							    FALSE, NULL, "link");
	}
	else if (button == 1 && (context & EMBED_CONTEXT_IMAGE) &&
		 (modifier & GDK_SHIFT_MASK))
	{
		galeon_embed_utils_save_event_property(embed, event, 
						       TRUE,  /* always_ask_dir */
						       FALSE, /* show_progress */
						       _("Save Image As..."),
						       "image");
	}
	else if (button == 1 && (context & EMBED_CONTEXT_LINK) &&
		 !strcmp(g_value_get_string(targetValue), "_content"))
	{
		const GValue *value;
		const char *link_address;

		value = galeon_embed_event_get_property (event, "link");
		link_address = g_value_get_string (value);
		galeon_window_load_url (window, link_address);

	}
	else if ((button == 1 && (context & EMBED_CONTEXT_LINK) &&
		  (modifier & GDK_CONTROL_MASK)) ||
		 (button == 2 && (context & EMBED_CONTEXT_LINK)))
	{
		GaleonNewTabFlags flags = 0;
		const GValue *value;
		const gchar *link;

		if (button == 2)
		{
			flags = galeon_shell_modifier_flags (modifier);
		}
		value = galeon_embed_event_get_property(event, "link");
		link = g_value_get_string (value);

		if (galeon_embed_event_has_property(event, "link-can-open-in-new-tab"))
		{
			galeon_shell_new_tab(galeon_shell, window, NULL,
					     link, flags);
		}
		else
		{
			galeon_embed_load_url (embed, link);
		}
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}

static GaleonEmbed*
embed_new_window_cb (GaleonEmbed *embed,
		     EmbedChromeMask chromemask,
		     gpointer data)
{
	GaleonWindow *window;
	GaleonTab *new_tab;

	window = galeon_window_new ();
	galeon_window_set_chrome (window, chromemask);

	new_tab = galeon_tab_new ();
	gtk_widget_show (GTK_WIDGET (new_tab));

	galeon_window_add_tab (window, new_tab, -1, FALSE);

	return galeon_tab_get_embed (new_tab);
}

static void
galeon_sidebar_embed_load_url (GaleonSidebarEmbed *sbembed)
{
	GaleonEmbed *embed;

	embed = GALEON_EMBED (GTK_BIN (sbembed)->child);

	if (sbembed->priv->url == NULL)
	{
		galeon_embed_load_url (embed, "about:blank");
	}
	else
	{
		galeon_embed_load_url (embed, sbembed->priv->url);
	}
}

static void
galeon_sidebar_embed_create_embed (GaleonSidebarEmbed *sbembed)
{
	GaleonEmbed *embed;

	if (GTK_BIN (sbembed)->child == NULL)
	{
		embed = galeon_embed_new ();
		gtk_container_add (GTK_CONTAINER (sbembed), GTK_WIDGET (embed));
		gtk_widget_show (GTK_WIDGET (embed));

		g_signal_connect (G_OBJECT (embed),
				  "ge_new_window",
				  G_CALLBACK(embed_new_window_cb),
				  NULL);
		g_signal_connect (G_OBJECT (embed),
				  "ge_dom_mouse_click",
				  G_CALLBACK(embed_mouse_click_cb),
				  sbembed);
		g_signal_connect (G_OBJECT (embed),
				  "ge_contextmenu",
				  G_CALLBACK(embed_contextmenu_cb),
				  sbembed);
	}

	galeon_sidebar_embed_load_url (sbembed);
}

static void
galeon_sidebar_embed_size_allocate (GtkWidget *widget,
				  GtkAllocation *allocation)
{
	GtkWidget *child;

	widget->allocation = *allocation;
	child = GTK_BIN (widget)->child;

	if (child && gtk_widget_get_visible (GTK_WIDGET (child)))
	{
		gtk_widget_size_allocate (child, allocation);
	}
}

static void
galeon_sidebar_embed_map (GtkWidget *widget)
{
	/* Delay creating the embed till the sidebar is mapped, as
	 * the GtkMozEmbed widget crashes if we never realize it, by
	 * ensureing we don't have an embed when hidden, no unncessary
	 * reloads happen either */

	if (GTK_BIN (widget)->child == NULL)
	{
		galeon_sidebar_embed_create_embed (GALEON_SIDEBAR_EMBED (widget));
	}

	GTK_WIDGET_CLASS (galeon_sidebar_embed_parent_class)->map (widget);
}

static void
galeon_sidebar_embed_unmap (GtkWidget *widget)
{
	if (GTK_BIN (widget)->child)
	{
		gtk_widget_destroy (GTK_BIN (widget)->child);
	}

	GTK_WIDGET_CLASS (galeon_sidebar_embed_parent_class)->unmap (widget);
}

static void
galeon_sidebar_embed_unrealize (GtkWidget *widget)
{
	if (GTK_BIN (widget)->child)
	{
		gtk_widget_destroy (GTK_BIN (widget)->child);
	}

	GTK_WIDGET_CLASS (galeon_sidebar_embed_parent_class)->unrealize (widget);
}


static void
galeon_sidebar_embed_init (GaleonSidebarEmbed *sbembed)
{
	sbembed->priv = GALEON_SIDEBAR_EMBED_GET_PRIVATE (sbembed);
}

static void
galeon_sidebar_embed_finalize (GObject *object)
{
	GaleonSidebarEmbed *sbembed = GALEON_SIDEBAR_EMBED (object);

	g_free (sbembed->priv->url);

	G_OBJECT_CLASS (galeon_sidebar_embed_parent_class)->finalize (object);
}

static void
galeon_sidebar_embed_class_init (GaleonSidebarEmbedClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
	
	object_class->finalize = galeon_sidebar_embed_finalize;

	widget_class->size_allocate = galeon_sidebar_embed_size_allocate;
	widget_class->map           = galeon_sidebar_embed_map;
	widget_class->unmap         = galeon_sidebar_embed_unmap;
	widget_class->unrealize     = galeon_sidebar_embed_unrealize;

	g_type_class_add_private (klass, sizeof (GaleonSidebarEmbedPrivate));
}

GtkWidget *
galeon_sidebar_embed_new (void)
{
	return GTK_WIDGET (g_object_new (GALEON_TYPE_SIDEBAR_EMBED, NULL));
}
