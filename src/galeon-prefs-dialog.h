/*
 *  Copyright (C) 2003  Tommi Komulainen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation. 
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef GALEON_PREFS_DIALOG_H
#define GALEON_PREFS_DIALOG_H 1

#include <gtk/gtkdialog.h>

#define GALEON_TYPE_PREFS_DIALOG  (galeon_prefs_dialog_get_type ())
#define GALEON_PREFS_DIALOG(o)	  (G_TYPE_CHECK_INSTANCE_CAST((o), GALEON_TYPE_PREFS_DIALOG, GaleonPrefsDialog))
#define GALEON_IS_PREFS_DIALOG(o) (G_TYPE_CHECK_INSTANCE_TYPE((o), GALEON_TYPE_PREFS_DIALOG))

typedef struct _GaleonPrefsDialog        GaleonPrefsDialog;
typedef struct _GaleonPrefsDialogClass   GaleonPrefsDialogClass;
typedef struct _GaleonPrefsDialogPrivate GaleonPrefsDialogPrivate;

typedef enum {
	GALEON_PREFS_CLEAR_CACHE,
	GALEON_PREFS_MANAGE_CERTIFICATES,
	GALEON_PREFS_MANAGE_DEVICES,
	GALEON_PREFS_MANAGE_COOKIES,
	GALEON_PREFS_MANAGE_IMAGES,
	GALEON_PREFS_MANAGE_PASSWORDS,
	GALEON_PREFS_MANAGE_POPUPS,
} GaleonPrefsRequest;

struct _GaleonPrefsDialog
{
	GtkDialog	parent;

	GaleonPrefsDialogPrivate *priv;
};

struct _GaleonPrefsDialogClass
{
	GtkDialogClass	parent_class;

	/* signals */

	void (* request)   (GaleonPrefsDialog  *self,
			    GaleonPrefsRequest  request);

	char* (* location) (GaleonPrefsDialog *self);
};

GType		 galeon_prefs_dialog_get_type	(void) G_GNUC_CONST;

GtkWidget	*galeon_prefs_dialog_new	(void);

#endif /* GALEON_PREFS_DIALOG_H */
