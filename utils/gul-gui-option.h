/*
 *  Copyright (C) 2003  Tommi Komulainen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation. 
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */ 

#ifndef GUL_GUI_OPTION_H
#define GUL_GUI_OPTION_H 1

#include <gtk/gtkcombobox.h>

G_BEGIN_DECLS

typedef struct _GulGuiOption GulGuiOption;
struct _GulGuiOption
{
	/*< public >*/
	char *title;
	char *value;
	
	/*< protected >*/
	char *key;
};

enum {
	OPTION_COL_TITLE,
	OPTION_COL_VALUE,
	OPTION_NUM_COLS
};

GulGuiOption *gul_gui_option_new                (const char *title,
						 const char *value);

void          gul_gui_option_combobox_populate  (GtkComboBox *combobox,
						 const GulGuiOption *options,
						 guint n_options,
						 gboolean sortable,
	                                         gboolean translate);

gchar        *gul_gui_option_combobox_get_value (GtkComboBox *combobox);

void          gul_gui_option_combobox_set_value (GtkComboBox *combobox,
		                                 const gchar *value);

G_END_DECLS

#endif /* GUL_GUI_OPTION_H */
