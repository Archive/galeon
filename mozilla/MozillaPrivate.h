/*
 *  Copyright (C) 2004 Galeon Developers
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#ifndef MOZILLA_PRIVATE_H
#define MOZILLA_PRIVATE_H

#include <nsError.h>
#include <glib.h>

#include "galeon-embed.h"

class nsIDocShell;
class nsIDOMWindow;
class nsIDOMNSDocument;
class nsIURI;
class nsIStyleSheet;
class nsAString;

namespace MozillaPrivate
{

	GList * GetPrinterList (gint* defaultPrintIndex);

	nsresult LoadOverrideStyleSheet (nsIDocShell *aDocShell, nsIURI *aUri, 
					 nsIStyleSheet**aStyleSheet);

	nsresult RemoveOverrideStyleSheet (nsIDocShell *aDocShell,
#ifdef HAVE_NSISTYLESHEETSERVICE_H
					   nsIURI *aUri);
#else
					   nsIStyleSheet*aStyleSheet);
#endif

	nsresult EvaluateJS (nsIDOMWindow *aWindow, const nsAString& aScript,
			     nsAString &aResult, PRBool *isUndefined);

        // Until https://bugzilla.mozilla.org/show_bug.cgi?id=154359 is fixed.
	nsresult GetCompatibilityMode (nsIDOMNSDocument *aDoc,
				       EmbedPageRenderMode *aMode);
}

#endif
