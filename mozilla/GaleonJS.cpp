/*
 *  Copyright (C) 2006 Galeon Developers
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* IMPORTANT: This file contains functions that require the old style
 * string api, i.e those that CAN'T use nsEmbedString
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_NSISCRIPTCONTEXT_INTERNAL_API
#define MOZILLA_INTERNAL_API
#endif
#include <nscore.h>
#include <nsIDOMWindow.h>
//#include <nsStringAPI.h>
#include <nsIScriptGlobalObject.h>
#include <nsIScriptContext.h>
#undef MOZILLA_INTERNAL_API

#include "GaleonJS.h"

nsresult
GaleonJS::EvaluateJS (nsIDOMWindow *aDOMWindow,
		      const PRUnichar *aRawScript,
		      PRUnichar **result,
		      PRBool *isUndefined)
{
	nsresult rv;

	nsCOMPtr<nsIScriptGlobalObject> globalObject = 
		do_QueryInterface (aDOMWindow);
	NS_ENSURE_TRUE (globalObject, NS_ERROR_FAILURE);

	nsIScriptContext *context = globalObject->GetContext();
	NS_ENSURE_TRUE (context, NS_ERROR_FAILURE);

	context->SetProcessingScriptTag(PR_TRUE);

	*isUndefined = PR_TRUE;

        nsDependentString aScript(aRawScript);
	nsAutoString ret;
	rv = context->EvaluateString(aScript, nsnull, nsnull, nsnull,
				     0, nsnull, &ret, isUndefined);  

	context->SetProcessingScriptTag(PR_FALSE);
	NS_ENSURE_SUCCESS (rv, rv);
	
	*result = ToNewUnicode(ret);
	return NS_OK;
}
