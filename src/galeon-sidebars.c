/*
 *  Copyright (C) 2004 Crispin Flowerday
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "galeon-sidebars.h"
#include "galeon-window.h"
#include "galeon-shell.h"
#include "galeon-sidebar-embed.h"
#include "galeon-session.h"
#include "gul-general.h"
#include "galeon-config.h"
#include "hig-alert.h"

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlmemory.h>

#include <string.h>

#include <glib/gi18n.h>

#define GALEON_SIDEBARS_GET_PRIVATE(object) (G_TYPE_INSTANCE_GET_PRIVATE ((object), \
				       GALEON_TYPE_SIDEBARS, GaleonSidebarsPrivate))

struct _GaleonSidebarsPrivate
{
	GaleonEmbedShell *embed_shell;
	char * filename;

	GList * sidebars;
};

enum
{
	PROP_0,
	PROP_EMBED_SHELL
};

G_DEFINE_TYPE (GaleonSidebars, galeon_sidebars, G_TYPE_OBJECT);

typedef struct 
{
	char * url;
	char * title;
} SidebarInfo;

static void
free_sidebar_info (SidebarInfo *info)
{
	g_free (info->url);
	g_free (info->title);
	g_free (info);
}


// ------------------------------------------------------------
struct ResponseCallbackData
{
	char *url;
	char *title;
	GaleonSidebars* sidebars;
};

static void
free_response_data (struct ResponseCallbackData *data)
{
	g_free (data->url);
	g_free (data->title);
	g_free (data);
}


static void
add_dialog_response_cb (GtkDialog *dialog,
			int response, 
			struct ResponseCallbackData *data)
{
	GaleonSidebarsPrivate * p = data->sidebars->priv;

	if (response == GTK_RESPONSE_ACCEPT)
	{
		const GList *windows, *li;
		Session *session;
		SidebarInfo *info;
		
		session = galeon_shell_get_session (galeon_shell);
		windows = session_get_windows(session);

		info = g_new0 (SidebarInfo, 1);
		info->url = g_strdup (data->url);
		info->title = g_strdup (data->title);

		for (li = windows ; li ; li = li->next)
		{
			GaleonSidebar *sidebar;
			if (!GALEON_IS_WINDOW(li->data)) continue;
			
			sidebar = galeon_window_get_sidebar (GALEON_WINDOW(li->data));
			galeon_sidebar_add_page (sidebar, info->title, 
						 info->url, TRUE);
		}
		p->sidebars = g_list_append (p->sidebars, info);
	}

	gtk_widget_destroy (GTK_WIDGET (dialog));
}

static void
embed_shell_add_sidebar_cb (GaleonEmbedShell *embed_shell, 
			    const char * url, const char * title,
			    GaleonSidebars *sidebars)
{
	GaleonSidebarsPrivate * p = sidebars->priv;
	GtkWidget *dialog;
	GaleonWindow *window;
	GList *li;
	struct ResponseCallbackData *info;

	for (li = p->sidebars; li; li = li->next)
	{
		SidebarInfo *info = li->data;
		if (strcmp (info->url, url) == 0)
		{
			return;
		}
	}

	window = galeon_shell_get_active_window (galeon_shell);

	dialog = hig_alert_new (GTK_WINDOW (window), 
				GTK_DIALOG_DESTROY_WITH_PARENT,
				HIG_ALERT_CONFIRMATION,
				NULL, NULL,
				GTK_STOCK_CANCEL,
				GTK_RESPONSE_CANCEL,
				_("_Add Sidebar"),
				GTK_RESPONSE_ACCEPT,
				NULL);
	
	hig_alert_set_primary_printf (HIG_ALERT (dialog),
				      _("Add \"%s\" to the Sidebar?"), title);

	hig_alert_set_secondary_printf (HIG_ALERT (dialog),
					_("The source to the new sidebar page is %s."), url);

	info = g_new0 (struct ResponseCallbackData, 1);
	info->url   = g_strdup (url);
	info->title = g_strdup (title);
	info->sidebars = sidebars;

	g_signal_connect_data (dialog, "response", 
			       G_CALLBACK (add_dialog_response_cb),
			       info, (GClosureNotify) free_response_data,
			       0);

	gtk_widget_show (GTK_WIDGET (dialog));
}


static void
galeon_sidebars_set_embed_shell (GaleonSidebars *sidebars, GaleonEmbedShell *embed_shell)
{
	sidebars->priv->embed_shell = embed_shell;

	g_signal_connect_object (embed_shell, "add-sidebar",
				 G_CALLBACK (embed_shell_add_sidebar_cb),
				 sidebars, 0);
}

static void 
sidebar_remove_request_cb (GaleonSidebar *sidebar, 
			   const char *page_id, 
			   GaleonSidebars *sidebars)
{
	GaleonSidebarsPrivate * p = sidebars->priv;
	SidebarInfo *sidebar_info = NULL;
	GList *windows, *li;
	Session *session;

	for (li = p->sidebars; li; li = li->next)
	{
		SidebarInfo *info = li->data;
		if (strcmp (info->url, page_id) == 0)
		{
			sidebar_info = info;
			break;
		}
	}

	if (!sidebar_info) return;

	session = galeon_shell_get_session(galeon_shell);
	if (!session) return;

	windows = session_get_windows(session);

	for (li = windows ; li ; li = li->next)
	{
		GaleonSidebar *sidebar;
		if (!GALEON_IS_WINDOW(li->data)) continue;

		sidebar = galeon_window_get_sidebar (GALEON_WINDOW(li->data));
		galeon_sidebar_remove_page (sidebar, page_id);
	}
	
	p->sidebars = g_list_remove (p->sidebars, sidebar_info);
	free_sidebar_info (sidebar_info);
}


static void
sidebar_page_changed_cb (GaleonSidebar *sidebar,
			 const char *page_id,
			 GaleonSidebars *sidebars)
{
	GaleonSidebarsPrivate * p = sidebars->priv;
	gboolean found = FALSE;
	GtkWidget *sidebar_embed;
	GList *li;

	for (li = p->sidebars; li; li = li->next)
	{
		SidebarInfo *info = li->data;
		if (strcmp (info->url, page_id) == 0)
		{
			found = TRUE;
			break;
		}
	}

	sidebar_embed = g_object_get_data (G_OBJECT (sidebar), "galeon-sidebar-embed");
	if (!sidebar_embed)
	{
		sidebar_embed = galeon_sidebar_embed_new ();
		g_object_ref (sidebar_embed);
		gtk_object_sink (GTK_OBJECT (sidebar_embed));
		g_object_set_data_full (G_OBJECT (sidebar), "galeon-sidebar-embed",
				   sidebar_embed, (GDestroyNotify)g_object_unref);
		gtk_widget_show (GTK_WIDGET (sidebar_embed));
	}
	galeon_sidebar_embed_set_url (GALEON_SIDEBAR_EMBED (sidebar_embed),
				      found ? page_id : NULL );
	if (found)
	{
		galeon_sidebar_set_content (sidebar, G_OBJECT (sidebar_embed));
	}
}

void
galeon_sidebars_init_window_sidebar (GaleonSidebars *sidebars, GaleonSidebar *sidebar)
{
	GaleonSidebarsPrivate * p = sidebars->priv;
	GList *li;

	for (li = p->sidebars; li; li = li->next)
	{
		SidebarInfo *info = li->data;
		
		galeon_sidebar_add_page(sidebar,
					info->title, info->url, TRUE);
	}

	g_signal_connect_object (sidebar, "remove_requested",
				 G_CALLBACK (sidebar_remove_request_cb),
				 sidebars, 0);
	g_signal_connect_object (sidebar, "page_changed",
				 G_CALLBACK (sidebar_page_changed_cb),
				 sidebars, 0);
}

static void
galeon_sidebars_save (GaleonSidebars *sidebars)
{
	GaleonSidebarsPrivate * p = sidebars->priv;
	GList *li;


	/* version doesn't really make sense, but... */
        xmlDocPtr doc = xmlNewDoc ((const xmlChar*)"1.0");

	/* create and set the root node for the session */
        xmlNodePtr root_node = xmlNewDocNode (doc, NULL, (const xmlChar*)"sidebars", NULL);

        xmlDocSetRootElement (doc, root_node);

	for (li = p->sidebars ; li ; li = li->next)
	{
		SidebarInfo *info = li->data;

		/* make a new XML node */
		xmlNodePtr page_node = xmlNewDocNode (doc, NULL, (const xmlChar*)"page", NULL);

		/* fill out fields */
		xmlSetProp (page_node, (const xmlChar*)"description", (const xmlChar*)info->title);
		xmlSetProp (page_node, (const xmlChar*)"url", (const xmlChar*)info->url);
		xmlAddChild (root_node, page_node);
	}

	/* save it all out to disk */
	gul_general_safe_xml_save (p->filename, doc);
	xmlFreeDoc (doc);
}


static void
galeon_sidebars_load (GaleonSidebars *sidebars)
{
	GaleonSidebarsPrivate * p = sidebars->priv;
	xmlNodePtr page;
	xmlDocPtr doc;

	/* check the file exists */
	if (!(g_file_test (p->filename, G_FILE_TEST_EXISTS)))
	{
		return;
	}

	/* load the file */
	doc = xmlParseFile (p->filename);
	if (doc == NULL) 
	{
		g_warning ("Unable to parse `%s', no sidebars loaded.",
			   p->filename);
		return;
	}

	/* iterate over sidebar pages in document */
        for (page = doc->children->children;
             page != NULL; page = page->next)
	{
		SidebarInfo *info;

		xmlChar *desc = xmlGetProp (page, (const xmlChar*)"description");
		xmlChar *url  = xmlGetProp (page, (const xmlChar*)"url");

		if (desc && url)
		{
			info = g_new0 (SidebarInfo, 1);
			info->url   = g_strdup ((char*)url);
			info->title = g_strdup ((char*)desc);
		
			p->sidebars = g_list_prepend (p->sidebars, info);
		}

		if (desc) xmlFree (desc);
		if (url) xmlFree (url);
	}
	xmlFreeDoc (doc);

	p->sidebars = g_list_reverse (p->sidebars);
}

static void
galeon_sidebars_set_property (GObject *object,
			      guint prop_id,
			      const GValue *value,
			      GParamSpec *pspec)
{
	GaleonSidebars *m = GALEON_SIDEBARS (object);

	switch (prop_id)
	{
	case PROP_EMBED_SHELL:
		{
			galeon_sidebars_set_embed_shell 
				(m, GALEON_EMBED_SHELL (g_value_get_object (value)));
		}
		break;
	default: 
		break;
	}
}

static void
galeon_sidebars_get_property (GObject *object,
			      guint prop_id,
			      GValue *value,
			      GParamSpec *pspec)
{
	GaleonSidebars *m = GALEON_SIDEBARS (object);
	
	switch (prop_id)
	{
	case PROP_EMBED_SHELL:
		g_value_set_object (value, m->priv->embed_shell);
		break;
	default: 
		break;
	}
}



static void
galeon_sidebars_finalize (GObject *o)
{
	GaleonSidebars *sidebars = GALEON_SIDEBARS (o);

	galeon_sidebars_save (sidebars);

	g_list_foreach (sidebars->priv->sidebars, (GFunc)free_sidebar_info, NULL);
	g_list_free (sidebars->priv->sidebars);
	g_free (sidebars->priv->filename);

	G_OBJECT_CLASS(galeon_sidebars_parent_class)->finalize (o);
}



static void
galeon_sidebars_init (GaleonSidebars *sidebars)
{
	GaleonSidebarsPrivate *p = GALEON_SIDEBARS_GET_PRIVATE (sidebars);
	sidebars->priv = p;


	sidebars->priv->filename = g_build_filename(g_get_home_dir(),
						    GALEON_DIR"/sidebars.xml",
						    NULL);
	galeon_sidebars_load (sidebars);
}


static void
galeon_sidebars_class_init (GaleonSidebarsClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->set_property = galeon_sidebars_set_property;
	object_class->get_property = galeon_sidebars_get_property;
	object_class->finalize = galeon_sidebars_finalize;
	
	g_object_class_install_property (object_class,
                                         PROP_EMBED_SHELL,
                                         g_param_spec_object ("EmbedShell",
                                                              "EmbedShell",
                                                              "Galeon embed shell",
                                                              GALEON_TYPE_EMBED_SHELL,
                                                              G_PARAM_READWRITE |
							      G_PARAM_CONSTRUCT_ONLY));


	g_type_class_add_private (klass, sizeof (GaleonSidebarsPrivate));
}


GaleonSidebars*
galeon_sidebars_new (GaleonEmbedShell *embed_shell)
{
	GaleonSidebars *ret = g_object_new (GALEON_TYPE_SIDEBARS,
					    "EmbedShell", embed_shell, NULL);
	return ret;
}
