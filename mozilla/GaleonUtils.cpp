/*
 *  Copyright (C) 2004 Crispin Flowerday
 *  Copyright (C) 2004 Philip Langdale
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include <nscore.h>
#include <nsServiceManagerUtils.h>

#include "GaleonUtils.h"
#include "GulString.h"

#include <nsIIOService.h>
#include <nsIURI.h>
#include <nsIPrefService.h>
#include <nsIPrintSettingsService.h>
#include <nsIPrintSettings.h>
#include <nsIWindowWatcher.h>
#include <nsIEmbeddingSiteWindow.h>
#include <nsIWebBrowserChrome.h>
#include <nsIDOMWindow.h>
#include <nsMemory.h>

#include <glib/gi18n.h>
#include <langinfo.h>

nsresult
GaleonUtils::GetIOService (nsIIOService **ioService)
{
	nsresult rv;

	nsCOMPtr<nsIServiceManager> mgr; 
	NS_GetServiceManager (getter_AddRefs (mgr));
	if (!mgr) return NS_ERROR_FAILURE;

	rv = mgr->GetServiceByContractID ("@mozilla.org/network/io-service;1",
					  NS_GET_IID (nsIIOService),
					  (void **)ioService);
	return rv;
}

nsresult
GaleonUtils::NewURI (nsIURI **result,
		   const nsAString &spec,
		   const char *charset,
		   nsIURI *baseURI)
{
	nsEmbedCString cSpec;
	NS_UTF16ToCString (spec, NS_CSTRING_ENCODING_UTF8, cSpec);

	return NewURI (result, cSpec, charset, baseURI);
}

nsresult
GaleonUtils::NewURI (nsIURI **result,
		   const nsACString &spec,
		   const char *charset,
		   nsIURI *baseURI)
{
	nsresult rv;

	nsCOMPtr<nsIIOService> ioService;
	rv = GaleonUtils::GetIOService (getter_AddRefs (ioService));
	NS_ENSURE_SUCCESS (rv, rv);

	rv = ioService->NewURI (spec, charset, baseURI, result);

	return rv;
}


GtkWidget *
GaleonUtils::FindEmbed (nsIDOMWindow *aDOMWindow)
{
	nsresult rv;

	nsCOMPtr<nsIWindowWatcher> wwatch
		(do_GetService("@mozilla.org/embedcomp/window-watcher;1"));
	NS_ENSURE_TRUE (wwatch, nsnull);

	nsCOMPtr<nsIDOMWindow> domWindow;
	if (aDOMWindow)
	{
		/* Get the top-level Window - bug 130425 */
		aDOMWindow->GetTop (getter_AddRefs (domWindow));
	}
	else
	{
		wwatch->GetActiveWindow(getter_AddRefs(domWindow));
	}
	NS_ENSURE_TRUE (domWindow, nsnull);

	nsCOMPtr<nsIWebBrowserChrome> windowChrome;
	wwatch->GetChromeForWindow(domWindow, getter_AddRefs(windowChrome));
	NS_ENSURE_TRUE (windowChrome, nsnull);
	
	nsCOMPtr<nsIEmbeddingSiteWindow> window (do_QueryInterface (windowChrome));
	NS_ENSURE_TRUE (window, nsnull);
	
	GtkWidget *mozembed;
	rv = window->GetSiteWindow((void **)&mozembed);
	NS_ENSURE_SUCCESS (rv, nsnull);

	return GTK_WIDGET(mozembed);
}

GtkWidget *
GaleonUtils::FindGtkParent (nsIDOMWindow *aDOMWindow)
{
	GtkWidget *embed = GaleonUtils::FindEmbed (aDOMWindow);
	NS_ENSURE_TRUE (embed, nsnull);

	return gtk_widget_get_toplevel (GTK_WIDGET (embed));
}

nsresult
GaleonUtils::PrintSettingsToEmbedPrintInfo(nsIPrintSettings *aPrintSettings,
					   EmbedPrintInfo **aPrintInfo)
{
	nsresult rv;
	EmbedPrintInfo *info = g_new0(EmbedPrintInfo, 1);

	info->paper_array = g_ptr_array_new();
	info->paper_desc_array = g_ptr_array_new();

	PRUnichar *tmp;

	aPrintSettings->GetPrinterName (&tmp);
	info->name = g_strdup (GulCString(tmp).get());
	nsMemory::Free (tmp);

	aPrintSettings->GetPrintCommand (&tmp);
	info->command = g_strdup (GulCString (tmp).get());
	nsMemory::Free (tmp);

	aPrintSettings->GetPrintToFile (&info->print_to_file);

	GulCString printPrefix("print.tmp.printerfeatures.");
	printPrefix.Append (info->name);

	nsCOMPtr<nsIPrefService> prefService =
		do_GetService(NS_PREFSERVICE_CONTRACTID);
	NS_ENSURE_TRUE (prefService, NS_ERROR_FAILURE);

	nsCOMPtr<nsIPrefBranch> pref;
	rv = prefService->GetBranch (printPrefix.get(), getter_AddRefs(pref));
	NS_ENSURE_SUCCESS(rv, rv);

	rv = pref->GetBoolPref(".can_change_spoolercommand",
			       &info->can_change_command);
	NS_ENSURE_SUCCESS(rv, rv);

	gint numPapers;
	rv = pref->GetIntPref(".paper.count", &numPapers);
	for (int i = 0; i < numPapers; i++)
	{
		char * name;
		char * key = g_strdup_printf (".paper.%d.name", i);
		rv = pref->GetCharPref (key, &name);
		g_free (key);

		g_ptr_array_add (info->paper_array, g_strdup (name));

		PRInt32 height, width;

		key = g_strdup_printf (".paper.%d.height_mm", i);
		rv = pref->GetIntPref (key, &height);
		g_free (key);

		key = g_strdup_printf (".paper.%d.width_mm", i);
		rv = pref->GetIntPref (key, &width);
		g_free (key);

#ifdef HAVE__NL_PAPER_WIDTH_HEIGHT
		// mark locale default paper size so that print dialog can use
		// it as best guess when switching printers
		if (height == (int) (long int) nl_langinfo (_NL_PAPER_HEIGHT) &&
		    width == (int) (long int) nl_langinfo (_NL_PAPER_WIDTH))
		{
			info->paper = i;
		}
#endif

		PRBool is_inch;
		key = g_strdup_printf (".paper.%d.is_inch", i);
		rv = pref->GetBoolPref (key, &is_inch);
		g_free (key);

		char * paper;
		if (is_inch)
		{
			double scale(25.4);
			paper = g_strdup_printf (_(" (%.1f x %.1f inch)"),
						width/scale, height/scale);
		}
		else
		{
			paper = g_strdup_printf (_(" (%d x %d mm)"), width, height);
		}
		
		g_ptr_array_add(info->paper_desc_array, 
				g_strconcat (name, paper, NULL));

		g_free (paper);
		nsMemory::Free (name);
	}

	*aPrintInfo = info;

	return NS_OK;
}
