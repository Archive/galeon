/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "galeon-popup.h"
#include <gtk/gtkaction.h>

void popup_cmd_new_window		(GtkAction *action, 
					 GaleonPopup *popup); 

void popup_cmd_open_link		(GtkAction *action, 
					 GaleonPopup *popup); 

void popup_cmd_new_tab			(GtkAction *action, 
					 GaleonPopup *popup);

void popup_cmd_image_in_new_tab		(GtkAction *action, 
					 GaleonPopup *popup);

void popup_cmd_image_in_new_window	(GtkAction *action, 
					 GaleonPopup *popup);

void popup_cmd_image_block_site		(GtkAction *action, 
					 GaleonPopup *popup);

void popup_cmd_add_bookmark		(GtkAction *action, 
					 GaleonPopup *popup);

void popup_cmd_frame_in_new_tab		(GtkAction *action, 
					 GaleonPopup *popup);

void popup_cmd_frame_in_new_window	(GtkAction *action, 
					 GaleonPopup *popup);

void popup_cmd_reload_frame		(GtkAction *action, 
					 GaleonPopup *popup);

void popup_cmd_view_source		(GtkAction *action, 
					 GaleonPopup *popup);

void popup_copy_text_cmd                (GtkAction *action, 
					 GaleonPopup *popup);

void popup_copy_location_cmd             (GtkAction *action, 
					  GaleonPopup *popup);

void popup_copy_email_cmd                (GtkAction *action, 
					  GaleonPopup *popup);

void popup_copy_link_location_cmd        (GtkAction *action,
					  GaleonPopup *popup);

void popup_download_link_cmd             (GtkAction *action, 
					  GaleonPopup *popup);

void popup_save_image_as_cmd             (GtkAction *action, 
					  GaleonPopup *popup);

void popup_set_image_as_background_cmd   (GtkAction *action, 
					  GaleonPopup *popup);

void popup_copy_image_location_cmd       (GtkAction *action, 
					  GaleonPopup *popup);

void popup_save_page_as_cmd              (GtkAction *action, 
					  GaleonPopup *popup);

void popup_save_background_as_cmd        (GtkAction *action, 
					  GaleonPopup *popup);

void popup_open_frame_cmd                (GtkAction *action, 
					  GaleonPopup *popup);

void popup_open_image_cmd                (GtkAction *action, 
					  GaleonPopup *popup);
