/*
 *  Copyright (C) 2005 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef GALEON_EMBED_FIND_H
#define GALEON_EMBED_FIND_H

#include <glib-object.h>
#include <glib.h>

#include "galeon-embed.h"
#include <gdk/gdktypes.h>

G_BEGIN_DECLS

#define GALEON_TYPE_EMBED_FIND		(galeon_embed_find_get_type ())
#define GALEON_EMBED_FIND(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), GALEON_TYPE_EMBED_FIND, GaleonEmbedFind))
#define GALEON_EMBED_FIND_IFACE(k)	(G_TYPE_CHECK_CLASS_CAST((k), GALEON_TYPE_EMBED_FIND, GaleonEmbedFindIface))
#define GALEON_IS_EMBED_FIND(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), GALEON_TYPE_EMBED_FIND))
#define GALEON_IS_EMBED_FIND_IFACE(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), GALEON_TYPE_EMBED_FIND))
#define GALEON_EMBED_FIND_GET_IFACE(inst)	(G_TYPE_INSTANCE_GET_INTERFACE ((inst), GALEON_TYPE_EMBED_FIND, GaleonEmbedFindIface))

typedef struct _GaleonEmbedFind		GaleonEmbedFind;
typedef struct _GaleonEmbedFindIface	GaleonEmbedFindIface;

/* Keep these the same as in nsITypeAheadFind */
typedef enum
{
	GALEON_EMBED_FIND_FOUND		= 0,
	GALEON_EMBED_FIND_NOTFOUND	= 1,
	GALEON_EMBED_FIND_FOUNDWRAPPED	= 2
} GaleonEmbedFindResult;

struct _GaleonEmbedFindIface
{
	GTypeInterface base_iface;

	/* Methods */
	void	 (* set_embed)		(GaleonEmbedFind *find,
					 GaleonEmbed *embed);
	void	 (* set_properties)	(GaleonEmbedFind *find,
					 const char *search_string,
					 gboolean case_sensitive);
	GaleonEmbedFindResult (* find)		(GaleonEmbedFind *find,
						 const char *search_string,
						 gboolean links_only);
	GaleonEmbedFindResult (* find_again)	(GaleonEmbedFind *find,
						 gboolean forward,
						 gboolean links_only);
	void     (* set_selection)      (GaleonEmbedFind *find,
					 gboolean attention);
	gboolean (* activate_link)	(GaleonEmbedFind *find,
					 GdkModifierType mask);

	void (*get_prefs)               (GaleonEmbedFind *find,
					 gboolean* autostart,
					 gboolean* links_only);

};

GType	 galeon_embed_find_get_type		(void);

GaleonEmbedFind  *galeon_embed_find_new         (void);

void	 galeon_embed_find_set_embed		(GaleonEmbedFind *find,
						 GaleonEmbed *embed);

void	 galeon_embed_find_set_properties		(GaleonEmbedFind *find,
						 const char *search_string,
						 gboolean case_sensitive);

GaleonEmbedFindResult	galeon_embed_find_find		(GaleonEmbedFind *find,
							 const char *search_string,
							 gboolean links_only);

GaleonEmbedFindResult	galeon_embed_find_find_again	(GaleonEmbedFind *find,
							 gboolean forward,
							 gboolean links_only);

void    galeon_embed_find_set_selection                (GaleonEmbedFind *find,
                                                gboolean attention);


gboolean galeon_embed_find_activate_link		(GaleonEmbedFind *find,
						 GdkModifierType mask);

void     galeon_embed_find_get_prefs            (GaleonEmbedFind *find,
						 gboolean *autostart,
						 gboolean *links_only);

G_END_DECLS

#endif
