/*
 * mozilla-cookie.cpp
 *
 * Copyright (C) 2003 Tommi Komulainen <tommi.komulainen@iki.fi>
 * Available under the terms of the GNU General Public License version 2.
 */ 

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mozilla-cookie.h"
#include "GulString.h"

#include <nsICookie.h>

#include <ctime>
#include <glib/gi18n.h>

CookieInfo *
mozilla_cookie_to_info (nsICookie *aCookie)
{
	CookieInfo *info = g_new0(CookieInfo, 1);
	GulCString str;
	PRUint64 expires;

	(void)aCookie->GetHost (str);
	info->domain = g_strdup (str.get());

	(void)aCookie->GetName (str);
	info->name = g_strdup (str.get());

	(void)aCookie->GetValue (str);
	info->value = g_strdup (str.get());

	(void)aCookie->GetPath (str);
	info->path = g_strdup (str.get());

	(void)aCookie->GetIsSecure(&info->secure);

	(void)aCookie->GetExpires(&expires);
	if (expires == 0)
	{
		info->expire = g_strdup (_("End of current session"));
	}
	else
	{
		time_t t = (time_t)expires;	// PRTime is PRInt64 is time_t
		struct tm tm;
		char buf[64];

		strftime (buf, sizeof(buf), _("%a, %b %-d %Y at %-I:%M %p"), 
			  localtime_r (&t, &tm));
		info->expire = g_locale_to_utf8 (buf, -1, NULL, NULL, NULL);
	}

	return info;
}

