/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __bookmarks_context_menu_several_h
#define __bookmarks_context_menu_several_h

#include <glib-object.h>
#include "bookmarks.h"
#include "bookmarks-util.h"
#include "bookmarks-location-source.h"

/* object forward declarations */

typedef struct _GbContextMenuSeveral GbContextMenuSeveral;
typedef struct _GbContextMenuSeveralClass GbContextMenuSeveralClass;
typedef struct _GbContextMenuSeveralPrivate GbContextMenuSeveralPrivate;

/**
 * Context menu object
 */

#define GB_TYPE_CONTEXT_MENU_SEVERAL			(gb_context_menu_several_get_type())
#define GB_CONTEXT_MENU_SEVERAL(object)			(G_TYPE_CHECK_INSTANCE_CAST((object), \
							 GB_TYPE_CONTEXT_MENU_SEVERAL,\
							 GbContextMenuSeveral))
#define GB_CONTEXT_MENU_SEVERAL_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), \
							 GB_TYPE_CONTEXT_MENU_SEVERAL,\
							 GbContextMenuSeveralClass))
#define GB_IS_CONTEXT_MENU_SEVERAL(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), \
							 GB_TYPE_CONTEXT_MENU_SEVERAL))
#define GB_IS_CONTEXT_MENU_SEVERAL_CLASS(klass)		(G_TYPE_CHECK_CLASS_TYPE((klass), \
							 GB_TYPE_CONTEXT_MENU_SEVERAL))
#define GB_CONTEXT_MENU_SEVERAL_GET_CLASS(obj)		(G_TYPE_INSTANCE_GET_CLASS((obj), \
							 GB_TYPE_CONTEXT_MENU_SEVERAL,\
							 GbContextMenuSeveralClass))

struct _GbContextMenuSeveralClass 
{
	GObjectClass parent_class;

	/* signals */
	GbBookmarkActivatedCallback gb_context_menu_several_bookmark_activated; /* not used currently, 
										   but may be useful 
										   someday */
	void		(*gb_context_menu_several_deactivated)		(GbContextMenuSeveral *cm);

};

/* Remember: fields are public read-only */
struct _GbContextMenuSeveral
{
	GObject parent_object;

	GbContextMenuSeveralPrivate *priv;
};

GType			gb_context_menu_several_get_type		(void);
GbContextMenuSeveral *	gb_context_menu_several_new			(void);
void			gb_context_menu_several_set_bookmarks		(GbContextMenuSeveral *cm, 
									 GSList *l);
void			gb_context_menu_several_popup			(GbContextMenuSeveral *cm, 
									 GdkEventButton *event);
void			gb_context_menu_several_set_location_source	(GbContextMenuSeveral *cm, 
									 GbLocationSource *src);
GbContextMenuSeveral *	gb_context_menu_several_new_quick		(GSList *l, 
									 GbLocationSource *location_source, 
									 GObject *creator);
void			gb_context_menu_several_quick			(GSList *l, GdkEventButton *event,
									 GbLocationSource *location_source, 
									 GObject *creator);

#endif
