/*
 *  Copyright (C) 2001,2002,2003 Philip Langdale
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "CookiePromptService.h"
#include "ContentHandler.h"
#include "FilePicker.h"
#include "GlobalHistory.h"
#include "MyportalProtocolHandler.h"
#include "GeckoPrintService.h"
#include "GeckoPrintSession.h"
#include "ProgressListener.h"
#include "SideBarProxy.h"
#include "EphyPromptService.h"

#ifdef HAVE_NSIJSCONSOLESERVICE_H
#include "JSConsoleService.h"
#endif

#ifdef HAVE_MOZILLA_PSM
#include "GtkNSSDialogs.h"
#include "GtkNSSKeyPairDialogs.h"
#include "GtkNSSClientAuthDialogs.h"
#include "GtkNSSSecurityWarningDialogs.h"
#endif

#ifdef HAVE_NSIXULAPPINFO_H
#include <nsXULAppAPI.h>
#include "EphyXULAppInfo.h"
#endif

#include "ExternalProtocolService.h"
#include "GaleonAboutModule.h"

#ifdef HAVE_NSICLASSINFOIMPL_H
#include <nsIClassInfoImpl.h>
#endif
#include <nsIGenericFactory.h>
#include <nsIComponentRegistrar.h>
#include <nsICategoryManager.h>
#include <nsIComponentManager.h>
#include <nsCOMPtr.h>
#include <nsXPCOM.h>
#include <nsComponentManagerUtils.h>
#include <nsServiceManagerUtils.h>
#include <nsDocShellCID.h>
#include <nsXPCOMCID.h>
#include <nsXPCOMCIDInternal.h>

#include <glib.h>

NS_GENERIC_FACTORY_CONSTRUCTOR(GProgressListener)
NS_GENERIC_FACTORY_CONSTRUCTOR(GFilePicker)
NS_GENERIC_FACTORY_CONSTRUCTOR(GContentHandler)
NS_GENERIC_FACTORY_CONSTRUCTOR(MozGlobalHistory)
NS_GENERIC_FACTORY_CONSTRUCTOR(GeckoPrintService)
NS_GENERIC_FACTORY_CONSTRUCTOR(GeckoPrintSession)
NS_GENERIC_FACTORY_CONSTRUCTOR(GSidebarProxy)
NS_DECL_CLASSINFO(GSidebarProxy)
NS_GENERIC_FACTORY_CONSTRUCTOR(GCookiePromptService)
NS_GENERIC_FACTORY_CONSTRUCTOR(EphyPromptService)
#ifdef HAVE_MOZILLA_PSM
NS_GENERIC_FACTORY_CONSTRUCTOR(GtkNSSDialogs)
NS_GENERIC_FACTORY_CONSTRUCTOR(GtkNSSKeyPairDialogs)
NS_GENERIC_FACTORY_CONSTRUCTOR(GtkNSSClientAuthDialogs)
NS_GENERIC_FACTORY_CONSTRUCTOR(GtkNSSSecurityWarningDialogs)
#endif
#ifdef HAVE_NSSTRING_INTERNAL
NS_GENERIC_FACTORY_CONSTRUCTOR(GaleonAboutModule)
#endif
NS_GENERIC_FACTORY_CONSTRUCTOR(GMyportalProtocolHandler)
#ifndef XPCOM_GLUE
NS_GENERIC_FACTORY_CONSTRUCTOR(GExternalProtocolService)
#endif
#ifdef HAVE_NSIJSCONSOLESERVICE_H
NS_GENERIC_FACTORY_CONSTRUCTOR(JSConsoleService)
#endif
#ifdef HAVE_NSIXULAPPINFO_H
NS_GENERIC_FACTORY_CONSTRUCTOR(EphyXULAppInfo)
#endif

// NS_DOWNLOAD_CONTRACTID was renamed in mozilla 1.8b
#ifndef NS_TRANSFER_CONTRACTID
#define NS_TRANSFER_CONTRACTID NS_DOWNLOAD_CONTRACTID
#endif

static NS_METHOD
RegisterSidebar(nsIComponentManager *aCompMgr, nsIFile *aPath,
                const char *registryLocation, const char *componentType,
                const nsModuleComponentInfo *info)
{
	nsCOMPtr<nsICategoryManager> cm =
		do_GetService(NS_CATEGORYMANAGER_CONTRACTID);
	NS_ENSURE_TRUE (cm, NS_ERROR_FAILURE);

	return cm->AddCategoryEntry("JavaScript global property",
				    "sidebar", NS_SIDEBAR_CONTRACTID,
				    PR_FALSE, PR_TRUE, nsnull);
}

static const nsModuleComponentInfo sAppComps[] = {
	{
		G_PROGRESSDIALOG_CLASSNAME,
		G_PROGRESSDIALOG_CID,
		G_PROGRESSDIALOG_CONTRACTID,
		GProgressListenerConstructor
	},
	{
		G_PROGRESSDIALOG_CLASSNAME,
		G_PROGRESSDIALOG_CID,
		NS_TRANSFER_CONTRACTID,
		GProgressListenerConstructor
	},
#ifdef HAVE_NSIJSCONSOLESERVICE_H
	{	
		G_JS_CONSOLESERVICE_CLASSNAME,
		G_JSCONSOLESERVICE_CID,
		NS_JSCONSOLESERVICE_CONTRACTID,
		JSConsoleServiceConstructor
	},
#endif
	{
		G_FILEPICKER_CLASSNAME,
		G_FILEPICKER_CID,
		G_FILEPICKER_CONTRACTID,
		GFilePickerConstructor
	},
	{
		NS_IHELPERAPPLAUNCHERDLG_CLASSNAME,
		G_CONTENTHANDLER_CID,
		NS_IHELPERAPPLAUNCHERDLG_CONTRACTID,
		GContentHandlerConstructor
	},
#ifndef XPCOM_GLUE
	{
		G_EXTERNALPROTOCOLSERVICE_CLASSNAME,
		G_EXTERNALPROTOCOLSERVICE_CID,
		NS_EXTERNALPROTOCOLSERVICE_CONTRACTID,
		GExternalProtocolServiceConstructor
	},
#endif
	{
		GALEON_GLOBALHISTORY_CLASSNAME,
		GALEON_GLOBALHISTORY_CID,
		NS_GLOBALHISTORY2_CONTRACTID,
		MozGlobalHistoryConstructor
	},
	{
		GECKO_PRINT_SERVICE_CLASSNAME,
		GECKO_PRINT_SERVICE_IID,
		"@mozilla.org/embedcomp/printingprompt-service;1",
		GeckoPrintServiceConstructor
        },
        {
		GECKO_PRINT_SESSION_CLASSNAME,
		GECKO_PRINT_SESSION_IID,
		"@mozilla.org/gfx/printsession;1",
		GeckoPrintSessionConstructor
        },
	{
		G_SIDEBAR_CLASSNAME,
		G_SIDEBAR_CID,
		NS_SIDEBAR_CONTRACTID,
		GSidebarProxyConstructor,
		RegisterSidebar,
		nsnull, // No unregister func
		nsnull, // No factory destructor
		NS_CI_INTERFACE_GETTER_NAME(GSidebarProxy),
		nsnull, // No language helper
		&NS_CLASSINFO_NAME(GSidebarProxy),
		nsIClassInfo::DOM_OBJECT
	},
	{
		G_COOKIEPROMPTSERVICE_CLASSNAME,
		G_COOKIEPROMPTSERVICE_CID,
		G_COOKIEPROMPTSERVICE_CONTRACTID,
		GCookiePromptServiceConstructor
	},
#ifdef HAVE_MOZILLA_PSM
#ifdef HAVE_NSIBADCERTLISTENER_H
	{
		GTK_NSSDIALOGS_CLASSNAME,
		GTK_NSSDIALOGS_CID,
		NS_BADCERTLISTENER_CONTRACTID,
		GtkNSSDialogsConstructor
	},
#endif
	{
		GTK_NSSDIALOGS_CLASSNAME,
		GTK_NSSDIALOGS_CID,
		NS_CERTIFICATEDIALOGS_CONTRACTID,
		GtkNSSDialogsConstructor
	},
	{
		GTK_NSSCLIENTAUTHDIALOGS_CLASSNAME,
		GTK_NSSCLIENTAUTHDIALOGS_CID,
		NS_CLIENTAUTHDIALOGS_CONTRACTID,
		GtkNSSClientAuthDialogsConstructor
	},
	{
		GTK_NSSKEYPAIRDIALOGS_CLASSNAME,
		GTK_NSSKEYPAIRDIALOGS_CID,
		NS_GENERATINGKEYPAIRINFODIALOGS_CONTRACTID,
		GtkNSSKeyPairDialogsConstructor
	},
        {
                GTK_NSSSECURITYWARNINGDIALOGS_CLASSNAME,
                GTK_NSSSECURITYWARNINGDIALOGS_CID,
                NS_SECURITYWARNINGDIALOGS_CONTRACTID,
                GtkNSSSecurityWarningDialogsConstructor
        },
#endif
	{
		G_ABOUT_MYPORTAL_CLASSNAME,
		G_MYPORTAL_PROTOCOL_CID,
		G_ABOUT_MYPORTAL_CONTRACTID,
		GMyportalProtocolHandlerConstructor
	},
	{
		G_MYPORTAL_HANDLER_CLASSNAME,
		G_MYPORTAL_PROTOCOL_CID,
		G_MYPORTAL_HANDLER_CONTRACTID,
		GMyportalProtocolHandlerConstructor
	},
#ifdef HAVE_NSSTRING_INTERNAL
	{
		GALEON_ABOUT_TOPHER_CLASSNAME,
		GALEON_ABOUT_MODULE_CID,
		GALEON_ABOUT_TOPHER_CONTRACTID,
		GaleonAboutModuleConstructor
	},
	{
		GALEON_ABOUT_NETERROR_CLASSNAME,
		GALEON_ABOUT_MODULE_CID,
		GALEON_ABOUT_NETERROR_CONTRACTID,
		GaleonAboutModuleConstructor
	},
#endif
	{
		EPHY_PROMPT_SERVICE_CLASSNAME,
		EPHY_PROMPT_SERVICE_IID,
		"@mozilla.org/embedcomp/prompt-service;1",
		EphyPromptServiceConstructor
	},
#ifdef HAVE_NSINONBLOCKINGALERTSERVICE_H
	{
		EPHY_PROMPT_SERVICE_CLASSNAME,
		EPHY_PROMPT_SERVICE_IID,
		"@mozilla.org/embedcomp/nbalert-service;1",
		EphyPromptServiceConstructor
	},
#endif /* HAVE_NSINONBLOCKINGALERTSERVICE_H */
#ifdef HAVE_NSIXULAPPINFO_H
	{
		EPHY_XUL_APP_INFO_CLASSNAME,
		EPHY_XUL_APP_INFO_CID,
		XULAPPINFO_SERVICE_CONTRACTID,
		EphyXULAppInfoConstructor
	},
#endif
};

#define NS_MSGCONTENTPOLICY_CONTRACTID "@mozilla.org/messenger/content-policy;1"

static nsresult
remove_thunderbird_contentpolicy()
{
	nsCOMPtr<nsICategoryManager> cm;
	cm = do_GetService(NS_CATEGORYMANAGER_CONTRACTID);
	NS_ENSURE_TRUE (cm, NS_ERROR_FAILURE);

	cm->DeleteCategoryEntry("content-policy",
				NS_MSGCONTENTPOLICY_CONTRACTID, PR_TRUE);

	return NS_OK;
}

gboolean
mozilla_register_components (void)
{
	gboolean ret = TRUE;
	nsresult rv;

	nsCOMPtr<nsIComponentRegistrar> cr;
	rv = NS_GetComponentRegistrar(getter_AddRefs(cr));
	NS_ENSURE_SUCCESS(rv, rv);

	nsCOMPtr<nsIComponentManager> cm;
	NS_GetComponentManager (getter_AddRefs (cm));
	NS_ENSURE_TRUE (cm, FALSE);

	for (guint i = 0; i < G_N_ELEMENTS (sAppComps); i++)
	{
		nsCOMPtr<nsIGenericFactory> componentFactory;
		componentFactory = do_CreateInstance(NS_GENERICFACTORY_CONTRACTID);
		if(!componentFactory)
		{
			g_warning ("Failed to create a generic factory for %s\n", sAppComps[i].mDescription);
			ret = FALSE;
			continue;
		}
		rv = componentFactory->SetComponentInfo(&(sAppComps[i]));
		if (NS_FAILED(rv))
		{
			g_warning ("Failed to make a factory for %s\n", sAppComps[i].mDescription);

			ret = FALSE;
			continue;  // don't abort registering other components
		}

		rv = cr->RegisterFactory(sAppComps[i].mCID,
					 sAppComps[i].mDescription,
					 sAppComps[i].mContractID,
					 componentFactory);
		if (NS_FAILED(rv))
		{
			g_warning ("Failed to register %s\n", sAppComps[i].mDescription);

			ret = FALSE;
		}

		if (sAppComps[i].mRegisterSelfProc)
		{
			rv = sAppComps[i].mRegisterSelfProc (cm, nsnull, nsnull, nsnull, &sAppComps[i]);

			if (NS_FAILED (rv))
			{
				g_warning ("Failed to register-self for %s\n", sAppComps[i].mDescription);
				ret = FALSE;
			}
		}
	}

	// Thunderbird includes a fascist content policy - we don't need it
	remove_thunderbird_contentpolicy ();

	return ret;
}
