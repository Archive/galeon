/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __location_entry_h
#define __location_entry_h

#include <glib-object.h>
#include <gtk/gtkhbox.h>

#include "galeon-autocompletion.h"

/* object forward declarations */

typedef struct _GaleonLocationEntry GaleonLocationEntry;
typedef struct _GaleonLocationEntryClass GaleonLocationEntryClass;
typedef struct _GaleonLocationEntryPrivate GaleonLocationEntryPrivate;

/**
 * GaleonFolderTbWidget object
 */

#define GALEON_TYPE_LOCATION_ENTRY	(galeon_location_entry_get_type())
#define GALEON_LOCATION_ENTRY(object)	(G_TYPE_CHECK_INSTANCE_CAST((object), GALEON_TYPE_LOCATION_ENTRY,\
					 GaleonLocationEntry))
#define GALEON_LOCATION_ENTRY_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), GALEON_TYPE_LOCATION_ENTRY,\
					    GaleonLocationEntryClass))
#define GALEON_IS_LOCATION_ENTRY(object) (G_TYPE_CHECK_INSTANCE_TYPE((object), GALEON_TYPE_LOCATION_ENTRY))
#define GALEON_IS_LOCATION_ENTRY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), GALEON_TYPE_LOCATION_ENTRY))
#define GALEON_LOCATION_ENTRY_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), GALEON_TYPE_LOCATION_ENTRY,\
					      GaleonLocationEntryClass))

struct _GaleonLocationEntryClass
{
	GtkHBoxClass parent_class;
	
	/* signals */
	void		(*galeon_location_entry_url_activated)	(GaleonLocationEntry *w, const gchar *url, gboolean new_tab_or_window);
};

/* Remember: fields are public read-only */
struct _GaleonLocationEntry
{
	GtkHBox parent_object;

	GaleonLocationEntryPrivate *priv;
};

GType			galeon_location_entry_get_type		(void);
GaleonLocationEntry *	galeon_location_entry_new		(void);
void			galeon_location_entry_set_location	(GaleonLocationEntry *w, 
								 const gchar *new_location);
gchar *			galeon_location_entry_get_location	(GaleonLocationEntry *w);
void			galeon_location_entry_set_autocompletion (GaleonLocationEntry *w, 
								  GaleonAutocompletion *ac);
void			galeon_location_entry_edit		(GaleonLocationEntry *w);

void			galeon_location_entry_clear_history	(void);

void			galeon_location_entry_set_is_secure	(GaleonLocationEntry *entry,
								 gboolean is_secure);

#endif
