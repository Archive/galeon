/*
 * egg-dock-box.c
 *
 * Copyright (C) 2002 Gustavo Gir�ldez <gustavo.giraldez@gmx.net>
 * Copyright (C) 2003 Biswapesh Chattopadhyay <biswapesh_chatterjee@tcscal.co.in>
 * Copyright (C) 2003 Philip Langdale <philipl@mail.utexas.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <eggintl.h>
#include <string.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtkvbox.h>
#include <egg-macros.h>

#include "egg-dock-box.h"

struct _EggDockBoxPrivate {
	gpointer unused;
};

/* Private prototypes */

static void     egg_dock_box_class_init     (EggDockBoxClass *klass);
static void     egg_dock_box_instance_init  (EggDockBox      *box);
static GObject *egg_dock_box_constructor    (GType              type,
                                               guint              n_construct_properties,
                                               GObjectConstructParam *construct_param);
static void     egg_dock_box_destroy        (GtkObject         *object);

static void     egg_dock_box_add            (GtkContainer      *container,
                                               GtkWidget         *widget);
static void     egg_dock_box_forall         (GtkContainer      *container,
                                               gboolean           include_internals,
                                               GtkCallback        callback,
                                               gpointer           callback_data);
static GType    egg_dock_box_child_type     (GtkContainer      *container);

static gboolean egg_dock_box_dock_request   (EggDockObject     *object, 
                                               gint               x,
                                               gint               y, 
                                               EggDockRequest    *request);
static void     egg_dock_box_dock           (EggDockObject    *object,
                                               EggDockObject    *requestor,
                                               EggDockPlacement  position,
                                               GValue           *other_data);

static void     egg_dock_box_set_orientation (EggDockItem    *item,
                                                GtkOrientation  orientation);

static gboolean egg_dock_box_child_placement (EggDockObject    *object,
                                                EggDockObject    *child,
                                                EggDockPlacement *placement);


/* ----- Class variables and definitions ----- */

#define SPLIT_RATIO  0.3

enum {
    PROP_0,
    PROP_POSITION
};


/* ----- Private functions ----- */

EGG_CLASS_BOILERPLATE (EggDockBox, egg_dock_box, EggDockItem, EGG_TYPE_DOCK_ITEM);

static void
egg_dock_box_class_init (EggDockBoxClass *klass)
{
    GObjectClass       *g_object_class;
    GtkObjectClass     *gtk_object_class;
    GtkWidgetClass     *widget_class;
    GtkContainerClass  *container_class;
    EggDockObjectClass *object_class;
    EggDockItemClass   *item_class;

    g_object_class = G_OBJECT_CLASS (klass);
    gtk_object_class = GTK_OBJECT_CLASS (klass);
    widget_class = GTK_WIDGET_CLASS (klass);
    container_class = GTK_CONTAINER_CLASS (klass);
    object_class = EGG_DOCK_OBJECT_CLASS (klass);
    item_class = EGG_DOCK_ITEM_CLASS (klass);

    g_object_class->constructor = egg_dock_box_constructor;
    
    gtk_object_class->destroy = egg_dock_box_destroy;

    container_class->add = egg_dock_box_add;
    container_class->forall = egg_dock_box_forall;
    container_class->child_type = egg_dock_box_child_type;
    
    object_class->is_compound = TRUE;
    
    object_class->dock_request = egg_dock_box_dock_request;
    object_class->dock = egg_dock_box_dock;
    object_class->child_placement = egg_dock_box_child_placement;
    
    item_class->has_grip = FALSE;
    item_class->set_orientation = egg_dock_box_set_orientation;    
}

static void
egg_dock_box_instance_init (EggDockBox *box)
{
    box->position_changed = TRUE;
}

static void 
egg_dock_box_create_child (EggDockBox   *box,
                             GtkOrientation  orientation) 
{
    EggDockItem *item;
    
    item = EGG_DOCK_ITEM (box);
    
    if (item->child)
        gtk_widget_unparent (GTK_WIDGET (item->child));
    
    /* create the container box */
    if (orientation == GTK_ORIENTATION_HORIZONTAL)
        item->child = gtk_hbox_new (FALSE, 0);
    else
        item->child = gtk_vbox_new (FALSE, 0);

    gtk_widget_set_parent (item->child, GTK_WIDGET (item));
    gtk_widget_show (item->child);
}

static GObject *
egg_dock_box_constructor (GType                  type,
                            guint                  n_construct_properties,
                            GObjectConstructParam *construct_param)
{
    GObject *g_object;
    
    g_object = EGG_CALL_PARENT_WITH_DEFAULT (G_OBJECT_CLASS, 
                                               constructor, 
                                               (type,
                                                n_construct_properties,
                                                construct_param),
                                               NULL);
    if (g_object) {
        EggDockItem *item = EGG_DOCK_ITEM (g_object);
        
        if (!item->child)
            egg_dock_box_create_child (EGG_DOCK_BOX (g_object),
                                         item->orientation);
        /* otherwise, the orientation was set as a construction
           parameter and the child is already created */
    }
    
    return g_object;
}

static void
egg_dock_box_destroy (GtkObject *object)
{
    EggDockItem *item = EGG_DOCK_ITEM (object);

    /* we need to call the virtual first, since in EggDockDestroy our
       children dock objects are detached */
    EGG_CALL_PARENT (GTK_OBJECT_CLASS, destroy, (object));

    /* after that we can remove the GtkNotebook */
    if (item->child) {
        gtk_widget_unparent (item->child);
        item->child = NULL;
    };
}

static void
egg_dock_box_add (GtkContainer *container,
                    GtkWidget    *widget)
{
    EggDockItem     *item;
    GtkBox        *box;
    EggDockPlacement pos = EGG_DOCK_NONE;
    
    g_return_if_fail (container != NULL && widget != NULL);
    g_return_if_fail (EGG_IS_DOCK_BOX (container));
    g_return_if_fail (EGG_IS_DOCK_ITEM (widget));

    item = EGG_DOCK_ITEM (container);
    g_return_if_fail (item->child != NULL);
    box = GTK_BOX (item->child);

    pos = item->orientation == GTK_ORIENTATION_HORIZONTAL ?
            EGG_DOCK_RIGHT : EGG_DOCK_BOTTOM;

    if (pos != EGG_DOCK_NONE)
        egg_dock_object_dock (EGG_DOCK_OBJECT (container),
                              EGG_DOCK_OBJECT (widget),
                              pos, NULL);
}

static void
egg_dock_box_forall (GtkContainer *container,
                       gboolean      include_internals,
                       GtkCallback   callback,
                       gpointer      callback_data)
{
    EggDockItem *item;

    g_return_if_fail (container != NULL);
    g_return_if_fail (EGG_IS_DOCK_BOX (container));
    g_return_if_fail (callback != NULL);

    if (include_internals) {
        /* use EggDockItem's forall */
        EGG_CALL_PARENT (GTK_CONTAINER_CLASS, forall, 
                           (container, include_internals, callback, callback_data));
    }
    else {
        item = EGG_DOCK_ITEM (container);
        if (item->child)
            gtk_container_foreach (GTK_CONTAINER (item->child), callback, callback_data);
    }
}

static GType
egg_dock_box_child_type (GtkContainer *container)
{
    EggDockItem *item = EGG_DOCK_ITEM (container);

    if (gtk_container_child_type (GTK_CONTAINER (item->child)) == G_TYPE_NONE)
        return G_TYPE_NONE;
    else
        return EGG_TYPE_DOCK_ITEM;
}

static void
egg_dock_box_request_foreach (EggDockObject *object,
                                gpointer       user_data)
{
    struct {
        gint            x, y;
        EggDockRequest *request;
        gboolean        may_dock;
    } *data = user_data;
    
    EggDockRequest my_request;
    gboolean       may_dock;
    
    my_request = *data->request;
    may_dock = egg_dock_object_dock_request (object, data->x, data->y, &my_request);
    if (may_dock) {
        data->may_dock = TRUE;
        *data->request = my_request;
    }
}

static gboolean
egg_dock_box_dock_request (EggDockObject  *object, 
                             gint            x,
                             gint            y, 
                             EggDockRequest *request)
{
    EggDockItem        *item;
    gint                bw;
    gint                rel_x, rel_y;
    GtkAllocation      *alloc;
    gboolean            may_dock = FALSE;
    EggDockRequest      my_request;

    g_return_val_if_fail (EGG_IS_DOCK_ITEM (object), FALSE);

    /* we get (x,y) in our allocation coordinates system */
    
    item = EGG_DOCK_ITEM (object);
    
    /* Get item's allocation. */
    alloc = &(GTK_WIDGET (object)->allocation);
    bw = GTK_CONTAINER (object)->border_width;

    /* Get coordinates relative to our window. */
    rel_x = x - alloc->x;
    rel_y = y - alloc->y;

    if (request)
        my_request = *request;
        
    /* Check if coordinates are inside the widget. */
    if (rel_x > 0 && rel_x < alloc->width &&
        rel_y > 0 && rel_y < alloc->height) {
        GtkRequisition my, other;
        gint divider = -1;
        
        egg_dock_item_preferred_size (EGG_DOCK_ITEM (my_request.applicant), &other);
        egg_dock_item_preferred_size (EGG_DOCK_ITEM (object), &my);

        /* It's inside our area. */
        may_dock = TRUE;

	/* Set docking indicator rectangle to the widget size. */
        my_request.rect.x = bw;
        my_request.rect.y = bw;
        my_request.rect.width = alloc->width - 2*bw;
        my_request.rect.height = alloc->height - 2*bw;

        my_request.target = object;

        /* See if it's in the border_width band. */
        if (rel_x < bw) {
            my_request.position = EGG_DOCK_LEFT;
            my_request.rect.width *= SPLIT_RATIO;
            divider = other.width;
        } else if (rel_x > alloc->width - bw) {
            my_request.position = EGG_DOCK_RIGHT;
            my_request.rect.x += my_request.rect.width * (1 - SPLIT_RATIO);
            my_request.rect.width *= SPLIT_RATIO;
            divider = MAX (0, my.width - other.width);
        } else if (rel_y < bw) {
            my_request.position = EGG_DOCK_TOP;
            my_request.rect.height *= SPLIT_RATIO;
            divider = other.height;
        } else if (rel_y > alloc->height - bw) {
            my_request.position = EGG_DOCK_BOTTOM;
            my_request.rect.y += my_request.rect.height * (1 - SPLIT_RATIO);
            my_request.rect.height *= SPLIT_RATIO;
            divider = MAX (0, my.height - other.height);
            
        } else { /* Otherwise try our children. */
            struct {
                gint            x, y;
                EggDockRequest *request;
                gboolean        may_dock;
            } data;

            /* give them coordinates in their allocation system... the
               GtkPaned has no window, so our children allocation
               coordinates are our window coordinates */
            data.x = rel_x;
            data.y = rel_y;
            data.request = &my_request;
            data.may_dock = FALSE;
            
            gtk_container_foreach (GTK_CONTAINER (object),
                                   (GtkCallback) egg_dock_box_request_foreach,
                                   &data);

            may_dock = data.may_dock;
            if (!may_dock) {
                /* the pointer is on the handle, so snap to top/bottom
                   or left/right */
                may_dock = TRUE;
                if (item->orientation == GTK_ORIENTATION_HORIZONTAL) {
                    if (rel_y < alloc->height / 2) {
                        my_request.position = EGG_DOCK_TOP;
                        my_request.rect.height *= SPLIT_RATIO;
                        divider = other.height;
                    } else {
                        my_request.position = EGG_DOCK_BOTTOM;
                        my_request.rect.y += my_request.rect.height * (1 - SPLIT_RATIO);
                        my_request.rect.height *= SPLIT_RATIO;
                        divider = MAX (0, my.height - other.height);
                    }
                } else {
                    if (rel_x < alloc->width / 2) {
                        my_request.position = EGG_DOCK_LEFT;
                        my_request.rect.width *= SPLIT_RATIO;
                        divider = other.width;
                    } else {
                        my_request.position = EGG_DOCK_RIGHT;
                        my_request.rect.x += my_request.rect.width * (1 - SPLIT_RATIO);
                        my_request.rect.width *= SPLIT_RATIO;
                        divider = MAX (0, my.width - other.width);
                    }
                }
            }
        }

        if (divider >= 0 && my_request.position != EGG_DOCK_CENTER) {
            if (G_IS_VALUE (&my_request.extra))
                g_value_unset (&my_request.extra);
            g_value_init (&my_request.extra, G_TYPE_UINT);
            g_value_set_uint (&my_request.extra, (guint) divider);
        }
        
        if (may_dock) {
            /* adjust returned coordinates so they are relative to
               our allocation */
            my_request.rect.x += alloc->x;
            my_request.rect.y += alloc->y;
        }
    }

    if (may_dock && request)
        *request = my_request;
    
    return may_dock;
}

static void
egg_dock_box_dock (EggDockObject    *object,
                     EggDockObject    *requestor,
                     EggDockPlacement  position,
                     GValue           *other_data)
{
    GtkBox *box;
    gboolean  done = FALSE;
    
    g_return_if_fail (EGG_IS_DOCK_BOX (object));
    g_return_if_fail (EGG_DOCK_ITEM (object)->child != NULL);

    box = GTK_BOX (EGG_DOCK_ITEM (object)->child);

    if (other_data) g_print("has somesort of value\n");
    if (G_VALUE_HOLDS(other_data, G_TYPE_FLAGS)) g_print("is a flag\n");

    /* see if we can dock the item in our box */
    switch (EGG_DOCK_ITEM (object)->orientation) {
        case GTK_ORIENTATION_HORIZONTAL:
	    if (position == EGG_DOCK_RIGHT)
		gtk_box_pack_start(box, GTK_WIDGET (requestor), TRUE, FALSE, 0);
	    else if (position == EGG_DOCK_LEFT)
		gtk_box_pack_end(box, GTK_WIDGET (requestor), TRUE, FALSE, 0);
            done = TRUE;
            break;
        case GTK_ORIENTATION_VERTICAL:
	    if (position == EGG_DOCK_BOTTOM)
	    {
		gtk_box_pack_start(box, GTK_WIDGET (requestor), TRUE, FALSE, 0);
	    }
	    else if (position == EGG_DOCK_TOP)
	    {
		gtk_box_pack_end(box, GTK_WIDGET (requestor), TRUE, FALSE, 0);
	    }
            done = TRUE;
            break;
        default:
            break;
    }

    if (!done) {
        /* this will create another paned and reparent us there */
        EGG_CALL_PARENT (EGG_DOCK_OBJECT_CLASS, dock, (object, requestor, position,
                                                         other_data));
    }
    else {
        egg_dock_item_show_grip (EGG_DOCK_ITEM (requestor));
        EGG_DOCK_OBJECT_SET_FLAGS (requestor, EGG_DOCK_ATTACHED);
    }
}

static void
egg_dock_box_set_orientation (EggDockItem    *item,
                                GtkOrientation  orientation)
{
    GtkBox    *old_box = NULL, *new_box;
    GList 	*i;
    
    g_return_if_fail (EGG_IS_DOCK_BOX (item));

    if (item->child) {
        old_box = GTK_BOX (item->child);
        g_object_ref (old_box);
        gtk_widget_unparent (GTK_WIDGET (old_box));
        item->child = NULL;
    }
    
    egg_dock_box_create_child (EGG_DOCK_BOX (item), orientation);
    
    if (old_box) {
        new_box = GTK_BOX (item->child);

	for (i = old_box->children; i ; i=i->next)
	{
	    GtkWidget *child = i->data;
            g_object_ref(child);
            gtk_container_remove(GTK_CONTAINER (old_box), child);
            gtk_box_pack_start (new_box, child, TRUE, FALSE, 0);
            g_object_unref (child);
        }
    }

    EGG_CALL_PARENT (EGG_DOCK_ITEM_CLASS, set_orientation, (item, orientation));
}

static gboolean 
egg_dock_box_child_placement (EggDockObject    *object,
                                EggDockObject    *child,
                                EggDockPlacement *placement)
{
    EggDockItem      *item = EGG_DOCK_ITEM (object);
    GtkBox         *box;
    EggDockPlacement  pos = EGG_DOCK_NONE;
    
    if (item->child) {
        box = GTK_BOX (item->child);
        if (GTK_BOX(gtk_widget_get_parent(GTK_WIDGET(child))) == box)
            pos = item->orientation == GTK_ORIENTATION_HORIZONTAL ?
                EGG_DOCK_RIGHT : EGG_DOCK_BOTTOM;
    }

    if (pos != EGG_DOCK_NONE) {
        if (placement)
            *placement = pos;
        return TRUE;
    }
    else
        return FALSE;
}


/* ----- Public interface ----- */

GtkWidget *
egg_dock_box_new (GtkOrientation orientation)
{
    EggDockBox *box;

    box = EGG_DOCK_BOX (g_object_new (EGG_TYPE_DOCK_BOX,
                                          "orientation", orientation, NULL));
    EGG_DOCK_OBJECT_UNSET_FLAGS (box, EGG_DOCK_AUTOMATIC);
    
    return GTK_WIDGET (box);
}
