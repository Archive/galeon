/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __bookmarks_iterator_h
#define __bookmarks_iterator_h

#include <glib.h>
#include <glib-object.h>
#include "bookmarks.h"


#ifdef __cplusplus
extern "C" {
#endif


/* object forward declarations */

typedef struct _GbIterator GbIterator;
typedef struct _GbIteratorClass GbIteratorClass;
typedef struct _GbIteratorPrivate GbIteratorPrivate;

/**
 * Iterator object
 */

#define GB_TYPE_ITERATOR			(gb_iterator_get_type())
#define GB_ITERATOR(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), GB_TYPE_ITERATOR,\
					 GbIterator))
#define GB_ITERATOR_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), GB_TYPE_ITERATOR,\
					 GbIteratorClass))
#define GB_IS_ITERATOR(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), GB_TYPE_ITERATOR))
#define GB_IS_ITERATOR_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), GB_TYPE_ITERATOR))
#define GB_ITERATOR_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), GB_TYPE_ITERATOR,\
					 GbIteratorClass))

struct _GbIteratorClass 
{
	GObjectClass parent_class;
};

struct _GbIterator
{
	GObject parent_object;
	GbIteratorPrivate *priv;
};

GType		gb_iterator_get_type		(void);
GbIterator *	gb_iterator_new			(GbBookmarkSet *set);
GbIterator *	gb_iterator_new_folder		(GbFolder *f, gboolean recursive);
GbBookmark *	gb_iterator_current		(GbIterator *i);
GbBookmark *	gb_iterator_next		(GbIterator *i);
gboolean	gb_iterator_has_next		(GbIterator *i);



#ifdef __cplusplus
}
#endif

#endif
