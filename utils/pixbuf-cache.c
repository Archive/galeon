/*
 *  Copyright (C) 2001-2002 Jorn Baayen 
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "gul-general.h"
#include "gul-gui.h"
#include "pixbuf-cache.h"
#include "eel-gconf-extensions.h"

#include <stdlib.h>
#include <string.h>
#include <gdk/gdkpixbuf.h>
#include <gtk/gtkstock.h>
#include <gtk/gtkiconfactory.h>

#include <libgnome/gnome-util.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

/* cache of theme pixmaps */
static GHashTable *gul_pixbufs_cache = NULL;

/* Local prototypes */
static gboolean gul_pixbuf_free_cb (gpointer key, GdkPixbuf *value,
			        gpointer user_data);

/**
 * pixbuf_free_cb: free one pixbuf
 */
static gboolean
gul_pixbuf_free_cb (gpointer key, GdkPixbuf *value, gpointer user_data)
{
	g_free (key);
	g_object_unref (value);
	return TRUE;
}

/**
 * pixbuf_cache_reset: reset the pixbuf cache
 */
void
gul_pixbuf_cache_reset (void)
{
	if (gul_pixbufs_cache != NULL)
	{
		g_hash_table_foreach_remove (gul_pixbufs_cache,
					     (GHRFunc) gul_pixbuf_free_cb, NULL);
	}
}

/**
 * pixbuf_cache_get: get 'n cache a pixbuf
 */
GdkPixbuf *
gul_pixbuf_cache_get (const gchar *filename)
{
	GdkPixbuf *pixbuf = NULL;
	gchar *tmp;

	/* lookup in cache */
	if (gul_pixbufs_cache != NULL)
		pixbuf = g_hash_table_lookup (gul_pixbufs_cache, filename);

	if (pixbuf != NULL)
	{
		g_object_ref (G_OBJECT (pixbuf));
		return pixbuf;
	}

	if (g_path_is_absolute (filename))
	{
		tmp = g_strdup (filename);
	}
	else
	{
		tmp = gul_general_user_file (filename, FALSE);
	}

	if (tmp && g_file_test (tmp, G_FILE_TEST_EXISTS))
	{
		pixbuf = gdk_pixbuf_new_from_file (tmp, NULL);
	}

	g_free (tmp);

	if (pixbuf == NULL)
		return NULL;

	/* cache the created GdkPixbuf */
	if (gul_pixbufs_cache == NULL)
	{
		gul_pixbufs_cache = g_hash_table_new (g_str_hash,
						      g_str_equal);
	}

	g_hash_table_insert (gul_pixbufs_cache, g_strdup (filename),
			     pixbuf);
	
	g_object_ref (G_OBJECT (pixbuf));
	return pixbuf;
}

/**
 * pixbuf_cache_register_stocks: register galeon icons with the gnome stock
 * system so people can theme our icons
 */
void
gul_pixbuf_cache_register_stocks (void)
{
	static gboolean done = FALSE;
	GtkIconFactory *factory;
	GtkIconSet *icon_set;
	GtkIconSource *icon_source;
	gint i;

	GtkStockItem items[] = {
		{ GALEON_STOCK_SEPARATOR     ,"_Separator",  0, 0, NULL },
		{ GALEON_STOCK_FOLDER        ,"_Folder",     0, 0, NULL },
		{ GALEON_STOCK_DEFAULT       ,"_Default",    0, 0, NULL },
		{ GALEON_STOCK_HISTORY       ,"_History",    0, 0, NULL },
		{ GALEON_STOCK_FILTER        ,"_Filter",     0, 0, NULL },
		{ GALEON_STOCK_POPUP_BLOCKED ,"_Popup blocked", 0, 0, NULL },
		{ GALEON_STOCK_SECURE        ,"_Secure",      0, 0, NULL },
		{ GALEON_STOCK_INSECURE      ,"_Insecure",    0, 0, NULL },
		{ GALEON_STOCK_DOWNLOAD      ,"_Download",    0, 0, NULL },
		{ GALEON_STOCK_ENTRY         ,"_Entry",       0, 0, NULL }
	};

	const char * icon_theme_items[] = {
		STOCK_ZOOM,
		STOCK_NEW_TAB,
		STOCK_FULLSCREEN,
		STOCK_VIEW_SOURCE, 
		STOCK_SEND_MAIL,
		STOCK_ADD_BOOKMARK,
		STOCK_SPINNER_REST,
		STOCK_SELECT_ALL,
		STOCK_EDIT_BOOKMARK,
		STOCK_CONNECT,
		STOCK_DISCONNECT,
		STOCK_LOCK_BROKEN
	};


	if (done) 
	{
		return;
	}
	done = TRUE;

	gtk_stock_add (items, G_N_ELEMENTS (items));

	factory = gtk_icon_factory_new ();
	gtk_icon_factory_add_default (factory);

	for (i = 0; i < (gint) G_N_ELEMENTS (items); i++)
	{
		GdkPixbuf *pixbuf;
		gchar *fn;

		fn = g_strconcat (items[i].stock_id, ".png", NULL);
		pixbuf = gul_pixbuf_cache_get (fn);
		g_free (fn);

		icon_set = gtk_icon_set_new_from_pixbuf (pixbuf);
		gtk_icon_factory_add (factory, items[i].stock_id, icon_set);
		gtk_icon_set_unref (icon_set);
		
		g_object_unref (G_OBJECT (pixbuf));
	}

	for (i = 0; i < (int) G_N_ELEMENTS (icon_theme_items); i++)
	{
		icon_set = gtk_icon_set_new ();
		icon_source = gtk_icon_source_new ();
		gtk_icon_source_set_icon_name (icon_source, icon_theme_items[i]);
		gtk_icon_set_add_source (icon_set, icon_source);
		gtk_icon_factory_add (factory, icon_theme_items[i], icon_set);
		gtk_icon_set_unref (icon_set);
		gtk_icon_source_free (icon_source);
	}

	g_object_unref (G_OBJECT (factory));
}
