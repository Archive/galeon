/*
 * GtkNSSDialogs.h
 *
 * Copyright (C) 2003 Crispin Flowerday <gnome@flowerday.cx>
 * Available under the terms of the GNU General Public License version 2.
 */ 

#ifndef GTKNSSDIALOGS_H
#define GTKNSSDIALOGS_H 1

#include <nsError.h>
#ifdef HAVE_NSIBADCERTLISTENER_H
#include "nsIBadCertListener.h"
#endif
#include "nsICertificateDialogs.h"

// 7a50a10d-9425-4e12-84b1-5822edacd8ce
#define GTK_NSSDIALOGS_CID	\
 {0x7a50a10d, 0x9425, 0x4e12, {0x84, 0xb1, 0x58, 0x22, 0xed, 0xac, 0xd8, 0xce}}

#define GTK_NSSDIALOGS_CLASSNAME  "Gtk NSS Dialogs"

class GtkNSSDialogs :
#ifdef HAVE_NSIBADCERTLISTENER_H
  public nsIBadCertListener,
#endif
  public nsICertificateDialogs
{
public:
  NS_DECL_ISUPPORTS
#ifdef HAVE_NSIBADCERTLISTENER_H
  NS_DECL_NSIBADCERTLISTENER
#endif
  NS_DECL_NSICERTIFICATEDIALOGS

  GtkNSSDialogs();
  virtual ~GtkNSSDialogs();
};


#endif /* GTKNSSDIALOGS_H */
