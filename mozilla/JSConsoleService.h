/*
 *  Copyright (C) 2000 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GJSConsoleService_h
#define __GJSConsoleService_h

#define G_JS_CONSOLESERVICE_CLASSNAME \
 "Galeon's JavaScript Console Service"
// {35807600-35bd-11d5-bb6f-b9f2e9fee03c}
#define G_JSCONSOLESERVICE_CID \
 { 0x35807600, 0x35bd, 0x11d5, {0xbb, 0x6f, 0xb9, 0xf2, 0xe9, 0xfe, 0xe0, 0x3c}}
#define NS_JSCONSOLESERVICE_CONTRACTID \
 "@mozilla.org/embedcomp/jsconsole-service;1"

#include <nsIJSConsoleService.h>

class JSConsoleService: public nsIJSConsoleService
{
public:

  JSConsoleService();
  virtual ~JSConsoleService();

  NS_DECL_NSIJSCONSOLESERVICE
  NS_DECL_ISUPPORTS

};

#endif

