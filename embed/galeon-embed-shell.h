/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GALEON_EMBED_SHELL_H
#define GALEON_EMBED_SHELL_H

#include "global-history.h"
#include "downloader-view.h"
#include "galeon-embed.h"
#include "js-console.h"
#include "cookie-info.h"
#include "galeon-encodings.h"

#include <glib-object.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

typedef struct GaleonEmbedShellClass GaleonEmbedShellClass;

#define GALEON_TYPE_EMBED_SHELL             (galeon_embed_shell_get_type ())
#define GALEON_EMBED_SHELL(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GALEON_TYPE_EMBED_SHELL, GaleonEmbedShell))
#define GALEON_EMBED_SHELL_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GALEON_TYPE_EMBED_SHELL, GaleonEmbedShellClass))
#define GALEON_IS_EMBED_SHELL(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GALEON_TYPE_EMBED_SHELL))
#define GALEON_IS_EMBED_SHELL_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GALEON_EMBED_SHELL))
#define GALEON_EMBED_SHELL_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GALEON_TYPE_EMBED_SHELL, GaleonEmbedShellClass))

typedef struct GaleonEmbedShell GaleonEmbedShell;
typedef struct GaleonEmbedShellPrivate GaleonEmbedShellPrivate;

extern GaleonEmbedShell *embed_shell;

/**
 * BlockedHost: a blocked host
 */
typedef struct
{
        char *domain;
	gboolean status;
} PermissionInfo;

/**
 * Password: a password manager entry
 */
typedef struct
{
        gchar *host;
        gchar *username;

	gchar *password;
	gchar *httpRealm;
	gchar *formSubmitURL;
	gchar *usernameField;
	gchar *passwordField;
} PasswordInfo;

typedef struct
{
	const char *name;
	const char *title;
} CharsetInfo;

typedef struct
{
	const char *title;
	const char *key;
} FontLangInfo;

/**
 * PasswordType: To distinguish actual passwords from blocked password sites
 */
typedef enum
{
        PASSWORD_PASSWORD,
        PASSWORD_REJECT
} PasswordType;

typedef enum
{
	COOKIES_PERMISSION = 0U,
	IMAGES_PERMISSION = 1U,
	POPUPS_PERMISSION = 2U
} PermissionType;

typedef enum
{
	UNKNOWN_ACTION = 0U,
	ALLOW_ACTION = 1U,
	DENY_ACTION = 2U
} PermissionActionType;

typedef enum
{
	DISK_CACHE = 2,
	MEMORY_CACHE = 1
} CacheType;

struct GaleonEmbedShell 
{
	GObject parent;
        GaleonEmbedShellPrivate *priv;
};

struct GaleonEmbedShellClass
{
        GObjectClass parent_class;

  	GaleonEmbed*    (* new_window)         (GaleonEmbed *embed, 
						EmbedChromeMask chromemask);

	void 		(* permission_changed)  (GaleonEmbedShell *shell);

	void            (* add_sidebar)         (const char * title, const char * url);

	/* Methods */
	gboolean        (* initialize)          (GaleonEmbedShell *shell);

	GlobalHistory * (* get_global_history)  (GaleonEmbedShell *shell);
	DownloaderView* (* get_downloader_view) (GaleonEmbedShell *shell);
	GaleonJSConsole* (* get_js_console)     (GaleonEmbedShell *shell);

	void            (* clear_cache)         (GaleonEmbedShell *shell,
						 CacheType type);
	void            (* set_offline_mode)    (GaleonEmbedShell *shell,
						 gboolean offline);
	void            (* show_java_console)   (GaleonEmbedShell *shell);
	GaleonEncodings*(* get_encodings)       (GaleonEmbedShell *shell);

	GList *		(* get_font_langs)      (GaleonEmbedShell *shell);

	GList *		(* get_font_list)	(GaleonEmbedShell *shell,
						 const char *langGroup,
						 const char *fontType);
	void            (* set_permission)      (GaleonEmbedShell *shell,
						 const char *url, 
					         PermissionType type,
				 	         gboolean allow);
	GList *         (* list_permissions)    (GaleonEmbedShell *shell,
					    	 PermissionType type);
	void            (* remove_permissions)  (GaleonEmbedShell *shell,
						 PermissionType type,
						 GList *permissions);
	PermissionActionType (* test_permission)(GaleonEmbedShell *shell,
						 const char *url,
						 PermissionType type);

	GList *         (* list_cookies)        (GaleonEmbedShell *shell);
	void            (* remove_cookies)      (GaleonEmbedShell *shell,
						 GList *cookies,
						 gboolean block);
	GList *         (* list_passwords)      (GaleonEmbedShell *shell,
						 PasswordType type);
	void            (* remove_passwords)    (GaleonEmbedShell *shell,
						 GList *passwords, 
						 PasswordType type);
	GtkPageSetup *  (* get_page_setup)	(GaleonEmbedShell *shell);
};

GType             galeon_embed_shell_get_type            (void);

GaleonEmbedShell *galeon_embed_shell_new                 (void);

gboolean          galeon_embed_shell_initialize          (GaleonEmbedShell *ges);

/* Services provided by galeon. They can be overriden. */
GlobalHistory    *galeon_embed_shell_get_global_history  (GaleonEmbedShell *shell);

DownloaderView   *galeon_embed_shell_get_downloader_view (GaleonEmbedShell *shell);

GaleonJSConsole  *galeon_embed_shell_get_js_console      (GaleonEmbedShell *shell);
 
/* Services provided by the renderer */
void              galeon_embed_shell_clear_cache         (GaleonEmbedShell *shell,
							  CacheType type);

void              galeon_embed_shell_set_offline_mode    (GaleonEmbedShell *shell,
							  gboolean offline);

void              galeon_embed_shell_show_java_console   (GaleonEmbedShell *shell);

/* Charsets */
GaleonEncodings *  galeon_embed_shell_get_encodings       (GaleonEmbedShell *shell);


GList *           galeon_embed_shell_get_font_list	 (GaleonEmbedShell *shell,
							  const char *langGroup,
							  const char *fontType);

GList *           galeon_embed_shell_get_font_langs      (GaleonEmbedShell *shell);

/* Permissions */
void              galeon_embed_shell_set_permission      (GaleonEmbedShell *shell,
							  const char *url, 
					                  PermissionType type,
				 	                  gboolean allow);

GList *           galeon_embed_shell_list_permissions    (GaleonEmbedShell *shell,
							  PermissionType type);

void		  galeon_embed_shell_free_permissions    (GaleonEmbedShell *shell,
							  GList *permissions);

void              galeon_embed_shell_remove_permissions  (GaleonEmbedShell *shell,
							  PermissionType type,
							  GList *permissions);

PermissionActionType  galeon_embed_shell_test_permission  (GaleonEmbedShell *shell,
							  const char *url,
							   PermissionType type);

/* Cookies */
GList *           galeon_embed_shell_list_cookies        (GaleonEmbedShell *shell);

void              galeon_embed_shell_remove_cookies      (GaleonEmbedShell *shell,
							  GList *cookies,
							  gboolean block);

void		  galeon_embed_shell_free_cookies        (GaleonEmbedShell *shell,
							  GList *cookies);

/* Passwords */
GList*            galeon_embed_shell_list_passwords      (GaleonEmbedShell *shell,
							  PasswordType type);

void		  galeon_embed_shell_free_passwords      (GaleonEmbedShell *shell,
							  GList *passwords);

void              galeon_embed_shell_remove_passwords    (GaleonEmbedShell *shell,
							  GList *passwords, 
							  PasswordType type);

/* Print */
GtkPageSetup*     galeon_embed_shell_get_page_setup      (GaleonEmbedShell *shell);
void              galeon_embed_shell_set_page_setup      (GaleonEmbedShell *shell,
							  GtkPageSetup     *page_setup);
GtkPrintSettings* galeon_embed_shell_get_print_settings  (GaleonEmbedShell *shell);
void              galeon_embed_shell_set_print_settings  (GaleonEmbedShell *shell,
                                                          GtkPrintSettings *settings);

/* accumulator for the new_window signals */
gboolean _galeon_embed_new_window_signal_accumulator (GSignalInvocationHint *ihint,
						      GValue                *return_accu,
						      const GValue          *handler_return,
						      gpointer               dummy);

G_END_DECLS

#endif
