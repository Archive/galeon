/*
 *  Copyright © 2004 Jean-François rameau
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#ifndef GALEON_DBUS_H
#define GALEON_DBUS_H

#include <glib-object.h>

/* Yes, we know that DBUS API isn't stable yet */
#define DBUS_API_SUBJECT_TO_CHANGE
#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-lowlevel.h>

G_BEGIN_DECLS

#define GALEON_TYPE_DBUS	 (galeon_dbus_get_type ())
#define GALEON_DBUS(o)		 (G_TYPE_CHECK_INSTANCE_CAST ((o), GALEON_TYPE_DBUS, GaleonDbus))
#define GALEON_DBUS_CLASS(k)	 (G_TYPE_CHECK_CLASS_CAST((k), GALEON_TYPE_DBUS, GaleonDbusClass))
#define GALEON_IS_DBUS(o)	 (G_TYPE_CHECK_INSTANCE_TYPE ((o), GALEON_TYPE_DBUS))
#define GALEON_IS_DBUS_CLASS(k)	 (G_TYPE_CHECK_CLASS_TYPE ((k), GALEON_TYPE_DBUS))
#define GALEON_DBUS_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), GALEON_TYPE_DBUS, GaleonDbusClass))

extern GQuark galeon_dbus_error_quark;
#define GALEON_DBUS_ERROR_QUARK	(galeon_dbus_error_quark)

typedef struct _GaleonDbus	GaleonDbus;
typedef struct _GaleonDbusPrivate	GaleonDbusPrivate;
typedef struct _GaleonDbusClass	GaleonDbusClass;

typedef enum
{
	GALEON_DBUS_SESSION,
	GALEON_DBUS_SYSTEM
} GaleonDbusBus;

struct _GaleonDbus
{
	GObject parent;

	/*< private >*/
	GaleonDbusPrivate *priv;
};

struct _GaleonDbusClass
{
	GObjectClass parent_class;

	/* Signals */
	void (* connected)	(GaleonDbus *dbus,
				 GaleonDbusBus kind);
	void (* disconnected)	(GaleonDbus *dbus,
				 GaleonDbusBus kind);
};

GType		 galeon_dbus_get_type	 (void);

GaleonDbus      *galeon_dbus_get_default (void);

DBusGConnection *galeon_dbus_get_bus	 (GaleonDbus *dbus,
					  GaleonDbusBus kind);

DBusGProxy	*galeon_dbus_get_proxy	 (GaleonDbus *dbus,
					  GaleonDbusBus kind);

/* private */
gboolean         _galeon_dbus_startup    (gboolean claim_name,
					  GError **error);

void	         _galeon_dbus_release	 (void);

gboolean         _galeon_dbus_is_name_owner	(void);

G_END_DECLS

#endif /* !GALEON_DBUS_H */
