/* 
 *  Copyright (C) 2002  Ricardo Fernándezs Pascual <ric@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GALEON_AUTOCOMPLETION_SOUCE_H__
#define __GALEON_AUTOCOMPLETION_SOUCE_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define GALEON_TYPE_AUTOCOMPLETION_SOURCE	(galeon_autocompletion_source_get_type ())
#define GALEON_AUTOCOMPLETION_SOURCE(obj)	(G_TYPE_CHECK_INSTANCE_CAST ((obj), \
						 GALEON_TYPE_AUTOCOMPLETION_SOURCE, \
						 GaleonAutocompletionSource))
#define GALEON_IS_AUTOCOMPLETION_SOURCE(obj)	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
						 GALEON_TYPE_AUTOCOMPLETION_SOURCE))
#define GALEON_AUTOCOMPLETION_SOURCE_GET_IFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE ((obj), \
						     GALEON_TYPE_AUTOCOMPLETION_SOURCE, \
						     GaleonAutocompletionSourceIface))


typedef struct _GaleonAutocompletionSource	GaleonAutocompletionSource;
typedef struct _GaleonAutocompletionSourceIface	GaleonAutocompletionSourceIface;
typedef void (* GaleonAutocompletionSourceForeachFunc) (GaleonAutocompletionSource *source, 
							gchar *item, 
							gchar *title,
							guint32 score,
							gpointer data);

struct _GaleonAutocompletionSourceIface
{
	GTypeInterface g_iface;
	
	/* Signals */

	/**
	 * Sources MUST emit this signal when theirs data changes, expecially if the 
	 * strings are freed / modified. Otherwise, things will crash.
	 */
	void		(* data_changed)	(GaleonAutocompletionSource *source);
	
	/* Virtual Table */
	void		(* foreach)		(GaleonAutocompletionSource *source, 
						 const gchar *basic_key,
						 GaleonAutocompletionSourceForeachFunc func,
						 gpointer data);
	void		(* set_basic_key)	(GaleonAutocompletionSource *source,
						 const gchar *basic_key);
};

GType	galeon_autocompletion_source_get_type		(void);
void	galeon_autocompletion_source_foreach		(GaleonAutocompletionSource *source,
							 const gchar *basic_key,
							 GaleonAutocompletionSourceForeachFunc func,
							 gpointer data);
void	galeon_autocompletion_source_set_basic_key	(GaleonAutocompletionSource *source,
							 const gchar *basic_key);
							 
G_END_DECLS

#endif

