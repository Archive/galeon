/*
 *  Copyright (C) 2003 Crispin Flowerday
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "bookmarks.h"
#include "bookmarks-dnd.h"
#include "galeon-dnd.h"

static void   gb_bookmark_drag_data_get_cb (GtkWidget *widget,
                                            GdkDragContext *context,
                                            GtkSelectionData *selection_data,
                                            guint info,
                                            guint32 time,
                                            GbBookmark *bm);

static void   gb_bookmark_drag_data_received_cb (GtkWidget *widget,
						 GdkDragContext *dc,
						 gint x, gint y,
						 GtkSelectionData *selection_data,
						 guint info,
						 guint t,
						 GbBookmark *bm);

static void
each_url_get_data_binder (GaleonDragEachSelectedItemDataGet iteratee, 
        		  gpointer iterator_context, gpointer data)
{
	GbBookmark *bm = GB_BOOKMARK(iterator_context);
	const gchar *url = GB_IS_SITE (bm) ? GB_SITE (bm)->url : NULL;
	const gchar *name = bm->name;

	if (url) 
	{
		iteratee (url, name, -1, -1, -1, -1, data);
	}
}


static void
gb_bookmark_drag_data_get_cb (GtkWidget *widget,
			      GdkDragContext *context,
			      GtkSelectionData *selection_data,
			      guint info,
			      guint32 time,
			      GbBookmark *bm)
{
        g_assert (widget != NULL);
        g_return_if_fail (context != NULL);

        galeon_dnd_drag_data_get (widget, context, selection_data,
                info, time, bm, each_url_get_data_binder);
}


static void
each_url_receive_data_binder (const char * url, const char * title, gpointer context)
{
	GbBookmark *bm = GB_BOOKMARK(context);
	GbSite *niu;

	g_return_if_fail (GB_IS_FOLDER (bm));

	if (!title) title = url;

	niu = gb_site_new (bm->set, title, url);
	gb_bookmark_set_time_added_now (GB_BOOKMARK (niu));

	gb_folder_add_child (GB_FOLDER (bm), GB_BOOKMARK (niu), -1);
	g_object_unref (niu);
}



static void
gb_bookmark_drag_data_received_cb (GtkWidget *widget,
				   GdkDragContext *dc,
				   gint x, gint y,
				   GtkSelectionData *selection_data,
				   guint info,
				   guint t,
				   GbBookmark *bm)
{
	galeon_dnd_drag_data_receive ( widget, dc, x, y, selection_data,
				info, t, bm, each_url_receive_data_binder);
}



void
gb_bookmark_dnd_drag_source_set (GtkWidget *widget, GbBookmark *bm)
{
	if (GB_IS_SITE(bm))
	{
		galeon_dnd_url_drag_source_set (widget);

		g_signal_connect (G_OBJECT (widget),
				  "drag_data_get",
				  G_CALLBACK (gb_bookmark_drag_data_get_cb),
				  bm);
	}
}



void
gb_bookmark_dnd_drag_dest_set (GtkWidget *widget, GbBookmark *bm)
{
	if (GB_IS_FOLDER(bm) && !gb_folder_is_autogenerated (GB_FOLDER (bm)))
	{
		galeon_dnd_url_drag_dest_set (widget);

		g_signal_connect (G_OBJECT (widget),
				  "drag_data_received",
				  G_CALLBACK (gb_bookmark_drag_data_received_cb),
				  bm);
	}
}
