/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GALEON_WINDOW_H
#define GALEON_WINDOW_H

#include "galeon-embed.h"
#include "galeon-embed-persist.h"
#include "galeon-sidebar.h"
#include "galeon-dialog.h"
#include "gul-notebook.h"
#include "page-info-dialog.h"
#include <glib-object.h>
#include <glib.h>
#include <gtk/gtkwindow.h>
#include "bookmarks-location-source.h"
#include "bookmarks-util.h"
#include "egg-editable-toolbar.h"

G_BEGIN_DECLS

typedef struct GaleonWindowClass GaleonWindowClass;
typedef struct GaleonWindow GaleonWindow;
typedef struct GaleonWindowPrivate GaleonWindowPrivate;

#include "galeon-popup.h"

#define GALEON_TYPE_WINDOW             (galeon_window_get_type ())
#define GALEON_WINDOW(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GALEON_TYPE_WINDOW, GaleonWindow))
#define GALEON_WINDOW_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GALEON_WINDOW, GaleonWindowClass))
#define GALEON_IS_WINDOW(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GALEON_TYPE_WINDOW))
#define GALEON_IS_WINDOW_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GALEON_WINDOW))

struct GaleonWindow 
{
	GtkWindow parent;
        GaleonWindowPrivate *priv;

	/* Public for ppv toolbar */
	GObject *egg_dock;

	/* Public for popup menus, ppv toolbar and maybe status bar */
	GtkUIManager *merge;
};

struct GaleonWindowClass
{
	GtkWindowClass parent_class;

	/* signals */
	void (*active_embed_changed)	(GaleonWindow *window, GaleonEmbed *old_embed, GaleonEmbed *new_embed);
};

enum
{
        GALEON_WINDOW_ADD_TAB_LAST = GUL_NOTEBOOK_INSERT_LAST,
        GALEON_WINDOW_ADD_TAB_GROUPED = GUL_NOTEBOOK_INSERT_GROUPED
};

/* Include the header down here to resolve circular dependency */
#include "galeon-tab.h"

GType         	 galeon_window_get_type     	  (void);

GaleonWindow 	*galeon_window_new          	  (void);

void          	 galeon_window_set_chrome  	  (GaleonWindow *window,
					     	   EmbedChromeMask chrome_flags);

EmbedChromeMask  galeon_window_get_chrome	  (GaleonWindow *window);

void		 galeon_window_toggle_fullscreen  (GaleonWindow *window);

void		galeon_window_bookmark_activate	  (GaleonWindow *w, 
						   GbBookmarkEventActivated *ev);

gboolean	 galeon_window_can_close	  (GaleonWindow *w);

void             galeon_window_update_edit_actions_sensitivity (GaleonWindow *w, gboolean hide);

void             galeon_window_enable_edit_actions_sensitivity (GaleonWindow *w);

/* Tabs */

void          	 galeon_window_add_tab      	  (GaleonWindow *window, 
					     	   GaleonTab *tab,
						   gint position,
						   gboolean jump_to);

gboolean	 galeon_window_remove_tab	  (GaleonWindow *window, 
					     	   GaleonTab *tab);

void		 galeon_window_next_tab		  (GaleonWindow *window);

void		 galeon_window_prev_tab		  (GaleonWindow *window);

void		 galeon_window_jump_to_tab	  (GaleonWindow *window, 
					     	   GaleonTab *tab);

void		 galeon_window_move_tab	  	  (GaleonWindow *window, 
					     	   GaleonTab *tab,
						   GaleonTab *new_sibling);

void		 galeon_window_reparent_tab	  (GaleonWindow *window,
		                                   GaleonWindow *source,
						   GaleonTab *tab);

void	      	 galeon_window_load_url      	  (GaleonWindow *window,
					     	   const char *url);

void		 galeon_window_set_zoom		  (GaleonWindow *window, 
						   gint zoom);

void             galeon_window_edit_location	  (GaleonWindow *window);

gchar * 	 galeon_window_get_location_entry_location (GaleonWindow *window);

void	 	 galeon_window_set_location_entry_location (GaleonWindow *window, 
							    const gchar *location);

GtkWidget        *galeon_window_get_find_toolbar  (GaleonWindow *window);

GtkWidget	 *galeon_window_get_statusbar	  (GaleonWindow *window);

GaleonSidebar	 *galeon_window_get_sidebar	  (GaleonWindow *window);

GaleonTab    	 *galeon_window_get_active_tab 	  (GaleonWindow *window);

GaleonEmbed  	 *galeon_window_get_active_embed  (GaleonWindow *window);

GaleonPopup	 *galeon_window_get_popup_factory (GaleonWindow *window);

GulNotebook	 *galeon_window_get_notebook	  (GaleonWindow *window);

GList 	 	 *galeon_window_get_tabs	  (GaleonWindow *window);

EggEditableToolbar *galeon_window_get_toolbar	  (GaleonWindow *window);

void              galeon_window_clear_history     (GaleonWindow *window);

/* Dialogs */
void		  galeon_window_show_page_info	(GaleonWindow *window, PageInfoDialogPage page);

void		  galeon_window_show_history	  (GaleonWindow *window);

void              galeon_window_show_js_console   (GaleonWindow *window);

G_END_DECLS

#endif
