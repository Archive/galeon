<!-- <!DOCTYPE Chapter PUBLIC "-//GNOME//DTD DocBook PNG Variant V1.1//EN"> -->
<chapter id="bookmarks">

      <title>Using Galeon Bookmarks</title>
      <para>
	Bookmarks serve several important purposes in web browsing.  Above
	all else, they allow you to quickly and easily jump to your
	favorite pages with only a click or two of the mouse.  They also
	enable you to organize information that interests you, and make it
	simple to keep track of long, complicated URLs.  In
	<application>Galeon</application>, additions have been made to the
	typical features of bookmarks to make them even more customizable
	and powerful.  This chapter discusses those features and how to use
	them.
      </para>

       <sect1 id="bookmarks-editor">
         <title>Bookmarks Editor</title>
         <para>
	   The <emphasis>Bookmarks Editor</emphasis>, unsurprisingly,
	   allows you to edit your bookmarks.  Instructions on how to load
	   it are given in <xref linkend="menubar-bookmarks"/>. You can
	   have several bookmarks editors opened at the same time, and you
	   can dock a bookmark editor in a
	   <application>Galeon</application> window.
         </para>
         <figure id="bookmarks-editor.png">
           <title>Galeon Bookmarks Editor</title>
           <screenshot>
             <graphic fileref="figures/bookmarks-editor.png" format="PNG"></graphic>
           </screenshot>
         </figure>
         <para>
	   In the center of the <emphasis>Bookmarks Editor</emphasis>
	   window, there is a tree-based listing of all of your folders,
	   bookmarks, and separators.  For easy viewing, all of the
	   branches of the tree can be quickly expanded or collapsed by
	   clicking the <guimenuitem>Expand all categories</guimenuitem> or
	   <guimenuitem>Collapse all categories</guimenuitem> items in the
	   editor's <guimenu>View</guimenu> menu.  Objects can be
	   repositioned by dragging them to new locations in the tree, and
	   you can also move them up and down in the listing by selecting
	   one and clicking the <guibutton>Up</guibutton> and
	   <guibutton>Down</guibutton> buttons in the toolbar.  New
	   bookmarks, separators, categories (folders), and aliases (see
	   <xref linkend="bookmarks-aliases"/> can be created by clicking
	   the appropriate buttons in the toolbar or in the editor's
	   <guimenu>File</guimenu> menu.  If the highlighted object is a
	   category, the new object will be created inside of it; otherwise,
	   the new object will be appended after the highlighted object.
	 </para>
	 <para>
	   After you select an object by clicking on it, its attributes will
	   appear in the area in the bottom of the window.  The available
	   options will vary depending on whether a bookmark or category is
	   selected.
	 </para>
	 <para>
	   When an bookmark is selected, you are able to edit its name,
	   give it a nickname (described in <xref
	   linkend="bookmarks-nicks"/>), edit the URL, add notes that are
	   only visible in the <emphasis>Bookmarks Editor</emphasis>
	   window, and select an image to replace the bookmark's name in
	   the toolbar and menus (described in <xref
	   linkend="bookmarks-with-images"/>).  By modifying the URL field
	   as described in <xref linkend="bookmarks-smart-bookmarks"/>, it
	   is possible to create a bookmark that will use user-supplied
	   arguments to form a URL, greatly simplifying the use of search
	   engines.
	 </para>
	 <para>
	   When a category is selected, you are able to edit its name, add
	   notes, and associate an image with it.  In addition to this, the
	   <guibutton>Show as toolbar</guibutton> checkbox allows you to
	   make the contents of the category appear within a toolbar, as
	   described in <xref linkend="bookmarks-toolbars"/>.
	 </para>
	 <para>
	   In both cases, the <guibutton>Add to context menu</guibutton>
	   checkbox can be used to add the bookmark or category to the menu
	   that is displayed when the right mouse button is clicked on the
	   browser window.  This feature is described in more detail in
	   <xref linkend="bookmarks-context-menus"/>.
	 </para>
	 <para>
	   You can search for bookmarks by typing into the <guibutton>Find</guibutton>
           text box.
	 </para>
	 <!-- FIXME: not all of the buttons / menuitems are described -->
       </sect1>

       <sect1 id="bookmarks-importing">
         <title>Importing and Exporting Bookmarks</title>
         <para>
	   You can import and export bookmarks in
	   <application>Epiphany</application> (import only)
	   <application>Netscape</application>,
	   <application>Mozilla</application>, and XBEL (used by galeon and konqueror)
           formats. The
	   importing will take care of duplicated bookmarks. This feature
	   has been tested extensively, but it would still be wise to make
	   a backup of your <filename>~/.galeon/bookmarks.xbel</filename>
	   file before importing or your
	   <application>Netscape</application> or
	   <application>Mozilla</application>
	   <filename>bookmarks.html</filename> file before exporting.
         </para>
       </sect1>

       <sect1 id="bookmarks-toolbars">
         <title>Bookmarks Toolbars</title>
         <para>
	   A toolbar can be created for any folder in your bookmarks,
	   including the AutoBookmarks folder.  As described in <xref
	   linkend="bookmarks-editor"/>, activate the folder's
	   <guibutton>Show as toolbar</guibutton> checkbox in the
	   <emphasis>Bookmark Editor</emphasis> window. Once the change is
	   applied, a toolbar will be created containing all the bookmarks
	   and folders within the selected folder.
           <tip>
             <title>Tip</title>
             <para>
		<guimenuitem>Show as toolbar</guimenuitem> only has an effect if
		<guimenuitem>Bookmarks toolbars</guimenuitem> under
		the <guimenu>View</guimenu> menu is selected. <!-- Bug 62484 -->
             </para>
           </tip>
	 </para>
	 <para>
	   Bookmarks toolbars support Drag and Drop (DnD) functionality.
	   If you want to add a bookmark for the current page to a toolbar,
	   simply click on the link drag button and drag it over an item in
	   the toolbar.  You will see a highlight box drawn around the item
	   when it is possible to drop the new link onto the toolbar.  If
	   you drag the link onto a bookmark item, the new bookmark will be
	   inserted after that item.  If you drag the link onto a folder,
	   the new bookmark will be inserted into the folder.
	 </para>
         <figure id="toolbars-bookmarks.png">
           <title>Bookmarks Toolbar</title>
           <screenshot>
             <graphic fileref="figures/toolbars-bookmarks.png" format="PNG"></graphic>
           </screenshot>
         </figure>
	 <para>
	   Finally, each bookmarks toolbar has a popup menu that allows you
	   to hide the toolbar or delete a bookmark from the toolbar.  To
	   display the popup menu, simply right click on any item in the
	   toolbar.
	 </para>
       </sect1>

       <sect1 id="bookmarks-context-menus">
         <title>Bookmarks Context menus</title>
         <para>
	   You can add any bookmark category to your main document context
	   menu (including the Root folder if you create an alias for it).
	   As described in <xref linkend="bookmarks-editor"/>, activate the
	   folder's <guibutton>Add to context menu</guibutton> checkbox in
	   the <emphasis>Bookmark Editor</emphasis> window.
	 </para>
       </sect1>

       <sect1 id="bookmarks-smart-bookmarks">
         <title>Smart Bookmarks</title>
	 <para>
	   A "smart bookmark" is a bookmark which takes one or more
	   arguments and substitutes them into the URL.  The arguments are
	   represented by the string <literal>%s</literal>.  For example,
	   the URL <literal>http://www.google.com/search?q=%s</literal> can
	   be used to expedite searching with the Google search
	   engine.
	 </para>
         <para>
           To add a smart bookmark, select <menuchoice> <guimenu>Bookmark</guimenu> 
           <guisubmenu>New Smart Bookmark</guisubmenu> </menuchoice>. You then alter the
           <guibutton>Smart URL</guibutton> entry, using <literal>%s</literal>
           to represent the argument.
         </para>
         <para>
           Smart bookmarks can be accessed in a number of ways:
         </para>
	 <para>
	 <itemizedlist>
	   <listitem>
  	     <para>
	       In the location field: If the bookmarks also has a nick, the
	       nick can be used with one or more arguments in the location
	       field.  For a bookmark configured in this manner, typing
	       <userinput>google galeon</userinput> would search for the
	       string <literal>galeon</literal> on Google.
	     </para>
	   </listitem>
	   <listitem>
  	     <para>
	       From the bookmarks menu: Selecting a smart bookmark will
	       open a dialog box for entering the bookmark arguments.
	     </para>
           <note>
           <title>Location</title>
           <para>
             The Location field should be blank to get the dialog.
           </para>
           </note>
	   </listitem>
	   <listitem>
  	     <para>
	       From bookmarks toolbars: If a smart bookmark is placed in a
	       folder which appears as a toolbar, a small right-arrow will
	       appear to its right.  When you click the icon, it will
	       expand to show an entry field on the toolbar. Typing a
	       string into this field and hitting <keycap>Enter</keycap>
	       will insert the string into the smart bookmark and jump to
	       the resulting URL.
	     </para>
	   </listitem>
	   <listitem>
  	     <para>
	       From My Portal: Smart bookmarks will generate form entry
	       fields in My Portal.
	     </para>
	   </listitem>
	 </itemizedlist>
         </para>
       </sect1>

       <sect1 id="bookmarks-nicks">
         <title>Bookmark "Nicks"</title>
         <para>
	   Bookmarks can be given nicknames for quick access. The nick is
	   assigned in the <link linkend="bookmarks-editor">Bookmarks
	   Editor</link>.  When the nick is entered in the Open URL dialog
	   or in the location entry field, is expanded to the full URL
	   automatically.  So, <application>Galeon</application> could be
	   configured so simply typing in <userinput>gal</userinput> could
	   load <literal>http://galeon.sourceforge.net</literal>.
         </para>
       </sect1>

       <sect1 id="bookmarks-aliases">
         <title>Bookmark Aliases</title>
	 <para>
	   Any bookmark or bookmark category (including the Root Bookmark)
	   can have one or several aliases. They are useful for organizing
	   your bookmarks, having special categories used only for toolbars
	   or context menus or other uses that you can imagine.
 	 </para>
       </sect1>

       <sect1 id="bookmarks-with-images">
         <title>Bookmarks with Images</title>
         <para>
	   Bookmarks can be assigned images to replace their names in the
	   bookmarks toolbarsAs well as
	   being a nice way to personalize your browser, this can be used
	   to speed up your browsing (as images are more quickly identified
	   than text) and conserve real estate on your toolbars.
         </para>
       </sect1>

       <sect1 id="bookmarks-my-portal">
         <title>My Portal</title>
         <para>
	   "My Portal" creates a portal style view of your bookmarks in a
	   web page. It makes a good choice for your homepage, since it
	   provides a rapid way of jumping to a large number of bookmarks.
	   You can visit "My Portal" at any time by opening the URL
	   <literal>about:myportal</literal>, or you can set it as your
	   homepage, as described in <xref
	   linkend="prefs-general"/>.
	 </para>
       </sect1>

</chapter>
    
