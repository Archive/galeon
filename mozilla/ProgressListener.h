/*
 *  Copyright (C) 2001 Philip Langdale, Matthew Aubury
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  As of 5th Jan 2005: The progress listener can optionally implement
 *  nsIObserver which allows it to be informed of the temporary file being
 *  used for a download before the final location is specified. We currently
 *  don't do this but if we need that information we can get it.
 *  nsITransfer.h for details.
 */

#ifndef PROGRESSLISTENER2_H__
#define PROGRESSLISTENER2_H__

#include "galeon-embed-persist.h"
#include "galeon-embed-shell.h"
#include "downloader-view.h"

#include <gtk/gtkwidget.h>

#include <nsITransfer.h>
#include <nsIWebProgressListener.h>
#include <nsCOMPtr.h>
#include <nsWeakReference.h>

#include "GulString.h"

class nsIHelperAppLauncherDialog;
class nsIExternalHelperAppService;
class nsIWebBrowserPersist;
class nsIURI;
class nsILocalFile;
class nsIFile;
class nsIRequest;
class nsIDOMDocument;
class nsIObserver;
class nsIInputStream;

#define G_PROGRESSDIALOG_CID                \
{ /* d2a2f743-f126-4f1f-1234-d4e50490f112 */         \
    0xd2a2f743,                                      \
    0xf126,                                          \
    0x4f1f,                                          \
    {0x12, 0x34, 0xd4, 0xe5, 0x04, 0x90, 0xf1, 0x12} \
}

#define G_PROGRESSDIALOG_CLASSNAME "Galeon's Download Progress Dialog"
#define G_PROGRESSDIALOG_CONTRACTID "@mozilla.org/progressdialog;1"

class NS_COM_GLUE GProgressListener : 
			  public nsITransfer,
 			  public nsSupportsWeakReference
{
 public:
 	NS_DECL_ISUPPORTS
	NS_DECL_NSIWEBPROGRESSLISTENER
	NS_DECL_NSIWEBPROGRESSLISTENER2
	NS_DECL_NSITRANSFER

	GProgressListener ();
	virtual ~GProgressListener ();

	NS_METHOD InitForPersist (nsIWebBrowserPersist *aPersist,
				  nsIURI *aURI,
				  nsIFile *aFile,
				  GaleonEmbedPersist *galeonPersist);

	nsresult Pause (void);
	nsresult Resume (void);
	nsresult Abort (void);

 private:
	NS_METHOD PrivateInit (void);
	NS_METHOD LaunchHelperApp (void);
	NS_METHOD SetRequest (nsIRequest *aRequest);

	nsCOMPtr<nsICancelable> mCancelable;
	nsCOMPtr<nsIRequest> mRequest;
	
	GaleonEmbedPersist *mGaleonPersist;
	
	nsCOMPtr<nsIURI> mUri;
	nsCOMPtr<nsIFile> mFile;
	nsCOMPtr<nsIURI> mTarget;
	
	PRInt64 mStartTime;
	PRInt64 mLastUpdate;

	PRBool mCanPause;
	PRBool mIsPaused;
	PRBool mAddToRecent;
	
	Download *mDownload;
	PRInt64   mContentLength;
	GulCString mContentType;

	nsCOMPtr<nsIMIMEInfo> mMIMEInfo;
};

nsresult 
InitiateMozillaDownload (nsIURI *sourceURI, nsILocalFile* inDestFile,
			 GaleonEmbedPersist *embedPersist,
			 nsIDOMDocument *domDocument, 
			 nsISupports *cacheKey,
			 nsIInputStream *postData,
			 PRBool decode,
			 nsIURI *displayURI);

nsresult
BuildDownloadPath (const char *defaultFileName, GtkWidget *parent,
		   nsILocalFile **_retval);

#endif // PROGRESSLISTENER2_H__

