/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "bookmarks-site-tb-widget.h"
#include "galeon-marshal.h"
#include "bookmarks-widgets-private.h"
#include "bookmarks-dnd.h"

#include <gtk/gtkbutton.h>

/**
 * Private data
 */
#define GB_SITE_TB_WIDGET_GET_PRIVATE(object) (G_TYPE_INSTANCE_GET_PRIVATE ((object), \
				       GB_TYPE_SITE_TB_WIDGET, GbSiteTbWidgetPrivate))


struct _GbSiteTbWidgetPrivate 
{
	GtkWidget *mainwid;
};

/**
 * Private functions, only availble from this file
 */
static void		gb_site_tb_widget_finalize_impl	(GObject *o);
static void		gb_site_tb_widget_rebuild_impl	(GbTbWidget *w);
static void		gb_site_tb_widget_clicked_cb	(GtkButton *button, GbSiteTbWidget *w);


/**
 * GbSiteTbWidget object
 */

G_DEFINE_TYPE (GbSiteTbWidget, gb_site_tb_widget, GB_TYPE_TB_WIDGET);

static void
gb_site_tb_widget_class_init (GbSiteTbWidgetClass *klass)
{
	G_OBJECT_CLASS (klass)->finalize = gb_site_tb_widget_finalize_impl;
	GB_TB_WIDGET_CLASS (klass)->rebuild = gb_site_tb_widget_rebuild_impl;

	g_type_class_add_private (klass, sizeof (GbSiteTbWidgetPrivate));
}

static void 
gb_site_tb_widget_init (GbSiteTbWidget *w)
{
	GbSiteTbWidgetPrivate *p = GB_SITE_TB_WIDGET_GET_PRIVATE (w);
	w->priv = p;
}

static void
gb_site_tb_widget_finalize_impl (GObject *o)
{
	G_OBJECT_CLASS (gb_site_tb_widget_parent_class)->finalize (o);
}

GbSiteTbWidget *
gb_site_tb_widget_new (GbSite *site)
{
	GbSiteTbWidget *ret = g_object_new (GB_TYPE_SITE_TB_WIDGET, "bookmark", site, NULL);
	return ret;
}

static void
gb_site_tb_widget_rebuild_impl (GbTbWidget *gtw)
{
	GbSite *site = GB_SITE (gb_tb_widget_get_bookmark (gtw));
	GbSiteTbWidgetPrivate *p = GB_SITE_TB_WIDGET (gtw)->priv;

	if (!p->mainwid)
	{
		p->mainwid = gtk_button_new ();
		gtk_widget_show (p->mainwid);
		gtk_box_pack_start (GTK_BOX (gtw), p->mainwid, FALSE, FALSE, 0);

		g_signal_connect (p->mainwid, "clicked", 
				  G_CALLBACK (gb_site_tb_widget_clicked_cb), gtw);
	}

	if (gtk_bin_get_child (GTK_BIN (p->mainwid)))
	{
		gtk_container_remove (GTK_CONTAINER (p->mainwid),
				      gtk_bin_get_child (GTK_BIN (p->mainwid)));
	}
	
	gb_widgets_fill_tb_item (GTK_CONTAINER (p->mainwid), GB_BOOKMARK (site), site->url);

	gtk_button_set_relief (GTK_BUTTON (p->mainwid), GTK_RELIEF_NONE);
	GTK_WIDGET_SET_FLAGS (GTK_BUTTON (p->mainwid), GTK_CAN_FOCUS);

	gb_tb_widget_setup_context_menu (gtw, p->mainwid);
	gb_bookmark_dnd_drag_source_set (p->mainwid, GB_BOOKMARK (site));
}

static void
gb_site_tb_widget_clicked_cb (GtkButton *button, GbSiteTbWidget *w)
{
	GbBookmark *b = gb_tb_widget_get_bookmark (GB_TB_WIDGET (w));
	if (b)
	{
		gb_activated_activate (w, b, GB_SITE (b)->url, GB_BAF_DEFAULT);
	}
}

