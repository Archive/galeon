/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __bookmarks_editor_h
#define __bookmarks_editor_h

#include <glib-object.h>
#include <gtk/gtkwidget.h>
#include "bookmarks-tree-model.h"
#include "bookmarks-tree-view.h"
#include "bookmarks-location-source.h"
#include "bookmarks-util.h"

/* object forward declarations */

typedef struct _GbEditor GbEditor;
typedef struct _GbEditorClass GbEditorClass;
typedef struct _GbEditorPrivate GbEditorPrivate;

/**
 * Editor object
 */

#define GB_TYPE_EDITOR			(gb_editor_get_type())
#define GB_EDITOR(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), GB_TYPE_EDITOR,\
					 GbEditor))
#define GB_EDITOR_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), GB_TYPE_EDITOR,\
					 GbEditorClass))
#define GB_IS_EDITOR(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), GB_TYPE_EDITOR))
#define GB_IS_EDITOR_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), GB_TYPE_EDITOR))
#define GB_EDITOR_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), GB_TYPE_EDITOR,\
					 GbEditorClass))

struct _GbEditorClass 
{
	GObjectClass parent_class;
	
	/* virtual methods */
	
	void			(*gb_editor_init_widgets)	(GbEditor *e);

	/* signals */
	GbBookmarkActivatedCallback gb_editor_bookmark_activated;

};

/* Remember: fields are public read-only */
struct _GbEditor
{
	GObject parent_object;

	GbEditorPrivate *priv;
};

GType		gb_editor_get_type		(void);
GbEditor *	gb_editor_new			(GbTreeModel *model);
GbEditor *	gb_editor_new_for_set		(GbBookmarkSet *set);
void		gb_editor_set_model		(GbEditor *editor, GbTreeModel *model);
GtkWidget *	gb_editor_get_main_widget	(GbEditor *editor);
void		gb_editor_select		(GbEditor *e, GbBookmark *b);
void		gb_editor_set_show_right_tree_view (GbEditor *e, gboolean value);
void		gb_editor_set_show_edit_pane	(GbEditor *e, gboolean value);
void		gb_editor_set_location_source	(GbEditor *e, GbLocationSource *s);

/* protected */
GbTreeView *	gb_editor_get_tree_view		(GbEditor *e);
void		gb_editor_set_main_widget	(GbEditor *e, GtkWidget *w);
void		gb_editor_set_search_widgets	(GbEditor *e, GtkWidget *search_text_entry);

/* commands */

void 		gb_editor_cmd_add_site				(GbEditor *e);
void 		gb_editor_cmd_add_smart_site			(GbEditor *e);
void 		gb_editor_cmd_add_folder			(GbEditor *e);
void		gb_editor_cmd_add_separator			(GbEditor *e);
void		gb_editor_cmd_add_alias				(GbEditor *e);
void		gb_editor_cmd_add_v_folder			(GbEditor *e);
void		gb_editor_cmd_add_auto_folder			(GbEditor *e);
void		gb_editor_cmd_edit_remove			(GbEditor *e);
void		gb_editor_cmd_file_open				(GbEditor *e);
void		gb_editor_cmd_file_close			(GbEditor *e);
void		gb_editor_cmd_edit_properties			(GbEditor *e);
void		gb_editor_cmd_edit_find				(GbEditor *e);
void		gb_editor_cmd_edit_cut				(GbEditor *e);
void		gb_editor_cmd_edit_copy				(GbEditor *e);
void		gb_editor_cmd_edit_paste			(GbEditor *e);
void		gb_editor_cmd_edit_set_as_default_folder	(GbEditor *e);
void		gb_editor_cmd_import_mozilla			(GbEditor *e);
void		gb_editor_cmd_import_netscape			(GbEditor *e);
void		gb_editor_cmd_import_konqueror			(GbEditor *e);
void		gb_editor_cmd_import_epiphany			(GbEditor *e);
void		gb_editor_cmd_export_mozilla			(GbEditor *e);
void		gb_editor_cmd_export_netscape			(GbEditor *e);
void		gb_editor_cmd_export_konqueror			(GbEditor *e);
void 		gb_editor_cmd_sort				(GbEditor *e, gboolean folders_first, 
								 gboolean recursive);
void		gb_editor_cmd_edit_goto				(GbEditor *e);
void		gb_editor_cmd_split_view			(GbEditor *e, gboolean split);
void		gb_editor_cmd_edit_pane				(GbEditor *e, gboolean pane);


#endif
