/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __bookmarks_toolbar_widgets_h
#define __bookmarks_toolbar_widgets_h

#include "bookmarks.h"

GbTbWidget *	gb_create_toolbar_widget_site			(GbSite *s);
GbTbWidget *	gb_create_toolbar_widget_folder			(GbFolder *f);
GbTbWidget *	gb_create_toolbar_widget_smart_site		(GbSmartSite *s);
GbTbWidget *	gb_create_toolbar_widget_separator		(GbSeparator *s);
GbTbWidget *	gb_create_toolbar_widget_alias_placeholder	(GbAliasPlaceholder *s);

#endif
