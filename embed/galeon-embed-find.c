/*
 *  Copyright (C) 2005 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#include "config.h"

#include "galeon-embed-find.h"

void
galeon_embed_find_set_embed (GaleonEmbedFind *find,
			   GaleonEmbed *embed)
{
	GaleonEmbedFindIface *iface = GALEON_EMBED_FIND_GET_IFACE (find);
	iface->set_embed (find, embed);
}

/**
 * galeon_embed_find_set_properties:
 * @find: an #GaleonEmbedFind
 * @case_sensitive: %TRUE for "case sensitive" to be set
 *
 * Sets the properties of @find
 **/
void
galeon_embed_find_set_properties (GaleonEmbedFind *find,
				const char *search_string,
				gboolean case_sensitive)
{
	GaleonEmbedFindIface *iface = GALEON_EMBED_FIND_GET_IFACE (find);
	iface->set_properties (find, search_string, case_sensitive);
}

/**
 * galeon_embed_find_find:
 * @embed: an #GaleonEmbedFind
 * @search_string: the text to search for
 * @links_only: whether to only search the text in links
 *
 * Return value: whether a match was found
 **/
GaleonEmbedFindResult
galeon_embed_find_find (GaleonEmbedFind *find,
		      const char *search_string,
		      gboolean links_only)
{
	GaleonEmbedFindIface *iface = GALEON_EMBED_FIND_GET_IFACE (find);
	return iface->find (find, search_string, links_only);
}

/**
 * galeon_embed_find_find_again:
 * @embed: an #GaleonEmbedFind
 * @forward %TRUE to search forwards in the document
 *
 * Return value: whether a match was found
 **/
GaleonEmbedFindResult
galeon_embed_find_find_again (GaleonEmbedFind *find,
			    gboolean forward,
			    gboolean links_only)
{
	GaleonEmbedFindIface *iface = GALEON_EMBED_FIND_GET_IFACE (find);
	return iface->find_again (find, forward, links_only);
}

void
galeon_embed_find_set_selection (GaleonEmbedFind *find,
                              gboolean attention)
{
       GaleonEmbedFindIface *iface = GALEON_EMBED_FIND_GET_IFACE (find);
       iface->set_selection (find, attention);
}

/**
 * galeon_embed_find_activate_link:
 * @embed: an #GaleonEmbedFind
 * @mask:
 * 
 * Activates the currently focused link, if there is any.
 * 
 * Return value: %TRUE if a link was activated
 **/
gboolean
galeon_embed_find_activate_link (GaleonEmbedFind *find,
			       GdkModifierType mask)
{
	GaleonEmbedFindIface *iface = GALEON_EMBED_FIND_GET_IFACE (find);
	return iface->activate_link (find, mask);
}

/**
 * galeon_embed_find_get_prefs
 * @embed: an #GaleonEmbedFind
 * @autostart: A pointer to a gboolean that will be filled in if the find
 *             interface wants to autostart on a key press
 * @linksonly: A pointer to a gboolean that will be filled in if the find
 *             interface wants autostart to just search for links
 **/
void
galeon_embed_find_get_prefs (GaleonEmbedFind *find,
			     gboolean* autostart,
			     gboolean* linksonly)
{
	GaleonEmbedFindIface *iface = GALEON_EMBED_FIND_GET_IFACE (find);
	iface->get_prefs (find, autostart, linksonly);
}


GType
galeon_embed_find_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0))
	{
		static const GTypeInfo our_info =
		{
			sizeof (GaleonEmbedFindIface),
			NULL,
			NULL,
		};

		type = g_type_register_static (G_TYPE_INTERFACE,
					       "GaleonEmbedFind",
					       &our_info, (GTypeFlags) 0);
	}

	return type;
}
