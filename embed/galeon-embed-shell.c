/*
 *  Copyright (C) 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>

#include "galeon-embed-shell.h"
#include "galeon-marshal.h"

#include "galeon-config.h"

#include <string.h>

#define PAGE_SETUP_FILENAME     GALEON_DIR "/page-setup-gtk.ini"
#define PRINT_SETTINGS_FILENAME GALEON_DIR "/print-settings.ini"

enum
{
	NEW_WINDOW,
	PERMISSION_CHANGED,
	ADD_SIDEBAR,
	LAST_SIGNAL
};
static guint galeon_embed_shell_signals[LAST_SIGNAL] = { 0 };

#define GALEON_EMBED_SHELL_GET_PRIVATE(object) (G_TYPE_INSTANCE_GET_PRIVATE ((object), \
				       GALEON_TYPE_EMBED_SHELL, GaleonEmbedShellPrivate))

struct GaleonEmbedShellPrivate
{
	GlobalHistory *global_history;
	DownloaderView *downloader_view;
	GaleonJSConsole *js_console;
	GtkPageSetup *page_setup;
	GtkPrintSettings *print_settings;
};

static void
galeon_embed_shell_finalize (GObject *object);

static GlobalHistory *
impl_get_global_history (GaleonEmbedShell *shell);
static DownloaderView *
impl_get_downloader_view (GaleonEmbedShell *shell);
static GaleonJSConsole *
impl_get_js_console (GaleonEmbedShell *shell);

GaleonEmbedShell *embed_shell;

G_DEFINE_TYPE (GaleonEmbedShell, galeon_embed_shell, G_TYPE_OBJECT);

static void
galeon_embed_shell_class_init (GaleonEmbedShellClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

        object_class->finalize = galeon_embed_shell_finalize;
	klass->get_downloader_view = impl_get_downloader_view;
	klass->get_global_history = impl_get_global_history;
	klass->get_js_console     = impl_get_js_console;

	galeon_embed_shell_signals[NEW_WINDOW] =
                g_signal_new ("new_window_orphan",
                              G_OBJECT_CLASS_TYPE (object_class),
                              G_SIGNAL_RUN_LAST,
                              G_STRUCT_OFFSET (GaleonEmbedShellClass, new_window),
                              _galeon_embed_new_window_signal_accumulator, NULL,
                              galeon_marshal_OBJECT__INT,
			      G_TYPE_OBJECT,
                              1,
			      G_TYPE_INT);
	
	galeon_embed_shell_signals[PERMISSION_CHANGED] =
                g_signal_new ("permission_changed",
                              G_OBJECT_CLASS_TYPE (object_class),
                              G_SIGNAL_RUN_LAST,
                              G_STRUCT_OFFSET (GaleonEmbedShellClass, permission_changed),
                              NULL, NULL,
                              galeon_marshal_VOID__VOID,
                              G_TYPE_NONE,
                              0);

	galeon_embed_shell_signals[ADD_SIDEBAR] =
		g_signal_new ("add_sidebar",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (GaleonEmbedShellClass, add_sidebar),
			      NULL, NULL,
			      galeon_marshal_VOID__STRING_STRING,
			      G_TYPE_NONE,
			      2,
			      G_TYPE_STRING,
			      G_TYPE_STRING);

	g_type_class_add_private (klass, sizeof (GaleonEmbedShellPrivate));
}

static void
galeon_embed_shell_init (GaleonEmbedShell *ges)
{
	/* Singleton, globally accessible */
	embed_shell = ges;
	
	ges->priv = GALEON_EMBED_SHELL_GET_PRIVATE (ges);
}

static void
galeon_embed_shell_finalize (GObject *object)
{
	GaleonEmbedShell *ges = GALEON_EMBED_SHELL (object);

	if (ges->priv->global_history)
	{
		global_history_save_if_needed (ges->priv->global_history);
		g_object_unref (ges->priv->global_history);
	}

	if (ges->priv->downloader_view)
	{
		g_object_remove_weak_pointer 
			(G_OBJECT(ges->priv->downloader_view),
			 (gpointer *)&ges->priv->downloader_view);
		g_object_unref (ges->priv->downloader_view);
	}

	if (ges->priv->js_console)
	{
		g_object_unref (ges->priv->js_console);
	}
	
        G_OBJECT_CLASS (galeon_embed_shell_parent_class)->finalize (object);
}

gboolean
galeon_embed_shell_initialize (GaleonEmbedShell *shell)
{
	GaleonEmbedShellClass *klass = GALEON_EMBED_SHELL_GET_CLASS (shell);
        return klass->initialize (shell);
}

GlobalHistory *
galeon_embed_shell_get_global_history (GaleonEmbedShell *shell)
{
	GaleonEmbedShellClass *klass = GALEON_EMBED_SHELL_GET_CLASS (shell);
        return klass->get_global_history (shell);
}

DownloaderView *
galeon_embed_shell_get_downloader_view (GaleonEmbedShell *shell)
{
	GaleonEmbedShellClass *klass = GALEON_EMBED_SHELL_GET_CLASS (shell);
        return klass->get_downloader_view (shell);
}

GaleonJSConsole *
galeon_embed_shell_get_js_console (GaleonEmbedShell *shell)
{
	GaleonEmbedShellClass *klass = GALEON_EMBED_SHELL_GET_CLASS (shell);
        return klass->get_js_console (shell);
}

void
galeon_embed_shell_clear_cache (GaleonEmbedShell *shell,
				CacheType type)
{
	GaleonEmbedShellClass *klass = GALEON_EMBED_SHELL_GET_CLASS (shell);
        klass->clear_cache (shell, type);
}

void
galeon_embed_shell_set_offline_mode (GaleonEmbedShell *shell,
				     gboolean offline)
{
	GaleonEmbedShellClass *klass = GALEON_EMBED_SHELL_GET_CLASS (shell);
        klass->set_offline_mode (shell, offline);
}

void
galeon_embed_shell_show_java_console (GaleonEmbedShell *shell)
{
	GaleonEmbedShellClass *klass = GALEON_EMBED_SHELL_GET_CLASS (shell);
        klass->show_java_console (shell);
}

GaleonEncodings *
galeon_embed_shell_get_encodings (GaleonEmbedShell *shell)
{
	GaleonEmbedShellClass *klass = GALEON_EMBED_SHELL_GET_CLASS (shell);
        return klass->get_encodings (shell);
}

GList *
galeon_embed_shell_get_font_langs (GaleonEmbedShell *shell)
{
	GaleonEmbedShellClass *klass = GALEON_EMBED_SHELL_GET_CLASS (shell);
        return klass->get_font_langs (shell);
}

GList *
galeon_embed_shell_get_font_list (GaleonEmbedShell *shell,
				  const char *langGroup,
				  const char *fontType)
{
	GaleonEmbedShellClass *klass = GALEON_EMBED_SHELL_GET_CLASS (shell);
        return klass->get_font_list (shell, langGroup, fontType);
}

void
galeon_embed_shell_set_permission (GaleonEmbedShell *shell,
				   const char *url, 
				   PermissionType type,
				   gboolean allow)
{
	GaleonEmbedShellClass *klass = GALEON_EMBED_SHELL_GET_CLASS (shell);
        klass->set_permission (shell, url, type, allow);
}

GList *
galeon_embed_shell_list_permissions (GaleonEmbedShell *shell,
				     PermissionType type)
{
	GaleonEmbedShellClass *klass = GALEON_EMBED_SHELL_GET_CLASS (shell);
        return klass->list_permissions (shell, type);
}

void
galeon_embed_shell_remove_permissions (GaleonEmbedShell *shell,
				       PermissionType type,
				       GList *permissions)
{
	GaleonEmbedShellClass *klass = GALEON_EMBED_SHELL_GET_CLASS (shell);
        klass->remove_permissions (shell, type, permissions);
}

PermissionActionType
galeon_embed_shell_test_permission(GaleonEmbedShell *shell,
				   const char *url,
				   PermissionType type)
{
	GaleonEmbedShellClass *klass = GALEON_EMBED_SHELL_GET_CLASS(shell);
	return klass->test_permission(shell, url, type);
}

GList *
galeon_embed_shell_list_cookies (GaleonEmbedShell *shell)
{
	GaleonEmbedShellClass *klass = GALEON_EMBED_SHELL_GET_CLASS (shell);
        return klass->list_cookies (shell);
}

void
galeon_embed_shell_remove_cookies (GaleonEmbedShell *shell,
				   GList *cookies,
				   gboolean block)
{
	GaleonEmbedShellClass *klass = GALEON_EMBED_SHELL_GET_CLASS (shell);
        klass->remove_cookies (shell, cookies, block);
}

GList *
galeon_embed_shell_list_passwords (GaleonEmbedShell *shell,
				   PasswordType type)
{
	GaleonEmbedShellClass *klass = GALEON_EMBED_SHELL_GET_CLASS (shell);
        return klass->list_passwords (shell, type);
}

void
galeon_embed_shell_remove_passwords (GaleonEmbedShell *shell,
				     GList *passwords, 
				     PasswordType type)
{
	GaleonEmbedShellClass *klass = GALEON_EMBED_SHELL_GET_CLASS (shell);
        klass->remove_passwords (shell, passwords, type);
}

static GlobalHistory *
impl_get_global_history (GaleonEmbedShell *shell)
{
	if (!shell->priv->global_history)
	{
		shell->priv->global_history = global_history_new ();
	}
	
	return shell->priv->global_history;
}

static DownloaderView *
impl_get_downloader_view (GaleonEmbedShell *shell)
{
	if (!shell->priv->downloader_view)
	{
		shell->priv->downloader_view = downloader_view_new ();
		g_object_add_weak_pointer 
			(G_OBJECT(shell->priv->downloader_view),
			 (gpointer *)&shell->priv->downloader_view);
	}
	
	return shell->priv->downloader_view;
}

static GaleonJSConsole *
impl_get_js_console (GaleonEmbedShell *shell)
{
	if (!shell->priv->js_console)
	{
		shell->priv->js_console = GALEON_JS_CONSOLE (galeon_js_console_new ());
		g_object_ref (shell->priv->js_console);
		g_object_ref_sink (shell->priv->js_console);
	}
	
	return shell->priv->js_console;
}

void
galeon_embed_shell_free_permissions (GaleonEmbedShell *shell,
				     GList *permissions)
{
	GList *l;
	
	for (l = permissions; l != NULL; l = l->next)
	{
		PermissionInfo *info = (PermissionInfo *)l->data;
		
		g_free (info->domain);
		g_free (info);
	}

	g_list_free (permissions);
}

GtkPageSetup *
galeon_embed_shell_get_page_setup (GaleonEmbedShell *shell)
{
	GaleonEmbedShellPrivate *priv;

	g_return_val_if_fail (GALEON_IS_EMBED_SHELL (shell), NULL);
	priv = shell->priv;

	if (priv->page_setup == NULL)
	{
		GError *error = NULL;
		char *path;

		path = g_build_filename (g_get_home_dir (), PAGE_SETUP_FILENAME, NULL);
		priv->page_setup = gtk_page_setup_new_from_file (path, &error);
		g_free (path);
		if (error)
		{
			g_error_free (error);
		}

		/* If that still didn't work, create a new, empty one */
		if (priv->page_setup == NULL)
		{
			priv->page_setup = gtk_page_setup_new ();
		}
	}

	return priv->page_setup;
}

void
galeon_embed_shell_set_page_setup (GaleonEmbedShell *shell,
                                   GtkPageSetup *page_setup)
{
	GaleonEmbedShellPrivate *priv;
	char *path;

	g_return_if_fail (GALEON_IS_EMBED_SHELL (shell));
	priv = shell->priv;

	if (page_setup != NULL)
	{
		g_object_ref (page_setup);
	}
	else
	{
		page_setup = gtk_page_setup_new ();
	}

	if (priv->page_setup != NULL)
	{
		g_object_unref (priv->page_setup);
	}

	priv->page_setup = page_setup;

	path = g_build_filename (g_get_home_dir (), PAGE_SETUP_FILENAME, NULL);
	gtk_page_setup_to_file (page_setup, path, NULL);
	g_free (path);
}

GtkPrintSettings *
galeon_embed_shell_get_print_settings (GaleonEmbedShell *shell)
{
	GaleonEmbedShellPrivate *priv;

	g_return_val_if_fail (GALEON_IS_EMBED_SHELL (shell), NULL);              
	priv = shell->priv;

	if (priv->print_settings == NULL)
	{
		GError *error = NULL;
		char *path;

		path = g_build_filename (g_get_home_dir (), PRINT_SETTINGS_FILENAME, NULL);
		priv->print_settings = gtk_print_settings_new_from_file (path, &error);
		g_free (path);

		if (priv->print_settings == NULL)
		{
			priv->print_settings = gtk_print_settings_new ();
		}
	}

	return priv->print_settings;
}

void
galeon_embed_shell_set_print_settings (GaleonEmbedShell *shell,
                                       GtkPrintSettings *settings)
{
	GaleonEmbedShellPrivate *priv;
	char *path;

	g_return_if_fail (GALEON_IS_EMBED_SHELL (shell));
	priv = shell->priv;

	if (settings != NULL)
	{
		g_object_ref (settings);
	}

	if (priv->print_settings != NULL)
	{
		g_object_unref (priv->print_settings);
	}

	priv->print_settings = settings ? settings : gtk_print_settings_new ();

	path = g_build_filename (g_get_home_dir (), PRINT_SETTINGS_FILENAME, NULL);
	gtk_print_settings_to_file (settings, path, NULL);
	g_free (path);
}

void
galeon_embed_shell_free_cookies (GaleonEmbedShell *shell,
				 GList *cookies)
{
	g_list_foreach (cookies, (GFunc)cookie_info_free, NULL);
	g_list_free (cookies);
}

void
galeon_embed_shell_free_passwords (GaleonEmbedShell *shell,
				   GList *passwords)
{
	GList *l;
	
	for (l = passwords; l != NULL; l = l->next)
	{
		PasswordInfo *info = (PasswordInfo *)l->data;
		g_free (info->host);
		g_free (info->username);
		g_free (info->password);
		g_free (info->httpRealm);
		g_free (info->formSubmitURL);
		g_free (info->usernameField);
		g_free (info->passwordField);
		g_free (info);
	}

	g_list_free (passwords);
}


/* Accumulator for the new_window signals, returns as soon as a 
 * GALEON_EMBED object is returned */
gboolean
_galeon_embed_new_window_signal_accumulator (GSignalInvocationHint *ihint,
					     GValue                *return_accu,
					     const GValue          *handler_return,
					     gpointer               dummy)
{
	GObject *object = g_value_get_object (handler_return);
	if (object)
	{
		g_return_val_if_fail (GALEON_IS_EMBED (object), TRUE);

		g_value_set_object (return_accu, object);
		return FALSE;
	}
	return TRUE;
}

