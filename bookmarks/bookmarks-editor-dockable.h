/*
 *  Copyright (C) 2002  Ricardo Fernández Pascual
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __bookmarks_editor_dockable_h
#define __bookmarks_editor_dockable_h

#include "bookmarks-editor.h"

/* object forward declarations */

typedef struct _GbEditorDockable GbEditorDockable;
typedef struct _GbEditorDockableClass GbEditorDockableClass;
typedef struct _GbEditorDockablePrivate GbEditorDockablePrivate;

/**
 * Dockable Editor object
 */

#define GB_TYPE_EDITOR_DOCKABLE			(gb_editor_dockable_get_type())
#define GB_EDITOR_DOCKABLE(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), \
						 GB_TYPE_EDITOR_DOCKABLE,\
						 GbEditorDockable))
#define GB_EDITOR_DOCKABLE_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), GB_TYPE_EDITOR_DOCKABLE,\
						 GbEditorDockableClass))
#define GB_IS_EDITOR_DOCKABLE(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), \
						 GB_TYPE_EDITOR_DOCKABLE))
#define GB_IS_EDITOR_DOCKABLE_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), GB_TYPE_EDITOR_DOCKABLE))
#define GB_EDITOR_DOCKABLE_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), GB_TYPE_EDITOR_DOCKABLE,\
						 GbEditorDockableClass))

struct _GbEditorDockableClass 
{
	GbEditorClass parent_class;

};

/* Remember: fields are public read-only */
struct _GbEditorDockable
{
	GbEditor parent_object;
};

GType			gb_editor_dockable_get_type		(void);
GbEditorDockable *	gb_editor_dockable_new			(GbTreeModel *model);
GbEditorDockable *	gb_editor_dockable_new_for_set		(GbBookmarkSet *set);

#endif
