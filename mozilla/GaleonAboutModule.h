/*
 *  Copyright (C) 2001 Matt Aubury, Philip Langdale
 *  Copyright (C) 2004 Crispin Flowerday
 *  Copyright (C) 2005 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */
 
#ifndef GALEON_PROTOCOL_HANDLER_H
#define GALEON_PROTOCOL_HANDLER_H
    
#include <nsError.h>
#include <nsIAboutModule.h>

/* a9aea13e-21de-4be8-a07e-a05f11658c55 */
#define GALEON_ABOUT_MODULE_CID \
{ 0xa9aea13e, 0x21de, 0x4be8, \
  { 0xa0, 0x7e, 0xa0, 0x5f, 0x11, 0x65, 0x8c, 0x55 } }

#define GALEON_ABOUT_NETERROR_CONTRACTID	NS_ABOUT_MODULE_CONTRACTID_PREFIX "neterror"
#define GALEON_ABOUT_NETERROR_CLASSNAME	"Galeon about:neterror module"
#define GALEON_ABOUT_TOPHER_CONTRACTID	NS_ABOUT_MODULE_CONTRACTID_PREFIX "topher"
#define GALEON_ABOUT_TOPHER_CLASSNAME	"Galeon about:topher module"

class nsIChannel;
class nsIOutputStream;
class nsIURI;

class GaleonAboutModule : public nsIAboutModule
{
  public:
	NS_DECL_ISUPPORTS
	NS_DECL_NSIABOUTMODULE

	GaleonAboutModule();
	virtual ~GaleonAboutModule();

  private:
	nsresult Redirect(const nsACString&, nsIChannel**);
	nsresult ParseErrorURL(const char*, nsACString&, nsACString&, nsACString&, nsACString&);
	nsresult GetErrorMessage(nsIURI*, const char*, char**, char**, char**, char**);
	nsresult CreateErrorPage(nsIURI*, nsIChannel**);
	nsresult Write(nsIOutputStream*, const char*);
};

#endif /* GALEON_PROTOCOL_HANDLER_H */
