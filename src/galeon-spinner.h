/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/*
 * Nautilus
 *
 * Copyright (C) 2000 Eazel, Inc.
 *
 * Nautilus is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Andy Hertzfeld <andy@eazel.com>
 *
 * This is the header file for the throbber on the location bar
 *
 */

#ifndef GALEON_SPINNER_H
#define GALEON_SPINNER_H

#include <gtk/gtkeventbox.h>

G_BEGIN_DECLS

#define GALEON_TYPE_SPINNER		(galeon_spinner_get_type ())
#define GALEON_SPINNER(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), GALEON_TYPE_SPINNER, GaleonSpinner))
#define GALEON_SPINNER_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), GALEON_TYPE_SPINNER, GaleonSpinnerClass))
#define GALEON_IS_SPINNER(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GALEON_TYPE_SPINNER))
#define GALEON_IS_SPINNER_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GALEON_TYPE_SPINNER))

typedef struct GaleonSpinner GaleonSpinner;
typedef struct GaleonSpinnerClass GaleonSpinnerClass;
typedef struct GaleonSpinnerDetails GaleonSpinnerDetails;

struct GaleonSpinner {
	GtkEventBox parent;
	GaleonSpinnerDetails *details;
};

struct GaleonSpinnerClass {
	GtkEventBoxClass parent_class;
};

GType         galeon_spinner_get_type       (void);
GtkWidget    *galeon_spinner_new            (void);
void          galeon_spinner_start          (GaleonSpinner *throbber);
void          galeon_spinner_stop           (GaleonSpinner *throbber);
void          galeon_spinner_set_small_mode (GaleonSpinner *throbber,
					     gboolean new_mode);

G_END_DECLS

#endif /* GALEON_SPINNER_H */


